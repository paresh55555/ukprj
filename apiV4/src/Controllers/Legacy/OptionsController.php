<?php


namespace Controllers\Legacy;

use SalesQuoter\MysqlConnection;

use SalesQuoter\Windows\CutSheet;


class OptionsController 
{
    protected $aci;

    protected $mysqlConnection;

    public function __construct()
    {
        $this->mysqlConnection = new MysqlConnection();
    }


    public function halfButtons($request)
    {
        $allGetVars = $request->getQueryParams();
        $option = $allGetVars['option'];

        $dbCon = $this->mysqlConnection->connect_db_pdo();

        if ($option != 'track' && $option != 'frame' && $option != 'track2') {
            return;
        }

        $sql = " SELECT * from SQ_halfButtons where `type`=?  ";
        $stmt = $dbCon->prepare($sql);
        $stmt->bind_param("s", $option);
        $stmt->execute();
        $results = $stmt->get_result();
        $halfButtons = Array();

        while ($halfButton = $results->fetch_assoc()) {
            $halfButtons[] = $halfButton;
        }

        header("Content-Type: application/json");
        echo json_encode($halfButtons);

    }

    public function method2()
    {
//your code
//to access items in the container... $this->ci->get('');
    }

    public function method3()
    {
//your code
//to access items in the container... $this->ci->get('');
    }
}