<?php

use SalesQuoter\SiteBranding\SiteBranding;

$app->get(
    '/siteBranding',
    function ($request, $response, $args) {

        $siteBranding = new SiteBranding();
        $responses =  $siteBranding->getWhere([]);

        $responseData = array('status' => 'success', 'data' => $responses );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->put(
    '/siteBranding',
    function ($request, $response, $args) {

        $data = jsonDecodeWithErrorChecking($request->getBody());
        $data['modified'] = date("Y-m-d H:i:s");

        if (!isset($data['id'])) {
            $responseData = array('status' => 'error', 'message' => 'id is missing' );

            return responseWithStatusCode($response, $responseData, 400);
        }

        $siteBranding = new SiteBranding();
        $responses =  $siteBranding->update($data);

        $responseData = array('status' => 'success', 'data' => $responses );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->delete(
    '/siteBranding',
    function ($request, $response, $args) {

        $siteBranding = new SiteBranding();
        $responses =  $siteBranding->getWhere([]);
   
        $deleteResponse = $siteBranding->delete($responses[0]['id']);

        return responseWithStatusCode($response, $deleteResponse, 200);
    }
);
