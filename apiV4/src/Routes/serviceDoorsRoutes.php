<?php
use SalesQuoter\Service\Doors;
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\DoorTraits;
use SalesQuoter\Service\DoorTypes;
use SalesQuoter\Service\DoorNotes;
use SalesQuoter\Service\DoorCustomers;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/orders/{id}/doors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doors = new Doors();
        $orders = new Orders();
        $traits = new DoorTraits();
        $notes = new DoorNotes();
        $doorCustomer = new DoorCustomers();

        $checkOrderExists = $orders->get( $args["id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {
            $allItems = $doors->getAll ( ["order_id" => $args["id"]] );

            for ($count = 0; $count < sizeof($allItems); $count++) {

                $allItems[$count]["traits"] = $traits->getWhere ( ["door_id" => $allItems[$count]["id"]] );
                $allItems[$count]["notes"] =  $notes->getWhere ( ["door_id" => $allItems[$count]["id"]] );
                $allItems[$count]["customers"] = $doorCustomer->getAll ( ["door_id" => $allItems[$count]["id"] ] );
            }

            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/orders/{order_id}/doors/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doors = new Doors();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );
        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {
            $allItems = $doors->getWhere ( ["order_id" => $args["order_id"], "id" => $args["id"] ] );

            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->post(
    '/service/orders/{order_id}/doors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doors = new Doors();
        $orders = new Orders();
        $traits = new DoorTraits();
        $activityLog = new ActivityLog();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else{

            $data = $request->getBody();
            $config = jsonDecodeWithErrorChecking($data);

            $config["order_id"] = $args["order_id"];

            $newDoor = $doors->create($config);
            $doors->createTraits( $newDoor[0]["id"], $config["traits"] );
            $newDoor[0]["traits"] = $traits->getWhere ( ["door_id" =>  $newDoor[0]["id"]] );

            $logMessage = "Doors has been added";
            $activityLog->addLog ( $logMessage, $args['order_id'], "doors", "orders", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newDoor );

            return responseWithStatusCode($response, $responseData, 201);
        }
    }
);


$app->put(
    '/service/orders/{order_id}/doors/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doors = new Doors();
        $orders = new Orders();
        $traits = new DoorTraits();
        $activityLog = new ActivityLog();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $data = $request->getBody();
            $config = jsonDecodeWithErrorChecking($data);

            $config["id"] = $args["id"];
            $updatedDoor = $doors->update($config);

            $doors->createTraits( $config["id"], $config["traits"] );
            $updatedDoor[0]["traits"] = $traits->getWhere ( ["door_id" =>  $config["id"]] );

            $logMessage = "Doors has been updated";
            $activityLog->addLog ( $logMessage, $args['order_id'], "doors", "orders", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $updatedDoor);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->delete(
    '/service/orders/{order_id}/doors/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doors = new Doors();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $checkOrderExists = $orders->get( $args["order_id"] );

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $doors->delete($args['id']);

            if ( $responseData["status"] == 'success') {

                $logMessage = "Doors has been deleted";
                $activityLog->addLog ( $logMessage, $args['order_id'], "doors", "orders", $this->token->decoded->userId );

            }

        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->get(
    '/services/doorTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorTypes = new DoorTypes();
        $allGetVars = $request->getQueryParams();

        if ( isset($allGetVars['category']) && strlen($allGetVars['category']) > '0') {
           $category = $allGetVars['category']; 
        }else {
           $responseData = array('status' => 'error', 'msg' => 'category cannot be empty');
        
           return responseWithStatusCode($response, $responseData, 400);
        }

        $allItems = $doorTypes->getWhere ( [ "category" => $category ] );

        $responseData = array ("status" => "success", "data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->delete(
    '/services/doorTypes/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorTypes = new DoorTypes();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else {
            $responseCode = 200;
            $responseData = $doorTypes->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);

$app->post(
    '/services/doorTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorTypes = new DoorTypes();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( strlen($config["type"]) == '0' ) {
            $statusCode = 400;
            $responseData = [ "status" => "error" , "msg" => "type cannot be empty" ];
        }elseif (strlen($config["category"]) == '0') {
            $statusCode = 400;
            $responseData = [ "status" => "error" , "msg" => "category cannot be empty" ];
        }else {

            $statusCode = 200; 
            $docsType = $doorTypes->create ( $config );

            $responseData = array ("status" => "success", "data" => $docsType );
        }

        return responseWithStatusCode($response, $responseData, $statusCode);
    }
);
