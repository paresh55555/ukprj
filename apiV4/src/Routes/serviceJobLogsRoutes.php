<?php
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/logs/{job_id}/activity_log/{type}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $log = new ActivityLog();

        $allItems = $log->getAll ( ["field_id" => $args["job_id"] , "log_type" => $args["type"] ] );
        
        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);
    }
);