<?php
use SalesQuoter\Service\Jobs;
use SalesQuoter\Service\JobsDoors;
use SalesQuoter\Service\JobStatus;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/jobs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();

        if ( isset($allGetVars['start_date']) && strlen($allGetVars['start_date']) > '0') {
           $startDate = ["start_date" => $allGetVars['start_date'] ];
        }else {
           $startDate = [];
        }

        $jobs = new Jobs();
        $allItems = $jobs->getAll( $startDate );

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->get(
    '/service/jobs/status',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();

        if ( isset($allGetVars['filter']) && strlen($allGetVars['filter']) > '0') {
           $filters = ["id" => $allGetVars['filter'] ];
        }else {
           $filters = [];
        }

        $jobStatus = new JobStatus();
        $allItems = $jobStatus->getAll( $filters );

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->get(
    '/service/jobs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobs = new Jobs();
        $allItems = $jobs->getAll( [ "id" => $args["id"] ] );

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->post(
    '/service/jobs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);


        if ( strlen($config["start_date"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "start date cannot be empty");

            return responseWithStatusCode($response, $responseData, 400);
        }else{

            $newJob = $jobs->create($config);
            $jobs->updateJobNumber ( $newJob[0]["id"] );

            $logMessage = "Job has been added";
            $activityLog->addLog ( $logMessage, $newJob[0]["id"], "jobs", "jobs", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newJob );

            return responseWithStatusCode($response, $responseData, 201);
        }
    }
);

$app->post(
    '/service/door/jobs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);


        $newJobDoor = new JobsDoors();


        if ( intval($config["door_id"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "door is not valid");

            return responseWithStatusCode($response, $responseData, 400);
        }else{

            $newJob = $jobs->createJobByDoor( $config["door_id"] );

            $job_id = $newJob[0]["id"];
            $order_id = $newJob[0]["order_id"];

            $newJob[0]['job_number'] = $jobs->updateJobNumber ( $job_id );

            $doorConfig = array (
                "job_id" => $job_id,
                "order_id" => $order_id,
                "door_id" => $config["door_id"]
            );

            $newJobDoor->create($doorConfig);
            $responseData = array ("status" => "success", "data" => $newJob );

            $logMessage = "Job has been added";
            $activityLog->addLog ( $logMessage, $job_id, "jobs", "jobs", $this->token->decoded->userId );

            return responseWithStatusCode($response, $responseData, 201);
        }
    }
);


$app->put(
    '/service/jobs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);


        if ( strlen($config["start_date"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "start date cannot be empty");

            return responseWithStatusCode($response, $responseData, 400);
        }else{

            $activityChanges = $jobs->trackChanges( $args["id"], $config );

            $config["id"] = $args["id"];
            unset ( $config["job_number"] );
            $newJob = $jobs->update($config);

            foreach ($activityChanges as $logMessage) {
                $activityLog->addLog ( $logMessage, $args["id"], "jobs", "jobs", $this->token->decoded->userId );
            }

            $responseData = array ("status" => "success", "data" => $newJob );

            return responseWithStatusCode($response, $responseData, 200 );
        }
    }
);


$app->delete(
    '/service/jobs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobs = new Jobs();

        $responseCode = 200;
        $responseData = $jobs->delete($args['id']);

        if ( $responseData["status"] == 'success' ) {
            $logMessage = "Job has been updated";
            $activityLog->addLog ( $logMessage, $args["id"], "jobs", "jobs", $this->token->decoded->userId );

        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->get(
    '/services/jobs/techician',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobs = new Jobs();
        $allItems = $jobs->getAllTechinician();

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);
    }
);
