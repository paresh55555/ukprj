<?php
use SalesQuoter\Service\JobCustomers;
use SalesQuoter\Service\Jobs;
use SalesQuoter\Service\OrderCustomers;  
use SalesQuoter\Service\JobAddresses;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/jobs/{job_id}/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobCustomers = new JobCustomers();
        $jobs = new Jobs();

        $checkJobExists = $jobs->get( $args["job_id"] );

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else { 

            $allItems = $jobCustomers->getAll ( ["job_id" => $args["job_id"] ] );

            $responseData = array ("status" => "success", "data" => $allItems );

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/jobs/{job_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobCustomers = new JobCustomers();
        $jobs = new Jobs();

        $checkJobExists = $jobs->get( $args["job_id"] );

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else { 

            $allItems = $jobCustomers->getAll ( ["id" => $args["id"] ] );
            
            $responseData = array ("status" => "success", "data" => $allItems );

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->post(
    '/service/jobs/{job_id}/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobCustomers = new JobCustomers();
        $jobs = new Jobs();
        $jobAddress = new JobAddresses();
        $activityLog = new ActivityLog();

        $checkJobExists = $jobs->get( $args["job_id"] );

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( strlen($config["first_name"]) == '0' || strlen($config["last_name"]) == '0' || strlen($config["phone"]) == '0') {

            $responseData = array ("status" => "error", "msg" => "customer fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["job_id"] = $args["job_id"];

            $insertAddress = $jobAddress->insertJobAddress( $config );

            $config["customer_id"] = $insertAddress["customer_id"];
            $config["address_id"] = $insertAddress["address_id"];

            $newCustomer[] = $jobCustomers->create($config);

            $logMessage = "New customer has been added to job";
            $activityLog->addLog ( $logMessage, $config["job_id"], "customers", "jobs", $this->token->decoded->userId );


            $responseData = array ("status" => "success", "data" => $newCustomer );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/jobs/{job_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobCustomers = new JobCustomers();
        $jobs = new Jobs();
        $jobAddress = new JobAddresses();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkJobExists = $jobs->get( $args["job_id"] );

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( strlen($config["first_name"]) == '0' || strlen($config["last_name"]) == '0' || strlen($config["phone"]) == '0') {

            $responseData = array ("status" => "error", "msg" => "customer fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];
            $config["job_id"] = $args["job_id"];
            
            $insertAddress = $jobAddress->insertJobAddress( $config );

            $config["customer_id"] = $insertAddress["customer_id"];
            $config["address_id"] = $insertAddress["address_id"];

            $updatedCustomer[] = $jobCustomers->update($config);

            $logMessage = "Updated customer has been added to job";
            $activityLog->addLog ( $logMessage, $config["job_id"], "customers", "jobs", $this->token->decoded->userId );
            
            $responseData = array ("status" => "success", "data" => $updatedCustomer);

            return responseWithStatusCode($response, $responseData, 200);
        }
    }
);


$app->delete(
    '/service/jobs/{job_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobCustomers = new JobCustomers();
        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $checkJobExists = $jobs->get( $args["job_id"] );

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $jobCustomers->delete($args['id']);

            if  ( $responseData["status"] == 'success' ) {

                $logMessage = "Jobs customer has been deleted";
                $activityLog->addLog ( $logMessage, $args["job_id"], "customers", "jobs", $this->token->decoded->userId );
                
            }

        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->post(
    '/service/contacts/search',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customer = new OrderCustomers();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $statusCode = 200;

        if ( strlen($config['search']) < '3' ) {

            $statusCode = 400;
            $responseData = array('status' => 'error', 'msg' => 'search string must be 3 character' );
        }else {
            $result = $customer->searchContacts( $config['search'] );
            $responseData = array('status' => 'success', 'data' => $result );
        }

        return responseWithStatusCode($response, $responseData, $statusCode);
    }
);