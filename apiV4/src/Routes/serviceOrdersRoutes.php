<?php
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/orders',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orders = new Orders();
        $allItems = $orders->getAllOrders ();

        $responseData = array ("status" => "success", "data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->post(
    '/service/orders',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orders = new Orders();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $newOrder = $orders->create($config);

        $logMessage = "Order has been added";
        $activityLog->addLog ( $logMessage, $newOrder[0]['id'], "order", "orders" );

        $responseData = array ("status" => "success", "data" => $newOrder);

        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/orders/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orders = new Orders();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkExists = $orders->get( $args["id"] );

        if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {
            $config["id"] = $args["id"];
            $updatedOrder = $orders->update($config);

            $orders->insertNotes( $config["notes"], $args["id"] );

            $activityChanges = $orders->trackChanges( $args["id"], $config );

            foreach ($activityChanges as $logMessage) {
                $activityLog->addLog ( $logMessage, $args["id"], "orders", "orders", $this->token->decoded->userId );
            }

            $responseData = array ("status" => "success", "data" => $updatedOrder);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->delete(
    '/service/orders/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orders = new Orders();
        $intVal = intval($args['id']);

        $checkExists = $orders->get( $args["id"] );

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $orders->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/service/orders/completed',
    function ($request, $response, $args) {

//        if (false === $this->token->hasScope(["service"])) {
//            return invalidPermissionsResponse($response);
//        }

        $orders = new Orders();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $newOrder = $orders->createFromCompleted($config);

        $responseData = array ("status" => "success", "data" => $newOrder);

        return responseWithStatusCode($response, $responseData, 201);

    }
);
