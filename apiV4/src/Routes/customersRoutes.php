<?php

use SalesQuoter\Customers\Customer;

$app->get(
    '/customers/{customer_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $customer = new Customer();
        $result = $customer->get($args['customer_id']);

        return responseWithStatusCode($response, $result, 200);
    }
);
