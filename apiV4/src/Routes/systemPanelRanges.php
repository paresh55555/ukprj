<?php

use SalesQuoter\PanelRanges\PanelRanges;

$app->get('/systems/{id}/panelRanges/width/{width}/height/{height}', function ($request, $response, $args) {

    if (false === $this->token->hasScope(["all"])) {
        return invalidPermissionsResponse($response);
    }

        $panelRanges = new PanelRanges();
 
        $width = (int) $args['width'];
        $height = (int) $args['height'];
        $systemId = $args['id'];

        $returnData = $panelRanges->getByHeightWidth($systemId, $width, $height);

        $responseData = array ( "status" => "success", "response_data" => $returnData );
        
        return responseWithStatusCode($response, $responseData, 200);
});
