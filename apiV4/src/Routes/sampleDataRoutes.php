<?php

use SalesQuoter\SampleData\SampleData;

$app->get(
    '/sampleData/{type}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $sampleData = new SampleData();

        $whereData['type'] = $args['type'];

        $isExists = $sampleData->checkTypeExists($whereData['type']);
        
        if ($isExists == false) {
            $responseData = array( 'status' => 'error' , 'msg' =>  'Type do not exists' );
        } else {
            $result = $sampleData->getAll($whereData);
            
            if (strlen($result[0]['json']) > '0') {

                $result[0]['json'] = jsonDecodeWithErrorChecking($result[0]['json']);
            }

            $responseData = array( 'status' => 'success' , 'data' =>  $result );
        }

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->post(
    '/sampleData/{type}',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $sampleData = new SampleData();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['type'] = $args['type'];
        $config['json'] = json_encode($config['json']);

        $isExists = $sampleData->checkTypeExists($config['type']);
        
        if (strlen($config['type']) == '0') {
            $responseData = array( 'status' => 'error' , 'msg' =>  'Empty Type' );
        } elseif ($isExists == false) {
            unset($config['id']);

            $result[] = $sampleData->create($config);

            if (strlen($result[0]['json']) > '0') {
                $result[0]['json'] = jsonDecodeWithErrorChecking($result[0]['json']);
            }

            $responseData = array( 'status' => 'success' , 'data' =>  $result );
        } else {
            $config['id'] = $isExists;
            $result[] = $sampleData->update($config);

            if (strlen($result[0]['json']) > '0') {
                $result[0]['json'] = jsonDecodeWithErrorChecking($result[0]['json']);
            }

            $responseData = array( 'status' => 'success' , 'data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->put(
    '/sampleData/{type}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $sampleData = new SampleData();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['type'] = $args['type'];
        $config['json'] = json_encode($config['json']);

        $isExists = $sampleData->checkTypeExists($config['type']);
        
        if (strlen($config['type']) == '0') {
            $responseData = array( 'status' => 'error' , 'msg' =>  'Empty Type' );
        } elseif ($isExists == false) {
            unset($config['id']);

            $result[] = $sampleData->create($config);

            if (strlen($result[0]['json']) > '0') {
                $result[0]['json'] = jsonDecodeWithErrorChecking($result[0]['json']);
            }

            $responseData = array( 'status' => 'success' , 'data' =>  $result );
        } else {
            $config['id'] = $isExists;
            $result[] = $sampleData->update($config);

            if (strlen($result[0]['json']) > '0') {
                $result[0]['json'] = jsonDecodeWithErrorChecking($result[0]['json']);
            }

            $responseData = array( 'status' => 'success' , 'data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, 200);
    }
);
