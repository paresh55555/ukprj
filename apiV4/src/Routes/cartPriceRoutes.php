<?php

use SalesQuoter\CartsPrice\CartsPrice;
use SalesQuoter\History\History;

$app->get(
    '/carts/{item_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cartsPrice = new CartsPrice();

        $where = array( "item_id" => $args['item_id'] );

        $allCart = $cartsPrice->allCarts( $where );

        $responseData = array( "status" => "success", "response_data" => $allCart );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/carts/{item_id}/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cartsPrice = new CartsPrice();

        $where = array( "item_id" => $args['item_id'], "id" => $args['id'] );
        
        $allCart = $cartsPrice->allCarts( $where );

        $responseData = array( "status" => "success", "response_data" => $allCart );

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->get(
    '/carts/{item_id}/{id}/history',
    function ($request, $response, $args) {

       $allGetVars = $request->getQueryParams();

       return renderHistoryItems( "cart", $args['id'], $allGetVars['length'], $this->token, $response );   
        
    }
);


$app->delete(
    '/carts/{item_id}/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cart = new CartsPrice();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteCart = $cart->delete($args['id']);

            $history = New History();
            $history->createLog("cart", $intVal, "DELETE", array(), $deleteCart);

            return responseWithStatusCode($response, $deleteCart, 200);
        }

    }
);




$app->post(
    '/carts/{item_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cartPrice = new CartsPrice();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        
        $config['item_id'] = $args['item_id']; 
        
        $requiredFields = array(
           "system_id",
           "quantity",
           "sub_total",
           "total",
           "component_values"
        ); 

        $requiredFieldsMissing = false;

        foreach ($requiredFields as $key) {
           
           if ( strlen($config[$key]) == '0' ) {

                $requiredFieldsMissing = true;
                $msg = $key." is a required field";
           }
        }

        if ( $requiredFieldsMissing ) {

            $responseData = array('status' => 'error', 'message' => $msg );
            $responseCode = 400;
            $pId = 0;
        }else {
            $responseCode = 201;
            $config['component_values'] = json_encode($config['component_values']);
            $result =  $cartPrice->create($config);
            $pId = $result[0]['id'];
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }

        $config['component_values'] = jsonDecodeWithErrorChecking( $config['component_values'] );

        $history = New History();
        $history->createLog("cart", $pId, "POST", $config, $responseData);

        return responseWithStatusCode($response, $responseData, $responseCode);

    }
);


$app->put(
    '/carts/{item_id}/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cartPrice = new CartsPrice();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        
        $config['item_id'] = $args['item_id']; 
        $config['id'] = $args['id']; 
        
        $requiredFields = array(
           "system_id",
           "quantity",
           "sub_total",
           "total",
           "component_values"
        ); 

        $requiredFieldsMissing = false;

        foreach ($requiredFields as $key) {
           
           if ( strlen($config[$key]) == '0' ) {

                $requiredFieldsMissing = true;
                $msg = $key." is a required field";
           }
        }

        if ( $requiredFieldsMissing ) {

            $responseData = array('status' => 'error', 'message' => $msg );
            $responseCode = 400;
        }else {
            $responseCode = 201;
            $config['component_values'] = json_encode($config['component_values']);
            $result =  $cartPrice->update($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }

        $config['component_values'] = jsonDecodeWithErrorChecking( $config['component_values'] );

        $history = New History(); 
        $history->createLog("cart", $args['id'], "PUT", $config, $responseData);

        return responseWithStatusCode($response, $responseData, $responseCode);

    }
);