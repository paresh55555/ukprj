<?php

/*
 * This file is part of the Slim API skeleton package
 *
 * Copyright (c) 2016 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   https://github.com/tuupola/slim-api-skeleton
 *
 */

use Ramsey\Uuid\Uuid;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use SalesQuoter\Users\User;
use SalesQuoter\Accounts\Accounts;

$app->post(
    "/token",
    function ($request, $response, $arguments) {

        return processUserLogin($request, $response, false);
    }
);


$app->post(
    "/token/login",
    function ($request, $response, $arguments) {

        return processUserLogin($request, $response, true);
    }
);

$app->post(
    "/token/reset",
    function ($request, $response, $arguments) {

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $user = new User();

        $checkEmail = $user->isEmailAvailable($config['email']);

        if ( strlen($config['email']) == '0' ) {

            $responseData = array('status' => 'error', 'msg' =>  'Email cannot be empty' );

            return responseWithStatusCode($response, $responseData, 400);

        }else if ( $checkEmail['availableEmail'] ) {

            $responseData = array('status' => 'error', 'response_data' => array(), 'msg' =>  'email not found' );

            return responseWithStatusCode($response, $responseData, 401);
        } else {

            $responseCode = 200;
            $result =  $user->sendResetEmail($config, $tableName);
            $responseData = array('status' => 'success', 'msg' => $result, 'response_data' => array() );

            return responseWithStatusCode($response, $responseData, $responseCode);
        }
    }
);

$app->post(
    '/token/pws',
    function ($request, $response, $args) {

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $user = new User();

        if ( strlen($config['unique_code']) == '0' ) {

            $responseData = array('status' => 'error', 'msg' =>  'code cannot be empty' );
    
            return responseWithStatusCode($response, $responseData, 400);

        } else {

            $responseCode = 200;
            $result =  $user->resetPassword($config);

            if ( $result['status'] == 'success' ) {
                $responseCode = 200;
            }else {
                $responseCode = 400;
            }

            return responseWithStatusCode($response, $result, $responseCode);
        }

    }
);


/**
 * processing User Login
 *
 * @param request  $request
 * @param response $response
 * @param accounts $accounts
 *
 * @return array
 */
function processUserLogin($request, $response, $accounts)
{

    $requestData = $request->getParsedBody();
    $user = new User();

    /*auth checked in db*/
    $permissionScope = $user->checkUserAuth($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']);
    if ($permissionScope === false) {
        $data['status'] =  'error';
        $data['message'] = 'Wrong Email or Password';

        return responseWithStatusCode($response, $data, 401);
    }

    if (isset($requestData['admin']) && ($requestData['admin'] == 'true' || $requestData['admin'] == true)) {
        $permissionScope['returnPermissions'][] = "admin";
    }

    $permissionScope['returnPermissions'][] = $permissionScope['userId'];


    $now = new DateTime();
    $future = new DateTime("now +5 days");
    $server = $request->getServerParams();

    $jti = Base62::encode(random_bytes(16));


    $payload = [
    "iat" => $now->getTimeStamp(),
    "exp" => $future->getTimeStamp(),
    "jti" => $jti,
    "sub" => $server["PHP_AUTH_USER"],
    "scope" => $permissionScope['returnPermissions'],
    "userId" => $permissionScope['userId'],
    ];


    $token = JWT::encode($payload, JWT_SECRET, "HS256");

    $data["status"] = "ok";
    $data["token"] = $token;
    $data["type"] = $user->getUserType($permissionScope['userId']);
    $data["id"] = $permissionScope['userId'];
    $data["permissions"] = $permissionScope['permissionJson'];
    $data["userName"] = $permissionScope['userName'];

    if ($accounts) {
        $accounts = new Accounts();
        $data["account"] = $accounts->userAccountDetails($permissionScope['account_id']);
    }

    return responseWithStatusCode($response, $data, 201);
}

/* This is just for debugging, not usefull in real life. */
$app->get(
    "/dump",
    function ($request, $response, $arguments) {
        print_r($this->token);
    }
);

$app->post(
    "/dump",
    function ($request, $response, $arguments) {
        print_r($this->token);
    }
);
