<?php

use SalesQuoter\Traits\Traits;

$app->get(
    '/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $allTraits = $traits->getAll();

        $responseData = array('status' => 'success', 'response_data' => $allTraits );

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->get(
    '/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $singleTraits = $traits->get($args['id']);

            if (sizeof($singleTraits) > 0) {
                 $responseCode = 200;
                 $responseData = array('status' => 'success', 'response_data' => $singleTraits  );
            } else {
                 $responseCode = 404;
                 $responseData = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
            }
        }

       return responseWithStatusCode($response, $responseData, $responseCode);
    }
);





$app->delete(
    '/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $deleteTraits = $traits->delete($args['id']);
            $responseData = $deleteTraits;
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();
        $responseCode = 400;
        if (intval($config['component_id']) == '0') {
            $responseData = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
        } elseif (strlen($config['name']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $traits->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->put(
    '/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();
        $singleTraits = $traits->get($config['id']);

        if (sizeof($singleTraits) > 0) {
            if (intval($config['component_id']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $traits->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


       return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
