<?php


$app->post(
    '/sendEmail',
    function ($request, $response, $args) {


        $allUploadedFiles = $request->getUploadedFiles();

        $toEmail = $request->getParam('to');
        $subject = $request->getParam('subject');
        $message = $request->getParam('message');

        $fileName = [];
        $files = [];

        foreach ( $allUploadedFiles["file"] as $uploadedFiles ) {

            $fileName[] = $uploadedFiles->getClientFilename();
            $file[] = $uploadedFiles->file ;
        }

        $isMailSent = sendApiEmail('', $toEmail, $subject, $message, $fileName, $file);

        if ( $isMailSent ) {

            $statusCode = 200;
            $responseData = array('status' => 'success', 'msg' => 'email successfully sent' );
        }else {
            $statusCode = 400;
            $responseData = array('status' => 'error', 'msg' => 'something went wrong in sending email' );
        }

       

        return responseWithStatusCode($response, $responseData, $statusCode);
    }
);