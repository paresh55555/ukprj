<?php

use SalesQuoter\Systems\Systems;
use SalesQuoter\Components\Components;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Cache\Cache;
use SalesQuoter\Options\Options;
use SalesQuoter\Traits\OptionTraits;
use SalesQuoter\Version\Version;

$app->get(
    '/systems',
    function ($request, $response, $args) {

  
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getWhere(array("type" => "system"));

        for ($count = 0; $count < sizeof($allComponents); $count++) {
             $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
             $allComponents[$count]['systems'] = $components->getWhere(array("system_id" => $allComponents[$count]['system_id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }

        $responseData = array('status' => 'success', 'response_data' =>  $allComponents );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/systems/{id}',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $traits = new Traits();

        $system = $components->getWhere(array("type" => "system", "id" => $args['id']));

        if (!empty($system)) {
            $system[0]['traits'] = $traits->getWhere(array("component_id" => $system[0]['id'] ));
        } else {
            http_response_code(404);
            $responseData = array('status' => 'error', 'message' =>  'System Not Exists');

            return responseWithStatusCode($response, $responseData, 200);
        }

        $responseData = array('status' => 'success', 'response_data' =>  $system );

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->get(
    '/systems/{id}/sampleDoorParts',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $traits = new Traits();

        $component = $components->getWhere(array("type" => "SampleDoorParts", "system_id" => $args['id']));

        if (!empty($component)) {
            $component[0]['traits'] = $traits->getWhere(array("component_id" => $component[0]['id'] ));
        } else {
            $responseData = array('status' => 'error', 'message' =>  'System Not Exists');

            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseData = array('status' => 'success', 'response_data' =>  $component );

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->get(
    '/systems/{id}/sampleDoorPrice',
    function ($request, $response, $args) {
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
        $components = new Components();
        $traits = new Traits();
        $component = $components->getWhere(array("type" => "SampleDoorPrice", "system_id" => $args['id']));
        if (!empty($component)) {
            $component[0]['traits'] = $traits->getWhere(array("component_id" => $component[0]['id'] ));
        } else {
            $responseData = array('status' => 'error', 'message' =>  'System Not Exists');

            return responseWithStatusCode($response, $responseData, 404);
        }
        $responseData = array('status' => 'success', 'response_data' =>  $component );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->post(
    '/systems/door',
    function ($request, $response, $args) {
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $doorData = array();
        $components = new Components();
        $traits = new Traits();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkData['id'] = $doorData['grouped_under'] = $config['grouped_under'];
        $checkData['name'] =  "PreMadeDoorSystem";
        $checkData['type'] = "systemGroup";

        $systemGroupExists = $components->getWhere($checkData);

        if (sizeof($systemGroupExists) == '0') {
             $code = 400;
             $responseData = array('status' => 'error', 'message' => 'System Group Not Exists' );
        } else {
            $doorData['type'] = "system";
            $doorData['menu_id'] = 0;
            $doorData['name'] = "Door System";

            $result = $componentsData =  $components->create($doorData);
            $systemID = $result[0]['id'];
             
            $newConfig = array(
                'id' => $systemID,
                'system_id' => $systemID,
            );

            $components->update($newConfig);

            $components->create([
                'type' => 'SampleDoorParts',
                'name' => 'SampleDoorParts',
                'system_id' => $systemID,
            ]);

            $components->create([
                'type' => 'SampleDoorPrice',
                'name' => 'SampleDoorPrice',
                'system_id' => $systemID,
            ]);
       
            $traits->create([
                'component_id' => $systemID,
                'name' => 'imgUrl',
                'type' => '',
                'value' => 'tmp',
                'visible' => 1,
                'active' => 1,
            ]);

            $traits->create([
                'component_id' => $systemID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Door System',
                'visible' => 1,
                'active' => 1,
            ]);

            $result = $components->create([
                'type' => 'menu',
                'name' => 'Menu',
                'system_id' => $systemID,
                'menu_id' => 0,
                'active' => 1,
                'grouped_under' => 0,
            ]);

            $menuID = $result[0]['id'];

            $traits->create([
                'component_id' => $menuID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Size',
                'visible' => 1,
                'active' => 1,
            ]);

            $result = $components->create([
                'type' => 'Dimensions',
                'name' => 'Dimensions',
                'system_id' => $systemID,
                'grouped_under' => $menuID,
                'menu_id' => 0,
                'active' => 1,
            ]);

            $dimensionID = $result[0]['id'];

            $traits->create([
                'component_id' => $dimensionID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Dimensions',
                'visible' => 1,
                'active' => 1,
            ]);

            $result = $components->create([
                'type' => 'NextTab',
                'name' => 'NextTab',
                'system_id' => $systemID,
                'grouped_under' => $menuID,
                'menu_id' => 0,
                'active' => 1,
            ]);

            $nextTabID = $result[0]['id'];
            
            $traits->create([
                'component_id' => $nextTabID,
                'name' => 'imgUrl',
                'type' => '',
                'value' => 'SQ-admin/img/circle-arrow.png',
                'visible' => 1,
                'active' => 1,
            ]);

            $code = 200;
            $responseData = array ( "status" => "success", "response_data" => $componentsData );
        }
        
        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->post(
    '/systems/window',
    function ($request, $response, $args) {
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $windowData = array();
        $components = new Components();
        $traits = new Traits();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkData['id'] = $windowData['grouped_under'] = $config['grouped_under'];
        $checkData['name'] =  "PreMadeWindowSystem";
        $checkData['type'] = "systemGroup";

        $systemGroupExists = $components->getWhere($checkData);

        if (sizeof($systemGroupExists) == '0') {
             $code = 400;
             $responseData = array('status' => 'error', 'message' => 'System Group Not Exists' );
        } else {
            $windowData['type'] = "system";
            $windowData['menu_id'] = 0;
            $windowData['name'] = "Window System";

            $result = $componentsData =  $components->create($windowData);
            $systemID = $result[0]['id'];
             
            $newConfig = array(
                'id' => $systemID,
                'system_id' => $systemID,
            );

            $components->update($newConfig);

            $components->create([
                'type' => 'SampleDoorParts',
                'name' => 'SampleDoorParts',
                'system_id' => $systemID,
            ]);

            $components->create([
                'type' => 'SampleDoorPrice',
                'name' => 'SampleDoorPrice',
                'system_id' => $systemID,
            ]);
       
            $components->create([
                'type' => 'SampleDoorParts',
                'name' => 'SampleDoorParts',
                'system_id' => $systemID,
            ]);

            $components->create([
                'type' => 'SampleDoorPrice',
                'name' => 'SampleDoorPrice',
                'system_id' => $systemID,
            ]);
            
            $traits->create([
                'component_id' => $systemID,
                'name' => 'imgUrl',
                'type' => '',
                'value' => 'tmp',
                'visible' => 1,
                'active' => 1,
            ]);

            $traits->create([
                'component_id' => $systemID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Window System',
                'visible' => 1,
                'active' => 1,
            ]);

            $result = $components->create([
                'type' => 'menu',
                'name' => 'Menu',
                'system_id' => $systemID,
                'menu_id' => 0,
                'active' => 1,
                'grouped_under' => 0,
            ]);

            $menuID = $result[0]['id'];

            $traits->create([
                'component_id' => $menuID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Size',
                'visible' => 1,
                'active' => 1,
            ]);

            $result = $components->create([
                'type' => 'Dimensions',
                'name' => 'Dimensions',
                'system_id' => $systemID,
                'grouped_under' => $menuID,
                'menu_id' => 0,
                'active' => 1,
            ]);

            $dimensionID = $result[0]['id'];

            $traits->create([
                'component_id' => $dimensionID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Dimensions',
                'visible' => 1,
                'active' => 1,
            ]);

            //
            $result = $components->create([
                'type' => 'Windows',
                'name' => 'Windows',
                'system_id' => $systemID,
                'grouped_under' => $menuID,
                'menu_id' => 0,
                'active' => 1,
            ]);

            $windowID = $result[0]['id'];

            $traits->create([
                'component_id' => $windowID,
                'name' => 'title',
                'type' => 'component',
                'value' => 'Choose Windows To Include',
                'visible' => 1,
                'active' => 1,
            ]);

            $result = $components->create([
                'type' => 'NextTab',
                'name' => 'NextTab',
                'system_id' => $systemID,
                'grouped_under' => $menuID,
                'menu_id' => 0,
                'active' => 1,
            ]);

            $nextTabID = $result[0]['id'];
            
            $traits->create([
                'component_id' => $nextTabID,
                'name' => 'imgUrl',
                'type' => '',
                'value' => 'SQ-admin/img/circle-arrow.png',
                'visible' => 1,
                'active' => 1,
            ]);

            $code = 200;
            $responseData = array ( "status" => "success", "response_data" => $componentsData );
        }
        
        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->post(
    '/systems',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $checkData = array();
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config["type"] = "system";
        
        if (isset($config["name"]) && strlen($config["name"]) > '0') {
            if ($config["name"] == 'Door System' || $config["name"] == 'Window System') {
            } else {
                $config["name"] = 'Door System';
            }
        } else {
            $config["name"] = 'Door System';
        }
        

        $checkData['id'] = $config['grouped_under'];
        $checkData['type'] = "systemGroup";

        $components = new Components();

        $systemGroup = $components->getWhere($checkData);
    
        if (sizeof($systemGroup) == '0') {
            $code = 404;
            $responseData = array('status' => 'error', 'message' => 'System Group Not Exists' );
        } else {
            if (intval($config['grouped_under']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'System Group ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $result = $components->create($config);
                $systemID = $result[0]['id'];
                $components->create([
                    'type' => 'SampleDoorParts',
                    'name' => 'SampleDoorParts',
                    'system_id' => $systemID,
                ]);
                $components->create([
                    'type' => 'SampleDoorPrice',
                    'name' => 'SampleDoorPrice',
                    'system_id' => $systemID,
                ]);
                $newConfig = array(
                    'id' => $systemID,
                    'system_id' => $systemID,
                );

                $code = 200; 
                $result = $components->update($newConfig);
                $responseData = array('status' => 'success', 'response_data' => $result);
            }
        }

    
        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->put(
    '/systems/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $checkData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkData['system_id'] =  $config['system_id'];
        $checkData['type'] = "system";
        $checkData['id'] = $config['id'] =  $args['id'];

        $components = new Components();
        $singleComponents = $components->getWhere($checkData);

        if (sizeof($singleComponents) > 0) {
            if (intval($config['system_id']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
            } else {
                $code = 200;
                $result = $components->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $code = 400;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->post(
    '/systems/{id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkData = array(
            'id' => $args['id'],
        );

        $traits = new Traits();
        $components = new Components();

        $singleComponent = $components->getWhere($checkData);

        if (sizeof($singleComponent) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'Component does not exist' );

            return responseWithStatusCode($response, $responseData, 400);
        }

        $resultTraits = [];
        foreach ($config as $trait) {
            $trait['component_id'] = $args['id'];
            if (intval($trait['component_id']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer');
            } elseif (strlen($trait['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'name can not be Empty');
            } else {
                $code = 200;
                if (isset($trait['id'])) {
                    $result = $traits->update($trait);
                } else {
                    $result = $traits->create($trait);
                }

                array_push($resultTraits, $result[0]);
            }
        }


        $version = new Version();
        $version->updateVersion("adminSystem", $args['id']);

        $responseData = array('status' => 'success', 'response_data' => $resultTraits);

        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->put(
    '/systems/{id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["id"] = $args["id"];

        $traits = new Traits();
        $components = new Components();

        $checkData = array("type" => "system", "id" => $args["id"] );

        $checkExists = $components->getWhere($checkData);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id , component_id and trait_id  combination is not correct' );

            return responseWithStatusCode($response, $responseData, 400);
        }

        $singleTraits = $traits->get($config['id']);

        if (sizeof($singleTraits) > 0) {
            if (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $code = 200;
                $result = $traits->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $code = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        $version = new Version();
        $version->updateVersion("adminSystem", $args['id']);

        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->delete(
    '/systems/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $code = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $code = 200;
            $deleteSystem = $components->delete($args['id']);
            $responseData = $deleteSystem;
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->post(
    '/systems/publish',
    function ($request, $response, $args) {
   
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $datas = $request->getBody();
        $config = jsonDecodeWithErrorChecking($datas);
    
        $components = new Components();
        $traits = new Traits();
        $options = new Options();
        $optionTraits = new OptionTraits();
        $cache = new Cache();


        $data["name"] = $config["name"];
        $data["type"] = "systemUX";
        $data["system_id"] = $config['system_id'];
        $data["current"] = true;
        $data["cachedArray"] = serialize(get_by_components($config["system_id"], $components, $traits, $options, $optionTraits));
     

        $action =  $config["action"];
        $cache->updateAllFalse("systemUX", $data["system_id"]);
        $result =  $cache->create($data);

        $cache->updateAllFalse("systemPrice", $data["system_id"]);
        $data["type"] = "systemPrice";
        $data["cachedArray"] =   serialize(get_components_price($config["system_id"], $components));
        $result =  $cache->create($data);

        $version = new Version();
        $version->updateVersion("frontSystem", $data["system_id"]);

        $responseData = array('status' => 'success' );

        return responseWithStatusCode($response, $responseData, 201);
    }
);




$app->get(
    '/systems/{id}/ux',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $cache = new Cache();
        $params = array( "type" => "systemUX" ,  "system_id" => $args["id"], "current" => true );
        $allSystems = $cache->getWhere($params)[0]['cachedArray'];

        $allSystems = unserialize($allSystems);

        $responseData = array('status' => 'success', 'response_data' => $allSystems );

        return responseWithStatusCode($response, $responseData, 201);
    }
);




$app->get(
    '/systems/{id}/price',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cache = new Cache();
        $params = array( "type" => "systemPrice" , "system_id" => $args["id"] , "current" => true );
        $allSystems = $cache->getWhere($params)[0]['cachedArray'];

        $allSystems = unserialize($allSystems);

        $responseData = array('status' => 'success', 'response_data' => $allSystems );

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->post(
    '/systems/{id}/images',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $data = $request->getParsedBody();

        $base64String = $data['file'];
        $systemId = intval($args['id']);
        $error = 0;

        if (empty($systemId) || $systemId == '0') {
            http_response_code(400);
            $responseData = array('status' => 'error', 'message' => 'Empty system ID');

            return responseWithStatusCode($response, $responseData, 400);
        } elseif (strlen($base64String) < 5) {
            http_response_code(400);
            $responseData = array('status' => 'error', 'message' => 'Empty Image ');

            return responseWithStatusCode($response, $responseData, 400);
        }

        $uploadPath = 'images/systems/'.$systemId.'/';

        if (!is_dir($uploadPath)) {
            $folderCreated =  mkdir($uploadPath);

            if (!$folderCreated) {
                $msg = " Folder is not Created. Pls check File Permissions ";
            }
        }

        $fileName = 'systems_'.$systemId.'.jpg';
        $outputFile = $uploadPath.$fileName;

        $ifp = fopen($outputFile, "wb");

        $data = explode(',', $base64String);

        $writeFile = fwrite($ifp, base64_decode($data[1]));
        if ($writeFile === false) {
             $error = 1;
             $msg = " File is written in desired folder. Pls check File Permissions ";
        }

        fclose($ifp);

        if ($error == '1') {
            $responseData = array(
                'status' => 'error',
                'msg' => $msg,
            );

            return responseWithStatusCode($response, $responseData, 400);

        } else {
            $responseData = array(
                'status' => 'success',
                'imgUrl' => "api/V4/".$outputFile,
            );

            return responseWithStatusCode($response, $responseData, 200);
        }
    }
);
