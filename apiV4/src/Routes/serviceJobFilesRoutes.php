<?php
use SalesQuoter\Service\JobFiles;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/jobs/{job_id}/file',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $files = new JobFiles();
        $allData = $files->getAll ( ["job_id" => $args["job_id"] ] );

        $responseData = [ "status" => "success", "data" => $allData ];


        return responseWithStatusCode($response, $responseData, 200 );

    }
);

$app->post(
    '/service/jobs/{job_id}/file',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $files = new JobFiles();
        $activityLog = new ActivityLog();

        $uploadedFiles = $request->getUploadedFiles();

        $extension = 'other';
        
        if ( sizeof ( $uploadedFiles ) > 0 ) {

            $extension = $files->getFileType ( $uploadedFiles["file"]->getClientFilename() );
        }
        

        $responseCode = 400;

        if  ( $extension == 'other' ) {

        	$responseData = ["status" => "error", "msg" => "Please upload valid image or video"];

        }else if ( sizeof ( $uploadedFiles ) > 0 ) {

        	$fileName = "salesquoter/jobs/".$args['job_id']."/".$extension."/".str_replace(" ", "-", $uploadedFiles["file"]->getClientFilename()); 
        	$responseData = $files->uploadFileToS3( $fileName, $uploadedFiles["file"]->file );

        	if ( $responseData["status"] == 'success' ) {

                if ( $extension == 'video' ) {

                    $posterImage = $files->getPosterImageFromVideo ( $uploadedFiles["file"]->file, $args['job_id'] ); 
                }else {
                    $posterImage = '';
                }

        		$responseCode = 200;
        		$newItem = [ 
        			"job_id" => $args['job_id'], 
        			"url" => $responseData["filePath"], 
        			"file_name" =>  $uploadedFiles["file"]->getClientFilename(),
        			"file_type" => $extension,
                    "poster_image" => $posterImage
        		];

                $logMessage = "<a href='".$responseData["filePath"]."' target='_blank'>".$extension."</a> has been uploaded" ;
                $activityLog->addLog ( $logMessage, $args['job_id'], $extension, "jobs", $this->token->decoded->userId );

        		$responseData = ["status" => "success", "data" => $files->create ( $newItem )];
        	}

        }else {

        	$responseData = ["status" => "error", "msg" => "No Files added to upload"];
        }

        return responseWithStatusCode($response, $responseData, $responseCode );

    }
);


$app->delete(
    '/service/jobs/{job_id}/file/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $files = new JobFiles();
        $activityLog = new ActivityLog();

        $intVal = intval($args['id']);

        $checkExists = $files->get( $args["id"] );

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else {
            $responseCode = 200;
            $responseData = $files->delete($args['id']);

            if ( $responseData["status"] == 'success' ) {

                $logMessage = "<a href='".$checkExists["url"]."' target='_blank'>".$checkExists["file_type"]."</a> has been deleted" ;
                $activityLog->addLog ( $logMessage, $args['job_id'], $checkExists["file_type"], "jobs", $this->token->decoded->userId );
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode );

    }
);