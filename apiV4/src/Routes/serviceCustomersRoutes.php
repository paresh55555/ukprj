<?php
use SalesQuoter\Service\Customers;
use SalesQuoter\Service\JobAddresses;

$app->get(
    '/service/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $allItems = $customers->getAll ();

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->get(
    '/service/customers/{id}/orders',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $allItems = $customers->getCustomersOrders ( $args["id"] );

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->get(
    '/service/customers/{id}/jobs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $allItems = $customers->getCustomersJobs ( $args["id"] );

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->put(
    '/service/address/{address_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if (  intval($args["address_id"]) == '0' && (strlen($config["address_1"]) == '0' || strlen($config["city"]) == '0')) {

            $responseData = array ("status" => "error", "msg" => "address fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["address_id"];

            $allItems = $customers->updateAddress ( $config );

            $responseData = array ("status" => "success", "data" => $allItems );
        }


        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->post(
    '/service/customers/{id}/address',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();
        $JobAddresses = new JobAddresses();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if (  strlen($config["address_1"]) == '0' || strlen($config["city"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "address fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["customer_id"] = $args["id"];
            $config["address_id"] = 0;

            $allItems = $customers->insertAddress ( $config );
            $details = $customers->getAddressDetails ( $allItems["address_id"] );

            $responseData = array ("status" => "success", "data" => $details );
        }


        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->put(
    '/service/customers/{id}/address/{address_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();
        $JobAddresses = new JobAddresses();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if (  strlen($config["address_1"]) == '0' || strlen($config["city"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "address fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["customer_id"] = $args["id"];
            $config["address_id"] = $args["address_id"];

            $allItems = $customers->insertAddress ( $config );

            $details = $customers->getAddressDetails ( $args["address_id"] );

            $responseData = array ("status" => "success", "data" => $details );
        }


        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->delete(
    '/service/customers/{id}/address/{address_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();
        $responseData = $customers->deleteAddress($args['address_id']);

        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->post(
    '/service/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( strlen($config["email"]) == '0' || strlen($config["first_name"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "customers fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $allItems = $customers->createCustomer ( $config );

            $responseData = array ("status" => "success", "data" => $allItems );
        }


        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->put(
    '/service/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( strlen($config["email"]) == '0' || strlen($config["first_name"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "customers fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];

            $allItems = $customers->updateCustomer ( $config );

            $responseData = array ("status" => "success", "data" => $allItems );
        }


        return responseWithStatusCode($response, $responseData, 200);

    }
);



$app->get(
    '/service/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customers = new Customers();

        $allItems = $customers->getAllCustomers ( $args["id"] );

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);