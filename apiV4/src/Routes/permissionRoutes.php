<?php

use SalesQuoter\Permissions\Permissions;

$app->get(
    '/permissions',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $permissions = new Permissions();
        $allPermissions = $permissions->getAll();

        $responseData = array('status' => 'success', 'response_data' => $allPermissions );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/permissions/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $permissions = new Permissions();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $singlePermissions = $permissions->get($args['id']);
 
            if (sizeof($singlePermissions) > 0) { 
                $responseCode = 200;
                $responseData = array('status' => 'success', 'response_data' => $singlePermissions  );
            } else {
                $responseCode = 404;
                $responseData = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found' );
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->delete(
    '/permissions/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $permissions = new Permissions();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $permissions->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->post(
    '/permissions',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $permissions = new Permissions();

        if (strlen($config['permissions']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'permissions can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $permissions->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);





$app->put(
    '/permissions',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $permissions = new Permissions();
        $singlePermissions = $permissions->get($config['id']);

        if (sizeof($singlePermissions) > 0) {
            if (strlen($config['permissions']) == '0') {
                http_response_code(400);
                $responseData = array('status' => 'error', 'message' =>  'permissions can not be Empty' );
            } else {
                $result = $permissions->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  

        return responseWithStatusCode($response, $responseData, 200);
    }
);
