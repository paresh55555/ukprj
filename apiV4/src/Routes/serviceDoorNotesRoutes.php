<?php
use SalesQuoter\Service\DoorNotes;
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\ActivityLog;

$app->post(
    '/service/orders/{order_id}/doors/{door_id}/notes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new DoorNotes();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( strlen($config["note"]) == '0' ) {
            $responseData = [ "status" => "error", "msg" => "note can not be empty" ];
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["door_id"] = $args["door_id"];
            $newNotes = $notes->create($config);

            $logMessage = "New Door notes has been added";
            $activityLog->addLog ( $logMessage, $args['order_id'], "door_notes", "orders", $this->token->decoded->userId );


            $responseData = array ("status" => "success", "data" => $newNotes );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/orders/{order_id}/doors/{door_id}/notes/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new DoorNotes();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];
            $config["door_id"] = $args["door_id"];
             
            $updatedNote = $notes->update($config);

            $logMessage = "Door notes has been updated";
            $activityLog->addLog ( $logMessage, $args['order_id'], "door_notes", "orders", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $updatedNote );

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->delete(
    '/service/orders/{order_id}/doors/{door_id}/notes/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new DoorNotes();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $intVal = intval($args['id']);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $notes->delete($args['id']);

            if ( $responseData["status"] == 'success' ) {

                $logMessage = "Door notes has been deleted";
                $activityLog->addLog ( $logMessage, $args['order_id'], "door_notes", "orders", $this->token->decoded->userId );

            }

        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
