<?php
use SalesQuoter\Service\OrderAttachments;
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\DocsTypes;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/orders/{order_id}/docs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new OrderAttachments();
        $orders = new Orders();

        $checkExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {
            $allItems = $attachments->getAll ( ["order_id" => $args["order_id"] ] );

            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/orders/{order_id}/docs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new OrderAttachments();
        $orders = new Orders();

        $checkExists = $orders->get( $args["order_id"] );
        if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {
            $allItems = $attachments->getAll ( ["order_id" => $args["order_id"], "id" => $args["id"] ] );

            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);

        }
    }
);


$app->post(
    '/service/orders/{order_id}/docs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new OrderAttachments();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $data = $request->getParsedBody();
        $checkExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( $data["docType"] == '0' || $data["docType"] == '' ) {

            $responseData = array ("status" => "error", "msg" => "please give valid docType");

            return responseWithStatusCode($response, $responseData, 400);
        }

        $newInsertData = [ 
            "order_id" => $args["order_id"], 
            "url" => "", 
            "type" => $data["docType"],
            "file_name" => $data["fileName"]
        ];

        $newOrder = $attachments->create( $newInsertData );
        $responseData = $attachments->doFileUpload ( $data, $args["order_id"], $newOrder["id"] );

        if ( $responseData["status"] == 'error' ) {
            $statusCode = 400;
            $attachments->delete ( $newOrder["id"] );
        }else {
            $statusCode = 201;
            $responseData = $attachments->update ([ "id" => $newOrder["id"], "url" => $responseData["url"] ]);

            $logMessage = "<a target='_blank' href='".$responseData["url"]."'>Docs</a> has been uploaded";

            $activityLog->addLog ( $logMessage, $args['order_id'], "docs", "orders", $this->token->decoded->userId );
        }

        return responseWithStatusCode($response, $responseData, $statusCode);

    }
);


$app->put(
    '/service/orders/{order_id}/docs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new OrderAttachments();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $checkExists = $orders->get( $args["order_id"] );
        $data = $request->getParsedBody();

        if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( $data["docType"] == '0' || $data["docType"] == '' ) {

            $responseData = array ("status" => "error", "msg" => "please give valid type");

            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseData = $attachments->doFileUpload ( $data, $args["order_id"], $args["id"] );
        if ( $responseData["status"] == "success" ) {
            $statusCode = 200;
            $responseData = $attachments->update ([ "id" => $args["id"], "file_name" => $data["fileName"], "url" => $responseData["url"], "type" => $data["docType"] ]);
        
            $logMessage = "<a target='_blank' href='".$responseData["url"]."'>Docs</a> has been updated";

            $activityLog->addLog ( $logMessage, $args['order_id'], "docs", "orders", $this->token->decoded->userId );

        }else {
            $statusCode = 400;
        }

        return responseWithStatusCode($response, $responseData, $statusCode);

    }
);


$app->delete(
    '/service/orders/{order_id}/docs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new OrderAttachments();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $checkExists = $orders->get( $args["order_id"] );
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $attachments->delete($args['id']);

            if ( $responseData["status"] == 'success' ) {

                $logMessage = "<a target='_blank' href='".$checkExists["url"]."'>Docs</a> has been deleted";
                $activityLog->addLog ( $logMessage, $args['order_id'], "docs", "orders", $this->token->decoded->userId );
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);

$app->get(
    '/service/orders/docs_types',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new OrderAttachments();

        $allItems = $attachments->getAllTypes ();

        $responseData = array ("status" => "success", "data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->get(
    '/services/docTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $docsTypes = new DocsTypes();
        $allGetVars = $request->getQueryParams();

        if ( isset($allGetVars['category']) && strlen($allGetVars['category']) > '0') {
           $category = $allGetVars['category']; 
        }else {
           $responseData = array('status' => 'error', 'msg' => 'category cannot be empty');
        
           return responseWithStatusCode($response, $responseData, 400);
        }

        $allItems = $docsTypes->getWhere ( [ "category" => $category ] );

        $responseData = array ("status" => "success", "data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->delete(
    '/services/docTypes/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $docsTypes = new DocsTypes();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else {
            $responseCode = 200;
            $responseData = $docsTypes->delete($args['id']);

        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);

$app->post(
    '/services/docTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $docsTypes = new DocsTypes();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( strlen($config["type"]) == '0' ) {
            $statusCode = 400;
            $responseData = [ "status" => "error" , "msg" => "type cannot be empty" ];
        }elseif (strlen($config["category"]) == '0') {
            $statusCode = 400;
            $responseData = [ "status" => "error" , "msg" => "category cannot be empty" ];
        }else {

            $statusCode = 200; 
            $docsType = $docsTypes->create ( $config );

            $responseData = array ("status" => "success", "data" => $docsType );
        }

        return responseWithStatusCode($response, $responseData, $statusCode);
    }
);
