<?php


use SalesQuoter\Reports\ReportOrders;
use SalesQuoter\Reports\ReportLeads;
use SalesQuoter\Quotes\Quote;

$app->get(
    '/reports/delivered',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();
        $report = new ReportOrders();
        $results = $report->deliveredMinToMax($allGetVars['min'], $allGetVars['max']);

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Description: File Transfer');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename=data.csv');

        $output = fopen('php://output', 'w');

        fputcsv($output, array('Delivered', 'First Name', 'Last Name', 'Email', 'Phone'));

        foreach ($results as $person) {
            $person['firstName'] = html_entity_decode($person['firstName']);
            $person['lastName'] = html_entity_decode($person['lastName']);
            $person['phone'] = html_entity_decode($person['phone']);
            fputcsv($output, $person);
        }

        exit();
    }
);


$app->post(
    '/reports/email/delivered',
    function ($request, $response, $args)  {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $config = $request->getParsedBody();

        $report = new ReportOrders();
        $results = $report->deliveredMinToMax($config['min'], $config['max']);

        $output = fopen('php://output', 'w');

        fputcsv($output, array('Email', 'First Name', 'Last Name'));

        foreach ($results as $person) {
            $person['email'] = trim(html_entity_decode($person['email']));
            $person['firstName'] = trim(html_entity_decode($person['firstName']));
            $person['lastName'] = trim(html_entity_decode($person['lastName']));
            fputcsv($output, $person);
        }

        fclose($output);
    }
);


$app->post(
    '/reports/email/completed',
    function ($request, $response, $args)  {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $config = $request->getParsedBody();

        $report = new ReportOrders();

        $results = $report->completedMinToMax($config['min'], $config['max']);


        return responseWithStatusCode($response, $results, 200);
    }
);


$app->get(
    '/reports/zipcodes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();
        $report = new ReportLeads();
        $max = null;
        if (!empty($allGetVars['max'])) {
            $max = $allGetVars['max'];
        }

        $min = null;
        if (!empty($allGetVars['min'])) {
            $min = $allGetVars['min'];
        }

        $results = $report->zipcodeByDate($min, $max);

        header('Content-Type: text/csv; charset=utf-8', true);
        header('Content-Disposition: attachment; filename=data.csv');

        $output = fopen('php://output', 'w');

        fputcsv($output, array('quoted', 'email', 'zip'));

        foreach ($results as $person) {
            $person['quoted'] = html_entity_decode($person['quoted']);
            $person['email'] = html_entity_decode($person['email']);
            $person['zip'] = html_entity_decode($person['zip']);
            fputcsv($output, $person);
        }

        exit();
    }
);


$app->get(
    '/reports/leads',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $allGetVars = $request->getQueryParams();
        $report = new ReportLeads();


        $results = $report->leadsByDate($allGetVars);

        header('Content-Type: text/csv; charset=utf-8', true);
        header('Content-Disposition: attachment; filename=data.csv');

        $output = fopen('php://output', 'w');

        fputcsv($output, array("SalesPerson", "Quote Number", "name", "email", "zip", "leadType", "phone",  "quoted"));


        foreach ($results as $leads) {
            $leads['SalesPerson'] = html_entity_decode($leads['SalesPerson']);
            $leads['Quote Number'] = html_entity_decode($leads['quoteNumber']);
            $leads['name'] = html_entity_decode($leads['name']);
            $leads['email'] = html_entity_decode($leads['email']);
            $leads['zip'] = html_entity_decode($leads['zip']);
            $leads['leadType'] = html_entity_decode($leads['leadType']);
            $leads['phone'] = html_entity_decode($leads['phone']);
            $leads['quoted'] = html_entity_decode($leads['quoted']);

            fputcsv($output, $leads);
        }

        exit();
    }
);


$app->get(
    '/reports/orders/doorsSold',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $quote = new Quote();
        $allGetVars = $request->getQueryParams();
        $startDate = $todayDate = date("Y-m-d");


        if (!empty($allGetVars['startDate'])) {
            $startDate = $allGetVars['startDate'];
        }

        $endDate = null;

        if (!empty($allGetVars['endDate'])) {
            $endDate = $allGetVars['endDate'];
        }

        if (strlen($startDate) == '0' || strlen($endDate) == '0') {
            $startDate = date("Y-m-d");
            $date = strtotime($startDate.' -1 month');
            $endDate = date('Y-m-d', $date);
        }

        $allOrders = $quote->getOrderByDateRange($startDate, $endDate);
        $allSalesPersons = $quote->getSalesManByDateRange($startDate, $endDate);

        $totalDoor = 0;

        foreach ($allOrders as $order) {
            $totalDoor = $totalDoor + $quote->countQuantity($order['cart']);
        }

        $discountInfo = $quote->calculateDiscount($allOrders, $totalDoor);
        $breakDowns = $quote->calculateBreakdowns($allOrders, $allSalesPersons);

        $responseData = array("StartDate" => $startDate,
        "endDate" => $endDate,
        "NumberOfDoors" => $totalDoor,
        "AverageCostPerDoorBeforeDiscount" => "$".number_format($discountInfo['averageCostPerDoor'], 2),
        "AverageCostPerDoorAfterDiscount" => "$".number_format($discountInfo['averageCostPerDoorWithDiscount'], 2),
        "AverageCostPerDoorPerOrder" => "$".number_format($discountInfo['averageCostPerDoorAllTogether'], 2),
        "AverageDiscountPercent" => number_format($discountInfo['averageDiscountPerDoor'], 2)."%",
        "TotalCostOfDoors" => "$".number_format($discountInfo['totalDoors'], 2),
        "TotalDiscounts" => "$".number_format($discountInfo['totalDiscount'], 2),
        "TotalCostOfDoorsMinusDiscount" => "$".number_format($discountInfo['totalDoors'] - $discountInfo['totalDiscount'], 2),
        "TotalOrdersAmount" => "$".number_format($discountInfo['totalOrders'], 2),
        "TotalOrders" => count($allOrders),
        "DoorsPerOrder" => number_format($totalDoor / count($allOrders), 2),
        "breakDowns" => $breakDowns,
        );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/reports/sales',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $allGetVars = $request->getQueryParams();
        $quote = new Quote();

        $startDate = strtotime($startDate.' -1 month');


        if (strlen($allGetVars['startDate']) > '2') {
            $startDate = $allGetVars['startDate'];
        }

        $endDate = date("Y-m-d");

        if (strlen($allGetVars['endDate']) > '2') {
            $endDate = $allGetVars['endDate'];
        }

        $quotes = 'not_quote';

        if ($allGetVars['quote'] == '1') {
            $quotes = 'quote';
        }

        $order = 'not_order';

        if ($allGetVars['order'] == '1') {
            $order = 'order';
        }


        $result = $quote->getQuotesSales ( $startDate, $endDate, $quotes, $order ); 

        $responseData = array("status" => "success", 'data' => $result);  

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/reports/delivered/balance',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $quote = new Quote();

        $result = $quote->geDeliveredBalancedReport (); 

        $responseData = array("status" => "success", 'data' => $result);  

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/reports/orders/delivered',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();

        if (empty($allGetVars['min']) or empty($allGetVars['max']) ) {
            print_r("Failed");
            exit();
        }

        $quote = new Quote();

        $results = $quote->getDeliveredOrders($allGetVars['min'], $allGetVars['max']);


        if (empty ($allGetVars['csv']) or $allGetVars['csv'] == "false") {

            $responseData = array("status" => "success", 'data' => $results);
            return responseWithStatusCode($response, $responseData, 200);

        }


        header('Content-Type: text/csv; charset=utf-8', true);
        header('Content-Disposition: attachment; filename=DeliveredOrders.csv');

        $output = fopen('php://output', 'w');

        fputcsv($output, array('Job Number', 'Customer Name', 'Delivered Date', 'Total'));

        $newOrder = array();
        foreach ($results as $order) {
            $newOrder['Job Number'] = html_entity_decode($order['Job Number']);
            $newOrder['Customer Name'] = html_entity_decode($order['Customer Name']);
            $newOrder['Delivered Date'] = html_entity_decode($order['Delivered Date']);
            $newOrder['Total'] = html_entity_decode($order['Total']);
            fputcsv($output, $newOrder);
            $newOrder =array();
        }

        exit();

    }
);

//Special Type of Delivered Orders for Sharon
$app->get(
    '/reports/orders/special/delivered',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $quote = new Quote();

        $result = $quote->geDeliveredOrdersdReport ();

        $responseData = array("status" => "success", 'data' => $result);

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->get(
    '/reports/quotes/yearOld',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $quote = new Quote();

        $results = $quote->yearOldQuotes();



        header('Content-Type: text/csv; charset=utf-8', true);
        header('Content-Disposition: attachment; filename=data.csv');

        $output = fopen('php://output', 'w');

        fputcsv($output, array("QuoteNumber", "DateQuoted", "CustomerLastName", "CustomerFirstName", "CustomerCompany",
            "CustomerEmail", "BillingName",  "BillingEmail",  "ShippingName",  "ShippingEmail"));

        $result = array();
        foreach ($results as $quote) {

            $result['QuoteNumber'] = html_entity_decode($quote['QuoteNumber']);
            $result['DateQuoted'] = html_entity_decode($quote['quoted']);
            $result['CustomerLastName'] = html_entity_decode($quote['CustomerLastName']);
            $result['CustomerFirstName'] = html_entity_decode($quote['CustomerFirstName']);
            $result['CustomerCompany'] = html_entity_decode($quote['CustomerCompany']);
            $result['CustomerEmail'] = html_entity_decode($quote['CustomerEmail']);
            $result['BillingName'] = html_entity_decode($quote['BillingName']);
            $result['BillingEmail'] = html_entity_decode($quote['BillingEmail']);
            $result['ShippingName'] = html_entity_decode($quote['ShippingName']);
            $result['ShippingEmail'] = html_entity_decode($quote['ShippingEmail']);
            fputcsv($output, $result);

            $result = array();
        }

        exit();
    }
);


$app->get(
    '/reports/accounting/fulldump',
    function ($request, $response, $args) {

        $allGetVars = $request->getQueryParams();

        $reports = new ReportOrders();

        $config = $reports->buildAccountingConfig( $allGetVars );
        $config['type'] = 'All';
        $config['userType'] = $user['type'];
        $config['export'] = $user['type'];

        $results = $reports->renderAccountingDetails ( $config );


        if (empty ($allGetVars['exportType']) or $allGetVars['exportType'] == "json") {

            $responseData = array("status" => "success", 'data' => $results);

            return responseWithStatusCode($response, $responseData, 200);

        }


        header('Content-Type: text/csv; charset=utf-8', true);
        header('Content-Disposition: attachment; filename=accountingReports.csv');

        $output = fopen('php://output', 'w');

        $header = [ 
            'Last Name',  
            'First Name', 
            'Company', 
            'Job total', 
            'Deposit', 
            'Final Payment 1',
            'Final Payment 2',
            'Balance due',
            'Paid in Full'
        ];
        
        fputcsv($output, $header);

        $newOrder = array();

        foreach ($results["data"] as $order) {

            $newOrder[] = html_entity_decode($order['lastName']);
            $newOrder[] = html_entity_decode($order['firstName']);
            $newOrder[] = html_entity_decode($order['companyName']);
            $newOrder[] = $order['total'];

            $paymentsInfo = $order['SalesDiscount']->Payments;

            if ( isset($paymentsInfo[0]) ) {

                $newOrder[] = "$".$paymentsInfo[0]["amount"]." ".$paymentsInfo[0]["date"]." ".$paymentsInfo[0]["kind"]." ".$paymentsInfo[0]["lastFour"];
            }else{
                $newOrder[] = '';
            }

            if ( isset($paymentsInfo[1]) ) {

                $newOrder[] = "$".$paymentsInfo[1]["amount"]." ".$paymentsInfo[1]["date"]." ".$paymentsInfo[1]["kind"]." ".$paymentsInfo[1]["lastFour"];
            }else{
                $newOrder[] = '';
            }


            if ( isset($paymentsInfo[2]) ) {

                $newOrder[] = "$".$paymentsInfo[2]["amount"]." ".$paymentsInfo[2]["date"]." ".$paymentsInfo[2]["kind"]." ".$paymentsInfo[2]["lastFour"];
            }else{
                $newOrder[] = '';
            }


            if  ( $order['balanceDue'] < 0 ) {
                $newOrder[] = '$0';
            }else {
                $newOrder[] = "$".$order['paidInFull'];
            }

            $newOrder[] = html_entity_decode($order['paidInFull']);

            fputcsv($output, $newOrder);

            $newOrder =array();
        }

        exit();

    }
);


$app->get(
    '/reports/openOrders',
    function ($request, $response, $args) {

        $allGetVars = $request->getQueryParams();

        $reports = new ReportOrders();

        $results = $reports->getOpenOrders ( $allGetVars["end_date"] );


        if (empty ($allGetVars['exportType']) or $allGetVars['exportType'] == "json") {

            $responseData = array("status" => "success", 'data' => $results);

            return responseWithStatusCode($response, $responseData, 200);

        }


        header('Content-Type: text/csv; charset=utf-8', true);
        header('Content-Disposition: attachment; filename=openOrderReports.csv');

        $output = fopen('php://output', 'w');

        $header = [ 
            'Job Number',  
            'extColor', 
            'intColor', 
            'alum', 
            'vinyl', 
            'flush',
            'upstep',
            'dueDate'
        ];
        
        fputcsv($output, $header);

        $newOrder = array();

        foreach ($results as $order) {

            $newOrder[] = html_entity_decode($order['jobNumber']);

            $newOrder[] = html_entity_decode($order['extColor']);
            $newOrder[] = html_entity_decode($order['intColor']);
            
            $newOrder[] = $order['alum'];
            $newOrder[] = $order['vinyl'];
            $newOrder[] = $order['flush'];
            $newOrder[] = $order['upstep'];
            $newOrder[] = $order['dueDate'];

            fputcsv($output, $newOrder);

            $newOrder =array();
        }

        exit();

    }
);