<?php

use SalesQuoter\Guest\Guest;

$app->post(
    '/guest',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $guestData = jsonDecodeWithErrorChecking($data);

        $guest = new Guest();

        $responseData = $guest->newGuest($guestData);

        return responseWithStatusCode($response, $responseData, 200);
    }
);
