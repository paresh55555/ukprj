<?php
use SalesQuoter\ListBuilder\Lists;

$app->get(
    '/listBuilder',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $lists = new Lists();
        $allLists = $lists->getLists();

        return responseWithStatusCode($response, $allLists, 200);
    }
);


$app->post(
    '/listBuilder',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        unset($config['name']);
        unset($config['id']);
        unset($config['data']);

        $lists = new Lists();
        $allLists = $lists->create($config);

        $responseData = array("status" => "success", 'data' => $allLists );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/listBuilder/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $lists = new Lists();
        $id = $args['id'];

        $allLists = $lists->getListsData($id);

        return responseWithStatusCode($response, $allLists, 200);
    }
);


$app->put(
    '/listBuilder/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $lists = new Lists();
        $id = $args['id'];

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $check = $lists-> checkValidJson($config);

        if ( strlen($config['name']) == '0' ) {

            $responseData = array("status" => "error", "msg" => 'Name cannot be empty' );
        }else if ($check) {

            $updateData = array();
            
            $updateData['id'] = $id;
            
            $updateData['name'] = $config['name'];
            $updateData['data'] = json_encode($config);

            $allLists = $lists->update($updateData);

            $responseData = array("status" => "success", "data" => $allLists );
        } else {
            $responseData = array("status" => "error", "msg" => 'Data array is not correct' );
        }


        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->delete(
    '/listBuilder/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $lists = new Lists();
        $deletedList = $lists->delete($args['id']);
            
        return responseWithStatusCode($response, $deletedList, 200);
    }
);



$app->get(
    '/listBuilder/available/quotes',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $list = new Lists();

        $allListsQuote = $list->getAvailableData("SQ_available_quotes");

        $responseData = array( "status" => "success" , "data" => $allListsQuote );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/listBuilder/available/orders',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $lists = new Lists();

        $allListsQuotes = $lists->getAvailableData("SQ_available_orders");

        $responseData = array( "status" => "success" , "data" => $allListsQuotes );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/listBuilder/available/sampleData',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $lists = new Lists();

        $allSampleData = $lists->formatSampleData();

        $responseData = array( "status" => "success" , "data" => $allSampleData );

        return responseWithStatusCode($response, $responseData, 200);
    }
);
