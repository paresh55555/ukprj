<?php

use SalesQuoter\CartLayout\LayoutItem;
use SalesQuoter\CartLayout\LayoutGroup;

$app->get(
    '/quoteBuilder/{layout_id}/layouts/{layout_group_id}/column/{column_id}/items',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutGroup = new LayoutGroup();
        $allLayout = $layoutGroup->allLayoutItem( $args['column_id'] );

        return responseWithStatusCode($response, $allLayout, 200);
    }
);


$app->delete(
    '/quoteBuilder/{layout_id}/layouts/{layout_group_id}/column/{column_id}/items/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutItem = new LayoutItem();
        $dataExists = $layoutItem-> checkItem ( $args['column_id'], $args['id'] );

        if ($dataExists == false) {
            $data = array('status' => 'error', 'message' => 'Layout Item id and Column id combination is not correct' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteItem = $layoutItem->delete($args['id']);
            
            return responseWithStatusCode($response, $deleteItem, 200);
        }
    }
);




$app->post(
    '/quoteBuilder/{layout_id}/layouts/{layout_group_id}/column/{column_id}/items',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutItem = new LayoutItem();
        $insertData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $insertData['component_type'] = $config['component_type'];
        $insertData['item'] = $config['item'];
        $insertData['label'] = $config['label'];
        $insertData['`order`'] = $layoutItem-> getInsertOrder ( $args['column_id'] ); 
        $insertData['layout_column_id'] = $args['column_id']; 
        

        if (strlen($insertData['component_type']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'component_type can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $layoutItem->create($insertData);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/quoteBuilder/{layout_id}/layouts/{layout_group_id}/column/{column_id}/items/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $updateData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $updateData['`order`'] = $config['order'];
        $updateData['component_type'] = $config['component_type'];
        $updateData['item'] = $config['item'];
        $updateData['label'] = $config['label'];
        $updateData['id'] = $args['id'];

        $layoutItem = new LayoutItem();
        $singleItem = $layoutItem->get($args['id']);

        $dataExists = $layoutItem-> checkItem ( $args['column_id'], $args['id'] );

        if (sizeof($singleItem) > 0) {
            if ($dataExists == false) {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'Layout Item id and Column id combination is not correct' );
            } else {
                $responseCode = 200;
                $result = $layoutItem->update($updateData);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
