<?php

use SalesQuoter\CartLayout\LayoutColumn;

$app->delete(
    '/quoteBuilder/{layout_id}/layouts/{layout_group_id}/column/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutColumn = new LayoutColumn();
        $dataExists = $layoutColumn-> checkCloumn ( $args['layout_group_id'], $args['id'] );

        if ($dataExists == false) {
            $data = array('status' => 'error', 'message' => 'Layout Group id and Column id combination is not correct' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteColumn = $layoutColumn->delete($args['id']);
            
            return responseWithStatusCode($response, $deleteColumn, 200);
        }
    }
);




$app->post(
    '/quoteBuilder/{layout_id}/layouts/{id}/column',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutColumn = new LayoutColumn();
        $insertData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $insertData['alignment'] = $config['alignment'];
        $insertData['`order`'] = $layoutColumn-> getInsertOrder ( $args['id'] ); 
        $insertData['layout_group_id'] = $args['id']; 
        

        if (strlen($insertData['alignment']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'alignment can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $layoutColumn->create($insertData);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/quoteBuilder/{layout_id}/layouts/{layout_group_id}/column/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $updateData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $updateData['`order`'] = $config['order'];
        $updateData['alignment'] = $config['alignment'];
        $updateData['id'] = $args['id'];

        $layoutColumn = new LayoutColumn();
        $singleColumn = $layoutColumn->get($args['id']);

        $dataExists = $layoutColumn-> checkCloumn ( $args['layout_group_id'], $args['id'] );

        if (sizeof($singleColumn) > 0) {
            if ($dataExists == false) {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'Layout Group id and Column id combination is not correct' );
            } else {
                $responseCode = 200;
                $result = $layoutColumn->update($updateData);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
