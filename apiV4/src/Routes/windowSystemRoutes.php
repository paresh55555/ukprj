<?php

use SalesQuoter\Windows\WindowSystem;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\CutSheet;
use SalesQuoter\Windows\Window;


$app->post(
    '/windows',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $window = new Window();
        $data = $window->getTypesOfWindowsBasedOnSize( );

        $responseData =  array('status' => 'success', 'data' => $data );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/windowSystem/cutSheet/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cutSheet = new CutSheet();
        $data = $cutSheet->getCutSheet($args['id']);

        header("Content-Type: application/json", false);
        echo($data['cutSheet']);
    }
);




$app->post(
    '/windowSystem/price',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $windowData = new WindowData($config);
        $windowSystem = new WindowSystem($windowData);
        $price = $windowSystem->getPrice();

        return responseWithStatusCode($response, $price, 200);
    }
);




$app->post(
    '/windowSystem/cutSheet',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $windowData = new WindowData($config);
        $windowSystem = new WindowSystem($windowData);
        $cutSheetParts = $windowSystem->getCutSheetParts();

        $cutSheet = new CutSheet();
        $id = $cutSheet->insertCutSheet($cutSheetParts);

        $responseData = array("cutSheetId" => $id);

        return responseWithStatusCode($response, $responseData, 201);
    }
);




$app->post(
    '/windowSystem',
    function ($request, $response, $args) {
        // Sample log message
        //    $this->logger->info("WindowSystemSelected");

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $windowData = new WindowData($config);

        $windowSystem = new WindowSystem($windowData);

        $cutSheetParts = $windowSystem->getCutSheetParts();
        
        return responseWithStatusCode($response, $cutSheetParts, 201);
    }
);
