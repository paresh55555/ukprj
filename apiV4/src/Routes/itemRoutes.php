<?php

use SalesQuoter\Items\Items;
use Ramsey\Uuid\Uuid;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use SalesQuoter\History\History;

$app->get(
    '/items',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["salesperson"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        
        $allGetVars = $request->getQueryParams();

        if ($allGetVars['state'] == 'quote' || $allGetVars['state'] == 'order' ) {
            $state = $allGetVars['state']; 
        }else {
            $responseData = array('status' => 'error', 'msg' => 'enter valid state');
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $requestHeaders = apache_request_headers(); 
        
        $decodedData = JWT::decode(str_replace("Bearer ", "", $requestHeaders['Authorization']), JWT_SECRET, ["HS256"]);


        $items = new Items();
        $allItems = $items->getAllItems($decodedData->userId, $state);

        $responseData = array("status" => "success", "response_data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/items/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["salesperson"])) {
            return invalidPermissionsResponse($response);
        }

        $items = new Items();
        $itemDetails = $items->getSingleItem($args['id']);

        $responseData = array("status" => "success", "response_data" => $itemDetails);

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/items/{id}/history',
    function ($request, $response, $args) {

       $allGetVars = $request->getQueryParams();

       return renderHistoryItems( "items", $args['id'], $allGetVars['length'], $this->token, $response );   
        
    }
);

$app->put(
    '/items/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $items = new Items();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ($config['state'] == 'quote' || $config['state'] == 'order' ) {
        }else {
            $responseData = array('status' => 'error', 'msg' => 'enter valid state');
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $config['id'] = $args['id'];
        $result = $items->update($config);

        $responseData = array("status" => "success", "response_data" => $result);

        $history = New History();
        $history->createLog("items", $args['id'], "PUT", $config, $responseData);

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->delete(
    '/items/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["salesperson"])) {
            return invalidPermissionsResponse($response);
        }

        $items = new Items();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteItem = $items->delete($args['id']);
            
            $history = New History();
            $history->createLog("items", $args['id'], "DELETE", array(), $deleteItem);

            return responseWithStatusCode($response, $deleteItem, 200);
        }
    }
);