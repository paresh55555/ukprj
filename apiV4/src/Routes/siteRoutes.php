<?php
/* siteRoutes added */
use SalesQuoter\Site\Defaults;


$app->get(
    '/site/defaults',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $defaults = new Defaults();
        $id = 1;
        $siteDefaults = $defaults->get($id);

        return responseWithStatusCode($response, $siteDefaults, 200);
    }
);


$app->put(
    '/site/defaults',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $defaults = new Defaults();
        $data = $request->getBody();

        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = 1;


        $siteDefaults = $defaults->update($config);
    
        return responseWithStatusCode($response, $siteDefaults, 200);
    }
);
