<?php

use SalesQuoter\Accounts\Accounts;

$app->get(
    '/accounts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $accounts = new Accounts();

        $result = $accounts->allAccounts();

        $responseData = array( 'status' => 'success' , 'data' =>  $result );

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->get(
    '/accounts/builders',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $accounts = new Accounts();

        $result = $accounts->renderAccountBuilder();

        $responseData = array( 'status' => 'success' , 'data' =>  $result );

        return responseWithStatusCode($response, $result, 200);
    }
);


$app->get(
    '/accounts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $accounts = new Accounts();

        $result = $accounts->accountDetails(intval($args['id']));

        $responseData = array( 'status' => 'success' , 'data' =>  $result );

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->post(
    '/accounts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $accounts = new Accounts();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $result = $accounts->createNewAccount($config);

        $responseData = array( 'status' => 'success' , 'data' =>  $result );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->put(
    '/accounts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $accounts = new Accounts();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $result = $accounts->updateAccount($args['id'], $config);

        $responseData = array( 'status' => 'success' , 'data' =>  $result );

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->delete(
    '/accounts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $accounts = new Accounts();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );

            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteAccount = $accounts->delete($args['id']);

            return responseWithStatusCode($response, $deleteAccount, 200);
        }
    }
);