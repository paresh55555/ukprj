<?php

use SalesQuoter\Addresses\Address;
use SalesQuoter\Quotes\Quote;
use SalesQuoter\Contracts\Contracts;

$app->get(
    '/quotes/{quote_id}/addresses',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $config = array ('quote_id' => $args['quote_id']);
        $address = new Address();
        $allAddress = $address->getAll($config);

        return responseWithStatusCode($response, $allAddress, 200);
    }
);




$app->put(
    '/quotes/{quote_id}/addresses',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if (json_last_error()) {
            echo json_last_error_msg();
        }
        $config['quote_id'] =  $args['quote_id'];

        $address = new Address();
        $checkEmptyParameter = $address->checkEmptyParameter($config);

        if ($checkEmptyParameter['isEmpty']) {
             $responseCode = 400;
             $responseData['status'] = 'error';
             $responseData['message'] =  $checkEmptyParameter['message'];
        } elseif (empty($config['sales_person_id']) ||  $config['sales_person_id'] == '0') {
             $responseCode = 400;
             $responseData['status'] = 'error';
             $responseData['message'] =  'sales_person_id must be Integer';
        } else {
            $responseCode = 200;
            $responseData = $address->update($config)[0];
            $responseData['status'] = 'success';

            if (!empty($allGetVars['addToQuote'])) {
                $quote = new Quote();
                $quoteConfig = array("id" => $args['quote_id'], "Customer" => $allGetVars['customer_id']);
                $quote->update($quoteConfig);
            }
        }
   

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->post(
    '/quotes/{quote_id}/addresses',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $responseData = array();
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        unset($config['id']);
        $config['quote_id'] =  $args['quote_id'];
        $address = new Address();

        $checkEmptyParameter = $address->checkEmptyParameter($config);

        if ($checkEmptyParameter['isEmpty']) {
             $responseCode = 400;
             $responseData['status'] = 'error';
             $responseData['message'] =  $checkEmptyParameter['message'];
        } elseif (empty($config['sales_person_id']) ||  $config['sales_person_id'] == '0') {
             $responseCode = 400;
             $responseData['status'] = 'error';
             $responseData['message'] =  'sales_person_id must be Integer';
        } else {
            $responseCode = 201;
            $responseData = $address->create($config);
            $responseData['status'] = 'success';
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/quotes/{quote_id}/contracts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $file = $request->getUploadedFiles();
        $config = array ('quote_id' => $args['quote_id']);

        if (empty($file['contract'])) {
            throw new Exception('Expected a contract file');
        }

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $config['contracts'] = file_get_contents($file['contract']->file);

        $contracts = new Contracts();
        $result = $contracts->create($config)[0];

        $responseData = array('status' => 'success', 'id' => $result['id']);


        return responseWithStatusCode($response, $responseData, 201);
    }
);




$app->post(
    '/quotes/{quote_id}/contracts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $file = $request->getUploadedFiles();
        $config = array (
            'id' => $args['id'],
            'quote_id' => $args['quote_id'],
        );

        if (empty($file['contract'])) {
            throw new Exception('Expected a contract file');
        }

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $config['contracts'] = file_get_contents($file['contract']->file);

        $contracts = new Contracts();
        $result = $contracts->update($config)[0];

        $responseData = array('status' => 'success', 'id' => $result['id']);

        return responseWithStatusCode($response, $responseData, 201);
    }
);





$app->get(
    '/quotes/{quote_id}/contracts',
    function ($request, $response, $args) {

        $allGetVars = $request->getQueryParams();

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $config = array ('quote_id' => $args['quote_id']);

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $contracts = new Contracts();
        $result = $contracts->getWhere($config);

        $responseData = array('status' => 'success', 'id' => $result[0]['id']);

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->delete(
    '/quotes/{quote_id}/contracts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $config = array (
            'quote_id' => $args['quote_id'],
        );

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $contracts = new Contracts();
        $result = $contracts->getWhere($config)[0];

        if (sizeof($result) > 0) {
            $responseCode = 200;
            $contracts->delete($result['id']);
            $responseData = array('status' => 'success');
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'response_data' => array(), 'message' => 'data not found');
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->get(
    '/quotes/{quote_id}/contracts/download',
    function ($request, $response, $args) {

        $config = array (
        'quote_id' => $args['quote_id'],
        );

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $quote = new Quote();
        $quoteInfo = $quote->getQuoteInfo($config['quote_id']);
        $contracts = new Contracts();
//        $result = $contracts->getWhere($config)[0];

        $allGetVars = $request->getQueryParams();
        $result = $contracts->getWhere($config)[0];


        $finfo = new finfo(FILEINFO_MIME);
        $mimeType = $finfo->buffer($result['contracts']);
        $mimeType = explode(';', $mimeType);
        $ext = $mimeType[0] === 'application/pdf' ? '.pdf' : '.jpg';

        $fileName = $quoteInfo['orderName'].$ext;

        if (sizeof($result) > 0) {
            $newResponse = $response->withAddedHeader('Content-Type', $mimeType[0])
                ->withAddedHeader('Pragma', "public")
                ->withAddedHeader('Content-disposition:', 'attachment; filename='.$fileName)
                ->withAddedHeader('Content-Transfer-Encoding', 'binary')
                ->withAddedHeader('Content-Length', strlen($result['contracts']));
                
            return $newResponse->write($result['contracts']);
        } else {
            $responseData = array('status' => 'error', 'response_data' => array(), 'message' => 'data not found');
            
            return responseWithStatusCode($response, $responseData, 404);
        }
    }
);




$app->post(
    '/quotes/{quote_id}/attachFiles',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $files = $request->getUploadedFiles();
        $config = array ('quote_id' => $args['quote_id']);

        if (empty($files['filesToUpload'])) {
            throw new Exception('Expected attachment file');
        }

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $filesDir = "/tmp/emailFiles-quote-".$args['quote_id'];
        if (!is_dir($filesDir)) {
            mkdir($filesDir, 0777, true);
        }

        $responseData = array('status' => 'success', 'files' => []);

        foreach ($files['filesToUpload'] as $file) {
            if ($file->getError() === UPLOAD_ERR_OK) {
                $filename = $file->getClientFilename();
                if (!file_exists($filesDir.'/'.$filename)) {
                    $file->moveTo($filesDir.'/'.$filename);
                    array_push($responseData['files'], $filename);
                }
            }
        }

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->post(
    '/quotes/{quote_id}/deleteAttachment',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = jsonDecodeWithErrorChecking($request->getBody());
        $file = $data['file'];
        $quoteId = $args['quote_id'];

        if (!isset($quoteId)) {
            throw new Exception('Quote ID not specified');
        }

        $filesDir = "/tmp/emailFiles-quote-".$args['quote_id'];
        $filePath = $filesDir.'/'.$file;
        if (file_exists($filePath) && unlink($filePath)) {
            $responseData = array('status' => 'success');
        } else {
            $responseData = array('status' => 'error', 'message' => 'Failed to delete attachment:'.$file);
        }

        return responseWithStatusCode($response, $responseData, 201);
    }
);




$app->post(
    '/quotes/{quote_id}/clearAttachments',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $quoteId = $args['quote_id'];

        if (!isset($quoteId)) {
            throw new Exception('Quote ID not specified');
        }

        $filesDir = "/tmp/emailFiles-quote-".$args['quote_id'];
        array_map('unlink', glob($filesDir."/*"));
        rmdir($filesDir);

        $responseData = array('status' => 'success');

        return responseWithStatusCode($response, $responseData, 201);
    }
);
