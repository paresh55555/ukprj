<?php

use SalesQuoter\Users\User;

$app->get(
    '/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();
        //var_dump($this->token->hasScope(["all"])); exit;
        $user = new User();
        $users = $user->getUsers($allGetVars);


        return responseWithStatusCode($response, $users, 200);
    }
);


$app->get(
    '/users/emailAvailable/{email}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
     
        $user = new User();
        $result = $user->isEmailAvailable($args['email']);

        return responseWithStatusCode($response, $result, 200);
    }
);




$app->get(
    '/users/{id}',
    function ($request, $response, $args) {

        $hasPermission = true;

        if (false === $this->token->hasScope(["admin"])) {
            
            if (false === $this->token->hasScope([$args["id"]])) {
                
                $hasPermission = false;
            }else {
                $hasPermission = true;
            }
        }else {

            $hasPermission = true; 
        }

        if ( $hasPermission ){

            $user = new User();
            $result = $user->getUser($args['id']);

            if (count($result) > 1) {
                 $responseCode = 200;
                 $result['status'] = "success";
            } else {
                 $responseCode = 404;
                 $result = array('status' => 'error' , 'message' => 'data not found');
            }

            return responseWithStatusCode($response, $result, $responseCode);

        } else {
            return invalidPermissionsResponse($response);
        } 

    }
);




$app->delete(
    '/users/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->deleteUser($args['id']);

        return responseWithStatusCode($response, $users, 200);
    }
);





$app->post(
    '/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $user = new User();
        $result = $user->createUser($config);
        $result['status'] = "success";

        return responseWithStatusCode($response, $result, 201);
    }
);


$app->put(
    '/users',
    function ($request, $response, $args) {

        $hasPermission = true;

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if (false === $this->token->hasScope(["admin"])) {
            
            if (false === $this->token->hasScope([$config["id"]])) {
                
                $hasPermission = false;
            }else {
                $hasPermission = true;
            }
        }else {

            $hasPermission = true; 
        }

        if ( $hasPermission ) {

            $data = $request->getBody();
            $config = jsonDecodeWithErrorChecking($data);
            
            /*
            if (false === $this->token->hasScope([$config["id"]])) {
                return invalidPermissionsResponse($response);
            }
            */

            $user = new User();
            $result = $user->updateUser($config);
            //$result->status = "success";

            if (count($result) > 1) {
                 $result['status']  =  'success';
                 $responseCode = 200;
            } else {
                 $responseCode = 404;
                 $result = array('status' => 'error' , 'message' => 'data not found');
            }

            return responseWithStatusCode($response, $result, $responseCode);

        }else {

            return invalidPermissionsResponse($response);
        }

    }
);





$app->put(
    '/users/{id}',
    function ($request, $response, $args) {

        /* 
        if (false === $this->token->hasScope([$args["id"]])) {
            return invalidPermissionsResponse($response);
        }
        */

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["id"] = $args["id"];

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $result['status']  =  'success';
             $responseCode = 200;
        } else {
             $responseCode = 404;
             $result = array('status' => 'error' , 'message' => 'data not found');
        }

        return responseWithStatusCode($response, $result, $responseCode);
    }
);
