<?php

use SalesQuoter\Components\Components;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Cache\Cache;
use SalesQuoter\Systems\Systems;
use SalesQuoter\Version\Version;

$app->get(
    '/systemGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getWithoutPremadeSystemGroups();

        for ($count = 0; $count < sizeof($allComponents); $count++) {
              $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
              $allComponents[$count]['systems'] = $components->getWhere(array("grouped_under" => $allComponents[$count]['id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }

        $data = array('status' => 'success', 'response_data' =>  $allComponents );
        
        return responseWithStatusCode($response, $data, 200);

    }
);



$app->post(
    '/systemGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config["type"] = "systemGroup";

        $components = new Components();

        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'name can not be Empty');
        } else {
            $responseCode = 200;
            $result = $components->create($config);
            $responseData = array('status' => 'success', 'response_data' => $result);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->put(
    '/systemGroups/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $checkData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkData['type'] = "systemGroup";
        $checkData['id'] =  $config['id'] =  $args['id'];

        $config["type"] = "systemGroup";

        $components = new Components();
        $singleComponents = $components->getWhere($checkData);

        if (sizeof($singleComponents) > 0) {
            if (intval($config['system_id']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
            } else {
                $responseCode = 400;
                $result = $components->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->delete(
    '/systemGroups/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $components->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/systemGroups/{id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["component_id"] = $args["id"];

        $traits = new Traits();
        $components = new Components();

        $checkData = array("type" => "systemGroup", "id" => $args["id"] );

        $checkExists = $components->getWhere($checkData);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id , component_id and trait_id  combination is not correct' );

            return responseWithStatusCode($response, $responseData, 400);
        }

        $singleTraits = $traits->get($config['id']);

        if (sizeof($singleTraits) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'Name can not be Empty' );
            } else {
                $responseCode = 200; 
                $result = $traits->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->delete(
    '/systemGroups/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $components = new Components();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $checkData = array(
                'id' => $args['id'],
                'component_id' => $args['component_id'],
            );
            $isExists = $traits->getWhere($checkData);

            if (sizeof($isExists) > 0) {
                $responseCode = 200;
                $responseData = $traits->delete($args['id']);
            } else {
                $responseCode = 404;
                $responseData = array('status' => 'error', 'message' => 'Data not Exists');
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systemGroups/publish',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $datas = $request->getBody();
        $config = jsonDecodeWithErrorChecking($datas);
    
        $cache = new Cache();
        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getWithoutPremadeSystemGroups();
        $cache->updateAllFalse("systemGroup", "0");

        for ($count = 0; $count < sizeof($allComponents); $count++) {
              $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
              $allComponents[$count]['systems'] = $components->getWhere(array("grouped_under" => $allComponents[$count]['id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }


        $config["cachedArray"] = serialize($allComponents);
        $config["current"] =  true;
        $config["type"] =  "systemGroup";
        $config["system_id"] = "0";

        $result =  $cache->create($config);
        $result[0]['cachedArray'] =  unserialize($result[0]['cachedArray']);

        $version = new Version();
        $version->updateVersion("frontSystemGroup", 0);

        $responseData = array('status' => 'success', 'response_data' =>  $result );

        return responseWithStatusCode($response, $responseData, 201);
    }
);





$app->get(
    '/systemGroups/ux',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $cache = new Cache();
        $params = array( "type" => "systemGroup" , "current" => true , "system_id" => 0);
        $allSystems = $cache->getWhere($params)[0]['cachedArray'];

        $allSystems = unserialize($allSystems);

        $responseData = array('status' => 'success', 'response_data' => $allSystems );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->post(
    '/systemGroup/{id}/images',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $data = $request->getParsedBody();

        $base64String = $data['file'];
        $systemId = intval($args['id']);
        $error = 0;

        if (empty($systemId) || $systemId == '0') {
            $responseData = array('status' => 'error', 'message' => 'Empty system ID');

            return responseWithStatusCode($response, $responseData, 400);
        } elseif (strlen($base64String) < 5) {
            $responseData = array('status' => 'error', 'message' => 'Empty Image ');

            return responseWithStatusCode($response, $responseData, 400);
        }

        $uploadPath = 'images/systemGroup/'.$systemId.'/';

        if (!is_dir($uploadPath)) {
            $folderCreated =  mkdir($uploadPath);

            if (!$folderCreated) {
                $msg = " Folder is not Created. Pls check File Permissions ";
            }
        }

        $fileName = 'systemGroup_'.$systemId.'.jpg';
        $outputFile = $uploadPath.$fileName;

        $ifp = fopen($outputFile, "wb");

        $data = explode(',', $base64String);

        $writeFile = fwrite($ifp, base64_decode($data[1]));
        if ($writeFile === false) {
             $error = 1;
             $msg = " File is not written in desired folder. Pls check File Permissions ";
        }

        fclose($ifp);

        if ($error == '1') {

            $responseData = array(
                'status' => 'error',
                'msg' => $msg,
            );

            return responseWithStatusCode($response, $responseData, 401);

        } else {

            $responseData = array(
                'status' => 'error',
                'imgUrl' => "api/V4/".$outputFile,
            );

            return responseWithStatusCode($response, $responseData, 200);
        }
    }
);
