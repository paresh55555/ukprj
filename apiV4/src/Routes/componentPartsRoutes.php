<?php

use SalesQuoter\Traits\Traits;
use SalesQuoter\Parts\OptionParts;
use SalesQuoter\Costs\OptionPartsCosts;

$app->get(
    '/systems/{system_id}/components/{component_id}/parts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $optionParts = new OptionParts();
        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400); 
        }
 
        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id'] = 0;

        $allOptionParts = $optionParts->getWhere($data);

        for ($count = 0; $count < sizeof($allOptionParts); $count++) {
              $allOptionParts[$count]['costs'] =   $optionPartsCosts->getAllCosts($allOptionParts[$count]['id']);
        }

        $responses = array('status' => 'success', 'response_data' => $allOptionParts );

        return responseWithStatusCode($response, $responses, 200); 
    }
);



$app->get(
    '/systems/{system_id}/components/{component_id}/parts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $optionParts = new OptionParts();
        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400); 
        }


        $singleOptionsParts = $optionParts->get($args['id']);

        if (sizeof($singleOptionsParts) > 0) {
             $responseCode = 200;
             $singleOptionsParts[0]['status'] = 'success';
             $singleOptionsParts[0]['costs'] =   $optionPartsCosts->getAllCosts($singleOptionsParts[0]['id']);
             $responses = $singleOptionsParts[0];
        } else {
             $responseCode = 400;
             $responses = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        
        return responseWithStatusCode($response, $responses, $responseCode); 
    }
);




$app->delete(
    '/systems/{system_id}/components/{component_id}/parts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $traits = new Traits();
        $optionParts = new OptionParts();
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData =  $optionParts->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/parts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = 0;

        $traits = new Traits();
        $optionParts = new OptionParts();
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $optionParts->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->put(
    '/systems/{system_id}/components/{component_id}/parts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = 0;
        $config['id'] = $args['id'];

        $traits = new Traits();
        $optionParts = new OptionParts();
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $singleOptionParts = $optionParts->get($config['id']);

        if (sizeof($singleOptionParts) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $optionParts->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  
        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
