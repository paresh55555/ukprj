<?php

use SalesQuoter\Traits\Traits;
use SalesQuoter\Traits\OptionTraits;


$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $optionTraits = new OptionTraits();

        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $data  = array();
        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id']  = $args['option_id'];

        $allOptionTraits = $optionTraits->getWhere($data);

        $responseData = array('status' => 'success', 'response_data' => $allOptionTraits );

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $optionTraits = new OptionTraits();

        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $singleOptionTraits = $optionTraits->get($args['id']);

        if (sizeof($singleOptionTraits) > 0) {
             $responseCode = 200;
             $singleOptionTraits[0]['status'] = 'success';
             $responseData = $singleOptionTraits[0];
        } else {
             $responseCode = 400;
             $responseData = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }       


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200; 
            $deleteOptionTraits = $optionTraits->delete($args['id']);
            $responseData = $deleteOptionTraits;
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];

        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseCode = 400;
         
        if (strlen($config['value']) == '0') {
            $responseData = array('status' => 'error', 'message' => 'Value can not be Empty' );
        } elseif (strlen($config['name']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $optionTraits->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
 

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/multipleTraits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $traits = jsonDecodeWithErrorChecking($data);
        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $resultTraits = [];
        foreach ($traits as $trait) {
            $trait['system_id'] = $args['system_id'];
            $trait['component_id'] = $args['component_id'];
            $trait['option_id'] = $args['option_id'];
            if (strlen($trait['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' => ' name can not be Empty');

                return responseWithStatusCode($response, $responseData, $responseCode);

            } else {
                if (isset($trait['id'])) {
                    $result = $optionTraits->update($trait);
                } else {
                    $result = $optionTraits->create($trait);
                }

                array_push($resultTraits, $result[0]);
            }
        }
        
        $responseCode = 201; 
        $responseData = array('status' => 'success', 'response_data' => $resultTraits);

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->put(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['id'] = $args['id'];

        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $singleOptionTraits = $optionTraits->get($config['id']);

        if (sizeof($singleOptionTraits) > 0) {
            if (strlen($config['value']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' => 'Value can not be Empty' );
            } elseif (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $optionTraits->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
