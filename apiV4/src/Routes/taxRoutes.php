<?php

use SalesQuoter\Tax\SalesTax;

$app->get(
    "/salesTax",
    function ($request, $response, $arguments) {

        $tax = new SalesTax();

		$allGetVars = $request->getQueryParams();

        $client = \TaxJar\Client::withApiKey("16955df3fdfb7d449137f0ed68e94cb4");

        try {

	        $rates = $client->ratesForLocation($allGetVars['zip'], [
	          'city' => $allGetVars['city'],
	          'country' =>  $allGetVars['country'] 
	        ]);

	        $rate = $rates->combined_rate;

	        $responseData = [ "status" => "success", "message" => '' , "rate" =>  $rate ];

        }catch ( Exception $exp ) {

        	$message =  $exp->getMessage();
        	$parseError = explode("–", $message);

        	if ( sizeof($parseError) > 0 ) {

        		$errorMsg = $parseError[1];
        	}else {
        		$errorMsg = $parseError[0];
        	}

        	$responseData = [ "status" => "error",  "message" => $errorMsg,  "rate" =>  '' ];
        }

        return responseWithStatusCode($response, $responseData, 200);
    }
);