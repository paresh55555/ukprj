<?php

use SalesQuoter\SalesTerritory\SalesTerritory;

$app->get(
    '/territories',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $salesTeriObj = new SalesTerritory();
        $territories = $salesTeriObj->getAllTeritory();


        return responseWithStatusCode($response, $territories, 200);
    }
);




$app->delete(
    '/territories/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $salesTeriObj = new SalesTerritory();
        $territories  =  $salesTeriObj->deleteTeritory($args['id']);

        return responseWithStatusCode($response, $territories, 200);
    }
);



$app->post(
    '/territories',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $salesTeriObj = new SalesTerritory();
        if (strlen($config['zip']) == '0') {
             $result  =  $salesTeriObj->zipOverLapValidation($config['zipLow'], $config['zipHigh']);
         
            if ($result === false) {
                $result = $salesTeriObj->create($config);
                $result['status'] = 'success';
            }
        } else {
            $config['zipLow'] = 0;
            $config['zipHigh'] = 0;

            $result = $salesTeriObj->create($config);

            $result['status'] = 'success';
        }
  

        return responseWithStatusCode($response, $result, 201);
    }
);




$app->put(
    '/territories',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $salesTeriObj = new SalesTerritory();
        $territoriesDetails = $salesTeriObj->get($config['id']);
         
        $code = 200; 
        if (sizeof($territoriesDetails) > 0) {
            if (strlen($config['zip']) == '0') {
                $result  =  $salesTeriObj->zipOverLapValidationUpdate($config['zipLow'], $config['zipHigh'], $config['id']);
         
                if ($result === false) {
                    $result = $salesTeriObj->update($config);
                    $result['status'] = 'success';
                }
            } else {
                $config['zipLow'] = 0;
                $config['zipHigh'] = 0;

                $result = $salesTeriObj->update($config);

                $result['status'] = 'success';
            }
        } else {
            $code = 404;
            $result['status'] = 'error';
            $result['message'] = 'ID not Exists';
        }
    

        return responseWithStatusCode($response, $result, $code);
    }
);
