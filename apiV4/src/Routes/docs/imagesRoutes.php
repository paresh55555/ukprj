<?php

/**
 *    @SWG\Post(
 *      path="/image",
 *      tags={"image"},
 *      operationId="getImage",
 *      summary="Uploading Components Image",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table primary key ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="file",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="base64 encoded image like data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAsIAAAPACAIAAACwz4gxAAA"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="system id"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="2"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="Option id "
 *      ),
 *       @SWG\Parameter(
 *          name="trait_id",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="Traits Id"
 *      ),
 *       @SWG\Parameter(
 *          name="type",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description=""
 *      ),
 *       @SWG\Parameter(
 *          name="width",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="width of image"
 *      ),
 *       @SWG\Parameter(
 *          name="height",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="height of image"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'result':'success','imgUrl':'api/V4/images/option/4/4/3.svg'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Empty system ID'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'File is written in desired folder. Pls check File Permissions'}",
 *           response=401,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */
