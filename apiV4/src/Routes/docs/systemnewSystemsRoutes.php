<?php


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="All Components Info Where type is system",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'system','grouped_under''my_order':0,'required':false,'multiple':false,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'ChooseDimensions:','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1},{'id':3,'component_id':4, ...}] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer'}",
 *           response=400,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



 /**
 *    @SWG\Post(
 *      path="/systems",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Components Details Information  Where type is system",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test traits 2', 'system_id':1, 'menu_id':0,'grouped_under':0,'my_order':0,'required':false,'multiple':false}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':848,'system_id':1,'name':'test traits','menu_id':0,'type':'system','grouped_under':0,'my_order':0,'required':false,'multiple':false}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Post(
 *      path="/systems/door",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Creates a new system named Door System under the systemGroup PreMadeDoorSystem",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'grouped_under':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':848,'system_id':848,'name':'Door System','menu_id':0,'type':'system','grouped_under':1,'my_order':0,'required':0,'multiple':0}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System Group Not Exists' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Post(
 *      path="/systems/window",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Creates a new system named Window System under the systemGroup PreMadeWindowSystem",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'grouped_under':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':848,'system_id':848,'name':'Window System','menu_id':0,'type':'system','grouped_under':1,'my_order':0,'required':0,'multiple':0}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System Group Not Exists' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Put(
 *      path="/systems/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Components Details Information  Where type is system",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Components ID"
 *      ),  
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test traits 2', 'system_id':1, 'menu_id':0,'grouped_under':0,'my_order':0,'required':false,'multiple':false}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':848,'system_id':1,'name':'test traits','menu_id':0,'type':'system','grouped_under':0,'my_order':0,'required':false,'multiple':false}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/systems/{id}/traits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Components traits Details Information  Where Components type is system",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{'id':957,'component_id':881,'name':'title','type':'component','value':'aaa','visible':1},{'id':958,'component_id':881,'name':'note','type':'component','value':'rrr','visible':1},{'name':'note','type':'component','value':'Test note'}]" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'21sdf', 'component_id':3,'type':'tesss','value':'21','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component does not exist' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Put(
 *      path="/systems/{id}/traits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating traits Information Whose Components type is systemGroup",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table primary key ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf','type':'tesss','value':'21','visible':1}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'21sdf', 'component_id':3,'type':'tesss','value':'21','visible':1}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id , component_id and trait_id  combination is not correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id , component_id and trait_id  combination is not correct'}",
 *           response="invalid combination",
 *      ),          
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Delete(
 *      path="/systems/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a Components by ID  Where type is system",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer'}",
 *           response=400,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd




// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/systems/publish",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding  systemGroups",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test systemGroup', 'system_id':1}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':3,'type':'systemGroup','cachedArray':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'systemGroup','grouped_under':0,'my_order':0,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'Choose Dimensions :','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1}]...]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{id}/ux",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting systemUX info",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ), 
 *        
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':3,'type':'systemUX','cachedArray':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'systemGroup','grouped_under':0,'my_order':0,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'Choose Dimensions :','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1}]...]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{id}/price",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting systemPrice type Components Info",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ),  
 *        
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':3,'type':'systemPrice','cachedArray':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'systemGroup','grouped_under':0,'my_order':0,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'Choose Dimensions :','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1}]...]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/systems/{id}/images",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Uploading Components Image where type is systems",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table primary key ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="file",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="base64 encoded image like data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAsIAAAPACAIAAACwz4gxAAA" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'result':'success','imgUrl':'api/V4/images/systems/1/systems_1.jpg'}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Empty system ID'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'File is written in desired folder. Pls check File Permissions'}",
 *           response=401,
 *      ),        
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


 // @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{id}/panelRanges/width/{width}/height/{height}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Gettting panel Ranges of by width & height ",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="systems id"
 *      ),
 *       @SWG\Parameter(
 *          name="width",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description=" width of panel ranges"
 *      ),
 *       @SWG\Parameter(
 *          name="height",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="height of panel ranges"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'result':'success','response_data':[2,3,4]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Empty system ID'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'File is written in desired folder. Pls check File Permissions'}",
 *           response=401,
 *      ),        
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
