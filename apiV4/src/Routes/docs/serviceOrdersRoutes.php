<?php

 /**
 *    @SWG\Get(
 *      path="/service/orders",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all service orders",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""order_number"":""2000"", ""site"": ""test"", ""warranty"": 1, ""warranty_length"": 1, ""warranty_end_date"": ""2018-05-23"", ""complete_date"": ""2018-05-31"", ""customers"": 0, ""jobs"": 0, ""doors"": 0, ""docs"": 0, ""site"": """",""notes"":[{""id"":""0"",""note"":""Some note""},{""id"":""2"",""note"":""Some note 2""}] } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

/**
 *    @SWG\Post(
 *      path="/service/orders",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="adding service order",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{""order_number"":""2000"", ""site"": ""test"", ""warranty"": 1, ""warranty_length"": 1, ""warranty_end_date"": ""2018-05-23"", ""complete_date"": ""2018-05-31"", ""door_installed"": ""2018-05-31"", ""installed_by"":""niloy b""}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ {""id"":""2"", ""order_number"":""2000"", ""site"": ""test"", ""warranty"": 1, ""warranty_length"": 1, ""warranty_end_date"": ""2018-05-23"", ""complete_date"": ""2018-05-31"", ""door_installed"": ""2018-05-31"", ""installed_by"":""niloy b""} ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Put(
 *      path="/service/orders/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="updating service order",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""order_number"":""2000"", ""site"": ""test"", ""warranty"": 1, ""warranty_length"": 1, ""warranty_end_date"": ""2018-05-23"", ""complete_date"": ""2018-05-31"", ""notes"":[{""id"":""0"",""note"":""Some note""},{""id"":""2"",""note"":""Some note 2""}], ""door_installed"": ""2018-05-31"", ""installed_by"":""niloy b"" } " 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""order_number"":""2000"", ""site"": ""test"", ""warranty"": 1, ""warranty_length"": 1, ""warranty_end_date"": ""2018-05-23"", ""complete_date"": ""2018-05-31"", ""door_installed"": ""2018-05-31"", ""installed_by"":""niloy b"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



  /**
 *    @SWG\Delete(
 *      path="/service/orders/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Deleting a service orders",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="items table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Post(
 *      path="/service/orders/completed",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="adding service order",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{""order_number"": ""FFF1234"", ""site"":""PD"", ""complete_date"":""12-10-2018"", ""notes"": [{""id"":""0"", ""note"": ""note 1""}], ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com"", ""phone"": ""+8801964405239"", ""address_1"":""test"", ""address_2"":""test"", ""city"":""test"", ""state"":""test"", ""zipcode"": ""10222"", ""doors"": [{ ""warranty_end_date"": ""12-07-2018"", ""door_type"": ""test"", ""traits"": [{ ""id"": ""0"", ""name"": ""color"", ""trait_value"": ""black"" }]}]}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ {""id"":""2"", ""order_number"":""2000"", ""site"": ""test"", ""warranty"": 1, ""warranty_length"": 1, ""warranty_end_date"": ""2018-05-23"", ""complete_date"": ""2018-05-31""} ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */