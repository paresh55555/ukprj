<?php

 /**
 *    @SWG\Get(
 *      path="/service/jobs/{job_id}/address",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all job addresses",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""job_id"": 1, ""address_id"": 1, ""customer_id"": 1,""note"": null, ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": null, ""address_2"": null, ""city"": null, ""state"": null, ""zip"": null } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Get(
 *      path="/service/jobs/{job_id}/address/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting a job address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""job_id"": 1, ""address_id"": 1, ""note"": null, ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": null, ""address_2"": null, ""city"": null, ""state"": null, ""zip"": null } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

/**
 *    @SWG\Post(
 *      path="/service/jobs/{job_id}/address",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="adding a job address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""note"": ""test customers"",""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test"", ""address_id"": ""0"", ""customer_id"": ""0"" }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": ""2"", ""job_id"": ""1"", ""address_id"": ""1"", ""note"": ""test customers"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Put(
 *      path="/service/jobs/{job_id}/address/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="updating job address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""note"": ""test customers"",""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test"", ""address_id"": ""0"", ""customer_id"": ""0"" }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": ""2"", ""job_id"": ""1"", ""address_id"": ""1"", ""note"": ""test customers"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



  /**
 *    @SWG\Delete(
 *      path="/service/jobs/{job_id}/address/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Deleting a job address"
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
