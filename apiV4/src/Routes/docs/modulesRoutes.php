<?php

use SalesQuoter\Doors\Size;
use SalesQuoter\Doors\PanelRange;
use SalesQuoter\Doors\Module;
use SalesQuoter\Doors\Frame;
use SalesQuoter\Doors\Fixed;
use SalesQuoter\Doors\Percent;
use SalesQuoter\Doors\Glass;
use SalesQuoter\Doors\ModuleGroup;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/modules",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *
 *      @SWG\Response(
 *           description="[{'id':2,'panelGroup':1,'title':'Absolute Vinyl with Nail Fin Frame','url':'images\/products\/Vinyl Door.svg'},......]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/modules',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $module = new ModuleGroup();

        $moduleObjects = $module->getAll();

        renderJSON($moduleObjects);
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Get(
 *      path="/modules/{module}/percent",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *        
 *
 *      @SWG\Response(
 *           description="[{'id':13,'module_id':9999,'nameOfOption':'White Vinyl','percentage':0},....]",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/modules/{module}/percent',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $percent = new Percent();

        $config = array ("module_id" => $args['module'] );
        $percentObjects = $percent->getAll($config);

        renderJSON($percentObjects);
    }
);

// @codingStandardsIgnoreStart
/**
 *    @SWG\Put(
 *      path="/modules/{module}/percent/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules Parcent",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *
 *      @SWG\Response(
 *           description="{'id':13,'module_id':9999,'nameOfOption':'White Vinyl','percentage':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->put(
    '/modules/{module}/percent/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $percent = new Percent();

        $percentObject = $percent->update($config);

        if (count($percentObject) > 1) {
             $percentObject['status'] = "success";
             renderJSON($percentObject);
        } else {
             renderJSON(array("status" => "error" , "message" => "data not found"));
        }
    }
);

// @codingStandardsIgnoreStart
/**
 *    @SWG\Post(
 *      path="/modules/{module}/percent",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Adding  Modules Parcent ",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="module percent JSON data"
 *      ),
 *      
 *      @SWG\Response(
 *           description="{'id':13,'module_id':9999,'nameOfOption':'White Vinyl','percentage':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->post(
    '/modules/{module}/percent',
    function ($request, $response, $args) {
   
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $percent = new Percent();
        $percentObject = $percent->create($config);
        $percentObject["status"] = "success";

        renderJSON($percentObject);
    }
);



// @codingStandardsIgnoreStart
/**
 *    @SWG\Delete(
 *      path="/modules/{module}/percent/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Deleting specific Modules Parcent",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *      
 *      @SWG\Response(
 *           description="{'status':'failed'}  OR {'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->delete(
    '/modules/{module}/percent/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $percent = new Percent();
        $percentObject = $percent->delete($args['id']);

        renderJSON($percentObject);
    }
);

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/modules/{module}/fixed",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Module Name"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':45,'module_id':9999,'nameOfOption':'White','partNumber':'T.B.D.','costItem':0,'quantityFormula':'swings','forCutSheet':1,'myOrder':45},{......]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/modules/{module}/fixed',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $fixed = new Fixed();

        $config = array ("module_id" => $args['module'] );
        $fixedObjects = $fixed->getAll($config);

        renderJSON($fixedObjects);
    }
);

// @codingStandardsIgnoreStart
/**
 *    @SWG\Put(
 *      path="/modules/{module}/fixed/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules Parcent",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *
 *      @SWG\Response(
 *           description="{'id':45,'module_id':9999,'nameOfOption':'White','partNumber':'T.B.D.','costItem':0,'quantityFormula':'swings','forCutSheet':1,'myOrder':45}",
 *           response=200,
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->put(
    '/modules/{module}/fixed/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $fixed = new Fixed();

        $fixedObject = $fixed->update($config);

        if (count($fixedObject) > 1) {
             $fixedObject['status'] = "success";
             renderJSON($fixedObject);
        } else {
             renderJSON(array("status" => "error" , "message" => "data not found"));
        }
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Post(
 *      path="/modules/{module}/fixed",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Adding  Modules Fixed",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="module percent JSON data"
 *      ),
 *      
 *      @SWG\Response(
 *           description="{'id':45,'module_id':9999,'nameOfOption':'White','partNumber':'T.B.D.','costItem':0,'quantityFormula':'swings','forCutSheet':1,'myOrder':45}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->post(
    '/modules/{module}/fixed',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $fixed = new Fixed();
        $fixedObject = $fixed->create($config);
        $fixedObject['status'] = 'success';

        renderJSON($fixedObject);
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Delete(
 *      path="/modules/{module}/fixed/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Deleting specific Modules Parcent",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *      
 *      @SWG\Response(
 *           description="{'status':'failed'}  OR {'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */

//@codingStandardsIgnoreEnd

$app->delete(
    '/modules/{module}/fixed/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $fixed = new Fixed();
        $fixedObject = $fixed->delete($args['id']);

        renderJSON($fixedObject);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/modules/{module}/frame",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Module Name"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':2,'panelGroup':1,'title':'Absolute Vinyl with Nail Fin Frame','url':'images\/products\/Vinyl Door.svg'},......]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/modules/{module}/frame',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $frame = new Frame();

        $config = array ("module_id" => $args['module'] );
        $frameObjects = $frame->getAll($config);

        renderJSON($frameObjects);
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Put(
 *      path="/modules/{module}/frame/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules Frame",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *
 *      @SWG\Response(
 *           description="[{'id':100,'module_id':9999,'nameOfOption':'OUTER FRAME TOP BOTTOM VINYL','option_type':0,'material':null,'cost_meter':100,'cost_paint_meter':10,'waste_percent':0,'number_of_lengths_formula':'2','cut_size_formula':'width','burnOff':null,'forCutSheet':1,'myOrder':1}...]",
 *           response=200,         
 * 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd    

$app->put(
    '/modules/{module}/frame/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $frame = new Frame();

        $frameObject = $frame->update($config);

        if (count($frameObject) > 1) {
             $frameObject['status'] = "success";
             renderJSON($frameObject);
        } else {
             renderJSON(array("status" => "error" , "message" => "data not found"));
        }
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Post(
 *      path="/modules/{module}/frame",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Adding  Modules Fixed",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="module frame JSON data"
 *      ),
 *      
 *      @SWG\Response(
 *           description="{'id':100,'module_id':9999,'nameOfOption':'OUTER FRAME TOP BOTTOM VINYL','option_type':0,'material':null,'cost_meter':100,'cost_paint_meter':10,'waste_percent':0,'number_of_lengths_formula':'2','cut_size_formula':'width','burnOff':null,'forCutSheet':1,'myOrder':1}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->post(
    '/modules/{module}/frame',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $frame = new Frame();
        $frameObject = $frame->create($config);
        $frameObject["status"] = "success";

        renderJSON($frameObject);
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Delete(
 *      path="/modules/{module}/frame/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Deleting specific Modules Frame",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *      
 *      @SWG\Response(
 *           description="{'status':'failed'}  OR {'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->delete(
    '/modules/{module}/frame/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $frame = new Frame();
        $frameObject = $frame->delete($args['id']);

        renderJSON($frameObject);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Get(
 *      path="/modules/{module}/panelRanges",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules panelRanges details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Module Name"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'panels':2,'low':36,'high':66},{'id':2,'panels':3,'low':63,'high':125},....]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/modules/{module}/panelRanges',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $panels = new PanelRange();
        $module = new Module();

        $moduleObject = $module->get($args['module']);


        $config = array ("panelGroup" => $moduleObject['panelGroup'] );
        $panelObject = $panels->getAll($config);

        renderJSON($panelObject);
    }
);


// @codingStandardsIgnoreStart

/**
 *    @SWG\Put(
 *      path="/modules/{module}/panelRanges/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules panelRanges",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *
 *      @SWG\Response(
 *           description="{'id':1,'panels':2,'low':36,'high':66}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->put(
    '/modules/{module}/panelRanges/{id}',
    function ($request, $response, $args) {
  
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $panel = new PanelRange();
        $panelObject = $panel->update($config);

        if (count($panelObject) > 1) {
             $panelObject['status'] = "success";
             renderJSON($panelObject);
        } else {
             renderJSON(array("status" => "error" , "message" => "data not found"));
        }
    }
);

// @codingStandardsIgnoreStart
/**
 *    @SWG\Post(
 *      path="/modules/{module}/panelRanges",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Adding  Modules Fixed",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
*      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="module panelRanges JSON data"
 *      ),
 *      
 *      @SWG\Response(
 *           description="{'id':1,'panels':2,'low':36,'high':66}",
 *           response=200,
 *           
 * 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->post(
    '/modules/{module}/panelRanges',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $module = new Module();
        $moduleObject = $module->get($args['module']);

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['panelGroup'] = $moduleObject['panelGroup'];

        $panel = new PanelRange();

        $checkValidation = $panel->checkValidation($config, $config['panelGroup']);
        if (sizeof($checkValidation) > 0) {
            $checkValidation['status']  = 'error';
            renderJSON($checkValidation);
        } else {
            $panelObject = $panel->create($config);
            $panelObject['status']  = 'success';
            renderJSON($panelObject);
        }
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Delete(
 *      path="/modules/{module}/panelRanges/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Deleting specific Modules panelRanges",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *      
 *      @SWG\Response(
 *           description="{'status':'failed'}  OR {'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->delete(
    '/modules/{module}/panelRanges/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $panel = new PanelRange();
        $panelObject = $panel->delete($args['id']);

        renderJSON($panelObject);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/modules/{module}/size",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules size details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Module Name"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'id':9999,'minWidth':26,'maxWidth':300,'minHeight':36,'maxHeight':96}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
 //@codingStandardsIgnoreEnd

$app->get(
    '/modules/{module}/size',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $size = new Size();
        $sizeObject = $size->get($args['module']);

        renderJSON($sizeObject);
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Put(
 *      path="/modules/{module}/size",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules size",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
 *
 *      @SWG\Response(
 *           description="{'id':9999,'minWidth':26,'maxWidth':300,'minHeight':36,'maxHeight':96}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->put(
    '/modules/{module}/size',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['module'];

        $size = new Size();
        $sizeObject = $size->update($config);

        if (count($sizeObject) > 1) {
             $sizeObject['status'] = "success";
             renderJSON($sizeObject);
        } else {
             renderJSON(array("status" => "error" , "message" => "data not found"));
        }
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/modules/{module}/glass",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Getting all Modules glass details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Module Name"
 *      ),
 *
 *      @SWG\Response(
 *           description="",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/modules/{module}/glass',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $glass = new Glass();

        $config = array ("module_id" => $args['module'] );
        $glassObject = $glass->getAll($config);

        renderJSON($glassObject);
    }
);


// @codingStandardsIgnoreStart
/**
 *    @SWG\Post(
 *      path="/modules/{module}/glass",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Adding  Modules Glass",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="module Glass JSON data"
 *      ),
 *      
 *      @SWG\Response(
 *           description="{'id':10,'module_id':9999,'nameOfOption':'Low-E 2','costPerPanel':0,'costSquareMeter':1550,'height_cut_size_formula':'height','width_cut_size_formula':'width \/ panels ','description':'Visible Light Transmittance (VLT): 70% Solar Heat Gain Coefficient (SHGC): 0.39 U-Value: .25'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->post(
    '/modules/{module}/glass',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $glass = new Glass();
        $glassObject = $glass->create($config);
        $glassObject['status'] = 'success';

        renderJSON($glassObject);
    }
);



// @codingStandardsIgnoreStart
/**
 *    @SWG\Put(
 *      path="/modules/{module}/glass/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules Glass",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *
 *      @SWG\Response(
 *           description="{'id':10,'module_id':9999,'nameOfOption':'Low-E 2','costPerPanel':0,'costSquareMeter':1550,'height_cut_size_formula':'height','width_cut_size_formula':'width \/ panels ','description':'Visible Light Transmittance (VLT): 70% Solar Heat Gain Coefficient (SHGC): 0.39 U-Value: .25'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->put(
    '/modules/{module}/glass/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];
        $config['id'] = $args['id'];

        $glass = new Glass();
        $glassObject = $glass->update($config);

        if (count($glassObject) > 1) {
             $glassObject['status'] = "success";
             renderJSON($glassObject);
        } else {
             renderJSON(array("status" => "error" , "message" => "data not found"));
        }
    }
);




// @codingStandardsIgnoreStart
/**
 *    @SWG\Delete(
 *      path="/modules/{module}/glass/{id}",
 *      tags={"modules"},
 *      operationId="getModules",
 *      summary="Updating specific Modules Glass",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="module",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="module name"
 *      ),
 *      
*      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="auto-increment primary key id"
 *      ),  
 *
 *      @SWG\Response(
 *           description="{'status':'failed'}  OR {'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->delete(
    '/modules/{module}/glass/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $glass = new Glass();
        $glassObject = $glass->delete($args['id']);

        renderJSON($glassObject);
    }
);
