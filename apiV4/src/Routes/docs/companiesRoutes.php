<?php

use SalesQuoter\Company\Company;
use SalesQuoter\Users\User;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/companies",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Getting all Companies Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'companyName':'Panoramic Doors','firstName':'','lastName':'','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','phone':'1234',zip':'92054'},{..........]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



// @codingStandardsIgnoreStart
/**
 *    @SWG\Get(
 *      path="/companies/order",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Getting all Companies Details Information By Company name ascending order",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'companyName':'Panoramic Doors','firstName':'','lastName':'','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','phone':'1234',zip':'92054'},{..........]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
 

 /**
 *    @SWG\Get(
 *      path="/companies/{id}",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Getting a specific Company Details Information with company id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="dealer table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'companyName':'Panoramic Doors','firstName':'','lastName':'','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','phone':'1234','zip':'92054'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd




// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/companies/{id}/users",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Getting a specific Company Details Information with company id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="dealer table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':2,'name':'Robert Myers'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



$app->get(
    '/companies/{id}/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $allUsers = $user->getUsersFromDealersId($args['id']);

        renderJSON($allUsers);
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Put(
 *      path="/companies/{id}",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Updating a specific Company Information with company id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="dealer table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'companyName':'Panoramic Doors','firstName':'','lastName':'','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','phone':'1234','zip':'92054'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID not Exists' }",
 *           response="not existing ID",
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd





$app->put(
    '/companies/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $company = new Company();
        $singleCompany = $company->checkExists($config['id']);
    
        if (sizeof($singleCompany) > 0) {
            $responses = $company->update($config);
            $responses['status'] = "success";
        } else {
            http_response_code(404);
            $responses = array("status" => "error", "message" => "Parameter ID not Exists");
        }

        renderJSON($responses);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/companies",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Adding a Company Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="integer",
 *          description="Company sending JSON Data"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'companyName':'Panoramic Doors','firstName':'','lastName':'','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','phone':'1234','zip':'92054'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->post(
    '/companies',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $company = new Company();
        $companyObjects = $company->create($config);
        $companyObjects['status'] = "success";

        renderJSON($companyObjects);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Delete(
 *      path="/companies/{id}",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Delete a specific Company with Company id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="dealer table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,        
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd




$app->delete(
    '/companies/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $company = new Company();
        $deleteResponse = $company->delete($args['id']);

        renderJSON($deleteResponse);
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/companies/companyAvailable",
 *      tags={"companies"},
 *      operationId="getCompanies",
 *      summary="Checking  a Company Name is available or not",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="integer",
 *          description="{'companyName':'Better2Company'}"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','is_available':true}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'company name can not be empty' }",
 *           response="empty company Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



$app->post(
    '/companies/companyAvailable',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $company = new Company();

        if (strlen($config['companyName']) == '0') {
             http_response_code(400);
             $response = array("status" => "error", "message" => "company name can not be empty");
        } else {
            $companyObjects = $company->isAvailable($config['companyName']);
            $response = array( 'status' => 'success' , 'is_available' => $companyObjects);
        }
   

        renderJSON($response);
    }
);
