<?php

 /**
 *    @SWG\Get(
 *      path="/service/customers",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all service customers",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [{ ""id"": 8, ""customer_id"": 9, ""address_id"": 8, ""note"": null, ""first_name"": ""ikn"", ""last_name"": ""JNKJN"", ""email"": ""KJNKJNBKJB"", ""phone"": ""KJBKJBKJ"", ""address_1"": ""JBNKJB"", ""address_2"": ""KJBK"", ""city"": ""JBKJ"", ""state"": ""KJBK"", ""zip"": ""JBK"" }]} ",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Get(
 *      path="/service/customers/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting a customer all addresses",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 4, ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_list"": [ { ""id"": 2, ""address_1"": ""kadolgazi road, feni"", ""address_2"": """", ""city"": ""Feni"", ""state"": ""Feni zila"", ""zip"": ""3100"" }, { ""id"": 3, ""address_1"": ""kadolgazi road, feni"", ""address_2"": ""123"", ""city"": ""Feni"", ""state"": ""Feni zila"", ""zip"": ""3100"" } ] } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Post(
 *      path="/service/customers",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all orders customers",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677""  }" 
 *      ),   
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [{ ""id"": 8, ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"" }]} ",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Put(
 *      path="/service/customers/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all orders customers",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677""  }" 
 *      ),   
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [{ ""id"": 8, ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"" }]} ",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Post(
 *      path="/service/customers/{id}/address",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="adding new customer address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""address_1"": ""JBNKJB"", ""address_2"": ""KJBK"", ""city"": ""JBKJ"", ""state"": ""KJBK"", ""zip"": ""JBK"" }" 
 *      ),   
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [{ ""id"": 7, ""address_1"": ""JBNKJB"", ""address_2"": ""KJBK"", ""city"": ""JBKJ"", ""state"": ""KJBK"", ""zip"": ""JBK"" }]} ",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Put(
 *      path="/service/customers/{id}/address/{address_id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="updating customer address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""address_1"": ""JBNKJB"", ""address_2"": ""KJBK"", ""city"": ""JBKJ"", ""state"": ""KJBK"", ""zip"": ""JBK"" }" 
 *      ),   
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [{ ""id"": 7, ""address_1"": ""JBNKJB"", ""address_2"": ""KJBK"", ""city"": ""JBKJ"", ""state"": ""KJBK"", ""zip"": ""JBK"" }]} ",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


  /**
 *    @SWG\Delete(
 *      path="/service/customers/{id}/address/{address_id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="updating customer address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),  
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"" }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Get(
 *      path="/service/customers/{id}/orders",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting customer orders lists",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""order_number"":""FFF202"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


  /**
 *    @SWG\Get(
 *      path="/service/customers/{id}/jobs",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting customer jobs lists",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""job_number"":""FFF202"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Put(
 *      path="/service/address/{address_id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Updating address",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""customer_id"": ""4"",""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test""  }" 
 *      ),  
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 7, ""address_1"": ""JBNKJB"", ""address_2"": ""KJBK"", ""city"": ""JBKJ"", ""state"": ""KJBK"", ""zip"": ""JBK"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */