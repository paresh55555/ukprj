<?php


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/version/{type}/{id}",
 *      tags={"version"},
 *      operationId="getVersion",
 *      summary="Getting a version details with type and Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="type",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="type can be adminSystemGroup, adminSystem, frontSystem, frontSystemGroup"
 *      ),  
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'_id':2, 'version':2}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
 //@codingStandardsIgnoreEnd