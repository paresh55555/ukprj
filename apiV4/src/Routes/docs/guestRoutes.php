<?php

use SalesQuoter\Guest\Guest;

 /**
 *    @SWG\Post(
 *      path="/guest",
 *      tags={"guest"},
 *      operationId="getGuest",
 *      summary="Adding a Guest Users Info",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'niloy','email':'niloy.cste@gmail.com','zip':'132','phone':'12222222','leadType':'test'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':33231,'type':'guest','token':'23hsi3y823i2'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sending Parameter Can not Be Empty' }",
 *           response="empty parameter",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sales_person_id must be Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/guest',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $guestData = jsonDecodeWithErrorChecking($data);

        $guest = new Guest();

        $response = $guest->newGuest($guestData);

        renderJSON($response);
    }
);
