<?php


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':'3',name':'21sdf'},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID must be Valid Integer'}",
 *           response=400,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/systems/{system_id}/components',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $components = new Components();
        $traits = new Traits();

        $data['system_id'] = (int) $args['system_id'];

        if ($data['system_id'] == '0') {
             http_response_code(400);
             $responses = array('status' => 'error', 'message' => 'System ID must be Valid Integer' );
        } else {
            $allComponents = $components->getWhere($data);
            for ($count = 0; $count < sizeof($allComponents); $count++) {
                  $allComponents[$count]['traits'] =   $traits->getWhere(array("component_id" => $allComponents[$count]["id"]));
            }


            $responses = array('status' => 'success', 'response_data' => $allComponents );
        }

        renderJSON($responses);
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/menu",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':446,'system_id':1,'name':'Menu','menu_id':0,'type':'','grouped_under':0,'my_order':0,'traits':[{'id':348,'component_id':446,'name':'title','type':'component','value':'yjyttt','visible':1}]}] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID must be Valid Integer'}",
 *           response=400,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
 
$app->get(
    '/systems/{system_id}/components/menu',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $components = new Components();
        $traits = new Traits();

        $data['system_id'] = (int) $args['system_id'];
        $data['name'] = 'Menu';

        if ($data['system_id'] == '0') {
             http_response_code(400);
             $responses = array('status' => 'error', 'message' => 'System ID must be Valid Integer' );
        } else {
            $allComponents = $components->getWhere($data);
            for ($count = 0; $count < sizeof($allComponents); $count++) {
                  $allComponents[$count]['traits'] =   $traits->getWhere(array("component_id" => $allComponents[$count]["id"]));
            }

            $responses = array('status' => 'success', 'response_data' => $allComponents );
        }

        renderJSON($responses);
    }
);




// @codingStandardsIgnoreStart

 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/menu/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':446,'system_id':1,'name':'Menu','menu_id':0,'type':'','grouped_under':1,'my_order':0,'traits':[{'id':348,'component_id':446,'name':'title','type':'component','value':'yjyttt','visible':1}],'options':[]}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID must be Valid Integer'}",
 *           response=400,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/systems/{system_id}/components/menu/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();

        $components = new Components();
        $traits = new Traits();
        $options = new options();

        $data['system_id'] = (int) $args['system_id'];
        $data['grouped_under'] = (int) $args['id'];

        if ($data['system_id'] == '0') {
             http_response_code(400);
             $responses = array('status' => 'error', 'message' => 'System ID must be Valid Integer' );
        } else {
            $allComponents = $components->getWhere($data);
            for ($count = 0; $count < sizeof($allComponents); $count++) {
                $allComponents[$count]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['id']));
                $allComponents[$count]['options'] = $options->getOptionTraitsBYComponents($allComponents[$count]['id']);

                $singleComponents = &$allComponents[$count];

                if (strtolower($singleComponents['type']) === strtolower('Interior Color') ||
                    strtolower($singleComponents['type']) === strtolower('Exterior Color')) {
                    $colorComponent = &$singleComponents;
                    $colorComponent['groups'] = $components->getWhere(array("grouped_under" => $colorComponent['id'], 'type' => 'ColorGroup'));
                    foreach ($colorComponent['groups'] as $gkey => $group) {
                        $colorComponent['groups'][$gkey]['traits'] = $traits->getWhere(array("component_id" => $group['id']));
                        $colorComponent['groups'][$gkey]['options'] = $options->getOptionTraitsBYComponents($group['id']);
                        $subGroups = $components->getWhere(array("grouped_under" => $group['id'], 'type' => 'ColorSubGroup'));
                        foreach ($subGroups as $sgkey => $subGroup) {
                            $subGroups[$sgkey]['traits'] = $traits->getWhere(array("component_id" => $subGroup['id']));
                            $subGroups[$sgkey]['options'] = $options->getOptionTraitsBYComponents($subGroup['id']);
                        }
                        $colorComponent['groups'][$gkey]['subGroups'] = $subGroups;
                    }
                }
            }


            $responses = array('status' => 'success', 'response_data' => $allComponents );
        }

        renderJSON($responses);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting a specific Components Details with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),  
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/systems/{system_id}/components/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $traits = new Traits();
        $options = new options();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $data = array('system_id' => $args['system_id'], 'id' => $args['id']);

            $singleComponents = $components->getWhere($data);
      
            if (sizeof($singleComponents) > 0) {
                $singleComponents[0]['traits'] = $traits->getWhere(array("component_id" => $singleComponents[0]['id']));
                $singleComponents[0]['options'] = $options->getOptionTraitsBYComponents($singleComponents[0]['id']);

                if (strtolower($singleComponents[0]['type']) === strtolower('Interior Color') ||
                    strtolower($singleComponents[0]['type']) === strtolower('Exterior Color')) {
                    $colorComponent = &$singleComponents[0];
                    $colorComponent['groups'] = $components->getWhere(array("grouped_under" => $colorComponent['id'], 'type' => 'ColorGroup'));
                    foreach ($colorComponent['groups'] as $gkey => $group) {
                        $colorComponent['groups'][$gkey]['traits'] = $traits->getWhere(array("component_id" => $group['id']));
                        $colorComponent['groups'][$gkey]['options'] = $options->getOptionTraitsBYComponents($group['id']);
                        $subGroups = $components->getWhere(array("grouped_under" => $group['id'], 'type' => 'ColorSubGroup'));
                        foreach ($subGroups as $sgkey => $subGroup) {
                            $subGroups[$sgkey]['traits'] = $traits->getWhere(array("component_id" => $subGroup['id']));
                            $subGroups[$sgkey]['options'] = $options->getOptionTraitsBYComponents($subGroup['id']);
                        }
                        $colorComponent['groups'][$gkey]['subGroups'] = $subGroups;
                    }
                }
                if (strtolower($singleComponents[0]['type']) === strtolower('ColorGroup')) {
                    $group = &$singleComponents[0];
                    $subGroups = $components->getWhere(array("grouped_under" => $group['id'], 'type' => 'ColorSubGroup'));
                    foreach ($subGroups as $sgkey => $subGroup) {
                        $subGroups[$sgkey]['traits'] = $traits->getWhere(array("component_id" => $subGroup['id']));
                        $subGroups[$sgkey]['options'] = $options->getOptionTraitsBYComponents($subGroup['id']);
                    }
                    $group['subGroups'] = $subGroups;
                }

                $responses = array('status' => 'success', 'response_data' => $singleComponents);
                renderJSON($responses);
            } else {
                http_response_code(404);
                $response = array('status' => 'error', 'message' => 'data not found');
                renderJSON($response);
            }
        }
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a specific Components with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),  
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



$app->delete(
    '/systems/{system_id}/components/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'failed', 'message' => 'Parameter ID must be an integer' );
        } else {
            $data = array('system_id' => $args['system_id'], 'id' => $args['id']);
            $singleComponents = $components->getWhere($data);
        
            if (sizeof($singleComponents) > 0) {
                 $deleteComponents = $components->delete($args['id']);
                 $response = $deleteComponents;
            } else {
                 http_response_code(400);
                 $response = array('status' => 'failed', 'message' => 'data not found' );
            }
        }

        renderJSON($response);
    }
);




// @codingStandardsIgnoreStart

 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf', 'grouped_under':2}"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



$app->post(
    '/systems/{system_id}/components',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] =  $args['system_id'];
        $checkData = array(
        'system_id' => $args['system_id'],
        'type' => 'system',
        );

        $components = new Components();

        $singleSystem = $components->getWhere($checkData);

        if (sizeof($singleSystem) == '0') {
             $response = array('status' => 'error', 'message' => 'System ID Not Exists' );
        } else {
            if (intval($config['system_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $result =  $components->create($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        }

    

        renderJSON($response);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/menu",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf', 'menu_id':2}"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->post(
    '/systems/{system_id}/components/menu',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $checkData = array(
        'type' => 'system',
        'id' => $args['system_id'],
        );

        $components = new Components();
        $traits = new Traits();

        $singleSystem = $components->getWhere($checkData);

        if (sizeof($singleSystem) == '0') {
            $response = array('status' => 'error', 'message' => 'System ID Not Exists');
        } else {
            $resultComponents = [];
            foreach ($config as $menu) {
                $menu['system_id'] =  $args['system_id'];
                if (intval($menu['system_id']) == '0') {
                    http_response_code(400);
                    $response = array('status' => 'error', 'message' => 'System ID Must be Valid Integer');
                } elseif (strlen($menu['name']) == '0') {
                    http_response_code(400);
                    $response = array('status' => 'error', 'message' => 'name can not be Empty');
                } else {
                    $menuTraits = $menu['traits'];
                    unset($menu['traits']);

                    if (isset($menu['id'])) {
                        $result = $components->update($menu);
                    } else {
                        $result = $components->create($menu);
                    }

                    $result[0]['traits'] = [];
                    foreach ($menuTraits as $trait) {
                        $trait['component_id'] = $result[0]['id'];
                        if (isset($trait['id'])) {
                            $result[0]['traits'] = $traits->update($trait);
                        } else {
                            $result[0]['traits'] = $traits->create($trait);
                        }
                    }
                    array_push($resultComponents, $result[0]);
                }
            }

            $response = array('status' => 'success', 'response_data' => $resultComponents);
        }
        renderJSON($response);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/menu/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf', 'menu_id':2}"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->post(
    '/systems/{system_id}/components/menu/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] = $args['system_id'];
        $config['grouped_under'] = $args['id'];
        $checkData = array(
        'type' => 'system',
        'id' => $args['system_id'],
        );

        $components = new Components();

        $singleSystem = $components->getWhere($checkData);

        if (sizeof($singleSystem) == '0') {
             $response = array('status' => 'error', 'message' => 'System ID Not Exists', );
        } else {
            if (intval($config['system_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (intval($config['grouped_under']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'Menu ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'name can not be Empty');
            } else {
                $result =  $components->create($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        }

        renderJSON($response);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/components/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating Components Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ), 
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),   
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf', 'grouped_under':2}"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->put(
    '/systems/{system_id}/components/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $checkData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $checkData['system_id'] =  $args['system_id'];
        $config['id'] = $checkData['id'] =  $args['id'];

        $components = new Components();
        $singleComponents = $components->getWhere($checkData);

        if (sizeof($singleComponents) > 0) {
            if (intval($config['system_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  ' name can not be Empty' );
            } else {
                $result = $components->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  

        renderJSON($response);
    }
);




// @codingStandardsIgnoreStart

 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/traits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Traits Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'component_id':'3',name':'21sdf','type':'tesss','value':'21','visible':1},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id and components_id combination is not valid'}",
 *           response=404,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->get(
    '/systems/{system_id}/components/{component_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $components = new Components();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) > 0) {
            $where = array('component_id' => $args['component_id'] );
            $allTraits = $traits->getWhere($where);
            $response = array('status' => 'success', 'response_data' => $allTraits );
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' => 'system_id and components_id combination is not valid' );
        }

        renderJSON($response);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{component_id}/traits/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a specific traits with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Data not Exists'}",
 *           response=404,
 *      ),       
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/systems/{system_id}/components/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        $data = array('component_id' => $args['component_id'], 'id' => $args['id']);
        $singleTraits = $traits->getWhere($data);

        if (sizeof($singleTraits) > 0) {
             renderJSON($singleTraits[0]);
        } else {
            renderJSON(array("status" => "error"));
        }
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{component_id}/traits/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a Components by ID  Where type is systemGroups",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="SQ_traits table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'Data not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer'}",
 *           response=400,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->delete(
    '/systems/{system_id}/components/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $isExists = $traits->checkSystemComponentsTraitsExits($args['system_id'], $args['component_id'], $args['id']);

            if (sizeof($isExists) > 0) {
                $deleteTraits = $traits->delete($args['id']);
                $response = $deleteTraits;
            } else {
                http_response_code(404);
                $response = array('status' => 'error', 'message' => 'Data not Exists');
            }
        }

        renderJSON($response);
    }
);




// @codingStandardsIgnoreStart

 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/traits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding traits Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf','type':'tesss','value':'21','visible':1}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'component_id':'3',name':'21sdf','type':'tesss','value':'21','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id and components_id combination is not valid'}",
 *           response="invalid combination",
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->post(
    '/systems/{system_id}/components/{component_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['component_id'] = $args['component_id'];

        $traits = new Traits();
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'system_id and components_id combination is not valid' );

            renderJSON($response);

            exit;
        }
  
        if (intval($config['component_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
        } elseif (strlen($config['name']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $result =  $traits->create($config);
            $response = array('status' => 'success', 'response_data' =>  $result );
        }

    
        renderJSON($response);
    }
);


// @codingStandardsIgnoreStart

 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/multipleTraits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding traits Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{""id"":2115,""component_id"":2468,""name"":""title"",""type"":""component"",""value"":""Dimension"",""visible"":1}]" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'component_id':'3',name':'21sdf','type':'tesss','value':'21','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id and components_id combination is not valid'}",
 *           response="invalid combination",
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/components/{component_id}/traits/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating traits Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{'name':'21sdf','type':'tesss','value':'21','visible':1}]" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'21sdf', 'component_id':3,'type':'tesss','value':'21','visible':1}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id , component_id and trait_id  combination is not correct'}",
 *           response="invalid combination",
 *      ),          
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/sampleDoorPrice",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting Door Price Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""response_data"": [ { ""id"": 2465, ""system_id"": 2464, ""name"": ""SampleDoorPrice"", ""menu_id"": 0, ""type"": ""SampleDoorPrice"", ""grouped_under"": 0, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [] } ] }",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),        
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


  /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/sampleDoorParts",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting Door Parts Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""response_data"": [ { ""id"": 2465, ""system_id"": 2464, ""name"": ""SampleDoorPrice"", ""menu_id"": 0, ""type"": ""SampleDoorPrice"", ""grouped_under"": 0, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [] } ] }",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),        
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */