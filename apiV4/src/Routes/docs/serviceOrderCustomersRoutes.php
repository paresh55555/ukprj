<?php

 /**
 *    @SWG\Get(
 *      path="/service/orders/{order_id}/doors/{order_id}/customers",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all door customers",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""door_id"": 1, ""customer_id"": 1, ""note"": null, ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": null, ""address_2"": null, ""city"": null, ""state"": null, ""zip"": null } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

 /**
 *    @SWG\Get(
 *      path="/service/orders/{order_id}/doors/{order_id}/customers/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting a door customers",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""door_id"": 1, ""customer_id"": 1, ""note"": null, ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": null, ""address_2"": null, ""city"": null, ""state"": null, ""zip"": null } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

/**
 *    @SWG\Post(
 *      path="/service/orders/{order_id}/doors/{order_id}/customers",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="adding a door customer",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""note"": ""test customers"",""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test""  }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""door_id"": 1, ""customer_id"": 1, ""note"": ""test customers"", ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Put(
 *      path="/service/orders/{order_id}/doors/{order_id}/customers/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="updating door customer",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""note"": ""test customers"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test""  }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""door_id"": 1, ""customer_id"": 1, ""note"": ""test customers"", ""first_name"": ""niloy"", ""last_name"":""B"", ""email"":""niloy.cste@gmail.com"", ""phone"": ""123455677"", ""address_1"": ""test"", ""address_2"": ""test"", ""zip"": ""test"", ""city"": ""test"", ""state"": ""test""  } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



  /**
 *    @SWG\Delete(
 *      path="/service/orders/{order_id}/doors/{order_id}/customers/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Deleting a door customer",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
