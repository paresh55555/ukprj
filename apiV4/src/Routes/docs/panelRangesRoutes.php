<?php

use SalesQuoter\PanelRanges\PanelRanges;

 /**
 *    @SWG\Get(
 *      path="/systems/{id}/panelRanges",
 *      tags={"PanelRanges"},
 *      operationId="getPanelRanges",
 *      summary="Getting All panel ranges Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','data':[{'id':3,'system_id':2,'panels':123,'min_width':122,'max_width':122,'min_height':122,'max_height':122,'modified':'2017-02-23 19:30:18','active':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */


  /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/panelRanges/{id}",
 *      tags={"PanelRanges"},
 *      operationId="getPanelRanges",
 *      summary="Getting a specific panelranges information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','data':[{'id':3,'system_id':2,'panels':123,'min_width':122,'max_width':122,'min_height':122,'max_height':122,'modified':'2017-02-23 19:30:18','active':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */


  /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/panelRanges/{id}",
 *      tags={"PanelRanges"},
 *      operationId="getPanelRanges",
 *      summary="Deleting a specific panelranges information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success'} or {'status':'failed'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */


 /**
 *    @SWG\Post(
 *      path="/systems/{id}/panelRanges",
 *      tags={"PanelRanges"},
 *      operationId="getPanelRanges",
 *      summary="Adding Panel Ranges Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="cname",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description=""
 *      ),
 *
 *     @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'panels':123,'min_width':122,'max_width':122,'min_height':122,'max_height':122}"
 *      ),
 *      @SWG\Response(
 *           description="{'status':'success','data':[{'id':4,'system_id':2,'panels':123,'min_width':122,'max_width':122,'min_height':122,'max_height':122,'modified':'2017-02-26 18:57:02','active':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/panelRanges/{id}",
 *      tags={"PanelRanges"},
 *      operationId="getPanelRanges",
 *      summary="updating Panel Ranges Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="cname",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description=""
 *      ),
 *
 *     @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'panels':123,'min_width':122,'max_width':122,'min_height':122,'max_height':122}"
 *      ),
 *      @SWG\Response(
 *           description="{'status':'success','data':[{'id':4,'system_id':2,'panels':123,'min_width':122,'max_width':122,'min_height':122,'max_height':122,'modified':'2017-02-26 18:57:02','active':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */
