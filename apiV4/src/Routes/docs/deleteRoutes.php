<?php

use SalesQuoter\Delete\Delete;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/delete",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting Deleted Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':['users','fixed','frame','panelRanges','size','percent']}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
 
$app->get(
    '/delete',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $delete = new Delete();
        $responseData = $delete->getAllEndPoint();
        $response = array("status" => "success", "response_data" => $responseData);

        renderJSON($response);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/delete/{table_name}",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting a tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="table_name",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Table Name (what the respones comes in /delete endpoint in response_data array. example (/delete/users, /delete/systems)"
 *      ), 
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':65,'response_data':[{'id':1,'superSales':0,'myOrder':25,'prefix':'A','name':'Alan Rees','pipelinerId':'','email':'alan@panoramicdoors.com','password':'test','type':'salesPerson','discount':20,'extended':'yes','defaults':.... ]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/delete/{table_name}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $delete = new Delete();
        $allGetVars = $request->getQueryParams();
        $items = 10;
        if (isset($allGetVars['page_no'])) {
            $pageNo = $allGetVars['page_no']-1;
        } else {
            $pageNo = 0;
        }

        if (isset($allGetVars['per_page'])) {
            $items = intval($allGetVars['per_page']);
        } else {
            $items = 10;
        }

        $result = $delete->getTableAll($args['table_name'], $items, $pageNo);

        renderJSON($result);
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Delete(
 *      path="/delete/{table_name}/{id}",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Permenantly Deleting From Database",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="table_name",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Table Name (what the respones comes in /delete endpoint in response_data array"
 *      ), 
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Tables auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'result':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->delete(
    '/delete/{table_name}/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $delete = new Delete();
        $tableName = $args['table_name'];
        $id = $args['id'];

        $response = $delete->hardDelete($tableName, $id);

        renderJSON($response);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/delete/users",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting a Users tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':49,'superSales':0,'myOrder':25,'prefix':'qq','name':'atest','pipelinerId':'','email':'atest','password':'tets','type':'salesPerson','discount':45,'extended':null,'defaults':'{\'Discounts\':[{\'amount\':45,\'id\':0,\'name\':\'Company Discount\',\'showOnQuote\':true,\'type\':\'percentage\'},{\'name\':\'\',\'amount\':0,\'type\':\'percent\',\'id\':1,\'showOnQuote\':false}],\'Totals\':{\'code\':\'YZ-0\',\'tax\':{\'amount\':0,\'showOnQuote\':true},\'shipping\':{\'amount\':0,\'showOnQuote\':false},\'installation\':{\'amount\':0,\'showOnQuote\':false},\'cost\':{\'amount\':10000,\'showOnQuote\':false},\'preCost\':10000,\'finalTotal\':{\'amount\':0,\'showOnQuote\':true},\'subTotal\':{\'amount\':0,\'showOnQuote\':true},\'extra\':{\'name\':\'\',\'amount\':0,\'showOnQuote\':false},\'screens\':{\'name\':\'\',\'amount\':0,\'showOnQuote\':false},\'discountTotal\':{\'amount\':0,\'showOnQuote\':false}}}','dealer':8,'phone':null,'sendMessage':0,'message':'','messageSubject':'','leadQuality':'','salesAdmin':'off','anytimeQuoteEdits':0,'active':0}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



/**
 *    @SWG\Get(
 *      path="/delete/fixed",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting a Fixed tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':6,'response_data':[{'module_id':7,'partNumber':'T.B.D.','myOrder':41,'nameOfOption':'Dale Stainless','active':0,'costItem':50,'quantityFormula':'swings','option_type':9,'id':100195,'forCutSheet':1,'totalExempt':0},",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Get(
 *      path="/delete/frame",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting Frame tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':144496,'module_id':1,'option_type':3,'group_id':0,'partNumber':null,'myOrder':204,'nameOfOption':'tesnkllk test','material':null,'cost_meter':10,'cost_paint_meter':0,'cut_size_formula':'WIDTH','number_of_lengths_formula':'1','waste_percent':34,'burnOff':0,'active':0,'forCutSheet':1}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Get(
 *      path="/delete/panelRanges",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting panelRanges tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':4,'response_data':[{'id':70,'panelGroup':1,'panels':1,'low':18,'high':58,'active':0},{'id':71,'panelGroup':1,'panels':1,'low':90,'high':99,'active':0},",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */




/**
 *    @SWG\Get(
 *      path="/delete/glass",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting glass tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':22,'response_data':[{'module_id':9999,'nameOfOption':'Laminated','costSquareMeter':775.002,'height_cut_size_formula':'height','width_cut_size_formula':'width \/ panels','active':0,'option_type':3,'description':'Laminated glass is constructed by placing a thin layer of plastic laminate material over the glass panes. There are usually two layers of glass sandwiched with the laminate to create a substantially sturdier glass. If the glass is broken the laminate keeps the pieces together.','url':'images\/glass\/laminated-glass-121.jpg','myOrder':null,'id':12,'glassOrOption':'option','costPerPanel':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */




/**
 *    @SWG\Get(
 *      path="/delete/percent",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting percent tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'module_id':9999,'nameOfOption':'White Vinyl','percentage':0,'option_type':9,'id':13,'active':0}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



/**
 *    @SWG\Get(
 *      path="/delete/systems",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting systems tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':1,'name':'test222','active':0}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Get(
 *      path="/delete/validations",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting validations tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':1,'trait_id':'2',name':'test222','active':0}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



/**
 *    @SWG\Get(
 *      path="/delete/components",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting components tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':1,'system_id':'2',name':'test222'}]}",
 *           response=200,          
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Get(
 *      path="/delete/traits",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting Traits tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':1,'component_id':'2',name':'test222'}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Get(
 *      path="/delete/permissions",
 *      tags={"delete"},
 *      operationId="getDelete",
 *      summary="Getting SQ_permissions tables Deleted Rows",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="per_page",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Number of Rows/results that you want to show In (if per_page is not given then default value will be 10)"
 *      ), 
 *       @SWG\Parameter(
 *          name="page_no",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Result Number offset (total result set)"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','total_items':1,'response_data':[{'id':2,'permissions':'{\'admin\':\'all\'}','active':0}]}",
 *           response=200,          
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
