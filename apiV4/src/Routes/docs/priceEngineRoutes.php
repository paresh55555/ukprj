<?php

use SalesQuoter\PriceEngine\PriceEngine;
use SalesQuoter\Cache\Cache;

 /**
 *    @SWG\Post(
 *      path="/price",
 *      tags={"price"},
 *      operationId="getPrice",
 *      summary="Calculation of total prices with given traits ",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'system':1, 'components':[{'id':1, 'options': [{'id':17,'value':'test'}]}]}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','price':30}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->post(
    '/price',
    function ($request, $response, $args) {

  
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $totalPrice = 0;

        $cache = new Cache();
        $priceEngine = new PriceEngine();

        $where["system_id"] = $config["system"];
        $where["type"] = "systemPrice";
        $where["current"] = true;

        $componentsArray  = $config['components'];

        $priceDetails = $cache->getWhere($where);
        if (sizeof($priceDetails) > '0') {
             $where["type"] = "systemUX";
             $uxRoutesData =  $cache->getWhere($where)[0]['cachedArray'];

             $details = $priceEngine->calculatePrice($priceDetails[0]['cachedArray'], $componentsArray, $uxRoutesData);
        }

        renderJSON(array('status' => 'success', 'price' =>  $details['price'] ));
    }
);





 /**
 *    @SWG\Post(
 *      path="/price/details",
 *      tags={"price"},
 *      operationId="getPrice",
 *      summary="Calculation details of total prices with given traits ",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'system':1, 'components':[{'id':1, 'options': [{'id':17,'value':'test'}]}]}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'price':30,'details':[{'option':'width','parts':[{'parts':'bd vs eng','costs':[{'costs':'sd','value':30}]}]}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->post(
    '/price/details',
    function ($request, $response, $args) {

  
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $totalPrice = 0;

        $cache = new Cache();
        $priceEngine = new PriceEngine();

        $where["system_id"] = $config["system"];
        $where["type"] = "systemPrice";
        $where["current"] = true;

        $componentsArray  = $config['components'];

        $priceDetails = $cache->getWhere($where);
        if (sizeof($priceDetails) > '0') {
             $where["type"] = "systemUX";
             $uxRoutesData =  $cache->getWhere($where)[0]['cachedArray'];

             $details = $priceEngine->calculatePrice($priceDetails[0]['cachedArray'], $componentsArray, $uxRoutesData);
        }

        renderJSON($details);
    }
);
