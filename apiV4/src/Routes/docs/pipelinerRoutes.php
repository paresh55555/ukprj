<?php

use SalesQuoter\SalesPerson\Crm;
use SalesQuoter\Quotes\Quote;

/**
 *    @SWG\Post(
 *      path="/pipeliner/leads",
 *      tags={"pipeliner"},
 *      operationId="getPipeliner",
 *      summary="Inserting Pipeline Newlead",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'email':'noValidGuestEmail-Cart1484855196164@salesquoter.com','firstName':'t','lastName':'t','phone':'t','prefix':'FF','quoteId':32132,'amount':5563.16,'ownerId':'40721','quality':'new'}"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'ownerId':'12', 'email':'niloy.cste@gmail.com', 'firstName':'niloy', 'lastName':'sd', 'phone':'01974494332', 'quoteNumber':'23', 'amount':'23.90'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/pipeliner/leads',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $ownerId = $config['ownerId'];
        $crm = new Crm($ownerId);

        $opportunityID = $crm->createNewSalesQuoterLead($config);

        renderJSON($opportunityID);
    }
);



/**
 *    @SWG\Put(
 *      path="/pipeliner/leads",
 *      tags={"pipeliner"},
 *      operationId="getPipeliner",
 *      summary="Updating Pipeline Newlead",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'email':'noValidGuestEmail-Cart1484855196164@salesquoter.com','firstName':'t','lastName':'t','phone':'t','prefix':'FF','quoteId':32132,'amount':5563.16,'ownerId':'40721','quality':'new'}"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'ownerId':'12', 'email':'niloy.cste@gmail.com', 'firstName':'niloy', 'lastName':'sd', 'phone':'01974494332', 'quoteNumber':'23', 'amount':'23.90'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->put(
    '/pipeliner/leads',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $ownerId = $config['ownerId'];
        $crm = new Crm($ownerId);


        $result = $crm->updateOpportunity($config);
        $result['status'] = 'success';

        renderJSON($result);
    }
);



/**
 *    @SWG\Delete(
 *      path="/pipeliner/leads",
 *      tags={"pipeliner"},
 *      operationId="getPipeliner",
 *      summary="Deleting Pipeline Newlead",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="32232"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','message':'Opportunity marked as Lost'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */



$app->delete(
    '/pipeliner/leads',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $quoteID = jsonDecodeWithErrorChecking($data);

        if (!$quoteID) {
            renderJSON(
                array(
                'error' => 'Empty quoteID',
                )
            );
        }

        $quote = new Quote();
        $quoteInfo = $quote->getQuoteInfo($quoteID);

        if (empty($quoteInfo)) {
            renderJSON(
                array(
                'error' => "Quote {$quoteID} not found",
                )
            );
        }

        $crm = new Crm($quoteInfo['ownerId']);

        $result = $crm->markAsLostOpportunity($quoteInfo);
        $result['status'] = 'success';

        renderJSON($result);
    }
);
