<?php


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/pdf",
 *      tags={"general"},
 *      operationId="getGeneral",
 *      summary="Downloading General PDF",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *
 *      @SWG\Response(
 *           description="Downloading PDF ",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
 //@codingStandardsIgnoreEnd



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/sendDocument",
 *      tags={"general"},
 *      operationId="getGeneral",
 *      summary="Sending document",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *
 *      @SWG\Response(
 *           description="{'status':'success','message':'Opportunity for quote FF32102 not found'}",
 *           response=200,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd





 /**
 *    @SWG\Post(
 *      path="/uploadComponentImage/{cname}",
 *      tags={"general"},
 *      operationId="getGeneral",
 *      summary="Uploading Component Image ",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="cname",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description=""
 *      ),
 *
 *     @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="base64 encoded Image "
 *      ),
 *      @SWG\Response(
 *           description="{'result':'success', 'imgUrl':'img/cname1.jpg'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */

 

 /**
 *    @SWG\Get(
 *      path="/premadeColors",
 *      tags={"general"},
 *      operationId="getGeneral",
 *      summary="List of all Colors",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="[{'id':0,'name':'Standard Color','colors':[{'id':0,'myOrder':6,'nameOfOption':'Clear Anodized','active':1,'kind':'basic','superSection':'all','hex':null,'url':'anodized-121.png','ralColor':null,'myGroup':5,'location':'extColor'},{'id':1,'myOrder':9,'nameOfOption':'Apollo White','active':1,'kind':'basic','superSection':'all','hex':'#fcffff','url':'','ralColor':null,'myGroup':5,'location':'extColor'},",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */
