<?php
/* siteRoutes added */
use SalesQuoter\Site\Defaults;

 /**
 *    @SWG\Get(
 *      path="/site/defaults",
 *      tags={"sites"},
 *      operationId="getSites",
 *      summary="Getting Sites Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'id':1,'siteName':'testSiteName','SalesEmail':'admin@panoramicdoors.com','hasSalesAdmin':'1','superHasSalesAdmin':1,'showCustomers':1,'showCommunication':0,'showLeadStatus':1,'showFullOrderStatus':1,'siteURL':'pd.rioft.com','location':'US','link1text':'Return to Site','link1':'panoramicdoors.com','link2text':'Contact Us','link2':'panoramicdoors.com\/contact\/','companyName':'Panoramic Doors','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','zip':'92054','cssMainColor':'','cssBackgroundColor':'#33333','logoURL':'\/images\/company\/panoramic-sq-header-logo.png','phone':'760-722-1250','numberofProductsPerLine':2,'cssFont':'#339ddb','cssReverseFont':'#FFFFFF','cssBorder':'#339ddb','cssReverseBackground':'#339ddb','cssBackground':'#FFFFFF','termsURL':'http:\/\/panoramicdoors.com\/terms-and-conditions\/','allowGuest':1,'printLogoURL':'\/images\/company\/pano-logo-blk-blue.png','salesFlow':1,'allowSignUps':0,'guestProductsPerLine':2,'takesCreditCards':0,'productionFlow':1}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */


$app->get(
    '/site/defaults',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $defaults = new Defaults();
        $id = 1;
        $siteDefaults = $defaults->get($id);

        renderJSON($siteDefaults);
    }
);


 /**
 *    @SWG\Put(
 *      path="/site/defaults",
 *      tags={"sites"},
 *      operationId="getSites",
 *      summary="Updating Sites Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="Updating JSON data"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'siteName':'testSiteName','SalesEmail':'admin@panoramicdoors.com','hasSalesAdmin':'1','superHasSalesAdmin':1,'showCustomers':1,'showCommunication':0,'showLeadStatus':1,'showFullOrderStatus':1,'siteURL':'pd.rioft.com','location':'US','link1text':'Return to Site','link1':'panoramicdoors.com','link2text':'Contact Us','link2':'panoramicdoors.com\/contact\/','companyName':'Panoramic Doors','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','zip':'92054','cssMainColor':'','cssBackgroundColor':'#33333','logoURL':'\/images\/company\/panoramic-sq-header-logo.png','phone':'760-722-1250','numberofProductsPerLine':2,'cssFont':'#339ddb','cssReverseFont':'#FFFFFF','cssBorder':'#339ddb','cssReverseBackground':'#339ddb','cssBackground':'#FFFFFF','termsURL':'http:\/\/panoramicdoors.com\/terms-and-conditions\/','allowGuest':1,'printLogoURL':'\/images\/company\/pano-logo-blk-blue.png','salesFlow':1,'allowSignUps':0,'guestProductsPerLine':2,'takesCreditCards':0,'productionFlow':1}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */

$app->put(
    '/site/defaults',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $defaults = new Defaults();
        $data = $request->getBody();

        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = 1;


        $siteDefaults = $defaults->update($config);
    
        renderJSON($siteDefaults);
    }
);
