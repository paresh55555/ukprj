<?php

/*
 * This file is part of the Slim API skeleton package
 *
 * Copyright (c) 2016 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   https://github.com/tuupola/slim-api-skeleton
 *
 */


/**
 *    @SWG\Post(
 *      path="/token",
 *      tags={"token"},
 *      operationId="getToken",
 *      summary="User auth via email and password",
 *
 *
 *      @SWG\Response(
 *           description="{'status':'ok','token':'eysdfnsdjkfadnfaiefsakdjfaskdfaiufsdfnskfasdf......'}",
 *           response=201,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'ok','message':'Wrong Email or Password'}",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


/**
 *    @SWG\Post(
 *      path="/token/login",
 *      tags={"token"},
 *      operationId="getToken",
 *      summary="User auth via email and password",
 *
 *
 *      @SWG\Response(
 *           description="{ 'status': 'ok', 'token': 'eyJ0eXAiOiJKV1QiLCJhbG...........', 'id': 14, 'permissions': { 'admin': 'all' }, 'account': { 'quotes': { 'quote_builder_id': [], 'list_builder_id': [], 'cart_builder_id': [] }, 'orders': { 'order_builder_id': [], 'list_builder_id': [], 'cart_builder_id': [] } } }",
 *           response=201,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'ok','message':'Wrong Email or Password'}",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


/**
 *    @SWG\Post(
 *      path="/token/reset",
 *      tags={"token"},
 *      operationId="getToken",
 *      summary="Request of sending a reset password link via Email",
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="integer",
 *          description="{ ""email"":""niloy.cste@gmail.com""}"
 *      ), 
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""msg"": ""Reset password link sent to your e-mail"" }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""msg"": ""email not found"" }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: POST'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

/**
 *    @SWG\Post(
 *      path="/token/pws",
 *      tags={"token"},
 *      operationId="getToken",
 *      summary="Resetting password with unique code",
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="integer",
 *          description="{ ""password"":""12345"", ""unique_code"":""48aii984jdjksdfakdqh.....""}"
 *      ), 
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""msg"": ""Reset password link sent to your e-mail"" }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""msg"": ""email not found"" }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: POST'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


