<?php


 /**
 *    @SWG\Get(
 *      path="/cart",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Getting All carts",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,name':'21sdf'},{...}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Get(
 *      path="/cart/{id}",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Getting a specific Components types Details with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,name':'21sdf', 'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Delete(
 *      path="/cart/{id}",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Delete a specific Cart with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Cart table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */





 /**
 *    @SWG\Post(
 *      path="/cart",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Adding Cart Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'test'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Put(
 *      path="/cart",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Updating Carts Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'id':1,name':'test'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'test'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name", 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'ID Not Exists' }",
 *           response="Not Existing ID", 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),   
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

