<?php

use SalesQuoter\Windows\WindowSystem;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\CutSheet;

 /**
 *    @SWG\Get(
 *      path="/windowSystem/cutSheet/{id}",
 *      tags={"window"},
 *      operationId="getWindow",
 *      summary="Getting Windows System Details by Id ",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="cutSheet Id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->get(
    '/windowSystem/cutSheet/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $cutSheet = new CutSheet();
        $data = $cutSheet->getCutSheet($args['id']);

        header("Content-Type: application/json", false);
        echo($data['cutSheet']);
    }
);



/**
 *    @SWG\Post(
 *      path="/windowSystem/price",
 *      tags={"window"},
 *      operationId="getWindow",
 *      summary="Inserting Pipeline Newlead",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description=" JSON data"
 *      ),
 *
 *      @SWG\Response(
 *           description="",
 *           response=200,
 *
 *
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/windowSystem/price',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $windowData = new WindowData($config);
        $windowSystem = new WindowSystem($windowData);
        $price = $windowSystem->getPrice();

        renderJSON($price);
    }
);




/**
 *    @SWG\Post(
 *      path="/windowSystem/cutSheet",
 *      tags={"window"},
 *      operationId="getWindow",
 *      summary="/windowSystem/cutSheet",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description=" JSON data"
 *      ),
 *
 *      @SWG\Response(
 *           description="",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/windowSystem/cutSheet',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $windowData = new WindowData($config);
        $windowSystem = new WindowSystem($windowData);
        $cutSheetParts = $windowSystem->getCutSheetParts();

        $cutSheet = new CutSheet();
        $id = $cutSheet->insertCutSheet($cutSheetParts);

        $response = array("cutSheetId" => $id);
        renderJSON($response);
    }
);



/**
 *    @SWG\Post(
 *      path="/windowSystem",
 *      tags={"window"},
 *      operationId="getWindow",
 *      summary="Inserting Window cutSheet",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description=" JSON data"
 *      ),
 *
 *      @SWG\Response(
 *           description="",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/windowSystem',
    function ($request, $response, $args) {
        // Sample log message
        //    $this->logger->info("WindowSystemSelected");

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $windowData = new WindowData($config);

        $windowSystem = new WindowSystem($windowData);

        $cutSheetParts = $windowSystem->getCutSheetParts();
        renderJSON($cutSheetParts);
    }
);
