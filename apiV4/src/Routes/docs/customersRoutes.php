<?php

use SalesQuoter\Customers\Customer;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/customers/{customer_id}",
 *      tags={"customers"},
 *      operationId="getCustomers",
 *      summary="Getting a customers  Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'companyName':'Panoramic Doors','firstName':'','lastName':'','address1':'2515 Industry St.','address2':'','city':'Oceanside','state':'CA','phone':'1234',zip':'92054'}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="Company",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Company"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Company"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/customers/{customer_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $customer = new Customer();
        $result = $customer->get($args['customer_id']);

        renderJSON($result);
    }
);
