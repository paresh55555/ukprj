<?php

 /**
 *    @SWG\Get(
 *      path="/service/jobs/{id}/doors",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all service jobs door",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""job_id"": 1, ""order_id"": 1, ""door_id"": 1, ""warranty_end_date"": ""2018-05-23"", ""door_type_id"": 1, ""traits"": [], ""notes"": [ { ""id"": 1, ""note"": ""test notess"", ""door_id"": 1 } ], ""customers"": [ { ""id"": 1, ""door_id"": 1, ""customer_id"": 1, ""note"": null, ""first_name"": ""sss"", ""last_name"": ""ddd"", ""email"": ""ddd"", ""phone"": ""ddd"", ""address_1"": null, ""address_2"": null, ""city"": null, ""state"": null, ""zip"": null }, { ""id"": 3, ""door_id"": 1, ""customer_id"": 5, ""note"": null, ""first_name"": ""niloy"", ""last_name"": ""B"", ""email"": ""niloy.cste@gmail.com1"", ""phone"": ""1234556771"", ""address_1"": ""test"", ""address_2"": ""test"", ""city"": ""test"", ""state"": ""test"", ""zip"": ""test"" } ] }, { ""id"": 2, ""job_id"": 1, ""order_id"": 1, ""door_id"": 2, ""warranty_end_date"": ""2018-05-23"", ""door_type_id"": 1, ""traits"": [ { ""id"": 1, ""name"": ""Exterior Colorss"", ""trait_value"": ""some test valuess"", ""door_id"": 2 } ], ""notes"": [], ""customers"": [] } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

/**
 *    @SWG\Post(
 *      path="/service/jobs/{id}/doors",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="adding jobs door",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""order_id"": ""2"",""door_id"": ""1"" }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": { ""id"": 3, ""job_id"": 1, ""order_id"": 1, ""door_id"": 3 } }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Put(
 *      path="/service/jobs/{jobs_id}/doors/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="updating jobs door",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ ""order_id"": ""2"",""door_id"": ""1"" }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 1, ""job_number"": ""ORDER1000"", ""start_date"": ""2018-10-17"", ""start_time"": ""11 AM"", ""description"": ""test description"", ""tech"": 1 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



  /**
 *    @SWG\Delete(
 *      path="/service/jobs/{jobs_id}/doors/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Deleting a service jobs door",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="items table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */