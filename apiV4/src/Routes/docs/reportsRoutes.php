<?php


use SalesQuoter\Reports\ReportOrders;
use SalesQuoter\Reports\ReportLeads;
use SalesQuoter\Quotes\Quote;

// @codingStandardsIgnoreStart
/**
 * @SWG\Get(
 *      path="/reports/delivered",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Reports in CSV",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="Comma seperated Values",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



 /**
 *    @SWG\Post(
 *      path="/reports/email/delivered",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Send Reports to Any Email Address",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'from':'admin@salesquoter.com', 'to':'niloy.cste@gmail.com', 'message':'Test Messages', 'subject':'Test Email Messages subject', 'min':'2016-01-01', 'max':'2017-04-14'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','message':'Report Sent to Email'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


// @codingStandardsIgnoreStart
/**
 * @SWG\Get(
 *      path="/reports/zipcodes",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Zipcode Reports in CSV",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="quoted,email,zip
 *                       '2015-01-18 06:00:12',f,f
 *                       '2015-01-19 14:48:50',james.congleton@adph.state.al.us,35660",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *       @SWG\Parameter(
 *          name="min",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Minimum Date"
 *      ),
 *       @SWG\Parameter(
 *          name="max",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Maximum Date"
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
/**
 * @SWG\Get(
 *      path="/reports/leads",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Leads Reports in CSV",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="name,email,zip,leadType,phone,SalesPerson,quoted
 *                       'colleen muncy',muncy.colleen@gmail.com,14221,'Home Owner',7165252132,Barry,'2016-07-27 11:38:39'
 *                        amber,phg@peterhasekglass.com,44256,Contractor,3306672336,Barry,'2016-07-27 11:38:46'
 *                        Theresa,Tmhennigan3@gmail.com,60607,'Home Owner',773-835-0122,Barry,'2016-07-27 11:38:48'",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *       @SWG\Parameter(
 *          name="min",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Minimum Date"
 *      ),
 *       @SWG\Parameter(
 *          name="max",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Maximum Date"
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
/**
 * @SWG\Get(
 *      path="/reports/orders/doorsSold",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Order Reports",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="startDate",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Start Date Range"
 *      ),
 *       @SWG\Parameter(
 *          name="endDate",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="End Date Range"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'StartDate':'2016-01-19','endDate':'2016-12-31','NumberOfDoors':9,'AverageCostPerDoorBeforeDiscount':'$5,460.41','AverageCostPerDoorAfterDiscount':'$5,446.79','AverageCostPerDoorPerOrder':'$5,446.79','AverageDiscountPercent':'0.25%','TotalCostOfDoors':'$49,143.68','TotalDiscounts':'$122.60','TotalCostOfDoorsMinusDiscount':'$49,021.08','TotalOrdersAmount':'$49,021.08','TotalOrders':6,'DoorsPerOrder':'1.50','breakDowns':[{'salesPersonName':'Fracka Future','totalSales':'$42,121.08'},{'salesPersonName':'Alan Rees','totalSales':'$6,900.00'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


/**
 * @SWG\Get(
 *      path="/reports/sales",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Sales Reports",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="startDate",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Start Date Range"
 *      ),
 *       @SWG\Parameter(
 *          name="endDate",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="End Date Range"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status': 'success', 'data': [{ 'quoted': '2016-12-09 08:33:42', 'total': 6636.11, 'job_number': 'H32107', 'number_of_panels': 5 }]...}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */



/**
 * @SWG\Get(
 *      path="/reports/delivered/balance",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Delivered Balance sales reports",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'data': [ { 'Customer': 29133, 'balanceDue': 5225.76, 'total': 5225.76, 'Order_number': 'FF32062', 'first_name': 'f', 'last_name': 'f', 'company_name': 'f' }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */


/**
 * @SWG\Get(
 *      path="/reports/orders/delivered",
 *      tags={"reports"},
 *      operationId="getReports",
 *      summary="Generating Delivered Balance sales reports",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'data': [ { 'Customer': 29133, 'balanceDue': 5225.76, 'total': 5225.76, 'Order_number': 'FF32062', 'first_name': 'f', 'last_name': 'f', 'company_name': 'f' }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 *
 */