<?php


 /**
 *    @SWG\Post(
 *      path="/sendEmail",
 *      tags={"emails"},
 *      operationId="getEmail",
 *      summary="Sending email",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="to",
 *          in="form",
 *          required=true,
 *          type="input",
 *          description="To emails"
 *      ),
 *       @SWG\Parameter(
 *          name="subject",
 *          in="form",
 *          required=true,
 *          type="input",
 *          description="Email subject"
 *      ),
 *       @SWG\Parameter(
 *          name="message",
 *          in="form",
 *          required=true,
 *          type="input",
 *          description="Email message"
 *      ),
 *       @SWG\Parameter(
 *          name="file",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="form file upload not base64 encoded data"
 *      ),    
 *
 *      @SWG\Response(
 *           description="{""status"":""success""}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */