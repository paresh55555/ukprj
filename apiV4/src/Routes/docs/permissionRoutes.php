<?php

use SalesQuoter\Permissions\Permissions;

 /**
 *    @SWG\Get(
 *      path="/permissions",
 *      tags={"permissions"},
 *      operationId="getPermissions",
 *      summary="Getting All Permission Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'permissions':'{\'leads\':\'all\'}'},{'id':2,'permissions':'{\'admin\':\'all\'}'},{'id':4,'permissions':'{\'salesperson\':\'all\'}'}...]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->get(
    '/permissions',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $permissions = new Permissions();
        $allPermissions = $permissions->getAll();

        $response = array('status' => 'success', 'response_data' => $allPermissions );

        renderJSON($response);
    }
);



 /**
 *    @SWG\Get(
 *      path="/permissions/{id}",
 *      tags={"permissions"},
 *      operationId="getPermissions",
 *      summary="Getting a specific Permissions Details with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Permissions table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'permissions':'{\'leads\':\'all\'}'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], message': 'data not found' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/permissions/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $permissions = new Permissions();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
             http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $singlePermissions = $permissions->get($args['id']);
 
            if (sizeof($singlePermissions) > 0) {
                $response = array('status' => 'success', 'response_data' => $singlePermissions  );
            } else {
                http_response_code(404);
                $response = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found' );
            }
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Delete(
 *      path="/permissions/{id}",
 *      tags={"permissions"},
 *      operationId="getPermissions",
 *      summary="Delete a specific Permissions with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Permissions table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */




$app->delete(
    '/permissions/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $permissions = new Permissions();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
             http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $response = $permissions->delete($args['id']);
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Post(
 *      path="/permissions",
 *      tags={"permissions"},
 *      operationId="getPermissions",
 *      summary="Adding Permissions Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{permissions':'{\'admin\':\'all\'}'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':11,'permissions':'{\'admin\':\'all\'}'}]}",
 *           response=200,
 *
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'permissions can not be Empty' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/permissions',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $permissions = new Permissions();

        if (strlen($config['permissions']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'permissions can not be Empty' );
        } else {
            $result =  $permissions->create($config);
            $response = array('status' => 'success', 'response_data' =>  $result );
        }


        renderJSON($response);
    }
);





 /**
 *    @SWG\Put(
 *      path="/permissions",
 *      tags={"permissions"},
 *      operationId="getPermissions",
 *      summary="Updating Permissions Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'id':2,'permissions':'{\'admin\':\'all\'}'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':11,'permissions':'{\'admin\':\'all\'}'}]}",
 *           response=200
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'permissions can not be Empty' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */





$app->put(
    '/permissions',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $permissions = new Permissions();
        $singlePermissions = $permissions->get($config['id']);

        if (sizeof($singlePermissions) > 0) {
            if (strlen($config['permissions']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'permissions can not be Empty' );
            } else {
                $result = $permissions->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  

        renderJSON($response);
    }
);
