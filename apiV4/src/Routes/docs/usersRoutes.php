<?php

use SalesQuoter\Users\User;

/**
 * @SWG\Swagger(
 *      basePath="/api/V4",
 *      host="ev-flow1.rioft.com",
 *      schemes={"https"},
 *      produces={"application/json"},
 *      consumes={"application/json"},
 *      @SWG\Info(
 *          title="Sales Quoter APIV4",
 *          description="REST API",
 *          version="4.0",
 *          termsOfService="terms",
 *          @SWG\License(name="proprietary")
 *      )
 *
 * )
 */



 /**
 *    @SWG\Get(
 *      path="/users",
 *      tags={"users"},
 *      operationId="getUser",
 *      summary="Getting User Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'name':'Alan Rees','email':'alan@panoramicdoors.com','password':'test','type':'salesPerson','discount':20,'companyName':'Panoramic Doors','prefix':'A','salesAdmin':'on','anytimeQuoteEdits':0},{...}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->get(
    '/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
//            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();
        //var_dump($this->token->hasScope(["all"])); exit;
        $user = new User();
        $users = $user->getUsers($allGetVars);


        renderJSON($users);
    }
);




 /**
 *    @SWG\Get(
 *      path="/users/emailAvailable/{email}",
 *      tags={"users"},
 *      operationId="getUser",
 *      summary="E-mail address to check its available or not",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="email",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="E-mail field"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'availableEmail':false}  OR {'availableEmail':true}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/users/emailAvailable/{email}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
     
        $user = new User();
        $result = $user->isEmailAvailable($args['email']);

        renderJSON($result);
    }
);



 /**
 *    @SWG\Get(
 *      path="/users/{id}",
 *      tags={"users"},
 *      operationId="getUser",
 *      summary="Getting a specific User Details Information with user id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="users table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':2,'name':'Robert Myers','email':'robert@panoramicdoors.com','password':'test','type':'salesPerson','discount':20,'companyName':'Panoramic Doors','prefix':'H','salesAdmin':'on','anytimeQuoteEdits':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'data not found' }",
 *           response="Not Exist ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/users/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->getUser($args['id']);

    

        if (count($users) > 1) {
             renderJSON($users);
        } else {
             http_response_code(404);
             renderJSON(array('status' => 'error' , 'message' => 'data not found'));
        }
    }
);




 /**
 *    @SWG\Delete(
 *      path="/users/{id}",
 *      tags={"users"},
 *      operationId="getUser",
 *      summary="Delete a specific User with user id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="users table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */




$app->delete(
    '/users/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->deleteUser($args['id']);

        renderJSON($users);
    }
);




 /**
 *    @SWG\Post(
 *      path="/users",
 *      tags={"users"},
 *      operationId="getUser",
 *      summary="Adding User Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="User Json data "
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':3,'name':'Leads','email':'Leads','password':'test','type':'leads','discount':20,'companyName':'Panoramic Doors','prefix':'SQ','salesAdmin':'on','anytimeQuoteEdits':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $user = new User();
        $result = $user->createUser($config);
        $result['status'] = "success";

        renderJSON($result);
    }
);





 /**
 *    @SWG\Put(
 *      path="/users",
 *      tags={"users"},
 *      operationId="getUser",
 *      summary="Updating User Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="User Json data "
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':3,'name':'Leads','email':'Leads','password':'test','type':'leads','discount':20,'companyName':'Panoramic Doors','prefix':'SQ','salesAdmin':'on','anytimeQuoteEdits':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */





$app->put(
    '/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $result['status']  =  'success';
             renderJSON($result);
        } else {
             http_response_code(404);
             renderJSON(array('status' => 'error' , 'message' => 'data not found'));
        }
    }
);





$app->put(
    '/users/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["id"] = $args["id"];

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $result['status']  =  'success';
             renderJSON($result);
        } else {
             http_response_code(404);
             renderJSON(array('status' => 'error' , 'message' => 'data not found'));
        }
    }
);
