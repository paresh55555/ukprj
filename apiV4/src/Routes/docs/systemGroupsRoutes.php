<?php

use SalesQuoter\Components\Components;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Cache\Cache;
use SalesQuoter\Systems\Systems;

 /**
 *    @SWG\Get(
 *      path="/systemGroups",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Getting All Components Details Information Where type is systeGroup",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'systemGroup','grouped_under''my_order':0,'required':false,'multiple':false,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'ChooseDimensions:','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1},{'id':3,'component_id':4, ...}] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->get(
    '/systemGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getWhere(array("type" => "systemGroup"));

        for ($count = 0; $count < sizeof($allComponents); $count++) {
              $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
              $allComponents[$count]['systems'] = $components->getWhere(array("grouped_under" => $allComponents[$count]['id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }

        renderJSON(array('status' => 'success', 'response_data' =>  $allComponents ));
    }
);




 /**
 *    @SWG\Post(
 *      path="/systemGroups",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Adding Components Details Information  Where type is systeGroup",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test traits 2', 'system_id':1, 'menu_id':0,'grouped_under':0,'my_order':0,'required':false,'multiple':false}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':848,'system_id':1,'name':'test traits','menu_id':0,'type':'systemGroup','grouped_under':0,'my_order':0,'required':false,'multiple':false}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->post(
    '/systemGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config["type"] = "systemGroup";

        $components = new Components();

        if (strlen($config['name']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'name can not be Empty');
        } else {
            $result = $components->create($config);
            $response = array('status' => 'success', 'response_data' => $result);
        }

        renderJSON($response);
    }
);



/**
 *    @SWG\Put(
 *      path="/systemGroups/{id}",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Update Components Details Information  Where type is systeGroup",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Components ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test compoents 2', 'system_id':1, 'menu_id':0,'grouped_under':0,'my_order':0,'required':false,'multiple':false}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':848,'system_id':1,'name':'test traits','menu_id':0,'type':'systemGroup','grouped_under':0,'my_order':0,'required':false,'multiple':false}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->put(
    '/systemGroups/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $checkData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkData['type'] = "systemGroup";
        $checkData['id'] =  $config['id'] =  $args['id'];

        $config["type"] = "systemGroup";

        $components = new Components();
        $singleComponents = $components->getWhere($checkData);

        if (sizeof($singleComponents) > 0) {
            if (intval($config['system_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  ' name can not be Empty' );
            } else {
                $result = $components->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        renderJSON($response);
    }
);


 /**
 *    @SWG\Delete(
 *      path="/systemGroups/{id}",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Delete a Components by ID  Where type is systemGroups",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer'}",
 *           response=400,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->delete(
    '/systemGroups/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $response = $components->delete($args['id']);
        }

        renderJSON($response);
    }
);


 /**
 *    @SWG\Put(
 *      path="/systemGroups/{id}/traits",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Updating traits Information Whose Components type is systemGroup",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table primary key ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'21sdf','type':'tesss','value':'21','visible':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'21sdf', 'component_id':3,'type':'tesss','value':'21','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id , component_id and trait_id  combination is not correct'}",
 *           response="invalid combination",
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->put(
    '/systemGroups/{id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["component_id"] = $args["id"];

        $traits = new Traits();
        $components = new Components();

        $checkData = array("type" => "systemGroup", "id" => $args["id"] );

        $checkExists = $components->getWhere($checkData);

        if (sizeof($checkExists) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'system_id , component_id and trait_id  combination is not correct' );

            renderJSON($response);

            exit;
        }

        $singleTraits = $traits->get($config['id']);

        if (sizeof($singleTraits) > 0) {
            if (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'Name can not be Empty' );
            } else {
                $result = $traits->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Delete(
 *      path="/systemGroups/{component_id}/traits/{id}",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Delete a Traits by ID  Where Component type is systemGroups",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer'}",
 *           response=400,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->delete(
    '/systemGroups/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $components = new Components();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $checkData = array(
            'id' => $args['id'],
            'component_id' => $args['component_id'],
            );
            $isExists = $traits->getWhere($checkData);

            if (sizeof($isExists) > 0) {
                $response = $traits->delete($args['id']);
            } else {
                http_response_code(404);
                $response = array('status' => 'error', 'message' => 'Data not Exists');
            }
        }

        renderJSON($response);
    }
);





 /**
 *    @SWG\Post(
 *      path="/systemGroups/publish",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Adding  systemGroups",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'test systemGroup'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':3,'type':'systemGroup','cachedArray':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'systemGroup','grouped_under':0,'my_order':0,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'Choose Dimensions :','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1}]...]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/systemGroups/publish',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $datas = $request->getBody();
        $config = jsonDecodeWithErrorChecking($datas);
    
        $cache = new Cache();
        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getWhere(array("type" => "systemGroup"));
        $cache->updateAllFalse("systemGroup", "0");

        for ($count = 0; $count < sizeof($allComponents); $count++) {
              $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
              $allComponents[$count]['systems'] = $components->getWhere(array("grouped_under" => $allComponents[$count]['id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }


        $config["cachedArray"] = serialize($allComponents);
        $config["current"] =  true;
        $config["type"] =  "systemGroup";
        $config["system_id"] = "0";

        $result =  $cache->create($config);
        $result[0]['cachedArray'] =  unserialize($result[0]['cachedArray']);

        $response = array('status' => 'success', 'response_data' =>  $result );

        renderJSON($response);
    }
);



 /**
 *    @SWG\Get(
 *      path="/systemGroups/ux",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Getting SystemGroups UX Info",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':4,'system_id':1,'name':'Dimensions','menu_id':1,'type':'systemGroup','grouped_under':0,'my_order':0,'traits':[{'id':1,'component_id':4,'name':'title','type':'component','value':'Choose Dimensions :','visible':1},{'id':2,'component_id':4,'name':'titleBackgroundColor','type':'component','value':'','visible':1},{'id':3,'component_id':4,'name':'widthUnitOfMeasure','type':'component','value':'mm','visible':1}]..]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/systemGroups/ux',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $cache = new Cache();
        $params = array( "type" => "systemGroup" , "current" => true , "system_id" => 0);
        $allSystems = $cache->getWhere($params)[0]['cachedArray'];

        $allSystems = unserialize($allSystems);

        $response = array('status' => 'success', 'response_data' => $allSystems );

        renderJSON($response);
    }
);



 /**
 *    @SWG\Post(
 *      path="/systemGroup/{id}/images",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Uploading Components Image where type is systemGroups",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table primary key ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="file",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="base64 encoded image like data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAsIAAAPACAIAAACwz4gxAAA" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'result':'success','imgUrl':'api/V4/images/systemGroups/1/systems_1.jpg'}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Empty system ID'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'File is written in desired folder. Pls check File Permissions'}",
 *           response=401,
 *      ),        
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systemGroups/preMade",
 *      tags={"systemGroups"},
 *      operationId="getSystemsGroups",
 *      summary="Getting Premade systemGroups Details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *
 *      @SWG\Response(
 *           description=" ""status"": ""success"", ""response_data"": [ { ""id"": 1, ""name"": ""PreMadeDoorSystem"", ""system_id"": 0, ""active"": 1, ""menu_id"": 0, ""type"": ""systemGroup"", ""grouped_under"": 0, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [ { ""id"": 1, ""component_id"": 1, ""name"": ""title"", ""type"": ""component"", ""value"": ""System Builder for Doors"", ""visible"": 1 } ], ""systems"": [ { ""id"": 2322, ""system_id"": 2322, ""name"": ""Door System"", ""menu_id"": 0, ""type"": ""system"", ""grouped_under"": 1, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [ { ""id"": 1946, ""component_id"": 2322, ""name"": ""imgUrl"", ""type"": ""component"", ""value"": ""api/V4/images/system/2322/0/0/1946_1.png"", ""visible"": 1 }, { ""id"": 1947, ""component_id"": 2322, ""name"": ""title"", ""type"": ""component"", ""value"": ""Signature Aluminum"", ""visible"": 1 }, { ""id"": 1951, ""component_id"": 2322, ""name"": ""note"", ""type"": ""component"", ""value"": ""Width 45 feet Height 10 feet"", ""visible"": 1 } ] }, { ""id"": 2351, ""system_id"": 2351, ""name"": ""Door System"", ""menu_id"": 0, ""type"": ""system"", ""grouped_under"": 1, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [ { ""id"": 1997, ""component_id"": 2351, ""name"": ""imgUrl"", ""type"": """", ""value"": ""tmp"", ""visible"": 1 }, { ""id"": 1998, ""component_id"": 2351, ""name"": ""title"", ""type"": ""component"", ""value"": ""Door System"", ""visible"": 1 } ] } ] }, { ""id"": 6, ""name"": ""PreMadeWindowSystem"", ""system_id"": 0, ""active"": 1, ""menu_id"": 0, ""type"": ""systemGroup"", ""grouped_under"": 0, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [ { ""id"": 7, ""component_id"": 6, ""name"": ""title"", ""type"": ""component"", ""value"": ""System Builder for Windows"", ""visible"": 1 } ], ""systems"": [ { ""id"": 2332, ""system_id"": 2332, ""name"": ""Window System"", ""menu_id"": 0, ""type"": ""system"", ""grouped_under"": 6, ""my_order"": 0, ""required"": 0, ""multiple"": 0, ""traits"": [ { ""id"": 1972, ""component_id"": 2332, ""name"": ""imgUrl"", ""type"": """", ""value"": ""tmp"", ""visible"": 1 }, { ""id"": 1973, ""component_id"": 2332, ""name"": ""title"", ""type"": ""component"", ""value"": ""Window System new 11.09.2017"", ""visible"": 1 } ] } ] } ] }",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),        
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



