<?php


 /**
 *    @SWG\Get(
 *      path="/carts/{item_id}",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Getting All carts Info",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 1, 'item_id': 1, 'component_values': { 'name': 'Dimensions2', 'amount': '-0.45' }, 'system_id': 45, 'quantity': 1, 'sub_total': '45.50', 'total': '50.00', 'price_changes': [ { 'id': 1, 'cart_id': 1, 'kind': '', 'amount': '45.0000', 'name': 'Dimensions2', 'show_on_item': 0, 'active': 1 }, { 'id': 2, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0, 'active': 1 }, { 'id': 3, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0, 'active': 1 }, { 'id': 4, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0, 'active': 1 } ] }, { 'id': 2, 'item_id': 1, 'component_values': { 'name': 'Dimensions2', 'amount': '-0.45' }, 'system_id': 45, 'quantity': 1, 'sub_total': '45.50', 'total': '50.00', 'price_changes': [] } ]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Get(
 *      path="/carts/{item_id}/{id}",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Getting a specific Carts Price",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="SQ_carts table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Get(
 *      path="/carts/{item_id}/{id}/histroy?length=10",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Getting a specific Carts Price",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="SQ_carts table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="[ { ""id"": 3, ""kind"": ""cart"", ""kind_id"": 12, ""event"": ""PUT"", ""raw_data"": { ""system_id"": ""45"", ""component_values"": { ""name"": ""Dimensions2"", ""amount"": ""-0.45"" }, ""quantity"": 1, ""total"": 50, ""sub_total"": 45.5, ""item_id"": ""1"", ""id"": ""12"" }, ""result"": { ""status"": ""success"", ""response_data"": [ { ""id"": 12, ""item_id"": 1, ""component_values"": ""{\""name\"":\""Dimensions2\"",\""amount\"":\""-0.45\""}"", ""system_id"": 45, ""quantity"": 1, ""sub_total"": ""45.50"", ""total"": ""50.00"" } ] }, ""date"": ""2017-10-16 04:59:28"" }, { ""id"": 2, ""kind"": ""cart"", ""kind_id"": 12, ""event"": ""POST"", ""raw_data"": { ""system_id"": ""45"", ""component_values"": { ""name"": ""Dimensions2"", ""amount"": ""-0.45"" }, ""quantity"": 1, ""total"": 50, ""sub_total"": 45.5, ""item_id"": ""1"" }, ""result"": { ""status"": ""success"", ""response_data"": [ { ""id"": 12, ""item_id"": 1, ""component_values"": ""{\""name\"":\""Dimensions2\"",\""amount\"":\""-0.45\""}"", ""system_id"": 45, ""quantity"": 1, ""sub_total"": ""45.50"", ""total"": ""50.00"" } ] }, ""date"": ""2017-10-16 04:59:13"" } ]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Delete(
 *      path="/carts/{item_id}/{id}",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Delete a specific Carts with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Cart table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */





 /**
 *    @SWG\Post(
 *      path="/carts/{item_id}",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Adding Cart Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'system_id': '45', 'component_values':[{'name':'Dimensions2', 'amount': '-0.45'}], 'quantity': 1, 'total': 50, 'sub_total': 45.50 }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 3, 'item_id': 1, 'component_values': '{\'name\':\'Dimensions2\',\'amount\':\'-0.45\'}', 'system_id': 45, 'quantity': 1, 'sub_total': '45.50', 'total': '50.00' } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Put(
 *      path="/carts/{item_id}/{id}",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Updating Carts Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'system_id': '45', 'component_values':[{'name':'Dimensions2', 'amount': '-0.45'}], 'quantity': 1, 'total': 50, 'sub_total': 45.50 }" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 3, 'item_id': 1, 'component_values': '{\'name\':\'Dimensions2\',\'amount\':\'-0.45\'}', 'system_id': 45, 'quantity': 1, 'sub_total': '45.50', 'total': '50.00' } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),  
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

