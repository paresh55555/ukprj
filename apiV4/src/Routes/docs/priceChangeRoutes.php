<?php


 /**
 *    @SWG\Get(
 *      path="/priceChange/{type}/{type_id}",
 *      tags={"priceChange"},
 *      operationId="priceChange",
 *      summary="Getting All Price Changes Info",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 5, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Get(
 *      path="/priceChange/{type}/{type_id}/{id}",
 *      tags={"priceChange"},
 *      operationId="priceChange",
 *      summary="Getting a specific Price Change",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="SQ_carts table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 5, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Get(
 *      path="/priceChange/{type}/{type_id}/{id}/histroy?length=10",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Getting a History Carts Price",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="SQ_items table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="[ { ""id"": 5, ""kind"": ""price_change_item"", ""kind_id"": 1, ""event"": ""PUT"", ""raw_data"": { ""id"": ""1"", ""item_id"": ""1"", ""kind"": """", ""amount"": ""45.00"", ""name"": ""Dimensions2"", ""show_on_item"": 1 }, ""result"": { ""status"": ""success"", ""response_data"": [ { ""id"": 1, ""item_id"": 1, ""kind"": """", ""amount"": ""45.0000"", ""name"": ""Dimensions2"", ""show_on_item"": 1 } ] }, ""date"": ""2017-10-16 05:10:21"" } ]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */




 /**
 *    @SWG\Delete(
 *      path="/priceChange/{type}/{type_id}/{id}",
 *      tags={"priceChange"},
 *      operationId="priceChange",
 *      summary="Delete a specific Carts with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Cart table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Post(
 *      path="/priceChange/{type}/{type_id}",
 *      tags={"priceChange"},
 *      operationId="priceChange",
 *      summary="Adding Cart Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'Dimensions2', 'amount': '-0.45', 'kind': 'percentages'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 5, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Put(
 *      path="/priceChange/{type}/{type_id}/{id}",
 *      tags={"priceChange"},
 *      operationId="priceChange",
 *      summary="Updating Price Changes Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'Dimensions2', 'amount': '-0.45', 'kind': 'percentages'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'response_data': [ { 'id': 5, 'cart_id': 1, 'kind': '', 'amount': '-0.4500', 'name': 'Dimensions2', 'show_on_item': 0 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),  
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

