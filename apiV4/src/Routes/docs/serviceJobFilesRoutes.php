<?php

 /**
 *    @SWG\Get(
 *      path="/service/jobs/{job_id}/file",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all jobs images/videos",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 3, ""job_id"": 1, ""url"": ""/var/www/dev-demo1.rioft.com/images/orders/1/3.pdf"", ""file_name"": ""test.png"" }, { ""id"": 1, ""job_id"": 1, ""url"": ""api/V4/images/orders/1/1.jpeg"", ""file_name"": ""test.png"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Post(
 *      path="/service/jobs/{job_id}/file",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="creating new job images/videos",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="file",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="form file upload not base64 encoded data"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""id"": 3, ""job_id"": 1,  ""url"": ""https://s3-us-west-2.amazonaws.com/bucket-name/salesquoter/jobs/1/order-file-upload.png"" }",
 *           response=200,
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



/**
 *    @SWG\Delete(
 *      path="/service/jobs/{job_id}/file/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="delete  job images/videos",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success""}",
 *           response=200,
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

