<?php


 /**
 *    @SWG\Get(
 *      path="/accounts",
 *      tags={"accounts"},
 *      operationId="getAccounts",
 *      summary="Getting all accounts",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 3, ""name"": ""sales"" }, { ""id"": 4, ""name"": ""Production"" }, { ""id"": 8, ""name"": ""sales"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Get(
 *      path="/accounts/builders",
 *      tags={"accounts"},
 *      operationId="getAccounts",
 *      summary="Getting a accounts builder details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""quotes"": { ""lists_builders"": [ { ""id"": 53, ""name"": ""Sales "" }, { ""id"": 54, ""name"": ""Production"" } ], ""cart_builders"": [ { ""id"": 1, ""name"": ""Default"" }, { ""id"": 2, ""name"": ""Special"" } ], ""quote_builders"": [ { ""id"": 99, ""name"": ""Sales List"" }, { ""id"": 100, ""name"": ""Accounting List"" }, { ""id"": 104, ""name"": ""Production List"" } ] }, ""orders"": { ""lists_builders"": [ { ""id"": 52, ""name"": ""Sales"" }, { ""id"": 55, ""name"": ""Production"" } ], ""cart_builders"": [ { ""id"": 1, ""name"": ""Default"" }, { ""id"": 2, ""name"": ""Special"" } ], ""order_builders"": [ { ""id"": 105, ""name"": ""Sales List"" }, { ""id"": 106, ""name"": ""Accouting List"" } ] } }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Get(
 *      path="/accounts/{id}",
 *      tags={"accounts"},
 *      operationId="getAccounts",
 *      summary="Getting a accounts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""name"": ""sales"", ""id"": 3, ""quotes"": { ""quote_builder_id"": 10, ""list_builder_id"": 2, ""cart_builder_id"": 4 }, ""orders"": { ""order_builder_id"": 10, ""list_builder_id"": 20, ""cart_builder_id"": 10 } } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Post(
 *      path="/accounts",
 *      tags={"accounts"},
 *      operationId="getAccounts",
 *      summary="Adding a new accounts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{""name"":""1234"",""id"":9,""quotes"":{""quote_builder_id"":100,""list_builder_id"":53,""cart_builder_id"":1},""orders"":{""order_builder_id"":106,""list_builder_id"":52,""cart_builder_id"":1}}"
 *      ),
 *
 *      @SWG\Response(
 *           description="{""status"":""success"",""data"":[{""name"":""1234"",""id"":9,""quotes"":{""quote_builder_id"":100,""list_builder_id"":53,""cart_builder_id"":1},""orders"":{""order_builder_id"":106,""list_builder_id"":52,""cart_builder_id"":1}}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Put(
 *      path="/accounts/{id}",
 *      tags={"accounts"},
 *      operationId="getAccounts",
 *      summary="Updating a accounts",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{""name"":""1234"",""id"":9,""quotes"":{""quote_builder_id"":100,""list_builder_id"":53,""cart_builder_id"":1},""orders"":{""order_builder_id"":106,""list_builder_id"":52,""cart_builder_id"":1}}"
 *      ),
 *
 *      @SWG\Response(
 *           description="{""status"":""success"",""data"":[{""name"":""1234"",""id"":9,""quotes"":{""quote_builder_id"":100,""list_builder_id"":53,""cart_builder_id"":1},""orders"":{""order_builder_id"":106,""list_builder_id"":52,""cart_builder_id"":1}}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
