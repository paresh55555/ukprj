<?php

use SalesQuoter\Costs\OptionPartsCosts;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/costs",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Costs Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *       @SWG\Parameter(
 *          name="part_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Part ID"
 *      ),  
 *  
 *  
 * 
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1, 'part_id':'2', 'name':'testsss','formula':'test'},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and  Option_id  combination is not Correct' }",
 *           response=400,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
 //@codingStandardsIgnoreEnds


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/costs/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting a specific Costs Details with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *       @SWG\Parameter(
 *          name="part_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Part ID"
 *      ),  
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),  
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'part_id':1,'name':'testsss','formula':'test'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response=404,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
 //@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/costs/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a specific Costs with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),      
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/costs",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Costs Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'testsss','part_id':'2','formula':'test'}" 
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1, 'part_id':1, 'name':'testsss','formula':'test'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
 //@codingStandardsIgnoreEnd




// @codingStandardsIgnoreStart

 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/costs/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating Costs Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'testsss','part_id':'2','formula':'test'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'part_id':1,'name':'testsss','formula':'test'}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),      
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
