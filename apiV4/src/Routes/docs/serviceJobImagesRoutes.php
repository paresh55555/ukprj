<?php

 /**
 *    @SWG\Get(
 *      path="/service/jobs/{job_id}/images",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Getting all jobs images",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""data"": [ { ""id"": 3, ""order_id"": 1, ""type"": ""2"", ""url"": ""/var/www/dev-demo1.rioft.com/images/orders/1/3.pdf"" }, { ""id"": 1, ""order_id"": 1, ""url"": ""api/V4/images/orders/1/1.jpeg"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Post(
 *      path="/service/jobs/{job_id}/images",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="creating new job images",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="docs",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="base64 encoded image like data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA....."
 *      ),
 *       @SWG\Parameter(
 *          name="width",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="width of image"
 *      ),
 *       @SWG\Parameter(
 *          name="height",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="height of image"
 *      ),
 *       @SWG\Parameter(
 *          name="fileName",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="file name"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""id"": 3, ""job_id"": 1,  ""url"": ""ap/V4/images/orders/1/3.pdf"" }",
 *           response=200,
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


/**
 *    @SWG\Put(
 *      path="/service/jobs/{job_id}/images/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="creating new job images",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="docs",
 *          in="form",
 *          required=true,
 *          type="file",
 *          description="base64 encoded image like data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA....."
 *      ),
 *       @SWG\Parameter(
 *          name="width",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="width of image"
 *      ),
 *       @SWG\Parameter(
 *          name="height",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="height of image"
 *      ),
 *       @SWG\Parameter(
 *          name="fileName",
 *          in="form",
 *          required=true,
 *          type="integer",
 *          description="file name"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""id"": 3, ""job_id"": 1,  ""url"": ""ap/V4/images/orders/1/3.pdf"" }",
 *           response=200,
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


  /**
 *    @SWG\Delete(
 *      path="/service/jobs/{job_id}/images/{id}",
 *      tags={"serviceOrder"},
 *      operationId="getServiceOrder",
 *      summary="Deleting a jobs images",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="job_id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="job id"
 *      ), 
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="order id"
 *      ),  
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */