<?php

use SalesQuoter\Options\Options;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Traits\OptionTraits;
use SalesQuoter\Components\Components;

 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Options Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':1,'component_id':10,'name':'tests','allowDeleted':1},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id or component_id combination is not correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->get(
    '/systems/{system_id}/components/{component_id}/options',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $options = new Options();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
             exit();
        }

        $data = array();
        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];

        $allOptions = $options->getWhere($data);

        $returnData = array();
        foreach ($allOptions as $options) {
             $return['name'] = $options['name'];
             $return['id'] = $options['id'];
             $return['traits'] = $options->getOptionTratis($options['id']);
             $returnData[] = $return;
        }


        $response = array('status' => 'success', 'response_data' => $returnData );

        renderJSON($response);
    }
);



 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting a specific Components types Details with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':1,'component_id':10,'name':'tests','allowDeleted':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'system_id or component_id combination is not correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/systems/{system_id}/components/{component_id}/options/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $options = new Options();
        $intVal = intval($args['id']);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
             exit();
        }



        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $data['system_id'] = $args['system_id'];
            $data['component_id'] = $args['component_id'];
            $data['id']  = $args['id'];

            $singleOptions = $options->getWhere($data)[0];
      
            if (sizeof($singleOptions) > 0) {
                 $singleOptions['status'] = 'success';
                 renderJSON($singleOptions);
            } else {
                 http_response_code(404);
                 $response = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found');
                 renderJSON($response);
            }
        }
    }
);




 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{component_id}/options/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a specific Options with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Components table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */




$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $options = new Options();
        $intVal = intval($args['id']);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
             exit();
        }

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $deleteOptions = $options->delete($args['id']);
            $response = $deleteOptions;
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/options",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Options Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'tests','allowDeleted':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':1,'component_id':10,'name':'tests','allowDeleted':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/systems/{system_id}/components/{component_id}/options',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];

        $options = new Options();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
             exit();
        }

        if (strlen($config['name']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'name can not be Empty' );
        } elseif (intval($config['system_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($config['component_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            $result =  $options->create($config);
            $response = array('status' => 'success', 'response_data' =>  $result );
        }
    

        renderJSON($response);
    }
);


 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/premadeColors",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Options Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{'id':4,'myOrder':7,'nameOfOption':'Gray','active':1,'kind':'arch','superSection':'all','hex':'#97968E','url':'','ralColor':null,'myGroup':2,'location':'extColor'},{'id':5,'myOrder':12,'nameOfOption':'Black','active':1,'kind':'arch','superSection':'all','hex':'#0B0B0B','url':'','ralColor':null,'myGroup':2,'location':'extColor'}]"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="[{'id':4,'myOrder':7,'nameOfOption':'Gray','active':1,'kind':'arch','superSection':'all','hex':'#97968E','url':'','ralColor':null,'myGroup':2,'location':'extColor'},{'id':5,'myOrder':12,'nameOfOption':'Black','active':1,'kind':'arch','superSection':'all','hex':'#0B0B0B','url':'','ralColor':null,'myGroup':2,'location':'extColor'}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/systems/{system_id}/components/{component_id}/premadeColors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $colors = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
        }

        if (intval($args['system_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($args['component_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            saveColors($colors, $args['system_id'], $args['component_id']);
            $response = array('status' => 'success', 'response_data' => []);
        }

        renderJSON($response);
    }
);


 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/premadeGroups",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding premadeGroups Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{'id':4,'name':'Foils','colors':[{'id':226,'nameOfOption':'Golden Oak Foil','active':1,'hex':null,'url':'img/premadeColors/foilsStandard/GoldenOakFoil.jpg','ralColor':null},{'id':227,'nameOfOption':'Rosewood Foil','active':1,'hex':null,'url':'img/premadeColors/foilsStandard/RosewoodFoil.jpg','ralColor':null}]}]"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="[{'id':4,'name':'Foils','colors':[{'id':226,'nameOfOption':'Golden Oak Foil','active':1,'hex':null,'url':'img/premadeColors/foilsStandard/GoldenOakFoil.jpg','ralColor':null},{'id':227,'nameOfOption':'Rosewood Foil','active':1,'hex':null,'url':'img/premadeColors/foilsStandard/RosewoodFoil.jpg','ralColor':null}]}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->post(
    '/systems/{system_id}/components/{component_id}/premadeGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $groups = jsonDecodeWithErrorChecking($data);

        $components = new Components();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
        }

        if (intval($args['system_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($args['component_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            foreach ($groups as $group) {
                $groupConfig = [
                'name' => 'ColorGroup',
                'type' => 'ColorGroup',
                'system_id' => $args['system_id'],
                'grouped_under' => $args['component_id'],
                ];

                $groupResult = $components->create($groupConfig);
                $groupID = $groupResult[0]['id'];
                foreach ($group['traits'] as $trait) {
                    $traits->create(
                        [
                        'name' => $trait['name'],
                        'value' => $trait['value'],
                        'component_id' => $groupID,
                        'type' => 'component',
                        ]
                    );
                }

                if ($group['colors'] && !empty($group['colors'])) {
                    //save colors for group
                    saveColors($group['colors'], $args['system_id'], $groupID);
                } elseif ($group['subGroups']) {
                    // save subgroups with colors
                    saveSubGroups($group['subGroups'], $args['system_id'], $groupID);
                }
            }
            $response = array('status' => 'success', 'response_data' => []);
        }

        renderJSON($response);
    }
);


 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/premadeSubGroups",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding premadeSubGroups Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{'id':2,'name':'Red','imgUrl':'img/premadeSubGroups/red.svg','colors':[{'kind':'ral','location':'extColor','colorGroupKind':'ral','superSection':'Red','nameOfOption':'Flame red','hex':'#AF2B1E','type':'ral','ralColor':'RAL 3000','id':63},{'kind':'ral','location':'extColor','colorGroupKind':'ral','superSection':'Red','nameOfOption':'Signal red','hex':'#A52019','type':'ral','ralColor':'RAL 3001','id':64},"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="[{'id':2,'name':'Red','imgUrl':'img/premadeSubGroups/red.svg','colors':[{'kind':'ral','location':'extColor','colorGroupKind':'ral','superSection':'Red','nameOfOption':'Flame red','hex':'#AF2B1E','type':'ral','ralColor':'RAL 3000','id':63},{'kind':'ral','location':'extColor','colorGroupKind':'ral','superSection':'Red','nameOfOption':'Signal red','hex':'#A52019','type':'ral','ralColor':'RAL 3001','id':64},",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->post(
    '/systems/{system_id}/components/{component_id}/premadeSubGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $subGroups = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) == '0') {
             http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
        }

        if (intval($args['system_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($args['component_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            // save subgroups with colors
            saveSubGroups($subGroups, $args['system_id'], $args['component_id']);

            $response = array('status' => 'success', 'response_data' => []);
        }

        renderJSON($response);
    }
);

 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/components/{component_id}/options/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating Options Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'tests','allowDeleted':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'system_id':1,'component_id':10,'name':'tests','allowDeleted':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */





$app->put(
    '/systems/{system_id}/components/{component_id}/options/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];

        $options = new Options();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
              http_response_code(400);
             renderJSON(array( "status" => "error",  "message" => "system_id or component_id combination is not correct" ));
             exit();
        }


        $singleOptions = $options->get($config['id']);

        if (sizeof($singleOptions) > 0) {
            if (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } elseif (intval($config['system_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
            } elseif (intval($config['component_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
            } else {
                $result = $options->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        renderJSON($response);
    }
);
