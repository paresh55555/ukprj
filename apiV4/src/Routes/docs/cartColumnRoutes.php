<?php

 /**
 *    @SWG\Delete(
 *      path="/carts/{layout_id}/layouts/{layout_group_id}/column/{id}",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Delete a specific cart column with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Layout column table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Post(
 *      path="/carts/{layout_id}/layouts/{id}/column",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Adding a layout Column Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'alignment':'top'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ status': 'success', 'response_data': [ { 'id': 4, 'layout_group_id': 1, 'alignment': 'top', 'order': 4 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),     
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Put(
 *      path="/carts/{layout_id}/layouts/{layout_group_id}/column/{id}",
 *      tags={"cart"},
 *      operationId="getCart",
 *      summary="Updating Layout Column Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'alignment':'top','order':2}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ status': 'success', 'response_data': [ { 'id': 4, 'layout_group_id': 1, 'alignment': 'top', 'order': 4 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'name can not be Empty' }",
 *           response="empty name", 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'ID Not Exists' }",
 *           response="Not Existing ID", 
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),   
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */

