<?php

use SalesQuoter\Addresses\Address;
use SalesQuoter\Quotes\Quote;
use SalesQuoter\Contracts\Contracts;

 /**
 *    @SWG\Get(
 *      path="/quotes/{quote_id}/addresses",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Getting all Quotes details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="quote_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'quote_id':12121212,'sales_person_id':2,'shipping_address1':'132','shipping_address2':'er','shipping_city':'2','shipping_state':'werrwer','shipping_zip':'werwe','shipping_phone':'wer','shipping_contact':'wer','billing_address1':'wes','billing_address2':'rwer','billing_city':'wer','billing_state':'werwer','billing_zip':'wer','billing_phone':'wersdw','billing_contact':'wesdr'}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */



$app->get(
    '/quotes/{quote_id}/addresses',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $config = array ('quote_id' => $args['quote_id']);
        $address = new Address();
        $allAddress = $address->getAll($config);

        renderJSON($allAddress);
    }
);




 /**
 *    @SWG\Put(
 *      path="/quotes/{quote_id}/addresses",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Updating a Specific Quotes and If a Quote is not Exists, then It will create a new one",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="quote_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="Quotes Sending JSON data"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'quote_id':12121212,'sales_person_id':2,'shipping_address1':'132','shipping_address2':'er','shipping_city':'2','shipping_state':'werrwer','shipping_zip':'werwe','shipping_phone':'wer','shipping_contact':'wer','billing_address1':'wes','billing_address2':'rwer','billing_city':'wer','billing_state':'werwer','billing_zip':'wer','billing_phone':'wersdw','billing_contact':'wesdr'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sales_person_id must be Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */



 /**
 *    @SWG\Put(
 *      path="/quotes/{quote_id}/addresses",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="If a Quote is not Exists, then It will create a new one",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="quote_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'sales_person_id':2,'shipping_address1':'132','shipping_address2':'er','shipping_city':'2','shipping_state':'werrwer','shipping_zip':'werwe','shipping_phone':'wer','shipping_contact':'wer','billing_address1':'wes','billing_address2':'rwer','billing_city':'wer','billing_state':'werwer','billing_zip':'wer','billing_phone':'wersdw','billing_contact':'wesdr'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'quote_id':12121212,'sales_person_id':2,'shipping_address1':'132','shipping_address2':'er','shipping_city':'2','shipping_state':'werrwer','shipping_zip':'werwe','shipping_phone':'wer','shipping_contact':'wer','billing_address1':'wes','billing_address2':'rwer','billing_city':'wer','billing_state':'werwer','billing_zip':'wer','billing_phone':'wersdw','billing_contact':'wesdr'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sending Parameter Can not Be Empty' }",
 *           response="empty parameter",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sales_person_id must be Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->put(
    '/quotes/{quote_id}/addresses',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();

        $response = array();
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if (json_last_error()) {
            echo json_last_error_msg();
        }
        $config['quote_id'] =  $args['quote_id'];

        $address = new Address();
        $checkEmptyParameter = $address->checkEmptyParameter($config);

        if ($checkEmptyParameter['isEmpty']) {
             http_response_code(400);
             $response['status'] = 'error';
             $response['message'] =  $checkEmptyParameter['message'];
        } elseif (empty($config['sales_person_id']) ||  $config['sales_person_id'] == '0') {
             http_response_code(400);
             $response['status'] = 'error';
             $response['message'] =  'sales_person_id must be Integer';
        } else {
            $response = $address->update($config)[0];
            $response['status'] = 'success';

            if (!empty($allGetVars['addToQuote'])) {
                $quote = new Quote();
                $quoteConfig = array("id" => $args['quote_id'], "Customer" => $allGetVars['customer_id']);
                $quote->update($quoteConfig);
            }
        }

    

        renderJSON($response);
    }
);





 /**
 *    @SWG\Post(
 *      path="/quotes/{quote_id}/addresses",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Adding a Specific Quotes",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="quote_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *      @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="Quotes Sending JSON data"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'id':1,'quote_id':12121212,'sales_person_id':2,'shipping_address1':'132','shipping_address2':'er','shipping_city':'2','shipping_state':'werrwer','shipping_zip':'werwe','shipping_phone':'wer','shipping_contact':'wer','billing_address1':'wes','billing_address2':'rwer','billing_city':'wer','billing_state':'werwer','billing_zip':'wer','billing_phone':'wersdw','billing_contact':'wesdr'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sending Parameter Can not Be Empty' }",
 *           response="empty parameter",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'sales_person_id must be Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/quotes/{quote_id}/addresses',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $response = array();
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        unset($config['id']);
        $config['quote_id'] =  $args['quote_id'];
        $address = new Address();

        $checkEmptyParameter = $address->checkEmptyParameter($config);

        if ($checkEmptyParameter['isEmpty']) {
             http_response_code(400);
             $response['status'] = 'error';
             $response['message'] =  $checkEmptyParameter['message'];
        } elseif (empty($config['sales_person_id']) ||  $config['sales_person_id'] == '0') {
             http_response_code(400);
             $response['status'] = 'error';
             $response['message'] =  'sales_person_id must be Integer';
        } else {
            $response = $address->create($config);
            $response['status'] = 'success';
        }

        renderJSON($response);
    }
);


 /**
 *    @SWG\Post(
 *      path="/quotes/{quote_id}/contracts",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Getting all Quotes Contracts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Parameter(
 *          name="pdf",
 *          in="body",
 *          required=true,
 *          type="file",
 *          description="pdf or jpg"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','id':'1'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/quotes/{quote_id}/contracts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $file = $request->getUploadedFiles();
        $config = array ('quote_id' => $args['quote_id']);

        if (empty($file['contract'])) {
            throw new Exception('Expected a contract file');
        }

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $config['contracts'] = file_get_contents($file['contract']->file);

        $contracts = new Contracts();
        $result = $contracts->create($config)[0];

        $response = array('status' => 'success', 'id' => $result['id']);

        renderJSON($response);
    }
);



 /**
 *    @SWG\Post(
 *      path="/quotes/{quote_id}/contracts/{id}",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Getting a specific Quotes Contracts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="contracts id"
 *      ),
 *       @SWG\Parameter(
 *          name="quote_id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Quote id"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','id':'1'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/quotes/{quote_id}/contracts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $file = $request->getUploadedFiles();
        $config = array (
        'id' => $args['id'],
        'quote_id' => $args['quote_id'],
        );

        if (empty($file['contract'])) {
            throw new Exception('Expected a contract file');
        }

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $config['contracts'] = file_get_contents($file['contract']->file);

        $contracts = new Contracts();
        $result = $contracts->update($config)[0];

        $response = array('status' => 'success', 'id' => $result['id']);

        renderJSON($response);
    }
);




 /**
 *    @SWG\Get(
 *      path="/quotes/{quote_id}/contracts",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Getting a specific Quotes Contracts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="contracts id"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','id':'1'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */



$app->get(
    '/quotes/{quote_id}/contracts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $config = array ('quote_id' => $args['quote_id']);

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $contracts = new Contracts();
        $result = $contracts->getWhere($config);

        $response = array('status' => 'success', 'id' => $result[0]['id']);

        renderJSON($response);
    }
);


 /**
 *    @SWG\Delete(
 *      path="/quotes/{quote_id}/contracts",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Getting a specific Quotes Contracts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="contracts id"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */

$app->delete(
    '/quotes/{quote_id}/contracts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $config = array (
        'quote_id' => $args['quote_id'],
        );

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $contracts = new Contracts();
        $result = $contracts->getWhere($config)[0];

        if (sizeof($result) > 0) {
            $contracts->delete($result['id']);
            $response = array('status' => 'success');
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'response_data' => array(), 'message' => 'data not found');
        }

        renderJSON($response);
    }
);



 /**
 *    @SWG\Get(
 *      path="/quotes/{quote_id}/contracts/download",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Getting a specific Quotes Contracts details",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */

$app->get(
    '/quotes/{quote_id}/contracts/download',
    function ($request, $response, $args) {

        $config = array (
        'quote_id' => $args['quote_id'],
        );

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $quote = new Quote();
        $quoteInfo = $quote->getQuoteInfo($config['quote_id']);
        $contracts = new Contracts();
        $result = $contracts->getWhere($config)[0];

        $finfo = new finfo(FILEINFO_MIME);
        $mimeType = $finfo->buffer($result['contracts']);
        $mimeType = explode(';', $mimeType);
        $ext = $mimeType[0] === 'application/pdf' ? '.pdf' : '.jpg';

        $fileName = $quoteInfo['prefix'].$quoteInfo['quoteId'].$ext;

        if (sizeof($result) > 0) {
            $newResponse = $response->withAddedHeader('Content-Type', $mimeType[0])
                ->withAddedHeader('Pragma', "public")
                ->withAddedHeader('Content-disposition:', 'attachment; filename='.$fileName)
                ->withAddedHeader('Content-Transfer-Encoding', 'binary')
                ->withAddedHeader('Content-Length', strlen($result['contracts']));
                
            return $newResponse->write($result['contracts']);
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'response_data' => array(), 'message' => 'data not found');
            renderJSON($response);
        }
    }
);



 /**
 *    @SWG\Post(
 *      path="/quotes/{quote_id}/attachFiles",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Uploading Files",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="quote_id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *       @SWG\Parameter(
 *          name="filesToUpload",
 *          in="body",
 *          required=true,
 *          type="file",
 *          description="uploaded file"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','files':['icon-selected.svg','logo.png']}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */

$app->post(
    '/quotes/{quote_id}/attachFiles',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $files = $request->getUploadedFiles();
        $config = array ('quote_id' => $args['quote_id']);

        if (empty($files['filesToUpload'])) {
            throw new Exception('Expected attachment file');
        }

        if (!isset($config['quote_id'])) {
            throw new Exception('Quote ID not specified');
        }

        $filesDir = "/tmp/emailFiles-quote-".$args['quote_id'];
        if (!is_dir($filesDir)) {
            mkdir($filesDir, 0777, true);
        }

        $response = array('status' => 'success', 'files' => []);

        foreach ($files['filesToUpload'] as $file) {
            if ($file->getError() === UPLOAD_ERR_OK) {
                $filename = $file->getClientFilename();
                if (!file_exists($filesDir.'/'.$filename)) {
                    $file->moveTo($filesDir.'/'.$filename);
                    array_push($response['files'], $filename);
                }
            }
        }

        renderJSON($response);
    }
);


 /**
 *    @SWG\Post(
 *      path="/quotes/{quote_id}/deleteAttachment",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Uploading Files",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="quote_id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *       @SWG\Parameter(
 *          name="file",
 *          in="body",
 *          required=true,
 *          type="file",
 *          description="Quote Id"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','files':['icon-selected.svg','logo.png']}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/quotes/{quote_id}/deleteAttachment',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = jsonDecodeWithErrorChecking($request->getBody());
        $file = $data['file'];
        $quoteId = $args['quote_id'];

        if (!isset($quoteId)) {
            throw new Exception('Quote ID not specified');
        }

        $filesDir = "/tmp/emailFiles-quote-".$args['quote_id'];
        $filePath = $filesDir.'/'.$file;
        if (file_exists($filePath) && unlink($filePath)) {
            $response = array('status' => 'success');
        } else {
            $response = array('status' => 'error', 'message' => 'Failed to delete attachment:'.$file);
        }

        renderJSON($response);
    }
);



 /**
 *    @SWG\Post(
 *      path="/quotes/{quote_id}/clearAttachments",
 *      tags={"quotes"},
 *      operationId="getQuotes",
 *      summary="Uploading Files",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="quote_id",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="Quote Id"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Modules",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Modules"),
 *           },
 *        @SWG\Property(
 *           property="company",
 *           description="?????",
 *           ref="#/definitions/Modules"
 *         )
 *      )
 * )
 */


$app->post(
    '/quotes/{quote_id}/clearAttachments',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $quoteId = $args['quote_id'];

        if (!isset($quoteId)) {
            throw new Exception('Quote ID not specified');
        }

        $filesDir = "/tmp/emailFiles-quote-".$args['quote_id'];
        array_map('unlink', glob($filesDir."/*"));
        rmdir($filesDir);

        $response = array('status' => 'success');

        renderJSON($response);
    }
);
