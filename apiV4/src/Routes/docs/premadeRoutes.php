<?php

 /**
 *    @SWG\Get(
 *      path="/premadecolors",
 *      tags={"premadecolors"},
 *      operationId="getPremademcolors",
 *      summary="Getting all premadeColors Info",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','data':[{'id':1067,'system_id':150,'name':'test_component','menu_id':0,'type':'preMadeColorGroup','grouped_under':0,'my_order':0,'required':0,'multiple':0,'subGroups':[{'id':1068,'name':'test_component','system_id':151,'colors':[{'id':941,'name':'test_options','color':'#123422','Hex':'#123422'}]}]}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */
