<?php

use SalesQuoter\FormulaParts\FormulaParts;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Specific Formula Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *       @SWG\Parameter(
 *          name="part_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Part ID"
 *      ),  
 *  
 *  
 * 
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1, 'part_id':'2', 'name':'testsss','formula':'test'},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct' }",
 *           response=400,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
 
$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $formulaParts = new FormulaParts();

        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct'));
        }

        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id'] = $args['option_id'];
        $data['part_id'] = $args['part_id'];
        $data['cost_id'] = $args['cost_id'];

        $allFormulaParts = $formulaParts->getWhere($data);

        $response = array('status' => 'success', 'response_data' => $allFormulaParts );

        renderJSON($response);
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting a specific Formula Details with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *       @SWG\Parameter(
 *          name="part_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Part ID"
 *      ),  
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="formula parts table auto-increment primary id"
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),  
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'part_id':1,'name':'testsss','formula':'test'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response=404,
 *      ), 
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $formulaParts = new FormulaParts();

        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct'));
        }


        $singleFormulaParts = $formulaParts->get($args['id']);

        if (sizeof($singleFormulaParts) > 0) {
             $singleFormulaParts[0]['status'] = 'success';
             $response = $singleFormulaParts[0];
        } else {
              http_response_code(400);
              $response = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        


        renderJSON($response);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a specific Formula Parts  with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Formula Parts  table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),      
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $formulaParts = new FormulaParts();
        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct'));
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $response =  $formulaParts->delete($args['id']);
        }

        renderJSON($response);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Formula Parts Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'testsss','formula':'test'}" 
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1, 'part_id':1, 'name':'testsss','formula':'test'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['part_id'] = $args['part_id'];
        $config['cost_id'] = $args['cost_id'];

        $formulaParts = new FormulaParts();
        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct'));
        }


        if (strlen($config['name']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $result =  $formulaParts->create($config);
            $response = array('status' => 'success', 'response_data' =>  $result );
        }

    

        renderJSON($response);
    }
);



// @codingStandardsIgnoreStart

 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating Formula Parts Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ), 
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'testsss','formula':'test'}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'part_id':1,'name':'testsss','formula':'test'}]}",
 *           response=200, 
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),      
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd




$app->put(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['part_id'] = $args['part_id'];
        $config['cost_id'] = $args['cost_id'];
        $config['id'] = $args['id'];

        $formulaParts = new FormulaParts();
        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct'));
        }


        $formulaCosts = $formulaParts->get($config['id']);

        if (sizeof($formulaCosts) > 0) {
            if (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $result = $formulaParts->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  
        renderJSON($response);
    }
);
