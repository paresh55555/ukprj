<?php


 /**
 *    @SWG\Get(
 *      path="/siteBranding",
 *      tags={"siteBranding"},
 *      operationId="getsiteBranding",
 *      summary="Getting Sites Branding Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success',  data:[{'banner_url':'https://facebook.com', 'background_color':'blue', 'first_link_text':'text1', 'second_link_text':'text2', 'second_link_url':'https://facebook.com', 'first_link_url':'https://facebook.com', 'first_link_color':'black'}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */


 /**
 *    @SWG\Put(
 *      path="/siteBranding",
 *      tags={"siteBranding"},
 *      operationId="getsiteBranding",
 *      summary="Updating Site Branding Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'id':2, 'banner_url':'https://facebook.com', 'background_color':'blue', 'first_link_text':'text1', 'second_link_text':'text2', 'second_link_url':'https://facebook.com', 'first_link_url':'https://facebook.com', 'first_link_color':'black'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success', data:[{'banner_url':'https://facebook.com', 'background_color':'blue', 'first_link_text':'text1', 'second_link_text':'text2', 'second_link_url':'https://facebook.com', 'first_link_url':'https://facebook.com', 'first_link_color':'black'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */


 /**
 *    @SWG\Delete(
 *      path="/siteBranding",
 *      tags={"siteBranding"},
 *      operationId="getsiteBranding",
 *      summary="Deleting Site Branding Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */
