<?php

use SalesQuoter\SalesTerritory\SalesTerritory;

 /**
 *    @SWG\Get(
 *      path="/territories",
 *      tags={"territories"},
 *      operationId="getTerritories",
 *      summary="Getting All Sales Territories Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':4,'name':'Fracka Future','salesPersonId':9,'zipLow':0,'zipHigh':0,'zip':'frackaTest'},{...}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */

$app->get(
    '/territories',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $salesTeriObj = new SalesTerritory();
        $territories = $salesTeriObj->getAllTeritory();


        renderJSON($territories);
    }
);


 /**
 *    @SWG\Delete(
 *      path="/territories/{id}",
 *      tags={"territories"},
 *      operationId="getTerritories",
 *      summary="Deleting Sales Territories By Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="Territories table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */


$app->delete(
    '/territories/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $salesTeriObj = new SalesTerritory();
        $territories  =  $salesTeriObj->deleteTeritory($args['id']);

        renderJSON($territories);
    }
);




 /**
 *    @SWG\Post(
 *      path="/territories",
 *      tags={"territories"},
 *      operationId="getTerritories",
 *      summary="Inserting Sales Territories Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description=" Json data "
 *      ),
 *
 *      @SWG\Response(
 *           description="{'id':4,'name':'Fracka Future','salesPersonId':9,'zipLow':0,'zipHigh':0,'zip':'frackaTest', 'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */




$app->post(
    '/territories',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $salesTeriObj = new SalesTerritory();
        if (strlen($config['zip']) == '0') {
             $result  =  $salesTeriObj->zipOverLapValidation($config['zipLow'], $config['zipHigh']);
         
            if ($result === false) {
                $result = $salesTeriObj->create($config);
                $result['status'] = 'success';
            }
        } else {
            $config['zipLow'] = 0;
            $config['zipHigh'] = 0;

            $result = $salesTeriObj->create($config);

            $result['status'] = 'success';
        }

    

        renderJSON($result);
    }
);




 /**
 *    @SWG\Put(
 *      path="/territories",
 *      tags={"territories"},
 *      operationId="getTerritories",
 *      summary="Updating Sales Territories Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description=" Json data "
 *      ),
 *
 *      @SWG\Response(
 *           description="{'id':4,'name':'Fracka Future','salesPersonId':9,'zipLow':0,'zipHigh':0,'zip':'frackaTest', 'status':'success'}",
 *           response=200,
 *
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'ID not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Territories",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Territories"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/Territories"
 *         )
 *      )
 * )
 */



$app->put(
    '/territories',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $salesTeriObj = new SalesTerritory();
        $territoriesDetails = $salesTeriObj->get($config['id']);

        if (sizeof($territoriesDetails) > 0) {
            if (strlen($config['zip']) == '0') {
                $result  =  $salesTeriObj->zipOverLapValidationUpdate($config['zipLow'], $config['zipHigh'], $config['id']);
         
                if ($result === false) {
                    $result = $salesTeriObj->update($config);
                    $result['status'] = 'success';
                }
            } else {
                $config['zipLow'] = 0;
                $config['zipHigh'] = 0;

                $result = $salesTeriObj->update($config);

                $result['status'] = 'success';
            }
        } else {
            http_response_code(404);
            $result['status'] = 'error';
            $result['message'] = 'ID not Exists';
        }

    

    

        renderJSON($result);
    }
);
