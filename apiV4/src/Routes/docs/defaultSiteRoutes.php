<?php

 /**
 *    @SWG\Get(
 *      path="/default/site",
 *      tags={"defaults"},
 *      operationId="getdefaults",
 *      summary="Getting Sites Defaults Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id': 3,'name': 'currency','value': 'dollars'},....]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */


 /**
 *    @SWG\Put(
 *      path="/default/site/{id}",
 *      tags={"defaults"},
 *      operationId="getdefaults",
 *      summary="Updating Sites Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Site ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name': 'currency','value': 'dollars'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="[{'id': 3,'name': 'currency','value': 'dollars'}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */



 /**
 *    @SWG\Get(
 *      path="/default/customer",
 *      tags={"defaults"},
 *      operationId="getdefaults",
 *      summary="Getting Customer Sites Defaults Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ 'status': 'success', 'data': [ { 'id': 15, 'title': 'Billing Zipcode\r\n', 'type': 'customer', 'placeholder': 'Billing Zipcode\r\n', 'display': 1, 'required': 0, 'error': 'This value is required', 'order': 15 }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */


 /**
 *    @SWG\Put(
 *      path="/default/customer/{id}",
 *      tags={"defaults"},
 *      operationId="getdefaults",
 *      summary="Updating Customer Sites Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Site ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{ 'title': 'Billing Zipcode\r\n', 'type': 'customer', 'placeholder': 'Billing Zipcode\r\n', 'display': 1, 'required': 0 }"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="[ { 'id': 15, 'title': 'Billing Zipcode\r\n', 'type': 'customer', 'placeholder': 'Billing Zipcode\r\n', 'display': 1, 'required': 0, 'error': 'This value is required', 'order': 15 }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="Site",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/Site"),
 *           },
 *        @SWG\Property(
 *           property="site",
 *           description="?????",
 *           ref="#/definitions/Site"
 *         )
 *      )
 * )
 */
