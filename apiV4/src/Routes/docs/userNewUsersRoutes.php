<?php

use SalesQuoter\Users\User;
use SalesQuoter\Php_hash\PasswordHash;

// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/users_new",
 *      tags={"users_new"},
 *      operationId="getNewUsers",
 *      summary="Getting User Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="[{'id':1,'name':'Alan Rees','email':'alan@panoramicdoors.com','password':'test','type':'salesPerson','discount':20,'companyName':'Panoramic Doors','prefix':'A','salesAdmin':'on','anytimeQuoteEdits':0},{...}]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd
 
$app->get(
    '/users_new',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();
        $user = new User();
        $users = $user->getUsers($allGetVars);


        renderJSON($users);
    }
);


// @codingStandardsIgnoreStart
 /**
 *    @SWG\Get(
 *      path="/users_new/{id}",
 *      tags={"users_new"},
 *      operationId="getNewUsers",
 *      summary="Getting a specific User Details Information with user id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="users table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':2,'name':'Robert Myers','email':'robert@panoramicdoors.com','password':'test','type':'salesPerson','discount':20,'companyName':'Panoramic Doors','prefix':'H','salesAdmin':'on','anytimeQuoteEdits':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'data not found' }",
 *           response="Not Exist ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd

$app->get(
    '/users_new/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->getUser($args['id']);

    

        if (count($users) > 1) {
             renderJSON($users);
        } else {
             http_response_code(404);
             renderJSON(array('status' => 'error' , 'message' => 'data not found'));
        }
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Delete(
 *      path="/users_new/{id}",
 *      tags={"users_new"},
 *      operationId="getNewUsers",
 *      summary="Delete a specific User with user id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="users table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),      
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd



$app->delete(
    '/users_new/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->deleteUser($args['id']);

        renderJSON($users);
    }
);



// @codingStandardsIgnoreStart
 /**
 *    @SWG\Post(
 *      path="/users_new",
 *      tags={"users_new"},
 *      operationId="getNewUsers",
 *      summary="Adding User Details Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="  { 'name': 'fracka', 'email': 'fracka@fracka.com', 'password': 'tests','type': null, 'discount': null,'companyName': '','prefix': null,'salesAdmin': 'off','anytimeQuoteEdits': 0,'permissions_group_id': 1}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':3,'name':'Leads','email':'Leads','password':'test','type':'leads','discount':20,'companyName':'Panoramic Doors','prefix':'SQ','salesAdmin':'on','anytimeQuoteEdits':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->post(
    '/users_new',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $hasher = new PasswordHash();

        $hashConfig = $hasher->PasswordHash(8, false);
        $config['password'] = $hasher->HashPassword($config['password']);
        //$check = $hasher->CheckPassword( "testsdsdfsdfsd",  $hash);

        $user = new User();
        $result = $user->createUser($config);
        $result['status'] = "success";

        renderJSON($result);
    }
);




// @codingStandardsIgnoreStart
 /**
 *    @SWG\Put(
 *      path="/users_new",
 *      tags={"users_new"},
 *      operationId="getNewUsers",
 *      summary="Updating User Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'id':3, 'name': 'fracka', 'email': 'fracka@fracka.com', 'password': 'tests','type': null, 'discount': null,'companyName': '','prefix': null,'salesAdmin': 'off','anytimeQuoteEdits': 0,'permissions_group_id': 1}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'id':3,'name':'Leads','email':'Leads','password':'test','type':'leads','discount':20,'companyName':'Panoramic Doors','prefix':'SQ','salesAdmin':'on','anytimeQuoteEdits':0}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'SQLSTATE[42S22]: Column not found: 1054 Unknown column status in 'field list, 'line':'517', 'file': '/var/www/apiV3/vendor/slim/pdo/src/PDO/Statement/StatementContainer.php' }",
 *           response=500,
 *      ),    
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
//@codingStandardsIgnoreEnd


$app->put(
    '/users_new',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $hasher = new PasswordHash();
        $hashConfig = $hasher->PasswordHash(8, false);
        $config['password'] = $hasher->HashPassword($config['password']);

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $result['status']  =  'success';
             renderJSON($result);
        } else {
             http_response_code(404);
             renderJSON(array('status' => 'error' , 'message' => 'data not found'));
        }
    }
);





$app->put(
    '/users_new/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["id"] = $args["id"];

        $hasher = new PasswordHash();
        $hashConfig = $hasher->PasswordHash(8, false);
        $config['password'] = $hasher->HashPassword($config['password']);

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $result['status']  =  'success';
             renderJSON($result);
        } else {
             http_response_code(404);
             renderJSON(array('status' => 'error' , 'message' => 'data not found'));
        }
    }
);
