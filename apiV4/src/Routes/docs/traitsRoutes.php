<?php

use SalesQuoter\Traits\Traits;

 /**
 *    @SWG\Get(
 *      path="/traits",
 *      tags={"traits"},
 *      operationId="getTraits",
 *      summary="Getting All Traits Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'component_id':'3',name':'21sdf'},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->get(
    '/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $allTraits = $traits->getAll();

        $response = array('status' => 'success', 'response_data' => $allTraits );

        renderJSON($response);
    }
);



 /**
 *    @SWG\Get(
 *      path="/traits/{id}",
 *      tags={"traits"},
 *      operationId="getTraits",
 *      summary="Getting a specific Traits Details with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'component_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $singleTraits = $traits->get($args['id']);

            if (sizeof($singleTraits) > 0) {
                 $response = array('status' => 'success', 'response_data' => $singleTraits  );
            } else {
                 http_response_code(400);
                 $response = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
            }
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Delete(
 *      path="/traits/{id}",
 *      tags={"traits"},
 *      operationId="getTraits",
 *      summary="Delete a specific traits with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */




$app->delete(
    '/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $deleteTraits = $traits->delete($args['id']);
            $response = $deleteTraits;
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Post(
 *      path="/traits",
 *      tags={"traits"},
 *      operationId="getTraits",
 *      summary="Adding traits Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'component_id':'3',name':'21sdf'}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,'component_id':'3',name':'21sdf'}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();
        if (intval($config['component_id']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
        } elseif (strlen($config['name']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $result =  $traits->create($config);
            $response = array('status' => 'success', 'response_data' =>  $result );
        }

    

        renderJSON($response);
    }
);





 /**
 *    @SWG\Put(
 *      path="/traits",
 *      tags={"traits"},
 *      operationId="getTraits",
 *      summary="Updating traits Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'id':1,name':'21sdf', 'component_id':3}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':1,name':'21sdf', 'component_id':3}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Component ID Must be Valid Integer' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */





$app->put(
    '/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();
        $singleTraits = $traits->get($config['id']);

        if (sizeof($singleTraits) > 0) {
            if (intval($config['component_id']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $result = $traits->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  

        renderJSON($response);
    }
);
