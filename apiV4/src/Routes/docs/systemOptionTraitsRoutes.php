<?php

use SalesQuoter\Traits\Traits;
use SalesQuoter\Traits\OptionTraits;

 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/traits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting All Traits Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *
 *
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'name':'testsss','value':'1','visible':1},{...}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */

$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $optionTraits = new OptionTraits();

        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct'));
        }

        $data  = array();
        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id']  = $args['option_id'];

        $allOptionTraits = $optionTraits->getWhere($data);

        $response = array('status' => 'success', 'response_data' => $allOptionTraits );

        renderJSON($response);
    }
);



 /**
 *    @SWG\Get(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Getting a specific Traits Details with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'name':'testsss','value':'1','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $optionTraits = new OptionTraits();

        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct'));
        }


        $singleOptionTraits = $optionTraits->get($args['id']);

        if (sizeof($singleOptionTraits) > 0) {
             $singleOptionTraits[0]['status'] = 'success';
             $response = $singleOptionTraits[0];
        } else {
              http_response_code(400);
              $response = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        


        renderJSON($response);
    }
);




 /**
 *    @SWG\Delete(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Delete a specific traits with Id",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="traits table auto-increment primary id"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */




$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct'));
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $deleteOptionTraits = $optionTraits->delete($args['id']);
            $response = $deleteOptionTraits;
        }

        renderJSON($response);
    }
);




 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/traits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding traits Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'testsss','value':'1','visible':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'name':'testsss','value':'1','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */



$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];

        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct'));
        }


        if (strlen($config['value']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' => 'Value can not be Empty' );
        } elseif (strlen($config['name']) == '0') {
            http_response_code(400);
            $response = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $result =  $optionTraits->create($config);
            $response = array('status' => 'success', 'response_data' =>  $result );
        }

    

        renderJSON($response);
    }
);



 /**
 *    @SWG\Post(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/multipleTraits",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Adding Multiple traits Details Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="[{""name"":""title"",""value"":""test1"",""visible"":1},{""name"":""selected"",""value"":false,""visible"":1},{""name"":""note"",""value"":""test"",""visible"":1},{""id"":13859,""system_id"":2464,""component_id"":2555,""option_id"":6011,""name"":""imgUrl"",""value"":""tmp"",""visible"":1}]"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'name':'testsss','value':'1','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */


$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/multipleTraits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $traits = jsonDecodeWithErrorChecking($data);
        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct'));
        }


        $resultTraits = [];
        foreach ($traits as $trait) {
            $trait['system_id'] = $args['system_id'];
            $trait['component_id'] = $args['component_id'];
            $trait['option_id'] = $args['option_id'];
            if (strlen($trait['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => ' name can not be Empty');
            } else {
                if (isset($trait['id'])) {
                    $result = $optionTraits->update($trait);
                } else {
                    $result = $optionTraits->create($trait);
                }

                array_push($resultTraits, $result[0]);
            }
        }

        $response = array('status' => 'success', 'response_data' => $resultTraits);

        renderJSON($response);
    }
);


 /**
 *    @SWG\Put(
 *      path="/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}",
 *      tags={"systems"},
 *      operationId="getSystems",
 *      summary="Updating traits Information",
 *
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="system_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="System ID"
 *      ),
 *       @SWG\Parameter(
 *          name="component_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Component ID"
 *      ),
 *       @SWG\Parameter(
 *          name="option_id",
 *          in="path",
 *          required=true,
 *          type="string",
 *          description="Option ID"
 *      ),
 *
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description="{'name':'testsss','value':'1','visible':1}"
 *      ),
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success','response_data':[{'id':2,'system_id':1,'component_id':10,'option_id':1,'name':'testsss','value':'1','visible':1}]}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System_id , Component_id and Option_id combination is not Correct' }",
 *           response=400,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'name can not be Empty' }",
 *           response="Empty Name",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', message': 'ID Not Exists' }",
 *           response=404,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),
 *
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 */





$app->put(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['id'] = $args['id'];

        $optionTraits = new OptionTraits();
        $checkExists = $optionTraits->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
             http_response_code(400);
             renderJSON(array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct'));
        }


        $singleOptionTraits = $optionTraits->get($config['id']);

        if (sizeof($singleOptionTraits) > 0) {
            if (strlen($config['value']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' => 'Value can not be Empty' );
            } elseif (strlen($config['name']) == '0') {
                http_response_code(400);
                $response = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $result = $optionTraits->update($config);
                $response = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            http_response_code(404);
            $response = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  

        renderJSON($response);
    }
);
