<?php


 /**
 *    @SWG\Get(
 *      path="/items",
 *      tags={"Items"},
 *      operationId="getItems",
 *      summary="Getting All Items",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *       @SWG\Parameter(
 *          name="state",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="State can be either quote or order"
 *      ),
 *       @SWG\Parameter(
 *          name="fields",
 *          in="query",
 *          required=true,
 *          type="string",
 *          description="fields of the table"
 *      ), 
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""response_data"": [ { ""item_number"": ""fff2"", ""id"": 2, ""sub_total"": ""12.00"", ""total"": ""12.00"", ""created"": ""2017-10-31 00:00:00"", ""shipping_address1"": """", ""shipping_address2"": """", ""shipping_city"": """", ""shipping_state"": """", ""shipping_zip"": """", ""shipping_phone"": """", ""shipping_contact"": """", ""billing_address1"": """", ""billing_address2"": """", ""billing_city"": """", ""billing_state"": """", ""billing_zip"": """", ""billing_phone"": """", ""billing_contact"": """", ""shipping_email"": """", ""billing_email"": """", ""firstName"": ""Benjamin"", ""lastName"": ""Cold"", ""phone"": ""+189586890"", ""companyName"": """", ""email"": ""Benjamin@gmail.com"" } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */



 /**
 *    @SWG\Get(
 *      path="/items/{id}",
 *      tags={"Items"},
 *      operationId="getItems",
 *      summary="Getting a Items Details",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""response_data"": { ""id"": 1, ""created"": ""2017-08-31 00:00:00"", ""prefix"": ""FFFF"", ""state"": ""order"", ""sales_person_id"": 14, ""updated"": ""2017-10-31 00:00:00"", ""sub_total"": ""12.00"", ""total"": ""12.00"", ""customer_id"": 29194, ""addresses_id"": 61912, ""active"": 1, ""customer"": [ { ""id"": 29194, ""firstName"": ""Benjamin"", ""lastName"": ""Cold"", ""phone"": ""+189586890"", ""companyName"": """", ""email"": ""Benjamin@gmail.com"", ""sales_person_id"": 14, ""billingAddress1"": """", ""billingAddress2"": """", ""billingCity"": ""NY"", ""billingState"": ""NY"", ""billingZip"": ""178952"", ""jobAddress1"": null, ""jobAddress2"": null, ""jobCity"": null, ""jobState"": null, ""jobZip"": null, ""customerNotes"": null, ""shippingName"": """", ""shippingEmail"": """", ""billingName"": """", ""billingEmail"": ""Benjamin123@gmail.com"", ""leadSource"": ""new"", ""leadType"": """", ""sameAsShipping"": 0, ""customerPO"": """", ""customerREF"": """", ""contact"": """", ""jobPhone"": null, ""active"": 1, ""billingPhone"": ""+1978565416"", ""shippingPhone"": """" } ], ""address"": [ { ""id"": 61912, ""quote_id"": 32073, ""sales_person_id"": 9, ""shipping_address1"": """", ""shipping_address2"": """", ""shipping_city"": """", ""shipping_state"": """", ""shipping_zip"": """", ""shipping_phone"": """", ""shipping_contact"": """", ""billing_address1"": """", ""billing_address2"": """", ""billing_city"": """", ""billing_state"": """", ""billing_zip"": """", ""billing_phone"": """", ""billing_contact"": """", ""shipping_email"": """", ""billing_email"": """" } ], ""price_changes"": [ { ""id"": 1, ""item_id"": 1, ""kind"": """", ""amount"": ""45.0000"", ""name"": ""Dimensions2"", ""show_on_item"": 0 } ], ""cart"": [ { ""id"": 1, ""item_id"": 1, ""component_values"": { ""name"": ""Dimensions2"", ""amount"": ""-0.45"" }, ""system_id"": 45, ""quantity"": 1, ""sub_total"": ""45.50"", ""total"": ""50.00"", ""price_changes"": [ { ""id"": 1, ""cart_id"": 1, ""kind"": """", ""amount"": ""45.0000"", ""name"": ""Dimensions2"", ""show_on_item"": 0, ""active"": 1 }, { ""id"": 2, ""cart_id"": 1, ""kind"": """", ""amount"": ""-0.4500"", ""name"": ""Dimensions2"", ""show_on_item"": 0, ""active"": 1 }, { ""id"": 3, ""cart_id"": 1, ""kind"": """", ""amount"": ""-0.4500"", ""name"": ""Dimensions2"", ""show_on_item"": 0, ""active"": 1 }, { ""id"": 4, ""cart_id"": 1, ""kind"": """", ""amount"": ""-0.4500"", ""name"": ""Dimensions2"", ""show_on_item"": 0, ""active"": 1 }, { ""id"": 5, ""cart_id"": 1, ""kind"": """", ""amount"": ""-0.4500"", ""name"": ""Dimensions2"", ""show_on_item"": 0, ""active"": 1 } ] }, { ""id"": 2, ""item_id"": 1, ""component_values"": { ""name"": ""Dimensions2"", ""amount"": ""-0.45"" }, ""system_id"": 45, ""quantity"": 1, ""sub_total"": ""45.50"", ""total"": ""50.00"", ""price_changes"": [] }, { ""id"": 3, ""item_id"": 1, ""component_values"": { ""name"": ""Dimensions2"", ""amount"": ""-0.45"" }, ""system_id"": 45, ""quantity"": 1, ""sub_total"": ""45.50"", ""total"": ""50.00"", ""price_changes"": [] } ] } }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ), 
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Get(
 *      path="/items/{id}/histroy?length=10",
 *      tags={"cartPrice"},
 *      operationId="cartPrice",
 *      summary="Getting a History Carts Price",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="SQ_items table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="[ { ""id"": 4, ""kind"": ""items"", ""kind_id"": 2, ""event"": ""PUT"", ""raw_data"": { ""state"": ""order"", ""id"": ""2"" }, ""result"": { ""status"": ""success"", ""response_data"": [ { ""id"": 2, ""created"": ""2017-10-31 00:00:00"", ""prefix"": ""fff"", ""state"": ""order"", ""sales_person_id"": 14, ""updated"": ""2017-10-31 00:00:00"", ""sub_total"": ""12.00"", ""total"": ""12.00"", ""customer_id"": 29194, ""addresses_id"": 61912 } ] }, ""date"": ""2017-10-16 05:04:55"" } ]",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Parameter ID must be an integer' }",
 *           response="invalid id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'response_data':[], 'message': 'data not found' }",
 *           response="not existing id",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),   
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */


 /**
 *    @SWG\Put(
 *      path="/items/{id}",
 *      tags={"Items"},
 *      operationId="getItems",
 *      summary="Updating Items Information",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="sending_data",
 *          in="body",
 *          required=true,
 *          type="string",
 *          description=" { ""id"": 1, ""created"": ""2017-08-31 00:00:00"", ""prefix"": ""FFFF"", ""state"": ""order"", ""sales_person_id"": 14, ""updated"": ""2017-10-31 00:00:00"", ""sub_total"": ""12.00"", ""total"": ""12.00"", ""customer_id"": 29194, ""addresses_id"": 61912}" 
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{ ""status"": ""success"", ""response_data"": [ { ""id"": 1, ""created"": ""2017-08-31 00:00:00"", ""prefix"": ""FFFF"", ""state"": ""order"", ""sales_person_id"": 14, ""updated"": ""2017-10-31 00:00:00"", ""sub_total"": ""12.00"", ""total"": ""12.00"", ""customer_id"": 29194, ""addresses_id"": 61912 } ] }",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{'status':'error','message':'Sending JSON data can not be parsed','error_type':'Syntax error, malformed JSON'}",
 *           response="invalid_json_data",
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'System ID Must be Valid Integer' }",
 *           response="invalid system id",
 *      ),  
 * 
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */




  /**
 *    @SWG\Delete(
 *      path="/items/{id}",
 *      tags={"Items"},
 *      operationId="getItems",
 *      summary="Delete a specific Carts with Id",
 *     
 *      @SWG\Parameter(
 *          name="Authorization",
 *          in="header",
 *          required=true,
 *          type="string",
 *          description="Authentication Bearer Token"
 *      ),
 *        
 *       @SWG\Parameter(
 *          name="id",
 *          in="path",
 *          required=true,
 *          type="integer",
 *          description="items table auto-increment primary id"
 *      ), 
 *
 *
 *      @SWG\Response(
 *           description="{'status':'success'}",
 *           response=200,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'not enough permission' }",
 *           response=403,
 *      ),
 *      @SWG\Response(
 *           description="{ 'status': 'failed', 'message': 'ID not exists or already Deleted' }",
 *           response="Not deleted ID",
 *      ),  
 *      @SWG\Response(
 *           description="{ 'status': 'error', 'message': 'Method must be one of: GET, POST, DELETE'}",
 *           response=405,
 *      ),  
 *   
 *      @SWG\Definition(
 *           definition="User",
 *           allOf={
 *             @SWG\Schema(ref="#/definitions/User"),
 *           },
 *        @SWG\Property(
 *           property="user",
 *           description="?????",
 *           ref="#/definitions/User"
 *         )
 *      )
 * )
 *
 */
