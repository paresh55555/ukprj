<?php

use SalesQuoter\CartLayout\LayoutGroup;

$app->get(
    '/carts/{id}/layouts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();

        $layoutGroup = new LayoutGroup();
        $allLayout = $layoutGroup->allLayoutGroup( $args['id'] );

        return responseWithStatusCode($response, $allLayout, 200);
    }
);


$app->delete(
    '/carts/{layout_id}/layouts/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutGroup = new LayoutGroup();
        $dataExists = $layoutGroup-> checkLayouts ($args['layout_id'], $args['id']);

        if ($dataExists == false) {
            $data = array('status' => 'error', 'message' => 'Layout Group id and Layout id combination is not correct' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteLayout = $layoutGroup->delete($args['id']);
            
            return responseWithStatusCode($response, $deleteLayout, 200);
        }
    }
);




$app->post(
    '/carts/{id}/layouts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $layoutGroup = new LayoutGroup();
        $insertData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $insertData['number_of_columns'] = $config['number_of_columns'];
        $insertData['`order`'] = $layoutGroup-> getInsertOrder ( $args['id'] ); 
        $insertData['layout_id'] = $args['id']; 
        

        if (strlen($insertData['number_of_columns']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'number_of_columns can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $layoutGroup->create($insertData);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/carts/{layout_id}/layouts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $updateData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $updateData['`order`'] = $config['order'];
        $updateData['id'] = $args['id'];

        $layoutGroup = new LayoutGroup();
        $singleLayout = $layoutGroup->get($args['id']);

        $dataExists = $layoutGroup-> checkLayouts ($args['layout_id'], $args['id']);

        if (sizeof($singleLayout) > 0) {
            if ($dataExists == false) {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'Order can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $layoutGroup->update($updateData);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
