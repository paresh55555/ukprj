<?php

use SalesQuoter\History\History;


function renderHistoryItems( $type, $pId, $limit, $token, $response ) {

    if ( $type == 'items' ) {

        if (false === $token->hasScope(["salesperson"])) {
            return invalidPermissionsResponse($response);
        }
    }else {

        if (false === $token->hasScope(["admin"])) {
         return invalidPermissionsResponse($response);
       }
    }

    $history = new History();

    $getWhere = array(
      "kind" => $type,
      "kind_id" => $pId,
    );

    $intVal = intval($limit);
    if ( $intVal == '0' ) {

    	 $intVal = 10; 
    }

    $result = $history->renderHistory( $getWhere, $intVal );

    $responseData = array( "status" => "success", "response_data" => $result );

    return responseWithStatusCode($response, $result, 200);
}