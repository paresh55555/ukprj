<?php

use SalesQuoter\Users\User;
use SalesQuoter\Php_hash\PasswordHash;


 
$app->get(
    '/users_new',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $allGetVars = $request->getQueryParams();
        $user = new User();
        $users = $user->getUsers($allGetVars);

        return responseWithStatusCode($response, $users, 200);
    }
);



$app->get(
    '/users_new/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->getUser($args['id']);
    

        if (count($users) > 1) {
             $responseCode = 200;
        } else {
             $responseCode = 404;
             $users = array('status' => 'error' , 'message' => 'data not found');
        }

        return responseWithStatusCode($response, $users, $responseCode);
    }
);



$app->delete(
    '/users_new/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $users = $user->deleteUser($args['id']);

        return responseWithStatusCode($response, $users, 200);
    }
);


$app->post(
    '/users_new',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $hasher = new PasswordHash();

        $hashConfig = $hasher->PasswordHash(8, false);
        $config['password'] = $hasher->HashPassword($config['password']);
        //$check = $hasher->CheckPassword( "testsdsdfsdfsd",  $hash);

        $user = new User();
        $result = $user->createUser($config);
        $result['status'] = "success";

        return responseWithStatusCode($response, $result, 200);
    }
);



$app->put(
    '/users_new',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $hasher = new PasswordHash();
        $hashConfig = $hasher->PasswordHash(8, false);
        $config['password'] = $hasher->HashPassword($config['password']);

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $responseCode = 200;
             $result['status']  =  'success';
        } else {
             $responseCode = 404;
             $result = array('status' => 'error' , 'message' => 'data not found');
        }

        return responseWithStatusCode($response, $result, $responseCode);
    }
);





$app->put(
    '/users_new/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config["id"] = $args["id"];

        $hasher = new PasswordHash();
        $hashConfig = $hasher->PasswordHash(8, false);
        $config['password'] = $hasher->HashPassword($config['password']);

        $user = new User();
        $result = $user->updateUser($config);
        //$result->status = "success";

        if (count($result) > 1) {
             $result['status']  =  'success';
             return responseWithStatusCode($response, $result, 200);
        } else {
             $result = array('status' => 'error' , 'message' => 'data not found');
             return responseWithStatusCode($response, $result, 404);
        }
    }
);
