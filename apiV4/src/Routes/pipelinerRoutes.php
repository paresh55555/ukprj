<?php

use SalesQuoter\SalesPerson\Crm;
use SalesQuoter\Quotes\Quote;


$app->post(
    '/pipeliner/leads',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $ownerId = $config['ownerId'];
        $crm = new Crm($ownerId);

        $opportunityID = $crm->createNewSalesQuoterLead($config);

        return responseWithStatusCode($response, $opportunityID, 200);
    }
);



$app->put(
    '/pipeliner/leads',
    function ($request, $response, $args) {


        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $ownerId = $config['ownerId'];
        $crm = new Crm($ownerId);


        $result = $crm->updateOpportunity($config);
        $result['status'] = 'success';

        return responseWithStatusCode($response, $result, 200);
    }
);




$app->delete(
    '/pipeliner/leads',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $quoteID = jsonDecodeWithErrorChecking($data);

        if (!$quoteID) {
            $result = array(
                'error' => 'Empty quoteID',
            );

            return responseWithStatusCode($response, $result, 200);
        }

        $quote = new Quote();
        $quoteInfo = $quote->getQuoteInfo($quoteID);

        if (empty($quoteInfo)) {

            $result = array(
                'error' => "Quote {$quoteID} not found",
            );

            return responseWithStatusCode($response, $result, 200);
        }

        $crm = new Crm($quoteInfo['ownerId']);

        $result = $crm->markAsLostOpportunity($quoteInfo);
        $result['status'] = 'success';

        return responseWithStatusCode($response, $result, 200);
    }
);
