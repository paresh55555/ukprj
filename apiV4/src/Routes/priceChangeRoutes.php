<?php

use SalesQuoter\PriceChange\PriceChangeCart;
use SalesQuoter\PriceChange\PriceChangeItem;
use SalesQuoter\History\History;

$app->get(
    '/priceChange/{type}/{type_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();

        if ( $args['type'] == 'cart' ) {

            $priceChange = new PriceChangeCart();
            $data['cart_id'] = $args['type_id'];  

        }else if ( $args['type'] == 'item' ) {

           $priceChange = new PriceChangeItem();
           $data['item_id'] = $args['type_id'];  
        }else{
          
          $responseData = array( "status" => "error", "msg" => "Price change type is not correct" );

          return responseWithStatusCode($response, $responseData, 401);
        }

        
        $allItems = $priceChange->getWhere( $data );

        $responseData = array( "status" => "success", "response_data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/priceChange/{type}/{type_id}/{id}/history',
    function ($request, $response, $args) {

       $allGetVars = $request->getQueryParams();

        if ( $args['type'] == 'cart' ) {

            $type = 'price_change_cart';  

        }else if ( $args['type'] == 'item' ) {

           $type = 'price_change_item';   
        }else{
          
          $responseData = array( "status" => "error", "msg" => "Price change type is not correct" );

          return responseWithStatusCode($response, $responseData, 401);
        }

       return renderHistoryItems( $type, $args['id'], $allGetVars['length'], $this->token, $response );   
        
    }
);


$app->get(
    '/priceChange/{type}/{type_id}/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();

        if ( $args['type'] == 'cart' ) {

            $priceChange = new PriceChangeCart();
            $data['cart_id'] = $args['type_id'];  

        }else if ( $args['type'] == 'item' ) {

           $priceChange = new PriceChangeItem();
           $data['item_id'] = $args['type_id'];  
        }else{
          
          $responseData = array( "status" => "error", "msg" => "Price change type is not correct" );

          return responseWithStatusCode($response, $responseData, 401);
        }

        $data['id'] = $args['id'];
        $allItems = $priceChange->getWhere( $data );
        
        $responseData = array( "status" => "success", "response_data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->delete(
    '/priceChange/{type}/{type_id}/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        if ( $args['type'] == 'cart' ) {

            $priceChange = new PriceChangeCart(); 

        }else if ( $args['type'] == 'item' ) {

           $priceChange = new PriceChangeItem(); 
        }else{
          
          $responseData = array( "status" => "error", "msg" => "Price change type is not correct" );

          return responseWithStatusCode($response, $responseData, 200);
        }

        $intVal = intval( $args['id'] );

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $data, 401);

        } else {
            $deletePriceChange = $priceChange->delete($args['id']);
            
            $history = New History();
            $history->createLog("price_change_".$args['type'], $intVal, "DELETE", array(), $deletePriceChange);

            return responseWithStatusCode($response, $deletePriceChange, 200);
        }
    }
);



$app->post(
    '/priceChange/{type}/{type_id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( $args['type'] == 'cart' ) {

            $priceChange = new PriceChangeCart(); 
            $config['cart_id'] = $args['type_id'];  

        }else if ( $args['type'] == 'item' ) {

           $priceChange = new PriceChangeItem();
           $config['item_id'] = $args['type_id'];  

        }else{
          
          $responseData = array( "status" => "error", "msg" => "Price change type is not correct" );

          return responseWithStatusCode($response, $responseData, 401);
        }

        $result =  $priceChange->create($config);
        $responseData = array('status' => 'success', 'response_data' =>  $result );
    
        $history = New History();
        $history->createLog("price_change_".$args['type'], $result[0]['id'], "POST", $config, $responseData);

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->put(
    '/priceChange/{type}/{type_id}/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( $args['type'] == 'cart' ) {

            $priceChange = new PriceChangeCart(); 
            $config['cart_id'] = $args['type_id'];  

        }else if ( $args['type'] == 'item' ) {

           $priceChange = new PriceChangeItem();
           $config['item_id'] = $args['type_id'];  

        }else{
          
          $responseData = array( "status" => "error", "msg" => "Price change type is not correct" );

          return responseWithStatusCode($response, $responseData, 401);
        }
 
        $config['id'] = $args['id'];

        $result =  $priceChange->update($config);
        $responseData = array('status' => 'success', 'response_data' =>  $result );
 
        $history = new History();
        $history->createLog("price_change_".$args['type'], $args['id'], "PUT", $config, $responseData);    

        return responseWithStatusCode($response, $responseData, 200);

    }
);