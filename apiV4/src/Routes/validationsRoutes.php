<?php

use SalesQuoter\Validations\Validations;

$app->get(
    '/validations',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $validations = new Validations();
        $allValidations = $validations->getAll();

        $responseData = array('status' => 'success', 'response_data' => $allValidations );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/validations/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $validations = new Validations();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $singleValidation = $validations->get($args['id']);
        
            if (sizeof($singleValidation) > 0) {
                $responseCode = 200;
                $responseData = array('status' => 'success', 'response_data' => $singleValidation  );
            } else {
                $responseCode = 404;
                $responseData = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found');
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->delete(
    '/validations/{id}',
    function ($request, $response, $args) {
   
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $validations = new Validations();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $validations->delete($args['id']);
        }

       return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/validations',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $validations = new Validations();

        if (intval($config['trait_id']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'ID Must be Valid Integer' );
        } elseif (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $validations->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, 201);
    }
);



$app->put(
    '/validations',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
    
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $validations = new Validations();
        $singleValidation = $validations->get($config['id']);

        if (sizeof($singleValidation) > 0) {
            if (intval($config['trait_id']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' => 'Trait ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $validations->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
