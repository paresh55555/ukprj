<?php

use SalesQuoter\Costs\OptionPartsCosts;

$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/costs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $optionPartsCosts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }

        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id'] = $args['option_id'];

        $allOptionPartsCosts = $optionPartsCosts->getWhere($data);

        $responseData = array('status' => 'success', 'response_data' => $allOptionPartsCosts );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/costs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $optionPartsCosts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        $singleOptionsParts = $optionPartsCosts->get($args['id']);

        if (sizeof($singleOptionsParts) > 0) {
             $responseCode = 200;
             $singleOptionsParts[0]['status'] = 'success';
             $responseData = $singleOptionsParts[0];
        } else {
             $responseCode = 400;
             $responseData = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/costs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $optionPartsCosts = new OptionPartsCosts();
        $checkExists = $optionPartsCosts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData =  $optionPartsCosts->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/costs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];

        $optionPartsCosts = new OptionPartsCosts();
        $checkExists = $optionPartsCosts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 201;
            $result =  $optionPartsCosts->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->put(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/costs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['id'] = $args['id'];

        $optionPartsCosts = new OptionPartsCosts();
        $checkExists = $optionPartsCosts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        $singleOptionPartsCosts = $optionPartsCosts->get($config['id']);

        if (sizeof($singleOptionPartsCosts) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $optionPartsCosts->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  
        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
