<?php
use SalesQuoter\Service\JobAddresses;
use SalesQuoter\Service\Jobs;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/jobs/{job_id}/address',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobAddresses = new JobAddresses();
        $jobs = new Jobs();

        $checkJobExists = $jobs->get( $args["job_id"] );

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else { 

            $allItems = $jobAddresses->getAll ( ["job_id" => $args["job_id"] ] );

            $responseData = array ("status" => "success", "data" => $allItems );

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/jobs/{job_id}/address/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobAddresses = new JobAddresses();
        $jobs = new Jobs();


        $checkJobExists = $jobs->get( $args["job_id"] );

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else { 

            $allItems = $jobAddresses->getAll ( ["id" => $args["id"] ] );
            
            $responseData = array ("status" => "success", "data" => $allItems );

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->post(
    '/service/jobs/{job_id}/address',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobAddresses = new JobAddresses();
        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $checkJobExists = $jobs->get( $args["job_id"] );

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if (  intval($config["address_id"]) == '0' && (strlen($config["address_1"]) == '0' || strlen($config["city"]) == '0')) {

            $responseData = array ("status" => "error", "msg" => "address fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["job_id"] = $args["job_id"];

            if ( intval($config["address_id"]) == '0' ) {
                $insertAddress = $jobAddresses->insertJobAddress( $config );
                $config["address_id"] =  $insertAddress["address_id"];
            }

            $newAddress[] = $jobAddresses->create($config);

            $logMessage = "New address has been addded" ;
            $activityLog->addLog ( $logMessage, $args['job_id'], "address", "jobs", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newAddress );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/jobs/{job_id}/address/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobAddresses = new JobAddresses();
        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $checkJobExists = $jobs->get( $args["job_id"] );

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if (  intval($config["address_id"]) == '0' && (strlen($config["address_1"]) == '0' || strlen($config["city"]) == '0')) {

            $responseData = array ("status" => "error", "msg" => "address fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];
            $config["job_id"] = $args["job_id"];

            $insertAddress = $jobAddresses->insertJobAddress( $config );
            $config["address_id"] =  $insertAddress["address_id"];

            $newAddress[] = $jobAddresses->update ($config);

            $logMessage = "Address has been updated" ;
            $activityLog->addLog ( $logMessage, $args['job_id'], "address", "jobs", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newAddress );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->delete(
    '/service/jobs/{job_id}/address/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobAddresses = new JobAddresses();
        $jobs = new Jobs();
        $activityLog = new ActivityLog();

        $checkJobExists = $jobs->get( $args["job_id"] );

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkJobExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid job id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $jobAddresses->delete($args['id']);

            $logMessage = "Address has been deleted" ;
            $activityLog->addLog ( $logMessage, $args['job_id'], "address", "jobs", $this->token->decoded->userId );
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);