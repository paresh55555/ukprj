<?php

use SalesQuoter\Cart\Cart;

$app->get(
    '/cart',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $data['kind'] = 'cart';

        $cart = new Cart();
        $allCart = $cart->getWhere( $data );

        return responseWithStatusCode($response, $allCart, 200);
    }
);



$app->get(
    '/cart/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cart = new Cart();
        $intVal = intval($args['id']);

        $singleCart = $cart->get($args['id']);

        if (sizeof($singleCart) > 0) {
            $singleCart[0]['status'] = 'success';

            return responseWithStatusCode($response, $singleCart[0], 200);
        } else {

            $responseData = array('status' => 'error', 'message' => 'data not found');
                
            return responseWithStatusCode($response, $responseData, 404);
        }

    }
);




$app->delete(
    '/cart/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cart = new Cart();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteCart = $cart->delete($args['id']);
            
            return responseWithStatusCode($response, $deleteCart, 200);
        }
    }
);




$app->post(
    '/cart',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['kind'] = 'cart';

        $cart = new Cart();

        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $cart->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/cart',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $cart = new Cart();
        $singleCart = $cart->get($config['id']);

        if (sizeof($singleCart) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $cart->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
