<?php

use SalesQuoter\Options\Options;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Systems\Systems;
use SalesQuoter\Components\Components;
use SalesQuoter\Traits\OptionTraits;

/**
 * Returns Options Traits
 *
 * @param systemId        $systemId        Where something interesting takes place
 * @param componentsObj   $componentsObj   How many times something interesting should happen
 * @param traitsObj       $traitsObj       Where something interesting takes place
 * @param optionsObj      $optionsObj      How many times something interesting should happen
 * @param optionTraitsObj $optionTraitsObj Where something interesting takes place
 *
 * @return Components Array
 */
function get_by_components($systemId, $componentsObj, $traitsObj, $optionsObj, $optionTraitsObj)
{

 
    $returnData = array();

    $params = array("system_id" => $systemId);
    $allComponents = $componentsObj->getWhere($params);

    foreach ($allComponents as $components) {
        $data["id"] = $components["id"];
        $data["menu_id"] = $components["menu_id"];
        $data["type"] = $components["type"];
        $data["system_id"] = $components["system_id"];
        $data["grouped_under"] = $components["grouped_under"];
        $data["name"] = $components["name"];
        $data["traits"] = get_by_components_traits($components["id"], $traitsObj);
        $data["options"] = get_by_components_options($systemId, $components["id"], $optionsObj, $optionTraitsObj);
        $returnData[] = $data;
    }

  
    return $returnData ;
}

/**
 * Returns Options Traits
 *
 * @param $componentId $componentId Where something interesting takes place
 * @param $traitsObj   $traitsObj   How many times something interesting should happen
 *
 * @return Traits Array
 */
function get_by_components_traits($componentId, $traitsObj)
{

    $returnData = array();

    $params = array("component_id" => $componentId );
    $allTraits = $traitsObj->getWhere($params);

    foreach ($allTraits as $traits) {
        $data["id"] = $traits["id"];
        $data["component_id"] = $traits["component_id"];
        $data["name"] = $traits["name"];
        $data["type"] = $traits["type"];
        $data["value"] = $traits["value"];
        $data["visible"] = $traits["visible"];

        $returnData[] = $data;
    }

  
    return $returnData ;
}

/**
 * Returns Options Traits
 *
 * @param systemId        $systemId        Where something interesting takes place
 * @param componentId     $componentId     How many times something interesting should happen
 * @param optionId        $optionsObj      Where something interesting takes place
 * @param $optionTraitsObj $optionTraitsObj How many times something interesting should happen
 *
 * @return Options Array
 */
function get_by_components_options($systemId, $componentId, $optionsObj, $optionTraitsObj)
{

  
    $returnData = array();

    $params = array("system_id" => $systemId, "component_id" => $componentId );
    $allOptions = $optionsObj->getWhere($params);

    foreach ($allOptions as $options) {
        $data["id"] = $options["id"];
        $data["system_id"] = $options["system_id"];
        $data["component_id"] = $options["component_id"];
        $data["name"] = $options["name"];
        $data["allowDeleted"] = $options["allowDeleted"];
        $data["traits"] = get_by_components_option_traits($systemId, $componentId, $options["id"], $optionTraitsObj);
        $returnData[] = $data;
    }

  
    return $returnData ;
}

/**
 * Returns Options Traits
 *
 * @param systemId        $systemId        Where something interesting takes place
 * @param componentId     $componentId     How many times something interesting should happen
 * @param optionId        $optionId        Where something interesting takes place
 * @param $optionTraitsObj $optionTraitsObj How many times something interesting should happen
 *
 * @return Traits Array
 */
function get_by_components_option_traits($systemId, $componentId, $optionId, $optionTraitsObj)
{

  
    $returnData = array();

    $params = array("system_id" => $systemId, "component_id" => $componentId , "option_id" => $optionId );
    $allTraits = $optionTraitsObj->getWhere($params);

    foreach ($allTraits as $optionTraits) {
        $data["id"] = $optionTraits["id"];
        $data["system_id"] = $optionTraits["system_id"];
        $data["component_id"] = $optionTraits["component_id"];
        $data["option_id"] = $optionTraits["option_id"];
        $data["name"] = $optionTraits["name"];
        $data["visible"] = $optionTraits["visible"];
        $data["value"] = $optionTraits["value"];

        $returnData[] = $data;
    }

  
    return $returnData ;
}
