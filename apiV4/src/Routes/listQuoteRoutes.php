<?php
use SalesQuoter\ListBuilder\ListQuote;


$app->post(
    '/listQuote',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $listQuote = new ListQuote();
        $allListQuote = $listQuote->create($config);

        $responseData = array("status" => "success", 'data' => $allListQuote );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->delete(
    '/listQuote/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $listQuote = new ListQuote();
        $deletedOrder = $listQuote->delete($args['id']);
            
        return responseWithStatusCode($response, $deletedOrder, 200);
    }
);