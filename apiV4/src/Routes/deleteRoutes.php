<?php

use SalesQuoter\Delete\Delete;

$app->get(
    '/delete',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $delete = new Delete();
        $responseData = $delete->getAllEndPoint();
        $responses = array("status" => "success", "response_data" => $responseData);

        return responseWithStatusCode($response, $responses, 200);
    }
);


$app->get(
    '/delete/{table_name}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $delete = new Delete();
        $allGetVars = $request->getQueryParams();
        $items = 10;
        if (isset($allGetVars['page_no'])) {
            $pageNo = $allGetVars['page_no']-1;
        } else {
            $pageNo = 0;
        }

        if (isset($allGetVars['per_page'])) {
            $items = intval($allGetVars['per_page']);
        } else {
            $items = 10;
        }

        $result = $delete->getTableAll($args['table_name'], $items, $pageNo);

        return responseWithStatusCode($response, $result, 200);
    }
);



$app->delete(
    '/delete/{table_name}/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $delete = new Delete();
        $tableName = $args['table_name'];
        $id = $args['id'];

        $result = $delete->hardDelete($tableName, $id);

        return responseWithStatusCode($response, $result, 200);
    }
);
