<?php

use SalesQuoter\Defaults\Site;
use SalesQuoter\Defaults\Customer;

$app->get(
    '/default/site',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $site = new Site();
        $siteDefaults = $site->getAll();

        return responseWithStatusCode($response, $siteDefaults, 200);
    }
);


$app->put(
    '/default/site/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $site = new Site();
        $data = $request->getBody();

        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $siteDefaults = $site->update($config);
    
        return responseWithStatusCode($response, $siteDefaults, 200);
    }
);



$app->get(
    '/default/customer',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $customer = new Customer();
        $allDefaults = $customer->getAll();

        $responseData = array ( "status" => "success", "data" => $allDefaults );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->put(
    '/default/customer/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
    
        $customer = new Customer();
        $data = $request->getBody();

        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        unset($config['order']);
        unset($config['type']);

        $siteDefaults = $customer->update($config);

        return responseWithStatusCode($response, $siteDefaults, 200);
    }
);
