<?php
use SalesQuoter\ListBuilder\ListOrder;


$app->post(
    '/listOrder',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $listOrder = new ListOrder();
        $allListOrder = $listOrder->create($config);

        $responseData = array("status" => "success", 'data' => $allListOrder );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->delete(
    '/listOrder/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $listOrder = new ListOrder();
        $deletedOrder = $listOrder->delete($args['id']);
            
        return responseWithStatusCode($response, $deletedOrder, 200);
    }
);