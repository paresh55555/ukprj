<?php

use SalesQuoter\Pricing_traits\PricingTraits;

$app->get(
    '/pricingTraits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $pricingTraits = new PricingTraits();
        $allPricingTraits = $pricingTraits->getAll();

        $responseData = array('status' => 'success', 'response_data' => $allPricingTraits );

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->get(
    '/pricingTraits/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $pricingTraits = new PricingTraits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $singlePricingTraits = $pricingTraits->get($args['id'])[0];
      
            if (sizeof($singlePricingTraits) > 0) {
                 $singlePricingTraits['status'] = 'success';

                 return responseWithStatusCode($response, $singlePricingTraits, 200);
            } else {

                 $responseData = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found');
                 
                 return responseWithStatusCode($response, $responseData, 400);
            }
        }
    }
);



$app->delete(
    '/pricingTraits/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $pricingTraits = new PricingTraits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $pricingTraits->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->post(
    '/pricingTraits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $pricingTraits = new PricingTraits();

        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $pricingTraits->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->put(
    '/pricingTraits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $pricingTraits = new PricingTraits();
        $singlePricingTraits = $pricingTraits->get($config['id']);

        if (sizeof($singlePricingTraits) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $pricingTraits->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
