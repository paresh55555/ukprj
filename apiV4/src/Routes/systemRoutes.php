<?php

use SalesQuoter\Systems\Systems;
use SalesQuoter\Components\Components;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Options\Options;
use SalesQuoter\Version\Version;

$app->get(
    '/systems/{system_id}/components',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $components = new Components();
        $traits = new Traits();
        $options = new Options();

        $data['system_id'] = (int) $args['system_id'];

        if ($data['system_id'] == '0') {
             $code = 400;
             $responseData = array('status' => 'error', 'message' => 'System ID must be Valid Integer' );
        } else {
            $allComponents = $components->getWhere($data);
            for ($count = 0; $count < sizeof($allComponents); $count++) {
                  $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]["id"]));
                  $allComponents[$count]['options'] = $options->getOptionTraitsBYComponents($allComponents[$count]['id']);
            }

            $code = 200;
            $responseData = array('status' => 'success', 'response_data' => $allComponents );
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->get(
    '/systems/{system_id}/components/menu',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $components = new Components();
        $traits = new Traits();

        $data['system_id'] = (int) $args['system_id'];
        $data['name'] = 'Menu';

        if ($data['system_id'] == '0') {
             $code = 400;
             $responseData = array('status' => 'error', 'message' => 'System ID must be Valid Integer' );
        } else {
            $code = 200;
            $allComponents = $components->getWhere($data);
            for ($count = 0; $count < sizeof($allComponents); $count++) {
                  $allComponents[$count]['traits'] =   $traits->getWhere(array("component_id" => $allComponents[$count]["id"]));
            }

            $responseData = array('status' => 'success', 'response_data' => $allComponents );
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->get(
    '/systems/{system_id}/components/menu/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();

        $components = new Components();
        $traits = new Traits();
        $options = new options();

        $data['system_id'] = (int) $args['system_id'];
        $data['grouped_under'] = (int) $args['id'];

        if ($data['system_id'] == '0') {
             $code = 400;
             $responseData = array('status' => 'error', 'message' => 'System ID must be Valid Integer' );
        } else {
            $allComponents = $components->getWhere($data);
            for ($count = 0; $count < sizeof($allComponents); $count++) {
                $allComponents[$count]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['id']));
                $allComponents[$count]['options'] = $options->getOptionTraitsBYComponents($allComponents[$count]['id']);

                $singleComponents = &$allComponents[$count];

                if (strtolower($singleComponents['type']) === strtolower('Interior Color') ||
                    strtolower($singleComponents['type']) === strtolower('Exterior Color')) {
                    $colorComponent = &$singleComponents;
                    $colorComponent['groups'] = $components->getWhere(array("grouped_under" => $colorComponent['id'], 'type' => 'ColorGroup'));
                    foreach ($colorComponent['groups'] as $gkey => $group) {
                        $colorComponent['groups'][$gkey]['traits'] = $traits->getWhere(array("component_id" => $group['id']));
                        $colorComponent['groups'][$gkey]['options'] = $options->getOptionTraitsBYComponents($group['id']);
                        $subGroups = $components->getWhere(array("grouped_under" => $group['id'], 'type' => 'ColorSubGroup'));
                        foreach ($subGroups as $sgkey => $subGroup) {
                            $subGroups[$sgkey]['traits'] = $traits->getWhere(array("component_id" => $subGroup['id']));
                            $subGroups[$sgkey]['options'] = $options->getOptionTraitsBYComponents($subGroup['id']);
                        }
                        $colorComponent['groups'][$gkey]['subGroups'] = $subGroups;
                    }
                }
            }

            $code = 200;
            $responseData = array('status' => 'success', 'response_data' => $allComponents );
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);


$app->get(
    '/systems/{system_id}/components/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $traits = new Traits();
        $options = new options();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
       
            return responseWithStatusCode($response, $responseData, 404);
        } else {
            $data = array('system_id' => $args['system_id'], 'id' => $args['id']);

            $singleComponents = $components->getWhere($data);
      
            if (sizeof($singleComponents) > 0) {
                $singleComponents[0]['traits'] = $traits->getWhere(array("component_id" => $singleComponents[0]['id']));
                $singleComponents[0]['options'] = $options->getOptionTraitsBYComponents($singleComponents[0]['id']);

                if (strtolower($singleComponents[0]['type']) === strtolower('Interior Color') ||
                    strtolower($singleComponents[0]['type']) === strtolower('Exterior Color')) {
                    $colorComponent = &$singleComponents[0];
                    $colorComponent['groups'] = $components->getWhere(array("grouped_under" => $colorComponent['id'], 'type' => 'ColorGroup'));
                    foreach ($colorComponent['groups'] as $gkey => $group) {
                        $colorComponent['groups'][$gkey]['traits'] = $traits->getWhere(array("component_id" => $group['id']));
                        $colorComponent['groups'][$gkey]['options'] = $options->getOptionTraitsBYComponents($group['id']);
                        $subGroups = $components->getWhere(array("grouped_under" => $group['id'], 'type' => 'ColorSubGroup'));
                        foreach ($subGroups as $sgkey => $subGroup) {
                            $subGroups[$sgkey]['traits'] = $traits->getWhere(array("component_id" => $subGroup['id']));
                            $subGroups[$sgkey]['options'] = $options->getOptionTraitsBYComponents($subGroup['id']);
                        }
                        $colorComponent['groups'][$gkey]['subGroups'] = $subGroups;
                    }
                }
                if (strtolower($singleComponents[0]['type']) === strtolower('ColorGroup')) {
                    $group = &$singleComponents[0];
                    $subGroups = $components->getWhere(array("grouped_under" => $group['id'], 'type' => 'ColorSubGroup'));
                    foreach ($subGroups as $sgkey => $subGroup) {
                        $subGroups[$sgkey]['traits'] = $traits->getWhere(array("component_id" => $subGroup['id']));
                        $subGroups[$sgkey]['options'] = $options->getOptionTraitsBYComponents($subGroup['id']);
                    }
                    $group['subGroups'] = $subGroups;
                }

                $responseData = array('status' => 'success', 'response_data' => $singleComponents);
                
                return responseWithStatusCode($response, $responseData, 200);

            } else {
                $responseData = array('status' => 'error', 'message' => 'data not found');
                
                return responseWithStatusCode($response, $responseData, 404);
            }
        }
    }
);




$app->delete(
    '/systems/{system_id}/components/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $components = new Components();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $code = 400;
            $responseData = array('status' => 'failed', 'message' => 'Parameter ID must be an integer' );
        } else {
            $data = array('system_id' => $args['system_id'], 'id' => $args['id']);
            $singleComponents = $components->getWhere($data);
        
            if (sizeof($singleComponents) > 0) {
                 $code = 200;
                 $deleteComponents = $components->delete($args['id']);
                 $responseData = $deleteComponents;
            } else {
                 $code = 404;
                 $responseData = array('status' => 'failed', 'message' => 'data not found' );
            }
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->post(
    '/systems/{system_id}/components',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] =  $args['system_id'];
        $checkData = array(
        'system_id' => $args['system_id'],
        'type' => 'system',
        );

        $components = new Components();

        $singleSystem = $components->getWhere($checkData);

        if (sizeof($singleSystem) == '0') {
             $code = 404;
             $responseData = array('status' => 'error', 'message' => 'System ID Not Exists' );
        } else {
            if (intval($config['system_id']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $code = 200;
                $result =  $components->create($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        }
    

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->post(
    '/systems/{system_id}/components/menu',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $checkData = array(
            'id' => $args['system_id'],
        );

        $components = new Components();
        $traits = new Traits();

        $singleSystem = $components->getWhere($checkData);

        if (sizeof($singleSystem) == '0') {
            $code = 404;
            $responseData = array('status' => 'error', 'message' => 'System ID Not Exists');
        } else {
            $resultComponents = [];
            foreach ($config as $menu) {
                $menu['system_id'] =  $args['system_id'];
                if (intval($menu['system_id']) == '0') {
                    $code = 400;
                    $responseData = array('status' => 'error', 'message' => 'System ID Must be Valid Integer');
                } elseif (strlen($menu['name']) == '0') {
                    $code = 400;
                    $responseData = array('status' => 'error', 'message' => 'name can not be Empty');
                } else {
                    $menuTraits = $menu['traits'];
                    unset($menu['traits']);

                    if (isset($menu['id'])) {
                        $result = $components->update($menu);
                    } else {
                        $result = $components->create($menu);
                        $components->create([
                            'type' => 'NextTab',
                            'name' => 'NextTab',
                            'system_id' => $args['system_id'],
                            'grouped_under' => $result[0]['id'],
                            'menu_id' => 0,
                            'active' => 1,
                        ]);
                    }

                    $result[0]['traits'] = [];
                    foreach ($menuTraits as $trait) {
                        $trait['component_id'] = $result[0]['id'];
                        if (isset($trait['id'])) {
                            $result[0]['traits'] = $traits->update($trait);
                        } else {
                            $result[0]['traits'] = $traits->create($trait);
                        }
                    }
                    array_push($resultComponents, $result[0]);
                }
            }
            
            $code = 201;
            $responseData = array('status' => 'success', 'response_data' => $resultComponents);
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->post(
    '/systems/{system_id}/components/menu/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] = $args['system_id'];
        $config['grouped_under'] = $args['id'];
        $checkData = array(
            'type' => 'system',
            'id' => $args['system_id'],
        );

        $components = new Components();

        $singleSystem = $components->getWhere($checkData);

        if (sizeof($singleSystem) == '0') {
             $code = 404;
             $responseData = array('status' => 'error', 'message' => 'System ID Not Exists', );
        } else {
            if (intval($config['system_id']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (intval($config['grouped_under']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  'Menu ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'name can not be Empty');
            } else {
                $code = 201;
                $result =  $components->create($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->put(
    '/systems/{system_id}/components/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $checkData = array();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $checkData['system_id'] =  $args['system_id'];
        $config['id'] = $checkData['id'] =  $args['id'];

        $components = new Components();
        $singleComponents = $components->getWhere($checkData);

        if (sizeof($singleComponents) > 0) {
            if (intval($config['system_id']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'System ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
            } else {
                $code = 200;
                $result = $components->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $code = 400;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }
  

        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->get(
    '/systems/{system_id}/components/{component_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $components = new Components();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) > 0) {
            $code = 200;
            $where = array('component_id' => $args['component_id'] );
            $allTraits = $traits->getWhere($where);
            $responseData = array('status' => 'success', 'response_data' => $allTraits );
        } else {
            $code = 404;
            $responseData = array('status' => 'error', 'message' => 'system_id and components_id combination is not valid' );
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);


$app->get(
    '/systems/{system_id}/components/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        $data = array('component_id' => $args['component_id'], 'id' => $args['id']);
        $singleTraits = $traits->getWhere($data);

        if (sizeof($singleTraits) > 0) {
            $responseData = $singleTraits[0];

            return responseWithStatusCode($response, $responseData, 200);
        } else {
            $responseData = array("status" => "error");

            return responseWithStatusCode($response, $responseData, 400);
        }
    }
);



$app->delete(
    '/systems/{system_id}/components/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $code = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $isExists = $traits->checkSystemComponentsTraitsExits($args['system_id'], $args['component_id'], $args['id']);

            if (sizeof($isExists) > 0) {
                $code = 200;
                $deleteTraits = $traits->delete($args['id']);
                $responseData = $deleteTraits;
            } else {
                $code = 404;
                $responseData = array('status' => 'error', 'message' => 'Data not Exists');
            }
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['component_id'] = $args['component_id'];

        $traits = new Traits();
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $code = 400;
            $responseData = array('status' => 'error', 'message' =>  'system_id and components_id combination is not valid' );

            return responseWithStatusCode($response, $responseData, $code);
        }
  
        if (intval($config['component_id']) == '0') {
            $code = 400;
            $responseData = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
        } elseif (strlen($config['name']) == '0') {
            $code = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $code = 200;
            $result =  $traits->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }

    
        return responseWithStatusCode($response, $responseData, $code);
    }
);




$app->post(
    '/systems/{system_id}/components/{component_id}/multipleTraits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $code = 400;
            $responseData = array('status' => 'error', 'message' =>  'system_id and components_id combination is not valid' );

            return responseWithStatusCode($response, $responseData, $code);
        }
    
        $resultTraits = [];
        foreach ($config as $trait) {
            $trait['component_id'] = $args['component_id'];
            if (intval($trait['component_id']) == '0') {
                http_response_code(400);
                $responseData = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer');
            } elseif (strlen($trait['name']) == '0') {
                http_response_code(400);
                $responseData = array('status' => 'error', 'message' => 'name can not be Empty');
            } else {
                if (isset($trait['id'])) {
                    $result = $traits->update($trait);
                } else {
                    $result = $traits->create($trait);
                }

                array_push($resultTraits, $result[0]);
            }
        }

        $version = new Version();
        $version->updateVersion("adminSystemGroup", $args['system_id']);

        $responseData = array('status' => 'success', 'response_data' => $resultTraits);
    
        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->put(
    '/systems/{system_id}/components/{component_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['component_id'] = $args['component_id'];
        $config['id']  = $args['id'];

        $traits = new Traits();

        $checkExists = $traits->checkSystemComponentsTraitsExits($args['system_id'], $args['component_id'], $args['id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id , component_id and trait_id  combination is not correct' );

            return responseWithStatusCode($response, $responseData, 400); 
        }

        $singleTraits = $traits->get($args['id']);

        if (sizeof($singleTraits) > 0) {
            if (intval($config['component_id']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' => 'Component ID Must be Valid Integer' );
            } elseif (strlen($config['name']) == '0') {
                $code = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $code = 200;
                $result = $traits->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $code = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

        $version = new Version();
        $version->updateVersion("adminSystemGroup", $args['system_id']);

        return responseWithStatusCode($response, $responseData, $code);
    }
);
