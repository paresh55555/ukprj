<?php
use SalesQuoter\Service\JobNotes;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/jobs/{job_id}/notes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new JobNotes();


        $allNotes = $notes->getAll( ["job_id" => $args["job_id"] ] );

        $responseData = array ("status" => "success", "data" => $allNotes );


        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->post(
    '/service/jobs/{job_id}/notes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new JobNotes();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( strlen($config["note"]) == '0' ) {
            $responseData = [ "status" => "error", "msg" => "note can not be empty" ];
        }else {

            $config["job_id"] = $args["job_id"];
            $newNotes = $notes->create($config);

            $logMessage = "New notes has been updated" ;
            $activityLog->addLog ( $logMessage, $args['job_id'], "notes", "jobs" );

            $responseData = array ("status" => "success", "data" => $newNotes );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);

$app->put(
    '/service/jobs/{job_id}/notes/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new JobNotes();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( strlen($config["note"]) == '0' ) {
            $responseData = [ "status" => "error", "msg" => "note can not be empty" ];
        }else {

            $config["job_id"] = $args["job_id"];
            $config["id"] = $args["id"];

            $newNotes = $notes->update($config);

            $logMessage = "Notes has been updated" ;
            $activityLog->addLog ( $logMessage, $args['job_id'], "notes", "jobs" );

            $responseData = array ("status" => "success", "data" => $newNotes );
        }


        return responseWithStatusCode($response, $responseData, 200);

    }
);

$app->delete(
    '/service/jobs/{job_id}/notes/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $notes = new JobNotes();
        $intVal = intval($args['id']);


        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $notes->delete($args['id']);

            if ( $responseData["status"] == 'success') {

                $logMessage = "Notes has been deleted" ;
                $activityLog->addLog ( $logMessage, $args['job_id'], "notes", "jobs" );
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
