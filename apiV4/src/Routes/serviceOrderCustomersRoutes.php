<?php
use SalesQuoter\Service\OrderCustomers;
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\JobAddresses;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/orders/{order_id}/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orderCustomers = new OrderCustomers();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $allItems = $orderCustomers->getAll ( ["order_id" => $args["order_id"] ] );
            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/orders/{order_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orderCustomers = new OrderCustomers();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $allItems = $orderCustomers->getAll ( ["order_id" => $args["order_id"], "id" => $args["id"]] );
            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->post(
    '/service/orders/{order_id}/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orderCustomers = new OrderCustomers();
        $orders = new Orders();
        $jobAddresses = new JobAddresses();
        $activityLog = new ActivityLog();

        $checkOrderExists = $orders->get( $args["order_id"] );

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( strlen($config["first_name"]) == '0' || strlen($config["last_name"]) == '0' || strlen($config["phone"]) == '0') {

            $responseData = array ("status" => "error", "msg" => "customer fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["order_id"] = $args["order_id"];
            $insertAddress = $jobAddresses->insertJobAddress( $config );

            $config["customer_id"] = $insertAddress["customer_id"];
            $config["address_id"] = $insertAddress["address_id"];

            $newCustomer[] = $orderCustomers->create($config);

            $logMessage = "New customer has been added";

            $activityLog->addLog ( $logMessage, $args['order_id'], "customers", "orders", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newCustomer );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/orders/{order_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orderCustomers = new OrderCustomers();
        $orders = new Orders();
        $jobAddresses = new JobAddresses();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( strlen($config["first_name"]) == '0' || strlen($config["last_name"]) == '0' || strlen($config["phone"]) == '0') {

            $responseData = array ("status" => "error", "msg" => "customer fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];
            $config["order_id"] = $args["order_id"];

            $insertAddress = $jobAddresses->insertJobAddress( $config );
            
            $config["customer_id"] = $insertAddress["customer_id"];
            $config["address_id"] = $insertAddress["address_id"];
            
            $updatedCustomer[] = $orderCustomers->update($config);
            $responseData = array ("status" => "success", "data" => $updatedCustomer);

            $logMessage = "Customer has been updated";

            $activityLog->addLog ( $logMessage, $args['order_id'], "customers", "orders", $this->token->decoded->userId );

            return responseWithStatusCode($response, $responseData, 200);
        }
    }
);


$app->delete(
    '/service/orders/{order_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $orderCustomers = new OrderCustomers();
        $orders = new Orders();
        $activityLog = new ActivityLog();

        $checkOrderExists = $orders->get( $args["order_id"] );

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $orderCustomers->delete($args['id']);

            if ( $responseData["status"] == 'success' ) {

                $logMessage = "Customer has been deleted";
                $activityLog->addLog ( $logMessage, $args['order_id'], "customers", "orders", $this->token->decoded->userId );
            }

        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->post(
    '/service/customers/search',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $customer = new OrderCustomers();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $statusCode = 200;

        if ( strlen($config['search']) < '3' ) {

            $statusCode = 400;
            $responseData = array('status' => 'error', 'msg' => 'search string must be 3 character' );
        }else {
            $result = $customer->searchCustomer( $config['search'] );
            $responseData = array('status' => 'success', 'data' => $result );
        }

        return responseWithStatusCode($response, $responseData, $statusCode);
    }
);