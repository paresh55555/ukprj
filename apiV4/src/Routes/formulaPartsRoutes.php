<?php

use SalesQuoter\FormulaParts\FormulaParts;

$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $formulaParts = new FormulaParts();

        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id'] = $args['option_id'];
        $data['part_id'] = $args['part_id'];
        $data['cost_id'] = $args['cost_id'];

        $allFormulaParts = $formulaParts->getWhere($data);

        $responseData = array('status' => 'success', 'response_data' => $allFormulaParts );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $formulaParts = new FormulaParts();

        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $singleFormulaParts = $formulaParts->get($args['id']);

        if (sizeof($singleFormulaParts) > 0) {
             $responseCode = 200;
             $singleFormulaParts[0]['status'] = 'success';
             $responseData = $singleFormulaParts[0];
        } else {
             $responseCode = 404;
             $responseData = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $formulaParts = new FormulaParts();
        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        } 


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData =  $formulaParts->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['part_id'] = $args['part_id'];
        $config['cost_id'] = $args['cost_id'];

        $formulaParts = new FormulaParts();
        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $formulaParts->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->put(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{part_id}/costs/{cost_id}/formulaParts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['part_id'] = $args['part_id'];
        $config['cost_id'] = $args['cost_id'];
        $config['id'] = $args['id'];

        $formulaParts = new FormulaParts();
        $checkExists = $formulaParts->checkExits($args['system_id'], $args['component_id'], $args['option_id'], $args['part_id'], $args['cost_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id , Option_id ,  part_id and cost id  combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $formulaCosts = $formulaParts->get($config['id']);

        if (sizeof($formulaCosts) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $formulaParts->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  
        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
