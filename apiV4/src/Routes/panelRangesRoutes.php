<?php

use SalesQuoter\PanelRanges\PanelRanges;

$app->get(
    '/systems/{id}/panelRanges',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $systemId = (int) $args['id'];

        if ($systemId == '0') {
            $responseData = array("status" => "error", "message" => "Please Enter Correct System Id ");
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $panelRanges = new PanelRanges();
        $allPanelRanges = $panelRanges->getWhere(array("system_id" => $systemId ));

        $responseData = array ("status" => "success", "data" => $allPanelRanges);

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->get(
    '/systems/{system_id}/panelRanges/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $systemId = (int) $args['system_id'];
        $id = (int) $args['id'];

        if ($systemId == '0') {
             $responseData = array("status" => "error", "message" => "Please Enter Correct System Id ");
             return responseWithStatusCode($response, $responseData, 400); 
        }

        if ($id == '0') {
             $responseData = array("status" => "error", "message" => "Please Enter Correct panelRanges Id ");
             return responseWithStatusCode($response, $responseData, 400);
        }

        $panelRanges = new PanelRanges();
        $allPanelRanges = $panelRanges->getWhere(array("system_id" => $systemId , "id" => $id ));

        $responseData = array ("status" => "success", "data" => $allPanelRanges);

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->delete(
    '/systems/{system_id}/panelRanges/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $panelRanges = new PanelRanges();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $panelRanges->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{id}/panelRanges',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $systemId = (int) $args['id'];

        if ($systemId == '0') {
            $responseData = array("status" => "error", "message" => "Please Enter Correct System Id ");
        
            return responseWithStatusCode($response, $responseData, 400);
        }

   
        $config['system_id'] =  $systemId;
        $config['modified'] =  date("Y-m-d H:i:s");

        $panelRanges = new PanelRanges();
        $newPanelRanges = $panelRanges->create($config);

        $responseData = array ("status" => "success", "data" => $newPanelRanges);

        return responseWithStatusCode($response, $responseData, 201);
    }
);



$app->put(
    '/systems/{system_id}/panelRanges/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $systemId = (int) $args['system_id'];
        $id = (int) $args['id'];

        if ($systemId == '0') {
            $responseData = array("status" => "error", "message" => "Please Enter Correct System Id ");
            
            return responseWithStatusCode($response, $responseData, 400);
        }

        if ($id == '0') {
             $responseData = array("status" => "error", "message" => "Please Enter Correct panelRanges Id ");
             
             return responseWithStatusCode($response, $responseData, 400);
        }

   
        $config['system_id'] =  $systemId;
        $config['id'] =  $id;
        $config['modified'] =  date("Y-m-d H:i:s");

        $panelRanges = new PanelRanges();
        $newPanelRanges = $panelRanges->update($config);

        $responseData = array ("status" => "success", "data" => $newPanelRanges);

        return responseWithStatusCode($response, $responseData, 200);
    }
);
