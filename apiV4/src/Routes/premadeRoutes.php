<?php

use SalesQuoter\PremadeColor\PremadeColor;
use SalesQuoter\Components\Components;
use SalesQuoter\AdminTasks\AdminPreMadeColors;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Systems\Systems;

$app->get(
    '/premadecolors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $components = new Components();
        $premadeColor = new PremadeColor();
        $traits = new Traits();

        $data['type'] = "preMadeColorGroup";

        $allGroups = $components->getWhere($data);

        for ($count = 0; $count < sizeof($allGroups); $count++) {
              $allGroups[$count]['subGroups'] = $premadeColor->allSubGroups($allGroups[$count]['id']);
              $allGroups[$count]['colors'] = $premadeColor->getPremadeColors($allGroups[$count]['id']);
              $allGroups[$count]['traits'] = $traits->getWhere(array("component_id" => $allGroups[$count]['id']));
        }
 
        $responseData = array("status" => "success" , 'data' => $allGroups );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/premadecolors/copy',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $adminPreMadeColors = new AdminPreMadeColors();
        $results = $adminPreMadeColors->getCopy();

        $responseData = array("status" => "success" , 'data' => $results );

        return responseWithStatusCode($response, $responseData, 200);
    }
);

$app->post(
    '/premadecolors/copy',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = jsonDecodeWithErrorChecking($request->getBody());

        $adminPreMadeColors = new AdminPreMadeColors();
        $results = $adminPreMadeColors->createCopy($data);

        $responseData = array("status" => "success" , 'data' => $results );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/systemGroups/preMade',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getPremadeSystemGroups();

        for ($count = 0; $count < sizeof($allComponents); $count++) {
              $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
              $allComponents[$count]['systems'] = $components->getWhere(array("grouped_under" => $allComponents[$count]['id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }

        $responseData = array('status' => 'success', 'response_data' =>  $allComponents );

        return responseWithStatusCode($response, $responseData, 200);
    }
);


$app->get(
    '/systemGroups/sampleDoors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = array();
        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();

        $allComponents = $components->getPremadeSystemGroupOfDoors();

        for ($count = 0; $count < sizeof($allComponents); $count++) {
            $allComponents[$count]['traits'] =  $traits->getWhere(array("component_id" => $allComponents[$count]['id'] ));
            $allComponents[$count]['systems'] = $components->getWhere(array("grouped_under" => $allComponents[$count]['id'] ));

            for ($cout = 0; $cout < sizeof($allComponents[$count]['systems']); $cout++) {
                $allComponents[$count]['systems'][$cout]['traits'] = $traits->getWhere(array("component_id" => $allComponents[$count]['systems'][$cout]['id'] ));
            }
        }

        $responseData = array('status' => 'success', 'response_data' =>  $allComponents );
        
        return responseWithStatusCode($response, $responseData, 200);
    }
);
