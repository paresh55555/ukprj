<?php

use SalesQuoter\Components\ComponentTypes;

$app->get(
    '/componentTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $componentTypes = new ComponentTypes();
        $allComponentTypes = $componentTypes->getAll();

        return responseWithStatusCode($response, $allComponentTypes, 200);
    }
);



$app->get(
    '/componentTypes/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $componentTypes = new ComponentTypes();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            http_response_code(400);
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        
            return responseWithStatusCode($response, $responseData, 404);

        } else {
            $singleComponentsTypes = $componentTypes->get($args['id'])[0];
      
            if (sizeof($singleComponentsTypes) > 0) {
                $singleComponentsTypes['status'] = 'success';

                return responseWithStatusCode($response, $singleComponentsTypes, 404);
            } else {

                $responseData = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found');
                
                return responseWithStatusCode($response, $responseData, 404);
            }
        }
    }
);




$app->delete(
    '/componentTypes/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $componentTypes = new ComponentTypes();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteComponentTypes = $componentTypes->delete($args['id']);
            
            return responseWithStatusCode($response, $deleteComponentTypes, 200);
        }
    }
);




$app->post(
    '/componentTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $componentTypes = new ComponentTypes();

        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
        } else {
            $responseCode = 200;
            $result =  $componentTypes->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/componentTypes',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $componentTypes = new ComponentTypes();
        $singleComponentTypes = $componentTypes->get($config['id']);

        if (sizeof($singleComponentTypes) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $componentTypes->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
