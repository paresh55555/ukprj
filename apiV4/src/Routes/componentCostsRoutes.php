<?php

use SalesQuoter\Traits\Traits;
use SalesQuoter\Costs\OptionPartsCosts;

$app->get(
    '/systems/{system_id}/components/{component_id}/costs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
             $responseCode = 400;
             $responses = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        }else {
             
             $responseCode = 200;
             $data['system_id'] = $args['system_id'];
             $data['component_id'] = $args['component_id'];
             $data['option_id'] = 0;

             $allOptionPartsCosts = $optionPartsCosts->getWhere($data);

             $responses = array('status' => 'success', 'response_data' => $allOptionPartsCosts );

        }

        return responseWithStatusCode($response, $responses, $responseCode);
    }
);


$app->get(
    '/systems/{system_id}/components/{component_id}/costs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new Traits();
        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $singleOptionsParts = $optionPartsCosts->get($args['id']);

        if (sizeof($singleOptionsParts) > 0) {
             $responseCode = 200;
             $singleOptionsParts[0]['status'] = 'success';
             $responses = $singleOptionsParts[0];
        } else {
              $responseCode = 400;
              $responses = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        
        return responseWithStatusCode($response, $responses, $responseCode); 
    }
);



$app->delete(
    '/systems/{system_id}/components/{component_id}/costs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }


        $traits = new Traits();
        $optionPartsCosts = new OptionPartsCosts();
        
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responses = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responses =  $optionPartsCosts->delete($args['id']);
        }

        return responseWithStatusCode($response, $responses, $responseCode); 
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/costs',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = 0;

        $traits = new Traits();
        $optionPartsCosts = new OptionPartsCosts();
        
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 400);
        }


        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 201;
            $result =  $optionPartsCosts->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }

    
        return responseWithStatusCode($response, $responseData, $responseCode); 
    }
);



$app->put(
    '/systems/{system_id}/components/{component_id}/costs/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = 0;
        $config['id'] = $args['id'];

        $traits = new Traits();
        $optionPartsCosts = new OptionPartsCosts();
        
        $checkExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id and Component_id combination is not Correct');
            
            return responseWithStatusCode($response, $responseData, 400);
        }

        $singleOptionPartsCosts = $optionPartsCosts->get($config['id']);

        if (sizeof($singleOptionPartsCosts) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responses = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $optionPartsCosts->update($config);
                $responses = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responses = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  
        return responseWithStatusCode($response, $responses, $responseCode); 
    }
);
