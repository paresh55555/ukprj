<?php
use SalesQuoter\Images\Images;


$app->post(
    '/image',
    function ($request, $response, $args) {

        try {

            if (false === $this->token->hasScope(["admin"])) {
                return invalidPermissionsResponse($response);
            }

            $data = $request->getParsedBody();

            $systemId = (int) $data['system_id'];
            $componentId = (int) $data['component_id'];
            $optionId = (int) $data['option_id'];
            $traitId = (int) $data['trait_id'];
            $type = $data['type'];
            $height = (int) $data['height'];
            $width = (int) $data['width'];

            $base64String = $data['file'];
            if ($height == '0' || $width == '0') {
                $responseData = array('status' => 'error', 'message' => 'Enter Both height & width dimension');
           
                return responseWithStatusCode($response, $responseData, 400);
            } elseif ($type == 'option') {
                if ($systemId == '0' || $optionId == '0' || $traitId == '0' || $componentId == '0') {
                    $responseData = array('status' => 'error', 'message' => 'Enter system_id, component_id, Option_id and trait id');
                
                    return responseWithStatusCode($response, $responseData, 400);
                }
            } elseif ($type == 'component') {
                if ($systemId == '0' || $traitId == '0' || $componentId == '0') {
                    $responseData = array('status' => 'error', 'message' => 'Enter system_id, component_id and trait id');
                
                    return responseWithStatusCode($response, $responseData, 400);
                }
            } elseif ($type == 'system') {
                if ($systemId == '0' || $traitId == '0') {
                    $responseData = array('status' => 'error', 'message' => 'Enter system_id and trait id');
                
                    return responseWithStatusCode($response, $responseData, 400);
                }
            } elseif ($type == 'siteBranding') {
                $traitId = 1;
                if ($traitId == '0') {
                    $responseData = array('status' => 'error', 'message' => 'Enter trait id');

                    return responseWithStatusCode($response, $responseData, 400);
                }
            } elseif (strlen($base64String) < 5) {
                $responseData = array('status' => 'error', 'message' => 'Empty Image ');

                return responseWithStatusCode($response, $responseData, 400);
            }

            $error = 0;


            if ( $_SERVER['IMAGE_STORE'] == null || strlen($_SERVER['IMAGE_STORE']) == '0' ) {

                throw new ImageException($_SERVER['IMAGE_STORE']);   
            }else {
                $uploadPath =  $_SERVER['IMAGE_STORE'];
            }  
             

            $uploadFolder = pathinfo($uploadPath)['filename'];
            mkdir($uploadPath);
            $uploadPath = $uploadPath.'/'.$type;
            $folderCreated1 =  mkdir($uploadPath);
            $uploadPath .= "/".$systemId;
            $folderCreated2 =  mkdir($uploadPath);
            $uploadPath .= "/".$componentId;
            $folderCreated3 =  mkdir($uploadPath);
            $uploadPath .= "/".$optionId;
            $folderCreated4 =  mkdir($uploadPath);
            $uploadPath .= "/".$traitId;
            $folderCreated5 =  mkdir($uploadPath);
        
            //data:image/png;base64,

            $data = explode(',', $base64String);
            $extensionPortion = explode(";", $data[0]);
            $extension = str_replace("data:image/", "", $extensionPortion[0]);
            $validExtension = array("jpg", "jpeg", "SVG", "svg", "png", "gif", "JPG", "JPEG", "svg+xml");

            if ($extension == 'svg+xml') {
                 $extension = 'svg';
            }

            if (!in_array($extension, $validExtension)) {
                $error = 1;
                $msg = "Please do Uplad Image In Correct Format. ";
            }


            $images = new Images();
            $outputFile = $images->getFileVersionUrl($uploadPath, $extension);

            $ifp = fopen($outputFile, "wb");

            $writeFile = fwrite($ifp, base64_decode($data[1]));
            if ($writeFile === false) {
                 $error = 1;
                 $msg = " File is not written in desired folder. Pls check File Permissions ";
            }

            fclose($ifp);

            if ($error == '1') {

                $responseData = array(
                    'status' => 'error',
                    'msg' => $msg,
                );

                return responseWithStatusCode($response, $responseData, 200);

            } else {
                if ($extension == 'svg') {

                    $responseData = array(
                        'status' => 'success',
                        'imgUrl' => parseImageName($uploadFolder, $outputFile),
                    );

                    return responseWithStatusCode($response, $responseData, 200);
                } else {
                    $responseData = array(
                        'status' => 'success',
                        'imgUrl' => parseImageName($uploadFolder, resizeImage($outputFile, $width, $height)),
                    );

                    return responseWithStatusCode($response, $responseData, 200);
                }
            }

        } catch ( ImageException $e ) {

                $responseData = array(
                    'status' => 'error',
                    'msg' => 'Server Variable IMAGE_STORE is not set',
                );

                return $responseData;
        } 

    }
);
