<?php

use SalesQuoter\Version\Version;

$app->get('/version/{type}/{id}', function ($request, $response, $args) {

    if (false === $this->token->hasScope(["all"])) {
        return invalidPermissionsResponse($response);
    }

        $version = new Version();

        $where = array();
        $where['type'] = $args['type'];
        $where['_id'] = $args['id'];

        $checkDataExists = $version->getWhere( $where['type'],  $where['_id'] );

        if (sizeof($checkDataExists) > '0') {
            $responseCode = 200; 
            $responseData = array ( "status" => "success", "response_data" => $checkDataExists );
        } else {
            $responseCode = 404;
            $responseData = array ( "status" => "error", "message" => "Data Not Found");
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
});
