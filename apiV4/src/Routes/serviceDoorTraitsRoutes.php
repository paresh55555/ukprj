<?php
use SalesQuoter\Service\DoorTraits;
use SalesQuoter\Service\Orders;

$app->get(
    '/service/orders/{order_id}/doors/{door_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new DoorTraits();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $allItems = $traits->getWhere ( ["door_id" => $args["door_id"]] );

            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/orders/{order_id}/doors/{door_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new DoorTraits();
        $allItems = $traits->getWhere ( ["door_id" => $args["door_id"], "id" => $args["id"] ] );

        $responseData = array ("status" => "success", "data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->post(
    '/service/orders/{order_id}/doors/{door_id}/traits',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new DoorTraits();
        $orders = new Orders();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( strlen($config["name"]) == '0' ) {
            $responseData = [ "status" => "error", "msg" => "name can not be empty" ];
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["door_id"] = $args["door_id"];
            $newTraits = $traits->create($config);

            $responseData = array ("status" => "success", "data" => $newTraits );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/orders/{order_id}/doors/{door_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new DoorTraits();
        $orders = new Orders();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];
            $config["door_id"] = $args["door_id"];

            $checkOrderExists = $orders->get( $args["order_id"] );
            
            $updatedTraits = $traits->update($config);

            $responseData = array ("status" => "success", "data" => $updatedTraits);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->delete(
    '/service/orders/{order_id}/doors/{door_id}/traits/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $traits = new DoorTraits();
        $orders = new Orders();

        $intVal = intval($args['id']);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $traits->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
