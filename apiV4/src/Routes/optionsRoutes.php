<?php

use SalesQuoter\Options\Options;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Traits\OptionTraits;
use SalesQuoter\Components\Components;

$app->get(
    '/systems/{system_id}/components/{component_id}/options',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $options = new Options();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
             
            return responseWithStatusCode($response, $responseData, 400);
        }

        $data = array();
        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];

        $allOptions = $options->getWhere($data);

        $returnData = array();
        foreach ($allOptions as $option) {
             $return['name'] = $option['name'];
             $return['id'] = $option['id'];
             $return['component_id'] = $option['component_id'];
             $return['traits'] = $options->getOptionTratis($option['id']);
             $returnData[] = $return;
        }


        $responseData = array('status' => 'success', 'response_data' => $returnData );

        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->get(
    '/systems/{system_id}/components/{component_id}/options/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $options = new Options();
        $intVal = intval($args['id']);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
            
            return responseWithStatusCode($response, $responseData, 400);
        }



        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $data['system_id'] = $args['system_id'];
            $data['component_id'] = $args['component_id'];
            $data['id']  = $args['id'];

            $$responseData = $options->getWhere($data)[0];
      
            if (sizeof($singleOptions) > 0) {
                 $responseCode = 200;
                 $$responseData['status'] = 'success';
            } else {
                 $responseCode = 404;
                 $responseData = array('status' => 'error', 'response_data' => array() , 'message' => 'data not found');
            }
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $options = new Options();
        $intVal = intval($args['id']);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
             
            return responseWithStatusCode($response, $responseData, 400);
        }

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $deleteOptions = $options->delete($args['id']);
            $responseData = $deleteOptions;
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->post(
    '/systems/{system_id}/components/{component_id}/options',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];

        $options = new Options();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
            
            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseCode = 400;
        if (strlen($config['name']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
        } elseif (intval($config['system_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($config['component_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            $responseCode = 200; 
            $result =  $options->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }
    

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->post(
    '/systems/{system_id}/components/{component_id}/premadeColors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $colors = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
         
            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseCode = 400;
        if (intval($args['system_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($args['component_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            $responseCode = 200;
            saveColors($colors, $args['system_id'], $args['component_id']);
            $responseData = array('status' => 'success', 'response_data' => []);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/premadeGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $groups = jsonDecodeWithErrorChecking($data);

        $components = new Components();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseCode = 400;
        if (intval($args['system_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($args['component_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
            foreach ($groups as $group) {
                $groupConfig = [
                'name' => 'ColorGroup',
                'type' => 'ColorGroup',
                'system_id' => $args['system_id'],
                'grouped_under' => $args['component_id'],
                ];

                $groupResult = $components->create($groupConfig);
                $groupID = $groupResult[0]['id'];
                foreach ($group['traits'] as $trait) {
                    $traits->create(
                        [
                        'name' => $trait['name'],
                        'value' => $trait['value'],
                        'component_id' => $groupID,
                        'type' => 'component',
                        ]
                    );
                }

                if ($group['colors'] && !empty($group['colors'])) {
                    //save colors for group
                    saveColors($group['colors'], $args['system_id'], $groupID);
                } elseif ($group['subGroups']) {
                    // save subgroups with colors
                    saveSubGroups($group['subGroups'], $args['system_id'], $groupID);
                }
            }
            $responseCode = 200;
            $responseData = array('status' => 'success', 'response_data' => []);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/premadeSubGroups',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $subGroups = jsonDecodeWithErrorChecking($data);

        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);

        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
        
            return responseWithStatusCode($response, $responseData, 400);
        }

        $responseCode = 400;
        if (intval($args['system_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
        } elseif (intval($args['component_id']) == '0') {
            $responseData = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
        } else {
 
            saveSubGroups($subGroups, $args['system_id'], $args['component_id']);
            $responseCode = 201;
            $responseData = array('status' => 'success', 'response_data' => []);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


$app->put(
    '/systems/{system_id}/components/{component_id}/options/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];

        $options = new Options();
        $traits = new Traits();

        $checkDataExists = $traits->checkSystemComponentsExits($args['system_id'], $args['component_id']);
    
        if (sizeof($checkDataExists) == '0') {
            $responseData = array( "status" => "error",  "message" => "system_id or component_id combination is not correct" );
            
            return responseWithStatusCode($response, $responseData, 400);
        }


        $singleOptions = $options->get($config['id']);

        if (sizeof($singleOptions) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } elseif (intval($config['system_id']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'system_id must be valid Integer' );
            } elseif (intval($config['component_id']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'component_id must be valid Integer' );
            } else {
                $responseCode = 200;
                $result = $options->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
