<?php
use SalesQuoter\Service\DoorCustomers;
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\JobAddresses;

$app->get(
    '/service/orders/{order_id}/doors/{door_id}/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorCustomers = new DoorCustomers();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else { 

            $allItems = $doorCustomers->getAll ( ["door_id" => $args["door_id"] ] );
            $responseData = array ("status" => "success", "data" => $allItems );

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);

$app->get(
    '/service/orders/{order_id}/doors/{door_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorCustomers = new DoorCustomers();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $allItems = $doorCustomers->getAll ( ["order_id" => $args["order_id"], "id" => $args["id"]] );
            $responseData = array ("status" => "success", "data" => $allItems);

            return responseWithStatusCode($response, $responseData, 200);
        }

    }
);


$app->post(
    '/service/orders/{order_id}/doors/{door_id}/customers',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorCustomers = new DoorCustomers();
        $orders = new Orders();
        $jobAddresses = new JobAddresses();

        $checkOrderExists = $orders->get( $args["order_id"] );

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( strlen($config["first_name"]) == '0' || strlen($config["last_name"]) == '0' || strlen($config["phone"]) == '0') {

            $responseData = array ("status" => "error", "msg" => "customer fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["door_id"] = $args["door_id"];

            $insertAddress = $jobAddresses->insertAddress( $config );

            $config["customer_id"] = $insertAddress["customer_id"];
            $config["address_id"] = $insertAddress["address_id"];

            $newCustomer[] = $doorCustomers->create($config);

            $responseData = array ("status" => "success", "data" => $newCustomer );
        }


        return responseWithStatusCode($response, $responseData, 201);

    }
);


$app->put(
    '/service/orders/{order_id}/doors/{door_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorCustomers = new DoorCustomers();
        $orders = new Orders();
        $jobAddresses = new JobAddresses();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $checkOrderExists = $orders->get( $args["order_id"] );

        if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        }else if ( strlen($config["first_name"]) == '0' || strlen($config["last_name"]) == '0' || strlen($config["phone"]) == '0') {

            $responseData = array ("status" => "error", "msg" => "customer fields are mandatory");

            return responseWithStatusCode($response, $responseData, 400);
        }else {

            $config["id"] = $args["id"];
            $config["door_id"] = $args["door_id"];
            $insertAddress = $jobAddresses->insertAddress( $config );

            $config["customer_id"] = $insertAddress["customer_id"];
            $config["address_id"] = $insertAddress["address_id"];
            
            $updatedCustomer[] = $doorCustomers->update($config);

            $responseData = array ("status" => "success", "data" => $updatedCustomer);

            return responseWithStatusCode($response, $responseData, 200);
        }
    }
);


$app->delete(
    '/service/orders/{order_id}/doors/{door_id}/customers/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $doorCustomers = new DoorCustomers();
        $orders = new Orders();

        $checkOrderExists = $orders->get( $args["order_id"] );

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        }else if ( sizeof($checkOrderExists) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "invalid order id");

            return responseWithStatusCode($response, $responseData, 400);
        } else {
            $responseCode = 200;
            $responseData = $doorCustomers->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);