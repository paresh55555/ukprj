<?php

use SalesQuoter\PriceEngine\PriceEngine;
use SalesQuoter\Cache\Cache;

$app->post(
    '/price',
    function ($request, $response, $args) {

  
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $totalPrice = 0;

        $cache = new Cache();
        $priceEngine = new PriceEngine();

        $where["system_id"] = $config["system"];
        $where["type"] = "systemPrice";
        $where["current"] = true;

        $componentsArray  = $config['components'];

        $priceDetails = $cache->getWhere($where);
        if (sizeof($priceDetails) > '0') {
             $where["type"] = "systemUX";
             $uxRoutesData =  $cache->getWhere($where)[0]['cachedArray'];

             $details = $priceEngine->calculatePrice($priceDetails[0]['cachedArray'], $componentsArray, $uxRoutesData);
        }

        if (empty($details['price'])) {
            $details['price'] = 999.98;
        }

        $responseData = array('status' => 'success', 'price' =>  $details['price'] );
        
        return responseWithStatusCode($response, $responseData, 200);
    }
);




$app->post(
    '/price/details',
    function ($request, $response, $args) {

  
        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $totalPrice = 0;

        $cache = new Cache();
        $priceEngine = new PriceEngine();

        $where["system_id"] = $config["system"];
        $where["type"] = "systemPrice";
        $where["current"] = true;

        $componentsArray  = $config['components'];

        $priceDetails = $cache->getWhere($where);
        if (sizeof($priceDetails) > '0') {
             $where["type"] = "systemUX";
             $uxRoutesData =  $cache->getWhere($where)[0]['cachedArray'];

             $details = $priceEngine->calculatePrice($priceDetails[0]['cachedArray'], $componentsArray, $uxRoutesData);
        }

        return responseWithStatusCode($response, $details, 200);
    }
);
