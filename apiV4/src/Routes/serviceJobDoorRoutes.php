<?php
use SalesQuoter\Service\JobsDoors;
use SalesQuoter\Service\Doors;
use SalesQuoter\Service\Orders;
use SalesQuoter\Service\DoorTraits;
use SalesQuoter\Service\DoorTypes;
use SalesQuoter\Service\DoorNotes;
use SalesQuoter\Service\DoorCustomers;
use SalesQuoter\Service\ActivityLog;

$app->get(
    '/service/jobs/{id}/doors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobDoor = new JobsDoors();
        $traits = new DoorTraits();
        $notes = new DoorNotes();
        $doorCustomer = new DoorCustomers();

        $allItems = $jobDoor->getAll( [ "job_id" => $args["id"] ] );

        for ($count = 0; $count < sizeof($allItems); $count++) {

            $allItems[$count]["traits"] = $traits->getWhere ( ["door_id" => $allItems[$count]["door_id"]] );
            $allItems[$count]["notes"] =  $notes->getWhere ( ["door_id" => $allItems[$count]["door_id"]] );
            $allItems[$count]["customers"] = $doorCustomer->getAll ( ["door_id" => $allItems[$count]["door_id"] ] );
        }

        $responseData = array ("status" => "success", "data" => $allItems );

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->post(
    '/service/jobs/{id}/doors',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $newJobDoor = new JobsDoors();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);


        if ( strlen($config["order_id"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "start date cannot be empty");

            return responseWithStatusCode($response, $responseData, 400);
        }else{

            $config["job_id"] = $args["id"];

            $newJob = $newJobDoor->create($config);

            $logMessage = "New door has been added" ;
            $activityLog->addLog ( $logMessage, $args['id'], "door", "jobs", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newJob );

            return responseWithStatusCode($response, $responseData, 201);
        }
    }
);


$app->put(
    '/service/jobs/{job_id}/doors/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobDoor = new JobsDoors();
        $activityLog = new ActivityLog();

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);


        if ( strlen($config["start_date"]) == '0' ) {

            $responseData = array ("status" => "error", "msg" => "start date cannot be empty");

            return responseWithStatusCode($response, $responseData, 400);
        }else{

            $config["id"] = $args["id"];
            $config["job_id"] = $args["job_id"];

            $newJobDoor = $jobDoor->update($config);

            $logMessage = "New door has been updated" ;
            $activityLog->addLog ( $logMessage, $args['job_id'], "door", "jobs", $this->token->decoded->userId );

            $responseData = array ("status" => "success", "data" => $newJobDoor );

            return responseWithStatusCode($response, $responseData, 200 );
        }
    }
);


$app->delete(
    '/service/jobs/{job_id}/doors/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobDoor = new JobsDoors();

        $responseCode = 200;
        $responseData = $jobDoor->delete($args['id']);

        $logMessage = "Door has been deleted" ;
        $activityLog->addLog ( $logMessage, $args['job_id'], "door", "jobs", $this->token->decoded->userId );

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
