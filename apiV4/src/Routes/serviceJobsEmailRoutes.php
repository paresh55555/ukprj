<?php
use SalesQuoter\Service\JobEmails;

$app->post(
    '/service/jobs/{jobs_id}/sendEmail',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $allUploadedFiles = $request->getUploadedFiles();

        $toEmail = $request->getParam('to');
        $subject = $request->getParam('subject');
        $message = $request->getParam('message');

        $fileName = [];
        $files = [];

        foreach ( $allUploadedFiles["file"] as $uploadedFiles ) {

            $fileName[] = $uploadedFiles->getClientFilename();
            $file[] = $uploadedFiles->file ;
        }

        $isMailSent = sendApiEmail('', $toEmail, $subject, $message, $fileName, $file);
        $jobEmails = New JobEmails();

        if ( $isMailSent ) {

            $statusCode = 200;
            
            $jobEmails = New JobEmails();

            $insertData = [
                "jobs_id" => $args["jobs_id"],
                "email" => $toEmail,
                "subject" => $subject,
                "message" => $message,
                "user_id" => $this->token->decoded->userId,
                "datetime" => date("Y-m-d H:i:s")
            ];

            $data = $jobEmails->create ( $insertData );

            if ( sizeof($data) > '0') {

                $jobEmails->insertJobFiles ( $args["jobs_id"], $data[0]['id'], $file, $fileName );
            }

            $responseData = array('status' => 'success', 'msg' => 'email successfully sent' );
        }else {
            $statusCode = 400;
            $responseData = array('status' => 'error', 'msg' => 'something went wrong in sending email' );
        }

       

        return responseWithStatusCode($response, $responseData, $statusCode);
    }
);


$app->get(
    '/service/jobs/{jobs_id}/sendEmail',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $jobEmails = New JobEmails();

        $data = $jobEmails->getWhere( ["jobs_id" => $args["jobs_id"] ] );

        for ( $count = 0; $count < sizeof($data); $count++ ) {

            $data[$count]["files"] = $jobEmails->getAllFiles ( $data[$count]["id"] );
        }


        $responseData = array('status' => 'success', 'data' => $data );
       

        return responseWithStatusCode($response, $responseData, 200);
    }
);