<?php

use SalesQuoter\Traits\Traits;
use SalesQuoter\Parts\OptionParts;
use SalesQuoter\Costs\OptionPartsCosts;

$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $optionParts = new OptionParts();
        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $optionParts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }
 
        $data['system_id'] = $args['system_id'];
        $data['component_id'] = $args['component_id'];
        $data['option_id'] = $args['option_id'];

        $allOptionParts = $optionParts->getWhere($data);

        for ($count = 0; $count < sizeof($allOptionParts); $count++) {
              $allOptionParts[$count]['costs'] =   $optionPartsCosts->getAllCosts($allOptionParts[$count]['id']);
        }

        $responseData = array('status' => 'success', 'response_data' => $allOptionParts );

        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->get(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $optionParts = new OptionParts();
        $optionPartsCosts = new OptionPartsCosts();

        $checkExists = $optionParts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        $singleOptionsParts = $optionParts->get($args['id']);

        if (sizeof($singleOptionsParts) > 0) {
             $responseCode = 200;
             $singleOptionsParts[0]['status'] = 'success';
             $singleOptionsParts[0]['costs'] =   $optionPartsCosts->getAllCosts($singleOptionsParts[0]['id']);
             $responseData = $singleOptionsParts[0];
        } else {
              $responseCode = 400;
              $responseData = array('status' => 'error', 'message' => 'data not found', 'response_data' => array());
        }
        

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->delete(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $optionParts = new OptionParts();
        $checkExists = $optionParts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode  = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData =  $optionParts->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);



$app->post(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];

        $optionParts = new OptionParts();
        $checkExists = $optionParts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        if (strlen($config['name']) == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' =>  ' name can not be Empty' );
        } else {
            $responseCode = 201;
            $result =  $optionParts->create($config);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);




$app->put(
    '/systems/{system_id}/components/{component_id}/options/{option_id}/parts/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
      
        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $config['system_id'] = $args['system_id'];
        $config['component_id'] = $args['component_id'];
        $config['option_id'] = $args['option_id'];
        $config['id'] = $args['id'];

        $optionParts = new OptionParts();
        $checkExists = $optionParts->checkSystemComponentsOptionsExits($args['system_id'], $args['component_id'], $args['option_id']);

        if (sizeof($checkExists) == '0') {
            $responseData = array('status' => 'error', 'message' => 'System_id , Component_id and Option_id combination is not Correct');
        
            return responseWithStatusCode($response, $responseData, 200);
        }


        $singleOptionParts = $optionParts->get($config['id']);

        if (sizeof($singleOptionParts) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  ' Name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $optionParts->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }

  
        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);
