<?php

use SalesQuoter\Cache\Cache;

$app->get('/system/{system_id}/cache/{id}/revert', function ($request, $response, $args) {

    if (false === $this->token->hasScope(["admin"])) {
        return invalidPermissionsResponse($response);
    }

        $cache = new Cache();

        $where = array();
        $where['system_id'] = $args['system_id'];
        $where['id'] = $args['id'];

        $checkDataExists = $cache->getWhere($where);

    if (sizeof($checkDataExists) > '0') {
        $code = 200;
        $type = $checkDataExists[0]['type'];
        $cachedArray = $checkDataExists[0]['cachedArray'];

        $cache->revertCache($type, $cachedArray);

        $responseData = array ("stauts" => "success", "message" => "Type ".$type." Revert Done" );
    } else {
        $code = 400;
        $responseData = array ( "status" => "error", "message" => "Data Not Found");
    }

        return responseWithStatusCode($response, $responseData, $code);
});
