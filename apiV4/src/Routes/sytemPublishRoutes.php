<?php

use SalesQuoter\Options\Options;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Systems\Systems;
use SalesQuoter\Components\Components;
use SalesQuoter\Traits\OptionTraits;
use SalesQuoter\SystemsPublish\SystemsPublish;
use SalesQuoter\Parts\OptionParts;
use SalesQuoter\Costs\OptionPartsCosts;
use SalesQuoter\FormulaParts\FormulaParts;

$app->post(
    '/systems/{system_id}/publish',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $datas = $request->getBody();
        $config = jsonDecodeWithErrorChecking($datas);
    
        $systemsPublish = new SystemsPublish();
        $components = new Components();
        $traits = new Traits();
        $options = new Options();
        $optionTraits = new OptionTraits();



        $data["name"] = $config["name"];
        $data["system_id"] = $args["system_id"];
        $data["current"] = true;
        $data["ux"] = serialize(get_by_components($data["system_id"], $components, $traits, $options, $optionTraits));
        $data["price"] =  serialize(get_components_price($args["system_id"], $components));

        $action =  $config["action"];
        if ($action == 'new') {
            $code = 200;
            $systemsPublish->updateAllSystemFalse($args["system_id"]);
            $result =  $systemsPublish->create($data);
            $result[0]['ux'] =  unserialize($result[0]['ux']);
            $result[0]['price'] =  unserialize($result[0]['price']);
            $responseData = array('status' => 'success', 'response_data' =>  $result );
        } else {
            $code = 400;
            $responseData = array('status' => 'error', 'message' =>  'Not a Valid Action' );
        }

        return responseWithStatusCode($response, $responseData, $code);
    }
);



$app->post(
    '/systems/{system_id}/duplicate',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $datas = $request->getBody();
        $config = jsonDecodeWithErrorChecking($datas);
    
        $newSystemName = $config["name"];
        $groupedUnder = $config["grouped_under"];
        $systemId = $args["system_id"];

        $systems = new Systems();
        $responses = $systems->createDuplicate($newSystemName, $systemId, $groupedUnder);

        $responseData = array('status' => 'success', 'response_data' =>  $responses );


        return responseWithStatusCode($response, $responseData, 200);
    }
);



$app->post(
    '/systems/{id}/duplicate/createTemplate',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $datas = $request->getBody();
        $config = jsonDecodeWithErrorChecking($datas);
    
        $newSystemName = $config["name"];
        $systemId = $args["id"];

        $systems = new Systems();
        $responses = $systems->createDuplicate($newSystemName, $systemId);

        $responseData = array('status' => 'success', 'response_data' =>  $responses );


        return responseWithStatusCode($response, $responseData, 200);
    }
);


/**
 * Returns Components Price
 *
 * @param systemId      $systemId      Where something interesting takes place
 * @param componentsObj $componentsObj Where something interesting takes place
 *
 * @return Components Array
 */
function get_components_price($systemId, $componentsObj)
{

    $optionsObj = new Options();
    $partsObj = new OptionParts();
    $partsCostsObj = new OptionPartsCosts();
    $formulaPartsObj = new FormulaParts();

    $returnData = array();

    $params = array("system_id" => $systemId);
    $allSysComponents = $componentsObj->getWhere($params);

    foreach ($allSysComponents as $components) {
        $data["id"] = $components["id"];
        $data["menu_id"] = $components["menu_id"];
        $data["system_id"] = $components["system_id"];
        $data["component_name"] = $components["name"];
        $data["type"] = $components["type"];
        $data["grouped_under"] = $components["grouped_under"];
        $data["required"] = $components["required"];
        $data["multiple"] = $components["multiple"];
        $data["options"] = get_components_options_price($systemId, $components["id"], $optionsObj, $partsObj, $partsCostsObj, $formulaPartsObj);
        $returnData[] = $data;
    }

  
    return $returnData ;
}


/**
 * Returns Components Options Traits
 *
 * @param systemId        $systemId        System ID
 * @param componentId     $componentId     Components ID
 * @param optionsObj      $optionsObj      Class instance of SQ_new_options
 * @param partsObj        $partsObj        Class instance of SQ_parts
 * @param partsCostsObj   $partsCostsObj   Class instance of parts costs
 * @param formulaPartsObj $formulaPartsObj Class instance of formula_parts
 *
 * @return Components Array
 */
function get_components_options_price($systemId, $componentId, $optionsObj, $partsObj, $partsCostsObj, $formulaPartsObj)
{

    
    $returnData = array();

    $params = array("system_id" => $systemId, "component_id" => $componentId );
    $allSysCompOptions = $optionsObj->getWhere($params);

    foreach ($allSysCompOptions as $options) {
        $data["id"] = $options["id"];
        $data["system_id"] = $options["system_id"];
        $data["component_id"] = $options["component_id"];
        $data["name"] = $options["name"];
        $data['allowDeleted'] = $options['allowDeleted'];
        $data["parts"] = get_components_options_parts_price($systemId, $componentId, $options["id"], $partsObj, $partsCostsObj, $formulaPartsObj);
        $returnData[] = $data;
    }

  
    return $returnData ;
}

/**
 * Returns Options Parts
 *
 * @param systemId        $systemId        Where something interesting takes place
 * @param componentId     $componentId     How many times something interesting should happen
 * @param optionId        $optionId        Where something interesting takes place
 * @param partsObj        $partsObj        Where something interesting takes place
 * @param partsCostsObj   $partsCostsObj   Where something interesting takes place
 * @param formulaPartsObj $formulaPartsObj Where something interesting takes place
 *
 * @return Components Array
 */
function get_components_options_parts_price($systemId, $componentId, $optionId, $partsObj, $partsCostsObj, $formulaPartsObj)
{

    
    $returnData = array();

    $params = array("system_id" => $systemId, "component_id" => $componentId, "option_id" => $optionId );
    $allParts = $partsObj->getWhere($params);

    foreach ($allParts as $parts) {
        $data["id"] = $parts["id"];
        $data["system_id"] = $parts["system_id"];
        $data["component_id"] = $parts["component_id"];
        $data["option_id"] = $parts["option_id"];
        $data["name"] = $parts["name"];
        $data['description'] = $parts['description'];
        $data['costs'] = get_components_options_parts_costs_price($systemId, $componentId, $optionId, $parts["id"], $partsCostsObj, $formulaPartsObj);
        $returnData[] = $data;
    }

  
    return $returnData ;
}

/**
 * Returns Parts Costs
 *
 * @param systemId        $systemId        id of SQ_components where type is system
 * @param componentId     $componentId     SQ_components table primary key id
 * @param optionId        $optionId        SQ_new_options table id
 * @param partsId         $partsId         SQ_parts table id
 * @param partsCostsObj   $partsCostsObj   class instance of parts cost
 * @param formulaPartsObj $formulaPartsObj Class instance of SQ_formula_parts
 *
 * @return Components Array
 */
function get_components_options_parts_costs_price($systemId, $componentId, $optionId, $partsId, $partsCostsObj, $formulaPartsObj)
{

    
    $returnData = array();

    $params = array("system_id" => $systemId, "component_id" => $componentId, "option_id" => $optionId , "part_id" => $partsId);
    $allPartCosts = $partsCostsObj->getWhere($params);

    foreach ($allPartCosts as $costs) {
        $data["id"] = $costs["id"];
        $data["system_id"] = $costs["system_id"];
        $data["component_id"] = $costs["component_id"];
        $data["option_id"] = $costs["option_id"];
        $data["part_id"] = $costs["part_id"];
        $data["name"] = $costs["name"];
        $data['formula'] = $costs['formula'];
        $data['formula_parts'] = get_components_options_parts_costs_formulas_price($systemId, $componentId, $optionId, $partsId, $costs["id"], $formulaPartsObj);
        $returnData[] = $data;
    }

  
    return $returnData ;
}

/**
 * Returns Costs Formulas
 *
 * @param systemId        $systemId        id of SQ_components where type is system
 * @param componentId     $componentId     SQ_components id
 * @param optionId        $optionId        SQ_new_options table id
 * @param partsId         $partsId         SQ_parts table id
 * @param costId          $costId          Cost Table Id
 * @param formulaPartsObj $formulaPartsObj Cost Table Id
 *
 * @return Components Array
 */
function get_components_options_parts_costs_formulas_price($systemId, $componentId, $optionId, $partsId, $costId, $formulaPartsObj)
{

    
    $returnData = array();

    $params = array("system_id" => $systemId, "component_id" => $componentId, "option_id" => $optionId, "cost_id" => $costId,  "part_id" => $partsId);
    $allFormulaParts = $formulaPartsObj->getWhere($params);

    foreach ($allFormulaParts as $formula) {
        $data["id"] = $formula["id"];
        $data["system_id"] = $formula["system_id"];
        $data["component_id"] = $formula["component_id"];
        $data["option_id"] = $formula["option_id"];
        $data["part_id"] = $formula["part_id"];
        $data["cost_id"] = $formula["cost_id"];
        $data["name"] = $formula["name"];
        $data['formula'] = $formula['formula'];
        $returnData[] = $data;
    }

  
    return $returnData ;
}
