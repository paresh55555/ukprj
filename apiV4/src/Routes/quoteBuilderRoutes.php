<?php

use SalesQuoter\Cart\Cart;

$app->get(
    '/quoteBuilder',
    function ($request, $response, $args) {

        return renderGetBuilder ( $this->token, $response, 'quote');
    }
);

$app->get(
    '/orderBuilder',
    function ($request, $response, $args) {

       return renderGetBuilder ( $this->token, $response, 'order');
    }
);



$app->get(
    '/quoteBuilder/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cart = new Cart();
        $intVal = intval($args['id']);

        $singleCart = $cart->get($args['id']);

        if (sizeof($singleCart) > 0) {
            $singleCart[0]['status'] = 'success';

            return responseWithStatusCode($response, $singleCart[0], 200);
        } else {

            $responseData = array('status' => 'error', 'message' => 'data not found');
                
            return responseWithStatusCode($response, $responseData, 404);
        }

    }
);


$app->delete(
    '/quoteBuilder/{id}',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $cart = new Cart();
        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $data = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
            
            return responseWithStatusCode($response, $data, 400);

        } else {
            $deleteCart = $cart->delete($args['id']);
            
            return responseWithStatusCode($response, $deleteCart, 200);
        }
    }
);




$app->post(
    '/quoteBuilder',
    function ($request, $response, $args) {

       return renderPostBuilder( $this->token, $request, $response, "quote" );
    }
);




$app->post(
    '/orderBuilder',
    function ($request, $response, $args) {

       return renderPostBuilder( $this->token, $request, $response, "order" );
    }
);



$app->put(
    '/quoteBuilder',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $cart = new Cart();
        $singleCart = $cart->get($config['id']);

        if (sizeof($singleCart) > 0) {
            if (strlen($config['name']) == '0') {
                $responseCode = 400;
                $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
            } else {
                $responseCode = 200;
                $result = $cart->update($config);
                $responseData = array('status' => 'success', 'response_data' =>  $result );
            }
        } else {
            $responseCode = 404;
            $responseData = array('status' => 'error', 'message' =>  'ID Not Exists' );
        }


        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);


/**
 * function renderPostBuilder
 *
 * @param token $token user token
 * @param request $request slim request obj
 * @param response $response slim response obj
 * @param kind $kind quote or order
 *
 * @return json array 
 */

function renderPostBuilder( $token, $request, $response, $kind ) {

    if (false === $token->hasScope(["admin"])) {
        return invalidPermissionsResponse($response);
    }

    $data = $request->getBody();
    $config = jsonDecodeWithErrorChecking($data);
    $config['kind'] = $kind;

    $cart = new Cart();

    if (strlen($config['name']) == '0') {
        $responseCode = 400;
        $responseData = array('status' => 'error', 'message' =>  'name can not be Empty' );
    } else {
        $responseCode = 200;
        $result =  $cart->create($config);
        $responseData = array('status' => 'success', 'response_data' =>  $result );
    }
    

    return responseWithStatusCode($response, $responseData, $responseCode);

}


/**
 * function renderGetBuilder
 *
 * @param token $token user token
 * @param response $response slim response obj
 * @param kind $kind quote or order
 *
 * @return json array 
 */

function renderGetBuilder( $token, $response, $kind ) {

    if (false === $token->hasScope(["admin"])) {
        return invalidPermissionsResponse($response);
    }

    $data = array();
    $data['kind'] = $kind;

    $cart = new Cart();
    $allCart = $cart->getWhere( $data );

    return responseWithStatusCode($response, $allCart, 200);

}