<?php

use SalesQuoter\Doors\Size;
use SalesQuoter\Doors\PanelRange;
use SalesQuoter\Doors\Module;
use SalesQuoter\Doors\Frame;
use SalesQuoter\Doors\Fixed;
use SalesQuoter\Doors\Percent;
use SalesQuoter\Doors\Glass;
use SalesQuoter\Doors\ModuleGroup;

$app->get(
    '/modules',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $module = new ModuleGroup();

        $moduleObjects = $module->getAll(array("active" => "1"));

        return responseWithStatusCode($response, $moduleObjects, 200);
    }
);



$app->get(
    '/modules/{module}/percent',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $percent = new Percent();

        $config = array ("module_id" => $args['module'] );
        $percentObjects = $percent->getAll($config);

        return responseWithStatusCode($response, $percentObjects, 200);
    }
);



$app->put(
    '/modules/{module}/percent/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $percent = new Percent();

        $percentObject = $percent->update($config);

        if (count($percentObject) > 1) {
             $percentObject['status'] = "success";
             $result = $percentObject;
        } else {
             $result = array("status" => "error" , "message" => "data not found");
        }

        return responseWithStatusCode($response, $result, 200);
    }
);



$app->post(
    '/modules/{module}/percent',
    function ($request, $response, $args) {
   
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $percent = new Percent();
        $percentObject = $percent->create($config);
        $percentObject["status"] = "success";

        return responseWithStatusCode($response, $percentObject, 201);
    }
);





$app->delete(
    '/modules/{module}/percent/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $percent = new Percent();
        $percentObject = $percent->delete($args['id']);

        return responseWithStatusCode($response, $percentObject, 200);
    }
);




$app->get(
    '/modules/{module}/fixed',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $fixed = new Fixed();

        $config = array ("module_id" => $args['module'] );
        $fixedObjects = $fixed->getAll($config);

        return responseWithStatusCode($response, $fixedObjects, 200);
    }
);


$app->put(
    '/modules/{module}/fixed/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $fixed = new Fixed();

        $fixedObject = $fixed->update($config);

        if (count($fixedObject) > 1) {
             $fixedObject['status'] = "success";          
        } else {
             $fixedObject = array("status" => "error" , "message" => "data not found");
        }

        return responseWithStatusCode($response, $fixedObject, 200);
    }
);



$app->post(
    '/modules/{module}/fixed',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $fixed = new Fixed();
        $fixedObject = $fixed->create($config);
        $fixedObject['status'] = 'success';

        return responseWithStatusCode($response, $fixedObject, 201);
    }
);



$app->delete(
    '/modules/{module}/fixed/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $fixed = new Fixed();
        $fixedObject = $fixed->delete($args['id']);

        return responseWithStatusCode($response, $fixedObject, 200);
    }
);




$app->get(
    '/modules/{module}/frame',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $frame = new Frame();

        $config = array ("module_id" => $args['module'] );
        $frameObjects = $frame->getAll($config);

        return responseWithStatusCode($response, $frameObjects, 200);
    }
);
 

$app->put(
    '/modules/{module}/frame/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $frame = new Frame();

        $frameObject = $frame->update($config);

        if (count($frameObject) > 1) {
             $frameObject['status'] = "success";
        } else {
             $frameObject = array("status" => "error" , "message" => "data not found");
        }

        return responseWithStatusCode($response, $frameObject, 200);
    }
);


$app->post(
    '/modules/{module}/frame',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $frame = new Frame();
        $frameObject = $frame->create($config);
        $frameObject["status"] = "success";

        return responseWithStatusCode($response, $frameObject, 201);
    }
);




$app->delete(
    '/modules/{module}/frame/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $frame = new Frame();
        $frameObject = $frame->delete($args['id']);

        return responseWithStatusCode($response, $frameObject, 200);
    }
);




$app->get(
    '/modules/{module}/panelRanges',
    function ($request, $response, $args) {
    
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }


        $panels = new PanelRange();
        $module = new Module();

        $moduleObject = $module->get($args['module']);


        $config = array ("panelGroup" => $moduleObject['panelGroup'] );
        $panelObject = $panels->getAll($config);

        return responseWithStatusCode($response, $panelObject, 200);
    }
);



$app->put(
    '/modules/{module}/panelRanges/{id}',
    function ($request, $response, $args) {
  
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $panel = new PanelRange();
        $panelObject = $panel->update($config);

        if (count($panelObject) > 1) {
             $panelObject['status'] = "success";
        } else {
             $panelObject = array("status" => "error" , "message" => "data not found");
        }

        return responseWithStatusCode($response, $panelObject, 200);
    }
);



$app->post(
    '/modules/{module}/panelRanges',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $module = new Module();
        $moduleObject = $module->get($args['module']);

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['panelGroup'] = $moduleObject['panelGroup'];

        $panel = new PanelRange();

        $checkValidation = $panel->checkValidation($config, $config['panelGroup']);
        if (sizeof($checkValidation) > 0) {
            $checkValidation['status']  = 'error';
            return responseWithStatusCode($response, $checkValidation, 400);
        } else {
            $panelObject = $panel->create($config);
            $panelObject['status']  = 'success';
            return responseWithStatusCode($response, $panelObject, 201);
        }

    }
);



$app->delete(
    '/modules/{module}/panelRanges/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $panel = new PanelRange();
        $panelObject = $panel->delete($args['id']);

        return responseWithStatusCode($response, $panelObject, 200);
    }
);





$app->get(
    '/modules/{module}/size',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $size = new Size();
        $sizeObject = $size->get($args['module']);

        return responseWithStatusCode($response, $sizeObject, 200);
    }
);





$app->put(
    '/modules/{module}/size',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['module'];

        $size = new Size();
        $sizeObject = $size->update($config);

        if (count($sizeObject) > 1) {
             $sizeObject['status'] = "success";
        } else {
             $sizeObject = array("status" => "error" , "message" => "data not found");
        }

        return responseWithStatusCode($response, $sizeObject, 200);
    }
);




$app->get(
    '/modules/{module}/glass',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $glass = new Glass();

        $config = array ("module_id" => $args['module'] );
        $glassObject = $glass->getAll($config);

        return responseWithStatusCode($response, $glassObject, 200);
    }
);



$app->post(
    '/modules/{module}/glass',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];

        $glass = new Glass();
        $glassObject = $glass->create($config);
        $glassObject['status'] = 'success';

        return responseWithStatusCode($response, $glassObject, 201);
    }
);




$app->put(
    '/modules/{module}/glass/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['module_id'] = $args['module'];
        $config['id'] = $args['id'];

        $glass = new Glass();
        $glassObject = $glass->update($config);

        if (count($glassObject) > 1) {
             $glassObject['status'] = "success";
        } else {
             $glassObject = array("status" => "error" , "message" => "data not found");
        }

        return responseWithStatusCode($response, $glassObject, 200);
    }
);





$app->delete(
    '/modules/{module}/glass/{id}',
    function ($request, $response, $args) {
 
        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }
    
        $glass = new Glass();
        $glassObject = $glass->delete($args['id']);

        return responseWithStatusCode($response, $glassObject, 200);
    }
);
