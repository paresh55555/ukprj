<?php

use SalesQuoter\Company\Company;
use SalesQuoter\Users\User;

$app->get(
    '/companies',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $company = new Company();
        $companyObjects = $company->getAll();

        return responseWithStatusCode($response, $companyObjects, 200);
    }
);



$app->get(
    '/companies/order',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["all"])) {
            return invalidPermissionsResponse($response);
        }

        $company = new Company();
        $companyObjects = $company->getAllOrderBy();

        return responseWithStatusCode($response, $companyObjects, 200);
    }
);




$app->get(
    '/companies/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $company = new Company();
        $companyObjects = $company->get($args['id']);

        if (count($companyObjects) > 1) {
             return responseWithStatusCode($response, $companyObjects, 200);
        } else {
             $responseMsg = array('status' => 'error' , 'message' => 'data not found');
             return responseWithStatusCode($response, $responseMsg, 404);
        }
    }
);





$app->get(
    '/companies/{id}/users',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $user = new User();
        $allUsers = $user->getUsersFromDealersId($args['id']);

        return responseWithStatusCode($response, $allUsers, 200);
    }
);



$app->put(
    '/companies/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);
        $config['id'] = $args['id'];

        $company = new Company();
        $singleCompany = $company->checkExists($config['id']);
    
        if (sizeof($singleCompany) > 0) {
            $responseCode = 200;
            $responses = $company->update($config);
            $responses['status'] = "success";
        } else {
            $responseCode = 404;
            $responses = array("status" => "error", "message" => "Parameter ID not Exists");
        }

        return responseWithStatusCode($response, $responses, $responseCode);
    }
);



$app->post(
    '/companies',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $company = new Company();
        $companyObjects = $company->create($config);
        $companyObjects['status'] = "success";

        return responseWithStatusCode($response, $companyObjects, 201);
    }
);




$app->delete(
    '/companies/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $company = new Company();
        $deleteResponse = $company->delete($args['id']);

        return responseWithStatusCode($response, $deleteResponse, 200);
    }
);



$app->post(
    '/companies/companyAvailable',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["admin"])) {
            return invalidPermissionsResponse($response);
        }

        $data = $request->getBody();
        $config = jsonDecodeWithErrorChecking($data);

        $company = new Company();

        if (strlen($config['companyName']) == '0') {
             $responseCode = 400;
             $responses = array("status" => "error", "message" => "company name can not be empty");
        } else {
            $responseCode = 201;
            $companyObjects = $company->isAvailable($config['companyName']);
            $responses = array( 'status' => 'success' , 'is_available' => $companyObjects);
        }
   
        return responseWithStatusCode($response, $responses, $responseCode); 
    }
);
