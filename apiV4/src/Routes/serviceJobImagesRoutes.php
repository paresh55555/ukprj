<?php
use SalesQuoter\Service\JobAttachments;

$app->get(
    '/service/jobs/{job_id}/images',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new JobAttachments();

        $allItems = $attachments->getAll ( ["job_id" => $args["job_id"] ] );

        $responseData = array ("status" => "success", "data" => $allItems);

        return responseWithStatusCode($response, $responseData, 200);

    }
);


$app->post(
    '/service/jobs/{job_id}/images',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new JobAttachments();

        $data = $request->getParsedBody();

        $newInsertData = [ 
            "job_id" => $args["job_id"], 
            "url" => "", 
            "file_name" => $data["fileName"]
        ];

        $newOrder = $attachments->create( $newInsertData );

        $responseData = $attachments->doFileUpload ( $data, $args["job_id"], $newOrder["id"] );

        if ( $responseData["status"] == 'error' ) {
            $statusCode = 400;
            $attachments->delete ( $newOrder["id"] );
        }else {
            $statusCode = 201;
            $responseData = $attachments->update ([ "id" => $newOrder["id"], "url" => $responseData["url"] ]);
        }

        return responseWithStatusCode($response, $responseData, $statusCode);

    }
);


$app->put(
    '/service/jobs/{job_id}/images/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new JobAttachments();
        $orders = new Orders();

        $data = $request->getParsedBody();

        $responseData = $attachments->doFileUpload ( $data, $args["job_id"], $args["id"] );

        if ( $responseData["status"] == "success" ) {
            $statusCode = 200;
            $responseData = $attachments->update ([ "id" => $args["id"], "file_name" => $data["fileName"], "url" => $responseData["url"] ]);
        }else {
            $statusCode = 400;
        }

        return responseWithStatusCode($response, $responseData, $statusCode);

    }
);


$app->delete(
    '/service/jobs/{job_id}/images/{id}',
    function ($request, $response, $args) {

        if (false === $this->token->hasScope(["service"])) {
            return invalidPermissionsResponse($response);
        }

        $attachments = new JobAttachments();

        $intVal = intval($args['id']);

        if ($intVal == '0') {
            $responseCode = 400;
            $responseData = array('status' => 'error', 'message' => 'Parameter ID must be an integer' );
        } else {
            $responseCode = 200;
            $responseData = $attachments->delete($args['id']);
        }

        return responseWithStatusCode($response, $responseData, $responseCode);
    }
);