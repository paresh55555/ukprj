<?php

/*
 * This file is part of the Slim API skeleton package
 *
 * Copyright (c) 2016 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   https://github.com/tuupola/slim-api-skeleton
 *
 */
use SalesQuoter\Token;

use Slim\Middleware\JwtAuthentication;
use Slim\Middleware\HttpBasicAuthentication;
use Tuupola\Middleware\Cors;
use Gofabian\Negotiation\NegotiationMiddleware;
use Micheh\Cache\CacheUtil;

$container = $app->getContainer();

$container["HttpBasicAuthentication"] = function ($container) {

    if (isset($_SERVER['PHP_AUTH_USER'])) {
        $user = $_SERVER['PHP_AUTH_USER'];
    } else {
        $user = '';
    }

    if (isset($_SERVER['PHP_AUTH_PW'])) {
        $password = $_SERVER['PHP_AUTH_PW'];
    } else {
        $password = '';
    }

    return new HttpBasicAuthentication([
        "path" => ["/token/login", "/token"],
        "passthrough" => ["/token/reset", "/token/pws"],
        "relaxed" => ["localhost/apislimtest"],
        "users" => [
            $user => $password,
        ],
    ]);
};

$container["token"] = function ($container) {

    return new Token();
};

$container["JwtAuthentication"] = function ($container) {
    return new JwtAuthentication([
        "path" => "/",
        "passthrough" => ["/token", "/siteBranding", "/sendEmail"],
        "secret" => JWT_SECRET,
        "logger" => $container["logger"],
        "relaxed" => ["localhost/apislimtest"],
        "error" => function ($request, $response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments["message"];

            return $response
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        },
        "callback" => function ($request, $response, $arguments) use ($container) {
            $container["token"]->hydrate($arguments["decoded"]);
        },
    ]);
};

$container["Cors"] = function ($container) {
    return new Cors([
        "logger" => $container["logger"],
        "origin" => ["*"],
        "methods" => ["GET", "POST", "PUT", "PATCH", "DELETE"],
        "headers.allow" => ["Authorization", "If-Match", "If-Unmodified-Since"],
        "headers.expose" => ["Authorization", "Etag"],
        "credentials" => true,
        "cache" => 60,
        "error" => function ($request, $response, $arguments) {
            $data["status"] = "error";
            $data["message"] = $arguments["message"];

            return $response
                ->withHeader("Content-Type", "application/json")
                ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
        },
    ]);
};

$container["Negotiation"] = function ($container) {
    return new NegotiationMiddleware([
        "accept" => ["application/json"],
    ]);
};

$loadPDF = preg_match("~/api/V4\/quotes\/[0-9]+\/contracts/download~", $_SERVER["REQUEST_URI"]);

if (!$loadPDF) {
    $app->add("HttpBasicAuthentication");
    $app->add("JwtAuthentication");
    $app->add("Cors");
    $app->add("Negotiation");
}

$container["cache"] = function ($container) {
    
    return new CacheUtil();
};
