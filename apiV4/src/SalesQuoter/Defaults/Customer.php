<?php
/* SalesQuoter added */
namespace SalesQuoter\Defaults;

use SalesQuoter\AbstractCrud;

class Customer extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "title", "type", "placeholder", "display", "required", "error", "`order`");
        $config = array ('table' => 'SQ_defaults', 'fields' => $fields);

        parent::__construct($config);
    }
}
