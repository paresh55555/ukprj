<?php
/* SalesQuoter added */
namespace SalesQuoter\Defaults;

use SalesQuoter\AbstractCrud;

class Site extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "name", "value");
        $config = array ('table' => 'SQ_defaults_localize', 'fields' => $fields);

        parent::__construct($config);
    }
}
