<?php

namespace SalesQuoter\Cache;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Cache Class
 *
 */
class Cache extends CustomAbstractCrud
{
    /**
     * Returns Construct function
     *
     */
    public function __construct()
    {

        $fields = array("id", "type", "cachedArray", "current", "name", "system_id");
        $config = array("table" => 'SQ_cache', 'fields' => $fields);
        parent::__construct($config);
    }

   /**
     * Updates all caches to False
     *
     * @param type     $type     systemUX or systemPrice
     * @param systemId $systemId type systems id
     *
     * @return returns bolean
     */
    public function updateAllFalse($type, $systemId)
    {

        $config['current'] =  false;
        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('type', '=', "$type")
            ->where('system_id', '=', $systemId);

        return $updateStatement->execute();
    }

    /**
     * revertCache or Updating Each Items
     *
     * @param type        $type        cachedArray or cachedArray
     * @param cachedArray $cachedArray cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function revertCache($type, $cachedArray)
    {

        if ($type == 'systemPrice') {
             $this->updateSystemPrice($cachedArray);
        }

        if ($type == 'systemUX') {
             $this->updateSystemUx($cachedArray);
        }
    }

    /**
     * Reverting System UX
     *
     * @param cachedArray $cachedArray cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function updateSystemUx($cachedArray)
    {

        $arrayData =  unserialize($cachedArray);

        foreach ($arrayData as $systemUx) {
            $newComponents['id'] = $systemUx['id'];
            $newComponents['menu_id'] = $systemUx['menu_id'];
            $newComponents['type'] = $systemUx['type'];
            $newComponents['system_id'] = $systemUx['system_id'];
            $newComponents['grouped_under'] = $systemUx['grouped_under'];
            $newComponents['name'] = $systemUx['name'];

            $this->updateItems("SQ_components", $newComponents, $newComponents['id']);

            $onlyTraitsArray = $systemUx['traits'];
            $optionsArray = $systemUx['options'];

            $this->processTraits($onlyTraitsArray, $newComponents['id']);
            $this->processOption($optionsArray, $newComponents['id']);
        }
    }

    
    /**
     * Reverting System UX
     *
     * @param onlyTraitsArray $onlyTraitsArray cachedArray or cachedArray
     * @param componentId     $componentId     cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function processTraits($onlyTraitsArray)
    {

        foreach ($onlyTraitsArray as $traits) {
            $data["id"] = $traits["id"];
            $data["component_id"] = $traits["component_id"];
            $data["name"] = $traits["name"];
            $data["type"] = $traits["type"];
            $data["value"] = $traits["value"];
            $data["visible"] = $traits["visible"];

            $this->updateItems("SQ_traits", $data, $traits["id"]);
        }
    }


   /**
     * Reverting System UX
     *
     * @param optionsArray $optionsArray cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function processOption($optionsArray)
    {

        foreach ($optionsArray as $options) {
            $data["id"] = $options["id"];
            $data["system_id"] = $options["system_id"];
            $data["component_id"] = $options["component_id"];
            $data["name"] = $options["name"];
            $data["allowDeleted"] = $options["allowDeleted"];
            
            $this->updateItems("SQ_options", $data, $options["id"]);

            $this->processOptionTraits($options["traits"]);
        }
    }


   /**
     * Processing Option Traits
     *
     * @param allTraits $allTraits cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function processOptionTraits($allTraits)
    {

        
        foreach ($allTraits as $optionTraits) {
            $data["id"] = $optionTraits["id"];
            $data["system_id"] = $optionTraits["system_id"];
            $data["component_id"] = $optionTraits["component_id"];
            $data["option_id"] = $optionTraits["option_id"];
            $data["name"] = $optionTraits["name"];
            $data["visible"] = $optionTraits["visible"];
            $data["value"] = $optionTraits["value"];

            $this->updateItems("SQ_options_traits", $data, $optionTraits["id"]);
        }
    }


    /**
     * Reverting System Price
     *
     * @param arrayData $arrayData cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function updateSystemPrice($arrayData)
    {
        
        $cachedArray =  unserialize($arrayData);

        foreach ($cachedArray as $components) {
            $componentsData["id"] = $components["id"];
            $componentsData["menu_id"] = $components["menu_id"];
            $componentsData["system_id"] = $components["system_id"];
            $componentsData["name"] = $components["component_name"];
            $componentsData["type"] = $components["type"];
            $componentsData["grouped_under"] = $components["grouped_under"];
            $componentsData["required"] = $components["required"];
            $componentsData["multiple"] = $components["multiple"];

            $this->updateItems("SQ_components", $componentsData, $componentsData['id']);

            foreach ($components['options'] as $options) {
                $optionData["id"] = $options["id"];
                $optionData["system_id"] = $options["system_id"];
                $optionData["component_id"] = $options["component_id"];
                $optionData["name"] = $options["name"];
                $optionData['allowDeleted'] = $options['allowDeleted'];

                $this->updateItems("SQ_new_options", $optionData, $optionData['id']);

                foreach ($options['parts'] as $parts) {
                    $partsData["id"] = $parts["id"];
                    $partsData["system_id"] = $parts["system_id"];
                    $partsData["component_id"] = $parts["component_id"];
                    $partsData["option_id"] = $parts["option_id"];
                    $partsData["name"] = $parts["name"];
                    $partsData['description'] = $parts['description'];

                    $this->updateItems("SQ_parts", $partsData, $parts['id']);

                    foreach ($parts['costs'] as $costs) {
                        $costsData["id"] = $costs["id"];
                        $costsData["system_id"] = $costs["system_id"];
                        $costsData["component_id"] = $costs["component_id"];
                        $costsData["option_id"] = $costs["option_id"];
                        $costsData["part_id"] = $costs["part_id"];
                        $costsData["name"] = $costs["name"];
                        $costsData["formula"] = $costs["formula"];

                        $this->updateItems("SQ_costs", $costsData, $costsData['id']);
                    }
                }
            }
        }
    }

    /**
     * Insert or Updating Each Items
     *
     * @param tableName $tableName cachedArray or cachedArray
     * @param items     $items     cachedArray or cachedArray
     * @param pId        $pId        cachedArray or cachedArray
     *
     * @return returns bolean
     */
    public function updateItems($tableName, $items, $pId)
    {

        $sql = $this->pdo->select(["id"])->from($tableName);
        $sql->where($tableName.'.id', '=', $pId);

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        $items['active'] = 1;

        if (sizeof($data) > '0') {
            unset($items['id']);

            $updateStatement = $this->pdo->update($items)
                                    ->table($tableName)
                                    ->where('id', '=', $pId);

            $updateStatement->execute();
        } else {
            $insertStatement = $this->pdo->insert(array_keys($items))
                                    ->into($tableName)
                                    ->values(array_values($items));

            $insertStatement->execute(true);
        }
    }
}
