<?php

namespace SalesQuoter\Customers;

use SalesQuoter\AbstractCrud;

/**
 *  Class Customer
 *
 */
class Customer extends AbstractCrud
{
     /**
     * Class constructor
     *
     */
    public function __construct()
    {
        $fields = array('id', 'firstName', 'lastName', 'phone', 'companyName', 'email', 'sales_person_id',
             'billingAddress1', 'billingAddress2', 'billingCity',
            'billingState', 'billingZip', 'billingPhone', 'billingName', 'billingEmail', );

        $config = array("table" => 'customer', 'fields' => $fields, );
        parent::__construct($config);
    }
}
