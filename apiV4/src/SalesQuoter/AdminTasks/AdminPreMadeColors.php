<?php

namespace SalesQuoter\AdminTasks;

use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\Components\Components;
use SalesQuoter\Traits\OptionTraits;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Options\Options;
use SalesQuoter\Systems\Systems;

/**
 *  AdminPreMadeColors Class
 *
 */

class AdminPreMadeColors
{

    protected $traits;
    protected $optionsTraits;
    protected $options;
    protected $components;

    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        $this->components = new Components();
        $this->traits = new Traits();
        $this->optionsTraits = new OptionTraits();
        $this->options = new Options();
    }

    /**
     * Getting copy
     *
     * @return array
     */
    public function getCopy()
    {

        $data['type'] = 'preMadeColorSubGroup';
        $subGroups = $this->components->getWhere($data);

        $data['type'] = 'preMadeColorGroup';
        $groups = $this->components->getWhere($data);


        $components = array_merge($subGroups, $groups);

        $allComponents = array();

        foreach ($components as $component) {
            $allComponents[] =  $this->getFullComponent($component);
        }

        return $allComponents;
    }

    /**
     * Creating a copy
     *
     * @param data $data
     *
     * @return array
     */
    public function createCopy($data)
    {
        $newComponents = array();
        foreach ($data as $component) {
            $newComponents[] = $this->duplicateComponent($component);
        }

        return $newComponents;
    }

    /**
     * Getting a full component
     *
     * @param component $component
     *
     * @return array
     */
    protected function getFullComponent($component)
    {

        $where = array("component_id" => $component['id']);
        $componentTraits = $this->traits->getWhere($where);
        $component['traits'] = $componentTraits;


        $optionResults = $this->options->getWhere($where);

        foreach ($optionResults as $option) {
            $optionsTraitsWhere = array("option_id" => $option['id']);
            $option['traits'] = $this->optionsTraits->getWhere($optionsTraitsWhere);
            $component['options'][] = $option;
        }

        return $component;
    }

    /**
     * Duplicating component
     *
     * @param conponents $component
     *
     * @return array
     */

    protected function duplicateComponent($component)
    {
        $config = array('system_id' => '-1');
        $config['name'] = $component['name'];
        $config['menu_id'] = $component['menu_id'];
        $config['type'] = $component['type'];
        $config['grouped_under'] = 0;
        $newComponent = $this->components->create($config);

        $this->duplicateComponentTraits($component, $newComponent[0]);
        $this->duplicateOptions($component, $newComponent[0]);

        return $newComponent[0];
    }

     /**
     * Duplicating duplicateComponentTraits
     *
     * @param conponents $component
     * @param newComponent $newComponent
     *
     * @return array
     */
    protected function duplicateComponentTraits($component, $newComponent)
    {
        foreach ($component['traits'] as $trait) {
            unset($trait['id']);
            $trait['component_id'] = $newComponent['id'];
            $this->traits->create($trait);
        }
    }

    /**
     * Duplicating options
     *
     * @param component $component
     * @param newComponent $newComponent
     *
     * @return array
     */
    protected function duplicateOptions($component, $newComponent)
    {
        foreach ($component['options'] as $option) {
            $traits = $option['traits'];
            unset($option['traits']);
            unset($option['id']);
            $option['component_id'] = $newComponent['id'];
            $option['system_id'] = -1;
            $newOption = $this->options->create($option);

            foreach ($traits as $trait) {
                $trait['component_id'] = $newComponent['id'];
                $trait['system_id'] = -1;
                $trait['option_id'] = $newOption[0]['id'];
                unset($trait['id']);

                $this->optionsTraits->create($trait);
            }
        }
    }
}
