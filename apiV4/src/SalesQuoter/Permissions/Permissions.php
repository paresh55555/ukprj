<?php

namespace SalesQuoter\Permissions;

use SalesQuoter\CustomAbstractCrud;

class Permissions extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "permissions");
        $config = array("table" => 'SQ_permissions', 'fields' =>$fields);
        parent::__construct($config);
    }
}
