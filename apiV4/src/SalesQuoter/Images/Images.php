<?php

namespace SalesQuoter\Images;

use SalesQuoter\CustomAbstractCrud;

/**
 * Images class for SQ_images table
 *
 * @author AlexR
 */
class Images extends CustomAbstractCrud {
    
    public function __construct()
    {
        $fields = array("id", "url", "ext", "version");
        $config = array("table" => 'SQ_images', 'fields' => $fields);
        parent::__construct($config);
    }
    
    public function getFileVersionUrl($uploadPath = '', $ext = ''){
        $uploadFile = '';
        
        $sql = $this->pdo->select(['id', 'url', 'ext', 'version'])->from($this->table);
        $sql->where($this->table.'.url', '=', $uploadPath);
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (count($data) > 0) {
            $updateData['version'] = $data[0]['version'] + 1;
            $updateData['ext'] = $ext;
            $pId = $data[0]['id'];

            $updateStatement = $this->pdo->update($updateData)->table($this->table)
                    ->where('id', '=', $pId);

            $updateStatement->execute();
            $insertId = $pId;
        } else {
            $insertData['url'] = $uploadPath;
            $insertData['ext'] = $ext;
            $insertData['version'] = 1;

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                            ->into($this->table)->values(array_values($insertData));


            $insertId = $insertStatement->execute(true);
        }
        
        $newData = $this->imageGet($insertId);
        
        if(count($newData) == 0){
            echo json_encode(
                    array(
                    'status' => 'error',
                    'msg' => 'Error while getting new file version',
                    )
                 );
        }else{
            $uploadFile = $newData[0]['url'].'_'.$newData[0]['version'].'.'.$newData[0]['ext'];

            return $uploadFile;
        }
    }
    
    private function imageGet($pId){
        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->where($this->table.'.id', '=', $pId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();
        
        return $data;
    }
}
