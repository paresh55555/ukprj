<?php

namespace SalesQuoter\FormulaParts;

use SalesQuoter\CustomAbstractCrud;

class FormulaParts extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "system_id", "component_id", "option_id", "part_id", "cost_id", "name", "formula");
        $config = array("table" => 'SQ_formula_parts', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function checkExits($systemId, $componentId, $optionId, $partId, $costId)
    {

        $sql = $this->pdo->select(["SQ_costs.id"])->from("SQ_costs");

        $sql->where('SQ_costs.component_id', '=', $componentId);
        $sql->where('SQ_costs.system_id', '=', $systemId);
        $sql->where('SQ_costs.option_id', '=', $optionId);
        $sql->where('SQ_costs.part_id', '=', $partId);
        $sql->where('SQ_costs.id', '=', $costId);
        
        $sql->where('SQ_costs.active', '=', 1);



        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return $data[0];
        }

        return array();
    }
}
