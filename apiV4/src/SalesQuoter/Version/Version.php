<?php

namespace SalesQuoter\Version;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class Version
 *
 */

class Version extends CustomAbstractCrud
{
    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "type", "_id", "version", "modified");
        $config = array("table" => 'SQ_version', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * function updateVersion
     *
     * @param type $type type can be adminSystemGroup, adminSystem, frontSystem, frontSystemGroup
     * @param pId  $pId  Components Id
     *
     * @return nothing
     */
    public function updateVersion($type, $pId)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_version');
        $sql->where('SQ_version._id', '=', $pId);
        $sql->where('SQ_version.type', '=', $type);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > '0') {
            $updateData['version'] = $data[0]['version'] + 1;
            $tableId = $data[0]['id'];

            $updateStatement = $this->pdo->update($updateData)->table("SQ_version")
                                         ->where('id', '=', $tableId);

            $updateStatement->execute();
        } else {
            $insertData['_id'] = $pId;
            $insertData['version'] = 1;
            $insertData['type'] = $type;
         
            $insertStatement = $this->pdo->insert(array_keys($insertData))
                                    ->into($this->table)->values(array_values($insertData));


            $insertStatement->execute(true);
        }
    }

    /**
     * function getWhere
     *
     * @param type $type type can be adminSystemGroup, adminSystem, frontSystem, frontSystemGroup
     * @param pId  $pId  Components Id
     *
     * @return nothing
     */
    public function getWhere($type, $pId)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_version');
        $sql->where('SQ_version._id', '=', $pId);
        $sql->where('SQ_version.type', '=', $type);

        $stmt = $sql->execute();

        return $stmt->fetchAll();

    }
}
