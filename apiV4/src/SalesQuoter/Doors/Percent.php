<?php

namespace SalesQuoter\Doors;

use SalesQuoter\SoftDeleteAbstractCrud;

class Percent extends SoftDeleteAbstractCrud
{

    public function __construct()
    {

        $fields = array('id','module_id', 'nameOfOption','percentage');
        $config = array("table" => 'optionsPercentage', 'fields' =>$fields);
        parent::__construct($config);
    }
}
