<?php



namespace SalesQuoter\Doors;

use SalesQuoter\SoftDeleteAbstractCrud;

error_reporting(0);

class PanelRange extends SoftDeleteAbstractCrud
{

    public function __construct()
    {
        $fields = array('id', 'panels','low', 'high');
        $config = array("table" => 'panelRanges', 'fields' =>$fields);
        parent::__construct($config);
    }


	/**
	 *
	 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
	 *
	 */
    public function checkValidation($data, $panelGroup)
    {

        $response = array();

        $checkRecordsExists = $this->checkMultipleRecordExists($data['panels'], $panelGroup);
        if ($checkRecordsExists) {
            $response['message'] = 'Panels Already Exists';
        }



        $isPanelsInt = is_int((int) $data['panels']);
        $isHighNumeric = is_numeric($data['high']);
        $isLowNumeric = is_numeric($data['low']);

        if (!$isPanelsInt) {
             $response['message'] = 'Panels Field value Must be a Number';
        }

        if (!$isHighNumeric) {
             $response['message'] = 'High Field value Must be a valid Number';
        }

        if (!$isLowNumeric) {
             $response['message'] = 'Low Field value Must be a valid Number';
        }

        if (sizeof($response) == '0') {
            if ($data['high'] <= $data['low']) {
                $response['message'] = 'High field value must be larger than Low';
            }
        }

     
     
        if (strlen($data['panels']) == '0' || strlen($data['high']) == '0' || strlen($data['low']) == '0') {
            $response['message'] = 'Field Value Can Not Be Empty';
        }


        return $response;
    }


    public function checkMultipleRecordExists($panels, $panelGroup)
    {

        $sql = $this->pdo->select(["*"])->from($this->table)->where('panels', '=', $panels)->where('panelGroup', '=', $panelGroup)->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return true;
        }


        return false;
    }
}
