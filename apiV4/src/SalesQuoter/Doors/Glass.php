<?php

namespace SalesQuoter\Doors;

use SalesQuoter\SoftDeleteAbstractCrud;

class Glass extends SoftDeleteAbstractCrud
{

    public function __construct()
    {

        $fields = array('id','module_id', 'nameOfOption','costPerPanel','costSquareMeter', 'height_cut_size_formula', 'width_cut_size_formula',
        'description');
        $config = array("table" => 'optionsGlass', 'fields' =>$fields);
        parent::__construct($config);
    }
}
