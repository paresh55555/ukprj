<?php

namespace SalesQuoter\Doors;

use SalesQuoter\AbstractCrud;

class ModuleGroup extends AbstractCrud
{

    protected $joinTable = 'SQ_modules';

    public function __construct()
    {

        $fields = array('id','panelGroup', 'title', 'url');
        $config = array("table" => 'module', 'fields' =>$fields);

        parent::__construct($config);
    }


    public function getAll($where = null)
    {
        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->join($this->joinTable, $this->table.'.id', '=', $this->joinTable.'.moduleId');

        if (!empty($where)) {
            $column = key($where);
            $sql->where($column, '=', $where[$column]);
        }

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }
}
