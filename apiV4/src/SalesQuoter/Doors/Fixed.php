<?php

namespace SalesQuoter\Doors;

use SalesQuoter\SoftDeleteAbstractCrud;

class Fixed extends SoftDeleteAbstractCrud
{

    public function __construct()
    {

        $fields = array('id','module_id', 'nameOfOption','partNumber','costItem', 'quantityFormula', 'forCutSheet', 'myOrder');
        $config = array("table" => 'optionsFixed', 'fields' =>$fields);
        parent::__construct($config);
    }
}
