<?php

namespace SalesQuoter\Doors;

use SalesQuoter\AbstractCrud;

class Module extends AbstractCrud
{

    public function __construct()
    {
        $fields = array('id', 'panelGroup');
        $config = array("table" => 'module', 'fields' =>$fields);
        parent::__construct($config);
    }
}
