<?php

namespace SalesQuoter\Doors;

use SalesQuoter\AbstractCrud;

class Size extends AbstractCrud
{

    public function __construct()
    {
        $fields = array('id', 'minWidth','maxWidth', 'minHeight','maxHeight');
        $config = array("table" => 'module', 'fields' =>$fields);
        parent::__construct($config);
    }
}
