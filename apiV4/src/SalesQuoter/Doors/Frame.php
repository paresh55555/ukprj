<?php

namespace SalesQuoter\Doors;

use SalesQuoter\SoftDeleteAbstractCrud;

class Frame extends SoftDeleteAbstractCrud
{

    public function __construct()
    {

        $fields = array('id','module_id', 'nameOfOption','option_type','material', 'cost_meter', 'cost_paint_meter',
            'waste_percent', 'number_of_lengths_formula', 'cut_size_formula', 'burnOff', 'forCutSheet', 'myOrder');
        $config = array("table" => 'optionsSized', 'fields' =>$fields);
        parent::__construct($config);
    }
}
