<?php

namespace SalesQuoter\History;

use SalesQuoter\CustomAbstractCrud;
/**
 *  Class History
 *
 */
class History extends CustomAbstractCrud
{
    /**
     * Constructor
     */
    public function __construct()
    {

        $fields = array('id', 'kind', 'kind_id', 'event', 'raw_data', 'result', 'date');

        $config = array("table" => 'SQ_history', 'fields' =>$fields);
        parent::__construct($config);
    }


    /**
     * function create
     *
     * @param config $config json encoded string
     *
     * @return json decoded array
     */
    public function createLog($kind, $eventId, $event, $rawData, $result)
    {

        $insertData = array();

        $insertData['kind'] = $kind;
        $insertData['kind_id'] = $eventId;
        $insertData['event'] = $event;
        $insertData['raw_data'] = json_encode($rawData);
        $insertData['result'] = json_encode($result);
        $insertData['date'] = date("Y-m-d H:i:s");

        $response = parent::create($insertData);

        if ( sizeof($response) > '0') {
            
            $response[0]['raw_data'] = $rawData;
            $response[0]['result'] = $result;

        }else {
            $reponse = array();
        }


        return $reponse;
    }

    /**
     * function renderHistory
     *
     * @param getWhere $getWhere json encoded string
     * @param limit $limit       default 10     
     *
     * @return json decoded array
     */
    public function renderHistory( $getWhere, $limit ) {

        $sql = $this->pdo->select($this->fields)->from($this->table);

        foreach ($getWhere as $key => $value) {
                $sql->where($this->table.".".$key, '=', $value);
        }

        $sql->where($this->table.'.active', '=', 1);
        $sql->orderBy('id', 'DESC');
        $sql->limit($limit);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        for ( $count = 0; $count < sizeof($data); $count++ ) {

            $data[$count]['raw_data'] = json_decode($data[$count]['raw_data'] , TRUE);
            $data[$count]['result'] = json_decode($data[$count]['result'] , TRUE);
        }

        return $data;

    }


}
