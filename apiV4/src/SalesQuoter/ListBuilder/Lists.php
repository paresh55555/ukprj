<?php

namespace SalesQuoter\ListBuilder;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class Lists
 *
 */
class Lists extends CustomAbstractCrud
{
    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "type", "name", "data", "active");
        $config = array("table" => 'SQ_lists', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * getting all lists from SQ_lists table
     *
     * @return array
     */
    public function getLists()
    {

        $sql = $this->pdo->select(["id", "type", "name"])->from($this->table);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * getting all lists data from SQ_lists table
     *
     * @param id $id
     * @return array
     */
    public function getListsData($id)
    {

        $sql = $this->pdo->select(["data"])->from($this->table);
        $sql->where($this->table.'.active', '=', 1);
        $sql->where($this->table.'.id', '=', $id);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > '0') {
            if (strlen($data[0]['data']) > '0') {
                return jsonDecodeWithErrorChecking($data[0]['data']);
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    /**
     * checking validations in json data
     *
     * @param jsonData $jsonData json array data
     * @return array
     */
    public function checkValidJson($jsonData)
    {

        if (sizeof($jsonData[0]['columns']) > '0') {
            if (strlen($jsonData[0]['name']) == '0') {
                return false;
            } elseif (strlen($jsonData[0]['columns'][0]['name']) == '0') {
                return false;
            } elseif (strlen($jsonData[0]['columns'][0]['order']) == '0') {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    /**
     * getting data from a table
     *
     * @param tableName $tableName table name
     * @return array
     */
    public function getAvailableData($tableName)
    {

        $sql = $this->pdo->select(["type", "name", "ui_name", "column_name"])->from($tableName);
        $sql->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $status = array();
        $columns = array();

        foreach ($data as $quotes) {
            $renderName =  array();

            if ($quotes['type'] == 'column') {
                $renderName['ui_name'] = $quotes['ui_name'];
                $renderName['column_name'] = $quotes['column_name'];
                
                $columns[] = $renderName;
            }

            if ($quotes['type'] == 'status') {
                $status[] = $quotes['name'];
            }
        }

        return array("statuses" => $status, "columns" => $columns );
    }

    /**
     * formatting sample data
     *
     * @return array
     */
    public function formatSampleData()
    {

        $sql = $this->pdo->select(["DISTINCT(row_id) as row_id"])->from("SQ_sample_data");
        $sql->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $responseData = array();

        foreach ($data as $sampleData) {
            $responseData[] = $this->getSampledData($sampleData['row_id']);
        }

        return $responseData;
    }


    /**
     * getting sample data
     *
     * @param id $id
     * @return array
     */
    public function getSampledData($id)
    {

        $sql = $this->pdo->select(["name", "value"])->from("SQ_sample_data");
        $sql->where('active', '=', 1);
        $sql->where('row_id', '=', $id);

        $stmt = $sql->execute();

        $data =  $stmt->fetchAll();
         
        $response = array();

        foreach ($data as $sampleData) {
            $response[$sampleData['name']] = $sampleData['value'];
        }

        return $response;
    }
}
