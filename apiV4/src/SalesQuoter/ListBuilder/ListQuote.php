<?php

namespace SalesQuoter\ListBuilder;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class ListQuote
 *
 */
class ListQuote extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "type", "name");
        $config = array("table" => 'SQ_available_quotes', 'fields' => $fields);
        parent::__construct($config);
    }

}
