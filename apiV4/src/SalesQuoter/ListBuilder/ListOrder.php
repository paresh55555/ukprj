<?php

namespace SalesQuoter\ListBuilder;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class ListOrder
 *
 */
class ListOrder extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "type", "name");
        $config = array("table" => 'SQ_available_order', 'fields' => $fields);
        parent::__construct($config);
    }

}
