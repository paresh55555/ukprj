<?php

namespace SalesQuoter\PremadeColor;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Cache Class
 *
 */
class PremadeColor extends CustomAbstractCrud
{
    /**
     * Returns Construct function
     *
     */
    public function __construct()
    {

        $fields = array("id", "system_id", "grouped_under", "menu_id", "name");
        $config = array("table" => 'SQ_components', 'fields' => $fields);
        parent::__construct($config);
    }

   /**
     * Returns all premade subGroups
     *
     * @param groupId $groupId Group id
     *
     * @return returns bolean
     */
    public function allSubGroups($groupId)
    {

        $sql = $this->pdo->select(['id', 'name', 'system_id'])->from($this->table);
        $sql->where($this->table.'.grouped_under', '=', $groupId);
        $sql->where($this->table.'.type', '=', 'preMadeColorSubGroup');
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        for ($count = 0; $count < sizeof($data); $count++) {
              $data[$count]['colors'] = $this->getPremadeColors($data[$count]['id']);
              $data[$count]['traits'] = $this->getComponentsTraits($data[$count]['id']);
        }

        return $data;
    }

    /**
     * Returns all premadeColors
     *
     * @param componentId $componentId Component Id
     *
     * @return returns array
     */
    public function getPremadeColors($componentId)
    {

        $sql = $this->pdo->select(['id', 'name'])->from("SQ_new_options");
        $sql->where('component_id', '=', $componentId);
        $sql->where('active', '=', 1);

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();
        $allOptions = array();

        foreach ($data as $d) {
            $options = [];
            $allTraits = [];
            $options['id'] = $d['id'];
            $options['name'] = $d['name'];

            $allTraits = $this->getTraits($componentId, $d['id']);

            foreach ($allTraits as $traits) {
                if ($traits['name'] == 'imgUrl') {
                     $options[$traits['name']] =  $this->checkUpdate($traits['value'], $componentId, $options['id'], $traits['id']);
                } else {
                    $options[$traits['name']] = $traits['value'];
                }
            }


             $allOptions[] = $options;
        }

        return $allOptions;
    }

    public function checkUpdate($imageName, $componentId, $optionId, $traitsId)
    {

        $exists = strpos($imageName, "api/V4/premadecolors");

        if ($exists === false) {
            return  $this-> updateImageUrl($imageName, $componentId, $optionId, $traitsId);
        } else {
            return $imageName;
        }
    }

    public function updateImageUrl($imageName, $componentId, $optionId, $traitsId)
    {

         $imagePath = "images/premadeColors/".$imageName;

        if (file_exists($imagePath)) {
             $uploadPath = 'images/premadecolors';
             mkdir($uploadPath);
             $uploadPath .= "/0";
             mkdir($uploadPath);
             $uploadPath .= "/".$componentId;
             mkdir($uploadPath);
             $uploadPath .= "/".$optionId;
             mkdir($uploadPath);

             $pathInfo = pathinfo($imageName);

             $newImagePath =  $uploadPath."/".$traitsId.".".$pathInfo['extension'];

             $isCopied = copy($imagePath, $newImagePath);

            if ($isCopied) {
                return "api/V4/".$newImagePath;
            } else {
                return $imageName;
            }
        } else {
            return $imageName;
        }
    }

    /**
     * Returns all Traits
     *
     * @param componentId $componentId Component Id
     * @param optionId    $optionId    optionId
     *
     * @return returns array
     */
    public function getTraits($componentId, $optionId)
    {

        $sql = $this->pdo->select(['id', 'name', 'value'])->from("SQ_options_traits");
        $sql->where('component_id', '=', $componentId);
        $sql->where('option_id', '=', $optionId);
        $sql->where('active', '=', 1);

        $stmt = $sql->execute();

        return  $stmt->fetchAll();
    }

    public function getComponentsTraits($componentId)
    {

        $sql = $this->pdo->select(['id', 'name', 'value', 'type', 'visible'])->from("SQ_traits");
        $sql->where('component_id', '=', $componentId);
        $sql->where('active', '=', 1);

        $stmt = $sql->execute();

        return  $stmt->fetchAll();
    }
}
