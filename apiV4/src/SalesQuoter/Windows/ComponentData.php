<?php


namespace SalesQuoter\Windows;

use SalesQuoter\MysqlConnection;

class ComponentData
{

    protected $module;
    protected $mysqlConnection;

    public function __construct($module)
    {
        $this->module = $module;
        $this->mysqlConnection = new MysqlConnection();
    }


    public function getSashFrameGroup()
    {
        $frameGroup = array();
        $frameGroup['topFrameData'] = $this->getComponent('sashFrameTop');
        $frameGroup['bottomFrameData'] = $this->getComponent('sashFrameBottom');
        $frameGroup['leftFrameData'] = $this->getComponent('sashFrameLeft');
        $frameGroup['rightFrameData'] = $this->getComponent('sashFrameRight');

        return $frameGroup;
    }


    public function getFrameGroup()
    {
        $frameGroup = array();
        $frameGroup['topFrameData'] = $this->getComponent('frameTop');
        $frameGroup['bottomFrameData'] = $this->getComponent('frameBottom');
        $frameGroup['leftFrameData'] = $this->getComponent('frameLeft');
        $frameGroup['rightFrameData'] = $this->getComponent('frameRight');

        return $frameGroup;
    }

    public function getBeadGroup()
    {
        $beadGroup = array();
        $beadGroup['topBeadData'] = $this->getComponent('beadTop');
        $beadGroup['bottomBeadData'] = $this->getComponent('beadBottom');
        $beadGroup['leftBeadData'] = $this->getComponent('beadLeft');
        $beadGroup['rightBeadData'] = $this->getComponent('beadRight');
        $beadGroup['priority'] = 'top';

        return $beadGroup;
    }


    public function getMullion()
    {
        $data = $this->getComponent('mullion');
        return $data;
    }

    public function getTransom()
    {
        $data = $this->getComponent('transom');
        return $data;
    }

    public function getBead()
    {
        $data = $this->getComponent('Bead');
        return $data;
    }

    public function getGlass()
    {
        $data = $this->getGlassData('e3');

        return $data;
    }
//
//    public function getExampleBeadData()
//    {
//        $data = array("thickness" => 30, "burnOff" => 2, "overlap" => 5, "costPerMeter" => 1 );
//        return $data;
//    }
//
//    public function getExampleGlass()
//    {
//        $data = array("type" => "e3", "gap" => 5, "name" => "E3 with Argon" );
//        return $data;
//    }
//
//    public function getExampleSashConfig()
//    {
//        $data = array("gap" => "5", "openingHeight" => 300, "openingWidth" => 400, "opener" => "bottom" );
//
//        return $data;
//    }
//
//
//    public function getExampleSashFrameData()
//    {
//        $data = array("thickness" => 48, "burnOff" => 2, "overlap" => 5, "costPerMeter" => 1 );
//        return $data;
//    }


//    public function getExampleFrameData()
//    {
//        $data = array("thickness" => 50, "burnOff" => 2, "overlap" => 5 , "costPerMeter" => 1);
//        return $data;
//    }

//    public function getExampleMullionData()
//    {
//        $data = array("thickness" => 40, "burnOff" => 2, "overlap" => 5 , "costPerMeter" => 1);
//        return $data;
//    }
//
//    public function getExampleTransomData()
//    {
//        $data = array("thickness" => 42, "burnOff" => 2, "overlap" => 5 , "costPerMeter" => 1 );
//        return $data;
//    }

    public function getSashGap()
    {
        $dbObj = $this->mysqlConnection->connect_db_pdo();

        $sql = "SELECT * from sash where module=:module limit 1";

        $stmt = $dbObj->prepare($sql);
        $stmt->bindParam(":module", $this->module);
        $stmt->execute();
        $data = $stmt->fetch();

        return $data['gap'];
    }


    protected function getGlassData($type)
    {
        $dbObj = $this->mysqlConnection->connect_db_pdo();

        $sql = "SELECT * from glass where module=? and `type`= ? ";

        $stmt = $dbObj->prepare($sql);
        $stmt->bindParam("is", $this->module, $type);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = $results->fetch_assoc();

        return $data;
    }
    
    protected function getComponent($type)
    {
        $dbObj = $this->mysqlConnection->connect_db_pdo();

        $sql = "SELECT * from components2D where `module`=:module and `type`=:type ";
        $stmt = $dbObj->prepare($sql);
        $stmt->bindParam(":module", $this->module);
        $stmt->bindParam(":type", $type);
        $stmt->execute();
//        $results = $stmt->get_result();
        $data = $stmt->fetch();
        return $data;
    }
}
