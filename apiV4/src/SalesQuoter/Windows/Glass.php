<?php

namespace SalesQuoter\Windows;

class Glass extends AbstractWindowComponent
{

    protected $locationWidth;
    protected $locationHeight;
    protected $glassWidth;
    protected $glassHeight;
    protected $gap;
    protected $costPerSquareMeter;
    protected $config;


    protected $requiredProperties = array(
        "gap", "costPerSquareMeter"
    );

    /**
     *
     */
    public function init()
    {
    }


    /**
     * @return mixed
     */
    public function getCost()
    {
        $this->validateConfig();
        $cost = $this->costPerSquareMeter * ($this->glassWidth / 1000) * ($this->glassHeight / 1000);

        return $cost;
    }


    /**
     *
     * @param array $config
     */
    public function setSize($config)
    {
        $this->setConfig($config);
        $this->setGlassSize();
    }

    public function setGlassSize()
    {
        $this->glassWidth = $this->locationWidth - (2 * $this->gap);
        $this->glassHeight = $this->locationHeight - (2 * $this->gap);
    }

    /**
     * @return array
     */
    public function getCutSheet()
    {
        $this->validateConfig();
        $glass = array("name" => "Low E3 with Argon", "width" => $this->glassWidth, "height" => $this->glassHeight);

        return $glass;
    }


    /**
     *
     */
    protected function validateConfig()
    {
        if (empty($this->config['width'])) {
            throw new \RuntimeException('Required width was not passed in for Glass');
        }
        if (empty($this->config['height'])) {
            throw new \RuntimeException('Required height was not passed in for Glass');
        }
    }


    /**
     * Needs to be set for cutSheet and Cost
     * @param array $config
     */
    protected function setConfig($config)
    {
        $this->config = $config;
        $this->validateConfig();
        $this->locationWidth = $config['width'];
        $this->locationHeight = $config['height'];
    }

//    public function setLocationWidth($locationWidth)
//    {
//        $this->locationWidth = $locationWidth;
//    }
//
//    public function setLocationHeight($locationHeight)
//    {
//        $this->locationHeight = $locationHeight;
//    }


//Fracka --- Is this needed?
    /**
     *
     * @return mixed
     */
    public function getTopFrameThickness()
    {
        return $this->topFrame->getThickness();
    }

//Fracka --- Is this needed?
    /**
     *
     * @return mixed
     */
    public function getBottomFrameThickness()
    {
        return $this->bottomFrame->getThickness();
    }
}
