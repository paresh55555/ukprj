<?php


namespace SalesQuoter\Windows;

/**
 * Class MullionGroup
 *
 * @package SalesQuoter\Windows
 */
class MullionGroup extends AbstractWindowComponent
{
    protected $height;
    protected $frameGroup;
    protected $transom;
    protected $rowNumber;
    protected $lastRow;
    protected $parts;
    protected $windowData;
    protected $rows;
    protected $mullions;
    protected $mullion;

    protected $requiredProperties = array(
        "parts", "windowData"
    );


    /**
     * Init
     */
    public function init()
    {
        $this->lastRow = $this->windowData->numberOfRows();
        $this->frameGroup = $this->parts->getFrameGroup();
        $this->transom = $this->parts->getTransom();
        $this->mullion = $this->parts->getMullion();
        $this->buildMullions();
    }


    /**
     * @return mixed
     */
    public function getCutSheetParts()
    {
        return $this->mullions;
    }


    public function getCost()
    {
        $length = 0;
        foreach ($this->mullions as $mullion) {
            $length = $length + ($mullion['length'] * $mullion['number']);
        }

        $this->mullion->setLength($length);
        $cost = $this->mullion->getCost();

        return $cost;
    }


    /**
     *
     */
    protected function buildMullions()
    {
        $this->rowNumber=0;
        foreach ($this->windowData->getRows() as $row) {
            $this->buildMullionRow($row);
        }
    }


    /**
     * @param $row
     */
    protected function buildMullionRow($row)
    {
        $this->rowNumber++;
        $this->height = $row['height'];
        $numberOfMullion = $row['columns'] - 1;
        if ($numberOfMullion > 0) {
            $mullionHeight = $this->mullionHeight();
            $this->mullions[] = array ("name" => 'Mullions in Row'.$this->rowNumber, "length" => $mullionHeight, "number" => $numberOfMullion);
        }
    }


    /**
     * Calculates the mullion height depending on row and rows around it for vertical window systems
     * @return mixed
     */
    protected function mullionHeight()
    {
        $modifiedHeight = $this->height + (2 * $this->mullion->getBurnOff()) ;

        //First Row and Last Row
        if ($this->rowNumber == 1 && $this->lastRow == 1) {
            $mullionHeight = $modifiedHeight - ($this->frameGroup->getTopFrameThickness() + $this->frameGroup->getTopFrameThickness());
            return $mullionHeight;
        }

        //First Row and there are multiple rows
        if ($this->rowNumber == 1) {
            $mullionHeight = $modifiedHeight - ($this->frameGroup->getTopFrameThickness() + $this->transom->getDeduction());
            return $mullionHeight;
        }

        //Last Row
        if ($this->lastRow == $this->rowNumber) {
            $mullionHeight = $modifiedHeight - ($this->frameGroup->getBottomFrameThickness() + $this->transom->getDeduction());
            return $mullionHeight;
        }

        //Middle Row
        $mullionHeight = $modifiedHeight - ( 2 * $this->transom->getDeduction() );

        return $mullionHeight;
    }
}
