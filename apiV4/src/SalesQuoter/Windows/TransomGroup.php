<?php


namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Parts;
use SalesQuoter\Windows\WindowData;

class TransomGroup extends AbstractWindowComponent
{

    protected $config;
    protected $parts;
    protected $windowData;
    public $transoms;

    protected $requiredProperties = array(
        "parts", "windowData"
    );


    public function init()
    {
        if (!$this->windowData instanceof WindowData) {
            throw new \RuntimeException('WindowData object must be passed in');
        }

        if (!$this->parts instanceof Parts) {
            throw new \RuntimeException('Parts object must be passed in');
        }

        $this->buildTransoms();
    }


    /**
     *
     *
     * @return array
     */
    public function getCutSheetParts()
    {
        $cutSheetParts = array ();

        $count = 0;
        foreach ($this->transoms as $transom) {
            $count++;
            $part = $transom->getCutSheetParts();
            $part['name'] = 'Transom'.$count;

            $cutSheetParts[] = $part;
        }

        return $cutSheetParts;
    }


    public function getCost()
    {
        $cost = 0;

        foreach ($this->transoms as $transom) {
            $cost = $cost + $transom->getCost();
        }

        return $cost;
    }

    protected function buildTransoms()
    {
        $numberOfTransoms = count($this->windowData->getRows()) - 1;
        $count = 0;

        while ($count < $numberOfTransoms) {
            $transom = $this->parts->getTransom();

            $transom->setFrameWidth($this->windowData->getWidth());
            $this->transoms[] = $transom;
            $count++;
        }
    }
}
