<?php


namespace SalesQuoter\Windows;

use SalesQuoter\Windows\WindowData;

class WindowSystem
{

    protected $rows;
    protected $transomGroup;
    protected $mullionGroup;
    protected $frameGroup;
    protected $parts;
    protected $locations;
    protected $windowData;
    protected $pricing;
    

    public function __construct($windowData)
    {
        if (!$windowData instanceof WindowData) {
            throw new \RuntimeException('WindowData object must be passed in');
        }
        $this->windowData = $windowData;
        $this->init();
    }

    public function init()
    {
        $this->getPricing();
        $this->getParts();
        $this->buildFrame();
        $this->getTransomGroup();
        $this->getMullionsGroup();
        $this->getLocations();
    }

    public function getSystemInfo()
    {
        $systemInfo = array(
            "height" => $this->windowData->getHeight(),
            "width" => $this->windowData->getWidth(),
            "locations" => $this->windowData->getNumberOfLocations(),
            "type" => $this->windowData->getSystemType()
        );

        return $systemInfo;
    }

    public function getCutSheetParts()
    {
        $frame = $this->frameGroup->getCutSheetParts();
        $transoms = $this->transomGroup->getCutSheetParts();
        $locations = $this->locations->getCutSheetParts();
        $mullions = $this->mullionGroup->getCutSheetParts();

        $cutSheetParts = array(
            "system" => $this->getSystemInfo(),
            "parts" => array_merge($frame, array_merge($mullions, $transoms)),
            "locations" => $locations,
        );

        return $cutSheetParts;
    }


    protected function getMaterialCosts()
    {
        $materialCost = $this->frameGroup->getCost() + $this->transomGroup->getCost() + $this->mullionGroup->getCost() + $this->locations->getCost();

        return $materialCost;
    }

    /**
     *
     * @return array
     */
    public function getPrice()
    {
        $config = array("numberOfWindows" => $this->windowData->getNumberOfLocations(), "numberOfSashes" => $this->windowData->getNumberOfOpeners());

        if ($this->windowData->getPricingType() == 'fixed') {
            $price = array("price" => $this->pricing->getFixedPrice($config));
        } else {
            $config['materialCost'] = $this->getMaterialCosts();
            $price = array("price" => $this->pricing->getMaterialPrice($config));
        }

        return $price;
    }

    /**
     *
     */
    protected function getPricing()
    {
        $config = array("module" => $this->windowData->getModule() );
        $this->pricing = new Pricing($config);
    }


    /**
     *
     */
    protected function getParts()
    {
        $this->parts = new Parts($this->windowData->getModule());
    }

    /**
     *
     */
    protected function getTransomGroup()
    {
        $config = array("windowData" => $this->windowData, "parts" => $this->parts);
        $this->transomGroup = new TransomGroup($config);
    }


    /**
     *
     */
    protected function getMullionsGroup()
    {
        $config = array("windowData" => $this->windowData, "parts" => $this->parts);
        $this->mullionGroup = new MullionGroup($config);
    }


    /**
     *
     */
    protected function buildFrame()
    {
        $this->frameGroup = $this->parts->getFrameGroup();

        $size = array("width" => $this->windowData->getWidth(), "height" => $this->windowData->getHeight());
        $this->frameGroup->setSize($size);

        return $this->frameGroup;
    }


    /**
     * @return Locations
     */
    protected function getLocations()
    {
        $config = array("parts" => $this->parts, "windowData" => $this->windowData);

        $this->locations = new Locations($config);

        return $this->locations;
    }
}
