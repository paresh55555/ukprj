<?php


namespace SalesQuoter\Windows;

class Parts extends AbstractWindowComponent
{

    protected $module;
    protected $frame;
    protected $frameGroup;
    protected $sashFrameGroup;
    protected $sash;
    protected $mullion;
    protected $bead;
    protected $transom;
    protected $beadGroup;
    protected $componentData;
    protected $glass;
    protected $sashGap;

    protected $typeOfFrameGroup = 'frame';

    protected $requiredProperties = array(
        "module"
    );


    public function __construct($module)
    {
        $this->module = $module;
        $this->getComponentData();
    }

    public function init()
    {
    }

    /**
     * For testing
     *
     * @param $frame
     */
    public function setFrame($frame)
    {
        $this->frame = $frame;
    }

    public function setTypeOfFrameGroup($type)
    {
        $this->typeOfFrameGroup = $type;
    }

    /**
     *
     */
    public function getSashGap()
    {
        if (empty($this->sashGap)) {
            $this->sashGap = $this->componentData->getSashGap();
        }

        return $this->sashGap;
    }

    /**
     *
     * @return mixed
     */
    public function getSashFrameGroup()
    {
        if (empty($this->sashFrameGroup)) {
            $config = $this->componentData->getSashFrameGroup();
            $this->sashFrameGroup = $this->buildFrameGroup($config);
        }

        return clone ($this->sashFrameGroup);
    }

    /**
     * Get a frame per module and saved it's state.
     * @return mixed
     */
    public function getFrameGroup()
    {
        if ($this->typeOfFrameGroup == 'sash') {
            return $this->getSashFrameGroup();
        }

        if (empty($this->frameGroup)) {
            $config = $this->componentData->getFrameGroup();
            
            $this->frameGroup = $this->buildFrameGroup($config);
        }

        return clone ($this->frameGroup);
    }


    public function buildFrameGroup($config)
    {
        $frameGroup = new FrameGroup($config);

        return $frameGroup;
    }


    /**
     *
     * @return mixed
     */
    public function getGlass()
    {
        if (empty($this->glass)) {
            $config = $this->componentData->getGlass();
            $this->glass = $this->buildGlass($config);
        }

        return clone ($this->glass);
    }


    /**
     * @param $data
     * @return Glass
     */
    public function buildGlass($data)
    {
        $config = array("gap" => $data['gap'], "costPerSquareMeter" => $data['costPerSquareMeter']);
        $glass = new Glass($config);

        return $glass;
    }


    /**
     *
     * @return mixed
     */
    public function getBeadGroup()
    {
        if (empty($this->beadGroup)) {
            $config = $this->componentData->getBeadGroup();
            $this->beadGroup = $this->buildBeadGroup($config);
        }

        return clone ($this->beadGroup);
    }

    /**
     *
     * @param $config
     * @return BeadGroup
     */
    public function buildBeadGroup($config)
    {
        $beadGroup = new BeadGroup($config);

        return $beadGroup;
    }

    /**
     * Get a frame per module and saved it's state.
     * @return mixed
     */
    public function getFrame()
    {
        if (empty($this->frame)) {
            $this->frame = $this->componentData->getFrame();
        }

        return $this->frame;
    }

    /**
     * Get a mullion per module and saved it's state.
     * @return mixed
     */
    public function getMullion()
    {
        if (empty($this->mullion)) {
            $config = $this->componentData->getMullion();
            $this->mullion = $this->buildMullion($config);
        }

        return clone ($this->mullion);
    }


    public function buildMullion($config)
    {
        $mullion = new Mullion($config);

        return $mullion;
    }

    /**
     *
     * @return mixed
     */
    public function getBead()
    {
        if (empty($this->bead)) {
            $this->bead = $this->componentData->getBead();
        }

        return $this->bead;
    }


    /**
     * Get a mullion per module and saved it's state.
     * @return mixed
     */
    public function getTransom()
    {
        if (empty($this->transom)) {
            $config = $this->componentData->getTransom();
            $this->transom = $this->buildTransom($config);
        }

        return clone ($this->transom);
    }


    /**
     *
     * @param $config
     * @return Transom
     */
    public function buildTransom($config)
    {
        $config['frameGroup'] = $this->getFrameGroup();
        $transom = new Transom($config);

        return $transom;
    }

    /**
     * Get ComponentData Object in a testable manner
     */
    public function getComponentData()
    {
        $this->componentData = new ComponentData($this->module);
    }
}
