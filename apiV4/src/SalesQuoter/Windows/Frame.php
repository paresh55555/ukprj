<?php

namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Component2D;

class Frame extends AbstractWindowComponent
{

    protected $topFrameData;
    protected $bottomFrameData;
    protected $leftFrameData;
    protected $rightFrameData;

    protected $topFrame;
    protected $bottomFrame;
    protected $leftFrame;
    protected $rightFrame;

    protected $requiredProperties = array(
        "topFrameData", "bottomFrameData", "leftFrameData", "rightFrameData"
    );


    public function init()
    {
        $this->topFrame = new Component2D($this->topFrameData);
        $this->bottomFrame = new Component2D($this->bottomFrameData);
        $this->leftFrame = new Component2D($this->leftFrameData);
        $this->rightFrame = new Component2D($this->rightFrameData);
    }


    public function getTopFrameThickness()
    {
        return $this->topFrame->getThickness();
    }

    public function getBottomFrameThickness()
    {
        return $this->bottomFrame->getThickness();
    }


    public function cutSheetPart()
    {
    }
}
