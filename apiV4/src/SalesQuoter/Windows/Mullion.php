<?php


namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Component2D;

class Mullion extends Component2D
{

    protected $height;


    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getSize()
    {
    }


    public function getDeduction()
    {
        $deduction = $this->getThickness() / 2;

        return $deduction;
    }

    public function cutSheetPart()
    {
    }
}
