<?php


namespace SalesQuoter\Windows;

/**
 *
 * @SuppressWarnings(PHPMD)
 *
 */
class PartsExamples extends Parts
{

    public function __construct()
    {

        $this->module = 26;
        $this->componentData = new ComponentDataExamples();
        $this->buildTestPart();
    }

    public function init()
    {
    }

    public function buildTestPart()
    {
        $this->frameGroup = $this->exampleFrameGroup();
        $this->mullion = $this->exampleMullion();
        $this->transom = $this->exampleTransom();
        $this->beadGroup = $this->exampleBeadGroup();
        $this->glass = $this->exampleGlass();
    }



    /**
     * Builds an example sash frame config
     *
     * @return array
     */
    public function exampleSashFrameGroupConfig()
    {
        return $this->exampleFrameGroupConfig();
    }


    /**
     * Builds an example frame config
     *
     * @return array
     */
    public function exampleFrameGroupConfig()
    {
        $frameData = ComponentDataExamples::getExampleFrameData();
        $frameDataPriority = $frameData;
        $frameDataPriority['priority'] = true;

        $frameGroupConfig = array(
            'topFrameData' => $frameData,
            'bottomFrameData' => $frameData,
            'leftFrameData' => $frameDataPriority,
            'rightFrameData' => $frameDataPriority
        );

        return $frameGroupConfig;
    }

    public function exampleBeadGroup()
    {

        $exampleBead = ComponentDataExamples::getExampleBeadData();

        $config = array(
            'priority' => 'top',
            'topBeadData' => $exampleBead,
            'bottomBeadData' => $exampleBead,
            'leftBeadData' => $exampleBead,
            'rightBeadData' => $exampleBead
        );

        $beadGroup = $this->buildBeadGroup($config);

        return $beadGroup;
    }

    /**
     * Example FramGroup
     *
     * @return FrameGroup
     */
    public function exampleFrameGroup()
    {
        $frameGroupConfig = $this->exampleFrameGroupConfig();
        $frameGroup = $this->buildFrameGroup($frameGroupConfig);

        return $frameGroup;
    }


    /**
     * Example Mullion
     *
     * @return Mullion
     */
    public function exampleMullion()
    {
        $mullionConfig = ComponentDataExamples::getExampleMullionData();
        $mullion = new Mullion($mullionConfig);

        return $mullion;
    }


    /**
     * Example SashConfig
     *
     * @return array
     */
    public function exampleSashConfig()
    {

        $sashConfig = ComponentDataExamples::getExampleSashConfig();

        return $sashConfig;
    }
    

    /**
     * Example Glass
     *
     * @return Glass
     */
    public function exampleGlass()
    {
        $glassConfig = ComponentDataExamples::getExampleGlass();
        $glass = new Glass($glassConfig);

        return $glass;
    }


    /**
     * Example Transom
     *
     * @return Transom
     */
    public function exampleTransom()
    {
        $transomConfig = ComponentDataExamples::getExampleTransomData();
        $transom = $this->buildTransom($transomConfig);

        return $transom;
    }


    /**
     * Example Bead
     *
     * @return Transom
     */
    public function exampleBeads()
    {
        $transomConfig = ComponentDataExamples::getExampleTransomData();
        $transom = new Transom($transomConfig);

        return $transom;
    }
}
