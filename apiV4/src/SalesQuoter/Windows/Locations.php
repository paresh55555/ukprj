<?php

namespace SalesQuoter\Windows;

class Locations extends AbstractWindowComponent
{
    protected $parts;
    protected $windowData;
    protected $locations;

    protected $requiredProperties = array(
        "windowData", "parts"
    );

    public function init()
    {
        $this->buildLocations() ;
    }

    /**
     *
     * @return array
     */
    public function getCutSheetParts()
    {
        $cutSheetParts = [];
        
        foreach ($this->locations as $location) {
            $cutSheetParts[] = $location->getCutSHeetParts();
        }

        return $cutSheetParts;
    }


    public function getCost()
    {
        $cost = 0;

        foreach ($this->locations as $location) {
            $cost = $cost + $location->getCost();
        }

        return $cost;
    }



    protected function buildLocations()
    {
        $windowType = $this->windowData->getWindowType();
        $rows = $this->windowData->getRows();
        $this->parts->getSashGap();
        $rowNumber = 1;

        foreach ($rows as $row) {
            $widthNumber = 1;
            foreach ($row['widths'] as $width) {
                $whichLocation = $this->windowData->getWhichLocation($rowNumber, $widthNumber, $rows);

                $this->parts->setTypeOfFrameGroup('frame');
                $config = array(
                    "height" => $row['height'],
                    "width" => $width,
                    "windowType" => $windowType,
                    "whichLocation" => $whichLocation,
                    "parts" => $this->parts,
                    "opener" => $row['openers'][$widthNumber - 1], );


                $this->locations[] = $this->getLocation($config);

                $widthNumber++;
            }
            $rowNumber++;
        }

        return $this->locations;
    }


    protected function getLocation($config)
    {
        
        $location = new LocationSize($config);

        return $location;
    }
}
