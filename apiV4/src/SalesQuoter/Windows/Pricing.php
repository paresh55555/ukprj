<?php


namespace SalesQuoter\Windows;

class Pricing extends AbstractWindowComponent
{

    protected $height;
    protected $cost;
    protected $costPerWindow;
    protected $costPerSash;
    protected $markUp;

    protected $module;
    protected $pricingData;


    protected $requiredProperties = array(
        "module"
    );

    public function init()
    {
        $this->getPricingData();
    }

    protected function getPricingData()
    {

        $this->pricingData = new PricingData($this->module);
    }

    public function getFixedPrice($config)
    {
        $this->getPricingForModule();
        $numberOfWindows = $config['numberOfWindows'];
        $numberOfSashes = $config['numberOfSashes'];

        $cost =  ($numberOfSashes * $this->costPerSash + $numberOfWindows * $this->costPerWindow) * (1 + $this->markUp/100);

        return $cost;
    }

    public function getMaterialPrice($config)
    {
        $this->getPricingForModule();
        $numberOfWindows = $config['numberOfWindows'];
        $numberOfSashes = $config['numberOfSashes'];
        $materialCost = $config['materialCost'];

        $cost = ($numberOfSashes * $this->costPerSash + $numberOfWindows * $this->costPerWindow + $materialCost) * (1 + $this->markUp/100);

        return $cost;
    }

    public function getPricingForModule()
    {
        $data = $this->pricingData->getPricingData();

        $this->costPerSash = $data['costPerSash'];
        $this->costPerWindow = $data['costPerWindow'];
        $this->markUp = $data['markUp'];
    }
}
