<?php

namespace SalesQuoter\Windows;

use SalesQuoter\AbstractCrud;

class Window extends AbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "windowSection", "Layout", "stacking", "order", "img", "minWidthSize", "maxWidthSize");
        $config = array("table" => 'SQ_windows', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function getTypesOfWindowsBasedOnSize($data = array())
    {

        $sql = $this->pdo->select(['*'])->from('SQ_windows');

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }
}
