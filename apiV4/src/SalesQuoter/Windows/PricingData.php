<?php


namespace SalesQuoter\Windows;

use SalesQuoter\MysqlConnection;

class PricingData
{

    protected $module;
    protected $mysqlConnection;

    public function __construct($module)
    {
        $this->module = $module;
        $this->mysqlConnection = new MysqlConnection();
    }

    public function getPricingData()
    {
        $dbCon = $this->mysqlConnection->connect_db_pdo();

        $sql = "SELECT * from pricing where module=:module limit 1";

        $stmt = $dbCon->prepare($sql);
        $stmt->bindParam(":module", $this->module);
        $stmt->execute();
        $data = $stmt->fetch();

        return $data;
    }
}
