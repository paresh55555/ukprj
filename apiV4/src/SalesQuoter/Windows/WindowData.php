<?php


namespace SalesQuoter\Windows;

class WindowData extends AbstractWindowComponent
{

    protected $height;
    protected $width;
    protected $rows;
    protected $type;
    protected $module;
    protected $numberOfLocations;
    protected $systemType;
    protected $pricingType;

    protected $requiredProperties = array(
        "height", "width", "rows", "type", "module", "systemType", "pricingType"
    );

    protected $requiredInRows = array(
        "columns", "widths", "height"
    );

    protected $validTypes = array(
        "vertical", "horizontal"
    );


    /**
     *
     */
    public function init()
    {
        $this->validateType();
        $this->validateRows();
        $this->validateRowsWidths();
    }


    public function numberOfRows()
    {
        return count($this->rows);
    }

    /**
     * @return mixed
     */
    public function getRows()
    {
        return $this->rows;
    }


    /**
     * @return mixed
     */
    public function getModule()
    {
        return $this->module;
    }


    public function getSystemType()
    {
        return $this->systemType;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    
    public function getNumberOfLocations()
    {
        $total = 0;
        foreach ($this->rows as $row) {
            $total = $row['columns'] + $total;
        }

        return $total;
    }

    protected function countOpenersPerRow($row)
    {
        $count = 0;
        foreach ($row['openers'] as $opener) {
            if (!empty($opener)) {
                $count++;
            }
        }

        return $count;
    }

    
    public function getNumberOfOpeners()
    {
        $total = 0;
        foreach ($this->rows as $row) {
            $count = $this->countOpenersPerRow($row);
            $total = $total + $count;
        }

        return $total;
    }

    public function getWhichLocation($rowNumber, $widthNumber)
    {
        $whichLocation = array();
        $count = 1;
        while ($count <= count($this->rows)) {
            if ($count == $rowNumber) {
                array_push($whichLocation, $widthNumber);
            } else {
                array_push($whichLocation, 0);
            }
            $count++;
        }

        return $whichLocation;
    }


    public function getPricingType()
    {
        return $this->pricingType;
    }


    public function getWindowType()
    {
        $windowType = array();

        foreach ($this->rows as $row) {
            array_push($windowType, count($row['widths']));
        }

        return $windowType;
    }


    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }


    public static function getSampleData()
    {

        $row = array ("columns" => 3, "widths" => array("300", "100", "100"), "height" => "250", "openers" => array("top", "bottom", ""));
        $rows = array($row, $row, $row);

        $config = array(
            'height' => 500,
            'width' => 500,
            'rows' => $rows,
            'type' => 'vertical',
            'module' => 26,
            'systemType' => 'V-3-3-3',
            'pricingType' => 'material'
        );

        return $config;
    }
    /**
     *
     * @return WindowData
     */
    public static function createWithSampleData()
    {

//        $row = array ("columns" => 3, "widths" => array("300", "100", "100"), "height" => "250", "openers" => array("top", "bottom", ""));
//        $rows = array($row, $row, $row);
//
//        $config = array(
//            'height' => 500,
//            'width' => 500,
//            'rows' => $rows,
//            'type' => 'vertical',
//            'module' => 26,
//            'systemType' => 'V-3-3-3',
//            'pricingType' => 'material'
//        );

        $config = self::getSampleData();
        $windowData = new WindowData($config);

        return $windowData;
    }

    /**
     *
     */
    protected function validateType()
    {
        if (!in_array($this->type, $this->validTypes)) {
            throw new \RuntimeException('Invalid Type Passed');
        }
    }


    /**
     * Makes sure the correct rows values are passed
     *
     * Validate Rows
     */
    protected function validateRows()
    {
        foreach ($this->rows as $row) {
            foreach ($this->requiredInRows as $property) {
                if (!isset($row[$property])) {
                    throw new \RuntimeException('Required row property: '.$property.' was not passed in');
                }
            }
        }
    }


    /**
     *
     *
     */
    protected function validateRowsWidths()
    {
        foreach ($this->rows as $row) {
            $this->validateWidthCount($row);
            $this->validateWidthTotal($row);
        }
    }


    protected function validateWidthTotal($row)
    {
        $widthTotal = 0;
        foreach ($row['widths'] as $width) {
            $widthTotal = $widthTotal + $width;
        }
        if ($widthTotal != $this->width) {
            throw new \RuntimeException('Row widths are not adding up to width of the system');
        }
    }

    protected function validateWidthCount($row)
    {
        if ($row['columns'] != count($row['widths'])) {
            throw new \RuntimeException('Column count not matching passed in number of widths');
        }
    }
}
