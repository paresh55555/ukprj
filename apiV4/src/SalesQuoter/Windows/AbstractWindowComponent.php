<?php


namespace SalesQuoter\Windows;

abstract class AbstractWindowComponent
{

    protected $config;
    protected $testProperty;
    protected $requiredProperties = array ();
    protected $width;
    protected $height;

    abstract protected function init();
    
    /**
     * WindowRow constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->config = $config;
        $this->hasRequiredProperties($config);
        $this->initWithConfig($config);
        $this->init();
    }

    /**
     *
     * @param $size
     */
    public function setSize($size)
    {
        if (empty($size['width'])) {
            throw new \RuntimeException("Width wasn't passed in");
        }

        if (empty($size['height'])) {
            throw new \RuntimeException("Height wasn't passed in");
        }

        $this->height = $size['height'];
        $this->width = $size['width'];
    }

    /**
     * @param $config
     */
    protected function hasRequiredProperties($config)
    {
        foreach ($this->requiredProperties as $property) {
            if (!isset($config[$property])) {
                throw new \RuntimeException('Required property was not passed in: '.$property.' was not passed in');
            }
        }
    }

    /**
     * @param $config
     */
    protected function initWithConfig($config)
    {
        foreach ($this->requiredProperties as $property) {
            if (!property_exists($this, $property)) {
                throw new \RuntimeException('Protected Property of CLASS: '.$property.' does not exist');
            }
            $this->{$property} = $config[$property];
        }
    }
}
