<?php

namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Component2D;

class FrameGroup extends AbstractWindowComponent
{

    protected $topFrameData;
    protected $bottomFrameData;
    protected $leftFrameData;
    protected $rightFrameData;

    protected $topFrame;
    protected $bottomFrame;
    protected $leftFrame;
    protected $rightFrame;

    protected $height;
    protected $width;

    protected $requiredProperties = array(
        "topFrameData", "bottomFrameData", "leftFrameData", "rightFrameData"
    );


    public function init()
    {
        $this->topFrame = new Component2D($this->topFrameData);
        $this->bottomFrame = new Component2D($this->bottomFrameData);
        $this->leftFrame = new Component2D($this->leftFrameData);
        $this->rightFrame = new Component2D($this->rightFrameData);

        $this->buildFrameSizes();
    }

    /**
     *
     *
     * @return array
     */
    public function getCutSheetParts()
    {
        $cutSheetParts = array(
            array("name" => "topFrame", "length" => $this->topFrame->getLength(), "number" => 1),
            array("name" => "bottomFrame", "length" => $this->bottomFrame->getLength(), "number" => 1),
            array("name" => "leftFrame", "length" => $this->leftFrame->getLength(), "number" => 1),
            array("name" => "rightFrame", "length" => $this->rightFrame->getLength(), "number" => 1)
        );

        return $cutSheetParts;
    }

    public function getCost()
    {
        $cost =  $this->topFrame->getCost() + $this->bottomFrame->getCost() + $this->leftFrame->getCost() + $this->rightFrame->getCost();

        return $cost;
    }

    /**
     *
     * @param $size
     */
    public function setSize($size)
    {
        parent::setSize($size);
        $this->buildFrameSizes();
    }

    /**
     *
     */
    public function buildFrameSizes()
    {
        $this->setTopBottomFrameWidth($this->topFrame);
        $this->setTopBottomFrameWidth($this->bottomFrame);
        $this->setLeftRightFrameHeight($this->leftFrame);
        $this->setLeftRightFrameHeight($this->rightFrame);
    }


    /**
     * @param $frame
     */
    protected function setLeftRightFrameHeight($frame)
    {
        $deduction = 0;

        if ($this->topFrame->getPriority() <= $frame->getPriority()) {
            $deduction = $deduction + $this->topFrame->getThickness() + $frame->getBurnOff();
        }

        if ($this->bottomFrame->getPriority() <= $frame->getPriority()) {
            $deduction = $deduction +  $this->bottomFrame->getThickness() + $frame->getBurnOff();
        }

        $height = $this->height - $deduction;

        $frame->setLength($height);
    }


    /**
     * @param $frame
     */
    public function setTopBottomFrameWidth($frame)
    {
        $deduction = 0;

        if ($this->leftFrame->getPriority() < $frame->getPriority()) {
            $deduction = $deduction + $this->leftFrame->getThickness() + $frame->getBurnOff();
        }

        if ($this->rightFrame->getPriority() < $frame->getPriority()) {
            $deduction = $deduction +  $this->rightFrame->getThickness() + $frame->getBurnOff();
        }

        $width = $this->width - $deduction;

        $frame->setLength($width);
    }


    public function getTopFrameThickness()
    {
        return $this->topFrame->getThickness();
    }

    public function getBottomFrameThickness()
    {
        return $this->bottomFrame->getThickness();
    }


    /**
     * @return mixed
     */
    public function getTopFrame()
    {
        return $this->topFrame;
    }

    public function getBottomFrame()
    {
        return $this->bottomFrame;
    }

    public function getLeftFrame()
    {
        return $this->leftFrame;
    }

    public function getRightFrame()
    {
        return $this->rightFrame;
    }

    public function cutSheetPart()
    {
    }
}
