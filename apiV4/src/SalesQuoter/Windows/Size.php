<?php


namespace SalesQuoter\Windows;

class Size
{

    protected $height;
    protected $width;

    public function __construct($config)
    {
        if (empty($config['width'])) {
            throw new \RuntimeException("Width wasn't passed in");
        }

        if (empty($config['height'])) {
            throw new \RuntimeException("Height wasn't passed in");
        }

        $this->height = $config['height'];
        $this->width = $config['width'];
    }


    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }
}
