<?php


namespace SalesQuoter\Windows;

use SalesQuoter\MysqlConnection;

class CutSheet
{
    protected $mysqlConnection;


    public function __construct()
    {
        $this->mysqlConnection = new MysqlConnection();
    }

    public function insertCutSheet($cutSheet)
    {
        $dbObj = $this->mysqlConnection->connect_db_pdo();
        $sql = "INSERT INTO  cutSheets SET `cutSheet`=?";
        $stmt = $dbObj->prepare($sql);
        $stmt->bind_param("s", json_encode($cutSheet));
        $stmt->execute();

        $pId = $dbObj->insert_id;

        return $pId;
    }

    public function getCutSheet($pId)
    {
        $dbObj = $this->mysqlConnection->connect_db_pdo();

        $sql = "SELECT * from cutSheets where id=? ";

        $stmt = $dbObj->prepare($sql);
        $stmt->bind_param("i", $pId);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = $results->fetch_assoc();

        return $data;
    }
}
