<?php

namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Component2D;

class Sash extends AbstractWindowComponent
{
    protected $gap;
    protected $openingHeight;
    protected $openingWidth;
    protected $parts;

    protected $locationSize;
    protected $frameGroup;
    protected $beadGroup;
    protected $glass;

    protected $width;
    protected $height;
    protected $opener;


    protected $requiredProperties = array(
        "gap", "openingHeight", "openingWidth", "parts", "opener"
    );


    public function init()
    {
        
        $this->calculateHeight();
        $this->calculateWidth();

        $this->parts->setTypeOfFrameGroup('sash');
        $configLocation = $this->getLocationSizeConfig();
        $this->locationSize = $this->buildLocationSize($configLocation);

        $size = array("height" => $this->height, "width" => $this->width);
        $this->parts->getFrameGroup()->setSize($size);
    }


    /**
     * @return mixed
     */
    public function getCost()
    {
        $cost = $this->locationSize->getCost() + $this->parts->getFrameGroup()->getCost();
        return $cost;
    }


    /**
     * @return mixed
     */
    public function getCutSheetParts()
    {
        $cutSheetParts = $this->locationSize->getCutSheetParts();
        $cutSheetParts['parts'] = array_merge($this->parts->getFrameGroup()->getCutSheetParts(), $cutSheetParts['parts']);
        $cutSheetParts['sash'] = array("height" => $this->height, "width" => $this->width, "opener" => $this->opener);
        return $cutSheetParts;
    }


    /**
     * @return array
     */
    protected function getLocationSizeConfig()
    {
        $configLocationSize = array(
            'height' => $this->height,
            'width' => $this->width,
            'windowType' => array(1,1),
            'whichLocation' => array (1),
            'parts' => $this->parts,
        );

        return $configLocationSize;
    }


    /**
     * @param $config
     * @return LocationSize
     */
    protected function buildLocationSize($config)
    {
        $locationSize = new LocationSize($config);

        return $locationSize;
    }


    protected function calculateHeight()
    {
        $this->height = $this->openingHeight - (2 *$this->gap);
    }


    protected function calculateWidth()
    {
        $this->width = $this->openingWidth - (2 * $this->gap);
    }
}
