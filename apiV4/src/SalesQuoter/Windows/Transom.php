<?php


namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Part;

class Transom extends Component2D
{

    protected $frameWidth;
    protected $length;
    protected $frameGroup;

    protected $requiredProperties = array(
        "thickness", "burnOff", "overlap", "frameGroup", "costPerMeter"
    );

    public function init()
    {
    }

    /**
     *
     * @return array
     */
    public function getCutSheetParts()
    {

        $partSheet = array ("length" => $this->length, "number" => 1);

        return $partSheet;
    }


    public function getDeduction()
    {
        $deduction = $this->getThickness() / 2;

        return $deduction;
    }

    public function setFrameWidth($width)
    {
        $this->frameWidth = $width;
        $this->setTransomWidth();
    }

    protected function setTransomWidth()
    {
        $frameThickness = $this->frameGroup->getLeftFrame()->getThickness() + $this->frameGroup->getRightFrame()->getThickness();
        $burnOffs = $this->getBurnOff() * 2;
        $this->length = $this->frameWidth + $burnOffs - $frameThickness;
    }
}
