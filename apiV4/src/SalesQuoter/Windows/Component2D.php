<?php


namespace SalesQuoter\Windows;

class Component2D extends AbstractWindowComponent
{
    protected $thickness;
    protected $burnOff;
    protected $overlap;
    protected $length;
    protected $priority;
    protected $costPerMeter;

    protected $requiredProperties = array(
        "thickness", "burnOff", "overlap", "costPerMeter"
    );


    public function init()
    {
        $this->priority = 3;
        if (!empty($this->config['priority'])) {
            $this->priority = $this->config['priority'];
        }
    }

    public function getCost()
    {
        $cost = $this->costPerMeter * $this->length / 1000;

        return $cost;
    }


    public function getDeduction()
    {
        $deduction = $this->getThickness() ;

        return $deduction;
    }

    public function getOverlap()
    {
        return $this->overlap;
    }

    public function setLength($length)
    {
        $this->length = $length;
    }

    public function getLength()
    {
        return  $this->length ;
    }

    public function getThickness()
    {
        return $this->thickness;
    }

    public function getBurnOff()
    {
        return $this->burnOff;
    }

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
    }
}
