<?php

namespace SalesQuoter\Windows;

use SalesQuoter\Windows\Component2D;

class BeadGroup extends AbstractWindowComponent
{

    protected $topBeadData;
    protected $bottomBeadData;
    protected $leftBeadData;
    protected $rightBeadData;

    protected $topBead;
    protected $bottomBead;
    protected $leftBead;
    protected $rightBead;
    protected $priority;
    protected $height;
    protected $width;

    protected $requiredProperties = array(
        "topBeadData", "bottomBeadData", "leftBeadData", "rightBeadData", "priority"
    );

    protected $validPriorities = array("top", "side");

    /**
     *
     */
    public function init()
    {
        if (!in_array($this->priority, $this->validPriorities)) {
            throw new \RuntimeException('invalid priority passed');
        }
        $this->topBead = new Component2D($this->topBeadData);
        $this->bottomBead = new Component2D($this->bottomBeadData);
        $this->leftBead = new Component2D($this->leftBeadData);
        $this->rightBead = new Component2D($this->rightBeadData);
    }


    /**
     *
     * @param $height
     * @param $width
     * @return array
     */
    public function cutSheetPartsFromHeightAndWidth($height, $width)
    {
        $this->height = $height;
        $this->width = $width;
        $this->setBeadLength();
        
        $partSheet = array(
            array("name" => "Top Bead" , "length" => $this->topBead->getLength(), "number" => 1),
            array("name" => "Bottom Bead" ,"length" => $this->bottomBead->getLength(), "number" => 1),
            array("name" => "Left Bead" ,"length" => $this->leftBead->getLength(), "number" => 1),
            array("name" => "Right Bead" ,"length" => $this->rightBead->getLength(), "number" => 1),
        );
        
        return $partSheet;
    }


    public function costFromHeightAndWidth($height, $width)
    {
        $this->height = $height;
        $this->width = $width;
        $this->setBeadLength();

        $cost = $this->topBead->getCost() + $this->bottomBead->getCost() + $this->leftBead->getCost() + $this->rightBead->getCost();

        return $cost;
    }

    /**
     *
     */
    protected function setBeadLength()
    {
        $this->setTopBeadLength();
        $this->setBottomBeadLength();
        $this->setLeftBeadLength();
        $this->setRightBeadLength();
    }

    /**
     *
     */
    protected function setTopBeadLength()
    {
        $length = $this->setTopBottomLength($this->topBead);
        $this->topBead->setLength($length);
    }


    /**
     *
     */
    protected function setBottomBeadLength()
    {
        $length = $this->setTopBottomLength($this->bottomBead);
        $this->bottomBead->setLength($length);
    }


    /**
     *
     */
    protected function setLeftBeadLength()
    {
        $length = $this->setLeftRightLength($this->leftBead);
        $this->leftBead->setLength($length);
    }

    /**
     *
     */
    protected function setRightBeadLength()
    {
        $length = $this->setLeftRightLength($this->rightBead);
        $this->rightBead->setLength($length);
    }


    /**
     * Figures out the length of a right or left bead based on priority
     *
     * @return mixed
     */
    protected function setLeftRightLength($bead)
    {
        $deduction = 0;
        if ($bead->getPriority() >= $this->topBead->getPriority()) {
            $deduction = $deduction + $this->topBead->getThickness() + $bead->getBurnOff()  ;
        }

        if ($bead->getPriority() >= $this->bottomBead->getPriority()) {
            $deduction = $deduction + $this->bottomBead->getThickness() + $bead->getBurnOff()  ;
        }

        $length = $this->height - $deduction;

        return $length;
    }


    /**
     * Figures out the length of a top or bottom bead based on priority
     *
     * @param $bead
     * @return mixed
     */
    protected function setTopBottomLength($bead)
    {
        $deduction = 0;
        if ($bead->getPriority() > $this->leftBead->getPriority()) {
            $deduction = $deduction + $this->leftBead->getThickness() + $bead->getBurnOff()  ;
        }

        if ($bead->getPriority() > $this->rightBead->getPriority()) {
            $deduction = $deduction + $this->rightBead->getThickness() + $bead->getBurnOff()  ;
        }

        $length = $this->width - $deduction;

        return $length;
    }


//    public function getTopBeadThickness()
//    {
//        return $this->topBead->getThickness();
//    }
//
//    public function getBottomBeadThickness()
//    {
//        return $this->bottomBead->getThickness();
//    }
}
