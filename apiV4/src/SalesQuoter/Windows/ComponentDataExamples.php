<?php


namespace SalesQuoter\Windows;

use SalesQuoter\MysqlConnection;

class ComponentDataExamples extends ComponentData
{

    protected $module;
    protected $mysqlConnection;

    public function __construct()
    {
        //Override as mysql and module not needed for examples
    }

    public static function getExampleBeadData($priority = 3)
    {
        $data = array("thickness" => 30, "burnOff" => 2, "overlap" => 5, "costPerMeter" => 1, 'priority' => $priority );
        return $data;
    }

    public static function getExampleGlass()
    {
        $data = array("type" => "e3", "gap" => 5, "name" => "E3 with Argon", "costPerSquareMeter" => 100 );
        return $data;
    }

    public static function getExampleSashConfig()
    {
        $data = array("gap" => "5", "openingHeight" => 300, "openingWidth" => 400, "opener" => "bottom" );

        return $data;
    }


    public static function getExampleSashFrameData()
    {
        $data = array("thickness" => 48, "burnOff" => 2, "overlap" => 5, "costPerMeter" => 1 );
        return $data;
    }


    public static function getExampleFrameData($priority = 3)
    {
        $data = array("thickness" => 50, "burnOff" => 2, "overlap" => 5 , "costPerMeter" => 1, "priority" => $priority);
        return $data;
    }

    public static function getExampleMullionData()
    {
        $data = array("thickness" => 40, "burnOff" => 2, "overlap" => 5 , "costPerMeter" => 1);
        return $data;
    }

    public static function getExampleTransomData()
    {
        $data = array("thickness" => 42, "burnOff" => 2, "overlap" => 5 , "costPerMeter" => 1 );
        return $data;
    }
}
