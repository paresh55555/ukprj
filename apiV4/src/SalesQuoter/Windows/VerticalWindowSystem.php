<?php


namespace SalesQuoter\Windows;

class VerticalWindowSystem
{
    protected $module;

    protected $height;
    protected $width;

    protected $layout;
    protected $layoutDimensions;

    //Array of windowRows Objects
    protected $windowRows = array();

    //Frame Object
    protected $frame;

    //Array of Transom Objects
    protected $transoms;

    //Array of Location Objects
    //Can have Windows or Openers in them
    protected $locations;


    public function __construct()
    {
        $this->initWithConfig();
        $this->buildFrame();
        $this->buildTransoms();
        $this->buildWindowRows();
    }

    public function calculateCost()
    {
    }

    public function cutSheetData()
    {
    }

    protected function initWithConfig()
    {
    }


    protected function buildWindowRows()
    {
    }

    protected function buildFrame()
    {
    }

    protected function buildTransoms()
    {
    }
}
