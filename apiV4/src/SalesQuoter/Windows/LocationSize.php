<?php



namespace SalesQuoter\Windows;

/**
*
* @SuppressWarnings(PHPMD.TooManyFields)
*
*/
class LocationSize extends AbstractWindowComponent
{
 
    protected $height;
    protected $width;
    protected $windowType;
    protected $whichLocation;
    protected $parts;
    protected $type;

    protected $top;
    protected $bottom;
    protected $left;
    protected $right;

    protected $locationWidth;
    protected $locationHeight;

    protected $row;
    protected $column;
    protected $sash;


    protected $requiredProperties = array(
        "height", "width", "windowType", "whichLocation", "parts"
    );

    public function init()
    {
        $this->setType();
    }

    protected function addSash()
    {
        if (empty($this->sash)) {
            $config = array(
                "gap" => $this->parts->getSashGap(),
                "openingHeight" => $this->locationHeight,
                "openingWidth" => $this->locationWidth,
                "parts" => $this->parts,
                "opener" => $this->config['opener']);

            $this->sash = new Sash($config);
        }
    }


    protected function setType()
    {
        $this->type = 'frame';
        if (!empty($this->config['type'])) {
            $this->type = $this->config['type'];
        }
    }


    public function getCost()
    {
        $this->calculateRowAndColumn();
        $this->calculateLocationSize();

        if (!empty($this->config['opener'])) {
            $cost = $this->sashPartsCost();
        } else {
            $cost = $this->windowPartsCost();
        }

        return $cost;
    }


    /**
     *
     * @return mixed
     */
    public function getCutSheetParts()
    {
        $this->calculateRowAndColumn();
        $this->calculateLocationSize();

        if (!empty($this->config['opener'])) {
            $partSheet = $this->sashParts();
        } else {
            $partSheet = $this->windowParts();
        }

        $partSheet['column'] = $this->column;
        $partSheet['height'] = $this->locationHeight;
        $partSheet['width'] = $this->locationWidth;
        $partSheet['row'] = $this->row;


        return $partSheet;
    }


    protected function glassCost()
    {
        if (empty($this->locationWidth) || empty($this->locationHeight)) {
            $this->calculateLocationSize();
        }

        $glass = $this->parts->getGlass();
        $size = array ("height" => $this->locationHeight, "width" => $this->locationWidth);
        $glass->setSize($size);

        return $glass->getCost();
    }

    protected function sashPartsCost()
    {
        $this->addSash();

        return $this->sash->getCost();
    }
    
    protected function windowPartsCost()
    {
        $cost = $this->parts->getBeadGroup()->costFromHeightAndWidth($this->locationHeight, $this->locationWidth);
        $cost = $cost + $this->glassCost();
        
        return $cost;
    }

    
    protected function windowParts()
    {
        $partSheet['parts'] = $this->parts->getBeadGroup()->cutSheetPartsFromHeightAndWidth($this->locationHeight, $this->locationWidth);
        $partSheet['glass'] = $this->glassSheet();

        return $partSheet;
    }

    protected function sashParts()
    {

        $this->addSash();
        $partSheet = $this->sash->getCutSheetParts();

        return $partSheet;
    }

    /**
     *
     * @return mixed
     */
    protected function glassSheet()
    {
        if (empty($this->locationWidth) || empty($this->locationHeight)) {
            $this->calculateLocationSize();
        }

        $glass = $this->parts->getGlass();

        $config = array ("height" => $this->locationHeight, "width" => $this->locationWidth );
        $glass->setSize($config);

        return $glass->getCutSheet();
    }

    /**
     *
     * @return mixed
     */
    protected function calculateRight()
    {
        $this->right = $this->parts->getMullion();

        $count = 0;
        foreach ($this->whichLocation as $location) {
            $windowRow = $this->windowType[$count];

            if ($location != 1 && $location == $windowRow) {
                $this->right = $this->parts->getFrameGroup()->getRightFrame();
            }
            if (1 == $windowRow && 1 == $location) {
                $this->right = $this->parts->getFrameGroup()->getRightFrame();
            }
            $count++;
        }

        return $this->right;
    }


    /**
     *
     * @return mixed
     */
    protected function calculateLeft()
    {
        $this->left = $this->parts->getFrameGroup()->getLeftFrame();

        foreach ($this->whichLocation as $location) {
            if ($location != 1 && !empty($location)) {
                $this->left = $this->parts->getMullion();
            }
        }

        return $this->left;
    }


    /**
     *
     * @return mixed
     */
    protected function calculateTop()
    {
        $this->top = $this->parts->getFrameGroup()->getTopFrame();
        if (empty($this->whichLocation[0])) {
            $this->top = $this->parts->getTransom();
        }

        return $this->top;
    }


    protected function calculateBottom()
    {
        $this->bottom = $this->parts->getTransom();
        if (!empty(end($this->whichLocation))) {
            $this->bottom = $this->parts->getFrameGroup()->getBottomFrame();
        }

        return $this->bottom;
    }


    protected function buildBox()
    {
        $this->calculateTop();
        $this->calculateBottom();
        $this->calculateLeft();
        $this->calculateRight();
    }


    public function calculateLocationSize()
    {
        $this->buildBox();

        $this->locationWidth = $this->width - $this->left->getDeduction() - $this->right->getDeduction();
        $this->locationHeight = $this->height - $this->top->getDeduction() - $this->bottom->getDeduction();
        $locationSize = array("width" => $this->locationWidth, "height" => $this->locationHeight);

        return $locationSize;
    }


    public function calculateRowAndColumn()
    {
        $row = 1;
        foreach ($this->whichLocation as $number) {
            if ($number > 0) {
                $this->row = $row;
                $this->column = $number;
            }
            $row++;
        }
    }

    public function whichLocationString()
    {
        $locationString = '';
        foreach ($this->whichLocation as $number) {
            $locationString = $locationString.$number.'-';
        }
        $locationString = substr($locationString, 0, -1);

        return $locationString;
    }
}
