<?php

/**
 * Helper function for printing out what happened.
 */

/**
 * function safeDecodeJSON
 *
 * @param data $data json encoded data
 *
 * @return json encoded string
 *
*/
function safeDecodeJSON($data)
{
    $json = json_decode($data, true);
    if (json_last_error()) {
        throw new \RuntimeException(json_last_error_msg());
    }

    return $json;
}

/**
 * function writePhantomjsPDF
 *
 * @param config $config json encoded data
 *
 * @return all data array
*/
function writePhantomjsPDF($config)
{
    $url = $config['url'];
    $fileNamePath = $config['fileNamePath'];
    execPhantomjs($url, $fileNamePath);
}


/**
 * function writeQuoteOrderWithConfig
 *
 * @param config $config json encoded data
 *
 * @return array
*/
function writeQuoteOrderWithConfig($config)
{
    $config['writeOnly'] = true;
    printQuoteOrderWithConfig($config);
}

/**
 * function printQuoteOrderWithConfig
 *
 * @param config $config data arrat
 *
 * @return array
*/
function printQuoteOrderWithConfig($config)
{
    writePhantomjsPDF($config);
    if (empty($config['writeOnly'])) {
        $response = displayPDF($config);

        return $response;
    }
}

/**
 * function displayPDF
 *
 * @param config $config data arrat
 *
 * @return pdf stream
*/
function displayPDF($config)
{
    $response = $config['response'];
    $fileNamePath = $config['fileNamePath'];
    $fileName = $config['fileName'];

    $newResponse = $response->withHeader('Content-Type', 'application/pdf');
    $newResponse->withHeader('Pragma', "public");
    $newResponse->withHeader('Content-disposition:', 'attachment; filename='.$fileName);
    $newResponse->withHeader('Content-Transfer-Encoding', 'binary');
    $newResponse->withHeader('Content-Length', filesize($fileNamePath));

    readfile($fileNamePath);

    return $newResponse;
}

/**
 * function execPhantomJs
 *
 * @param url          $url          string
 * @param fileNamePath $fileNamePath fileNamePath string
 *
 * @return pdf stream
*/
function execPhantomJs($url, $fileNamePath)
{
    $command = "/usr/bin/phantomjs --ignore-ssl-errors=true /usr/bin/rasterize.js '".$url."' $fileNamePath 'Letter' ";

    exec($command);
}

/**
 *  @SuppressWarnings(PHPMD.ExitExpression) 
 *
 * function jsonDecodeWithErrorChecking
 *
 * @param json $json json encoded string
 *
 * @return json decoded array
 *
 */
function jsonDecodeWithErrorChecking($json)
{

    $decodedData = json_decode($json, true);

    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $errorMsg = '';

            break;
        case JSON_ERROR_DEPTH:
            $errorMsg = 'Maximum stack depth exceeded';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $errorMsg  = 'Underflow or the modes mismatch';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $errorMsg = 'Unexpected control character found';
            break;
        case JSON_ERROR_SYNTAX:
            $errorMsg = 'Syntax error, malformed JSON';
            break;
        case JSON_ERROR_UTF8:
            $errorMsg = 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
        default:
            $errorMsg = 'Unknown error';
            break;
    }

    
    if (strlen($errorMsg) == '0') {
        return $decodedData;
    }

    http_response_code(400);
    echo json_encode(array('status' => 'error', 'message' => 'Sending JSON data can not be parsed', 'error_type' => $errorMsg ));
    exit;
}

/**
 * function jsonDecode
 *
 * @param response $response json encoded string
 *
 * @return json decoded array
*/
function invalidPermissionsResponse($response)
{

    $returnData = array('status' => 'error', 'message' => 'not enough permission');
         
    return $response->withStatus(403)
                    ->withHeader("Content-Type", "application/json")
                    ->write(json_encode($returnData, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
}

/**
 * function jsonDecode
 *
 * @param response   $response   string
 * @param data       $data       json encoded string
 * @param statusCode $statusCode status code of content
 *
 * @return json content type header
*/
function responseWithStatusCode($response, $data, $statusCode)
{

    return $response->withStatus($statusCode)
                   ->withHeader("Content-Type", "application/json")
                   ->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
}

/**
 * Save premade colors
 * @param array   $colors
 * @param integer $systemID
 * @param integer $componentID
 */
function saveColors($colors, $systemID, $componentID)
{
    $options = new SalesQuoter\Options\Options();
    $optionTraits = new \SalesQuoter\Traits\OptionTraits();
    foreach ($colors as $color) {
        $colorConfig = [
            'name' => 'colorOption',
            'system_id' => $systemID,
            'component_id' => $componentID,
            'allowDeleted' => 1,
        ];
        $colorConfig['system_id'] = $systemID;
        $colorConfig['component_id'] = $componentID;

        $colorResult = $options->create($colorConfig);
        $colorID = $colorResult[0]['id'];

        $traits = [
            [
                'name' => 'title',
                'visible' => 1,
                'value' => $color['title'],
            ],
        ];
        if (!empty($color['hex'])) {
            array_push($traits, [
                'name' => 'hex',
                'value' => $color['hex'],
                'visible' => 1,
            ]);
        }
        if (!empty($color['note'])) {
            array_push($traits, [
                'name' => 'note',
                'value' => $color['note'],
                'visible' => 1,
            ]);
        }
        if (!empty($color['imgUrl'])) {
            if (strpos($color['imgUrl'], 'api/V4/') === false) {
                $filePath = "images/premadeColors/".$color['imgUrl'];
            } else {
                $filePath = str_replace('api/V4/', '', $color['imgUrl']);
            }

            $newImgFolder = "/systems/".$colorID;
            // full path folder like /var/www/dev-demo1.rioft.com/images/systems/1204
            $colorDir = $_SERVER['IMAGE_STORE'].$newImgFolder;

            if (!is_dir($colorDir)) {
                mkdir($colorDir);
            }

            $pathInfo = pathinfo($color['imgUrl']);
            // image file name
            $imagePath = "/systems_".$colorID.".".$pathInfo['extension'];
            $newFilePath = $colorDir.$imagePath;
            // only folder images
            $uploadPath = pathinfo($_SERVER['IMAGE_STORE'])['filename'];
            // the path to store in db images/systems/1204/1205.jpg
            $apiImagePath = $uploadPath.$newImgFolder.$imagePath;

            if (!copy($filePath, $newFilePath)) {
                throw new Exception('Copy subGroup image failed. Please check Image location and Permissions');
            }
            array_push($traits, [
                'name' => 'imgUrl',
                'value' =>  "api/V4/".$apiImagePath,
                'visible' => 1,
            ]);
        }

        foreach ($traits as $trait) {
            $traitConfig = $trait;
            $traitConfig['system_id'] = $systemID;
            $traitConfig['component_id'] = $componentID;
            $traitConfig['option_id'] = $colorID;
            $optionTraits->create($traitConfig);
        }
    }
}

/**
 * Save premade sub-groups with colors
 * @param array   $subGroups
 * @param integer $systemID
 * @param integer $componentID
 * @throws Exception
 */
function saveSubGroups($subGroups, $systemID, $componentID)
{
    $components = new \SalesQuoter\Components\Components();
    $traits = new \SalesQuoter\Traits\Traits();
    foreach ($subGroups as $subGroup) {
        $subGroupConfig = [
            'name' => 'ColorSubGroup',
            'type' => 'ColorSubGroup',
            'system_id' => $systemID,
            'grouped_under' => $componentID,
        ];

        $subGroupResult = $components->create($subGroupConfig);
        $subGroupID = $subGroupResult[0]['id'];

        foreach ($subGroup['traits'] as $trait) {
            if ($trait['name'] === 'imgUrl') {

                if (strpos($trait['value'], 'api/V4/') === false) {
                    $filePath = "images/premadeColors/premadeSubGroups/".$trait['value'];
                } else {
                    $filePath = str_replace('api/V4/', '', $trait['value']);
                }

                $newImgFolder =  "/systemGroup/".$subGroupID;
                $colorDir = $_SERVER['IMAGE_STORE'].$newImgFolder;

                if (!is_dir($colorDir)) {
                    mkdir($colorDir);
                }

                $pathInfo = pathinfo($trait['value']);

                $imagePath = "/systemGroup_".$subGroupID.".".$pathInfo['extension'];
                $newFilePath = $colorDir.$imagePath;
                // only folder images
                $uploadPath = pathinfo($_SERVER['IMAGE_STORE'])['filename'];
                // the path to store in db images/systems/1204/1205.jpg
                $apiImagePath = $uploadPath.$newImgFolder.$imagePath;

                //$newFilePath = $sGDir."/systemGroup_".$subGroupID.".".$pathInfo['extension'];

                if (!copy($filePath, $newFilePath)) {
                    throw new Exception('Copy subGroup image failed');
                }
            }
            $traits->create([
                'name' => $trait['name'],
                'value' => $trait['name'] == 'imgUrl' ? "api/V4/".$apiImagePath : $trait['value'],
                'component_id' => $subGroupID,
                'type' => 'component',
            ]);
        }
        if ($subGroup['colors'] && !empty($subGroup['colors'])) {
            saveColors($subGroup['colors'], $systemID, $subGroupID);
        }
    }
}

/**
 * Save premade sub-groups with colors
 * @param imagePath $imagePath
 * @param width     $width
 * @param height    $height
 * @return Image Path
 */
function resizeImage($imagePath, $width, $height)
{

    $thumb = new Imagick();
    $thumb->readImage(realpath($imagePath));

    $thumb->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
    $thumb->writeImage($imagePath);

    $thumb->destroy();

    return $imagePath;
}



/**
 * Send Email From this application
 *
 * @param fromEmail $fromEmail To Email Address
 * @param toEmail   $toEmail   sending Email Address
 * @param subject   $subject   subject of the messages
 * @param message   $message   messages
 * @param fileName  $fileName  file name of csv file
 * @param file      $file      fulll file path of csv file
 *
 * @return Image Path
 */
function sendEmail($fromEmail, $toEmail, $subject, $message, $fileName, $file)
{

    if (strlen($fromEmail) == '0') {
         $fromEmail = 'no-reply@salesquoter.com';
    }
    
    $from = new SendGrid\Email("Salesquoter Reports", $fromEmail);
    $eTo = new SendGrid\Email(null, $toEmail);
    $content = new SendGrid\Content("text/plain", $message);
    $fileEncoded = base64_encode(file_get_contents($file));
    $attachment = new SendGrid\Attachment();
    $attachment->setContent($fileEncoded);
    $attachment->setType("application/csv");
    $attachment->setDisposition("attachment");
    $attachment->setFilename($fileName);

    $mail = new SendGrid\Mail($from, $subject, $eTo, $content);
    $mail->addAttachment($attachment);

    $apiKey = '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';
    $sengGrid = new \SendGrid($apiKey);

    $response = $sengGrid->client->mail()->send()->post($mail);
    $statusCode = $response->statusCode();

    if ($statusCode == '200' || $statusCode == '201' || $statusCode == '202') {
        return true;
    } else {
        return false;
    }
}

/**
 * Send Reset Email From this application
 *
 * @param fromEmail $fromEmail To Email Address
 * @param toEmail   $toEmail   sending Email Address
 * @param subject   $subject   subject of the messages
 * @param message   $message   messages
 *
 * @return boolean
 */
function sendResetEmail($fromEmail, $toEmail, $subject, $message )
{

    if (strlen($fromEmail) == '0') {
         $fromEmail = 'no-reply@salesquoter.com';
    }
    
    $from = new SendGrid\Email("Reset Email", $fromEmail);
    $eTo = new SendGrid\Email(null, $toEmail);
    $content = new SendGrid\Content("text/html", $message);

    $mail = new SendGrid\Mail($from, $subject, $eTo, $content);

    $apiKey = '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';
    $sendGrid = new \SendGrid($apiKey);

    $response = $sendGrid->client->mail()->send()->post($mail);
    $statusCode = $response->statusCode();

    if ($statusCode == '200' || $statusCode == '201' || $statusCode == '202') {
        return true;
    } else {
        return false;
    }
}

/**
 * Send General Email From this application
 *
 * @param fromEmail $fromEmail To Email Address
 * @param toEmail   $toEmail   sending Email Address
 * @param subject   $subject   subject of the messages
 * @param message   $message   messages
 *
 * @return boolean
 */
function sendApiEmail($fromEmail, $toEmail, $subject, $message, $fileName, $file)
{

    if (strlen($fromEmail) == '0') {
         $fromEmail = 'no-reply@salesquoter.com';
    }
    
    $from = new SendGrid\Email("Salesquoter", $fromEmail);
    $eTo = new SendGrid\Email(null, $toEmail);
    $content = new SendGrid\Content("text/html", $message);

    $mail = new SendGrid\Mail($from, $subject, $eTo, $content);

    for ($count = 0; $count < sizeof($fileName); $count++) {

        $fileEncoded = base64_encode(file_get_contents($file[$count]));

        $attachment = new SendGrid\Attachment();
        $attachment->setContent($fileEncoded);
        $attachment->setType("application/csv");
        $attachment->setDisposition("attachment");
        $attachment->setFilename($fileName[$count]);

       
        $mail->addAttachment($attachment);
    }


    $apiKey = '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';
    $sengGrid = new \SendGrid($apiKey);

    $response = $sengGrid->client->mail()->send()->post($mail);
    $statusCode = $response->statusCode();

    if ($statusCode == '200' || $statusCode == '201' || $statusCode == '202') {
        return true;
    } else {
        return false;
    }
}

function generateUniqueCode() {

    $uniqueCode = sha1(uniqid(rand(), true)).md5(time());

    return $uniqueCode;
}

/**
 * return absoulte file path of the image
 *
 * @param reset redirect link
 *
 * @return Email Templae
 */
function getEmailTemplate( $link ) {

  return "<a href='".$link."' target='_blank' > Click </a> here to reset password";
}

/**
 * return absoulte file path of the image
 * @param folderName $folderName
 * @param fullPath     $fullPath
 * @return Image Path
 */
function parseImageName($folderName, $fullPath)
{

    $filePathArray = explode($folderName, $fullPath);
              
    return "api/V4/".$folderName.$filePathArray[1];
}

/**
 * Validating Input User Requeset.If not a valid field then discard
 *
 * @param request       $request       input request
 * @param tableFields   $tableFields   Tables valid fields
 *
 * @return valid fields
 */
function validateRequest( $request, $tableFields ) {

    foreach ($request as $fieldName => $fieldValue) {
            
        if ( !in_array( $fieldName, $tableFields ) ) {

            unset($request[$fieldName]);
        }
    }

    return $request;
}