<?php

namespace SalesQuoter\Quotes;

use SalesQuoter\Guest\Guest;
use SalesQuoter\Users\User;
use SalesQuoter\Customers\Customer;
use SalesQuoter\SoftDeleteAbstractCrud;

/**
 *  Quotes Class
 *
 */
class Quote extends SoftDeleteAbstractCrud
{

    /**
     * Returns Construct function
     *
     */
    public function __construct()
    {

        $fields = array('id', 'Customer');
        $config = array("table" => 'quotesSales', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * Returns Quotes Details Info
     *
     * @param quoteId $quoteId quoteSales table id
     *
     * @return Guest Array
     */
    public function getGuestQuote($quoteId)
    {

         $sql = $this->pdo->select(['id', 'Cart', 'quantity', 'guest', 'total', 'quoted', 'transferred'])
                          ->from($this->table)
                          ->where('id', '!=', $quoteId);

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }



    /**
     * Returns Quotes Status to Transferred
     *
     * @param salesPersonID $salesPersonID SalesPerson  id
     *
     * @return updates quote
     */
    public function clearUserTransferredQuotes($salesPersonID = null)
    {
        $stmt = $this->pdo->prepare("UPDATE quotesSales SET transferred = DEFAULT where status != 'deleted' AND SalesPerson = :spid");
        $stmt->bindParam(':spid', $salesPersonID);
        $stmt->execute();
    }

    /**
     * Returns Quotes Details Info
     *
     * @param quoteID $quoteID quoteSales table id
     *
     * @return updates quote
     */
    public function getQuoteInfo($quoteID = null)
    {
        $sql = $this->pdo->select(array(
            $this->table.'.id quoteId',
            $this->table.'.orderName',
            $this->table.'.type',
            $this->table.'.prefix',
            $this->table.'.total amount',
            $this->table.'.leadQuality quality',
            $this->table.'.status',
            'users.pipelinerId ownerId',
        ))->from($this->table);

        $sql->join('users', $this->table.'.SalesPerson', '=', "users.id")
            ->where($this->table.'.id', '=', $quoteID);

        $stmt = $sql->execute();

        $data = $stmt->fetch();

        return $data;
    }

    /**
     * Returns All Quotes By User Id
     *
     * @param pipelinerID $pipelinerID pipelinerId
     *
     * @return return Array
     */
    public function getQuotesByUserPLID($pipelinerID = null)
    {
        $sql = $this->pdo->select(array(
            $this->table.'.id quoteId',
            $this->table.'.type',
            $this->table.'.prefix',
            $this->table.'.total amount',
            $this->table.'.leadQuality quality',
            $this->table.'.status',
            $this->table.'.paidInFull',
            $this->table.'.SalesPerson',
            'users.pipelinerId ownerId',
            'customer.firstName',
            'customer.lastName',
            'customer.email',
            'customer.phone',
        ))->from($this->table);

        $sql->join('users', $this->table.'.SalesPerson', '=', "users.id AND users.pipelinerId = {$pipelinerID}")
            ->leftJoin('customer', $this->table.'.Customer', '=', 'customer.id');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Returns All Orders By Date Range
     *
     * @param startDate $startDate Start Date
     * @param endDate   $endDate   End Date
     *
     * @return return All Orders Array
     */
    public function getOrderByDateRange($startDate, $endDate)
    {

        $sql = $this->pdo->select(['SalesDiscount', 'cart', 'total', 'SalesPerson'])
            ->from($this->table)
            ->where('quoted', '>=', $startDate)
            ->where('quoted', '<=', $endDate)
            ->where('type', '!=', 'quote')
            ->where('status', '!=', 'deleted');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }
   

    /**
     * Returns Total Quantity
     *
     * @param cart $cart Total Cart items
     *
     * @return return All SalesMan Array
     */
    public function countQuantity($cart)
    {

        $totalQuantity = 0;

        $parsedCart = json_decode($cart, true);

        foreach ($parsedCart as $carts) {
            $totalQuantity = $totalQuantity + $carts['quantity'];
        }

        return $totalQuantity;
    }
   
    /**
     * Returns All Calculated Discounts
     *
     * @param allOrders $allOrders Total Cart items
     * @param totalDoor $totalDoor Total Cart items
     *
     * @return return All calculated Discounts array
     */
    public function calculateDiscount($allOrders, $totalDoor)
    {

        $totalOrders = 0;

        $totalDoorCosts = 0;
        $tlDorCostMinusDiscnt = 0;
        $totalDiscount = 0;
        $grandTotalDiscount = 0;

        foreach ($allOrders as $order) {
            $parseDiscountJson = json_decode($order['SalesDiscount'], true);

            $totalOrders = $totalOrders + $order['total'];
            $totalDoorCosts = $totalDoorCosts + $parseDiscountJson['Totals']['preCost'];
            $totalDiscount = $totalDiscount + $this->addAllDiscount($parseDiscountJson);
        }

        $tlDorCostMinusDiscnt = $totalDoorCosts - $grandTotalDiscount;

      
        if ($totalDoor == '0') {
            $totalDoor = 1;
        }

        $data = array();
        $data['averageCostPerDoor'] = $totalDoorCosts / $totalDoor;
        $data['averageCostPerDoorWithDiscount'] = ($totalDoorCosts - $totalDiscount) / $totalDoor;
        $data['averageCostPerDoorAllTogether'] = $totalOrders / $totalDoor;
        $data['averageDiscountPerDoor'] = (($totalDiscount / $totalDoor) / ($data['averageCostPerDoor']))*100 ;
        $data['totalOrders'] = $totalOrders;
        $data['totalDoors'] = $totalDoorCosts;
        $data['totalDiscount'] = $totalDiscount;


        return $data;
    }


     /**
     * Returns All Added discount
     *
     * @param discountJson $discountJson All discount json
     *
     * @return return Total discount
     */
    public function addAllDiscount($discountJson)
    {

        $total = 0;
        foreach ($discountJson['Discounts'] as $discount) {
            if ($discount['type'] == 'percentage') {
                $total = $total + ($discount["amount"]/100 * $discountJson['Totals']['preCost']);
            } else {
                $total = $total + $discount["amount"];
            }
        }


        return $total;
    }

 
    /**
     * Returns Total From Sales Person
     *
     * @param allOrders $allOrders All discount json
     * @param personId  $personId  personId
     *
     * @return return Total From SalesPerson
     */
    public function getTotalFromSalesPerson($allOrders, $personId)
    {

        $total = 0;

        foreach ($allOrders as $order) {
            if ($order['SalesPerson'] == $personId) {
                 $total = $total + $order['total'];
            }
        }


        return $total;
    }

     /**
     * Returns Inserted Data Array
     *
     * @param config $config All keys Values Data
     *
     * @return return Quotes Array
     */
    public function create($config)
    {

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));

        $insertId = $insertStatement->execute(true);

        $data = $this->get($insertId);

        return $data;
    }


    /**
     * Cleans all Cart
     *
     * @param cart $cart All Carts Json Data
     *
     * @return return Total From SalesPerson
     */
    public function cleanCart($cart)
    {
        $cleanCart = array();
        if (empty($cart)) {
            return '';
        }
        foreach ($cart as $door) {
            if (isset($door['total'])) {
                array_push($cleanCart, $door);
            }
        }

        return $cleanCart;
    }

   /**
     * Calculating Total discounts
     *
     * @param quote $quote Quotes Data
     *
     * @return returns total discount
     */
    public function getDiscounts($quote)
    {
        if (empty($quote['SalesDiscount'])) {
            return 0;
        }
        if (empty($quote['SalesDiscount']['Totals'])) {
            return 0;
        }
        if (empty($quote['SalesDiscount']['Discounts'])) {
            return 0;
        }

        $total = $this->cartTotal($quote);
        $markUp = $this->markUpAmount($quote);
        $preTotal = $total + ($total * $markUp);


        $totalDiscount = 0;

        foreach ($quote['SalesDiscount']['Discounts'] as $discount) {
            if ($discount['type'] == 'flat') {
                $totalDiscount = $totalDiscount + floatval($discount['amount']);
            } else {
                $totalDiscount = $totalDiscount + floatval((($discount['amount'] * $preTotal) / 100));
            }
        }

        return $totalDiscount;
    }

    /**
     * Get Guest Details
     *
     * @param cart $cart Cart array
     *
     * @return  total number of doors
     */
    public function guestNumberOfDoorsWithCart($cart)
    {
        $total = 0;
        foreach ($cart as $door) {
            if (isset($door['quantity'])) {
                $total = $total + $door['quantity'];
            }
        }

        return $total;
    }


   /**
     * Get Guest Details
     *
     * @param pId $pId Guest Table Id
     *
     * @return  guest Array
     */
    public function getGuest($pId)
    {
        
        $sql = $this->pdo->select(['*'])->from('guest');
        $sql->where('guest.id', '=', $pId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data[0] ;
    }


    /**
     * Returns All Quotes By Date Range
     *
     * @param startDate $startDate Start Date
     * @param endDate   $endDate   End Date
     *
     * @return return All Orders Array
     */
    public function getQuotesByDateRange($startDate, $endDate, $quote, $order)
    {

        $allowedFields = array(
            "cart",  
            "id",
            "quoted",
            "prefix",
            "total"
        );

        $sql = $this->pdo->select($allowedFields)
            ->from($this->table)
            ->where('quoted', '>=', $startDate)
            ->where('quoted', '<=', $endDate)
            ->where('type', '=', $quote)
            ->where('status', '!=', 'deleted');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $allQuotes = array(); 

        foreach ( $data as $quotes ) {
            $allQuotes[] = $quotes;
        }

        //Skip running the query to get orders
        if ($order != 'order') {
            return $allQuotes;
        }

        $sql = $this->pdo->select($allowedFields)
            ->from($this->table)
            ->where('quoted', '>=', $startDate)
            ->where('quoted', '<=', $endDate)
            ->where('type', '!=', 'quote');


        $stmt = $sql->execute();

        $data = $stmt->fetchAll();  

        foreach ( $data as $quotes ) {
            $allQuotes[] = $quotes;
        }
        
        return $allQuotes;
    }


    /**
     * Returns All Quotes By Date Range
     *
     * @param startDate $startDate Start Date
     * @param endDate   $endDate   End Date
     *
     * @return return All Orders Array
     */
    public function getQuotesSales($startDate, $endDate, $quote, $order)
    {

        $allQuotes = $this->getQuotesByDateRange($startDate, $endDate, $quote, $order);

        for ( $count = 0; $count < sizeof($allQuotes); $count++ ){

            $allQuotes[$count]['job_number'] = $allQuotes[$count]['prefix'].$allQuotes[$count]['id'];
            $allQuotes[$count]['number_of_panels'] = $this->calculateTotalPanels ( $allQuotes[$count]['cart'] );
            
            unset($allQuotes[$count]['id']);
            unset($allQuotes[$count]['prefix']);
            unset($allQuotes[$count]['cart']);
        }

        return $allQuotes;
    }

    /**
     *  Calculating total panels in cart json
     *
     * @param cart $cart cart json data
     *
     * @return return Integar
     */
    public function calculateTotalPanels( $cart ){

       $parsedCart = json_decode($cart, TRUE);

       $totalPanels = 0;

       foreach ( $parsedCart as $eachCart ) {

          $totalPanels += $eachCart['numberOfPanels']*$eachCart['quantity'];
       }

       return $totalPanels;
    }


    /**
     *  Calculating all delivered items in quoteSales table
     *
     * @return return Array
     */
    public function geDeliveredBalancedReport () {

        $allowedFields = array(
            "concat (`quotesSales`.`prefix`, `quotesSales`.`id`) as Order_number",  
            "customer.LastName as last_name",
            "customer.FirstName as first_name",
            "customer.companyName as company_name",
            "quotesSales.total as total",
            "quotesSales.balanceDue",
            "quotesSales.quoted"
        );

        $sql = $this->pdo->select($allowedFields)->from("quotesSales");
        $sql->join('customer', 'customer.id', '=', 'quotesSales.Customer');
        $sql->where('quotesSales.type', '=', 'Delivered');
        $sql->where('quotesSales.status', '!=', 'deleted');
        $sql->where('quotesSales.total', '>', 0);
        //Per Sharon 08/07/17
        $sql->where('quotesSales.balanceDue', '!=', 0);
        //Per Sharon 02/20/19
        $sql->where('quotesSales.balanceDue', '>', 0.009)->orWhere('quotesSales.balanceDue', '<',-0.009);
        $sql->where('quotesSales.paidInFull', '!=', 'yes');
        $sql->orderBy('quotesSales.quoted', 'ASC');


        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function getDeliveredOrders($min, $max)
    {
        $min = $min.  ' 00:00:00';
        $max = $max . ' 23:59:59';

        $allowedFields = array(
            "concat (`quotesSales`.`prefix`, `quotesSales`.`id`) as 'Job Number'",
            "concat (customer.FirstName, ' ', customer.LastName) as 'Customer Name'",
            "DATE_FORMAT(delivered,'%m/%d/%Y') as 'Delivered Date'",
            "total as Total"
        );

        $sql = $this->pdo->select($allowedFields)->from("quotesSales");
        $sql->join('customer', 'customer.id', '=', 'quotesSales.Customer');
        $sql->where('quotesSales.delivered', '>', $min);
        $sql->where('quotesSales.delivered', '<', $max);
        $sql->where('quotesSales.status', '!=', 'deleted');
        $sql->orderBy('quotesSales.delivered', 'ASC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function yearOldQuotes()
    {

        $time = strtotime("-1 year", time());
        $oneYearAgo = date("Y-m-d", $time);

        $allowedFields = array(
            "concat (`quotesSales`.`prefix`, `quotesSales`.`id`) as QuoteNumber",
            "quotesSales.quoted",
            "customer.LastName as CustomerLastName",
            "customer.FirstName as CustomerFirstName",
            "customer.companyName as CustomerCompany",
            "customer.email as CustomerEmail",
            "SQ_addresses.billing_contact as BillingName",
            "SQ_addresses.billing_email as BillingEmail",
            "SQ_addresses.shipping_contact as ShippingName",
            "SQ_addresses.shipping_email as ShippingEmail",
        );

        $sql = $this->pdo->select($allowedFields)->from("quotesSales");
        $sql->join('customer', 'customer.id', '=', 'quotesSales.Customer');
        $sql->join('SQ_addresses', 'SQ_addresses.quote_id', '=', 'quotesSales.id');
        $sql->where('quotesSales.type', '=', 'quote');
        $sql->where('quotesSales.status', '!=', 'deleted');
        $sql->where('quotesSales.quoted', '<', $oneYearAgo);
        $sql->orderBy('quotesSales.quoted', 'DESC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }
}
