<?php

namespace SalesQuoter;

require __DIR__.'/../../vendor/autoload.php';
require __DIR__.'/../../src/SalesQuoter/SalesPerson/Crm.php';

use SalesQuoter\SalesPerson\Crm;
use PipelinerSales\ApiClient\Query\Filter;

class CrmExt extends Crm
{

    /**
     * Method deletes:
     *  - accounts
     *  - activities
     *  - appointments
     *  - contacts
     *  - leads
     *  - messages
     *  - notes
     *  - opportunities
     *  - reminders
     */
    public function deletePipelinerAccountData()
    {

        $entitiesNames = array(
            'accounts',
            'activities',
            'appointments',
            'contacts',
            'leads',
            'messages',
            'notes',
            'opportunities',
            'reminders'
        );

        $filter = Filter::eq('OWNER_ID', $this->ownerId);

        foreach ($entitiesNames as $entityName) {
            $entitiesIDs = array();
            $total = 0;

            $entities = $this->pipeliner->{$entityName}->get($filter);
            $entitiesIterator = $this->pipeliner->{$entityName}->getEntireRangeIterator($entities);

            foreach ($entitiesIterator as $entity) {
                $entitiesIDs[] = $entity->getId();
                $total++;
                if ($total >= 100) {
                    $this->deleteGroup($entityName, $total, $entitiesIDs);
                    $total = 0;
                    $entitiesIDs = array();
                }
            }
        }
    }

    public function deleteGroup($entityName, $total, $entitiesIDs)
    {

        echo "\r\nDeleting {$total} {$entityName}... \r\n";
        try {
            if (!empty($entitiesIDs)) {
                $this->pipeliner->{$entityName}->deleteById($entitiesIDs);
            }
        } catch (PipelinerHttpException $e) {
            echo $e->getErrorCode().': '.$e->getErrorMessage();
        }

        $entitiesIDstr = implode("\r\n", $entitiesIDs);
        if (!empty($entitiesIDstr)) {
            echo "Deleted: \r\n".$entitiesIDstr;
        } else {
            echo $entityName." not found \r\n";
        }
    }
}

if (!isset($argv[1]) == 1) {
    exit("Please specify pipelinerID \r\n");
}

$crmExt = new CrmExt($argv[1]);

$crmExt->deletePipelinerAccountData();
