<?php

namespace SalesQuoter\Accounts;

use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\CartLayout\LayoutGroup;
use SalesQuoter\ListBuilder\Lists;

/**
 *  Accounts Class
 *
 */
class Accounts extends CustomAbstractCrud
{
    /**
     * Class constructor
     */
    public function __construct()
    {

        $fields = array("id", "name", "quote_builder_id", "quote_list_builder_id", "quote_cart_builder", "order_builder_id", "order_list_builder", "order_cart_builder");
        $config = array("table" => 'SQ_accounts', 'fields' => $fields);
        parent::__construct($config);
    }


    /**
     * updating an account
     *
     * @param id          $id
     * @param requestData $requestData
     *
     * @return array
     */
    public function updateAccount($id, $requestData)
    {

        $updateData = array();
        $updateData['id'] = $id;
        $updateData['name'] = $requestData['name'];

        $updateData['quote_builder_id'] = intval($requestData['quotes']['quote_builder_id']);
        $updateData['quote_list_builder_id'] = intval($requestData['quotes']['list_builder_id']);
        $updateData['quote_cart_builder'] = intval($requestData['quotes']['cart_builder_id']);

        $updateData['order_builder_id'] = intval($requestData['orders']['order_builder_id']);
        $updateData['order_list_builder'] = intval($requestData['orders']['list_builder_id']);
        $updateData['order_cart_builder'] = intval($requestData['orders']['cart_builder_id']);

        $this->update($updateData);

        return $this->accountDetails($id);
    }

   /**
     * Creating new accounts with provided data
     *
     * @return array
     */
    public function createNewAccount($requestData)
    {

        $insert = array();
        $insert['name'] = $requestData['name'];

        $insert['quote_builder_id'] = intval($requestData['quotes']['quote_builder_id']);
        $insert['quote_list_builder_id'] = intval($requestData['quotes']['list_builder_id']);
        $insert['quote_cart_builder'] = intval($requestData['quotes']['cart_builder_id']);

        $insert['order_builder_id'] = intval($requestData['orders']['order_builder_id']);
        $insert['order_list_builder'] = intval($requestData['orders']['list_builder_id']);
        $insert['order_cart_builder'] = intval($requestData['orders']['cart_builder_id']);

        $insert['active'] = 1;

        $insertStatement = $this->pdo->insert(array_keys($insert))
            ->into($this->table)
            ->values(array_values($insert));

        $insertId = $insertStatement->execute(true);

        return $this->accountDetails($insertId);
    }

   /**
     * Getting a Premade system Groups
     *
     * @return array
     */
    public function allAccounts()
    {

        $sql = $this->pdo->select(["id", "name"])->from($this->table);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }

   /**
     * Getting a Accounts Builder
     *
     * @return array
     */
    public function renderAccountBuilder()
    {

        $response = array();
        $quotesData = array();
        $orderData = array();

        $quotesData['lists_builders'] = $this->getListBuilder("quote");
        $quotesData['cart_builders'] = $this->renderLayoutBuilder("cart");
        $quotesData['quote_builders'] = $this->renderLayoutBuilder("quote");


        $orderData['lists_builders'] = $this->getListBuilder("order");
        $orderData['cart_builders'] = $this->renderLayoutBuilder("cart");
        $orderData['order_builders'] = $this->renderLayoutBuilder("order");


        return array ( "quotes" => $quotesData , "orders" => $orderData );
    }


    /**
     * Getting a Accounts Details
     *
     * @param accountId $accountId
     *
     * @return array
     */
    public function accountDetails($accountId)
    {

        $sql = $this->pdo->select(['*'])->from($this->table);
        $sql->where($this->table.'.active', '=', 1);
        $sql->where($this->table.'.id', '=', $accountId);

        $stmt = $sql->execute();

        $data =  $stmt->fetchAll();

        if (sizeof($data) > '0') {
            $response = array();

            $response[0]['name'] = $data[0]['name'];
            $response[0]['id'] = $data[0]['id'];
            $response[0]['quotes'] = array(
                 "quote_builder_id" => $data[0]['quote_builder_id'],
                 "list_builder_id" => $data[0]['quote_list_builder_id'],
                 "cart_builder_id" => $data[0]['quote_cart_builder'],
            );
            $response[0]['orders'] = array(
                 "order_builder_id" => $data[0]['order_builder_id'],
                 "list_builder_id" => $data[0]['order_list_builder'],
                 "cart_builder_id" => $data[0]['order_cart_builder'],
            );

            return $response;
        } else {
            return array();
        }
    }

    /**
     * Getting Builder from from SQ_layout
     *
     * @param type $type
     *
     * @return array
     */
    public function renderLayoutBuilder($type)
    {

        $sql = $this->pdo->select(["id", "name"])->from("SQ_layout");
        $sql->where('SQ_layout.active', '=', 1);
        $sql->where('SQ_layout.kind', '=', $type);

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }


    /**
     * Getting Builder from from SQ_lists
     *
     * @param type $type
     *
     * @return array
     */
    public function getListBuilder($type)
    {

        $sql = $this->pdo->select(["id", "name"])->from("SQ_lists");
        $sql->where('SQ_lists.active', '=', 1);
        $sql->where('SQ_lists.type', '=', $type);

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }


    /**
     * gettin an user accounts Details
     *
     * @param accountId $accountId
     *
     * @return array
     */
    public function userAccountDetails($accountId)
    {

        $layoutGroup = new LayoutGroup();
        $lists = new Lists();

        $sql = $this->pdo->select(['*'])->from($this->table);
        $sql->where($this->table.'.active', '=', 1);
        $sql->where($this->table.'.id', '=', $accountId);

        $stmt = $sql->execute();

        $data =  $stmt->fetchAll();

        if (sizeof($data) > '0') {
            $response = array();

            $response['quotes'] = array(
                 "quote_builder_id" => $layoutGroup->allLayoutGroup($data[0]['quote_builder_id']),
                 "list_builder_id" => $lists->getListsData($data[0]['quote_list_builder_id']),
                 "cart_builder_id" => $layoutGroup->allLayoutGroup($data[0]['quote_cart_builder']),
            );
            $response['orders'] = array(
                 "order_builder_id" => $layoutGroup->allLayoutGroup($data[0]['order_builder_id']),
                 "list_builder_id" => $lists->getListsData($data[0]['order_list_builder']),
                 "cart_builder_id" => $layoutGroup->allLayoutGroup($data[0]['order_cart_builder']),
            );
        } else {
             $response['quotes'] = array(
                 "quote_builder_id" => array(),
                 "list_builder_id" => array(),
                 "cart_builder_id" => array(),
            );
            $response['orders'] = array(
                 "order_builder_id" => array(),
                 "list_builder_id" => array(),
                 "cart_builder_id" => array(),
            );
        }

        return $response;
    }
}
