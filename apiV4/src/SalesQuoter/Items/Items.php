<?php

namespace SalesQuoter\Items;


use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\CartsPrice\CartsPrice;
use SalesQuoter\PriceChange\PriceChangeItem;

/**
 *  Items Class
 */
class Items extends CustomAbstractCrud
{

    /**
     * Returns Construct function
     */
    public function __construct()
    {

        $fields = array('id', 'created', 'prefix', 'state', 'sales_person_id', 'updated', 'sub_total', 'total', 'customer_id', 'addresses_id');
        $config = array("table" => 'SQ_items', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * Returns All Items
     *
     * @param salesPersonId $salesPersonId 
     * @param state         $state         can be order or quote
     *
     * @return Array
     */
    public function getAllItems( $salesPersonId, $state )
    {

        $allowedFields = array(
            "concat (`SQ_items`.`prefix`, `SQ_items`.`id`) as item_number", 
            "SQ_items.id", 
            "SQ_items.sub_total",
            "SQ_items.total",
            "SQ_items.created",
            "SQ_addresses.shipping_address1",
            "SQ_addresses.shipping_address2",
            "SQ_addresses.shipping_city",
            "SQ_addresses.shipping_state",
            "SQ_addresses.shipping_zip",
            "SQ_addresses.shipping_phone",
            "SQ_addresses.shipping_contact",
            "SQ_addresses.billing_address1",
            "SQ_addresses.billing_address2",
            "SQ_addresses.billing_city",
            "SQ_addresses.billing_state",
            "SQ_addresses.billing_zip",
            "SQ_addresses.billing_phone",
            "SQ_addresses.billing_contact",
            "SQ_addresses.shipping_email",
            "SQ_addresses.billing_email",
            "customer.firstName",
            "customer.lastName",
            "customer.phone",
            "customer.companyName",
            "customer.email",
        );

        $sql = $this->pdo->select($allowedFields)->from("SQ_items");
        $sql->join('customer', 'customer.id', '=', 'SQ_items.customer_id');
        $sql->join('SQ_addresses', 'SQ_addresses.id', '=', 'SQ_items.addresses_id');
        $sql->where('SQ_items.sales_person_id', '=', $salesPersonId);
        $sql->where('SQ_items.active', '=', 1);
        $sql->where('SQ_items.state', '=', $state);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Returns Items Details Info
     *
     * @param itemId $itemId SQ_items table id
     *
     * @return Array
     */
    public function getSingleItem( $itemId ) 
    {

        $allowedFields = array( 
            "SQ_items.*"
        );

        $sql = $this->pdo->select($allowedFields)->from("SQ_items");
        $sql->where('SQ_items.active', '=', 1);
        $sql->where('SQ_items.id', '=', $itemId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > '0' ) {

            $items = $data[0];

            $cartsPrice = new CartsPrice();
            $priceChange = new PriceChangeItem();

            $items['customer'] = $this->getTableDetails("customer", $items['customer_id']);
            $items['address'] = $this->getTableDetails("SQ_addresses", $items['addresses_id']);
            $items['price_changes'] = $priceChange->getWhere(array( 'item_id' => $items['id'] ));
            $items['cart'] = $cartsPrice->allCarts($items['id']);

            return $items;

        }else {
            return array();
        }
    }

    /**
     * Returns Table Details
     *
     * @param tableName $tableName
     * @param pId       $pId
     *
     * @return Array
     */
    public function getTableDetails( $tableName, $pId) 
    {

        $sql = $this->pdo->select(['*'])->from($tableName);
        $sql->where('id', '=', $pId);

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }
}
