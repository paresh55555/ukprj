<?php

namespace SalesQuoter\Contracts;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class Contracts
 *
 */

class Contracts extends CustomAbstractCrud
{
    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        $fields = array("id", "quote_id", "contracts");
        $config = array("table" => 'SQ_signed_contracts', 'fields' => $fields);
        parent::__construct($config);
    }
}
