<?php

namespace SalesQuoter\Components;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Components Class
 *
 */
class Components extends CustomAbstractCrud
{
    /**
     * Class constructor
     */
    public function __construct()
    {

        $fields = array("id", "system_id", "name", "menu_id", "type", "grouped_under", "my_order", "required", "multiple");
        $config = array("table" => ' SQ_components', 'fields' => $fields);
        parent::__construct($config);
    }


   /**
     * Getting a Premade system Groups
     *
     * @return array
     */
    public function getPremadeSystemGroups()
    {
 
        $sql = $this->pdo->select(['*'])->from('SQ_components');
        $sql->where('SQ_components.active', '=', 1);
        $sql->where('SQ_components.type', '=', 'systemGroup');
        $sql->where('SQ_components.name', '=', 'PreMadeDoorSystem');
        $sql->orWhere('SQ_components.name', '=', 'PreMadeWindowSystem');

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }


    /**
     * Getting a Premade system Groups of Sample Doors
     *
     * @return array
     */
    public function getPremadeSystemGroupOfDoors()
    {

        $sql = $this->pdo->select(['*'])->from('SQ_components');
        $sql->where('SQ_components.active', '=', 1);
        $sql->where('SQ_components.type', '=', 'systemGroup');
        $sql->where('SQ_components.name', '=', 'SampleDoors');

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }


    /**
     * Getting a system Groups Excepts Premades
     *
     * @return array
     */
    public function getWithoutPremadeSystemGroups()
    {
 
        $sql = $this->pdo->select(['*'])->from('SQ_components');
        $sql->where('SQ_components.active', '=', 1);
        $sql->where('SQ_components.type', '=', 'systemGroup');
        $sql->where('SQ_components.name', '!=', 'PreMadeDoorSystem');
        $sql->Where('SQ_components.name', '!=', 'PreMadeWindowSystem');
        $sql->Where('SQ_components.name', '!=', 'SampleDoors');

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }
}
