<?php

namespace SalesQuoter\Components;

use SalesQuoter\CustomAbstractCrud;
/**
 *  ComponentTypes Class
 *
 */
class ComponentTypes extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "name");
        $config = array("table" => 'SQ_component_types', 'fields' =>$fields);
        parent::__construct($config);
    }
}
