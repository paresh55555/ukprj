<?php

namespace SalesQuoter\SalesTerritory;

use SalesQuoter\MysqlConnection;

class SalesTerritory
{

    protected $pdo;
    protected $fields = array("SQ_sales_terriority.id", "name", "salesPersonId", "zipLow", "zipHigh", "zip");
    protected $table = 'SQ_sales_terriority';

    public function __construct()
    {
        $this->pdo = $this->mysqlConnection();
    }


    public function update($config)
    {
        $pId = $config['id'];
        unset($config['id']);

        if (isset($config['name'])) {
            unset($config['name']);
        }

        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $updateStatement->execute();

        $data = $this->get($pId);

        return $data[0];
    }

    public function get($pId)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->join('users', $this->table.'.salesPersonId', '=', 'users.id')->where('SQ_sales_terriority.id', '=', $pId);

        if (!empty($where['search']) and !empty($where['value'])) {
            $sql->whereLike($where['search'], strtolower($where['value']).'%');
        }

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function create($config)
    {

        if (isset($config['name'])) {
            unset($config['name']);
        }

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));

        $insertId = $insertStatement->execute(true);
        $data = $this->get($insertId);

        return $data[0];
    }


    public function deleteTeritory($pId)
    {
       
        $deleteStatement = $this->pdo->delete()
            ->from($this->table)
            ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "failed" ,"message"=> "ID not exists or already Deleted");
        }

        return $result;
    }



    public function getAllTeritory($where = null)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->join('users', $this->table.'.salesPersonId', '=', 'users.id');

        if (!empty($where['search']) and !empty($where['value'])) {
            $sql->whereLike($where['search'], strtolower($where['value']).'%');
        }

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function zipOverLapValidation($zipLow, $zipHigh)
    {

        $sql =  $this->pdo->select(['id'])->from($this->table)->where('zipHigh', '>=', $zipLow)->where('zipLow', '<=', $zipHigh);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             return array('status' => 'error', 'message' => 'Over Laping Zip Low or Zip High ');
        } else {
            return false;
        }
    }

    public function zipOverLapValidationUpdate($zipLow, $zipHigh, $pId)
    {

        $sql =  $this->pdo->select(['id','zipHigh','zipLow'])->from($this->table)->where('zipHigh', '>=', $zipLow)->where('zipLow', '<=', $zipHigh)->where('id', '!=', $pId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             return array('status' => 'error', 'message' => 'Over Laping Zip Low or Zip High ');
        }


        return false;
    }


    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
