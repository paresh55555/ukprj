<?php

namespace SalesQuoter\Reports;

use SalesQuoter\MysqlConnection;

/**
 *  Class Customer
 *
 */

class ReportOrders
{

    protected $pdo;
    protected $fields = array("email", "firstName", "lastName");
    protected $oldFields = array("delivered", "firstName", "lastName", "email", "phone");
    protected $table = 'customer';
    
    /**
     * Class constructor
     *
     */
    public function __construct()
    {
        $this->pdo = $this->mysqlConnection();
    }


    /**
 * function deliveredMinToMax
 *
 * @param minDate $minDate Minimum date
 * @param maxDate $maxDate Maximum Date
 *
 * @return array
 */
    public function deliveredMinToMax($minDate, $maxDate)
    {

        if (empty($minDate) && empty($maxDate)) {
            $minDate = date('Y-m-d', strtotime("-37 days"));
            $maxDate = date('Y-m-d', strtotime("-30 days"));
        }
        $maxDate = $maxDate.' 00:00:00';
        $minDate = $minDate.' 00:00:00';

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->join('quotesSales', $this->table.'.id', '=', 'quotesSales.Customer');
        $sql->where('quotesSales.type', '=', 'Delivered');
        $sql->where('quotesSales.status', '!=', 'deleted');
        $sql->where('quotesSales.delivered', '<=', $maxDate);
        $sql->where('quotesSales.delivered', '>=', $minDate);
        $sql->orderBy('quotesSales.delivered', 'DESC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    /**
     * function completedMinToMax
     *
     * @param minDate $minDate Minimum date
     * @param maxDate $maxDate Maximum Date
     *
     * @return array
     */
    public function completedMinToMax($minDate, $maxDate)
    {
        array_push ($this->fields, "prefix", "quotesSales.id","total", "Cart");

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->join('quotesSales', $this->table.'.id', '=', 'quotesSales.Customer');
        $sql->where('quotesSales.type', '=', 'Completed');
        $sql->where('quotesSales.status', '!=', 'deleted');
        $sql->where('quotesSales.completedDate', '<=', $maxDate);
        $sql->where('quotesSales.completedDate', '>=', $minDate);
        $sql->orderBy('quotesSales.completedDate', 'DESC');

        $stmt = $sql->execute();

        $data = [];

        while ($row = $stmt->fetch()) {
            $cart = json_decode($row['Cart']);
            $row['panels'] = $this->decodeCartTotalPanels($cart);
            $row['doors'] = $this->decodeCartTotalDoors($cart);
            unset($row['Cart']);
            unset($row['email']);
            $data[] = $row;
        }

        return $data;
    }


    /**
     * function getOpenOrders
     *
     * @param endDate $endDate End date
     *
     * @return array
     */
    public function getOpenOrders($endDate)
    {

        $allowedFields = array(
            "`quotesSales`.`orderName` as jobNumber",
            "DATE_FORMAT(dueDate,'%m/%d/%Y') as dueDate",
            "Cart"
        );

        $sql = $this->pdo->select($allowedFields)->from("quotesSales");
        $sql->where('quotesSales.type', '=', 'In Production');
        $sql->where('quotesSales.status', '!=', 'deleted');
        $sql->where('quotesSales.active', '=', '1');
        $sql->orderBy('quotesSales.dueDate', 'ASC');


        if ( strlen($endDate) > '0' ) {
            $sql->where('quotesSales.dueDate', '<=', $endDate);
        }

        $stmt = $sql->execute();

        $data = [];

        while ($row = $stmt->fetch()) {
            $cart = json_decode($row['Cart'], true);

            if ( sizeof($cart) > 0 ) {

                $row['extColor'] = $cart[0]["extColor"]["name1"];
                $row['intColor'] = $cart[0]["intColor"]["name1"];
            }

            $calculateAlumVinyl = $this->calculateAlumVinyl( $cart );
            $calculateFlushUpstep = $this->calculateFlushUpstep( $cart );
            
            $row['alum'] = $calculateAlumVinyl["alumCount"];
            $row['vinyl'] = $calculateAlumVinyl["vinylCount"];

            $row['flush'] =  $calculateFlushUpstep["flushCount"];
            $row['upstep'] =  $calculateFlushUpstep["upstepCount"];

            unset( $row["Cart"] );

            $data[] = $row;
        }

        return $data;
    }


    public function calculateFlushUpstep ( $cart ) {

        $flushCount = 0;
        $upstepCount = 0;

        foreach ($cart as $item) {
            
            $selectedItem = strtolower(  $item["panelOptions"]["track"]["selected"] );

            if ( strrpos($selectedItem, "upstep") !== false ) {

                $upstepCount = $upstepCount + 1;
            }

            if (  strrpos($selectedItem, "flush") !== false) {

                $flushCount = $flushCount + 1;
            }
        }

        return [ "upstepCount" => $upstepCount, "flushCount" => $flushCount ];
    }

    public function calculateAlumVinyl( $cart ) {

        $alumCount = 0;
        $vinylCount = 0;

        foreach ($cart as $item) {
            
            if ( $item["module"] == '24') {

                $alumCount = $alumCount + 1;
            }

            if ( $item["module"] == '14') {

                $vinylCount = $vinylCount + 1;
            }
        }

        return [ "alumCount" => $alumCount, "vinylCount" => $vinylCount ];
    }



    protected function decodeCartTotalDoors($cart)
    {
        $totalDoors = 0;
        foreach ($cart as $door) {
            $doors =  $door->quantity;
            $totalDoors = $totalDoors + $doors;
        }

        return $totalDoors;
    }

    protected function decodeCartTotalPanels($cart)
    {
        $totalPanels = 0;
        foreach ($cart as $door) {
            $doorsPanels = $door->panelOptions->panels->selected * $door->quantity;
            $totalPanels = $totalPanels + $doorsPanels;
        }

        return $totalPanels;
    }

    /**
     * function deliveredMinToMax
     *
     * @return DB instance
     */
    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }

        public function getPageAndLimitWithRequest($request)
    {
        $page = intval($request['page']);
        $limit = intval($request['limit']);


        if ($page < 1 or $limit < 1 or $limit > 100) {
            $pageLimit = array('page' => 1, 'limit' => 10);
        } else {
            $pageLimit = array('page' => $page, 'limit' => $limit);

        }


        if ($request['limit'] == 'All') {
            $pageLimit['limit'] = 90000000;
        }

        return $pageLimit;
    }

    public function buildAccountingConfig( $allGetVars  )
    {
        $search = $allGetVars ['search'];
        $order = $this->validQuoteColumn( $allGetVars ['order'] );
        $direction = $this->validDirection( $allGetVars ['direction'] );
        $pageLimit = $this->getPageAndLimitWithRequest( $allGetVars );

        $export = $allGetVars['export'];

        $config = array( "order" => $order, "direction" => $direction, "search" => $search, "pageLimit" => $pageLimit, "export" => $export);
        
        return $config;
    }

   public function validDirection($direction) {
        if ($direction == 'desc') {
            return 'DESC';
        } else {
            return 'ASC';
        }

    }

    public function validQuoteColumn($column)
    {

        $validColumns = array('id', 'lastName', 'firstName', 'companyName', 'phone',
            'quantity', 'total', 'followUp', 'secondFollowUp', 'leadQuality', 'status',
            'balanceDue', 'dueDate', 'estComplete', 'depositDue', 'billingZip');

        if (in_array($column, $validColumns)) {
            return $column;
        } else {
            return 'id';
        }
    }


    public function buildOffSetLimitWithPageLimit($page, $limit)
    {

        if ($limit == 'All') {
            return '';
        }

        if ($page < 1 or $limit < 1) {
            $page = 1;
            $limit = 10;
        }

        $offset = ($page - 1) * $limit;

        $pageLimit = "LIMIT $limit OFFSET $offset";

        return $pageLimit;
    }

    public function getPayments($quoteID)
    {

        $db = $this->pdo;

        $sql = " SELECT id, amount, `date`, lastFour, kind, verified FROM SQ_payments  WHERE  quoteID=? and status = '1' ";

        $stmt = $db->prepare($sql);
        $stmt->execute([$quoteID]);
        /* Fetch result to array */
        $results = $stmt->fetchAll();
        $data = array();
        foreach ($results as $payment) {

            $payment['date'] = date('m/d/Y', strtotime($payment['date']));
            $data[] = $payment;
        }

        return $data;
    }

    public function renderAccountingDetails ( $config )
    {

        $offsetLimit = '';

        if (is_array($config)) {

            $direction = $config['direction'];
            $order = $config['order'];
            $search = $config['search'];
            $type = $config['type'];
            $pageLimit = $config['pageLimit'];
            $offsetLimit = $this->buildOffSetLimitWithPageLimit($pageLimit['page'], $pageLimit['limit']);

        } else {
            $type = $config;
        }

        $db = $this->pdo;


        $select = 'SELECT orderName, delivered, paidInFull, version, quotesSales.id, companyName, SalesDiscount, prefix,
                postfix,  quotesSales.status, lastName, firstName, phone, email,  quantity, total, orderDate, dueDate,
                SQ_addresses.billing_city as city1,
                SQ_addresses.shipping_city as city2, 
                customer.billingCity as city3, 
                SQ_addresses.billing_state as state1,
                SQ_addresses.shipping_state as state2, 
                customer.billingState as state3, 
                `type`,  depositDue, balanceDue from quotesSales
                LEFT JOIN customer ON Customer=customer.id
                LEFT JOIN SQ_payments ON quotesSales.id=SQ_payments.quoteID
                LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id ';

        $whereCore = " where quotesSales.status !='deleted'  ";

        if ($type == 'paymentCompleted') {
            $wherePlus = " and `paidInFull`='yes'  ";

        } else if ($type == 'balanceDue') {
            $wherePlus = " and `paidInFull`!='yes' and (`type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

        } else if ($type == 'inProduction') {
            $wherePlus = " and (`type`='In Production') ";

        } else if ($type == 'needsSurvey') {
            $wherePlus = " and (`type`='Needs Survey') ";
        } else if ($type == 'verifyDeposit') {
            $wherePlus = " and ( `type`='Ready For Production') ";
        } else if ($type == 'Hold') {
            $wherePlus = " and quotesSales.status='Hold' and  ( `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";
        } else if ($type == 'All') {
            $wherePlus = " and  ( `type` = 'Needs Survey' or  `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

        } else {
            $wherePlus = " and `type`='$type' ";
        }

        $sqlSearch = " and ( (SQ_payments.kind like ? && SQ_payments.status = 1)  OR SQ_payments.amount like ? OR lastName like ? OR firstName like ? OR phone like ? OR `total` like ? OR companyName like ? OR quotesSales.orderName like ? OR SQ_addresses.billing_email like ? OR  SQ_addresses.shipping_email like ? OR  customer.email like ? OR quotesSales.orderName like ? ) ";


        $sql = $select.$whereCore.$wherePlus.$sqlSearch." GROUP BY (quotesSales.id) ORDER BY quotesSales.id DESC ";
        $stmt = $db->prepare($sql);

        if (empty($search)) {
            $sqlSearch = "%%";
        } else {
            $sqlSearch = "%$search%";
        }
        $stmt->execute( [$sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch]);

        $results = $stmt->fetchAll();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        foreach ($results as $options) {

            $options['number'] = $options['id'];
            $options['SalesDiscount'] = json_decode($options['SalesDiscount']);

            $payments = $this->getPayments($options['id']);
            $options['SalesDiscount']->Payments = $payments;

            $orderDate = strtotime($options['orderDate']);
            $dueDate = strtotime($options['dueDate']);

            $options['orderDate'] = date('m/d/Y', $orderDate);
            $options['dueDate'] = date('m/d/Y', $dueDate);

            $delivered = strtotime($options['delivered']);
            $options['delivered'] = date('m/d/Y', $delivered);

            $data[] = $options;
        }

        $results = array("total" => $count, "data" => $data);

        if (!empty($config['userType'])) {

            if ($config['userType'] == 'audit' && ! $this->isMagnaline() ) {

                $config['totalFromPD'] = $count;
                $config['returnedFromPD'] = count($data);

                $config['db'] = 'ces';
                $resultsFromCES = $this->renderProductionDouble($config);
                $results['total'] = $results['total'] + $resultsFromCES['total'];
                $results['data'] = array_merge($results['data'], $resultsFromCES['data']);

            }
        }

        return $results;
    }

    public function isMagnaline()
    {
        if ($_SERVER['SITE_NAME'] == 'magnalineSystems'  )  {
            return true;
        }

        return false;
    }

    public function specialOffSetLimitWithPageLimit($config )
    {
        $pageLimit = $config['pageLimit'];
        $page = $pageLimit['page'];
        $limit = $pageLimit['limit'];

        $needFromCES = $limit - $config['returnedFromPD'];

        if ($limit == 'All') {
            return '';
        }

        if ($needFromCES == 0) {
            $pageLimit = "LIMIT 0";
        }
        else if ($needFromCES < $limit ) {
            $pageLimit = "LIMIT $needFromCES OFFSET 0";
        }
        else {
            $pdPage = ceil ( $config['totalFromPD']/ $limit) ;
            $cesPage = $page - $pdPage;
            $bonus = $pdPage * $limit - $config['totalFromPD'];
            $offset = ($cesPage - 1) * $limit + $bonus;
            $pageLimit = "LIMIT $limit OFFSET $offset";
        }

        return $pageLimit;
    }

    public function renderProductionDouble($config)
    {

        $offsetLimit = '';
        if (is_array($config)) {
            $direction = $config['direction'];
            $order = $config['order'];
            $search = $config['search'];
            $type = $config['type'];

            $offsetLimit = $this->specialOffSetLimitWithPageLimit($config);
        } else {
            $type = $config;
        }

        $db = $this->mysqlConnection->connect_db_pdo_special( $config['db'] );


        $select = 'SELECT "ces", delivered, paidInFull, version, quotesSales.id, companyName, SalesDiscount, prefix,
                postfix,  quotesSales.status, lastName, firstName, phone, email,  quantity, total, orderDate, dueDate,
                SQ_addresses.billing_city as city1,
                SQ_addresses.shipping_city as city2, 
                customer.billingCity as city3, 
                SQ_addresses.billing_state as state1,
                SQ_addresses.shipping_state as state2, 
                customer.billingState as state3, 
                `type`,  depositDue, balanceDue from quotesSales
                LEFT JOIN customer ON Customer=customer.id
                LEFT JOIN SQ_payments ON quotesSales.id=SQ_payments.quoteID
                LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id ';

        $whereCore = " where quotesSales.status !='deleted'  ";

        if ($type == 'paymentCompleted') {
            $wherePlus = " and `paidInFull`='yes'  ";

        } else if ($type == 'balanceDue') {
            $wherePlus = " and `paidInFull`!='yes' and (`type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

        } else if ($type == 'inProduction') {
            $wherePlus = " and (`type`='In Production') ";

        } else if ($type == 'needsSurvey') {
            $wherePlus = " and (`type`='Needs Survey') ";
        } else if ($type == 'verifyDeposit') {
            $wherePlus = " and ( `type`='Ready For Production') ";
        } else if ($type == 'Hold') {
            $wherePlus = " and quotesSales.status='Hold' and  ( `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";
        } else if ($type == 'All') {
            $wherePlus = " and  ( `type` = 'Needs Survey' or  `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

        } else {
            $wherePlus = " and `type`='$type' ";
        }

        $sqlSearch = " and ( (SQ_payments.kind like ? && SQ_payments.status = 1)  OR SQ_payments.amount like ? OR lastName like ? OR firstName like ? OR phone like ? OR `total` like ? OR companyName like ? OR quotesSales.id like ? OR SQ_addresses.billing_email like ? OR  SQ_addresses.shipping_email like ? OR  customer.email like ? OR concat_ws('',quotesSales.prefix,quotesSales.id) like ? ) ";


        $sql = $select.$whereCore.$wherePlus.$sqlSearch." GROUP BY (quotesSales.id) ORDER BY quotesSales.id DESC ";
        $stmt = $db->prepare($sql);

        if (empty($search)) {
            $sqlSearch = "%%";
        } else {
            $sqlSearch = "%$search%";
        }
        $stmt->bindParam("ssssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);

        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;

        $sql = $sql.$offsetLimit;

        $stmt = $db->prepare($sql);
        $stmt->bindParam("ssssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        $stmt->execute();

        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];
            $options['SalesDiscount'] = json_decode($options['SalesDiscount']);

            $payments = $this->getPaymentsSpecial($options['id'],$config['db']);
            $options['SalesDiscount']->Payments = $payments;

            $orderDate = strtotime($options['orderDate']);
            $dueDate = strtotime($options['dueDate']);
            $options['orderDate'] = date('m/d/Y', $orderDate);
            $options['dueDate'] = date('m/d/Y', $dueDate);

            $delivered = strtotime($options['delivered']);
            $options['delivered'] = date('m/d/Y', $delivered);
            $data[] = $options;
        }


        $results = array("total" => $count, "data" => $data);

        return $results;
    }

    public function getPaymentsSpecial($quoteID,$site)
    {

        $db = $this->mysqlConnection->connect_db_pdo_special( $site );

        $sql = " SELECT id, amount, `date`, lastFour, kind, verified FROM SQ_payments  WHERE  quoteID=? and status = '1' ";

        $stmt = $db->prepare($sql);

        $stmt->bindParam("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $data = array();
        while ($payment = $results->fetchAll()) {

            $payment['date'] = date('m/d/Y', strtotime($payment['date']));
            $data[] = $payment;
        }

        return $data;
    }
}
