<?php

namespace SalesQuoter\Reports;

use SalesQuoter\MysqlConnection;

/**
 * Class ReportLeads
 *
 * @package SalesQuoter\Reports
 */
class ReportLeads
{

    protected $pdo;
    protected $fields = array("quoted", "email", "zip");
    protected $leadsFields = array("users.name as SalesPerson", "CONCAT(quotesSales.prefix, quotesSales.id) as QuoteNumber", "guest.name", "guest.email", "guest.zip", "guest.leadType", "guest.phone", "quotesSales.quoted");
    protected $table = 'quotesGuest';

    /**
     * ReportLeads constructor.
     */
    public function __construct()
    {
        $this->pdo = $this->mysqlConnection();
    }


    /**
     * Search Zipcode by Date
     *
     * @param string $minDate
     * @param string $maxDate
     * @return mixed
     */
    public function zipcodeByDate($minDate = null, $maxDate = null)
    {
        if (empty($minDate)) {
            $minDate = '0000-00-00 00:00:00';
        } else {
            $minDate = $minDate.' 00:00:00';
        }

        

        if (empty($maxDate)) {
            $maxDate = '9999-01-01 00:00:00';
        } else {
             $maxDate = $maxDate.' 23:59:59';
        }

       

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->join('guest', 'guest.id', '=', 'quotesGuest.guest');

        $sql->where('quotesGuest.status', '=', 'transferred');
        $sql->where('quoted', '>=', $minDate);
        $sql->where('quoted', '<=', $maxDate);
        $sql->groupBy('email');
        $sql->orderBy('quotesGuest.quoted', 'ASC');

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();


        $quoteSalesFields = array("quoted", "email", "billingZip");
        $sql = $this->pdo->select($quoteSalesFields)->from('quotesSales');
        $sql->join('customer', 'customer.id', '=', 'quotesSales.Customer');

        $sql->where('transferred', '>', '2016-07-20 20:57:24');
        $sql->where('quoted', '>=', $minDate);
        $sql->where('quoted', '<=', $maxDate);
        $sql->groupBy('email');
        $sql->orderBy('quotesSales.quoted', 'ASC');

        $stmt = $sql->execute();


        $data2 = $stmt->fetchAll();

        foreach ($data2 as $row) {
            if (!in_array($row['email'], $data)) {
                array_push($data, array("quoted" => $row['quoted'], "email" => $row['email'], "zip" => $row['billingZip'] ));
            }
        }

        return $data;
    }

    /**
     * @param null $minDate
     * @param null $maxDate
     * @param null $unique
     * @return mixed
     */
    public function leadsByDate($config)
    {

        $minDate = $config['min'];
        $maxDate = $config['max'];

        if (empty($minDate)) {
            $minDate = '0000-00-00 00:00:00';
        } else {
            $minDate = $minDate.' 00:00:00';
        }

        if (empty($maxDate)) {
            $maxDate = '9999-01-01 00:00:00';
        } else {
            $maxDate = $maxDate.' 23:59:59';
        }

        $sql = $this->pdo->select($this->leadsFields)->from('guest');
        $sql->join('quotesSales', 'guest.id', '=', 'quotesSales.guest');
        $sql->leftJoin('users', 'users.id', '=', 'quotesSales.SalesPerson');
        $sql->where('quotesSales.quoted', '>=', $minDate);
        $sql->where('quotesSales.quoted', '<=', $maxDate);
        if (!empty($config['salesPerson']))  {
            $sql->where('quotesSales.SalesPerson', '=', $config['salesPerson']);
        }
        if ($config['unique'] == 'yes') {
            $sql->groupBy('guest.email');
        }
        $sql->orderBy('SalesPerson', 'ASC');
        $sql->orderBy('quotesSales.quoted', 'ASC');

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }


    /**
     * Create a PDO mysql connection
     *
     * @return \SalesQuoter\db
     */
    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
