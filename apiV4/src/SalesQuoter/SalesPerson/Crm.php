<?php

namespace SalesQuoter\SalesPerson;

use PipelinerSales\ApiClient\PipelinerClient;
use PipelinerSales\ApiClient\Criteria;
use PipelinerSales\ApiClient\Query\Filter;

class Crm
{
    const DAYS_AHEAD = 30;

    protected $pipeliner;
    protected $ownerId;
    protected $validContact = array("email", "firstName");
    protected $validLead = array("name");
    protected $stage = 'PY-7FFFFFFF-B54F022F-B35C-4BAC-A473-5061FDA264D6';


    public function __construct($ownerId)
    {
        $this->ownerId = $ownerId;
        $this->pipeliner = $this->buildPipeliner();
    }

    public function updateOpportunity($optConfig)
    {
        $results = $this->findOpportunityByName($optConfig['prefix'].$optConfig['quoteId']);

        if (empty($results[0])) {
            return array("status" => "success");
        }
        
        $opportunityId = $results[0]->getId();

        $data = array('ID' => $opportunityId);

        if (!empty($optConfig['amount'])) {
            $data['OPPORTUNITY_VALUE'] = $optConfig['amount'];
            $data['OPPORTUNITY_VALUE_FOREGIN'] = $optConfig['amount'];
            $data['SQ_Price'] = $optConfig['amount'];
        }

        if (!empty($optConfig['quality'])) {
            $data['Quality2'] = $optConfig['quality'];
            if ($optConfig['quality'] == 'Archived') {
                $data['IS_ARCHIVE'] = 1;
                $data['REASON_OF_CLOSE_ID'] = "DV-VALUE_3";
            } else {
                $data['IS_ARCHIVE'] = 0;
                $data['REASON_OF_CLOSE_ID'] = "";
            }
        }

        if (!empty($optConfig['phone'])) {
            $data['Phone_Number2'] = $optConfig['phone'];
        }

        $this->pipeliner->opportunities->save($data);

//        $opportunity = $this->pipeliner->opportunities->getById($opportunityId);
//        print_r($opportunity);
//        exit();

        return array("status" => "success");
    }

    public function markAsLostOpportunity($optConfig)
    {
        $results = $this->findOpportunityByName($optConfig['prefix'].$optConfig['quoteId']);

        if (empty($results[0])) {
            return array(
                "message" => "Opportunity for quote {$optConfig['prefix']}{$optConfig['quoteId']} not found"
            );
        }

        $opportunityId = $results[0]->getId();

        $data = array(
            'ID' => $opportunityId,
            'IS_ARCHIVE' => 1,
            'REASON_OF_CLOSE_ID' => "DV-VALUE_3"
        );

        $this->pipeliner->opportunities->save($data);

        return array("message" => "Opportunity marked as 'Lost'");
    }

    public function saveDocument($quoteInfo, $fileInfo)
    {
        $results = $this->findOpportunityByName($quoteInfo['prefix'].$quoteInfo['quoteId']);

        if (empty($results[0])) {
            return array(
                "message" => "Opportunity for quote {$quoteInfo['prefix']}{$quoteInfo['quoteId']} not found"
            );
        }

        $opportunityId = $results[0]->getId();

        try {
            $contents = file_get_contents($fileInfo['fileNamePath']);
        } catch (Exception $e) {
            $contents ='';
        }

        try {
            $data = array(
                'FILENAME' => $fileInfo['fileName'],
                'TYPE' => '12',
                'CONTENT' =>  base64_encode($contents),
                'OPID' => $opportunityId
            );
            $result = $this->pipeliner->documents->save($data);
            return $result;
        } catch (PipelinerHttpException $e) {
            return $e->getErrorMessage();
        }
    }

    public function createNewSalesQuoterLead($lead)
    {
        $contact = $this->findContact($lead['email']);

        if (empty($contact)) {
            $contactData = array(
                'firstName' => $lead['firstName'],
                'email' => $lead['email'],
                'lastName' => $lead['lastName'],
                'phone' => $lead['phone']
            );

            $contactId = $this->createContact($contactData);
        } else {
            $contactId = $contact->getId();

            $filter = Filter::eq('OWNER_ID', $this->ownerId)->eq('IS_ARCHIVE', 0)->eq('IS_DELETED', 0)->eq('account_id', $contactId);
            $results = $this->pipeliner->opportunities->get($filter);

            if (count($results) > 0) {
                $this->stage = 'PY-7FFFFFFF-D9F667F5-A538-4084-ACA0-DCA9A31EBD58';
            }
        }

        $opportunity = array(
            "contactId" => $contactId,
            "prefix" => $lead['prefix'],
            "quoteId" => $lead['quoteId'],
            "value" => $lead['amount'],
            "quality" => $lead['quality'],
            "phone" => $lead['phone']
        );

        $opportunityId = $this->createOpportunity($opportunity);


//        $quoteNumber = substr($lead['quoteNumber'],2);
//
//        try {
//            $contents = file_get_contents('/tmp/SQ'.$quoteNumber.'.pdf');
//        } catch (Exception $e) {
//            $contents ='';
//        }

//        try {
//            $data = array(
//                'FILENAME' => $lead['quoteNumber'].'.pdf',
//                'TYPE' => '12',
//                'CONTENT' =>  base64_encode($contents),
//                'OPID' => $opportunityId
//            );
//            $this->pipeliner->documents->save($data);
//        }catch (PipelinerHttpException $e) {
//
//        }

        return $opportunityId;
    }
    
    
    public function importQuote($quoteInfo = null)
    {
        $opportunity = $this->findOpportunityByName($quoteInfo['prefix'].$quoteInfo['quoteId']);

        if (empty($opportunity[0])) {
            $opportunityID = $this->createNewSalesQuoterLead($quoteInfo);
            return array("message" => "Opportunity {$opportunityID} created for quote ".$quoteInfo['prefix'].$quoteInfo['quoteId']);
        } else {
            return array("message" => "Quote {$quoteInfo['prefix']}{$quoteInfo['quoteId']} already exists in pipeliner.");
        }
    }

    protected function buildPipeliner()
    {
        $url = 'https://us.pipelinersales.com';
        $pipelineId = 'nv1_PanoramicDoors1';
        $token = 'nv1_PanoramicDoors1_75UQ3SOQ71REXUIQ';
        $password = 'z7XwjGtja9rjOXLD';

        $pipeliner = PipelinerClient::create($url, $pipelineId, $token, $password);

        return $pipeliner;
    }


    /**
     * @param $email
     * @return \PipelinerSales\ApiClient\EntityCollection
     */
    protected function findContact($email)
    {
        $filter = Filter::eq('OWNER_ID', $this->ownerId)->eq('EMAIL1', $email);
        $result = $this->pipeliner->contacts->get($filter);

        if (count($result) == 0) {
            return null;
        }

        return $result[0];
    }


    /**
     * @param $email
     * @return \PipelinerSales\ApiClient\EntityCollection
     */
    protected function findAllContacts($email)
    {
        $filter = Filter::eq('OWNER_ID', $this->ownerId)->eq('EMAIL1', $email);
        $result = $this->pipeliner->contacts->get($filter);

        if (count($result) == 0) {
            return array();
        }

        return $result;
    }




    /**
     * @param $contact
     */
    protected function validContact($contact)
    {
        foreach ($this->validContact as $field) {
            if (empty($contact[$field])) {
                throw new \RuntimeException('Valid Contact of: '.$field.' was not passed');
            }
        }
    }


    /**
     * @param $lead
     */
    protected function validLead($lead)
    {
        foreach ($this->validLead as $field) {
            if (empty($lead[$field])) {
                throw new \RuntimeException('Valid Lead field of: '.$field.' was not passed');
            }
        }
    }

    protected function padIfEmpty($value)
    {
        if (empty($value)) {
            $value = 'JohnDoe';
        }

        return $value;
    }


    protected function ifFakePhone($value)
    {
        if (empty($value)) {
            $value = '555-555-5555';
        }

        return $value;
    }


    /**
     * @param $contact
     * @return \PipelinerSales\ApiClient\Http\CreatedResponse|\PipelinerSales\ApiClient\Http\Response
     */
    protected function createContact($contact)
    {
//        $this->validContact($contact);

        if (filter_var($contact['email'], FILTER_VALIDATE_EMAIL) === false) {
            $contact['email'] = "badEmail_".$this->ownerId."@salesquoter.com";
        }

        $data = array(
            'FIRST_NAME' => $this->padIfEmpty($contact['firstName']),
            'EMAIL1' => $contact['email'],
            'OWNER_ID' => $this->ownerId,
            'SALES_UNIT_ID' => '0',
            'SURNAME' => $this->padIfEmpty($contact['lastName']),
            'PHONE1' => $this->ifFakePhone($contact['phone'])
        );


        try {
            $result = $this->pipeliner->contacts->save($data);
            $pId = $result->getCreatedId();
        } catch (PipelinerHttpException $e) {
            echo $e->getErrorMessage();
        }

//        try {
//            print_r("here");
//            $result = $this->pipeliner->contacts->save($data);
//            print_r("and here");
//        } catch(\Exception $e) {
//            // something went wrong
//            print_r($e);
//            $data['EMAIL1'] = "badEmail_".$this->ownerId."@salesquoter.com";
//
//            $data['EMAIL1'] = 'fracka@fracka.com';
////            $result = $this->pipeliner->contacts->save($data);
//        }



        return $pId;
    }


    protected function findLead($name)
    {
        $filter = Filter::eq('OWNER_ID', $this->ownerId)->eq('OPPORTUNITY_NAME', $name);
        $result = $this->pipeliner->leads->get($filter);

        return $result;
    }


    protected function findOpportunityByName($name)
    {
        $filter = Filter::eq('OWNER_ID', $this->ownerId)->eq('OPPORTUNITY_NAME', $name);
        $result = $this->pipeliner->opportunities->get($filter);

        return $result;
    }



    protected function findOpportunityById($pId)
    {
        $filter = Filter::eq('ID', $pId);
        $result = $this->pipeliner->opportunities->get($filter);

        return $result;
    }


    protected function deleteOpportunityById($pId)
    {
        $result = $this->pipeliner->opportunities->deleteById($pId);

        return $result;
    }


    protected function deleteContactById($pId)
    {
        $result = $this->pipeliner->contacts->deleteById($pId);

        return $result;
    }

    protected function createOpportunity($opportunity)
    {

        $futureDate = date('Y-m-d', strtotime("+".self::DAYS_AHEAD." days"));
        $contactRelations =  array ("CONTACT_ID" => $opportunity['contactId'], "IS_PRIMARY" => 1 );

//        'OPPORTUNITY_VALUE_FOREGIN' => $opportunity['value'],

        $data = array(
            'CONTACT_RELATIONS' => array ($contactRelations),
            'account_id' => $opportunity['contactId'],
            'OPPORTUNITY_NAME' => $opportunity['prefix'].$opportunity['quoteId'],
            'STAGE' => $this->stage,
            'OPPORTUNITY_VALUE' => $opportunity['value'],
            'OWNER_ID' => $this->ownerId,
            'SALES_UNIT_ID' => '0',
            'CLOSING_DATE' => $futureDate,
            'OPPORTUNITY_VALUE_FOREGIN' => $opportunity['value'],
            'Phone_Number2' => $opportunity['phone'],
            'Quality2' => $opportunity['quality'],
            'Quote_Link' => 'https://'.$_SERVER['SERVER_NAME'].'/?tab=viewQuote&quote='.$opportunity['quoteId'],
            'SQ_Price' => $opportunity['value']
        );



        $result = $this->pipeliner->opportunities->save($data);

        $pId = $result->getCreatedId();

        return $pId;
    }






    /**
     * @param $lead
     * @return \PipelinerSales\ApiClient\Http\CreatedResponse|\PipelinerSales\ApiClient\Http\Response
     */
    protected function createLead($lead)
    {
        $this->validLead($lead);

        $data = array(
            'OPPORTUNITY_NAME' => $lead['name'],
            'SALES_UNIT_ID' => '0',
            'OWNER_ID' => $this->ownerId,
        );

        $result = $this->pipeliner->leads->save($data);

        return $result;
    }


    //        $data = array(

//        );
//        $result = $pipeliner->leads->save($data);

    /**
     * @group crm
     */
    public function getAccount()
    {

//        $filter = new Filter();

//        $types = $pipeliner->getEntityTypes();

//        $criteria = new Criteria();


//        print_r($types);
//        $info = $pipeliner->accounts->get(array('EMAIL1' => 'a.alayev@gmail.com'));
//
//        $info = $pipeliner->accounts->get(Filter::contains('EMAIL1', ''));
//        $info = $pipeliner->accounts->get();
//
//
//        foreach ($info as $record) {
////            echo $record->getAccountTypeId()." - ".$record->getEmail1(). "\n";
//            print_r($record);
//        }
//        print_r($info);


//        $result = $pipeliner->accounts->getById('AR-00009f45-220D617F-8FBD-74EE-A4B7-73893D9A1720');
//        $result->setEmail5('');
//        $pipeliner->accounts->save($result);

//        $result = $pipeliner->contacts->get();


//        $data = array(
//            'FIRST_NAME' => 'Fracka',
//            'EMAIL1' => 'fracka@fracka.com',
//            'OWNER_ID' => '40721',
//            'SALES_UNIT_ID' => '0',
//            'SURNAME' => 'Future'
//        );
//        $result = $pipeliner->contacts->save($data);
//
//          print_r($result);
//

//        $data = array(
//            'OPPORTUNITY_NAME' => 'We Got One',
//            'OWNER_ID' => 40721,
//            'SALES_UNIT_ID' => '0'
//        );
//        $result = $pipeliner->leads->save($data);
//        Filter::contains('OWNER_ID', '40721')


        //$query = array('limit' => 1, 'filter' => Filter::gt('CREATED', '2013-04-14 11:22:00'));

//        print_r($query);
        $results = $this->pipeliner->opportunities->getById('PY-7FFFFFFF-A6652320-DB96-4C49-8916-96A7B72DEE6E');

//        $results = $this->pipeliner->opportunities->get();
        print_r($results);
//        $results['OPPOR$data TUNITY_VALUE'] = 8888.88;


//
//        $data = array(
//            'ID' => 'PY-7FFFFFFF-0FD26275-C19C-43B2-BA6C-83BE72E866B5',
//            'OPPORTUNITY_VALUE' => '5100'
//        );
//
//
//        $result = $this->pipeliner->opportunities->save($data);

//        $results->setOpportunityValueForeign( 88.88);
//        $result = $pipeliner->save($results);
//        print_r($results);
    }
}
