<?php

namespace SalesQuoter\PriceChange;

use SalesQuoter\CustomAbstractCrud;
/**
 *  Class Price Change Item
 *
 */
class PriceChangeItem extends CustomAbstractCrud
{
    /**
     * Constructor
     */
    public function __construct()
    {

        $fields = array('id', 'item_id', 'kind', 'amount', 'name', 'show_on_item');

        $config = array("table" => 'SQ_price_change_item', 'fields' =>$fields);
        parent::__construct($config);
    }
}
