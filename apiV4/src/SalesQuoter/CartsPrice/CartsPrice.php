<?php

namespace SalesQuoter\CartsPrice;

use SalesQuoter\CustomAbstractCrud;
/**
 *  Class Price Change Cart
 *
 */
class CartsPrice extends CustomAbstractCrud
{
    /**
     * Constructor
     */
    public function __construct()
    {

        $fields = array('id', 'item_id', 'component_values', 'system_id', 'quantity', 'sub_total', 'total');

        $config = array("table" => 'SQ_carts', 'fields' =>$fields);
        parent::__construct($config);
    }

    public function allCarts( $where = array() ) {

        $allCarts = $this->getWhere ( $where );

        for ( $count = 0; $count < sizeof($allCarts); $count++ ) {

              $allCarts[$count]['price_changes'] = $this->allChangeItems( $allCarts[$count]['id'] );
              $allCarts[$count]['component_values'] = json_decode($allCarts[$count]['component_values'], TRUE);
        }

        return $allCarts;
    }


    public function allChangeItems( $cartId ) {

        $sql = $this->pdo->select(['*'])->from("SQ_price_change_cart");
        $sql->where('SQ_price_change_cart.cart_id', '=', $cartId);
        $sql->where('SQ_price_change_cart.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

}
