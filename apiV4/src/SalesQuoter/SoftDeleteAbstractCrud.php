<?php

namespace SalesQuoter;

use SalesQuoter\MysqlConnection;

/**
 *  Class SoftDeleteAbstractCrud
 *
 */

class SoftDeleteAbstractCrud
{

    protected $pdo;
    protected $fields;
    protected $table;
   
    /**
     * Class constructor
     *
     * @param config $config Childs class tables and fields info
     *
     */
    public function __construct($config)
    {
        $this->table = $config['table'];
        $this->fields = $config['fields'];

        $this->pdo = $this->mysqlConnection();
    }

    /**
     * Update a record
     *
     * @param config $config Childs class tables and fields info
     *
     * @return array
     */
    public function update($config)
    {
        $pId = $config['id'];
        unset($config['id']);

        $config['active'] = 1;
        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $updateStatement->execute();

        $data = $this->get($pId);

        return $data;
    }

    /**
     * function create
     *
     * @param config $config all inserted fields info
     *
     * @return array
     */
    public function create($config)
    {

        $config['active'] = 1;
        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));

        $insertId = $insertStatement->execute(true);
        $data = $this->get($insertId);

        return $data;
    }

    /**
     * function get
     *
     * @param pId $pId Primary Key Id of table
     *
     * @return array
     */
    public function get($pId)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->where($this->table.'.id', '=', $pId);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $result = json_decode("{}");
        if (!empty($data)) {
            $result = $data[0];
        }

        return $result;
    }

    /**
     * function delete
     *
     * @param pId $pId Primary Key Id of table
     *
     * @return array
     */
    public function delete($pId)
    {

        $config = array();
        $config['active'] = 0;
        $deleteStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "failed", "message" => "ID not exists or already Deleted");
        }

        return $result;
    }


    /**
     * function getAll
     *
     * @param getAll $getAll Where conditions of SQL Queries
     *
     * @return array
     */
    public function getAll($where = null)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table)->where($this->table.'.active', '=', 1);
        ;

        if (!empty($where)) {
            $column = key($where);
            $sql->where($column, '=', $where[$column]);
        }
        
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }


    /**
     * function mysqlConnection
     *
     * @return db instances
     */
    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
