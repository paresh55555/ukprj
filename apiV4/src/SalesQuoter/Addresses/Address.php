<?php

namespace SalesQuoter\Addresses;

use SalesQuoter\AbstractCrud;
/**
 *  Address Class
 *
 */
class Address extends AbstractCrud
{

    public function __construct()
    {
        $fields = array('id', 'quote_id', 'sales_person_id', 'shipping_address1', 'shipping_address2', 'shipping_city', 'shipping_state',
            'shipping_zip', 'shipping_phone', 'shipping_contact', 'shipping_email', 'billing_address1', 'billing_address2', 'billing_city',
            'billing_state', 'billing_zip', 'billing_phone', 'billing_contact', 'billing_email');

        $config = array("table" => 'SQ_addresses', 'fields' => $fields);
        parent::__construct($config);
    }


    public function update($config)
    {

        $quoteID = $config['quote_id'];
        unset($config['id']);

        $isQuoteExists = $this->checkQuoteExists($quoteID);

        if ($isQuoteExists) {
            unset($config['quote_id']);

            $updateStatement = $this->pdo->update($config)->table('SQ_addresses')->where('quote_id', '=', $quoteID);

             $updateStatement->execute();
        }else{
            $this->create($config);
        }


        $allData = array('quote_id' => $quoteID);

        return $this->getAll($allData);
    }


    public function checkQuoteExists($quoteID)
    {
        
        $sql = $this->pdo->select(['id'])->from('SQ_addresses')->where('quote_id', '=', $quoteID);
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return true;
        }

        return false;
    }

    public function checkEmptyParameter($data)
    {
//
//        $hasEmpty = false;
//        $message = '';
//        $sendingAllFields = array_keys($data);
//
//
//        foreach ($this->fields as $key) {
//
//            if ($key != 'id' && (!in_array($key, $sendingAllFields) || strlen($data[$key]) == '0')) {
//
//                $hasEmpty = true;
//                $message = $key.' can not be Empty';
//            }
//
//        }

        if (empty($data)) {
            return array('isEmpty' => true, 'message' => 'sending Parameter Can not Be Empty');
        }

        return array('isEmpty' => false);
    }
}
