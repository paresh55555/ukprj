<?php

namespace SalesQuoter;

use SalesQuoter\MysqlConnection;

/**
 *  Class AbstractCrud
 *
 */

class AbstractCrud
{

    protected $pdo;
    protected $fields;
    protected $table;

    /**
     * Class constructor
     *
     * @param config $config Childs class tables and fields info
     *
     */
    public function __construct($config)
    {
        $this->table = $config['table'];
        $this->fields = $config['fields'];

        $this->pdo = $this->mysqlConnection();
    }

    /**
     * Update a record
     *
     * @param config $config Childs class tables and fields info
     *
     * @return array
     */
    public function update($config)
    {
        $pId = $config['id'];
        unset($config['id']);

        $config = validateRequest( $config, $this->fields );

        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $updateStatement->execute();

        $data = $this->get($pId);

        return $data;
    }

    /**
     * function create
     *
     * @param config $config Childs class tables and fields info
     *
     * @return array
     */
    public function create($config)
    {

        $config = validateRequest( $config, $this->fields );

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));

        $insertId = $insertStatement->execute(true);
        $data = $this->get($insertId);

        return $data;
    }

    /**
     * function get
     *
     * @param pId $pId Childs class tables and fields info
     *
     * @return array
     */
    public function get($pId)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->where($this->table.'.id', '=', $pId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $result = json_decode("{}");
        if (!empty($data)) {
            $result = $data[0];
        }

        return $result;
    }

    /**
     * function delete
     *
     * @param pId $pId Childs class tables and fields info
     *
     * @return array
     */
    public function delete($pId)
    {

        $deleteStatement = $this->pdo->delete()
            ->from($this->table)
            ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "failed", "message" => "ID not exists or already Deleted", );
        }

        return $result;
    }

    /**
     * function getAll
     *
     * @param where $where where conditions
     *
     * @return all data array
     */
    public function getAll($where = null)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);

        foreach ($where as $key => $value) {
                $sql->where($this->table.".".$key, '=', $value);
        }

        $sql->orderBy("id", "DESC");
        
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * function mysqlConnection
     *
     * @return db instance
     */
    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
