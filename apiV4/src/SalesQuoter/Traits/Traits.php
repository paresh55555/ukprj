<?php

namespace SalesQuoter\Traits;

use SalesQuoter\CustomAbstractCrud;

class Traits extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "component_id", "name", "type", "value", "visible");
        $config = array("table" => 'SQ_traits', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function checkSystemComponentsTraitsExits($systemId, $componentId, $traitId)
    {

        $sql = $this->pdo->select([$this->table.".id"])->from($this->table);
        $sql->leftJoin('SQ_components', $this->table.'.component_id', '=', 'SQ_components.id');

        $sql->where($this->table.'.id', '=', $traitId);
        $sql->where('SQ_components.id', '=', $componentId);
        $sql->where('SQ_components.system_id', '=', $systemId);

        $sql->where($this->table.'.active', '=', 1);
        $sql->where('SQ_components.active', '=', 1);



        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }


    public function checkSystemComponentsExits($systemId, $componentId)
    {

        $sql = $this->pdo->select(["SQ_components.id"])->from("SQ_components");

        $sql->where('SQ_components.id', '=', $componentId);
        $sql->where('SQ_components.system_id', '=', $systemId);

        $sql->where('SQ_components.active', '=', 1);


        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }
}
