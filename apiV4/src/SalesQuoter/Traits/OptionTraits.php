<?php

namespace SalesQuoter\Traits;

use SalesQuoter\CustomAbstractCrud;

class OptionTraits extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "system_id", "component_id", "option_id", "name", "value", "visible");
        $config = array("table" => 'SQ_options_traits', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function checkSystemComponentsTraitsExits($systemId, $componentId, $traitId)
    {

        $sql = $this->pdo->select([$this->table.".id"])->from($this->table);
        $sql->leftJoin('SQ_components', $this->table.'.component_id', '=', 'SQ_components.id');
        $sql->leftJoin('SQ_systems', 'SQ_components.system_id', '=', 'SQ_systems.id');

        $sql->where($this->table.'.id', '=', $traitId);
        $sql->where('SQ_components.id', '=', $componentId);
        $sql->where('SQ_systems.id', '=', $systemId);

        $sql->where($this->table.'.active', '=', 1);
        $sql->where('SQ_components.active', '=', 1);
        $sql->where('SQ_systems.active', '=', 1);



        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }


    public function checkSystemComponentsOptionsExits($systemId, $componentId, $optionId)
    {

        $sql = $this->pdo->select(["SQ_new_options.id"])->from("SQ_new_options");

        $sql->where('SQ_new_options.component_id', '=', $componentId);
        $sql->where('SQ_new_options.system_id', '=', $systemId);
        $sql->where('SQ_new_options.id', '=', $optionId);
        $sql->where('SQ_new_options.active', '=', 1);



        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return $data[0];
        }

         return array();
    }
}
