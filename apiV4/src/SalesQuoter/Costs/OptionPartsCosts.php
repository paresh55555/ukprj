<?php

namespace SalesQuoter\Costs;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class OptionPartsCosts
 *
 */
class OptionPartsCosts extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "system_id", "component_id", "option_id", "part_id", "name", "amount", "cost_type", "taxable", "markup", "swings");
        $config = array("table" => 'SQ_costs', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * function checkSystemComponentsOptionsPartsExits
     *
     * @param systemId    $systemId    system id
     * @param componentId $componentId components id
     * @param optionId    $optionId    options id
     *
     * @return array
     */
    public function checkSystemComponentsOptionsExits($systemId, $componentId, $optionId)
    {

        $sql = $this->pdo->select(["SQ_new_options.id"])->from("SQ_new_options");

        $sql->where('SQ_new_options.component_id', '=', $componentId);
        $sql->where('SQ_new_options.system_id', '=', $systemId);
        $sql->where('SQ_new_options.id', '=', $optionId);
        $sql->where('SQ_new_options.active', '=', 1);



        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             return $data[0];
        }

        return array();
    }

    /**
     * function getAllCosts
     *
     * @param partId $partId parts id
     *
     * @return array
     */
    public function getAllCosts($partId)
    {

        $sql = $this->pdo->select(["SQ_costs.id,SQ_costs.name,SQ_costs.cost_type,SQ_costs.taxable,SQ_costs.markup,SQ_costs.amount"])->from("SQ_costs");

        $sql->where('SQ_costs.part_id', '=', $partId);
        $sql->where('SQ_costs.active', '=', 1);


        $stmt = $sql->execute();
        
        return $stmt->fetchAll();
    }
}
