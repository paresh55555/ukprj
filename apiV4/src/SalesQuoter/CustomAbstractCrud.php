<?php

namespace SalesQuoter;

use SalesQuoter\MysqlConnection;

/**
 *
 *  @SuppressWarnings(PHPMD)
 *
 *  Class CustomAbstractCrud
 *
 */
class CustomAbstractCrud
{

    protected $pdo;
    protected $fields;
    protected $table;

    /**
     * Class constructor
     *
     * @param config $config Childs class tables and fields info
     *
     */
    public function __construct($config)
    {
        $this->table = $config['table'];
        $this->fields = $config['fields'];
        $this->pdo = $this->mysqlConnection();
    }

    /**
     * function getAll
     *
     * @return array
     */
    public function getAll()
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * function get
     *
     * @param pId $pId Child class primary id
     *
     * @return array
     */
    public function get($pId)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->where($this->table.'.id', '=', $pId);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * function getWhere
     *
     * @param data $data Where conditions array
     *
     * @return array
     */
    public function getWhere($data)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);

        foreach ($data as $key => $value) {
                  $sql->where($this->table.".".$key, '=', $value);
        }

        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    /**
     * function get
     *
     * @param pId $pId Child class primary id
     *
     * @return array
     */
    public function delete($pId)
    {

        $config = array();
        $config['active'] = 0;
        $deleteStatement = $this->pdo->update($config)
          ->table($this->table)
          ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "failed", "message" => "ID not exists or already Deleted");
        }

        return $result;
    }


    /**
     * function update
     *
     * @param config $config Updated Data array
     *
     * @return array
     */
    public function update($config)
    {
        $pId = $config['id'];
        unset($config['id']);

        $config = validateRequest( $config, $this->fields );

        $config['active']  = 1;
         
        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $updateStatement->execute();

        $data = $this->get($pId);

        return $data;
    }
    

    /**
     * function create
     *
     * @param config $config newly creating Data array
     *
     * @return array
     */
    public function create($config)
    {

        if (isset($config['id'])) {
            unset($config['id']);
        }

        $config = validateRequest( $config, $this->fields );
        $config['active']  = 1;

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));


        $insertId = $insertStatement->execute(true);
        $data = $this->get($insertId);

        return $data;
    }


    /**
     * function mysqlConnection
     *
     * @return db instance
     */
    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
