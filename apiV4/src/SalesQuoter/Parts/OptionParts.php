<?php

namespace SalesQuoter\Parts;

use SalesQuoter\CustomAbstractCrud;

class OptionParts extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "system_id", "component_id", "option_id", "name", "description", "number", "quantity_formula", "size_formula", "on_cut_sheet");
        $config = array("table" => 'SQ_parts', 'fields' =>$fields);
        parent::__construct($config);
    }

    public function checkSystemComponentsOptionsExits($systemId, $componentId, $optionId)
    {

        $sql = $this->pdo->select(["SQ_new_options.id"])->from("SQ_new_options");

        $sql->where('SQ_new_options.component_id', '=', $componentId);
        $sql->where('SQ_new_options.system_id', '=', $systemId);
        $sql->where('SQ_new_options.id', '=', $optionId);
        $sql->where('SQ_new_options.active', '=', 1);



        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return $data[0];
        }

        return array();
    }
}
