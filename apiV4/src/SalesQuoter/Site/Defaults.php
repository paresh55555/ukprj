<?php
/* SalesQuoter added */
namespace SalesQuoter\Site;

use SalesQuoter\AbstractCrud;

class Defaults extends AbstractCrud
{

    public function __construct()
    {

       
        $fields =  array("SQ_site_defaults.id", "siteName", "SalesEmail", "hasSalesAdmin",
                              "superHasSalesAdmin", "showCustomers","showCommunication","showLeadStatus" ,
                              "showFullOrderStatus","siteURL", "location", "link1text", "link1", "link2text",
                              "link2", "companyName",  "address1", "address2", "city", "state","zip",
                              "cssMainColor","cssBackgroundColor","logoURL", "phone","numberofProductsPerLine",
                              "cssFont", "cssReverseFont", "cssBorder", "cssReverseBackground",
                              "cssBackground", "termsURL", "allowGuest"," printLogoURL" ,"salesFlow",
                              "allowSignUps", "guestProductsPerLine","takesCreditCards","productionFlow");

    
                               

        $config = array ('table' => 'SQ_site_defaults', 'fields' => $fields) ;
        parent::__construct($config);
    }
}
