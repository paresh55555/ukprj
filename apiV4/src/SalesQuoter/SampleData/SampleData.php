<?php

namespace SalesQuoter\SampleData;

use SalesQuoter\AbstractCrud;

/**
 *  SampleData Class
 *
 */
class SampleData extends AbstractCrud
{
    /**
     * Class constructor
     */
    public function __construct()
    {

        $fields = array("id", "type", "json");
        $config = array("table" => 'SQ_new_sample_data', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * function checkTypeExists
     *
     * @param type $type
     *
     * @return boolean
     */
    public function checkTypeExists($type)
    {

        $sql = $this->pdo->select(["id"])->from($this->table);
        $sql->where($this->table.'.type', '=', $type);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > '0') {
            return $data[0]['id'];
        } else {
            return false;
        }
    }
}
