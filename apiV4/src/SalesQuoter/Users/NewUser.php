<?php

namespace SalesQuoter\Users;

use SalesQuoter\MysqlConnection;
use SalesQuoter\Permissions\Permissions;
use SalesQuoter\Quotes\Quote;

error_reporting(0);


class User
{

    protected $pdo;
    protected $fields = array("users.id", "name", "email", "password", "type", "discount", "companyName", "prefix", "salesAdmin", "anytimeQuoteEdits" , "permissions_group_id");
    protected $table = 'users';

    public function __construct()
    {
        $this->pdo = $this->mysqlConnection();
    }


    public function updateDealer($config)
    {
        array_push($this->fields, 'dealer');
        $user = $this->getUser($config['id']);

        $dealerId = $user->dealer;

        array_pop($this->fields);
        unset($config['id']);

        $updateStatement = $this->pdo->update(array("companyName" => $config['companyName']))
            ->table('SQ_dealers')
            ->where('id', '=', $dealerId);

        $updateStatement->execute();
    }

    public function updateCompanyName($config)
    {

        array_push($this->fields, 'dealer');
        $user = $this->getUser($config['id']);

        $prevDealerId = $user->dealer;

        array_pop($this->fields);
        unset($config['id']);

        $sql = $this->pdo->select(["id"])->from("SQ_dealers")->where('companyName', '=', $config['companyName']);
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             return $data[0]['id'];
        } else {
            return $prevDealerId;
        }
    }




    public function updateUser($config)
    {
        if (!empty($config['companyName'])) {
            $dealerId =  $this->updateCompanyName($config);
            $config['dealer'] = $dealerId;
            unset($config['companyName']);
        };

        $pId = $config['id'];
        unset($config['id']);

        $config['defaults'] = $this->setDefaults($config);
        $config['active']  = 1;
         
        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $updateStatement->execute();

        $data = $this->getUser($pId);

        return $data;
    }

    public function setDefaults($config)
    {

        $default = '{"Discounts":[{"amount":'.$config['discount'].',"id":0,"name":"Company Discount","showOnQuote":true,"type":"percentage"},{"name":"","amount":0,"type":"percent","id":1,"showOnQuote":false}],"Totals":{"code":"YZ-08","tax":{"amount":0,"showOnQuote":true},"shipping":{"amount":0,"showOnQuote":false},"installation":{"amount":0,"showOnQuote":false},"cost":{"amount":10000,"showOnQuote":false},"preCost":10000,"finalTotal":{"amount":0,"showOnQuote":true},"subTotal":{"amount":0,"showOnQuote":true},"extra":{"name":"","amount":0,"showOnQuote":false},"screens":{"name":"","amount":0,"showOnQuote":false},"discountTotal":{"amount":0,"showOnQuote":false}}}';

        if ($config['salesAdmin'] == 'on') {
            $config['discount'] = 0;
            $default = '{"Discounts":[{"amount":"0","id":0,"name":"&nbsp;","showOnQuote":true,"type":"percentage"},{"name":"","amount":0,"type":"percent","id":1,"showOnQuote":false}],"Totals":{"code":"YZ-08","tax":{"amount":0,"showOnQuote":true},"shipping":{"amount":0,"showOnQuote":false},"installation":{"amount":0,"showOnQuote":false},"cost":{"amount":10000,"showOnQuote":false},"preCost":10000,"finalTotal":{"amount":0,"showOnQuote":true},"subTotal":{"amount":0,"showOnQuote":true},"extra":{"name":"","amount":0,"showOnQuote":false},"screens":{"name":"","amount":0,"showOnQuote":false},"discountTotal":{"amount":0,"showOnQuote":false}}}';
        }

        return $default;
    }


    public function createUser($config)
    {

        if (empty($config['companyName'])) {
            $config['companyName'] = '';
        }
        $insertStatement = $this->pdo->insert(array('companyName'))
            ->into('SQ_dealers')
            ->values(array($config['companyName']));

        $insertId = $insertStatement->execute(true);

        unset($config['companyName']);
        $config['dealer'] = $insertId;

        $config['defaults'] = $this->setDefaults($config);
        $config['active']  = 1;

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));


        $insertId = $insertStatement->execute(true);
        $data = $this->getUser($insertId);

        return $data;
    }

    public function getUser($pId)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->leftJoin('SQ_dealers', $this->table.'.dealer', '=', 'SQ_dealers.id');
        $sql->where($this->table.'.id', '=', $pId);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $result = json_decode("{}");
        if (!empty($data)) {
            $result = $data[0];
        }
        return $result;
    }


    public function deleteUser($pId)
    {

        $config = array();
        $config['active'] = 0;
        $deleteStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "failed", "message" => "ID not exists or already Deleted");
        } else {
            // SQ-309
            $quote = new Quote();
            $quote->clearUserTransferredQuotes($pId);
        }

        return $result;
    }



    public function getUsers($where = null)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->leftJoin('SQ_dealers', $this->table.'.dealer', '=', 'SQ_dealers.id');
        $sql->where($this->table.'.active', '=', 1);

        if (!empty($where['search']) and !empty($where['value'])) {
            $sql->whereLike($where['search'], strtolower($where['value']).'%');
        }

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function isEmailAvailable($email)
    {

        $sql = $this->pdo->select(["*"])->from($this->table)->where('email', '=', $email)->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $result = array("availableEmail" => false) ;
        if (empty($data)) {
            $result['availableEmail'] = true;
        }
        return $result;
    }



    public function checkUserAuth($email, $password)
    {
        /*User auth added*/

        $sql = $this->pdo->select(array('id','permissions_group_id'))->from($this->table)->where("email", "=", $email)->where("password", "=", $password)->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             /*After authentication check User Permissions*/
             
             $permissionsGroupId = $data[0]['permissions_group_id'];

             $permissions = new Permissions();

             $checkPermissionsRows = $permissions->get($permissionsGroupId);

            if (sizeof($checkPermissionsRows) > 0) {
                $permissionGroup = json_decode($checkPermissionsRows[0]['permissions'], true);

                $returnPermissions = array();

                foreach ($permissionGroup as $key => $value) {
                         $returnPermissions[] = $key;
                         $returnPermissions[] = $value;
                }

                return $returnPermissions;
            } else {
                return array();
            }
        } else {
            return false;
        }
    }



    public function getUsersFromDealersId($dealerId)
    {

        $showFields  = array('id','name');

        $sql = $this->pdo->select($showFields)->from($this->table)->where("dealer", "=", $dealerId)->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }




        protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
