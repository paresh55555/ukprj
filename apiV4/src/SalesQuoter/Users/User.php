<?php

namespace SalesQuoter\Users;

use SalesQuoter\MysqlConnection;
use SalesQuoter\Permissions\Permissions;
use SalesQuoter\Quotes\Quote;

/**
 *  User Class
 *
 */
class User
{

    protected $pdo;
    protected $fields = array("users.id", "name", "email", "password", "type", "discount", "companyName", "prefix", "salesAdmin", "anytimeQuoteEdits" , "permissions_group_id", "phone", "messageSubject", "message", "sendMessage","superSales");
    protected $table = 'users';

    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->pdo = $this->mysqlConnection();
    }


    /**
     * Updating Dealer Name
     *
     * @param config $config
     *
     * @return array
     */
    public function updateDealer($config)
    {
        array_push($this->fields, 'dealer');
        $user = $this->getUser($config['id']);

        $dealerId = $user->dealer;

        array_pop($this->fields);
        unset($config['id']);

        $updateStatement = $this->pdo->update(array("companyName" => $config['companyName']))
            ->table('SQ_dealers')
            ->where('id', '=', $dealerId);

        $updateStatement->execute();
    }

    /**
     * Updating Company Name
     *
     * @param config $config
     *
     * @return array
     */
    public function updateCompanyName($config)
    {

        array_push($this->fields, 'dealer');
        $user = $this->getUser($config['id']);

        $prevDealerId = $user->dealer;

        array_pop($this->fields);
        unset($config['id']);

        $sql = $this->pdo->select(["id"])->from("SQ_dealers")->where('companyName', '=', $config['companyName']);
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return $data[0]['id'];
        } else {
            return $prevDealerId;
        }
    }


    /**
     * Updating User Info
     *
     * @param config $config
     *
     * @return array
     */
    public function updateUser($config)
    {
        if (!empty($config['companyName'])) {
            $dealerId =  $this->updateCompanyName($config);
            $config['dealer'] = $dealerId;
        };

        unset($config['companyName']);

        $pId = $config['id'];
        unset($config['id']);

        $config['defaults'] = $this->setDefaults($config);
        $config['active']  = 1;

        if ( strlen($config['password']) > '0' ) { 
            
            $hashPassword = $this->getPassword( $config['password'] );
            $config['pw_id'] = $this->insertData ( ["hash" => $hashPassword] ,"pws" );
        }
       
        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $updateStatement->execute();

        $data = $this->getUser($pId);

        return $data;
    }

    /**
     * Setting a Defaults
     *
     * @param config $config
     *
     * @return array
     */
    public function setDefaults($config)
    {

        $default = '{"Discounts":[{"amount":'.$config['discount'].',"id":0,"name":"Company Discount","showOnQuote":true,"type":"percentage"},{"name":"","amount":0,"type":"percent","id":1,"showOnQuote":false}],"Totals":{"code":"YZ-0","tax":{"amount":0,"showOnQuote":true},"shipping":{"amount":0,"showOnQuote":false},"installation":{"amount":0,"showOnQuote":false},"cost":{"amount":10000,"showOnQuote":false},"preCost":10000,"finalTotal":{"amount":0,"showOnQuote":true},"subTotal":{"amount":0,"showOnQuote":true},"extra":{"name":"","amount":0,"showOnQuote":false},"screens":{"name":"","amount":0,"showOnQuote":false},"discountTotal":{"amount":0,"showOnQuote":false}}}';

        if ($config['salesAdmin'] == 'on') {
            $config['discount'] = 0;
            $default = '{"Discounts":[{"amount":"0","id":0,"name":"&nbsp;","showOnQuote":true,"type":"percentage"},{"name":"","amount":0,"type":"percent","id":1,"showOnQuote":false}],"Totals":{"code":"YZ-0","tax":{"amount":0,"showOnQuote":true},"shipping":{"amount":0,"showOnQuote":false},"installation":{"amount":0,"showOnQuote":false},"cost":{"amount":10000,"showOnQuote":false},"preCost":10000,"finalTotal":{"amount":0,"showOnQuote":true},"subTotal":{"amount":0,"showOnQuote":true},"extra":{"name":"","amount":0,"showOnQuote":false},"screens":{"name":"","amount":0,"showOnQuote":false},"discountTotal":{"amount":0,"showOnQuote":false}}}';
        }

        return $default;
    }


    /**
     * Creating an User
     *
     * @param config $config
     *
     * @return array
     */
    public function createUser($config)
    {

        if (empty($config['companyName'])) {
            $config['companyName'] = '';
        }
        $insertStatement = $this->pdo->insert(array('companyName'))
            ->into('SQ_dealers')
            ->values(array($config['companyName']));

        $insertId = $insertStatement->execute(true);

        unset($config['companyName']);
        $config['dealer'] = $insertId;

        $config['defaults'] = $this->setDefaults($config);
        $config['active']  = 1;

        $hashPassword = $this->getPassword( $config['password'] );

        $config['pw_id'] = $this->insertData ( ["hash" => $hashPassword] ,"pws" );

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($this->table)
            ->values(array_values($config));


        $insertId = $insertStatement->execute(true);
        $data = $this->getUser($insertId);

        return $data;
    }

    /**
     * Deleting a User via id
     *
     * @param pId $pId
     *
     * @return array
     */
    public function getUser($pId)
    {

        $allowFields = [
            "users.*",
            "SQ_dealers.id",
            "SQ_dealers.companyName",
            "SQ_dealers.firstName",
            "SQ_dealers.lastName",
            "SQ_dealers.address1",
            "SQ_dealers.address2",
            "SQ_dealers.city",
            "SQ_dealers.state",
            "SQ_dealers.zip",
            "SQ_dealers.phone as dealer_phone",
            "SQ_dealers.logoQuote",
            "SQ_dealers.logoSite",
            "SQ_dealers.link1",
            "SQ_dealers.link1text",
            "SQ_dealers.link2",
            "SQ_dealers.link2text",
            "SQ_dealers.backgroundColor"
        ];

        $sql = $this->pdo->select($allowFields)->from($this->table);
        $sql->leftJoin('SQ_dealers', $this->table.'.dealer', '=', 'SQ_dealers.id');
        $sql->where($this->table.'.id', '=', $pId);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $result = json_decode("{}");
        if (!empty($data)) {
            $result = $data[0];
        }

        return $result;
    }

    /**
     * Deleting a User via id
     *
     * @param pId $pId
     *
     * @return array
     */
    public function deleteUser($pId)
    {

        $config = array();
        $config['active'] = 0;
        $deleteStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "failed", "message" => "ID not exists or already Deleted");
        } else {
            // SQ-309
            $quote = new Quote();
            $quote->clearUserTransferredQuotes($pId);
        }

        return $result;
    }


    /**
     * Getting a Lists Of Users
     *
     * @param where $where
     *
     * @return array
     */
    public function getUsers($where = null)
    {

        $allowFields = [
            "users.*",
            "SQ_dealers.companyName",
            "SQ_dealers.firstName",
            "SQ_dealers.lastName",
            "SQ_dealers.address1",
            "SQ_dealers.address2",
            "SQ_dealers.city",
            "SQ_dealers.state",
            "SQ_dealers.zip",
            "SQ_dealers.phone as dealer_phone",
            "SQ_dealers.logoQuote",
            "SQ_dealers.logoSite",
            "SQ_dealers.link1",
            "SQ_dealers.link1text",
            "SQ_dealers.link2",
            "SQ_dealers.link2text",
            "SQ_dealers.backgroundColor"
        ];

        $sql = $this->pdo->select($allowFields)->from($this->table);
        $sql->leftJoin('SQ_dealers', $this->table.'.dealer', '=', 'SQ_dealers.id');
        $sql->where($this->table.'.active', '=', 1);

        if (!empty($where['type'])) {
            $sql->where('type', '=', $where['type']);
        }
        $sql->orderBy('name');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Checking is Email is Available or Not
     *
     * @param email $email
     *
     * @return array
     */
    public function isEmailAvailable($email)
    {

        $sql = $this->pdo->select(["*"])->from($this->table)->where('email', '=', $email)->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $result = array("availableEmail" => false) ;
        if (empty($data)) {
            $result['availableEmail'] = true;
        }

        return $result;
    }

    /**
     * Checking User Auth Via Email & Password
     *
     * @param email    $email
     * @param password $password
     *
     * @return array
     */
    public function checkUserAuth($email, $password)
    {

        $sql = $this->pdo->select(array('id','permissions_group_id', 'name'))
                ->from($this->table)
                ->where("email", "=", $email)
                ->where("password", "=", $password)
                ->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             /*After authentication check User Permissions*/
             
            $userId = $data[0]['id'];
            $accountId = $data[0]['account_id'];
            $userName = $data[0]['name'];
            $permissionsGroupId = $data[0]['permissions_group_id'];

            $permissions = new Permissions();

            $checkPermissionsRows = $permissions->get($permissionsGroupId);

            if (sizeof($checkPermissionsRows) > 0) {
                $permissionGroup = json_decode($checkPermissionsRows[0]['permissions'], true);

                $returnPermissions = array();

                foreach ($permissionGroup as $key => $value) {
                         $returnPermissions[] = $key;
                         $returnPermissions[] = $value;
                }

                return array(
                    "returnPermissions" => $returnPermissions,
                    "userId" => $userId,
                    "userName" => $userName,
                    "permissionJson" => $permissionGroup,
                    "account_id" => $accountId,
                );
            } else {
                return array();
            }
        } else {
            /*User auth added*/
            $allowedFields = array(
                "users.id",
                "pws.hash",
                "users.account_id",
                "users.permissions_group_id",
                "users.name"
            );

            $sql = $this->pdo->select($allowedFields)->from($this->table);
            $sql->join('pws', 'pws.id', '=', $this->table.'.pw_id');
            $sql->where($this->table.'.email', '=', $email );
            $sql->where($this->table.'.active', '=', 1);

            $stmt = $sql->execute();

            $data = $stmt->fetchAll();

            if (sizeof($data) > 0) {
                 /*After authentication check User Permissions*/

                $chkCorrectPass = $this->checkCorrectPassword( $password, $data[0]['hash'] );
                if ( $chkCorrectPass ) {

                    $permissionsGroupId = $data[0]['permissions_group_id'];
                    $userId = $data[0]['id'];
                    $accountId = $data[0]['account_id'];
                    $userName = $data[0]['name'];

                    $permissions = new Permissions();

                    $checkPermissionsRows = $permissions->get($permissionsGroupId);

                    if (sizeof($checkPermissionsRows) > 0) {
                        $permissionGroup = json_decode($checkPermissionsRows[0]['permissions'], true);

                        $returnPermissions = array();

                        foreach ($permissionGroup as $key => $value) {
                                 $returnPermissions[] = $key;
                                 $returnPermissions[] = $value;
                        }

                        return array(
                            "returnPermissions" => $returnPermissions,
                            "userId" => $userId,
                            "userName" => $userName,
                            "permissionJson" => $permissionGroup,
                            "account_id" => $accountId,
                        );
                    }

                    return array(
                        "returnPermissions" => array(),
                        "userId" => $userId,
                        "userName" => $userName,
                        "permissionJson" => "{}",
                        "account_id" => $accountId,
                    );
                }

            }
        }
        
        return false;
    }


    public function checkCorrectPassword($password, $hash ) {

        return password_verify ($password, $hash);
    }


    /**
     * Getting a lists of Users from dealers Id
     *
     * @param dealerId $dealerId
     *
     * @return array
     */
    public function getUsersFromDealersId($dealerId)
    {

        $showFields  = array('id', 'name');

        $sql = $this->pdo->select($showFields)->from($this->table)->where("dealer", "=", $dealerId)->where('active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function getDetails($selectField, $data, $tableName)
    {

        $sql = $this->pdo->select($selectField)->from($tableName);

        foreach ($data as $key => $value) {
            $sql->where($tableName.".".$key, '=', $value);
        }

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }


    public function sendResetEmail($config){

        $where = array(
          "email" => $config['email'],
        ); 

        $selectField = ["pw_id"];

        $details = $this->getDetails( $selectField, $where, $this->table );

        $insertData['unique_code'] = generateUniqueCode();
        $insertData['pw_id'] = $details[0]['pw_id'];
        $insertData['expires'] = time()+ (60*20);

        $pId = $this->insertData( $insertData, "reset");
       
        $subject = "Reset Password";

        $link = "http://".$_SERVER['HTTP_HOST']."/SQ-admin/reset?unique_code=".$insertData['unique_code'];

        $message = getEmailTemplate( $link );

        $sendEmail = sendResetEmail('', $config['email'], $subject, $message );

        if ($sendEmail) {
           $msg = 'Reset password link sent to your e-mail';
        }else{
           $msg = 'Something went Wrong. Mail could not sent';
        }

        return $msg;

    }

    public function getPassword ( $password ) {

        return password_hash($password, PASSWORD_DEFAULT);
    }

    public function resetPassword( $config ) {
 
       $checkValid = $this->checkCodeValid( $config['unique_code'] );
       if ( sizeof($checkValid) > '0' ) {

           $hashPassword = $this->getPassword ( $config['password'] );

           $update = array(
             "hash" => $hashPassword  
           );

           $this->updatePw( $checkValid[0]['pw_id'], $update, "pws" );

           $responseData = array( "status" => "success", "msg" => "password successfully reset" );
       }else {

           $responseData = array( "status" => "error", "msg" => "code is not valid" );
       }

       return $responseData;

    }

    public function updatePw( $pId, $config, $table ) {

        $updateStatement = $this->pdo->update($config)
            ->table($table)
            ->where('id', '=', $pId);

        $updateStatement->execute();
    }

    public function insertData( $config, $tableName) {

        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($tableName)
            ->values(array_values($config));


        $insertId = $insertStatement->execute(true);

        return $insertId;
    }


    public function checkCodeValid ( $code ) {

        $sql = $this->pdo->select(["pw_id"])->from("reset");
        $sql->where("expires", '>', time());
        $sql->where("unique_code", '=', $code);

        $stmt = $sql->execute();

        return $stmt->fetchAll();

    }

    public function getUserType($userId)
    {
        $sql = $this->pdo->select(["type"])->from($this->table);
        $sql->where($this->table.'.id', '=', $userId);
        $sql->where($this->table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (!empty($data)) {
            $result = $data[0]['type'];
        }
        return $result;
    }

    /**
     * MySQL connection function
     *
     * @return array
     */
    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
