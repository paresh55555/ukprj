<?php

namespace SalesQuoter\SystemsPublish;

use SalesQuoter\CustomAbstractCrud;

class SystemsPublish extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "system_id", "ux", "price", "current", "name");
        $config = array("table" => 'SQ_systems_published', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function updateAllSystemFalse($systemId)
    {

        $config['current'] =  false;
        $updateStatement = $this->pdo->update($config)
            ->table($this->table)
            ->where('system_id', '=', $systemId);

        return $updateStatement->execute();
    }
}
