<?php

namespace SalesQuoter\Address;

use SalesQuoter\AbstractCrud;
/**
 *  Class Address2
 *
 */
class Address2 extends AbstractCrud
{
    /**
     * Constructor
     */
    public function __construct()
    {

        $fields = array('id','quote_id', 'sales_person_id','shipping_address1','shipping_address2', 'shipping_city', 'shipping_state',
                        'shipping_zip', 'shipping_phone', 'shipping_contact', 'billing_address1', 'billing_address2', 'billing_city',
                         'billing_state','billing_zip', 'billing_phone','billing_contact');

        $config = array("table" => 'SQ_addresses', 'fields' =>$fields);
        parent::__construct($config);
    }

    /**
     * function jsonDecode
     *
     * @param response $response json encoded string
     *
     * @return json decoded array
     */
    public function update($config)
    {

        $quoteID = $config['quote_id'];
        unset($config['id']);

        $isQuoteExists = $this->checkAddressExists($quoteID);

        if ($isQuoteExists) {
            unset($config['quote_id']);
            $updateStatement = $this->pdo->update($config)->table('SQ_addresses')->where('quote_id', '=', $quoteID);

            $updateStatement->execute();
        }

        $this->create($config);

        
        $allData = array ('quote_id' => $quoteID );

        return $this->getAll($allData);
    }

    
    /**
     * function checkAddressExists
     *
     * @param quoteID $quoteID quotes table id
     *
     * @return true or false
     */
    public function checkAddressExists($quoteID)
    {

              
          $sql = $this->pdo->select(['id'])->from('SQ_addresses')->where('quote_id', '=', $quoteID);
          $stmt = $sql->execute();
          $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
            return  true;
        }

          return false;
    }

    /**
     * function checkEmptyParameter
     *
     * @param data $data array 
     *
     * @return true or false
     */
    public function checkEmptyParameter($data)
    {

        $hasEmpty = false;
        $message = '';
        $sendingAllFields = array_keys($data);
 

        foreach ($this->fields as $key) {
            if ($key != 'id' && (!in_array($key, $sendingAllFields) || strlen($data[$key]) == '0')) {
                 $hasEmpty = true;
                 $message = $key.' can not be Empty';
            }
        }

        if (sizeof($data) == '0') {
             return array ('isEmpty' => $hasEmpty, 'message' => 'seding Parameter Can not Be Empty');
        }

        return array ('isEmpty' => $hasEmpty, 'message' => $message);
    }
}
