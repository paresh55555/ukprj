<?php

namespace SalesQuoter\SiteBranding;

use SalesQuoter\CustomAbstractCrud;

class SiteBranding extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "banner_url", "background_color", "first_link_text", "first_link_color", "first_link_url",
                        "second_link_text", "second_link_color", "second_link_url", "modified", "active");
        $config = array("table" => 'SQ_site_branding', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function getWhere($data)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);

        foreach ($data as $key => $value) {
                  $sql->where($this->table.".".$key, '=', $value);
        }

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }
}
