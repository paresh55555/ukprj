<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\Service\Doors;
use SalesQuoter\Service\DoorTypes;
use SalesQuoter\Service\JobAddresses;

class Orders extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "order_number", "site", "warranty", "warranty_length", "warranty_end_date", "complete_date", "door_installed", "installed_by");
        $config = array ('table' => 'service_orders', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAllOrders () {

    	$allOrders = $this->getAll();

    	for ( $count = 0; $count < sizeof($allOrders); $count++ ) {

    		$allOrders[$count]["customers"] = $this->getOrderCount ( $allOrders[$count]["id"], "service_order_customer", 0 );
    		$allOrders[$count]["jobs"] = 0;
    		$allOrders[$count]["doors"] = $this->getOrderCount ( $allOrders[$count]["id"], "service_doors", 0 );
    		$allOrders[$count]["docs"] = $this->getOrderCount (  $allOrders[$count]["id"], "service_attachments_orders", 0 );
            $allOrders[$count]["notes"] = $this->getOrderNotes (  $allOrders[$count]["id"] );
    	}

        return $allOrders;
    }

    public function getOrderNotes( $orderId ) {

        $sql = $this->pdo->select(["id", "note"])->from("service_notes_orders");
        $sql->where('order_id', '=', $orderId);
        
        $stmt = $sql->execute();

        return $stmt->fetchAll();

    }

    public function insertNotes ( $notes, $orderId ) {

        foreach ( $notes as $note ) {

            if  ( $note["id"] == '0' ) {

                $note["order_id"] = $orderId;
                unset( $note["id"] );

                $insertStatement = $this->pdo->insert(array_keys($note))
                    ->into("service_notes_orders")
                    ->values(array_values($note));


                $insertId = $insertStatement->execute(true);

            }else {

                $noteId = $note["id"];
                unset( $note["id"] );
                
                $updateStatement = $this->pdo->update($note)
                    ->table("service_notes_orders")
                    ->where('id', '=', $noteId);

                $updateStatement->execute();
            }
        }
    }

    public function getOrderCount( $orderId, $tableName, $allowActive ) {

    	$sql = $this->pdo->select(["id"])->from($tableName);
        $sql->where($tableName.'.order_id', '=', $orderId);

        if ( $allowActive == '1' ) {
        	$sql->where($tableName.'.active', '=', 1);
        }
        
        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return sizeof($data); 
    }


    public function createFromCompleted( $config ) {

        $insertOrder = [
            "order_number" => $config["order_number"],
            "site" => $config["site"],
            "warranty_length" => 10,
            "complete_date" => date( "m/d/Y", strtotime($config["complete_date"])),
            "warranty_end_date" =>  date('m/d/Y', strtotime('+10 years', strtotime( $config["complete_date"] ))),
            "warranty" => 0

        ];

        $orderDetails = $this->create ( $insertOrder );

        $this->insertNotes ( $config["notes"], $orderDetails[0]["id"] );

        $doorObj = new Doors();

        foreach ( $config["doors"] as $doors ) {

            $insertDoorData = [];

            $insertDoorData["warranty_end_date"] = $doors["warranty_end_date"];
            $insertDoorData["door_type_id"] = $this->getDoorTypeId (  $doors["door_type"] );
            $insertDoorData["order_id"] = $orderDetails[0]["id"];

            $doorDetails = $doorObj->create ( $insertDoorData );

            $doorObj->createTraits( $doorDetails[0]["id"], $doors['traits'] );
        }

        $jobAddresses = new JobAddresses();

        $customerDetails = $jobAddresses->insertJobAddress( $config );

        $orderCustomerData = [
            "order_id" => $orderDetails[0]["id"] ,
            "customer_id" => $customerDetails["customer_id"],
            "address_id" => $customerDetails["address_id"],
            "note" => "",
        ];


        $insertStatement = $this->pdo->insert(array_keys($orderCustomerData))
                    ->into("service_order_customer")
                    ->values(array_values($orderCustomerData));


        $insertId = $insertStatement->execute(true);

        return $orderDetails;

    }


    public function getDoorTypeId ( $doorType ) {

        $sql = $this->pdo->select(["id"])->from("service_door_types");
        $sql->where('type', '=', $doorType);
        $sql->where('category', '=', 'order');
        $sql->where('active', '=', 1);
        
        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if ( sizeof($data) > '0' ) {

            return $data[0]["id"];
        }else {
            
            $insertDoortype = [
                "type" => $doorType,
                "category" => "order"
            ]; 

            $doorTypes = new DoorTypes();

            $details = $doorTypes->create ( $insertDoortype );

            return  $details[0]["id"];
        }
    }


    public function trackChanges( $orderId, $newChanges = [] ) {

        $result = $this->get( $orderId );

        $fields = [
            "warranty" => "Warranty",
            "warranty_length" => "Warranty length",
            "warranty_end_date" => "Warranty end date",
            "complete_date" => "Complete date",
            "door_installed" => "Door installed date",
            "site" => "Site",
            "order_number" => "Order number",
            "installed_by" => "Intalled by"
        ];

        $allChangeLists = [];

        foreach ($fields as $key => $value) {
            
            if ( strlen($result[0][$key]) > 0 && $result[0][$key] != $newChanges[$key] )  {

                $allChangeLists[] = $value." has been changed from ".$result[0][$key]." to ".$newChanges[$key];
            }
        }

        return $allChangeLists;
    }



}
