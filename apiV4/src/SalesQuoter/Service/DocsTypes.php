<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class DocsTypes extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "type", "category");
        $config = array ('table' => 'service_docs_types', 'fields' => $fields);

        parent::__construct($config);
    }

}
