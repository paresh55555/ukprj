<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class DoorNotes extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "note", "door_id");
        $config = array ('table' => 'service_notes_doors', 'fields' => $fields);

        parent::__construct($config);
    }

}
