<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class DoorTraits extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "name", "trait_value", "door_id");
        $config = array ('table' => 'service_traits', 'fields' => $fields);

        parent::__construct($config);
    }

}
