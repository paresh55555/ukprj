<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;

class JobsDoors extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "job_id", "order_id", "door_id");
        $config = array ('table' => 'service_jobs_door', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAll( $where = [] ) {

    	$selectFields = [
    		"service_jobs_door.*",
    		"service_doors.warranty_end_date",
    		"service_doors.door_type_id",
            "service_door_types.type as door_type",
            "service_door_types.category as door_category"
    	];

        $sql = $this->pdo->select($selectFields)->from($this->table);
        $sql->leftJoin('service_doors', 'service_jobs_door.door_id', '=', 'service_doors.id');
        $sql->leftJoin('service_door_types', 'service_door_types.id', '=', 'service_doors.door_type_id');

        foreach ($where as $key => $value) {
        	$sql->where($this->table.'.'.$key, '=', $value);
        }

        $sql->groupBy("service_jobs_door.id");
        
        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }

}
