<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class Jobs extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "order_id", "job_number", "job_status", "start_date", "start_time", "description", "tech");
        $config = array ('table' => 'service_jobs', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAll ( $where = [] ) {

        $selectFields = [
            "service_jobs.*",
            "service_orders.order_number",
            "service_technician.name as tech_name",
            "service_customer.first_name as customer_first_name",
            "service_customer.last_name  as customer_last_name",
            "service_customer.email as customer_email",
            "service_customer.phone as customer_phone",
            "service_address.address_1 as address_address_1",
            "service_address.address_2 as address_address_2",
            "service_address.city as address_city",
            "service_address.state as address_state",
            "service_address.zip as address_zip"
        ];

        $sql = $this->pdo->select($selectFields)->from($this->table);
        $sql->leftJoin('service_orders', 'service_orders.id', '=', 'service_jobs.order_id');
        $sql->leftJoin('service_technician', 'service_jobs.tech', '=', 'service_technician.id');
        $sql->leftJoin('service_job_customer', 'service_jobs.id', '=', 'service_job_customer.job_id');
        $sql->leftJoin('service_customer', 'service_job_customer.customer_id', '=', 'service_customer.id');
        $sql->leftJoin('service_job_address', 'service_job_address.job_id', '=', 'service_jobs.id');
        $sql->leftJoin('service_address', 'service_address.id', '=', 'service_job_address.address_id');
        
        foreach ($where as $key => $value) {
            $sql->where($this->table.'.'.$key, '=', $value);
        }

        $sql->where('service_jobs.active', '=', 1);

        $sql->groupBy("service_jobs.id");

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }

    public function createJobByDoor ( $doorId = 0 ) {

        $sql = $this->pdo->select(["*"])->from("service_doors");
        $sql->where('service_doors.id', '=', $doorId );
        $sql->where('service_doors.active', '=', 1);

        $stmt = $sql->execute();

        $result = $stmt->fetchAll();

        if ( sizeof($result) > '0' ){

            $insertData = [
                "start_date" => "",
                "order_id" => $result[0]["order_id"],
                "start_time" => "",
                "description" => '',
                "tech" => 0,
                "job_number" => ''
            ];

            $response = $this->create ( $insertData );


            if ( sizeof($response) > '0' ) {

                $this->createJobCustomer ( $response[0]["id"], $doorId, $result[0]["order_id"] );

                return $this->get ( $response[0]["id"] );

            }else {
                return [];
            }

        }else {
            return [];
        }

    }

    public function createJobCustomer ( $jobId, $doorId, $orderId ) {

        $sql = $this->pdo->select(["customer_id", "address_id"])->from("service_door_customer");
        $sql->where('service_door_customer.door_id', '=', $doorId );
        $sql->orderBy('id', 'ASC');

        $stmt = $sql->execute();

        $result = $stmt->fetchAll();

        if ( sizeof($result) > '0' ) {

            $this->createNewJobCustomer ( $jobId, $result[0]["customer_id"], $result[0]["address_id"] );

        }else {

            $sql = $this->pdo->select(["customer_id", "address_id"])->from("service_order_customer");
            $sql->where('service_order_customer.order_id', '=', $orderId );
            $sql->orderBy('id', 'ASC');

            $stmt = $sql->execute();

            $result = $stmt->fetchAll();

            if ( sizeof( $result ) > '0' ) {

                $this->createNewJobCustomer ( $jobId, $result[0]["customer_id"], $result[0]["address_id"] );
            }

        }
    }


    public function createNewJobCustomer ( $jobId, $customerId, $addressId ) {

        $insertData = [
            "customer_id" => $customerId,
            "address_id" => $addressId,
            "job_id" => $jobId,
            "note" => ''
        ];

        $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_job_customer")
                    ->values(array_values($insertData));

        $insertId = $insertStatement->execute(true);
    }

    public function getAllTechinician() {

        $sql = $this->pdo->select(["*"])->from("service_technician");
        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }

    public function updateJobNumber ( $jobId ) {

        $job_number = "JOB-".$jobId;
        $this->update ( ["id" => $jobId, "job_number" => $job_number  ] );

        return $job_number;
    }

    public function trackChanges( $jobId, $newChanges = [] ) {

        $result = $this->get( $jobId );

        $fields = [
            "job_status" => "Status",
            "start_date" => "Start date",
            "start_time" => "Start time",
            "description" => "Description",
            "tech" => "Technician"
        ];

        $allChangeLists = [];

        foreach ($fields as $key => $value) {
            
            if ( strlen($result[0][$key]) > 0 && $result[0][$key] != $newChanges[$key] )  {

                $allChangeLists[] = $value." has been changed from ".$result[0][$key]." to ".$newChanges[$key];
            }
        }

        return $allChangeLists;
    }

}
