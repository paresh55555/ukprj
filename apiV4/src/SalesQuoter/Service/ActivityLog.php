<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class ActivityLog extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "logs", "field_id", "user_id", "log_time", "type", "log_type");
        $config = array ('table' => 'service_jobs_activity_log', 'fields' => $fields);

        parent::__construct($config);
    }


    public function addLog ( $logMessage, $fieldId, $type, $logType, $userId = 0 ) {

    	$insertData= [
    		"logs" => $logMessage,
    		"field_id" => $fieldId,
    		"user_id" => $userId,
    		"log_time" => date("Y-m-d H:i:s"),
    		"type" => $type,
            "log_type" => $logType
    	];

    	$this->create ( $insertData );
    }

    public function getAll( $where ) {

        $selectFields = [
            "CONCAT(service_jobs_activity_log.logs, ' by ', users.name) as logs",
            "service_jobs_activity_log.field_id",
            "service_jobs_activity_log.log_time",
            "service_jobs_activity_log.type",
            "service_jobs_activity_log.log_type",
            "service_jobs_activity_log.user_id",
            "users.name"
        ];
        
        $sql = $this->pdo->select($selectFields)->from($this->table);
        $sql->leftJoin('users', 'users.id', '=', 'service_jobs_activity_log.user_id');

        foreach ($where as $key => $value) {
        	$sql->where($this->table.'.'.$key, '=', $value);
        }

        $sql->orderBy("log_time", "desc");

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        for ( $count = 0; $count < sizeof($data); $count++) {

        	$data[$count]["elapsed_time"] = $this->time_elapsed_string ( $data[$count]["log_time"] );
        }

        return $data;
    }

    function time_elapsed_string($datetime, $full = false) {

	    $now = new \DateTime;
	    $ago = new \DateTime($datetime);
	    $diff = $now->diff($ago);

	    $diff->w = floor($diff->d / 7);
	    $diff->d -= $diff->w * 7;

	    $string = array(
	        'y' => 'year',
	        'm' => 'month',
	        'w' => 'week',
	        'd' => 'day',
	        'h' => 'hour',
	        'i' => 'minute',
	        's' => 'second',
	    );
	    foreach ($string as $k => &$v) {
	        if ($diff->$k) {
	            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
	        } else {
	            unset($string[$k]);
	        }
	    }

	    if (!$full) $string = array_slice($string, 0, 1);
	    return $string ? implode(', ', $string) . ' ago' : 'just now';
	}

}
