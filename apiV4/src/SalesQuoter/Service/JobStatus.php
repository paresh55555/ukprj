<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;

class JobStatus extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "status", "filter_id");
        $config = array ('table' => 'service_jobs_status', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAll( $filters ) {

        $sql = $this->pdo->select(["*"])->from("service_job_filters");

        foreach ($filters as $key => $value) {
        	$sql->where('service_job_filters.'.$key, '=', $value);
        }
        
        $stmt = $sql->execute();

        $result= $stmt->fetchAll();

        for ( $count = 0; $count < sizeof($result); $count++ ) {

        	$result[$count]["statusLists"] = $this->getStatusLists( $result[$count]["id"] );
        } 

        return $result;
    }

    public function getStatusLists ( $filterId = 0 ) {

        $sql = $this->pdo->select(["*"])->from($this->table);
        $sql->where($this->table.'.filter_id', '=', $filterId);
        
        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }
    
}
