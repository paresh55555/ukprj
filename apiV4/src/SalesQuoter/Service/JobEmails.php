<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\Service\JobFiles;

class JobEmails extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "jobs_id", "email", "subject", "message", "user_id", "datetime");
        $config = array ('table' => 'service_jobs_email_history', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getWhere($data) {

    	$allFields = [
    		"service_jobs_email_history.*",
    		"users.name as from_name"
    	];

        $sql = $this->pdo->select($allFields)->from($this->table);
 		$sql->leftJoin('users', 'service_jobs_email_history.user_id', '=', 'users.id');
        
        foreach ($data as $key => $value) {
            $sql->where($this->table.".".$key, '=', $value);
        }

        $sql->where($this->table.'.active', '=', 1);
        $sql->orderBy("service_jobs_email_history.datetime", "desc");

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    public function getAllFiles( $fileId = 0 ) {


        $sql = $this->pdo->select(["*"])->from("service_jobs_email_files");
        $sql->where("service_jobs_email_files.email_id", '=', $fileId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    public function insertJobFiles( $jobId, $emailId, $files, $fileNames ) {

        $jobFiles = new JobFiles();

        for ( $count= 0; $count < sizeof($files); $count++) {

            $fileName = "salesquoter/jobs/".$jobId."/emails/".$emailId."/".str_replace(" ", "-", $fileNames[$count]); 
            $uploadedFiles = $jobFiles->uploadFileToS3 ( $fileName, $files[$count] );

            if ( $uploadedFiles["status"] == 'success' ) {

                $insertData = [
                    "email_id" => $emailId,
                    "file_path" => $uploadedFiles["filePath"],
                    "file_name" => $fileNames[$count]
                ];

                $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_jobs_email_files")
                    ->values(array_values($insertData));

                $insertStatement->execute(true);    
            }
        }

    }

}
