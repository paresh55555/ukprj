<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class DoorTypes extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "type", "category");
        $config = array ('table' => 'service_door_types', 'fields' => $fields);

        parent::__construct($config);
    }

}
