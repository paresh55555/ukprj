<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;

class JobAttachments extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "job_id", "url", "file_name");
        $config = array ('table' => 'service_attachments_jobs', 'fields' => $fields);

        parent::__construct($config);
    }


    public function doFileUpload ( $data, $jobId, $attchmentId ) {

        try {
            
            $height =  intval($data['height']);
            $width = intval($data['width']);
            $base64String = $data['image'];

            $validImgExtension = array("jpg", "jpeg", "SVG", "svg", "png", "gif", "JPG", "JPEG", "svg+xml");
            $validPdfExtension = array( "pdf", "PDF", "Pdf" );

            $data = explode(",", $base64String);
            $extensionPortion1 = explode(";", $data[0]);
            $extensionPortion2 = explode("/", $extensionPortion1[0]);

            $extension = $extensionPortion2[1];
            $type = explode(":", $extensionPortion2[0]);

            if ($type[1] == "image" && ($height > '0' && $width == '0')) {
                $responseData = array('status' => 'error', 'message' => 'Enter Both height & width dimension');
           
                return $responseData;
            } elseif ($type[1] == "image" && (!in_array($extension, $validImgExtension))) {
                $responseData = array('status' => 'error', 'message' => 'Image type is not valid ');

                return $responseData;
            }

            $error = 0;

            if ( $_SERVER['IMAGE_STORE'] == null || strlen($_SERVER['IMAGE_STORE']) == '0' ) {

                throw new ImageException($_SERVER['IMAGE_STORE']);   
            }else {
                $uploadPath =  $_SERVER['IMAGE_STORE'];
            }  
             

            $uploadFolder = pathinfo($uploadPath)['filename'];
            $uploadPath = $uploadPath.'/jobs';
            $folderCreated2 =  mkdir($uploadPath);
            $uploadPath .= "/".$jobId;
            $folderCreated3 =  mkdir($uploadPath);
            $uploadPath .= "/".$attchmentId.".".$extension;
            $outputFile = $uploadPath;

            $ifp = fopen($outputFile, "wb");

            $writeFile = fwrite($ifp, base64_decode($data[1]));

            if ($writeFile === false) {
                 $error = 1;
                 $msg = " File is not written in desired folder. Pls check File Permissions ";
            }

            fclose($ifp);

            if ($error == '1') {

                $responseData = array(
                    'status' => 'error',
                    'msg' => $msg,
                );

                return responseWithStatusCode($response, $responseData, 200);

            } else {
                if ($extension == 'svg') {

                    $responseData = array(
                        'status' => 'success',
                        'url' => parseImageName($uploadFolder, $outputFile),
                    );

                    return $responseData;
                } else if ( $type[1] == "image" ) {

                    if ( $width == '0' && $height == '0' ) {
                        $imagePathDetails = explode ($uploadFolder, $outputFile);
                        $url =  "api/V4/".$uploadFolder.$imagePathDetails[1];

                    }else {
                        $url = parseImageName($uploadFolder, resizeImage($outputFile, $width, $height));
                    }
                    
                    $responseData = array(
                        'status' => 'success',
                        'url' => $url,
                    );

                    return $responseData;
                }else {

                    $imagePathDetails = explode ($uploadFolder, $outputFile);
              
                    $pdfFilePath =  "api/V4/".$uploadFolder.$imagePathDetails[1];

                    $responseData = array(
                        'status' => 'success',
                        'url' => $pdfFilePath,
                    );

                    return $responseData;
                }
            }

        } catch ( ImageException $e ) {

                $responseData = array(
                    'status' => 'error',
                    'msg' => 'Server Variable IMAGE_STORE is not set',
                );

                return $responseData;
        } 
    }
}
