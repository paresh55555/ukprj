<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;

class JobAddresses extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "job_id", "address_id", "note");
        $config = array ('table' => 'service_job_address', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAll( $where = [] ) {

    	$selectFields = [
    		"service_job_address.id",
    		"service_job_address.job_id",
    		"service_job_address.address_id",
    		"service_job_address.note",
            "service_customer.id as customer_id",
            "service_customer.first_name",
            "service_customer.last_name",
            "service_customer.phone",
            "service_customer.email",
            "service_address.address_1",
            "service_address.address_2",
            "service_address.city",
            "service_address.state",
            "service_address.zip"
    	];

        $sql = $this->pdo->select($selectFields)->from($this->table);
        $sql->leftJoin('service_address', 'service_address.id', '=', 'service_job_address.address_id');
        $sql->leftJoin('service_customer_address', 'service_customer_address.address_id', '=', 'service_address.id');
        $sql->leftJoin('service_customer', 'service_customer.id', '=', 'service_customer_address.customer_id');
        
        foreach ($where as $key => $value) {
        	$sql->where($this->table.'.'.$key, '=', $value);
        }

        $sql->groupBy("service_job_address.id");
        
        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }


    public function getAddressId ( $config ) {

        $sql = $this->pdo->select(["id"])->from("service_customer");
        $sql->where('service_customer.email', '=', $config["email"] );

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();
        
        if ( sizeof($data) == '0' ) {

        	$insertData = [
        		"first_name" => $config["first_name"],
        		"last_name" => $config["last_name"],
        		"email" => $config["email"],
        		"phone" => $config["phone"]
        	];

	        $insertStatement = $this->pdo->insert(array_keys($insertData))
	            ->into("service_customer")
	            ->values(array_values($insertData));

	        $insertId = $insertStatement->execute(true);

	        return $insertId;
        }else {

            $updateData = [
                "first_name" => $config["first_name"],
                "last_name" => $config["last_name"],
                "phone" => $config["phone"]
            ];

            $updateStatement = $this->pdo->update( $updateData )
                    ->table("service_customer")
                    ->where('id', '=', $data[0]['id']);

            $updateStatement->execute();

        	return $data[0]['id'];
        }
    }

    public function getCustomerId ( $config ) {

        if ( intval($config["customer_id"]) > '0' ) {

            return $config["customer_id"];
        }else {

            $sql = $this->pdo->select(["id"])->from("service_customer");
            $sql->where('service_customer.email', '=', $config["email"] );
            $sql->where('service_customer.first_name', '=', $config["first_name"] );
            $sql->where('service_customer.phone', '=', $config["phone"] );
            $sql->where('service_customer.last_name', '=', $config["last_name"] );

            $stmt = $sql->execute();

            $data = $stmt->fetchAll();
        
            if ( sizeof($data) == '0' ) {

                $insertData = [
                    "first_name" => $config["first_name"],
                    "last_name" => $config["last_name"],
                    "email" => $config["email"],
                    "phone" => $config["phone"]
                ];

                $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_customer")
                    ->values(array_values($insertData));

                $insertId = $insertStatement->execute(true);

                return $insertId;

            }else {

                return $data[0]['id'];
            }
        }
    }

    public function getJobCustomerId ( $config ) {

        if ( intval($config["customer_id"]) > '0' ) {

            $this->updateCustomer( $config, $config["customer_id"] );

            return $config["customer_id"];
        }else {

            $sql = $this->pdo->select(["id"])->from("service_customer");
            $sql->where('service_customer.email', '=', $config["email"] );
            $sql->where('service_customer.first_name', '=', $config["first_name"] );
            $sql->where('service_customer.phone', '=', $config["phone"] );
            $sql->where('service_customer.last_name', '=', $config["last_name"] );

            $stmt = $sql->execute();

            $data = $stmt->fetchAll();
        
            if ( sizeof($data) == '0' ) {

                $insertData = [
                    "first_name" => $config["first_name"],
                    "last_name" => $config["last_name"],
                    "email" => $config["email"],
                    "phone" => $config["phone"]
                ];

                $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_customer")
                    ->values(array_values($insertData));

                $insertId = $insertStatement->execute(true);

                return $insertId;

            }else {

                $this->updateCustomer( $config, $data[0]['id'] );
                
                return $data[0]['id'];
            }
        }
    }


    public function getContactsAddressId ( $data ) {

        if ( intval($data["address_id"]) > '0' ) {

            return $data["address_id"];
        }

        $sql = $this->pdo->select(["id"])->from("service_address");
        $sql->where('service_address.address_1', '=', $data["address_1"] );
        $sql->where('service_address.address_2', '=', $data["address_2"] );
        $sql->where('service_address.city', '=', $data["city"] );
        $sql->where('service_address.state', '=', $data["state"] );
        $sql->where('service_address.zip', '=', $data["zip"] );

        $stmt = $sql->execute();

        $result = $stmt->fetchAll();
        
        if ( sizeof($result) == '0' ) {

            $insertData = [
                "address_1" => $data["address_1"],
                "address_2" => $data["address_2"],
                "city" => $data["city"],
                "state" => $data["state"],
                "zip" => $data["zip"]
            ];

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_address")
                    ->values(array_values($insertData));

            $insertId = $insertStatement->execute(true);

            return $insertId;
                
        }else {

            $this->updateAddress( $data, $result[0]['id'] );

            return $result[0]['id'];

        }
        
    }

    public function getJobContactsAddressId ( $data ) {

        if ( intval($data["address_id"]) > '0' ) {

            $this->updateAddress( $data, $data["address_id"] );

            return $data["address_id"];
        }

        $sql = $this->pdo->select(["id"])->from("service_address");
        $sql->where('service_address.address_1', '=', $data["address_1"] );
        $sql->where('service_address.address_2', '=', $data["address_2"] );
        $sql->where('service_address.city', '=', $data["city"] );
        $sql->where('service_address.state', '=', $data["state"] );
        $sql->where('service_address.zip', '=', $data["zip"] );

        $stmt = $sql->execute();

        $result = $stmt->fetchAll();
        
        if ( sizeof($result) == '0' ) {

            $insertData = [
                "address_1" => $data["address_1"],
                "address_2" => $data["address_2"],
                "city" => $data["city"],
                "state" => $data["state"],
                "zip" => $data["zip"]
            ];

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_address")
                    ->values(array_values($insertData));

            $insertId = $insertStatement->execute(true);

            return $insertId;
                
        }else {

            return $result[0]['id'];

        }
        
    }

    public function insertAddress ( $data ) {

        $customerId = $this->getCustomerId( $data );
        $addressId = $this->getContactsAddressId( $data );

        $sql = $this->pdo->select(["id"])->from("service_customer_address");
        $sql->where('service_customer_address.address_id', '=', $addressId );
        $sql->where('service_customer_address.customer_id', '=', $customerId );

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();
        
        if ( sizeof($data) == '0' ) {

            $insertData = [];
            $insertData["address_id"] = $addressId;
            $insertData["customer_id"] = $customerId;

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_customer_address")
                    ->values(array_values($insertData));

            $insertStatement->execute(true);

        }

        return [ "address_id" => $addressId, "customer_id" => $customerId ];

    } 


    public function insertJobAddress ( $data ) {

        $customerId = $this->getJobCustomerId( $data );
        $addressId = $this->getJobContactsAddressId( $data );

        $sql = $this->pdo->select(["id"])->from("service_customer_address");
        $sql->where('service_customer_address.address_id', '=', $addressId );
        $sql->where('service_customer_address.customer_id', '=', $customerId );

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();
        
        if ( sizeof($data) == '0' ) {

            $insertData = [];
            $insertData["address_id"] = $addressId;
            $insertData["customer_id"] = $customerId;

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_customer_address")
                    ->values(array_values($insertData));

            $insertStatement->execute(true);

        }else{

            $sql = $this->pdo->select(["id"])->from("service_customer_address");
            $sql->where('service_customer_address.address_id', '=', $addressId );

            $stmt = $sql->execute();

            $data = $stmt->fetchAll();  

            if ( sizeof($data) == '0' ) {

                $insertData = [];
                
                $updateData = [
                    "customer_id" => $customerId,
                ];

                $updateStatement = $this->pdo->update( $updateData )
                            ->table("service_customer_address")
                            ->where('address_id', '=', $addressId);

                $updateStatement->execute();

            } 
        }

        return [ "address_id" => $addressId, "customer_id" => $customerId ];

    } 

    public function updateAddress( $data, $addressId ) {

        $updateData = [
            "address_1" => $data["address_1"],
            "address_2" => $data["address_2"],
            "city" => $data["city"],
            "state" => $data["state"],
            "zip" => $data["zip"]
        ];

        $updateStatement = $this->pdo->update( $updateData )
                    ->table("service_address")
                    ->where('id', '=', $addressId);

        $updateStatement->execute();
    }


    public function updateCustomer( $data, $customerId ) {

        $updateData = [
            "first_name" => $data["first_name"],
            "last_name" => $data["last_name"],
            "email" => $data["email"],
            "phone" => $data["phone"]
        ];

        $updateStatement = $this->pdo->update( $updateData )
                    ->table("service_customer")
                    ->where('id', '=', $customerId);

        $updateStatement->execute();
    }
}
