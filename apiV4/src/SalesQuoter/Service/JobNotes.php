<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;

class JobNotes extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "note", "job_id");
        $config = array ('table' => 'service_notes_jobs', 'fields' => $fields);

        parent::__construct($config);
    }

}
