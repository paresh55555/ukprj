<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;
use SalesQuoter\Service\JobAddresses;

class Customers extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "customer_id", "address_id");
        $config = array ('table' => 'service_customer_address', 'fields' => $fields);

        parent::__construct($config);
    }


    public function getAll( $where = [] ) {

    	$selectFields = [
    		"service_customer_address.id",
            "service_customer_address.address_id",
            "service_customer_address.note",
            "service_customer.id as customer_id",
    		"service_customer.first_name",
    		"service_customer.last_name",
    		"service_customer.email",
    		"service_customer.phone",
            "service_address.address_1",
            "service_address.address_2",
            "service_address.city",
            "service_address.state",
            "service_address.zip"
    	];

        $sql = $this->pdo->select($selectFields)->from('service_customer');
        $sql->leftJoin($this->table, 'service_customer_address.customer_id', '=', 'service_customer.id');
        $sql->leftJoin('service_address', 'service_address.id', '=', 'service_customer_address.address_id');

        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }

    public function getCustomersOrders ( $customerId = 0 ) {

        $query = "(SELECT service_orders.id, service_orders.order_number FROM service_orders
                 INNER JOIN `service_order_customer` ON service_order_customer.order_id = service_orders.id
                 WHERE service_order_customer.customer_id = '$customerId'
                 AND service_orders.active = 1 GROUP BY service_orders.id) 
                 UNION 
                 (SELECT service_orders.id, service_orders.order_number FROM service_orders
                 INNER JOIN `service_doors` ON service_doors.order_id = service_orders.id AND service_doors.active = 1
                 INNER JOIN `service_door_customer` ON service_door_customer.door_id = service_doors.id
                 WHERE service_door_customer.customer_id = '$customerId'
                 AND service_orders.active = 1 GROUP BY service_orders.id)";

        $stmt = $this->pdo->query($query);
        $stmt->execute();

        return  $stmt->fetchAll();
    }


    public function getCustomersJobs ( $customerId = 0 ) {

        $query = "(SELECT service_jobs.id, service_jobs.job_number 
            FROM service_jobs 
            INNER JOIN `service_job_customer` ON service_job_customer.job_id = service_jobs.id 
            WHERE service_job_customer.customer_id = '$customerId' 
            AND service_jobs.active = 1 GROUP BY service_jobs.id) 
            UNION 
            (SELECT service_jobs.id, service_jobs.job_number 
            FROM service_jobs 
            INNER JOIN `service_jobs_door` ON service_jobs_door.job_id = service_jobs.id 
            INNER JOIN `service_door_customer` ON service_door_customer.door_id = service_jobs_door.door_id 
            WHERE service_door_customer.customer_id = '$customerId' AND service_jobs.active = 1 
            GROUP BY service_jobs.id)";

        $stmt = $this->pdo->query($query);
        $stmt->execute();

        return  $stmt->fetchAll();
    }

    public function updateAddress ( $config ) {

        $id = $config["id"];

        unset ( $config["id"] );
        $jobAddress = new JobAddresses();

        $jobAddress->getJobCustomerId( $config );

        $updateData = [
            "address_1" => $config["address_1"],
            "address_2" => $config["address_2"],
            "city" => $config["city"],
            "state" => $config["state"],
            "zip" => $config["zip"]
        ];

        $updateStatement = $this->pdo->update($updateData)
            ->table("service_address")
            ->where('id', '=', $id);

        $updateStatement->execute();

        $sql = $this->pdo->select(["*"])->from("service_address");
        $sql->where('id', '=', $id);

        $stmt = $sql->execute();

        return  $stmt->fetchAll();
    }
    
    public function createCustomer ( $config ) {

		$insertStatement = $this->pdo->insert(array_keys($config))
            ->into("service_customer")
            ->values(array_values($config));


        $insertId = $insertStatement->execute(true);

        $sql = $this->pdo->select(["*"])->from("service_customer");
        $sql->where('id', '=', $insertId);

        $stmt = $sql->execute();

        return  $stmt->fetchAll();
    }

    public function updateCustomer ( $config ) {

        $id = $config["id"];
		
        $updateData = [
            "first_name" => $config["first_name"],
            "last_name" => $config["last_name"],
            "email" => $config["email"],
            "phone" => $config["phone"]
        ];

        $updateStatement = $this->pdo->update($updateData)
            ->table("service_customer")
            ->where('id', '=', $id);

        $updateStatement->execute();

        $sql = $this->pdo->select(["*"])->from("service_customer");
        $sql->where('id', '=', $id);

        $stmt = $sql->execute();

        return  $stmt->fetchAll();
    }
    
   public function getAllCustomers( $id ) {

    	$selectFields = [
    		"service_customer.*",
    	];

        $sql = $this->pdo->select($selectFields)->from("service_customer");
		$sql->where( 'service_customer.id', '=', $id );
        $stmt = $sql->execute();

        $result = $stmt->fetchAll();
		
		for ( $count = 0; $count < sizeof($result); $count++ ) {
		
		    $result[$count]["address_list"] = $this-> getAllAddress( $result[$count]["id"] );
		}
		
		return $result;
    }
	
	
	public function getAllAddress ( $customerId ) {
	
	    $selectFields = [
    		"service_address.*",
    	];

        $sql = $this->pdo->select($selectFields)->from("service_customer_address");
        $sql->leftJoin('service_address', 'service_address.id', '=', 'service_customer_address.address_id');
		$sql->where( 'service_customer_address.customer_id', '=', $customerId );
		
		$stmt = $sql->execute();

        $result = $stmt->fetchAll();
		
		return $result;
	}


    public function insertAddress ( $data ) {

        $customerId = $data["customer_id"];
        $jobAddress = new JobAddresses();
    
        $addressId = $jobAddress->getJobContactsAddressId( $data );

        $sql = $this->pdo->select(["id"])->from("service_customer_address");
        $sql->where('service_customer_address.address_id', '=', $addressId );
        $sql->where('service_customer_address.customer_id', '=', $customerId );

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();
        
        if ( sizeof($data) == '0' ) {

            $insertData = [];
            $insertData["address_id"] = $addressId;
            $insertData["customer_id"] = $customerId;

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                    ->into("service_customer_address")
                    ->values(array_values($insertData));

            $insertStatement->execute(true);

        }else{

            $sql = $this->pdo->select(["id"])->from("service_customer_address");
            $sql->where('service_customer_address.address_id', '=', $addressId );

            $stmt = $sql->execute();

            $data = $stmt->fetchAll();  

            if ( sizeof($data) == '0' ) {

                $insertData = [];
                
                $updateData = [
                    "customer_id" => $customerId,
                ];

                $updateStatement = $this->pdo->update( $updateData )
                            ->table("service_customer_address")
                            ->where('address_id', '=', $addressId);

                $updateStatement->execute();

            } 
        }

        return [ "address_id" => $addressId, "customer_id" => $customerId ];    
    }


    public function getAddressDetails ( $id )  {

        $sql = $this->pdo->select(["*"])->from("service_address");
        $sql->where('id', '=', $id);

        $stmt = $sql->execute();

        return  $stmt->fetchAll();
    }


    public function deleteAddress( $addressId ) {

        $deleteStatement = $this->pdo->delete()
            ->from("service_address")
            ->where('id', '=', $addressId);

        $affectedRows = $deleteStatement->execute();

        $allTables = [
            "service_customer_address",
            "service_job_address",
            "service_order_customer",
            "service_door_customer"
        ];

        foreach (  $allTables as $table ) {

            $deleteStatement = $this->pdo->delete()
                ->from($table)
                ->where('address_id', '=', $addressId);

            $affectedRows = $deleteStatement->execute();
        }

        $result = array("status" => "success");


        return $result;
    }
}
