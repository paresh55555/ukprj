<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;
use SalesQuoter\Amazon\S3;

class JobFiles extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "job_id", "url", "file_name", "file_type", "poster_image");
        $config = array ('table' => 'service_jobs_files', 'fields' => $fields);

        parent::__construct($config);
    }


    public function uploadFileToS3 ( $fileName, $temp_name )
    {

        $bucket = AWS_BUCKET;

        $filePath = "";
        $returArray = array(
            "status" => false
        );

        $accessKey = AWS_ACCESS_KEY;
        $secretKey = AWS_SECRET_KEY;

        $s3Url = AWS_URL;
    
        if (strlen($temp_name) > 0) {
            $file = $temp_name;
    
            // set authetication
            S3::setAuth($accessKey, $secretKey);

            if( ! S3::getBucket($bucket) ){
                S3::putBucket($bucket, S3::ACL_PUBLIC_READ, $location = "US");
            }
            $is_uploaded = S3::putObjectFile($file, $bucket, $fileName, S3::ACL_PRIVATE );

            if ( $is_uploaded["uploaded"] ) {
                $filePath = $s3Url . $bucket . "/" . $fileName;
                $returArray['filePath'] = $filePath;
                $returArray['status'] = "success";
                 $returArray['msg'] = '';
            } else {
                $returArray['filePath'] = '';
                $returArray['status'] = "error";
                $returArray['msg'] = $is_uploaded["msg"];
            }
        }
    
        return $returArray;
    }

    public function getPosterImageFromVideo ( $videoPath, $jobId ) {

        $ffmpeg = \FFMpeg\FFMpeg::create();
        $video = $ffmpeg->open($videoPath);
        $video->filters()->resize(new \FFMpeg\Coordinate\Dimension(320, 240))->synchronize();
        $imageName = time().rand(10000, 99999).'.jpg';
        $video->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(1))->save($imageName);

        if ( file_exists($imageName) ) {

            $fileName = "salesquoter/jobs/".$jobId."/videos/poster_images/".$imageName;
            $filePath = $this->uploadFileToS3 ( $fileName, $imageName );

            if ( $filePath["status"] == 'success' ) {

                unlink($imageName);

                return $filePath["filePath"];
            }else {
                return "https://i.ytimg.com/vi/NpEaa2P7qZI/maxresdefault.jpg";
            }

        }else {

            return "https://i.ytimg.com/vi/NpEaa2P7qZI/maxresdefault.jpg";
        }
    }


    public function getFileType ( $fileName ) {

        $imageExtension = [
            "png",
            "jpg",
            "gif",
            "jpeg",
            "bmp",
            "PNG",
            "JPG",
            "JPEG"
        ];

        $videoExtension = [
            "webm",
            "mkv",
            "mp4",
            "flv",
            "vob",
            "ogv",
            "vob",
            "ogg",
            "avi",
            "wmv",
            "yuv",
            "rm",
            "rmvb",
            "asf",
            "3gp",
            "mpeg",
            "mpg",
            "mpv",
            "f4v",
            "3g2",
            "m4p",
            "amv",
            "mov",
            "MTS",
            "mng",
            "M2TS",
            "3gpp"
        ];

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);

        if ( in_array($ext, $imageExtension) ) {

            return  "image"; 
        }else if (in_array($ext, $videoExtension) ) {
            return  "video"; 
        }else {
            return "other";
        }
    }
    
}
