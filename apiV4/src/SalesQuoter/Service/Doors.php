<?php

namespace SalesQuoter\Service;

use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\Service\DoorTraits;

class Doors extends CustomAbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "order_id", "warranty_end_date", "door_type_id");
        $config = array ('table' => 'service_doors', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAll ( $where) {

        $selectFields = [
            "service_doors.*",
            "service_door_types.type as door_type",
            "service_door_types.category as door_category",
        ];

        $sql = $this->pdo->select($selectFields)->from($this->table);
        $sql->leftJoin('service_door_types', 'service_door_types.id', '=', 'service_doors.door_type_id');
        $sql->where("service_doors.active", '=', 1);

        foreach ($where as $key => $value) {
            $sql->where($this->table.'.'.$key, '=', $value);
        }

        $stmt = $sql->execute();

        return $stmt->fetchAll();   
    }

    public function createTraits( $doorId, $traits ) {

        $doorTraits = new DoorTraits();

        $this->deleteTraits( $doorId );

        for ( $count = 0; $count < sizeof($traits); $count++ ) {

            $id = $traits[$count]["id"];
            $traits[$count]["door_id"] = $doorId;

            unset ( $traits[$count]["id"] );
            $doorTraits->create ( $traits[$count] );
        } 
    }

    public function deleteTraits( $doorId ) {

        $config['active'] = 0;
        $deleteStatement = $this->pdo->update($config)
          ->table("service_traits")
          ->where('door_id', '=', $doorId);

        $affectedRows = $deleteStatement->execute();
    }   

}
