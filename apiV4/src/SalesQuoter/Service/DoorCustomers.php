<?php

namespace SalesQuoter\Service;

use SalesQuoter\AbstractCrud;

class DoorCustomers extends AbstractCrud
{

    public function __construct()
    {
       
        $fields =  array("id", "door_id", "customer_id", "address_id", "note");
        $config = array ('table' => 'service_door_customer', 'fields' => $fields);

        parent::__construct($config);
    }

    public function getAll( $where = [] ) {

    	$selectFields = [
    		"service_door_customer.id",
    		"service_door_customer.door_id",
    		"service_door_customer.customer_id",
    		"service_door_customer.note",
    		"service_customer.first_name",
    		"service_customer.last_name",
    		"service_customer.email",
    		"service_customer.phone",
            "service_address.address_1",
            "service_address.address_2",
            "service_address.city",
            "service_address.state",
            "service_address.zip"
    	];

        $sql = $this->pdo->select($selectFields)->from($this->table);
        $sql->leftJoin('service_customer', 'service_door_customer.customer_id', '=', 'service_customer.id');
        $sql->leftJoin('service_address', 'service_address.id', '=', 'service_door_customer.address_id');
        
        foreach ($where as $key => $value) {
        	$sql->where($this->table.'.'.$key, '=', $value);
        }

        $sql->groupBy("service_door_customer.id");
        
        $stmt = $sql->execute();

        return $stmt->fetchAll();
    }


    public function getCustomerId ( $config ) {

        $sql = $this->pdo->select(["id"])->from("service_customer");
        $sql->where('service_customer.email', '=', $config["email"] );

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();
        
        if ( sizeof($data) == '0' ) {

        	$insertData = [
        		"first_name" => $config["first_name"],
        		"last_name" => $config["last_name"],
        		"email" => $config["email"],
        		"phone" => $config["phone"]
        	];

	        $insertStatement = $this->pdo->insert(array_keys($insertData))
	            ->into("service_customer")
	            ->values(array_values($insertData));

	        $insertId = $insertStatement->execute(true);

	        return $insertId;
        }else {

            $updateData = [
                "first_name" => $config["first_name"],
                "last_name" => $config["last_name"],
                "phone" => $config["phone"]
            ];

            $updateStatement = $this->pdo->update( $updateData )
                    ->table("service_customer")
                    ->where('id', '=', $data[0]['id']);

            $updateStatement->execute();

        	return $data[0]['id'];
        }
    }

    public function updateCustomerAddress ( $data, $customerId ) {

        $sql = $this->pdo->select(["id"])->from("service_address");
        $sql->where('service_address.phone', '=', $data["phone"] );
        $sql->where('service_address.email', '=', $data["email"] );

        $stmt = $sql->execute();

        $serviceAddressResult = $stmt->fetchAll();
        
        if ( sizeof($serviceAddressResult) == '0' ) {

            $insertData = [
                "address_1" => $data["address_1"],
                "address_2" => $data["address_2"],
                "city" => $data["city"],
                "state" => $data["state"],
                "zip" => $data["zip"],
                "phone" => $data["phone"]
            ];

            $insertStatement = $this->pdo->insert(array_keys($insertData))
                ->into("service_address")
                ->values(array_values($insertData));

            $insertId = $insertStatement->execute(true);

            $this->insertCustomerAddress ( $customerId, $insertId, $data["note"] );

            return $insertId;
        }else {

            $this->updateCustomerServiceAddress( $serviceAddressResult[0]["id"], $data );

            $sql = $this->pdo->select(["id"])->from("service_customer_address");
            $sql->where('address_id', '=', $serviceAddressResult[0]["id"] );
            $sql->where('customer_id', '=',  $customerId);

            $stmt = $sql->execute();

            $result = $stmt->fetchAll();

            if ( sizeof($result) == '0' ) {

                return $this->insertCustomerAddress ( $customerId, $serviceAddressResult[0]["id"], $data["note"] );
            }else {

                $updateStatement = $this->pdo->update( ["note" => $data["note"] ])
                    ->table("service_customer_address")
                    ->where('address_id', '=', $serviceAddressResult[0]["id"])
                    ->where('customer_id', '=', $customerId);

                $updateStatement->execute();
            }
        }

    }

    public function updateCustomerServiceAddress( $addressId, $data ) {

        $updateData = [
            "address_1" => $data["address_1"],
            "address_2" => $data["address_2"],
            "city" => $data["city"],
            "state" => $data["state"],
            "zip" => $data["zip"],
            "phone" => $data["phone"]
        ];

        $updateStatement = $this->pdo->update( $updateData )
                    ->table("service_address")
                    ->where('id', '=', $addressId);

        $updateStatement->execute();
    }

    public function insertCustomerAddress ( $customerId, $addressId, $note ) {

        $insertData = [
            "customer_id" => $customerId,
            "address_id" => $addressId,
            "note" => $note
        ];

        $insertStatement = $this->pdo->insert(array_keys($insertData))
                ->into("service_customer_address")
                ->values(array_values($insertData));

        $insertId = $insertStatement->execute(true);

        return $insertId;
    }

    public function searchCustomer( $search ) {

        $search  = str_replace(" ", "", $search);

        $selectFields = [
            "service_customer.*",
            "service_address.address_1",
            "service_address.address_2",
            "service_address.city",
            "service_address.state",
            "service_address.zip"
        ];

        $sql = $this->pdo->select($selectFields)->from("service_customer");
        $sql->leftJoin('service_customer_address', 'service_customer_address.customer_id', '=', 'service_customer.id');
        $sql->leftJoin('service_address', 'service_address.id', '=', 'service_customer_address.address_id');
        $sql->whereLike('service_customer.email', '%'.$search.'%');
        $sql->orWhereLike('service_customer.phone',  '%'.$search.'%');
        $sql->orWhereLike('service_customer.last_name',  '%'.$search.'%');
        $sql->orWhereLike('service_customer.first_name',  '%'.$search.'%');
        $sql->orWhereLike("CONCAT( service_customer.first_name, '', service_customer.last_name )", '%'.$search.'%');

        $stmt = $sql->execute();

        $result = $stmt->fetchAll();

        return $result;
    }

}
