<?php

namespace SalesQuoter\CartLayout;

use SalesQuoter\CustomAbstractCrud;
/**
 *  LayoutColumn Class
 */
class LayoutColumn extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "layout_group_id", "alignment", "`order`");
        $config = array("table" => 'SQ_layout_column', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function checkCloumn ( $layoutGroupId, $columnId )
    {
    
        $sql = $this->pdo->select(['SQ_layout_column.id'])->from('SQ_layout_group');
        $sql->join('SQ_layout_column', 'SQ_layout_group.id', '=', 'SQ_layout_column.layout_group_id');
        $sql->where('SQ_layout_group.id', '=', $layoutGroupId);
        $sql->where('SQ_layout_column.id', '=', $columnId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();    

        if ( sizeof($data) > '0' ){

            return true; 
        } else {
            return false;
        }  

    }

    public function getInsertOrder ( $layoutGroupId = 0 )
    {

        $sql = $this->pdo->select(['SQ_layout_column.order'])->from('SQ_layout_column');
        $sql->where('SQ_layout_column.active', '=', 1);
        $sql->where('SQ_layout_column.layout_group_id', '=', $layoutGroupId);
        $sql->orderBy('SQ_layout_column.order', 'DESC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if ( sizeof($data) > '0' ){
             
            return $data[0]['order'] + 1;
        }else {
            return 0; 
        }
    }
}
