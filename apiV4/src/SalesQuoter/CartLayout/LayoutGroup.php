<?php

namespace SalesQuoter\CartLayout;

use SalesQuoter\CustomAbstractCrud;
/**
 *  LayoutGroup Class
 */
class LayoutGroup extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "layout_id", "number_of_columns", "`order`");
        $config = array("table" => 'SQ_layout_group', 'fields' =>$fields);
        parent::__construct($config);
    }

    public function allLayoutGroup( $layoutId ) 
    {

        $selectArray = array(
           "SQ_layout_group.id",
           "SQ_layout_group.order",
           "SQ_layout_group.number_of_columns"
        );

        $sql = $this->pdo->select($selectArray)->from('SQ_layout_group');
        $sql->where('SQ_layout_group.active', '=', 1);
        $sql->where('SQ_layout_group.layout_id', '=', $layoutId);
        $sql->orderBy('SQ_layout_group.order', 'ASC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        for ( $count = 0; $count < sizeof($data); $count++ ){

              $data[$count]['columns'] = $this-> allLayoutColumn ( $data[$count]['id'] );
        }

        return $data;
    }


    public function allLayoutColumn ( $layoutGroupId = 0 )
    {

        $selectArray = array(
           "SQ_layout_column.id",
           "SQ_layout_column.order",
           "SQ_layout_column.alignment"
        );

        $sql = $this->pdo->select($selectArray)->from('SQ_layout_column');
        $sql->where('SQ_layout_column.active', '=', 1);
        $sql->where('SQ_layout_column.layout_group_id', '=', $layoutGroupId);
        $sql->orderBy('SQ_layout_column.order', 'ASC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        for ( $count = 0; $count < sizeof($data); $count++ ){

              $data[$count]['items'] = $this-> allLayoutItem ( $data[$count]['id'] );
        }

        return $data;      
    }

    public function allLayoutItem ( $layoutColumnId = 0 )
    {

        $selectArray = array(
           "SQ_layout_item.id",
           "SQ_layout_item.component_type",
           "SQ_layout_item.item",
           "SQ_layout_item.label",
           "SQ_layout_item.order",
        );

        $sql = $this->pdo->select($selectArray)->from('SQ_layout_item');
        $sql->where('SQ_layout_item.active', '=', 1);
        $sql->where('SQ_layout_item.layout_column_id', '=', $layoutColumnId);
        $sql->orderBy('SQ_layout_item.order', 'ASC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data; 

    }


    public function checkLayouts ( $layout_id, $id )
    {
    
        $sql = $this->pdo->select(['SQ_layout_group.id'])->from('SQ_layout_group');
        $sql->join('SQ_layout', 'SQ_layout.id', '=', 'SQ_layout_group.layout_id');
        $sql->where('SQ_layout_group.id', '=', $id);
        $sql->where('SQ_layout.id', '=', $layout_id);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();    

        if ( sizeof($data) > '0' ){

            return true; 
        } else {
            return false;
        }  

    }

    public function getInsertOrder ( $layoutId = 0 )
    {

        $sql = $this->pdo->select(['SQ_layout_group.order'])->from('SQ_layout_group');
        $sql->where('SQ_layout_group.active', '=', 1);
        $sql->where('SQ_layout_group.layout_id', '=', $layoutId);
        $sql->orderBy('SQ_layout_group.order', 'DESC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if ( sizeof($data) > '0' ){
             
            return $data[0]['order'] + 1;
        }else {
            return 0; 
        }
    }
}
