<?php

namespace SalesQuoter\CartLayout;

use SalesQuoter\CustomAbstractCrud;
/**
 *  LayoutColumn Class
 */
class LayoutItem extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "layout_column_id", "component_type", "item", "label", "`order`");
        $config = array("table" => 'SQ_layout_item', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function checkItem ( $columnId, $id )
    {
    
        $sql = $this->pdo->select(['SQ_layout_item.id'])->from('SQ_layout_item');
        $sql->join('SQ_layout_column', 'SQ_layout_column.id', '=', 'SQ_layout_item.layout_column_id');
        $sql->where('SQ_layout_column.id', '=', $columnId);
        $sql->where('SQ_layout_item.id', '=', $id);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();    

        if ( sizeof($data) > '0' ){

            return true; 
        } else {
            return false;
        }  

    }

    public function getInsertOrder ( $layoutColumnId = 0 )
    {

        $sql = $this->pdo->select(['SQ_layout_item.order'])->from('SQ_layout_item');
        $sql->where('SQ_layout_item.active', '=', 1);
        $sql->where('SQ_layout_item.layout_column_id', '=', $layoutColumnId);
        $sql->orderBy('SQ_layout_item.order', 'DESC');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if ( sizeof($data) > '0' ){
             
            return $data[0]['order'] + 1;
        }else {
            return 0; 
        }
    }
}
