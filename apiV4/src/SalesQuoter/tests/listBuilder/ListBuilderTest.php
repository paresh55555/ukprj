<?php

namespace SalesQuoter\tests\SalesPerson;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\ListBuilder\Lists;
use SalesQuoter\MysqlConnection;

class ListBuilderTest extends AbstractTestNonPublicMethods
{

    protected $uDB;
    protected $userData;

    public function setUp()
    {
        $lists = new Lists();

        $this->testClass = $lists;
        parent::setup();
    }

    /**
     * @group user3
     */
    public function testDeleteLists()
    {
        $result =  $this->testClass->delete(50);

        print_r($result);
    }


    public function testAvailableQuotes()
    {
        $result =  $this->testClass->getAvailableData("SQ_available_quotes");

        print_r($result);
    }

}
