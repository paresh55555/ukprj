<?php

use SalesQuoter\Systems\Systems;
use SalesQuoter\CustomAbstractCrud;
use SalesQuoter\Validations\Validations;
use SalesQuoter\MysqlConnection;

class CustomAbstractCrudTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
         $_SERVER['SITE_NAME'] = 'panoramic';
    }


    public function tearDown()
    {
    
          //unset($_SERVER['SITE_NAME']);
    }

    
    public function getValidationsObj()
    {

         
        $fields = array("id", "trait_id", "name", "active");
        $config = array("table" => 'SQ_validations', 'fields' =>$fields);

        return new Validations($config);
    }

    
    public function testValidationsGetAll()
    {
    
        $validationsObj = $this->getValidationsObj();
        $all =  $validationsObj->getAll();

        $this->assertEquals(true, is_array($all));
    }


    public function testValidationsGet()
    {
    
        $validationsObj = $this->getValidationsObj();
        $one =  $validationsObj->get(1);

        $this->assertEquals(true, is_array($one));
    }


    public function testValidationsDelete()
    {
    
        $validationsObj = $this->getValidationsObj();
        $deleted =  $validationsObj->delete(1);

        $keys = array_keys($deleted);

        $this->assertEquals(true, in_array("status", $keys));
    }



    public function testCreate()
    {
    
        $config = array('name' => "testValidationName", "trait_id" => 2);

        $validationsObj = $this->getValidationsObj();
        $created =  $validationsObj->create($config);

        $this->assertEquals($created[0]['name'], $config['name']);
    }


    public function getSystemsObj()
    {

         
        $fields = array("id", "name", "active");
        $config = array("table" => 'SQ_systems', 'fields' =>$fields);

        return new Systems($config);
    }



    public function testSystemsGetAll()
    {
    
        $systemsObj = $this->getSystemsObj();
        $all =  $systemsObj->getAll();

        $this->assertEquals(true, is_array($all));
    }



    public function testSystemsGet()
    {
    
        $systemsObj = $this->getSystemsObj();
        $single =  $systemsObj->get(2);

        $this->assertEquals(true, is_array($single));
    }



    public function testSystemsCreate()
    {
    
        $config = array('name' => "testSystemsName");

        $systemsObj = $this->getSystemsObj();
        $created =  $systemsObj->create($config);

        $this->assertEquals($created[0]['name'], $config['name']);
    }



    public function testSystemsDelete()
    {
    
        $systemsObj = $this->getSystemsObj();
        $deleted =  $systemsObj->delete(10);

        $keys = array_keys($deleted);

        $this->assertEquals(true, in_array("status", $keys));
    }
}
