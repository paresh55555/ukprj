<?php
/* unit testing done*/

use SalesQuoter\Company\Company;
use SalesQuoter\Users\User;

class CompanyTest extends \PHPUnit_Framework_TestCase
{

    
    public function testGetUsersType()
    {
    
        $user = new User();
        $allUsers = $user->getUsersFromDealersId(1);

        $this->assertEquals(is_array($allUsers), true);
    }


    public function testGetUsersFields()
    {
    
        $user = new User();
        $allUsers = $user->getUsersFromDealersId(1);

        if (sizeof($allUsers) > 0) {
            $result = in_array('name', array_keys($allUsers[0]));
            $expected = true;
        } else {
            $result = sizeof($result);
            $expected = 0;
        }
      
        $this->assertEquals($result, $expected);
    }
}
