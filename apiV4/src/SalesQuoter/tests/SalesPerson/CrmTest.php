<?php

namespace SalesQuoter\tests\SalesPerson;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\SalesPerson\Crm;
use PipelinerSales\ApiClient\Query\Filter;

class CrmTest extends AbstractTestNonPublicMethods
{

    protected $crm;
    protected $ownwerID = 40721;
    protected $email = "unitTests@salesquoter.com1";
    protected $opportunityName = 'SQTest1000';
    protected $amount = '99.99';
    protected $tempContact;

    public function setUp()
    {
        $crm = new Crm($this->ownwerID);

        $this->testClass = $crm;
        parent::setup();
    }


    /**
     * @group crm2
     */
    public function testDeleteAllOpportunitiesAndContacts()
    {
        $opportunity = $this->getMethod('findOpportunityByName');
        $results = $opportunity->invoke($this->testClass, $this->opportunityName);

        foreach ($results as $entity) {
            $opportunity = $this->getMethod('deleteOpportunityById');
            $opportunity->invoke($this->testClass, $entity->getId());
        }

        $contact = $this->getMethod('findAllContacts');
        $results = $contact->invoke($this->testClass, $this->email);
        
        foreach ($results as $entity) {
            $opportunity = $this->getMethod('deleteContactById');
            $opportunity->invoke($this->testClass, $entity->getId());
        }
    }

    /**
     * @group crm1
     */
    public function testCreateNewSalesQuoterLead()
    {

        $lead = array (
            "firstName" => 'FirstNameTest',
            "lastName" => 'LastNameTest',
            "email" => $this->email,
            "phone" => "412-555-1212",
            "quoteNumber" => $this->opportunityName,
            "amount" => $this->amount
        );

        $opportunityId = $this->testClass->createNewSalesQuoterLead($lead);

        $opportunity = $this->getMethod('deleteOpportunityById');
        $opportunity->invoke($this->testClass, $opportunityId);
    }


    /**
     * @group crm
     * @expectedException RuntimeException
     * @expectedExceptionMessage Valid Contact of: firstName was not passed
     */
    public function testCreateContactFirstNameFail()
    {
        $contact = array ("email" => $this->email);

        $createContact = $this->getMethod('createContact');
        $createContact->invoke($this->testClass, $contact);
    }


    /**
     * @group crm
     * @expectedException RuntimeException
     * @expectedExceptionMessage Valid Contact of: email was not passed
     */
    public function testCreateContactEmailFail()
    {
        $contact = array ("email" => '');

        $createContact = $this->getMethod('createContact');
        $createContact->invoke($this->testClass, $contact);
    }


    /**
     * @group crm
     * @expectedException RuntimeException
     * @expectedExceptionMessage Valid Contact of: lastName was not passed
     */
    public function testCreateContactLastNameFail()
    {
        $contact = array ("email" => $this->email, "firstName" => "yes");

        $createContact = $this->getMethod('createContact');
        $createContact->invoke($this->testClass, $contact);
    }

    /**
     * @group crm
     */
    public function testCreateContact()
    {
        $firstName = "FirstName";
        $lastName = "LastName";

        $contact = array (
            "email" => $this->email,
            "firstName" => $firstName,
            "lastName" => $lastName,
            "phone" => "555-1212"
        );

        $createContact = $this->getMethod('createContact');
        $contactId = $createContact->invoke($this->testClass, $contact);

        $opportunity = $this->getMethod('deleteContactById');
        $opportunity->invoke($this->testClass, $contactId);
    }


    /**
     * @group crm
     */
    public function testCreateOpportunity()
    {

        $firstName = "FirstName";
        $lastName = "LastName";

        $contact = array (
            "email" => $this->email,
            "firstName" => $firstName,
            "lastName" => $lastName,
            "phone" => "555-1212"
        );

        $createContact = $this->getMethod('createContact');
        $contactId = $createContact->invoke($this->testClass, $contact);

        $value = "6000.01";

        $opportunity = array (
            "value" => $value,
            "name" => $this->opportunityName,
            'contactId' => $contactId
        );

        $createOpportunity = $this->getMethod('createOpportunity');
        $pId = $createOpportunity->invoke($this->testClass, $opportunity);

        $opportunity = $this->getMethod('findOpportunityById');
        $matchResults = $opportunity->invoke($this->testClass, $pId);

//        $opportunity = $this->getMethod('deleteOpportunityById');
//        $opportunity->invoke($this->testClass, $id);

        $this->assertEquals($matchResults[0]->getOpportunityValue(), $value);
        $this->assertEquals($matchResults[0]->getOpportunityName(), $this->opportunityName);
        $this->assertEquals($matchResults[0]->getContactRelations()[0]->CONTACT_ID, $contactId);

//        $opportunity = $this->getMethod('deleteContactById');
//        $opportunity->invoke($this->testClass, $contactId);
    }


    /**
     * @group crm
     */
    public function testGetStages()
    {

        $pineLiner = $this->getProperty('pipeliner');

        $results = $pineLiner->stages->get();

        print_r($results);
    }


    /**
     * @group crm8
     */
    public function testOpportunities()
    {

        $pineLiner = $this->getProperty('pipeliner');
//
//        $results = $pineLiner->opportunity->get('');

        Filter::eq('OWNER_ID', 40721)->eq('IS_ARCHIVE', 0)->eq('IS_DELETED', 0)->eq('account_id', 'PY-7FFFFFFF-72C8AE09-D0D4-4453-937A-18167EB4FCEC');

//        $filter = Filter::eq('ID','PY-7FFFFFFF-188011EE-0ED6-4DEA-9933-5B5E610D5B6F');


        $contents = 'YILE CONTENTS';
        $contents = file_get_contents('/tmp/contents');

//        print_r($contents);

        try {
            $data = array(
                'FILENAME' => 'SQ26824.pdf',
                'TYPE' => '12',
                'CONTENT' =>  base64_encode($contents),
                'OPID' => 'PY-7FFFFFFF-188011EE-0ED6-4DEA-9933-5B5E610D5B6F'
            );
            $result = $pineLiner->documents->save($data);
        } catch (PipelinerHttpException $e) {
            echo "Here \n\n\n\n\n\n";
            print_r($e);
        }



//        $results = $pineLiner->opportunities->get($filter);
        print_r($result);
////
//        40721


//        try {
//            $data = array(
//                'FILENAME' => 'document.pdf',
//                'TYPE' => '12',
//                'CONTENT' => 'fileContent'
//            );
//            $result = $pipeliner->documents->save($data);
//        }catch (PipelinerHttpException $e) {
//            // something went wrong
//        }

//        $filter = Filter::eq('OWNER_ID',40721)->eq('IS_ARCHIVE', 0)->eq('IS_DELETED', 0)->eq('account_id', 'PY-7FFFFFFF-72C8AE09-D0D4-4453-937A-18167EB4FCEC');
//        $results = $pineLiner->opportunities->get($filter);

//        $results = $pineLiner->opportunities->get();

//        print_r($results);
        $this->assertTrue(true);
    }
}
