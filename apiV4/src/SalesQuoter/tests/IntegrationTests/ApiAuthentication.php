<?php

namespace SalesQuoter\tests\IntegrationTests;


class ApiAuthentication 
{
    public $userName = "demo";
    public $password = "demo";
    public $apiUrl = "";
    public $allTableId =   array();


    public function __construct()
    {

        $this->apiUrl = "https://".$_SERVER['HOST']."/api/V4";
        $this->userToken = $this->getUserToken();
        
    }

    public function formatResponse( $curlResponse ) {
 
        $response = json_decode($curlResponse['body'], TRUE);

        if ( $response['status'] == 'success' ) {

             if ( sizeof($response['response_data']) > '0' ) {

                  return $response['response_data'][0];
             }else {
                  return array();
             }

        }else{
            return array();
        }  

    }


    public function getSpecificMsg ( $curlResponse, $key ) {

        $response = json_decode($curlResponse['body'], TRUE);

        if ( isset($response[$key])) {

             return $response[$key]; 
        }else{
            return array();
        } 

    }



    public function formatEndpoint($endpoint)
    {

         $allSupportTables =  $this->allTableId;

        foreach ($allSupportTables as $key => $value) {
            $checkExists = strpos($endpoint, $key);
            if ($checkExists !== false) {
                $matchedKey = "{".$key."}";
                $endpoint = str_replace($matchedKey, $value, $endpoint);
            }
        }


         return $endpoint;
    }

    function format_get_response($response)
    {

        unset($response["id"]);
        unset($response["status"]);

         $allSupportTables =  $this->allTableId;

        foreach ($allSupportTables as $key => $value) {
            $allKeys = array_keys($response);

            if (in_array($key, $allKeys)) {
                unset($response[$key]);
            }

            if ( strlen($value) > '0') {
                
            }
        }

         return $response;
    }



    public function getUserToken()
    {
    
        $method = 'POST';
        $header =  array ('Authorization: Basic '.base64_encode($this->userName.':'.$this->password),'content-type: application/json');
        $url = $this->apiUrl."/token";
        $response = $this->loadEndpoint($url, $method, '', $header);
        $body = json_decode($response['body'], true);
      
        if ($body["status"] == "error") {
            echo 'Wrong Email or Password ';
            $this->assertEquals(false, false);
        } else {
            return $body['token'];
        }
    }


    public function callEndpoint($endpoint, $method, $postFields, $header = 'application/json') {

        $url = $this->apiUrl.$endpoint;
        $header =  array (
                'Authorization: Bearer '.$this->userToken,
                'content-type: '.$header
        );

        return $this->loadEndpoint($url, $method, $postFields, $header);
    }

    
    public function loadEndpoint($url, $method, $postFields, $header)
    {

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        return array(
          'body' => $output,
          'info' => $info
        );
    }
}
