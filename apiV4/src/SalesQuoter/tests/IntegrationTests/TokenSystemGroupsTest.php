<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class TokenSystemGroupsTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function testValidToken()
    {
    
        $method = 'POST';
        $header =  array ('Authorization: Basic '.base64_encode('demo:demo'),'content-type: application/json');
        $url = $this->api->apiUrl."/token";
        $response = $this->api->loadEndpoint($url, $method, '', $header);
        $body = json_decode($response['body'], true);
      
        $this->assertEquals($body["status"], "ok");
    }

    public function testInvalidPasswordReset()
    {
    
        // Building invalid Email
        $buildData = ["email" => md5(time())];

        $url = $this->api->apiUrl."/token/reset";
        $response = $this->api->loadEndpoint($url, 'POST', json_encode($buildData), []);
        $body = json_decode($response['body'], true);
      
        $this->assertEquals($body["msg"], "email not found");

        $buildData = [];

        $url = $this->api->apiUrl."/token/pws";
        $response = $this->api->loadEndpoint($url, 'POST', json_encode($buildData), []);
        $body = json_decode($response['body'], true);
      
        $this->assertEquals($body["msg"], "code cannot be empty");
    }

    public function testInValidToken()
    {
    
        $method = 'POST';
        $header =  array ('Authorization: Basic '.base64_encode('test:test1234'),'content-type: application/json');
        $url = $this->api->apiUrl."/token";
        $response = $this->api->loadEndpoint($url, $method, '', $header);
        $body = json_decode($response['body'], true);
      
        $this->assertEquals($body["status"], "error");
    }

   public function buildData() {

        $data = array(
           "name" => "SystemGroup",
        );

        return $data;
    }


    public function testListBuilder () {

        $buildData = $this->buildData();

        $postResponse = $this->api->callEndpoint("/systemGroups", "POST", json_encode($buildData));
        $postData = $this->api->formatResponse( $postResponse );

        $systemGroupId = $postData['id'];

        $getResponse = $this->api->callEndpoint("/systemGroups", "GET", array());
        $responseData = json_decode( $getResponse['body'], true );

        $systemGroupData = $responseData['response_data'];
        
        foreach ($systemGroupData as $groupData) {
            if ( $groupData['id'] == $systemGroupId ) {

                $systemGroupData = $groupData;
            }
        }

        unset ( $systemGroupData['traits'] );
        unset ( $systemGroupData['systems'] );
        unset ( $systemGroupData['active'] );

        $this->assertEquals( $systemGroupData, $postData );

        $deleteResponse = $this->api->callEndpoint("/systemGroups/".$systemGroupId, "DELETE", array());
        $responseData = json_decode( $deleteResponse['body'], TRUE );

        $this->assertEquals ( $responseData , array("status" => "success"));
    }





}
