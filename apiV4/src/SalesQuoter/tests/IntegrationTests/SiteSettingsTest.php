<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class SiteSettingsTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildBrandingData() {

        $menu = array(
           "background_color" => "rgba(20, 20, 20, 1)",
           "first_link_text" => "Return to Site!!!!!!",
           "first_link_color" => "rgba(23, 138, 228, 0.72)",
           "first_link_url" => "https://dev-demo1.rioft.com",
           "second_link_text" => "Contact Us",
           "second_link_color" => "rgba(0, 92, 211, 1)",
           "second_link_url" => "Return to Site!!!!!!",
           "banner_url" => "",
           "active" => 1
        );

        return $menu;

    }


    public function testSiteBranding () {

      $getResponse = $this->api->callEndpoint("/siteBranding", "GET", array());
      $getData = $this->api->getSpecificMsg( $getResponse, "data" );

      $siteBrandingId = $getData[0]['id'];

      $postData = $this->buildBrandingData();
      $postData['id'] = $siteBrandingId;

      $putResponse = $this->api->callEndpoint("/siteBranding", "PUT", json_encode($postData) );
      $putData = $this->api->getSpecificMsg( $putResponse, "data" );


      $getResponse = $this->api->callEndpoint("/siteBranding", "GET", array());
      $getData = $this->api->getSpecificMsg( $getResponse, "data" );

      unset($putData[0]['modified']);
      unset($getData[0]['modified']);

      $this->assertEquals($postData ,$getData[0] );

      $deleteResponse = $this->api->callEndpoint("/siteBranding", "DELETE", array());
      $deleteData = json_decode( $deleteResponse['body'], TRUE );

      $this->assertEquals($deleteData, array( 'status' => 'success' ));
 
    }


    public function testSiteDefaults() {

      $getResponse = $this->api->callEndpoint("/default/site", "GET", array());
      $getData = json_decode( $getResponse['body'], TRUE );

      $sampleData = $getData[0];

      foreach ($getData as $data ) {

         if ( $data['name'] == 'date') {
              
              $sampleData = $data;  
         } 
      }


      $sampleData['value'] = 'uk';

      $putResponse = $this->api->callEndpoint("/default/site/".$sampleData['id'], "PUT", json_encode($sampleData) );
      $putData = json_decode( $putResponse['body'], TRUE );  

      $this->assertEquals($putData, $sampleData);        

    }


}
