<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class PriceChangeTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildData() {

        $data = array(
           "name" => "dimensions",
           "amount" => 0.45,
           "kind" => "percentages",
           "show_on_item" => 0
        );

        return $data;
    }


    public function testPriceChange () {

      $buildData = $this->buildData();
      $cartId = rand(1000,9999);

      $postResponse = $this->api->callEndpoint("/priceChange/cart/$cartId", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $priceChangeId = $postData['id'];

      $getResponse = $this->api->callEndpoint("/priceChange/cart/$cartId/$priceChangeId", "GET", array() );
      $getData = $this->api->formatResponse( $getResponse );
   
      $this->assertEquals ( $postData, $getData );

      $buildData['name'] = "dimensions23";
      $putResponse = $this->api->callEndpoint("/priceChange/cart/$cartId/$priceChangeId", "PUT", json_encode($buildData));
      $putData = $this->api->getSpecificMsg( $putResponse, "response_data" );

      $priceChangeId = $putData[sizeof($putData)-1]['id'];
      $getResponse = $this->api->callEndpoint("/priceChange/cart/$cartId/$priceChangeId", "GET", array() );
      $getData = $this->api->formatResponse( $getResponse );

      unset($getData['cart_id']);
      unset($getData['id']);
      unset($getData['amount']);
      unset($buildData['amount']);

      $this->assertEquals ( $buildData, $getData );


      $deleteResponse = $this->api->callEndpoint("/priceChange/cart/$cartId/$priceChangeId", "DELETE", '');
      $deletData = json_decode( $postResponse['body'], true );

      $this->assertEquals ( $deletData['status'], "success" );

      $getResponse = $this->api->callEndpoint("/priceChange/cart/$cartId/$priceChangeId", "GET", array() );
      $getData = $this->api->formatResponse( $getResponse );
   
      $this->assertEquals ( 0, sizeof($getData) );

      $this->getHistoryItems( $cartId, $priceChangeId );
 
    }


    public function getHistoryItems( $cartId, $priceChangeId ) {

      $getResponse = $this->api->callEndpoint("/priceChange/cart/$cartId/$priceChangeId/history", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $this->assertEquals ( 1, sizeof($getData) );

    }


   


}
