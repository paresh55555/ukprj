<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class ListBuilderTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildData() {

        $menu = array(
           "type" => "quote",
        );

        return $menu;

    }

   public function buildPutData() {

        $menu = array(
           "name" => "test",
           "statuses" => array(array("name" => "View All", "order" => 0)),
           "columns" =>  array(array("name" => "first_name", "order" => 0))
        );

        return $menu;
    }


    public function testListBuilder () {

        $buildData = $this->buildData();

        $postResponse = $this->api->callEndpoint("/listBuilder", "POST", json_encode($buildData));
        $postData = $this->api->getSpecificMsg( $postResponse, "data" );

        $builderId = $postData[0]['id'];

        unset($postData[0]['name']);
        unset($postData[0]['data']);
        unset($postData[0]['active']);
        unset($postData[0]['id']);

        $this->assertEquals ( $buildData, $postData[0] );

        $getResponse = $this->api->callEndpoint("/listBuilder/".$builderId, "GET", array());
        $responseData = json_decode( $getResponse['body'] );

        $this->assertEquals ( $responseData, array() );


        $buildData = $this->buildPutData();

        $putResponse = $this->api->callEndpoint("/listBuilder/".$builderId, "PUT", json_encode($buildData));
        
        $getResponse = $this->api->callEndpoint("/listBuilder/".$builderId, "GET", array());
        $responseData = json_decode( $getResponse['body'], TRUE );

        $this->assertEquals( $responseData, $buildData );

        $deleteResponse = $this->api->callEndpoint("/listBuilder/".$builderId, "DELETE", array());
        $responseData = json_decode( $deleteResponse['body'], TRUE );

        $this->assertEquals ( $responseData , array("status" => "success"));
    }





}
