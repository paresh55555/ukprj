<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class OrderBuilderTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildData() {

        $menu = array(
           "name" => "Integration Test",
           "kind" => "order"
        );

        return $menu;

    }

   public function buildLayoutsData() {

        $menu = array(
           "number_of_columns" => 3,
        );

        return $menu;
    }


    public function testQuoteBuilder () {

      $buildData = $this->buildData();

      $postResponse = $this->api->callEndpoint("/orderBuilder", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $builderId = $postData['id'];

      $getResponse = $this->api->callEndpoint("/orderBuilder", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      unset( $matchData['id'] );

      $this->assertEquals (  $matchData, $buildData );

      $putData['name'] = 'test';
      $putData['id'] = $builderId;

      $putResponse = $this->api->callEndpoint("/quoteBuilder", "PUT", json_encode($putData));
      $putData = $this->api->formatResponse( $putResponse );


      $getResponse = $this->api->callEndpoint("/orderBuilder", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      $this->assertEquals( $matchData, $putData );

      $this->quoteBuilderStatus( $builderId );
 
    }


    public function quoteBuilderStatus( $builderId ) {
    
      $buildData = $this->buildLayoutsData();

      $postResponse = $this->api->callEndpoint("/quoteBuilder/$builderId/layouts", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $statusId = $postData['id'];


      $getResponse = $this->api->callEndpoint("/quoteBuilder/$builderId/layouts", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      unset( $matchData['columns'] );
      unset( $postData['layout_id'] );

      $this->assertEquals (  $matchData, $postData );

      $putData['order'] = 'test';

      $putResponse = $this->api->callEndpoint("/quoteBuilder/$builderId/layouts/".$statusId, "PUT", json_encode($putData));
      $putData = $this->api->formatResponse( $putResponse );


      $getResponse = $this->api->callEndpoint("/quoteBuilder/$builderId/layouts", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      unset( $matchData['columns'] );
      unset( $putData['layout_id'] );

      $this->assertEquals (  $matchData, $putData );
    }


}
