<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class CartBuilderTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildData() {

        $menu = array(
           "name" => "Integration Test",
           "kind" => "cart"
        );

        return $menu;

    }

   public function buildLayoutsData() {

        $menu = array(
           "number_of_columns" => 3,
        );

        return $menu;
    }


    public function testQuoteBuilder () {

      $buildData = $this->buildData();

      $postResponse = $this->api->callEndpoint("/cart", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $builderId = $postData['id'];

      $getResponse = $this->api->callEndpoint("/cart", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      unset( $matchData['id'] );

      $this->assertEquals (  $matchData, $buildData );

      $putData['name'] = 'test';
      $putData['id'] = $builderId;

      $putResponse = $this->api->callEndpoint("/cart", "PUT", json_encode($putData));
      $putData = $this->api->formatResponse( $putResponse );


      $getResponse = $this->api->callEndpoint("/cart", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      $this->assertEquals( $matchData, $putData );

      $this->quoteBuilderStatus( $builderId );
 
    }


    public function quoteBuilderStatus( $builderId ) {
    
      $buildData = $this->buildLayoutsData();

      $postResponse = $this->api->callEndpoint("/carts/".$builderId."/layouts", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $statusId = $postData['id'];


      $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      unset( $matchData['columns'] );
      unset( $postData['layout_id']  );

      $this->assertEquals (  $matchData, $postData );

      $putData['order'] = 'test';

      $putResponse = $this->api->callEndpoint("/carts/$builderId/layouts/".$statusId, "PUT", json_encode($putData));
      $putData = $this->api->formatResponse( $putResponse );


      $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $matchData = $getData[sizeof($getData)-1];

      unset( $matchData['columns'] );
      unset( $putData['layout_id'] );

      $this->assertEquals (  $matchData, $putData );

      $this->cartBuilderColumns ( $builderId, $statusId );
    }


    public function cartBuilderColumns( $builderId, $layoutsId ) {


      $buildData = array( "alignment" => "top" );

      $postResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $columnId = $postData['id'];

      $columnIdFound = false;

      $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      foreach ( $getData as $cartLayouts ) {

        if ( $cartLayouts['id'] == $layoutsId ) {

            if ( $cartLayouts['columns'][0]['id'] == $columnId ) {

                  $columnIdFound = true;
            }
        }

      }

     $this->assertEquals (  $columnIdFound, true );

     $this->cartBuilderColumnsItems ( $builderId, $layoutsId, $columnId );

    }


    public function cartBuilderColumnsItems( $builderId, $layoutsId, $columnId ) {

      $buildData = array( "component_type" => "top", "item" => "1", "label" => "3" );

      $postResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId/items", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $itemId = $postData['id'];


      $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId/items", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      unset($postData['layout_column_id']);

      $this->assertEquals (  $getData[0], $postData );


      $buildData = array( "component_type" => "bottom", "item" => "22", "label" => "23" );

      $putResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId/items/$itemId", "PUT", json_encode($buildData));
      $putData = $this->api->formatResponse( $putResponse );

      $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId/items", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      unset($putData['layout_column_id']);

      $this->assertEquals (  $getData[0], $putData );

      $this->checkDelete( $builderId, $layoutsId, $columnId, $itemId );

    }


    public function checkDelete( $builderId, $layoutsId, $columnId, $itemId ){

       $deleteResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId/items/$itemId", "DELETE", '');
       $response = json_decode( $deleteResponse['body'], TRUE );

       $this->assertEquals (  $response['status'], "success");

       $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId/items", "GET", array() );
       $getData = json_decode( $getResponse['body'], TRUE );

       $this->assertEquals ( sizeof($getData), 0);


       $deleteResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId/column/$columnId", "DELETE", '');
       $response = json_decode( $deleteResponse['body'], TRUE );

       $this->assertEquals (  $response['status'], "success");

       $deleteResponse = $this->api->callEndpoint("/carts/$builderId/layouts/$layoutsId", "DELETE", '');
       $response = json_decode( $deleteResponse['body'], TRUE );

       $this->assertEquals (  $response['status'], "success");

       $getResponse = $this->api->callEndpoint("/carts/$builderId/layouts", "GET", array() );
       $getData = json_decode( $getResponse['body'], TRUE );

       $this->assertEquals ( sizeof($getData), 0);


       $deleteResponse = $this->api->callEndpoint("/cart/$builderId", "DELETE", '');
       $response = json_decode( $deleteResponse['body'], TRUE );

       $this->assertEquals (  $response['status'], "success");

       $getResponse = $this->api->callEndpoint("/cart/$builderId", "GET", array() );
       $getData = json_decode( $getResponse['body'], TRUE );

       $this->assertEquals ( $getData['status'], "error");

    }


}
