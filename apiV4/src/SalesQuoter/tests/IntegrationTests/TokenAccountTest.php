<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class TokenAccountTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function testLoginToken()
    {
    
        $method = 'POST';
        $header =  array ('Authorization: Basic '.base64_encode('demo:demo'),'content-type: application/json');
        $url = $this->api->apiUrl."/token/login";
        $response = $this->api->loadEndpoint($url, $method, '', $header);
        $body = json_decode($response['body'], true);
      
        $this->assertEquals(is_array($body["account"]["quotes"]["quote_builder_id"]), true);
        $this->assertEquals(is_array($body["account"]["quotes"]["list_builder_id"]), true);
        $this->assertEquals(is_array($body["account"]["quotes"]["cart_builder_id"]), true);
        $this->assertEquals(is_array($body["account"]["orders"]["order_builder_id"]), true);
        $this->assertEquals(is_array($body["account"]["orders"]["list_builder_id"]), true);
        $this->assertEquals(is_array($body["account"]["orders"]["cart_builder_id"]), true);

    }


}
