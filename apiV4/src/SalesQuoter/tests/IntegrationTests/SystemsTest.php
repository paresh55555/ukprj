<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class SystemsTest extends \PHPUnit_Framework_TestCase
{
    public $allTableId = array();
    public $api;


    public function __construct()
    {
        $this->api = new ApiAuthentication();
        $this->setUpTable();     
    }


    public function buildMenuData() {

        $menu = array();
        $traits = array();

        $traits[] = array(
          "name" => "title",
          "type" => "component",
          "value" => "test",
          "visible" => 1
        ); 

        $menu[] = array(
           "name" => "Menu",
           "type" => "menu",
           "traits" => $traits
        );

        return $menu;

    }


    public function testSystems () {

       $postData = array(
          "grouped_under" => $this->allTableId['systemGroupId'],
          "name" => "Door System"
       );

       $postResponse = $this->api->callEndpoint("/systems", "POST", json_encode($postData) );
       $postData = $this->api->formatResponse( $postResponse );

       $this->systemId = $postData['id']; 

       $getResponse = $this->api->callEndpoint("/systems/".$postData['id'], "GET", array());
       $getData = $this->api->formatResponse( $getResponse );
       unset($getData['traits']); 
 
       // check POST data is matching with GET

       $this->assertEquals($postData , $getData);

       $putData = array(
          "my_order" => 10, 
          "required" => 1,
          "multiple" => 1,
          "system_id"=> $postData['id'],
          "name" => "Updated Door System"
       );

       $putResponse = $this->api->callEndpoint("/systems/".$postData['id'], "PUT", json_encode($putData) );
       $putData = $this->api->formatResponse( $putResponse );

       $getResponse = $this->api->callEndpoint("/systems/".$postData['id'], "GET", array());
       $getData = $this->api->formatResponse( $getResponse );
       unset($getData['traits']); 

       // check PUT data is matching with GET

       $this->assertEquals($putData , $getData);

       $this->systemsMenu( $this->systemId );

       $this->systemsPanelRanges( $this->systemId );

       $this->samplePrice( $this->systemId );

       $this->sampleParts( $this->systemId );

       $this->checkDuplicateSystems ( $this->systemId );

        $this->publishSystems ( $this->systemId );

       //now checking DELETE with GET

       //$deleteResponse = $this->callEndpoint("/systems/".$postData['id'], "DELETE", array());
       //$getResponse = $this->callEndpoint("/systems/".$postData['id'], "GET", array());
       
       //$getData = $this->getSpecificMsg( $getResponse, "message" );
       
       //$this->assertEquals($getData, "System Not Exists");
    }


    public function systemsMenu( $systemId ){

       $postData = $this->buildMenuData();
       $this->allTableId['systemId'] = $systemId;

       $postResponse = $this->api->callEndpoint("/systems/".$this->allTableId['systemId']."/components/menu", "POST", json_encode($postData) );
       $responseData = $this->api->formatResponse( $postResponse );

       $this->allTableId['menuId'] = $responseData['id']; 

       $getResponse = $this->api->callEndpoint("/systems/".$this->allTableId['systemId']."/components/".$this->allTableId['menuId'], "GET", array());
       $getData = $this->api->formatResponse( $getResponse );

       unset($getData['traits'][0]['id']);
       unset($getData['traits'][0]['component_id']); 

       $this->assertEquals($postData[0]['traits'][0] , $getData['traits'][0]);

       $this->systemComponents( $systemId, $this->allTableId['menuId'] );

       $this->componentsOptions( $systemId, $this->allTableId['menuId'] );

    }


    public function systemComponents( $systemId, $menuId ) {

       $typesResponse = $this->api->callEndpoint("/componentTypes", "GET", array()); 
       $componentTypes = json_decode($typesResponse['body'], TRUE);

       $postData = array(
           "name" => $componentTypes[0]['name'],
           "type" => $componentTypes[0]['name'],
       );

       $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/menu/".$menuId, "POST", json_encode($postData) );
       $responseData = $this->api->formatResponse( $postResponse );
       $menuComponentId = $responseData['id'];

       $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/menu/".$menuId, "GET", array());
       $getData = json_decode( $getResponse['body'], TRUE );

       $componentId = $responseData['id'];

       $getComponents =  $getData['response_data'][0];

       for ( $count = 0; $count < sizeof($getData['response_data']); $count++ ){

             if ( $getData['response_data'][$count]['name'] == $componentTypes[0]['name']) {

                  $getComponents =  $getData['response_data'][$count];
             }
              
       }

       unset($getComponents['traits']);
       unset($getComponents['options']);

       $this->assertEquals($responseData, $getComponents );  

       $this->systemComponentsTraits( $systemId, $menuId, $componentId );

    }


    public function systemComponentsTraits( $systemId, $menuId, $componentId ){

       $postData = array();
       $postData[] = array(
           "name" => 'title',
           "type" => 'component',
           "value" => "Dimensions"
       );

       $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/multipleTraits", "POST", json_encode($postData) );
       $responseData = $this->api->formatResponse( $postResponse );

       $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId, "GET", array());
       $getData = $this->api->formatResponse( $getResponse );
      
       $this->assertEquals($responseData, $getData['traits'][0] );

       $this->systemsComponentsParts ( $systemId, $componentId );
       $this->systemsComponentsCosts ( $systemId, $componentId );


    }


    public function systemsComponentsCosts ( $systemId, $componentId ) {

      $postData = array(
          "name" => '12',
          "amount" => 4,
          "cost_type" => 'PanelCost',
          "taxable" => 1,
          "markup" => 1,
          "swings" => 0
      );

      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/costs", "POST", json_encode($postData) );
      $postResponseData = $this->api->formatResponse( $postResponse );

      $costsId = $postResponseData['id'];

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/costs", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      $this->assertEquals($postResponseData, $getData);

      $putData = array(
          "name" => '1',
          "amount" => 40,
          "cost_type" => 'PanelCost',
          "taxable" => 0,
          "markup" => 1,
          "swings" => 1
      );

      $putResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/costs/".$costsId, "PUT", json_encode($putData) );
      $putResponseData = $this->api->formatResponse( $putResponse );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/costs", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      $this->assertEquals($putResponseData, $getData);

      $deleteResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/costs/".$costsId, "DELETE", array());
      $deleteData = json_decode( $deleteResponse['body'], TRUE);

      $this->assertEquals($deleteData, array('status' => 'success'));

    }


    public function systemsComponentsParts ( $systemId, $componentId ) {

      $postData = array(
          "on_cut_sheet" => 1,
          "size_formula" => 4,
          "quantity_formula" => 4,
          "name" => 1,
          "number" => 4,
      );

      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/parts", "POST", json_encode($postData) );
      $postResponseData = $this->api->formatResponse( $postResponse );

      $partsId = $postResponseData['id'];

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/parts", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      unset($getData['costs']);

      $this->assertEquals($postResponseData, $getData);

      $putData = array(
          "on_cut_sheet" => 1,
          "size_formula" => 1,
          "quantity_formula" => 1,
          "name" => 1,
          "number" => 1,
      );

      $putResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/parts/".$partsId, "PUT", json_encode($putData) );
      $putResponseData = $this->api->formatResponse( $putResponse );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/parts", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      unset($getData['costs']);

      $this->assertEquals($putResponseData, $getData);

      $deleteResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/parts/".$partsId, "DELETE", array());
      $deleteData = json_decode( $deleteResponse['body'], TRUE);

      $this->assertEquals($deleteData, array('status' => 'success'));

    }

    
    public function systemsPanelRanges ( $systemId ) {

      $postData = array(
          "panels" => 2,
          "min_height" => 4,
          "max_height" => 23,
          "min_width" => 1,
          "max_width" => 4,
      );

      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/panelRanges", "POST", json_encode($postData) );
      $postResponseData = $this->api->getSpecificMsg( $postResponse, "data" );

      $panelRangesId = $postResponseData[0]['id'];

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/panelRanges", "GET", array());
      $getData = $this->api->getSpecificMsg( $getResponse, "data"  );

      $this->assertEquals($postResponseData, $getData);

      $putData = array(
          "panels" => 1,
          "min_height" => 1,
          "max_height" => 1,
          "min_width" => 1,
          "max_width" => 1,
      );

      $putResponse = $this->api->callEndpoint("/systems/".$systemId."/panelRanges/".$panelRangesId, "PUT", json_encode($putData) );
      $putResponseData = $this->api->getSpecificMsg( $putResponse, "data" );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/panelRanges", "GET", array());
      $getData = $this->api->getSpecificMsg( $getResponse, "data"  );

      $this->assertEquals($putResponseData, $getData);

      $deleteResponse = $this->api->callEndpoint("/systems/".$systemId."/panelRanges/".$panelRangesId, "DELETE", array());
      $deleteData = json_decode( $deleteResponse['body'], TRUE);

      $this->assertEquals($deleteData, array('status' => 'success'));

    }


    public function samplePrice( $systemId ) {

       
      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/sampleDoorPrice", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      $samplePriceId = $getData['id'];
 
      $postData = $this->buildPartsData( $samplePriceId );

      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$samplePriceId."/multipleTraits", "POST", json_encode($postData) );
      $postResponseData = json_decode($postResponse['body'], TRUE );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/sampleDoorPrice", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      $this->assertEquals( $postResponseData['response_data'], $getData['traits'] );

    }

    public function sampleParts( $systemId ) {

       
      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/sampleDoorParts", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      $samplePartsId = $getData['id'];
 
      $postData = $this->buildPartsData( $samplePartsId );

      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$samplePartsId."/multipleTraits", "POST", json_encode($postData) );
      $postResponseData = json_decode($postResponse['body'], TRUE );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/sampleDoorParts", "GET", array());
      $getData = $this->api->formatResponse( $getResponse );

      $this->assertEquals( $postResponseData['response_data'], $getData['traits'] );

    }

    public function buildPartsData( $componentId ) {

      $data = array(
        array('name' => 'width', 'value' => 10, 'type' => 'component', 'component_id' => $componentId ),
        array('name' => 'height', 'value' => 12, 'type' => 'component', 'component_id' => $componentId ),
        array('name' => 'swings', 'value' => 13, 'type' => 'component', 'component_id' => $componentId ),
        array('name' => 'panels', 'value' => 14, 'type' => 'component', 'component_id' => $componentId ),
      );

      return $data;

    }


    public function componentsOptions ( $systemId, $menuId ) {

       $postData = array(
           "name" => "GlassOptions",
           "type" => "GlassOptions",
       );

       $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/menu/".$menuId, "POST", json_encode($postData) );
       $responseData = $this->api->formatResponse( $postResponse );
       $componentId = $responseData['id'];


       $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/menu/".$menuId, "GET", array());
       $getData = json_decode( $getResponse['body'], TRUE );

       $optionsData = $getData['response_data'][0];

       foreach ( $getData['response_data'] as $options ) {

             if ( $options['name'] == 'GlassOptions' )  {

                  $optionsData = $options;
             }          
       }

       unset($optionsData['traits']);
       unset($optionsData['options']);

       $this->assertEquals( $responseData, $optionsData );


       $postData = array(
           "name" => "GlassOptionsOption",
           "allowDeleted" => 1,
       );

       $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/options", "POST", json_encode($postData) );
       $responseData = $this->api->formatResponse( $postResponse );
       $optionId = $responseData['id'];

       $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/menu/".$menuId, "GET", array());
       $getData = json_decode( $getResponse['body'], TRUE );

       $optionsData = $getData['response_data'][0];

       foreach ( $getData['response_data'] as $options ) {

             if ( $options['name'] == 'GlassOptions' )  {

                  $optionsData = $options;
             }          
       }


      $this->assertEquals( $responseData['name'], $optionsData['options'][0]['name'] );

      $postData = "height=50&width=60&type=option&option_id=$optionId&system_id=$systemId&component_id=$componentId";
      $postData .= "&trait_id=1&file=data:image/png;base64,".base64_encode(file_get_contents('https://www.gstatic.com/webp/gallery/1.sm.jpg')); 

      $postResponse = $this->api->callEndpoint("/image", "POST", $postData, 'application/x-www-form-urlencoded' );
      $responseData = json_decode( $postResponse['body'], TRUE); 

      $this->assertEquals( $responseData['status'], 'success' );

      $traitsData = array(
         array( "name" => "title" , "value" => "test", "visible" => 1 ),
         array( "name" => "imgUrl" , "value" => $responseData['imgUrl'] , "visible" => 1 ),   
      );


      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/components/".$componentId."/options/".$optionId."/multipleTraits", "POST", json_encode($traitsData) );
      $responseData = $this->api->formatResponse( $postResponse );


      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/components/menu/".$menuId, "GET", array());
      $getData = json_decode( $getResponse['body'], TRUE );

      $optionsData = $getData['response_data'][0];

      foreach ( $getData['response_data'] as $options ) {

             if ( $options['name'] == 'GlassOptions' )  {

                  $optionsData = $options;
             }          
      }

      $this->assertEquals( sizeof($optionsData['options'][0]['traits']), 2 );


    }

    public function checkDuplicateSystems ( $systemId ) {

      $postData = array( "name" => "System Group" );
      $postResponse = $this->api->callEndpoint("/systems/".$systemId."/duplicate", "POST", json_encode($postData) );
      $postResponseData = $this->api->formatResponse( $postResponse );

      $newSystemId = $postResponseData['id'];

      $getResponse = $this->api->callEndpoint("/systems/".$newSystemId, "GET", array());
      $newSystemData = $this->api->formatResponse( $getResponse );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId, "GET", array());
      $oldystemData = $this->api->formatResponse( $getResponse );

      $this->assertEquals( $newSystemData['grouped_under'], $oldystemData['grouped_under']);
      $this->assertEquals( sizeof($newSystemData['traits']), sizeof($oldystemData['traits']));
    }

    public function publishSystems ( $systemId ) {

      $postData = array( "system_id" => $systemId, "name" => "Door System" );
      $postResponse = $this->api->callEndpoint("/systems/publish", "POST", json_encode($postData) );
      $postResponseData = json_decode( $postResponse['body'], TRUE );

      $this->assertEquals( $postResponseData['status'], "success");

      $getResponse = $this->api->callEndpoint("/systems/".$systemId, "GET", array());
      $adminData = $this->api->formatResponse( $getResponse );

      $getResponse = $this->api->callEndpoint("/systems/".$systemId."/ux", "GET", array());
      $uxData = $this->api->formatResponse( $getResponse );

      unset( $adminData['my_order']);
      unset( $adminData['required']);
      unset( $adminData['multiple']);
      unset( $uxData['options']);

      $this->assertEquals( $adminData, $uxData);

    }


    public function setUpTable()
    {

        $getResponse = $this->api->callEndpoint("/systemGroups", "GET", array());
        $getData = $this->api->formatResponse( $getResponse );

        if ( sizeof($getData) > '0' ) {

             $data["systemGroupId"] = $getData['id'];
        }else {

            $postData = array(
                "grouped_under" => 0,
                "name" => "test SystemGroup",
            );

            $postResponse = $this->api->callEndpoint("/systemGroups", "POST", json_encode($postData) );
            $postData = $this->api->formatResponse( $postResponse );

            $data["systemGroupId"] = $postData['id'];
        }

        $this->allTableId = $data;
    }



}
