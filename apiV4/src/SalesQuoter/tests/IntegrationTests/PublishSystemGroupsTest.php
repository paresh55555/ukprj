<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class PublishSystemGroupsTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }

   public function buildData() {

        $data = array(
           "name" => "System Group",
        );

        return $data;
    }


    public function testPublishSystemGroups () {

        $buildData = $this->buildData();

        $postResponse = $this->api->callEndpoint("/systemGroups/publish", "POST", json_encode($buildData));

        $getResponse = $this->api->callEndpoint("/systemGroups", "GET", array());
        $responseData = $this->api->formatResponse( $getResponse );
        $backEndSystems = $responseData['systems'];

        $getResponse = $this->api->callEndpoint("/systemGroups/ux", "GET", array());
        $responseData = $this->api->formatResponse( $getResponse );
        $frontEndSystems = $responseData['systems'];

        $this->assertEquals ( $backEndSystems , $frontEndSystems );
    }


    public function testPublishSystems() {


    }





}
