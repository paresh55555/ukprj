<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class PreMadeGroupsTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }

    public function testGetPremadeSystemGroups()
    {
    
        $getResponse = $this->api->callEndpoint("/systemGroups/preMade", "GET", array());
        $getData = $this->api->getSpecificMsg( $getResponse, "response_data" );

        $doorSystemFound = false;
        $windwSystemFound = false;

        $doorGroupId = $getData[0]['id'];
        $windowGroupId = $getData[1]['id'];

        foreach ($getData as $systemGroups) {
           
           if ( $systemGroups['name'] == 'PreMadeDoorSystem' && $systemGroups['type'] == 'systemGroup' ) {

                $doorSystemFound = true;
                $doorGroupId = $systemGroups['id'];
           }

           if ( $systemGroups['name'] == 'PreMadeWindowSystem' && $systemGroups['type'] == 'systemGroup' ) {

                $windwSystemFound = true;
                $windowGroupId = $systemGroups['id'];
           }
        }


        $this->assertEquals ( $doorSystemFound , true);
        $this->assertEquals ( $windwSystemFound , true);

        $this->PostPremadeSystemGroups( "door", $doorGroupId );
        $this->PostPremadeSystemGroups( 'window', $windowGroupId );

    }


    public function PostPremadeSystemGroups ( $type, $pId = 0  ) {

        $buildData = array(
            'name' => "PreMade".ucwords($type)."SystemCopy" ,
            'grouped_under' => $pId
        );

        $postResponse = $this->api->callEndpoint("/systems/".$type, "POST", json_encode($buildData));
        $postData = $this->api->formatResponse( $postResponse );

        $systemId = $postData['id'];

        $getResponse = $this->api->callEndpoint("/systemGroups/preMade", "GET", array());
        $systemGroupData = $this->api->getSpecificMsg( $getResponse, "response_data" );
        
        $systemsFound = false;
        $systemGroupName = "PreMade".ucwords($type)."System";

        foreach ($systemGroupData as $groupData) {
            if ( $groupData['name'] == $systemGroupName ) {

                 foreach ($groupData['systems'] as $systems) {
                    if ( $systems['id'] == $systemId ){

                         $systemsFound = true;
                    }
                 }
            }
        }


        $this->assertEquals ( $systemsFound , true );
    }





}
