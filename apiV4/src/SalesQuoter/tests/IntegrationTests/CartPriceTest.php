<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class CartPriceTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildData() {

        $data = array(
           "system_id" => "100",
           "quantity" => 1,
           "total" => 50.00,
           "sub_total" => 1000.00,
           "component_values" => array( 
              "name" => "test",
              "amount" => 50
            )
        );

        return $data;
    }

   public function buildLayoutsData() {

        $menu = array(
           "number_of_columns" => 3,
        );

        return $menu;
    }


    public function testCarts () {

      $buildData = $this->buildData();
      $cartId = rand(1000,9999);

      $postResponse = $this->api->callEndpoint("/carts/$cartId", "POST", json_encode($buildData));
      $postData = $this->api->formatResponse( $postResponse );

      $priceId = $postData['id'];

      $getResponse = $this->api->callEndpoint("/carts/$cartId/$priceId", "GET", array() );
      $getData = $this->api->formatResponse( $getResponse );
   
      unset( $getData['price_changes'] );
      $getData['component_values'] = json_encode( $getData['component_values'] );

      $this->assertEquals ( $postData, $getData );

      $buildData['quantity'] = 2;
      $buildData['total'] = 200;

      $putResponse = $this->api->callEndpoint("/carts/$cartId/$priceId", "PUT", json_encode($buildData));
      $putData = $this->api->formatResponse( $putResponse );

      $getResponse = $this->api->callEndpoint("/carts/$cartId/$priceId", "GET", array() );
      $getData = $this->api->formatResponse( $getResponse );

      unset( $getData['price_changes'] );
      $getData['component_values'] = json_encode( $getData['component_values'] );

      $this->assertEquals ( $putData, $getData );


      $putResponse = $this->api->callEndpoint("/carts/$cartId/$priceId", "DELETE", '');
      $putData = $this->api->formatResponse( $postResponse );

      $getResponse = $this->api->callEndpoint("/carts/$cartId/$priceId", "GET", array() );
      $getData = $this->api->formatResponse( $getResponse );

      $this->assertEquals (sizeof($getData), 0);

      $this->getHistoryItems( $cartId, $priceId );
 
    }

    public function getHistoryItems( $cartId, $priceId ) {

      $getResponse = $this->api->callEndpoint("/carts/$cartId/$priceId/history", "GET", array() );
      $getData = json_decode( $getResponse['body'], TRUE );

      $this->assertEquals ( 3, sizeof($getData) );

    }


   


}
