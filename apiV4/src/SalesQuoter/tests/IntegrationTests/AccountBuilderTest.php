<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class AccountBuilderTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


   public function buildPostData() {

        $menu = array(
           "name" => "test",
           "quotes" => array("quote_builder_id" => 0, "list_builder_id" => 0, "cart_builder_id" => 0 ),
           "orders" => array("order_builder_id" => 0, "list_builder_id" => 0, "cart_builder_id" => 0),
        );

        return $menu;
    }

    public function buildPutData() {

        $menu = array(
           "name" => "test2",
           "quotes" => array("quote_builder_id" => 1, "list_builder_id" => 2, "cart_builder_id" => 4 ),
           "orders" => array("order_builder_id" => 1, "list_builder_id" => 3, "cart_builder_id" => 4),
        );

        return $menu;
    }


    public function testBuilders() {

        $getResponse = $this->api->callEndpoint("/accounts/builders", "GET", array());
        $responseData = json_decode( $getResponse['body'], TRUE );

        $this->assertEquals ( is_array($responseData['quotes']['lists_builders']), true );
        $this->assertEquals ( is_array($responseData['quotes']['cart_builders']), true );
        $this->assertEquals ( is_array($responseData['quotes']['quote_builders']), true );
        $this->assertEquals ( is_array($responseData['orders']['lists_builders']), true );
        $this->assertEquals ( is_array($responseData['orders']['cart_builders']), true );
        $this->assertEquals ( is_array($responseData['orders']['order_builders']), true );

    }


    public function testListBuilder () {

        $buildData = $this->buildPostData();

        $postResponse = $this->api->callEndpoint("/accounts", "POST", json_encode($buildData));
        $postData = $this->api->getSpecificMsg( $postResponse, "data" );

        $accountId = $postData[0]['id'];

        $getResponse = $this->api->callEndpoint("/accounts/".$accountId, "GET", array());
        $responseData = $this->api->getSpecificMsg( $getResponse, "data" );

        $this->assertEquals ( $postData, $responseData );


        $buildData = $this->buildPutData();
        $buildData['id'] = $accountId;

        $postResponse = $this->api->callEndpoint("/accounts/".$accountId, "PUT", json_encode($buildData));
        $postData = $this->api->getSpecificMsg( $postResponse, "data" );

        $accountId = $postData[0]['id'];

        $getResponse = $this->api->callEndpoint("/accounts/".$accountId, "GET", array());
        $responseData = $this->api->getSpecificMsg( $getResponse, "data" );

        $this->assertEquals ( $buildData, $responseData[sizeof($responseData) - 1] );
        

        $deleteResponse = $this->api->callEndpoint("/accounts/".$accountId, "DELETE", array());
        $responseData = json_decode( $deleteResponse['body'], TRUE);

        $this->assertEquals ( $responseData, array("status" => "success") );

    }



}
