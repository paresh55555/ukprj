<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class usersTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function buildUserData() {

        $menu = array(
           "background_color" => "rgba(20, 20, 20, 1)",
           "first_link_text" => "Return to Site!!!!!!",
           "first_link_color" => "rgba(23, 138, 228, 0.72)",
           "first_link_url" => "https://dev-demo1.rioft.com",
           "second_link_text" => "Contact Us",
           "second_link_color" => "rgba(0, 92, 211, 1)",
           "second_link_url" => "Return to Site!!!!!!",
           "banner_url" => "",
           "active" => 1
        );

        return $menu;

    }

    public function getPermissions( $getData ) {

      foreach ( $getData['response_data'] as $permissions ) {
       
          $permissionsArray = json_decode($permissions['permissions'], TRUE);

          foreach ($permissionsArray as $key => $value) {
           
             if ( $key == 'admin' ) {

                  return $permissions['id'];
             }
          }
      }

      return $getData['response_data'][0]['id'];

    }


    public function testUsers () {

      $getResponse = $this->api->callEndpoint("/permissions", "GET", array());
      $getData = json_decode( $getResponse['body'], TRUE );
      
      $postData = array(
        "email" => "niloy.cste@gmail.com",
        "password" => "12345",
        "name" => "tarek",
        "discount" => 0,
        "anytimeQuoteEdits" => 1,
        "salesAdmin" => "on",
        "prefix" => "1",
        "type" => "salesPerson",
        "permissions_group_id" => $this->getPermissions( $getData )
      );


      $postResponse = $this->api->callEndpoint("/users", "POST", json_encode($postData) );
      $responseData = json_decode( $postResponse['body'], TRUE );


      $getResponse = $this->api->callEndpoint("/users/".$responseData['id'], "GET", array());
      $getResponseData = json_decode( $getResponse['body'], TRUE );

      unset($getResponseData['status']);
      unset($getResponseData['companyName']);
      unset($getResponseData['id']);

      $this->assertEquals($getResponseData ,$postData);


      $putData = array(
        "email" => "niloy.cste@gmail.com1",
        "password" => "1145",
        "name" => "tarek",
        "discount" => 11,
        "anytimeQuoteEdits" => 1,
        "salesAdmin" => "on",
        "prefix" => "1",
        "type" => "salesPerson",
        "permissions_group_id" => $this->getPermissions( $getData ),
        "id" => $responseData['id']
      );


      $postResponse = $this->api->callEndpoint("/users", "PUT", json_encode($putData) );
      $responseData = json_decode( $postResponse['body'], TRUE );

      $getResponse = $this->api->callEndpoint("/users/".$responseData['id'], "GET", array());
      $getResponseData = json_decode( $getResponse['body'], TRUE );

      unset($getResponseData['status']);
      unset($getResponseData['companyName']);

      $this->assertEquals($getResponseData ,$putData);
 
    }


}
