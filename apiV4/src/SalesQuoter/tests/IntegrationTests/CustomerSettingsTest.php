<?php

use SalesQuoter\tests\IntegrationTests\ApiAuthentication;


class CustomerSettingsTest extends \PHPUnit_Framework_TestCase
{

    public $api;

    public function __construct()
    {
        $this->api = new ApiAuthentication();  
    }


    public function testCustomer () {

      $getResponse = $this->api->callEndpoint("/default/customer", "GET", array());
      $getData = $this->api->getSpecificMsg( $getResponse, "data" );

      $this->assertEquals(is_array($getData[0]), true);

      $putData = $getData[0];
      $defaultId = $getData[0]['id'];

      $putData['display'] = 0;
      $putData['required'] = 0;
      $putData['placeholder'] = "test placeholder";

      $putResponse = $this->api->callEndpoint("/default/customer/".$defaultId, "PUT", json_encode($putData) );
      $responseData = json_decode( $putResponse['body'], TRUE );

      $this->assertEquals ( $putData, $responseData );
      
    }




}
