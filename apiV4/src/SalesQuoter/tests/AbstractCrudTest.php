<?php


use SalesQuoter\AbstractCrud;
use SalesQuoter\MysqlConnection;

class AbstractCrudTest extends \PHPUnit_Framework_TestCase
{

    public function setUp()
    {
         $_SERVER['SITE_NAME'] = 'panoramic';
    }


    public function tearDown()
    {
    
          //unset($_SERVER['SITE_NAME']);
    }

    
    public function getAbstractCrudObj()
    {

         
        $fields = array("users.id", "name", "email", "password", "type", "discount","prefix", "salesAdmin", "anytimeQuoteEdits");
        $config = array("table" => 'users', 'fields' =>$fields);

        return new AbstractCrud($config);
    }
    
    public function testGetAll()
    {
    
        $absCrudObj = $this->getAbstractCrudObj();
        $allUsers =  $absCrudObj->getAll();

        $this->assertEquals(true, is_array($allUsers));
    }

    public function testGet()
    {
    
        $absCrudObj = $this->getAbstractCrudObj();
        $singleUsers =  $absCrudObj->get(1);

        $this->assertEquals(is_array($singleUsers), true);
    }


    public function testDelete()
    {
    
        $absCrudObj = $this->getAbstractCrudObj();
        $singleUsersInfo =  $absCrudObj->get(20);

        $singleUsers =  $absCrudObj->delete(20);
        $resultStatus = $singleUsers['status'];

        if (isset($singleUsersInfo->status)) {
            $expectedStatus = 'success';
        } else {
            $expectedStatus = 'failed';
        }
        
        $this->assertEquals($expectedStatus, $resultStatus);
    }

    public function testNotDelete()
    {
    
        $absCrudObj = $this->getAbstractCrudObj();
        $singleUsers =  $absCrudObj->delete(99999);
        $resultStatus = $singleUsers['status'];

        $expectedStatus = 'failed';
        $this->assertEquals($expectedStatus, $resultStatus);
    }


    public function testCreate()
    {
    
        $testData = array('email' => 'test@test.com' , 'password'=> '12345','type' =>'salesPerson' ,
                      'discount' => 10.0, 'salesAdmin' => 'on','name'=>'test' ,'anytimeQuoteEdits'=>0,'prefix'=>'test');
        $absCrudObj = $this->getAbstractCrudObj();
        $singleUser =  $absCrudObj->create($testData);
        $result = array();
        foreach ($singleUser as $key => $value) {
            if ($key != 'id') {
                $result[$key] = $value;
            }
        }

        $this->assertEquals($testData, $result);
    }



    public function testUpdate()
    {
    
        $testData = array('email' => 'test@test.com' , 'password'=> '12345','type' =>'salesPerson' ,
                        'discount' => 10.0, 'salesAdmin' => 'on','name'=>'test' ,'anytimeQuoteEdits'=>0,'prefix'=>'test');
        $absCrudObj = $this->getAbstractCrudObj();
        $singleUser =  $absCrudObj->create($testData);
        $result = array();
        foreach ($singleUser as $key => $value) {
            if ($key != 'id') {
                $result[$key] = $value;
            } else {
                $pId = $value;
            }
        }


        $testData['id'] = $pId;
        $result =  $absCrudObj->update($testData);

        $this->assertEquals($testData, $result);
    }
}
