<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;

class WindowComponentTest extends AbstractTestNonPublicMethods
{
    protected $config;
    protected $windowComponent;
    protected $requiredMethods;

    public function setUp()
    {
        $this->windowComponent = $this->getMockBuilder('\SalesQuoter\Windows\AbstractWindowComponent')
            ->disableOriginalConstructor()
            ->setMethods(array('init'))
            ->getMock();

        $this->testClass = $this->windowComponent;
        parent::setup();
    }

    /**
     * @group windowComponent
     */
    public function testContruct()
    {
        $config = array();
        $requiredMethods = array (
            'hasRequiredProperties' => $this->config,
            'initWithConfig'=> $this->config ,
            'init' => '');

        $windowComponent = $this->getMockBuilder('\SalesQuoter\Windows\AbstractWindowComponent')
            ->disableOriginalConstructor()
            ->setMethods(array_keys($requiredMethods))
            ->getMock();

        foreach ($requiredMethods as $method => $with) {
            if (empty($with)) {
                $windowComponent->expects($this->once())
                    ->method($method);
            } else {
                $windowComponent->expects($this->once())
                    ->method($method)
                    ->with($with);
            }
        }

        $windowComponent->__construct($config);
    }


    /**
     * @group windowComponent
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required property was not passed in: height2
     */
    public function testHasRequiredPropertiesFail()
    {
        $requiredProperties = array('height2');
        $this->setProperty('requiredProperties', $requiredProperties);

        $this->windowComponent = $this->getMethod('hasRequiredProperties');
        $this->windowComponent->invoke($this->testClass, array());
    }

    /**
     * @group windowComponent
     */
    public function testValidateConfig()
    {
        $requiredProperties = array('height');
        $this->setProperty('requiredProperties', $requiredProperties);
        $config = array ("height" => "100");

        $windowRow = $this->getMethod('hasRequiredProperties');
        $windowRow->invoke($this->testClass, $config);
    }


    /**
     * @group windowComponent
     * @expectedException RuntimeException
     * @expectedExceptionMessage Protected Property of CLASS: bro does not exist
     */
    public function testInitWithConfigFail()
    {
        $requiredProperties = array('bro');
        $this->setProperty('requiredProperties', $requiredProperties);
        $config = array ();

        $windowRow = $this->getMethod('initWithConfig');
        $windowRow->invoke($this->testClass, $config);
    }


    /**
     * @group windowComponent
     */
    public function testInitWithConfig()
    {
        $requiredProperties = array('testProperty');
        $this->setProperty('requiredProperties', $requiredProperties);
        $config = array ("testProperty" => "100");

        $windowRow = $this->getMethod('initWithConfig');
        $windowRow->invoke($this->testClass, $config);

        $requiredProperties = $this->getProperty('requiredProperties');

        foreach ($requiredProperties as $key) {
            $value = $this->getProperty($key);
            $this->assertEquals($config[$key], $value);
        }

        $this->config['testProperty'] = 1001;
        $testProperty = $this->getProperty('testProperty');
        $this->assertNotEquals($this->config['testProperty'], $testProperty);
    }
}
