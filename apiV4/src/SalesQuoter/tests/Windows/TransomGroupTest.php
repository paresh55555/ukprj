<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\TransomGroup;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\PartsExamples;

class TransomGroupTest extends AbstractTestNonPublicMethods
{

    protected $parts;
    protected $config;
    protected $windowData;
    protected $transomGroup;

    /**
     *
     */
    public function setUp()
    {

        $this->parts = new PartsExamples();
        $this->windowData = WindowData::createWithSampleData();

        $this->config = array ("parts" => $this->parts, "windowData" => $this->windowData);

        $this->transomGroup = new TransomGroup($this->config);
        $this->testClass = $this->transomGroup;

        parent::setup();
    }


    /**
     * @group transomGroup
     */
    public function testStarting()
    {
        
        $buildTransoms = $this->getMethod('buildTransoms');
        $buildTransoms->invoke($this->testClass);
//        buildTransoms
        $this->getProperty('transoms');

        $this->testClass->getCutSheetParts();
    }
}
