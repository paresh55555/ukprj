<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\WindowData;

/**
 *
 * @SuppressWarnings(PHPMD)
 *
 */
class WindowDataTest extends AbstractTestNonPublicMethods
{
    protected $config;
    protected $windowData;

    public function setUp()
    {
        $row = array ("columns" => 3, "widths" => array("300", "100", "100"), "height" => "250", "openers" => array("top", "bottom", "left"));

        $rows = array($row, $row);

        $this->config = array(
            'height' => 500,
            'width' => 500,
            'rows' => $rows,
            'type' => 'vertical',
            'module' => 1,
            'systemType' => 'V-3-3',
            ''
        );

        $this->windowData = WindowData::createWithSampleData();

//        $this->windowData = new WindowData($this->config);
        $this->testClass = $this->windowData;
        parent::setup();
    }


    /**
     *
     * @group windowData
     */
    public function testInit()
    {

        $windowData = $this->getMockBuilder('\SalesQuoter\Windows\WindowData')
            ->disableOriginalConstructor()
            ->setMethods(array('validateType', 'validateRows', 'validateRowsWidths'))
            ->getMock();

        $windowData->expects($this->once())
            ->method('validateType');

        $windowData->expects($this->once())
            ->method('validateRows');

        $windowData->expects($this->once())
            ->method('validateRowsWidths');


        $this->testClass = $windowData;
        parent::setup();

        $init = $this->getMethod('init');
        $init->invoke($this->testClass);
    }


    /**
     *
     * @group windowData
     */
    public function testValidateType()
    {
        $this->setProperty('type', 'vertical');
        $validateRows = $this->getMethod('validateType');
        $validateRows->invoke($this->testClass);

        $this->setProperty('type', 'horizontal');
        $validateRows->invoke($this->testClass);
    }


    /**
     * @expectedException RuntimeException
     * @expectedExceptionMessage Invalid Type Passed
     *
     * @group windowData
     */
    public function testValidateTypeFail()
    {
        $this->setProperty('type', 'vertical1');
        $validateRows = $this->getMethod('validateType');
        $validateRows->invoke($this->testClass);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Invalid Type Passed
     *
     * @group windowData
     */
    public function testValidateTypeNone()
    {
        $this->setProperty('type', '');
        $validateRows = $this->getMethod('validateType');
        $validateRows->invoke($this->testClass);
    }


    /**
     *
     */
    protected function validateType()
    {
        if (!in_array($this->type, $this->validTypes)) {
            throw new \RuntimeException('Invalid Type Passed');
        }
    }


    /**
     * Double checks the required properties are set
     * @group windowData
     */
    public function testRequiredValues()
    {

        $requiredProperties = array(
            "height", "width", "rows", "type", "module", "systemType", "pricingType"
        );
        $checkRequired = $this->getProperty('requiredProperties');

        $this->assertEquals($requiredProperties, $checkRequired);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required row property: columns was not passed in
     *
     * @group windowData
     */
    public function testValidateRowsNoColumn()
    {

        $rows =  array(array ("height" => 10, "widths" => array("100") ));
        $this->setProperty('rows', $rows);

        $validateRows = $this->getMethod('validateRows');
        $validateRows->invoke($this->testClass);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required row property: widths was not passed in
     *
     * @group windowData
     */
    public function testValidateRowsNoWidths()
    {

        $rows =  array(array ("height" => 10, "columns" => 10 ));
        $this->setProperty('rows', $rows);

        $validateRows = $this->getMethod('validateRows');
        $validateRows->invoke($this->testClass);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required row property: height was not passed in
     *
     * @group windowData
     */
    public function testValidateRowsNoHeight()
    {

        $rows =  array(array ( "columns" => 10, "widths" => array() ));
        $this->setProperty('rows', $rows);

        $validateRows = $this->getMethod('validateRows');
        $validateRows->invoke($this->testClass);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Row widths are not adding up to width of the system
     *
     * @group windowData
     */
    public function testValidateRowsWidthsBlank()
    {

        $row = array ( "widths" => array() );

        $validateRowsWidths = $this->getMethod('validateWidthTotal');
        $validateRowsWidths->invoke($this->testClass, $row);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Row widths are not adding up to width of the system
     *
     * @group windowData
     */
    public function testValidateRowsWidthsInvalid()
    {

        $row =  array ( "widths" => array("100","300","99") );

        $validateRowsWidths = $this->getMethod('validateWidthTotal');
        $validateRowsWidths->invoke($this->testClass, $row);
    }


    /**
     *
     * @group windowData
     */
    public function testValidateRowsWidthsPass()
    {

        $row =  array ( "widths" => array("500") );

        $validateRowsWidths = $this->getMethod('validateWidthTotal');
        $validateRowsWidths->invoke($this->testClass, $row);

        $row =  array ( "widths" => array(100, 200, 200));
        $validateRowsWidths->invoke($this->testClass, $row);
    }


    /**
     *
     * @expectedException RuntimeException
     * @expectedExceptionMessage Column count not matching passed in number of widths
     *
     * @group windowData
     */
    public function testValidateRowsWidthCountInvalid()
    {

        $row =  array ("columns" => 1, "widths" => array("100","300","99") );

        $validateWidthCount = $this->getMethod('validateWidthCount');
        $validateWidthCount->invoke($this->testClass, $row);
    }

    /**
     *
     * @group windowData
     */
    public function testValidateRowsWidthCount()
    {

        $row =  array ("columns" => 3, "widths" => array("100","300","99") );

        $validateWidthCount = $this->getMethod('validateWidthCount');
        $validateWidthCount->invoke($this->testClass, $row);
    }


    /**
     *
     * @group windowData
     */
    public function testValidateRowsWidths()
    {

        $windowData = $this->getMockBuilder('\SalesQuoter\Windows\WindowData')
            ->disableOriginalConstructor()
            ->setMethods(array('validateWidthCount', 'validateWidthTotal'))
            ->getMock();

        $windowData->expects($this->once())
            ->method('validateWidthCount');

        $windowData->expects($this->once())
            ->method('validateWidthTotal');


        $this->testClass = $windowData;
        parent::setup();

        $this->setProperty('rows', array("something"));
        $validateRowsWidths = $this->getMethod('validateRowsWidths');
        $validateRowsWidths->invoke($this->testClass);
    }


    /**
     *
     * @group windowData
     */
    public function testgetNumberOfOpeners()
    {

        $config = WindowData::getSampleData();
        $openers = array("", "", "");
        $config['rows'][0]['openers'] = $openers;

        $this->windowData = new WindowData($config);
        $this->testClass = $this->windowData;
        parent::setup();

        $getNumberOfOpeners = $this->getMethod('getNumberOfOpeners');
        $openers = $getNumberOfOpeners->invoke($this->testClass);

        $this->assertEquals(4, $openers);
    }

    /**
     *
     * @group windowData
     */
    public function testCountOpenersPerRow()
    {

        $openers = array("top", "bottom", "");
        $row = array ("columns" => 3, "widths" => array("300", "100", "100"), "height" => "250", "openers" => $openers);

        $getNumberOfOpeners = $this->getMethod('countOpenersPerRow');
        $openers = $getNumberOfOpeners->invoke($this->testClass, $row);

        $this->assertEquals(2, $openers);

        $openers = array("","","");
        $row = array ("columns" => 3, "widths" => array("300", "100", "100"), "height" => "250", "openers" => $openers);
        $openers = $getNumberOfOpeners->invoke($this->testClass, $row);

        $this->assertEquals(0, $openers);


        $openers = array("top","top","top");
        $row = array ("columns" => 3, "widths" => array("300", "100", "100"), "height" => "250", "openers" => $openers);
        $openers = $getNumberOfOpeners->invoke($this->testClass, $row);

        $this->assertEquals(3, $openers);
    }
}
