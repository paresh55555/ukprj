<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\Location;
use SalesQuoter\Windows\ComponentDataExamples;

use SalesQuoter\Windows\LocationSize;
use SalesQuoter\Windows\PartsExamples;

class LocationSizeTest extends AbstractTestNonPublicMethods
{
    protected $config;
    protected $parts;
    protected $locationSize;
    protected $frameGroup;
    protected $height;
    protected $widht;

    public function setUp()
    {
        $this->height = 500;
        $this->width = 500;

        $this->parts = new PartsExamples();
        $this->frameGroup = $this->parts->getFrameGroup();
        $this->config = array(
            'height' => $this->height,
            'width' => $this->width,
            'windowType' => array(3,1),
            'whichLocation' => array (1,0),
            'parts' => $this->parts
        );

        $this->locationSize = new LocationSize($this->config);
        $this->testClass = $this->locationSize;
        parent::setup();
    }

//    /**
//     * @group locationSize
//     */
//    public function testlLocationGlass() {
//
//        $leftFrame = $this->frameGroup->getLeftFrame();
//        $topFrame = $this->frameGroup->getTopFrame();
//        $transom = $this->parts->getTransom();
//        $mullion = $this->parts->getMullion();
//
//        $width = $this->config['width'] - $leftFrame->getDeduction() - $mullion->getDeduction();
//        $height = $this->config['height'] - $topFrame->getDeduction() - $transom->getDeduction();
//
//        $checkWidth = $width + $leftFrame->getOverlap() + $mullion->getOverlap();
//        $checkHeight = $height + $topFrame->getOverlap() + $transom->getOverlap();
//
//        $glassSheet = $this->getMethod('glassSheet');
//        $glass = $glassSheet->invoke($this->testClass);
//
//    }


    /**
     * @group locationSize
     */
    public function testCutSheetParts()
    {
        $partSheet = $this->testClass->getCutSheetParts();
        
        $this->assertTrue(isset($partSheet['parts']));
        $this->assertTrue(isset($partSheet['glass']));
        $this->assertTrue(isset($partSheet['row']));
        $this->assertTrue(isset($partSheet['column']));
        $this->assertTrue(isset($partSheet['height']));
        $this->assertTrue(isset($partSheet['width']));
    }

    
    /**
     * @group locationSize
     */
    public function testGetLocationSize()
    {
        $leftFrame = $this->frameGroup->getLeftFrame();
        $rightFrame = $this->frameGroup->getRightFrame();
        $topFrame = $this->frameGroup->getTopFrame();
        $bottomFrame = $this->frameGroup->getbottomFrame();
        $transom = $this->parts->getTransom();
        $mullion = $this->parts->getMullion();

        $width = $this->config['width'] - $leftFrame->getDeduction() - $mullion->getDeduction();
        $height = $this->config['height'] - $topFrame->getDeduction() - $transom->getDeduction();

        $getLocationSize = $this->getMethod('calculateLocationSize');
        $size = $getLocationSize->invoke($this->testClass);

        $this->assertEquals($size['width'], $width);
        $this->assertEquals($size['height'], $height);

        $width = $this->config['width'] - $mullion->getDeduction() * 2;
        $this->setProperty('whichLocation', array (2,0));
        $size = $getLocationSize->invoke($this->testClass);

        $this->assertEquals($size['width'], $width);
        $this->assertEquals($size['height'], $height);

        $width = $this->config['width'] - $leftFrame->getDeduction() - $rightFrame->getDeduction();
        $height = $this->config['height'] - $bottomFrame->getDeduction() - $transom->getDeduction();
        $this->setProperty('whichLocation', array (0,1));
        $size = $getLocationSize->invoke($this->testClass);

        $this->assertEquals($size['width'], $width);
        $this->assertEquals($size['height'], $height);

        $width = $this->config['width'] - $mullion->getDeduction() * 2;
        $height = $this->config['height'] - $transom->getDeduction() * 2;
        $this->setProperty('windowType', array (3,3,3));
        $this->setProperty('whichLocation', array (0,2,0));
        $size = $getLocationSize->invoke($this->testClass);

        $this->assertEquals($size['width'], $width);
        $this->assertEquals($size['height'], $height);

        $width = $this->config['width'] - $leftFrame->getDeduction() - $rightFrame->getDeduction() ;
        $height = $this->config['height'] - $topFrame->getDeduction() - $bottomFrame->getDeduction() ;
        $this->setProperty('windowType', array (1));
        $this->setProperty('whichLocation', array (1));
        $size = $getLocationSize->invoke($this->testClass);

        $this->assertEquals($size['width'], $width);
        $this->assertEquals($size['height'], $height);
    }


    /**
     * @group locationSize
     */
    public function testCalculateRight()
    {
        $rightCheck =  $this->parts->getMullion();

        $calculateRight = $this->getMethod('calculateRight');
        $right = $calculateRight->invoke($this->testClass);

        $this->assertEquals($right, $rightCheck);

        $rightCheck =  $this->parts->getFrameGroup()->getRightFrame();
        $this->setProperty('whichLocation', array(0,1));
        $right = $calculateRight->invoke($this->testClass);

        $this->assertEquals($right, $rightCheck);


        $rightCheck =  $this->parts->getFrameGroup()->getRightFrame();
        $this->setProperty('whichLocation', array(3,0));
        $right = $calculateRight->invoke($this->testClass);

        $this->assertEquals($right, $rightCheck);
    }


    /**
     * @group locationSize
     */
    public function testCalculateLeft()
    {

        $leftCheck =  $this->parts->getFrameGroup()->getLeftFrame();

        $calculateLeft = $this->getMethod('calculateLeft');
        $left = $calculateLeft->invoke($this->testClass);

        $this->assertEquals($left, $leftCheck);

        $this->setProperty('whichLocation', array(0,1));
        $left = $calculateLeft->invoke($this->testClass);

        $this->assertEquals($left, $leftCheck);

        $leftCheck = $this->parts->getMullion();
        $this->setProperty('whichLocation', array(2,0));
        $left = $calculateLeft->invoke($this->testClass);

        $this->assertEquals($left, $leftCheck);
    }


    /**
     * @group locationSize
     */
    public function testCalculateTop()
    {

        $topCheck =  $this->frameGroup->getTopFrame();

        $calculateTop = $this->getMethod('calculateTop');
        $top = $calculateTop->invoke($this->testClass);

        $this->assertEquals($top, $topCheck);

        $topCheck = $this->parts->getTransom();
        $this->setProperty('whichLocation', array(0,1));
        $top = $calculateTop->invoke($this->testClass);

        $this->assertEquals($top, $topCheck);
    }


    /**
     * @group locationSize
     */
    public function testCalculateBottom()
    {

        $bottomCheck =  $this->parts->getTransom();

        $calculateBottom = $this->getMethod('calculateBottom');
        $bottom = $calculateBottom->invoke($this->testClass);

        $this->assertEquals($bottom, $bottomCheck);

        $bottomCheck = $this->frameGroup->getBottomFrame();
        $this->setProperty('whichLocation', array(0,1));
        $bottom = $calculateBottom->invoke($this->testClass);

        $this->assertEquals($bottom, $bottomCheck);
    }


    /**
     * @group locationSize
     */
    public function testBuildBox()
    {
        $buildBox = $this->getMethod('buildBox');
        $buildBox->invoke($this->testClass);
    }


    /**
     * @group locationSize
     */
    public function testGlassCost()
    {
        $glassCost = $this->getMethod('glassCost');
        $cost = $glassCost->invoke($this->testClass);

        $locationWidth = $this->getProperty('locationWidth');
        $locationHeight = $this->getProperty('locationHeight');

        $glassConfig = ComponentDataExamples::getExampleGlass();

        $costCheck = $glassConfig['costPerSquareMeter'] * ($locationWidth - 2*$glassConfig['gap'])/1000 *($locationHeight - 2*$glassConfig['gap'])/1000;

        $this->assertEquals($costCheck, $cost);
    }
}
