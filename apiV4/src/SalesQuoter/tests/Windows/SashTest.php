<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\Sash;
use SalesQuoter\Windows\TransomGroup;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\PartsExamples;

class SashTest extends AbstractTestNonPublicMethods
{

    protected $parts;
    protected $config;
    protected $windowData;
    protected $transomGroup;
    protected $sash;

    /**
     *
     */
    public function setUp()
    {
//        $this->parts = new PartsExamples();
//
//        $this->config = $this->parts->exampleSashConfig();
//        $this->config['parts'] = $this->parts;
//
//        $this->sash = new Sash($this->config);
//        $this->testClass = $this->sash;
//
//        parent::setup();
    }


    /**
     * @group sash
     */
    public function testInit()
    {


        $mockFrameGroup = $this->getMockBuilder('\SalesQuoter\Windows\FrameGroup')
            ->disableOriginalConstructor()
            ->setMethods(array('setSize'))
            ->getMock();

        $mockFrameGroup->expects($this->once())
            ->method('setSize');


        $mockParts = $this->getMockBuilder('\SalesQuoter\Windows\PartsExamples')
            ->disableOriginalConstructor()
            ->setMethods(array('setTypeOfFrameGroup', 'getFrameGroup'))
            ->getMock();

        $mockParts->expects($this->once())
            ->method('getFrameGroup')
            ->willReturn($mockFrameGroup);

        $mockSash = $this->getMockBuilder('\SalesQuoter\Windows\Sash')
            ->disableOriginalConstructor()
            ->setMethods(array('calculateHeight', 'calculateWidth', 'getLocationSizeConfig', 'buildLocationSize'))
            ->getMock();

        $mockSash->expects($this->once())
            ->method('calculateHeight');

        $mockSash->expects($this->once())
            ->method('calculateHeight');

        $this->testClass = $mockSash;
        parent::setup();

        $this->setProperty('parts', $mockParts);

        $this->testClass->init();
    }

    public function init()
    {

        $this->calculateHeight();
        $this->calculateWidth();

        $this->parts->setTypeOfFrameGroup('sash');
        $configLocation = $this->getLocationSizeConfig();
        $this->locationSize = $this->buildLocationSize($configLocation);

        $size = array("height" => $this->height, "width" => $this->width);
        $this->parts->getFrameGroup()->setSize($size);
    }

    /**
     * @group sash
     */
    public function testGetCutSheets()
    {

        //Needs to be mocked
//        $cutSheetParts = $this->testClass->getCutSheetParts();
//
//        $this->assertTrue(isset($cutSheetParts['parts']));
//        $this->assertTrue(isset($cutSheetParts['glass']));
//        $this->assertEquals(count($cutSheetParts['parts']), 8);
    }
}
