<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\Locations;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\PartsExamples;

class LocationsTest extends AbstractTestNonPublicMethods
{
    protected $config;
    protected $parts;
    protected $locationSize;
    protected $frameGroup;

    protected $locations;
    protected $windowData;

    public function setUp()
    {
        $this->parts = new PartsExamples();
        $this->windowData = WindowData::createWithSampleData();

        $this->config = array("parts" => $this->parts, "windowData" => $this->windowData);
        $this->locations = new Locations($this->config);
        $this->testClass = $this->locations;
        parent::setup();
    }

//    /**
//     * @group locations
//     */
//    public function testlLocationGlass()
//    {

//        $locations = $this->getProperty('locations');

//        $this->assertTrue(true);

//        $this->assertEquals($glass['width'], $checkWidth);
//        $this->assertEquals($glass['height'], $checkHeight);
//    }
}
