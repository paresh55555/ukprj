<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\Windows\MullionGroup;
use SalesQuoter\Windows\WindowSystem;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\PartsExamples;
use SalesQuoter\Windows\TransomGroup;

use SalesQuoter\AbstractTestNonPublicMethods;

class WindowSystemTest extends AbstractTestNonPublicMethods
{
    protected $windowSystem;
    protected $windowData;

    public function setUp()
    {
        $this->windowData = WindowData::createWithSampleData();
        $this->windowSystem = new WindowSystem($this->windowData);

        $this->testClass = $this->windowSystem;
        parent::setup();
    }


    /**
     * @group windowSystem
     */
    public function testInit()
    {
        $windowSystem = $this->getMockBuilder('\SalesQuoter\Windows\WindowSystem')
            ->disableOriginalConstructor()
            ->setMethods(array('getPricing', 'getParts', 'buildFrame', 'getTransomGroup', 'getMullionsGroup', 'getLocations'))
            ->getMock();

        $windowSystem->expects($this->once())
            ->method('getPricing');

        $windowSystem->expects($this->once())
            ->method('getParts');

        $windowSystem->expects($this->once())
            ->method('buildFrame');

        $windowSystem->expects($this->once())
            ->method('getTransomGroup');

        $windowSystem->expects($this->once())
            ->method('getMullionsGroup');

        $windowSystem->expects($this->once())
            ->method('getLocations');

        $windowSystem->init();
    }


    /**
     * @group windowSystem
     */
    public function testGetCutSheetParts()
    {
//        $parts = new PartsExamples();
//        $this->setProperty('parts', $parts);
//
//        $config = array("windowData" => $this->windowData, "parts" => $parts);
//        $transomGroup = new TransomGroup($config);
//        $this->setProperty('transomGroup', $transomGroup);
//
//        $mullionGroup = new MullionGroup($config);
//        $this->setProperty('mullionGroup', $mullionGroup);
//
//
//        $buildFrame = $this->getMethod('buildFrame');
//        $frameGroup = $buildFrame->invoke($this->testClass);
//
//        $this->setProperty('frameGroup', $frameGroup);
//
//
//        $locationsMethod = $this->getMethod('getLocations');
//        $locations = $locationsMethod->invoke($this->testClass);
//
//        $this->setProperty('locations', $locations);
//
//        $cutSheetParts = $this->testClass->getCutSheetParts();
//
//
//        //Building the cutsheet same as the call
//        $frame = $frameGroup->getCutSheetParts();
//        $transoms = $transomGroup->getCutSheetParts();
//        $locations = $locations->getCutSheetParts();
//        $mullions = $mullionGroup->getCutSheetParts();
//
//        $systemInfo = array(
//            "height" => $this->windowData->getHeight(),
//            "width" => $this->windowData->getWidth(),
//            "locations" => $this->windowData->getNumberOfLocations(),
//            "type" => $this->windowData->getSystemType()
//        );
//
//        $cutSheetPartsCheck = array(
//            "system" => $systemInfo,
//            "parts" => array_merge($frame, array_merge($mullions, $transoms)),
//            "locations" => $locations,
//
//        );
//
//        $this->assertEquals($cutSheetParts['system'], $cutSheetPartsCheck['system']);
//         $this->assertEquals($cutSheetParts['locations'][0]['parts'], $cutSheetPartsCheck['locations'][0]['parts']);
//        $this->assertEquals($cutSheetParts['parts'], $cutSheetPartsCheck['parts']);
    }


    /**
     * @group windowSystem
     */
    public function testBuildFrame()
    {
//        $parts = new PartsExamples();
//        $this->setProperty('parts', $parts);
        $buildFrame = $this->getMethod('buildFrame');
         $buildFrame->invoke($this->testClass);
    }


    /**
     * @group windowSystem1
     */
    public function testGetMaterialCosts()
    {
        $parts = new PartsExamples();
        $this->setProperty('parts', $parts);
        
        $getMaterialCosts = $this->getMethod('getMaterialCosts');
        $cost = $getMaterialCosts->invoke($this->testClass);

        debugAlert($cost);
    }
}
