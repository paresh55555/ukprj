<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;

use SalesQuoter\MysqlConnection;
use SalesQuoter\Windows\ComponentData;
use SalesQuoter\Windows\ComponentDataExamples;

class ComponentDataTest extends AbstractTestNonPublicMethods
{

    protected $componentData;
    protected $mysqlConnection;

    public function setUp()
    {
        $mysqlConnection = new MysqlConnection();
        $mysqlConnection->changeServer("localhost");
        
        $module = '26';
        $this->componentData = new ComponentData($module);
        $this->testClass = $this->componentData;
        parent::setup();

        $this->setProperty('mysqlConnection', $mysqlConnection);
    }


    /**
     * @group componentData
     */
    public function testGetExampleFrameData()
    {
        $data = ComponentDataExamples::getExampleFrameData();

        $this->assertEquals($data['thickness'], 50);
        $this->assertEquals($data['burnOff'], 2);
        $this->assertEquals($data['overlap'], 5);
    }
}
