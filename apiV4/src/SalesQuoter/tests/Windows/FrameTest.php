<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\ComponentData;
use SalesQuoter\Windows\ComponentDataExamples;
use SalesQuoter\Windows\Frame;

class FrameTest extends AbstractTestNonPublicMethods
{

    protected $config;
    protected $componentData;
    protected $mysqlConnection;
    protected $frame;

    public function setUp()
    {
        $topFrameData = ComponentDataExamples::getExampleFrameData();

        $this->config = array(
            'topFrameData' => $topFrameData,
            'bottomFrameData' => $topFrameData,
            'leftFrameData' => $topFrameData,
            'rightFrameData' => $topFrameData
        );

        $this->frame = new Frame($this->config);
        $this->testClass = $this->frame;
        parent::setup();
    }

    /**
     * @group frame
     */
    public function testExampleFrame()
    {
    }
}
