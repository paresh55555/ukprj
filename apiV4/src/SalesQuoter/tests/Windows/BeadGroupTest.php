<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\BeadGroup;
use SalesQuoter\Windows\ComponentData;
use SalesQuoter\Windows\ComponentDataExamples;

class BeadGroupTest extends AbstractTestNonPublicMethods
{

    protected $config;
    protected $componentData;
    protected $mysqlConnection;
    protected $frame;
    protected $exampleBead;
    protected $width;
    protected $height;

    protected $topBeadData;
    protected $bottomBeadData;
    protected $leftBeadData;
    protected $rightBeadData;

    public function setUp()
    {
        $this->componentData = new ComponentData(0);
        $this->topBeadData = ComponentDataExamples::getExampleBeadData(1);
        $this->bottomBeadData = ComponentDataExamples::getExampleBeadData(1);
        $this->leftBeadData = ComponentDataExamples::getExampleBeadData(3);
        $this->rightBeadData = ComponentDataExamples::getExampleBeadData(3);

        $this->height = 600;
        $this->width = 500;

        $this->setUpClass();
    }


    public function setUpClass()
    {
        $this->config = array(
            'priority' => 'top',
            'topBeadData' => $this->topBeadData,
            'bottomBeadData' => $this->bottomBeadData,
            'leftBeadData' => $this->leftBeadData,
            'rightBeadData' => $this->rightBeadData
        );

        $beadGroup = new BeadGroup($this->config);

        $this->testClass = $beadGroup;
        parent::setup();

        $this->setProperty('width', $this->width);
        $this->setProperty('height', $this->height);
    }

    /**
     * @group bead
     */
    public function testCutSheetPartsFromHeightAndWidth()
    {
        $height = 600;
        $width = 500;
        $partSheet = $this->testClass->cutSheetPartsFromHeightAndWidth($height, $width);

        $this->assertEquals(count($partSheet), 4);
    }


    /**
     * @group bead
     */
    public function testSetTopBottomLength()
    {
        $thickness = $this->topBeadData['thickness'];
        $burnOff = $this->topBeadData['burnOff'];

        $topBead = $this->getProperty('topBead');
        
        //Testing top priority
        $setTopBottomLength = $this->getMethod('setTopBottomLength');
        $length = $setTopBottomLength->invoke($this->testClass, $topBead);

        $this->assertEquals($length, $this->width);

        $this->topBeadData['priority'] = 3;
        $this->leftBeadData['priority'] = 1;
        $this->rightBeadData['priority'] = 1;

        $this->setUpClass();

        $setTopBottomLength = $this->getMethod('setTopBottomLength');
        $topBead = $this->getProperty('topBead');
        $length = $setTopBottomLength->invoke($this->testClass, $topBead);
        $checkLength = $this->width - (2 * $thickness) - (2 * $burnOff);

        $this->assertEquals($length, $checkLength);


        $this->topBeadData['priority'] = 2;
        $this->leftBeadData['priority'] = 1;
        $this->rightBeadData['priority'] = 3;

        $this->setUpClass();

        $setTopBottomLength = $this->getMethod('setTopBottomLength');
        $topBead = $this->getProperty('topBead');
        $length = $setTopBottomLength->invoke($this->testClass, $topBead);
        $checkLength = $this->width - $thickness -  $burnOff;
        $this->assertEquals($length, $checkLength);


        $this->topBeadData['priority'] = 2;
        $this->leftBeadData['priority'] = 2;
        $this->rightBeadData['priority'] = 2;

        $this->setUpClass();

        $setTopBottomLength = $this->getMethod('setTopBottomLength');
        $topBead = $this->getProperty('topBead');
        $length = $setTopBottomLength->invoke($this->testClass, $topBead);
        $checkLength = $this->width ;
        $this->assertEquals($length, $checkLength);
    }


    /**
     * @group bead
     */
    public function testSetLeftRightLength()
    {

        $thickness = $this->topBeadData['thickness'];
        $burnOff = $this->topBeadData['burnOff'];

        $leftBead = $this->getProperty('leftBead');

        $setLeftRightLength = $this->getMethod('setLeftRightLength');
        $length = $setLeftRightLength->invoke($this->testClass, $leftBead);

        $checkLength = $this->height - (2 * $thickness) - (2 * $burnOff);
        $this->assertEquals($length, $checkLength);


        $this->topBeadData['priority'] = 3;
        $this->leftBeadData['priority'] = 1;
        $this->bottomBeadData['priority'] = 3;

        $this->setUpClass();

        $setLeftRightLength = $this->getMethod('setLeftRightLength');
        $leftBead = $this->getProperty('leftBead');
        $length = $setLeftRightLength->invoke($this->testClass, $leftBead);

        $checkLength = $this->height;
        $this->assertEquals($length, $checkLength);


        $this->topBeadData['priority'] = 1;
        $this->leftBeadData['priority'] = 1;
        $this->bottomBeadData['priority'] = 3;

        $this->setUpClass();

        $setLeftRightLength = $this->getMethod('setLeftRightLength');
        $leftBead = $this->getProperty('leftBead');
        $length = $setLeftRightLength->invoke($this->testClass, $leftBead);
        
        $checkLength = $this->height - $thickness - $burnOff;
        $this->assertEquals($length, $checkLength);
    }


    /**
     * @group bead
     */
    public function testCostFromHeightAndWidth()
    {

        $costHeightAndWidth = $this->getMethod('costFromHeightAndWidth');
        $cost = $costHeightAndWidth->invoke($this->testClass, $this->height, $this->width);

        $top = $this->getProperty('topBead');
        $bottom = $this->getProperty('bottomBead');
        $left = $this->getProperty('leftBead');
        $right = $this->getProperty('rightBead');

        $checkCost = $top->getCost() + $bottom->getCost() + $left->getCost() + $right->getCost() ;

        $this->assertEquals($checkCost, $cost);
    }
}
