<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;

use SalesQuoter\MysqlConnection;
use SalesQuoter\Windows\Component2D;
use SalesQuoter\Windows\ComponentData;
use SalesQuoter\Windows\ComponentDataExamples;

class Component2DTest extends AbstractTestNonPublicMethods
{

    protected $component2D;
    protected $mysqlConnection;
    protected $config;

    public function setUp()
    {

        $this->config = ComponentDataExamples::getExampleFrameData();
        $this->component2D = new Component2D($this->config);
        $this->testClass = $this->component2D;
        parent::setup();
    }


    /**
     * @group component
     */
    public function testGetCost()
    {
        $length = 1000;
        $checkCost = $this->config['costPerMeter'] * $length / 1000;
        $this->testClass->setLength($length);
        $cost = $this->testClass->getCost();

        $this->assertEquals($cost, $checkCost);
    }
}
