<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\Windows\Mullion;
use SalesQuoter\Windows\WindowData;
use SalesQuoter\Windows\PartsExamples;
use SalesQuoter\Windows\MullionGroup;
use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\Component2D;
use SalesQuoter\Windows\Frame;
use SalesQuoter\Windows\Location;
use SalesQuoter\Windows\Parts;

class MillionGroupTest extends AbstractTestNonPublicMethods
{

    protected $transomData;
    protected $mullionData;
    protected $frameData;
    protected $frameDataBottom;
    protected $config;
    protected $mullionGroup;
    protected $mullion;
    protected $locationData;

    protected $windowData;
    protected $parts;

    public function setUp()
    {
        $this->windowData = WindowData::createWithSampleData();
        $this->parts =  new PartsExamples();
        $this->config = array ("windowData" => $this->windowData, "parts" => $this->parts);

        $this->mullionGroup = new MullionGroup($this->config);
        $this->testClass = $this->mullionGroup;
        parent::setup();
    }


    /**
     * @group mullionGroup
     */
    public function testGetLocations()
    {
        $cutSheetParts = $this->testClass->getCutSheetParts();

        $this->assertEquals(count($cutSheetParts), 3);
    }

    /**
     * @group mullionGroup
     */
    public function testMullionHeightLastIsFirstRow()
    {
        $this->frameGroup = $this->parts->getFrameGroup();
        $rows = $this->windowData->getRows();
        $mullion = $this->parts->getMullion();

        $correctHeight = $rows[0]['height'] + (2 * $mullion->getBurnOff()) - ($this->frameGroup->getTopFrameThickness() + $this->frameGroup->getBottomFrameThickness());

        $this->setProperty('lastRow', 1);
        $this->setProperty('rowNumber', 1);
        $mullionHeightMethod = $this->getMethod('mullionHeight');
        $mullionHeight = $mullionHeightMethod->invoke($this->testClass);

        $this->assertEquals($correctHeight, $mullionHeight);
    }


    /**
     * @group mullionGroup
     */
    public function testMullionHeightFirstRowMultiRows()
    {
        $this->frameGroup = $this->parts->getFrameGroup();
        $transom = $this->parts->getTransom();
        $rows = $this->windowData->getRows();
        $mullion = $this->parts->getMullion();

        $correctHeight = $rows[0]['height'] + (2 * $mullion->getBurnOff()) - ($this->frameGroup->getTopFrameThickness() + $transom->getDeduction() );

        $this->setProperty('lastRow', 2);
        $this->setProperty('rowNumber', 1);
        $mullionHeightMethod = $this->getMethod('mullionHeight');
        $mullionHeight = $mullionHeightMethod->invoke($this->testClass);

        $this->assertEquals($correctHeight, $mullionHeight);
    }


    /**
     * @group mullionGroup
     */
    public function testMullionHeightLastRowMultiRows()
    {
        $this->frameGroup = $this->parts->getFrameGroup();
        $transom = $this->parts->getTransom();
        $rows = $this->windowData->getRows();
        $mullion = $this->parts->getMullion();

        $correctHeight = $rows[0]['height'] + (2 * $mullion->getBurnOff()) - ($this->frameGroup->getBottomFrameThickness() + $transom->getDeduction() );

        $this->setProperty('lastRow', 2);
        $this->setProperty('rowNumber', 2);
        $mullionHeightMethod = $this->getMethod('mullionHeight');
        $mullionHeight = $mullionHeightMethod->invoke($this->testClass);

        $this->assertEquals($correctHeight, $mullionHeight);
    }


    /**
     * @group mullionGroup
     */
    public function testMullionHeightMiddleRows()
    {
        $this->frameGroup = $this->parts->getFrameGroup();
        $transom = $this->parts->getTransom();
        $rows = $this->windowData->getRows();
        $mullion = $this->parts->getMullion();

        $correctHeight = $rows[0]['height'] + (2 * $mullion->getBurnOff()) - (2 * $transom->getDeduction() );

        $this->setProperty('lastRow', 3);
        $this->setProperty('rowNumber', 2);
        $mullionHeightMethod = $this->getMethod('mullionHeight');
        $mullionHeight = $mullionHeightMethod->invoke($this->testClass);

        $this->assertEquals($correctHeight, $mullionHeight);
    }
}
