<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\ComponentDataExamples;
use SalesQuoter\Windows\Glass;

class GlassTest extends AbstractTestNonPublicMethods
{

    protected $glass;
    protected $config;
    protected $windowData;
    protected $transomGroup;
    protected $sash;
    protected $width;
    protected $height;
    protected $glassSizeConfig;

    /**
     *
     */
    public function setUp()
    {

        $this->width = 500;
        $this->height = 500;
        $this->config = ComponentDataExamples::getExampleGlass();
        $this->glass = new Glass($this->config);

        $this->testClass = $this->glass;

        $this->glassSizeConfig = array('height' => $this->height, 'width' => $this->width);

        parent::setup();

        $this->testClass->setSize($this->glassSizeConfig);
    }


    /**
     * @group glass
     */
    public function testSetSize()
    {

        $this->testClass->setSize($this->glassSizeConfig);

        $locationWidth = $this->getProperty('locationWidth');
        $locationHeight = $this->getProperty('locationHeight');

        $this->assertEquals($locationHeight, $this->height);
        $this->assertEquals($locationWidth, $this->width);
    }


    /**
     * @group glass
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required height was not passed in for Glass
     */
    public function testGlassConfigFailedHeight()
    {
        $this->glassSizeConfig['height'] = '';
        $this->testClass->setSize($this->glassSizeConfig);
    }


    /**
     * @group glass
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required width was not passed in for Glass
     */
    public function testGlassConfigFailedWidth()
    {
        $this->glassSizeConfig['width'] = '';
        $this->testClass->setSize($this->glassSizeConfig);
    }


    /**
     * @group glass
     * @expectedException RuntimeException
     * @expectedExceptionMessage Required width was not passed in for Glass
     */
    public function testGetCostFail()
    {
        $this->setProperty('config', '');
        $this->testClass->getCost();
    }


    /**
     * @group glass
     */
    public function testGetCost()
    {
        $costCheck = $this->config['costPerSquareMeter'] * (($this->width - $this->config['gap']*2) / 1000) * (($this->height - $this->config['gap']*2) / 1000);
        $cost = $this->testClass->getCost();

        $this->assertEquals($cost, $costCheck);
    }


    /**
     * @group glass
     */
    public function testGetCutSheet()
    {
        $cutSheet = $this->testClass->getCutSheet();

        $glassHeight = $this->getProperty('glassHeight');
        $glassWidth = $this->getProperty('glassWidth');

        $checkCutSheet = array (
            'name' => 'Low E3 with Argon',
            'height' => $glassHeight,
            'width' => $glassWidth);

        $this->assertEquals($cutSheet, $checkCutSheet);
    }
}
