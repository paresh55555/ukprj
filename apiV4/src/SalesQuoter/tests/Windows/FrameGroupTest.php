<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\ComponentDataExamples;
use SalesQuoter\Windows\FrameGroup;

class FrameGroupTest extends AbstractTestNonPublicMethods
{

    protected $config;
    protected $componentData;
    protected $mysqlConnection;
    protected $frameGroup;
    protected $frameData;
    protected $topFrameData;
    protected $bottomFrameData;
    protected $leftFrameData;
    protected $rightFrameData;
    protected $size;

    public function setUp()
    {
        $this->topFrameData = ComponentDataExamples::getExampleFrameData(1);
        $this->bottomFrameData = ComponentDataExamples::getExampleFrameData(3);
        $this->leftFrameData = ComponentDataExamples::getExampleFrameData(2);
        $this->rightFrameData = ComponentDataExamples::getExampleFrameData(2);

        $this->size = array ("height" => 200, "width" => 200);
        $this->setTestClass();
    }


    /**
     *
     */
    public function setTestClass()
    {
        $this->config = array(
            'topFrameData' => $this->topFrameData,
            'bottomFrameData' => $this->bottomFrameData,
            'leftFrameData' => $this->leftFrameData,
            'rightFrameData' => $this->rightFrameData
        );

        $this->frameGroup = new FrameGroup($this->config);
        $this->testClass = $this->frameGroup;
        parent::setup();

        $this->testClass->setSize($this->size);
    }

    /**
     * @group frameGroup
     */
    public function testExampleFrame()
    {
        $left = $this->getProperty('leftFrame');
        $right = $this->getProperty('rightFrame');
        $top = $this->getProperty('topFrame');
        $bottom = $this->getProperty('bottomFrame');

        $checkCost = $left->getCost() + $right->getCost() + $top->getCost() + $bottom->getCost();

        $size = array ("height" => 200, "width" => 200);
        $this->testClass->setSize($size);
        $cost = $this->testClass->getCost();

        $this->assertEquals($checkCost, $cost);
    }


//    /**
//     * @group frameGroup
//     */
//    public function testSetLeftFrameHeight()
//    {
//        $deduction = $this->topFrameData['thickness'] + $this->topFrameData['burnOff'];
//
//        $setLeftFrameHeight = $this->getMethod('setLeftRightFrameHeight');
//        $setLeftFrameHeight->invoke($this->testClass);
//        $leftFrame = $this->getProperty('leftFrame');
//        $this->assertEquals($leftFrame->getLength(), $this->size['height'] - $deduction);
//
//        $this->bottomFrameData['priority'] = 2;
//        $this->setTestClass();
//
//        $deduction =  $deduction + $this->bottomFrameData['thickness'] + $this->bottomFrameData['burnOff'];
//        $setLeftFrameHeight->invoke($this->testClass);
//        $leftFrame = $this->getProperty('setLeftRightFrameHeight');
//        $this->assertEquals($leftFrame->getLength(), $this->size['height'] - $deduction);
//
//
//        $this->bottomFrameData['priority'] = 1;
//        $this->setTestClass();
//        $setLeftFrameHeight->invoke($this->testClass);
//        $leftFrame = $this->getProperty('leftFrame');
//        $this->assertEquals($leftFrame->getLength(), $this->size['height'] - $deduction);
//
//    }


    /**
     * @group frameGroup
     */
    public function testSetRightFrameHeight()
    {
        $deduction = $this->topFrameData['thickness'] + $this->topFrameData['burnOff'];

        $setLeftFrameHeight = $this->getMethod('setLeftRightFrameHeight');
        $frame = $this->getProperty('rightFrame');
        $setLeftFrameHeight->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['height'] - $deduction);

        $frame = $this->getProperty('leftFrame');
        $setLeftFrameHeight->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['height'] - $deduction);

        $this->bottomFrameData['priority'] = 2;
        $this->setTestClass();

        $deduction =  $deduction + $this->bottomFrameData['thickness'] + $this->bottomFrameData['burnOff'];

        $frame = $this->getProperty('rightFrame');
        $setLeftFrameHeight->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['height'] - $deduction);

        $frame = $this->getProperty('leftFrame');
        $setLeftFrameHeight->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['height'] - $deduction);

        $this->bottomFrameData['priority'] = 1;
        $this->setTestClass();

        $frame = $this->getProperty('rightFrame');
        $setLeftFrameHeight->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['height'] - $deduction);

        $frame = $this->getProperty('leftFrame');
        $setLeftFrameHeight->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['height'] - $deduction);
    }


    /**
     * @group frameGroup
     */
    public function testSetTopBottomFrameWidth()
    {
        $deduction = $this->rightFrameData['thickness'] + $this->rightFrameData['burnOff'];

        $setTbFrameWidth = $this->getMethod('setTopBottomFrameWidth');
        $frame = $this->getProperty('topFrame');
        $setTbFrameWidth->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['width']);

        $frame = $this->getProperty('bottomFrame');
        $setTbFrameWidth->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['width'] - 2 * $deduction);


        $this->bottomFrameData['priority'] = 2;
        $this->setTestClass();

        $frame = $this->getProperty('bottomFrame');
        $setTbFrameWidth->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['width']);

        $this->bottomFrameData['priority'] = 1;
        $this->setTestClass();

        $frame = $this->getProperty('bottomFrame');
        $setTbFrameWidth->invoke($this->testClass, $frame);
        $this->assertEquals($frame->getLength(), $this->size['width']);
    }
}
