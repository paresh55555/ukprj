<?php

namespace SalesQuoter\tests\Windows;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Windows\ComponentData;
use SalesQuoter\Windows\ComponentDataExamples;
use SalesQuoter\Windows\Frame;
use SalesQuoter\Windows\FrameGroup;
use SalesQuoter\Windows\Mullion;
use SalesQuoter\Windows\PartsExamples;
use SalesQuoter\Windows\Transom;

class PartsExamplesTest extends AbstractTestNonPublicMethods
{

    protected $config;
    protected $componentData;
    protected $mysqlConnection;
    protected $frame;
    protected $parts;

    public function setUp()
    {
        $this->componentData = new ComponentData('0');
        $this->parts = new PartsExamples();
        $this->testClass = $this->parts;
        parent::setup();
    }

    /**
     * @group partsExamples
     */
    public function testExamplePart()
    {
        $checkFrame = $this->parts->exampleFrameGroup();
        $checkMullion = $this->parts->exampleMullion();
        $checkTransom = $this->parts->exampleTransom();
        $frameGroup = $this->getProperty('frameGroup');
        $mullion = $this->getProperty('mullion');
        $transom = $this->getProperty('transom');

        $this->assertEquals($checkFrame, $frameGroup);
        $this->assertEquals($checkMullion, $mullion);
        $this->assertEquals($checkTransom, $transom);
    }


    /**
     * @group partsExamples
     */
    public function testExampleMullion()
    {
        $exampleConfig = ComponentDataExamples::getExampleMullionData();
        $checkMullion = new Mullion($exampleConfig);
        $mullion = $this->parts->exampleMullion();

        $this->assertEquals($mullion, $checkMullion);
    }


    /**
     * @group partsExamples
     */
    public function testExampleTransom()
    {
        $frameGroup =$this->parts->exampleFrameGroup();
        $exampleConfig = ComponentDataExamples::getExampleTransomData();
        $exampleConfig['frameGroup'] =$frameGroup;
        $checkTransom = new Transom($exampleConfig);

        $transom = $this->parts->exampleTransom();

        $this->assertEquals($transom, $checkTransom);
    }



    /**
     * @group partsExamples
     */
    public function testExampleFrameConfig()
    {
        $exampleConfig = ComponentDataExamples::getExampleFrameData();
        $exampleConfPriority = $exampleConfig;
        $exampleConfPriority['priority'] = true;
        $config  = $this->parts->exampleFrameGroupConfig();

        $this->assertEquals($config['topFrameData'], $exampleConfig);
        $this->assertEquals($config['bottomFrameData'], $exampleConfig);
        $this->assertEquals($config['leftFrameData'], $exampleConfPriority);
        $this->assertEquals($config['rightFrameData'], $exampleConfPriority);
    }


    /**
     * @group partsExamples
     */
    public function testExampleFrame()
    {
        $config  = $this->parts->exampleFrameGroupConfig();
        $checkFrame = new FrameGroup($config);
        $frame  = $this->parts->exampleFrameGroup();

        $this->assertEquals($checkFrame, $frame);
    }
}
