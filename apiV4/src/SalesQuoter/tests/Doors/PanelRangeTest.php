<?php

namespace SalesQuoter\tests\SalesPerson;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Doors\PanelRange;

class PanelRangeTest extends AbstractTestNonPublicMethods
{

    protected $panelRange;

    public function setUp()
    {
        $this->panelRange = new PanelRange();

        $this->testClass = $this->panelRange;
        parent::setup();
    }



    public function testCheckValidationEmpty()
    {

        $panelRangeObj = new PanelRange();

        $data['panels'] = '';
        $data['high'] = '';
        $data['low'] = '';

        $panelGroup = 9999;

        $response = $panelRangeObj->checkValidation($data, $panelGroup);
        var_dump($response);

        $expected = 'Field Value Can Not Be Empty';
        $this->assertEquals($response['message'], $expected);
    }



    public function testPanelsIsInteger()
    {

        $panelRangeObj = new PanelRange();

        $data['panels'] = '';
        $data['high'] = '3';
        $data['low'] = '2';

        $panelGroup = 9999;

        $response = $panelRangeObj->checkValidation($data, $panelGroup);

        $expected = 'Field Value Can Not Be Empty';
        $this->assertEquals($response['message'], $expected);
    }



    public function testHighLow()
    {

        $panelRangeObj = new PanelRange();

        $data['panels'] = 2;
        $data['high'] = '3';
        $data['low'] = '21';

        $panelGroup = 9999;

        $response = $panelRangeObj->checkValidation($data, $panelGroup);

        $expected = 'High field value must be larger than Low';
        $this->assertEquals($response['message'], $expected);
    }

}
