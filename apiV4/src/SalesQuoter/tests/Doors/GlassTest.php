<?php

namespace SalesQuoter\tests\SalesPerson;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Doors\Glass;

class GlassTest extends AbstractTestNonPublicMethods
{

    protected $glass;

    public function setUp()
    {
        $this->glass = new Glass();

        $this->testClass = $this->glass;
        parent::setup();
    }


    /**
     * @group glassDoors
     */
    public function testGlassGetAll()
    {
        $glass = new Glass();

        $config = array ("module_id" => 5 );
        $results = $glass->getAll($config);

        print_r($results);
        $this->assertTrue(true);
    }


    /**
     * @group glassDoors
     */
    public function testGlassItem()
    {
        $glass = new Glass();

        $results = $glass->get(50);

        print_r($results);
        $this->assertTrue(true);
    }


    /**
     * @group glassDoors1
     */
    public function testGlassCreate()
    {
        $glass = new Glass();

        $config = array ("module_id" => 5, "nameOfOption" => "Low-E New", "costPerPanel" => 5);
        $results = $glass->create($config);

        print_r($results);
        $this->assertTrue(true);
    }
}
