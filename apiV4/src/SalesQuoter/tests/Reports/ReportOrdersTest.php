<?php

namespace SalesQuoter\tests\SalesPerson;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Reports\ReportOrders;
use SalesQuoter\MysqlConnection;

class ReportOrdersTest extends AbstractTestNonPublicMethods
{

    protected $udb;
    protected $userData;

    public function setUp()
    {
       
        $reports = new ReportOrders();

        $this->testClass = $reports;
        parent::setup();
    }

    /**
     * @group reports
     */
    public function testGetUsers()
    {
        $this->testClass->deliveredMinToMax('2016-07-22', '2016-07-31');
    }
}
