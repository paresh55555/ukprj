<?php

namespace SalesQuoter\tests\SalesPerson;

use SalesQuoter\AbstractTestNonPublicMethods;
use SalesQuoter\Users\User;
use SalesQuoter\MysqlConnection;

class UserTest extends AbstractTestNonPublicMethods
{

    protected $uDB;
    protected $userData;

    public function setUp()
    {
        $this->userData = array ();
        $user = new User();

        $this->testClass = $user;
        parent::setup();
    }

    /**
     * @group user
     */
    public function testGetUsers()
    {
        $results = $this->testClass->getUsers();

        print_r($results);
    }

    /**
     * @group user
     */
    public function testCreateUsers()
    {

        $userData = array("name" => "fracka", "email" => "fracka@fracka.com", "companyName" => "Cool Company");
        $userData = array("name" => "fracka", "email" => "fracka@fracka.com");

        $result =  $this->testClass->createUser($userData);

        print_r($result);
    }


    /**
     * @group user
     */
    public function testUpdateUser()
    {
        $userData = array("id" => "52", "name" => "fracka", "email" => "fracka2@fracka.com", "password" => "ok", "companyName" =>"Better2 Company");
        $this->testClass->updateUser($userData);

        //print_r($result);
    }


    /**
     * @group user3
     */
    public function testDeleteUser()
    {
        $result =  $this->testClass->deleteUser(50);

        print_r($result);
    }

//    /**
//     * @group user
//     */
//    public function testCreateUsers()
//    {
//        $this->assertTrue(true);
//    }
}
