<?php
/*
*  User Auth Test Class Added
*/

use Slim\App;
use Slim\Container;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;
use Slim\Exception\SlimException;
use Slim\Handlers\Strategies\RequestResponseArgs;
use Slim\Http\Body;
use Slim\Http\Environment;
use Slim\Http\Headers;
use Slim\Http\Request;
use Slim\Http\RequestBody;
use Slim\Http\Response;
use Slim\Http\Uri;

class UserAuthTest extends \PHPUnit_Framework_TestCase
{
    public function __construct(){
        $this->apiUrl = "https://".$_SERVER['HOST']; // error is on this line
    }

    
    public function loadEndpoint($url, $method, $postFields, $header)
    {
    
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        return array(
          'body' => $output,
          'info' => $info
        );
    }
    
    public function testGetUserInvalidResponse()
    {
    
        $method = 'POST';
        $email = 'test';
        $password = 'test';
        $header =  array ('Authorization : Basic '.base64_encode($email.':'.$password),'content-type : application/json');
        $url = $this->apiUrl."/api/V4/token";
        $response = $this->loadEndpoint($url, $method, '', $header);
        $expected = 'error';
        $actual = json_decode($response['body'], TRUE);
        $this->assertEquals($actual['status'], $expected);
    }

}
