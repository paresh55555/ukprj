<?php
/* unit testing done*/

use SalesQuoter\Site\Defaults;

class DefaultsTest extends \PHPUnit_Framework_TestCase
{
    
    public function testGetSiteDefaults()
    {
    
        $defaults = new Defaults();
        $pId = 1;
        $siteDefaults = $defaults->get($pId);
        $expected =  sizeof(array_keys($siteDefaults));
        $this->assertEquals(39, $expected);
    }
}
