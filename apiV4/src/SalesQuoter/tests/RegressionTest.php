<?php


use SalesQuoter\Systems\Systems;
use SalesQuoter\Components\Components;
use SalesQuoter\Traits\Traits;
use SalesQuoter\Options\Options;
use SalesQuoter\Parts\OptionParts;
use SalesQuoter\Costs\OptionPartsCosts;


class RegressionTest extends \PHPUnit_Framework_TestCase
{

    public $yamlFileDetails = array();
    public $userName = "test@test.com";
    public $password = "12345";
    public $apiUrl = "";
    public $routesTypes1 = array("validations","traits","permissions");
    public $routesTypes2 = array("componentTypes","pricingTraits");
    public $routesTypes3 = array("companies","users");
    public $routesTypes4 = array("ComponentTraits","ComponentOptions" ,"OptionTraits","OptionPartsCosts","formulaCosts");
    public $allTableId =   array();


    public function __construct()
    {

        $this->yamlFileDetails = yaml_parse_file(dirname(__FILE__)."/yaml/all_endpoints.yaml");
        $this->apiUrl = "https://".$_SERVER['HOST']."/api/V4";
        $this->userToken = $this->getUserToken();
        $this->setUpTable();
    }

    public function tearDownTable()
    {

        $systems = new Systems();
        $components = new Components();
        $traits = new Traits();
        $options = new Options();
        $optionParts = new OptionParts();

        $systems->delete($this->allTableId["system_id"]);
        $components->delete($this->allTableId["component_id"]);
        $traits->delete($this->allTableId["trait_id"]);
        $options->delete($this->allTableId["option_id"]);
        $optionParts->delete($this->allTableId["part_id"]);
    }

    public function setUpTable()
    {

        $systems = new Systems();
        $config = array("name" => "test_system");
        $response =  $systems->create($config);
        $data["system_id"] = $response[0]["id"];



        $components = new Components();
        $config = array("name" => "test_component", "system_id" => $data["system_id"] );
        $response =  $components->create($config);
        $data["component_id"] = $response[0]["id"];


        $traits = new Traits();
        $config = array("name" => "test_traits", "component_id" => $data["component_id"] );
        $response =  $traits->create($config);
        $data["trait_id"] = $response[0]["id"];


        $options = new Options();
        $config = array("name" => "test_options", "system_id"=>$data["system_id"] ,  "component_id" => $data["component_id"] );
        $response =  $options->create($config);
        $data["option_id"] = $response[0]["id"];


        $optionParts = new OptionParts();
        $config = array("name" => "test_options_parts", "option_id"=> $data["option_id"], "system_id"=>$data["system_id"] ,  "component_id" => $data["component_id"] );
        $response =  $optionParts->create($config);
        $data["part_id"] = $response[0]["id"];


        $optionPartsCosts = new OptionPartsCosts();
        $config = array("name" => "test_options_parts_costs", "part_id" => $data["part_id"] , "option_id"=> $data["option_id"], "system_id"=>$data["system_id"] ,  "component_id" => $data["component_id"] );
        $response =  $optionPartsCosts->create($config);
        $data["cost_id"] = $response[0]["id"];



        $this->allTableId = $data;
    }

    public function formatEndpoint($endpoint)
    {

         $allSupportTables =  $this->allTableId;

        foreach ($allSupportTables as $key => $value) {
            $checkExists = strpos($endpoint, $key);
            if ($checkExists !== false) {
                $matchedKey = "{".$key."}";
                $endpoint = str_replace($matchedKey, $value, $endpoint);
            }
        }


         return $endpoint;
    }

    function format_get_response($response)
    {

        unset($response["id"]);
        unset($response["status"]);

         $allSupportTables =  $this->allTableId;

        foreach ($allSupportTables as $key => $value) {
            $allKeys = array_keys($response);

            if (in_array($key, $allKeys)) {
                unset($response[$key]);
            }

            if ( strlen($value) > '0') {
                
            }
        }

         return $response;
    }



    public function getUserToken()
    {
    
        $method = 'POST';
        $header =  array ('Authorization : Basic '.base64_encode($this->userName.':'.$this->password),'content-type : application/json');
        $url = $this->apiUrl."/token";
        $response = $this->loadEndpoint($url, $method, '', $header);
        $body = json_decode($response['body'], true);
      
        if ($body["status"] == "error") {
            echo 'Wrong Email or Password ';
            $this->assertEquals(false, false);
        } else {
            return $body['token'];
        }
    }

    public function testRoutesTypes1()
    {
         
        $allRoutes =  $this->routesTypes1;
        $filesDetails = $this->yamlFileDetails;

        foreach ($allRoutes as $endpoints) {
            $systems =  $filesDetails[$endpoints];
            $endpoint = $systems["EndPoint"];
            $desc = $systems["description"];

            $header =  array ('Authorization : Bearer '.$this->userToken ,'content-type : application/json');

            // create post from what in YAML
            $postData =  $systems["CREATE"]["POST"];
            $response = $this->loadEndpoint($this->apiUrl.$endpoint, 'POST', $postData, $header) ;
            $decodedData = json_decode($response['body'], true);
            @$parsedPostData = $decodedData['response_data'][0];
            $createdId = $parsedPostData["id"];


            // Then do GET with this Now Created ID
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $matchedGetData = $systems["CREATE"]["GET"];
            $decodedData = json_decode($response['body'], true);
            @$parsedPostData = $decodedData['response_data'][0];
            unset($parsedPostData["id"]);

            if (json_encode($parsedPostData) == $matchedGetData) {
                  echo $desc.' POST Data Matched with GET Data';
            } else {
                  echo $desc.' POST data not matched with GET data ';
            }

            $this->assertEquals(json_encode($parsedPostData), $matchedGetData);

            echo "\n";

            $putData = json_decode($systems["UPDATE"]["PUT"], true);
            $putData["id"] = $createdId;

            $response = $this->loadEndpoint($this->apiUrl.$endpoint, 'PUT', json_encode($putData), $header) ;
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $yamlGetData = $systems["UPDATE"]["GET"];
            $responseGetData = json_decode($response['body'], true)['response_data'][0];
            unset($responseGetData["id"]);

            if (json_encode($responseGetData) == $yamlGetData) {
                echo $desc.' Updated PUT Data Matched with GET Data';
            } else {
                 echo $desc.' Updated PUT Data Not Matched with GET Data';
            }

            $this->assertEquals(json_encode($responseGetData), $yamlGetData);


            // Delete Record
             echo "\n";

             $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'DELETE', '', $header) ;
             $getResponse =  $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;
            
             $status = json_decode($getResponse["body"], true)["status"];

            if ($status == "error") {
                 echo $desc." Data Deleted successfully ";
            } else {
                echo $desc." Data not Deleted .something went worng";
            }

              $this->assertEquals($status, "error");

               echo "\n";
        }
    }



    public function testRoutesTypes2()
    {
         

        echo "\n";
        $allRoutes =  $this->routesTypes2;
        $filesDetails = $this->yamlFileDetails;

        foreach ($allRoutes as $endpoints) {
            $systems =  $filesDetails[$endpoints];
            $endpoint = $systems["EndPoint"];
            $desc = $systems["description"];

            $header =  array ('Authorization : Bearer '.$this->userToken ,'content-type : application/json');

            // create post from what in YAML
            $postData =  $systems["CREATE"]["POST"];
            $response = $this->loadEndpoint($this->apiUrl.$endpoint, 'POST', $postData, $header) ;
            $parsedPostData = json_decode($response['body'], true)['response_data'][0];
            $createdId = $parsedPostData["id"];

            // Then do GET with this Now Created ID
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $matchedGetData = $systems["CREATE"]["GET"];
            $parsedPostData = json_decode($response['body'], true);
            unset($parsedPostData["id"]);
            unset($parsedPostData["status"]);

            if (json_encode($parsedPostData) == $matchedGetData) {
                  echo $desc.' POST Data Matched with GET Data';
            } else {
                  echo $desc.' POST data not matched with GET data ';
            }

            $this->assertEquals(json_encode($parsedPostData), $matchedGetData);

            echo "\n";

            $putData = json_decode($systems["UPDATE"]["PUT"], true);
            $putData["id"] = $createdId;

            $response = $this->loadEndpoint($this->apiUrl.$endpoint, 'PUT', json_encode($putData), $header) ;
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $yamlGetData = $systems["UPDATE"]["GET"];
            $responseGetData = json_decode($response['body'], true);
            unset($responseGetData["id"]);
            unset($responseGetData["status"]);

            if (json_encode($responseGetData) == $yamlGetData) {
                echo $desc.' Updated PUT Data Matched with GET Data';
            } else {
                 echo $desc.' Updated PUT Data Not Matched with GET Data';
            }

            $this->assertEquals(json_encode($responseGetData), $yamlGetData);


            // Delete Record
             echo "\n";

             $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'DELETE', '', $header) ;
             $getResponse =  $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;
            
             $status = json_decode($getResponse["body"], true)["status"];

            if ($status == "error") {
                 echo $desc." Data Deleted successfully ";
            } else {
                echo $desc." Data not Deleted .something went worng";
            }

              $this->assertEquals($status, "error");
        }
    }



    public function testRoutesTypes3()
    {
         

        echo "\n";
        $allRoutes =  $this->routesTypes3;
        $filesDetails = $this->yamlFileDetails;

        foreach ($allRoutes as $endpoints) {
            $systems =  $filesDetails[$endpoints];
            $endpoint = $systems["EndPoint"];
            $desc = $systems["description"];

            $header =  array ('Authorization : Bearer '.$this->userToken ,'content-type : application/json');

            // create post from what in YAML
            $postData =  $systems["CREATE"]["POST"];
            $response = $this->loadEndpoint($this->apiUrl.$endpoint, 'POST', $postData, $header) ;
            $parsedPostData = json_decode($response['body'], true);
            $createdId = $parsedPostData["id"];
            

            // Then do GET with this Now Created ID
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $matchedGetData = $systems["CREATE"]["GET"];
            $parsedPostData = json_decode($response['body'], true);
            unset($parsedPostData["id"]);
            unset($parsedPostData["status"]);

            if (json_encode($parsedPostData) == $matchedGetData) {
                  echo $desc.' POST Data Matched with GET Data';
            } else {
                  echo $desc.' POST data not matched with GET data ';
            }

            $this->assertEquals(json_encode($parsedPostData), $matchedGetData);

            echo "\n";

            $putData = json_decode($systems["UPDATE"]["PUT"], true);
            $putData["id"] = $createdId;

            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'PUT', json_encode($putData), $header) ;
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $yamlGetData = $systems["UPDATE"]["GET"];
            $responseGetData = json_decode($response['body'], true);
            unset($responseGetData["id"]);
            unset($responseGetData["status"]);

            if (json_encode($responseGetData) == $yamlGetData) {
                echo $desc.' Updated PUT Data Matched with GET Data';
            } else {
                 echo $desc.' Updated PUT Data Not Matched with GET Data';
            }

            $this->assertEquals(json_encode($responseGetData), $yamlGetData);


            // Delete Record
             echo "\n";

             $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'DELETE', '', $header) ;
             $getResponse =  $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;
             $status = json_decode($getResponse['body'], true)['status'];

            if ($status == "error") {
                 echo $desc." Data Deleted successfully ";
            } else {
                echo $desc." Data not Deleted .something went worng";
            }

              $this->assertEquals($status, "error");
        }
    }

    public function testRoutesTypes4()
    {
         
        echo "\n";
        $allRoutes =  $this->routesTypes4;
        $filesDetails = $this->yamlFileDetails;

        foreach ($allRoutes as $endpoints) {
            $systems =  $filesDetails[$endpoints];
            $endpoint = $this->formatEndpoint($systems["EndPoint"]);
            $desc = $systems["description"];

            $header =  array ('Authorization : Bearer '.$this->userToken ,'content-type : application/json');

            // create post from what in YAML
            $postData =  $systems["CREATE"]["POST"];
            $response = $this->loadEndpoint($this->apiUrl.$endpoint, 'POST', $postData, $header) ;
            $parsedPostData = json_decode($response['body'], true)["response_data"][0];
            $createdId = $parsedPostData["id"];


            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;
            $yamlGetData = $systems["CREATE"]["GET"];
            $parsedGetData = $this->format_get_response(json_decode($response['body'], true));

            if (json_encode($parsedGetData)  == $yamlGetData) {
                    echo $desc.' POST Data Matched with GET Data';
            } else {
                    echo $desc.' POST Data Not Matched with GET Data';
            }

            $this->assertEquals(json_encode($parsedGetData), $yamlGetData);

            echo "\n";

            $putData = json_decode($systems["UPDATE"]["PUT"], true);
            $putData["id"] = $createdId;

            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'PUT', json_encode($putData), $header) ;
            $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;

            $yamlGetData = $systems["UPDATE"]["GET"];
            $responseGetData = $this->format_get_response(json_decode($response['body'], true));

            if (json_encode($responseGetData) == $yamlGetData) {
                echo $desc.' Updated PUT Data Matched with GET Data';
            } else {
                 echo $desc.' Updated PUT Data Not Matched with GET Data';
            }

            $this->assertEquals(json_encode($responseGetData), $yamlGetData);

              // Delete Record
             echo "\n";

             $response = $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'DELETE', '', $header) ;
             $getResponse =  $this->loadEndpoint($this->apiUrl.$endpoint."/".$createdId, 'GET', '', $header) ;
            
             $status = json_decode($getResponse["body"], true)["status"];

            if ($status == "error" || $status == "failed") {
                 echo $desc." Data Deleted successfully ";
                 $this->assertEquals(true, true);
            } else {
                echo $desc." Data not Deleted .something went worng";
                 $this->assertEquals(false, true);
            }

             

             echo "\n";
        }

        $this->tearDownTable();
    }
   

    
    public function loadEndpoint($url, $method, $postFields, $header)
    {
    

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        return array(
          'body' => $output,
          'info' => $info
        );
    }
}
