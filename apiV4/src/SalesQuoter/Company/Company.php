<?php

namespace SalesQuoter\Company;

use SalesQuoter\AbstractCrud;

/**
 *  Company Class
 *
 */
class Company extends AbstractCrud
{

    /**
     * Class Constructor
     *
     */
    public function __construct()
    {

        $fields = array('id','companyName', 'firstName','lastName','address1', 'address2', 'city', 'state','phone', 'zip',
            );
        $config = array("table" => 'SQ_dealers', 'fields' =>$fields);

        parent::__construct($config);
    }


    /**
     * checking a companyName is exists or not
     *
     * @param companyName     $companyName     systemUX or systemPrice
     *
     * @return returns bolean
     */
    public function isAvailable($companyName)
    {

        $sql = $this->pdo->select(['id'])->from($this->table);
        $sql->where('companyName', '=', "$companyName");

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        if (sizeof($data) > 0) {
             return false;
        }

        return true;
    }


    /**
     * checking a company Id is exists or not
     *
     * @param pId  $pId   company id
     *
     * @return returns array
     */
    public function checkExists($pId)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->where($this->table.'.id', '=', $pId);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Getting all Company order by Name
     *
     * @param where  $where  where array conditions
     *
     * @return returns array
     */
    public function getAllOrderBy($where = null)
    {

        $sql = $this->pdo->select($this->fields)->from($this->table);
        $sql->orderBy('companyName', 'ASC');

        if (!empty($where)) {
            $column = key($where);
            $sql->where($column, '=', $where[$column]);
        }
        
        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        return $data;
    }
}
