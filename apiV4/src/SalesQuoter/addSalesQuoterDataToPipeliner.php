<?php

namespace SalesQuoter;

require __DIR__.'/../../vendor/autoload.php';
require __DIR__.'/../../src/SalesQuoter/SalesPerson/Crm.php';

use SalesQuoter\Quotes\Quote;
use SalesQuoter\SalesPerson\Crm;
use PipelinerSales\ApiClient\Query\Filter;

class CrmExt extends Crm
{

    const STAGE_SUBQUOTE = 'PY-7FFFFFFF-D9F667F5-A538-4084-ACA0-DCA9A31EBD58';
    const STAGE_PRIMARY = 'PY-7FFFFFFF-B54F022F-B35C-4BAC-A473-5061FDA264D6';
    const STAGE_CONTACTED = 'PY-7FFFFFFF-9338D5BB-9972-4A2B-B6EF-58CC306DBF98';
    const STAGE_PRESENTATION = 'PY-7FFFFFFF-78FB80C3-3188-4A6B-A2E9-367B70EB6A7B';
    const STAGE_COMMITMENT = 'PY-7FFFFFFF-15214543-8378-4632-BE22-A29B10E07C8C';
    const STAGE_DEPOSITANDORDER = 'PY-7FFFFFFF-A20691F5-B9D3-4357-83D0-54755E98677E';
    const STAGE_SENTTOPRODUCTION = 'PY-7FFFFFFF-A9DBD993-8AF3-469C-9149-385DCA4994B0';
    const STAGE_WIN = 'PY-7FFFFFFF-8B98A656-8334-4283-A893-E7CCCC9C059F';

    public function __construct($ownerId)
    {
        parent::__construct($ownerId);
    }

    public function findOpportunityByName($name)
    {
        return parent::findOpportunityByName($name);
    }

    public function getStageData($quote)
    {

        $data = array();

        if ($quote['type'] === 'quote') {
            $data['STAGE'] = self::STAGE_PRIMARY;
        } else {
            $data['STAGE'] = $quote['paidInFull'] === 'yes' ? self::STAGE_WIN : self::STAGE_COMMITMENT;
        }

        if ($quote['status'] === 'deleted' or $quote['quality'] == 'Archived') {
            $data['IS_ARCHIVE'] = 1;
            $data['REASON_OF_CLOSE_ID'] = "DV-VALUE_3";
        }
        return $data;
    }

    public function updateOpportunityExt($quoteInfo, $opportunityId)
    {

        $data = array('ID' => $opportunityId);

        if (!empty($quoteInfo['amount'])) {
            $data['OPPORTUNITY_VALUE'] = $quoteInfo['amount'];
            $data['OPPORTUNITY_VALUE_FOREGIN'] = $quoteInfo['amount'];
            $data['SQ_Price'] = $quoteInfo['amount'];
        }

        if (!empty($quoteInfo['quality'])) {
            $data['Quality2'] = $quoteInfo['quality'];
        }

        if (!empty($quoteInfo['phone'])) {
            $data['Phone_Number2'] = $quoteInfo['phone'];
        }

        $data += $this->getStageData($quoteInfo);

        try {
            $this->pipeliner->opportunities->save($data);
        } catch (PipelinerHttpException $e) {
            echo $e->getErrorMessage();
        }
    }

    public function createNewSalesQuoterLead($quoteInfo)
    {

        $contact = $this->findContact($quoteInfo['email']);
        $setSubquote = false;

        if (empty($contact)) {
            $contactData = array(
                'firstName' => $quoteInfo['firstName'] ? $quoteInfo['firstName'] : 'empty',
                'email' => $quoteInfo['email'] ? $quoteInfo['email'] : 'empty',
                'lastName' => $quoteInfo['lastName'],
                'phone' => $quoteInfo['phone']
            );

            $contactId = $this->createContact($contactData);
        } else {
            $contactId = $contact->getId();

            $filter = Filter::eq('OWNER_ID', $this->ownerId)->eq('IS_ARCHIVE', 0)->eq('IS_DELETED', 0)->eq('account_id', $contactId);
            $results = $this->pipeliner->opportunities->get($filter);

            if (count($results) > 0) {
                $setSubquote = true;
            }
        }

        $opportunity = array(
            "contactId" => $contactId,
            "prefix" => $quoteInfo['prefix'],
            "quoteId" => $quoteInfo['quoteId'],
            "value" => $quoteInfo['amount'],
            "quality" => $quoteInfo['quality'],
            "phone" => $quoteInfo['phone']
        );

        $futureDate = date('Y-m-d', strtotime("+".self::DAYS_AHEAD." days"));
        $contactRelations = array("CONTACT_ID" => $opportunity['contactId'], "IS_PRIMARY" => 1);

        $data = array(
            'CONTACT_RELATIONS' => array($contactRelations),
            'account_id' => $opportunity['contactId'],
            'OPPORTUNITY_NAME' => $opportunity['prefix'].$opportunity['quoteId'],
            'OPPORTUNITY_VALUE' => $opportunity['value'],
            'OWNER_ID' => $this->ownerId,
            'SALES_UNIT_ID' => '0',
            'CLOSING_DATE' => $futureDate,
            'OPPORTUNITY_VALUE_FOREGIN' => $opportunity['value'],
            'Phone_Number2' => $opportunity['phone'],
            'Quality2' => $opportunity['quality'],
            'Quote_Link' => 'https://panoramicdoors.salesquoter.com/?tab=viewQuote&quote='.$opportunity['quoteId'],
            'SQ_Price' => $opportunity['value']
        );

        $data += $this->getStageData($quoteInfo);

        if ($setSubquote) {
            $data['STAGE'] = self::STAGE_SUBQUOTE;
        }

        try {
            $result = $this->pipeliner->opportunities->save($data);
        } catch (PipelinerHttpException $e) {
            echo $e->getErrorMessage();
        }

        $opportunityId = $result->getCreatedId();

        return $opportunityId;
    }

    public function generateAndSaveDocument($quote, $token)
    {

        $url = "https://panoramicdoors.salesquoter.com/?quote=".$quote['quoteId']."&tab=viewQuote&print=yes&salesPerson=".$quote['SalesPerson']."&token=".$token."&site=panoramicdoors.salesquoter.com";

        $fileName = 'Quote-'.$quote['quoteId'].'.pdf';
        $fileNamePath = "/tmp/".$fileName;

        $config['url'] = $url;
        $config['fileNamePath'] = $fileNamePath;
        $config['fileName'] = $fileName;

        writeQuoteOrderWithConfig($config);

        $this->saveDocument($quote, $config);
    }
}

if (!isset($argv[1]) == 1) {
    exit("Please specify pipelinerID \r\n");
}

if (!isset($argv[2]) == 1) {
    exit("Please specify token as second argument \r\n");
}

$cnt = 0;
$pipelinerID = $argv[1];
$token = $argv[2];

$quote = new Quote();
$quotes = $quote->getQuotesByUserPLID($pipelinerID);

if (empty($quotes)) {
    exit("User has no quotes for import \r\n");
}

$crmExt = new CrmExt($pipelinerID);
$total = count($quotes);

foreach ($quotes as $quote) {
    echo "\r\nProcessing ".$cnt++." from {$total} quotes \r\n";
    $opportunity = $crmExt->findOpportunityByName($quote['prefix'].$quote['quoteId']);

    if (empty($opportunity[0])) {
        $opportunityID = $crmExt->createNewSalesQuoterLead($quote);
        echo "Opportunity {$opportunityID} created for quote ".$quote['prefix'].$quote['quoteId']."\r\n";
    } else {
        $optID = $opportunity[0]->getId();
        $crmExt->updateOpportunityExt($quote, $optID);
        echo "Opportunity {$optID} for quote {$quote['prefix']}{$quote['quoteId']} updated.\r\n";
    }
    echo "Generating and saving document...";
    $crmExt->generateAndSaveDocument($quote, $token);
}
