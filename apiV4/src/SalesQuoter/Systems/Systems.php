<?php

/*
*
*  @SuppressWarnings(PHPMD)
*  
*/
namespace SalesQuoter\Systems;

use SalesQuoter\Components\Components;
use SalesQuoter\CustomAbstractCrud;

/*
*
*  Class systems
*  
*/
class Systems extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
    */
    public function __construct()
    {

        $fields = array("id", "name", "url");
        $config = array("table" => 'SQ_systems', 'fields' => $fields);
        parent::__construct($config);
    }


    /**
     * updateCurrentSystemId
     *
     * @param systemId $systemId
     *
     * @return boolean
     */
    public function updateCurrentSystemId($systemId)
    {

        $newUpdate['system_id'] = $systemId;
       
        $updateStatement = $this->pdo->update($newUpdate)
                                    ->table("SQ_components")
                                    ->where('id', '=', $systemId);

        $updateStatement->execute();
    }

    /**
     * getMenuDetails
     *
     * @param systemId $systemId
     *
     * @return array
     */
    public function getMenuDetails($systemId)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_components');
        $sql->where('SQ_components.system_id', '=', $systemId);
        $sql->where('SQ_components.active', '=', 1);
        $sql->where('SQ_components.type', '=', 'menu');

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Adding only Traits
     *
     * @param oldComponentId $oldComponentId old component Id
     * @param newComponentId $newComponentId new component Id
     */
    public function addOnlyTraits($oldComponentId, $newComponentId)
    {

        $where['component_id'] = $oldComponentId;
        $allTraits = $this->getWhereTable($where, "SQ_traits");

        foreach ($allTraits as $traits) {
            $traits['component_id'] = $newComponentId;
            unset($traits['id']);

            $this->insertTable($traits, "SQ_traits");
        }
    }

    /**
     * Adding only Options
     *
     * @param newSystemId    $newSystemId    new System Id
     * @param oldComponentId $oldComponentId old component Id
     * @param newComponentId $newComponentId new component Id
     *
     */
    public function addOptions($newSystemId, $oldComponentId, $newComponentId)
    {

        $where['component_id'] = $oldComponentId;
        $allOldOptions = $this->getWhereTable($where, "SQ_new_options");

        foreach ($allOldOptions as $options) {
            $oldOptionId = $options['id'];
            unset($options['id']);

            $options['component_id'] = $newComponentId;
            $options['system_id'] = $newSystemId;

            $newOptionId = $this->insertTable($options, "SQ_new_options");

            $this->addOptionsTraits($newSystemId, $oldComponentId, $newComponentId, $oldOptionId, $newOptionId);
        }
    }


    /**
     * Adding Options Traits
     *
     * @param newSystemId    $newSystemId    new System Id
     * @param oldComponentId $oldComponentId old component Id
     * @param newComponentId $newComponentId new component Id
     * @param oldOptionId    $oldOptionId    old Option Id
     * @param newOptionId    $newOptionId    new Option Id
     *
     */
    public function addOptionsTraits($newSystemId, $oldComponentId, $newComponentId, $oldOptionId, $newOptionId)
    {

        $where['component_id'] = $oldComponentId;
        $where['option_id'] = $oldOptionId;

        $allOldOptionsTraits = $this->getWhereTable($where, "SQ_options_traits");

        foreach ($allOldOptionsTraits as $traits) {
            unset($traits['id']);

            $traits['system_id'] = $newSystemId;
            $traits['component_id'] = $newComponentId;
            $traits['option_id'] = $newOptionId;

            if ($traits['name'] == 'imgUrl') {
                $oldFilePath = str_replace("api/V4/", "", $traits['value']);
                $newFilePath =  "images/systems/".$newOptionId;
                mkdir($newFilePath);

                $pathInfo = pathinfo($oldFilePath);

                $isCopied = copy($oldFilePath, $newFilePath."/systems_".$newOptionId.".".$pathInfo['extension']);

           
                if ($isCopied) {
                    $traits['value'] = str_replace($oldOptionId, $newOptionId, $traits['value']);
                }
            }

            $this->insertTable($traits, "SQ_options_traits");
        }
    }


    /**
     * Adding Color  Group
     *
     * @param newSystemId    $newSystemId    new System Id
     * @param oldComponentId $oldComponentId old component Id
     * @param newComponentId $newComponentId new component Id
     *
     */
    public function addColorGroup($newSystemId, $oldComponentId, $newComponentId)
    {

        $allColorGroup = $this->getWhereTable(array("grouped_under" => $oldComponentId, 'type' => 'ColorGroup'), "SQ_components");
    
        foreach ($allColorGroup as $group) {
            $group['system_id'] = $newSystemId;
            $group['grouped_under'] = $newComponentId;

            $oldGroupId =  $group['id'] ;
            unset($group['id']) ;

            $newGroupId = $this->insertTable($group, "SQ_components");
            $this->addOptions($newSystemId, $oldGroupId, $newGroupId);
            $this->addOnlyTraits($oldGroupId, $newGroupId);

            $where['type'] = 'ColorSubGroup';
            $where['grouped_under'] = $oldGroupId;

            $allSubGroups = $this->getWhereTable($where, "SQ_components");

            foreach ($allSubGroups as $subGroup) {
                $oldSubGroupId =  $subGroup['id'];

                unset($subGroup['id']);
               
                $subGroup['system_id'] = $newSystemId;
                $subGroup['grouped_under'] = $newGroupId;

                $newSubGroupId = $this-> insertTable($subGroup, "SQ_components");

                $this->addOptions($newSystemId, $oldSubGroupId, $newSubGroupId);
                $this->addOnlyTraits($oldSubGroupId, $newSubGroupId);
            }
        }
    }



     /**
     * Creating Duplicate Systems
     *
     * @param newSystemName $newSystemName new System Name
     * @param systemId      $systemId      new sysem Id
     * @param groupedUnder  $groupedUnder  
     *
     * @return array
     */


    /**
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *
     */
    public function createDuplicate($newSystemName, $systemId, $groupedUnder)
    {

        $componentsObj = new Components();
        $systemDetails = $componentsObj->get($systemId);

        $data['name'] = $newSystemName;
        $data['system_id']  = $systemDetails[0]["system_id"];
        $data['active']  = 1;
        $data['type']  = $systemDetails[0]["type"];
        $data['grouped_under']  = strlen($groupedUnder) > '0' ? $groupedUnder : $systemDetails[0]["grouped_under"];
        $data['my_order']  = $systemDetails[0]["my_order"];
        $data['required']  = $systemDetails[0]["required"];
        $data['multiple']  = $systemDetails[0]["multiple"];

        $newSystemDetails = $componentsObj->create($data);
        $newSystemId = $newSystemDetails[0]['id'];

        $this->updateCurrentSystemId($newSystemId);
        $this->copyImages($systemId, $newSystemId);
        $this->copySampleDoors($systemId, $newSystemId, "SampleDoorPrice");
        $this->copySampleDoors($systemId, $newSystemId, "SampleDoorParts");
        // duplicating SQ_panel_ranges
        $this->copyPanelRanges($systemId, $newSystemId);
 
        // Getting all Menus List 
        $menuComponents = $this->getMenuDetails($systemId);

        $allTraits = $this->getWhereTable(array("component_id" => $systemId ), "SQ_traits");


        foreach ($allTraits as $componentTraits) {
                $componentTraits["component_id"] = $newSystemId;

                $oldImage = "images/systemGroup/".$systemId."/systemGroup_".$systemId.".jpg";
                $newImage = "images/systemGroup/".$newSystemId;

                $folderCreated =  mkdir($newImage);

            if ($folderCreated) {
                $copyImage = copy($oldImage, $newImage."/systemGroup_".$newSystemId.".jpg");
                if ($copyImage) {
                     $componentTraits['value'] = str_replace($systemId, $newSystemId, $componentTraits['value']);
                }
            }

                unset($componentTraits["id"]);
                $this->insertTable($componentTraits, "SQ_traits");
        }

        foreach ($menuComponents as $eachMenu) {
            $menuWhere['grouped_under'] = $eachMenu['id'];
            $menuWhere['system_id'] = $systemId;

            unset($eachMenu['id']);

            $eachMenu['system_id'] = $newSystemId;

            // inserting new menu
            $newMenuId = $this->insertTable($eachMenu, "SQ_components");

            // Adding Traits For that New Menu Id

            $allTraits = $this->getWhereTable(array("component_id" => $menuWhere['grouped_under'] ), "SQ_traits");


            foreach ($allTraits as $componentTraits) {
                $componentTraits["component_id"] = $newMenuId;
                unset($componentTraits["id"]);
                $this->insertTable($componentTraits, "SQ_traits");
            }

            $componentsAll = $this->getWhereTable($menuWhere, "SQ_components");

            foreach ($componentsAll as $components) {
                unset($where);
                $where['component_id'] = $oldComponentId = $components["id"];
                $where['system_id']  = $systemId;
                $optionsDetails = $this->getWhereTable($where, "SQ_new_options");
                $allTraits = $this->getWhereTable(array("component_id" => $components["id"] ), "SQ_traits");

                $components['system_id'] = $newSystemId;
                $components['grouped_under'] = $newMenuId;

                if ($components["id"] ==  $systemId) {
                    unset($components["id"]);
                    $componentId = $newSystemId;
                } else {
                    unset($components["id"]);
                    $componentId = $this->insertTable($components, "SQ_components");
                }

                $this->addComponentsCosts( $systemId, $newSystemId, $oldComponentId, $componentId );
                $this->addComponentsParts( $systemId, $newSystemId, $oldComponentId, $componentId );

                if ($components['type'] == 'Color') {
                    $this->addColorGroup($newSystemId, $where['component_id'], $componentId);
                }
                                    
                       
                foreach ($allTraits as $componentTraits) {
                    $componentTraits["component_id"] = $componentId;
                    unset($componentTraits["id"]);
                    $this->insertTable($componentTraits, "SQ_traits");
                }

                foreach ($optionsDetails as $options) {
                     unset($where['part_id']);
                     $where['option_id'] = $oldOptionId =  $options['id'];
                     $optionTraits = $this->getWhereTable($where, "SQ_options_traits");
                     $optionPartsDetails = $this->getWhereTable($where, "SQ_parts");

                     $where['part_id'] = 0;
                     $optionCostsDetails = $this->getWhereTable($where, "SQ_costs");
                               
                     $options['system_id'] = $newSystemId;
                     $options['component_id'] = $componentId;

                     unset($options["id"]);
                     $optionId = $this->insertTable($options, "SQ_new_options");

                    foreach ($optionTraits as $traits) {
                        $traits['system_id'] = $newSystemId;
                        $traits['component_id'] = $componentId;
                        $traits['option_id'] = $optionId;

                        //$oldOptionTraitsId = $traits["id"];
                        unset($traits["id"]);

                        $newOptionTraitsId = $this->insertTable($traits, "SQ_options_traits");

                        if ($traits['name'] == 'imgUrl') {
                             $oldFilePath = "images/systems/".$oldOptionId."/systems_".$oldOptionId.".jpg";
                             $newFilePath =  "images/systems/".$optionId;
                         
                             mkdir($newFilePath);
                             $isCopied = copy($oldFilePath, $newFilePath."/systems_".$optionId.".jpg");

                            if ($isCopied) {
                                $newUpdate['value'] =  str_replace($oldOptionId, $optionId, $traits['value']);

                                $updateStatement = $this->pdo->update($newUpdate)
                                                     ->table("SQ_options_traits")
                                                     ->where('id', '=', $newOptionTraitsId);
                                $updateStatement->execute();
                            }
                        }
                    }

                    foreach ($optionCostsDetails as $costs) {
                        $costs['system_id'] = $newSystemId;
                        $costs['component_id'] = $componentId;
                        $costs['option_id'] = $optionId;

                        unset($costs["id"]);
                        $costId = $this->insertTable($costs, "SQ_costs");
                    }


                    foreach ($optionPartsDetails as $parts) {
                           $parts['system_id'] = $newSystemId;
                           $parts['component_id'] = $componentId;
                           $parts['option_id'] = $optionId;

                           $where['part_id'] = $parts['id'];

                           unset($parts["id"]);
                           $partId = $this->insertTable($parts, "SQ_parts");

                           $costsDetails = $this->getWhereTable($where, "SQ_costs");

                        foreach ($costsDetails as $costs) {
                             $costs['system_id'] = $newSystemId;
                             $costs['component_id'] = $componentId;
                             $costs['option_id'] = $optionId;
                             $costs['part_id'] = $partId;

                             $where['cost_id'] = $costs['id'];

                             unset($costs["id"]);
                             $costId = $this->insertTable($costs, "SQ_costs");

                              $formulaDetails = $this->getWhereTable($where, "SQ_formula_parts");
                              unset($where["cost_id"]);

                            foreach ($formulaDetails as $formula) {
                                $formula['system_id'] = $newSystemId;
                                $formula['component_id'] = $componentId;
                                $formula['option_id'] = $optionId;
                                $formula['part_id'] = $partId;
                                $formula['cost_id'] = $costId;

                                unset($formula["id"]);
                                $this->insertTable($formula, "SQ_formula_parts");
                            }
                        }
                    }
                }
            }
        }

        return $newSystemDetails;
    }


    /**
     * Copying Images
     *
     * @param oldSystemId $oldSystemId old System Name
     * @param newSystemId $newSystemId new sysem Id
     *
     */
    public function copyImages($oldSystemId, $newSystemId)
    {

         $oldImageFile = "images/systems/".$oldSystemId."/systems_".$oldSystemId.".jpg";
         $filePathExists = file_exists($oldImageFile);
        if ($filePathExists) {
            $newPath =  "images/systems/".$newSystemId;
            $folderCreated =  mkdir($newPath);

            if ($folderCreated) {
                copy($oldImageFile, $newPath."/systems_".$newSystemId.".jpg");
            }
        }
    }

    /**
     * Getting a Table details
     *
     * @param table    $table    table name
     * @param systemId $systemId system Id
     *
     * @return array
     */
    public function getTableDetails($table, $systemId)
    {

        $sql = $this->pdo->select(['*'])->from($table);
        $sql->where($table.'.system_id', '=', $systemId);
        $sql->where($table.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Getting a Table details via where
     *
     * @param data      $data      where conditions
     * @param tableName $tableName table name
     *
     * @return array
     */
    public function getWhereTable($data, $tableName)
    {

        $sql = $this->pdo->select(["*"])->from($tableName);

        foreach ($data as $key => $value) {
                  $sql->where($tableName.".".$key, '=', $value);
        }

        $sql->where($tableName.'.active', '=', 1);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return $data;
    }

    /**
     * Inserting a Table details
     *
     * @param config    $config    array key value
     * @param tableName $tableName table name
     *
     * @return boolean
     */
    public function insertTable($config, $tableName)
    {


        $config['active']  = 1;
        $insertStatement = $this->pdo->insert(array_keys($config))
            ->into($tableName)
            ->values(array_values($config));


        return $insertStatement->execute(true);
    }


    /**
     * Copying sample Doors
     *
     * @param systemId    $systemId
     * @param newSystemId $newSystemId
     * @param type        $type
     *
     * @return array
     */
    public function copySampleDoors($systemId, $newSystemId, $type)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_components');
        $sql->where('SQ_components.active', '=', 1);
        $sql->where('SQ_components.system_id', '=', $systemId);
        $sql->where('SQ_components.grouped_under', '=', 0);
        $sql->where('SQ_components.type', '=', $type);

        $stmt = $sql->execute();

        $allOldSystems = $stmt->fetchAll();

        foreach ($allOldSystems as $systems) {
            $oldComponentId = $systems['id'];
            $systems['system_id'] = $newSystemId;
            unset($systems['id']);

            $newComponentId = $this->insertTable($systems, "SQ_components");

            $allTraits = $this->getWhereTable(array(
                 "component_id" => $oldComponentId,
                 "active"       => 1,
            ), "SQ_traits");


            foreach ($allTraits as $componentTraits) {
                $componentTraits["component_id"] = $newComponentId;
                unset($componentTraits["id"]);
                $this->insertTable($componentTraits, "SQ_traits");
            }
        }
    }


    /**
     * Copying Panel Ranges
     *
     * @param systemId    $systemId
     * @param newSystemId $newSystemId
     * @param type        $type
     *
     * @return array
     */
    public function copyPanelRanges($systemId, $newSystemId)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_panel_ranges');
        $sql->where('SQ_panel_ranges.active', '=', 1);
        $sql->where('SQ_panel_ranges.system_id', '=', $systemId);

        $stmt = $sql->execute();

        $oldPanelRanges = $stmt->fetchAll();

        foreach ($oldPanelRanges as $panels) {
            $panels['system_id'] = $newSystemId;
            unset($panels['id']);

            $this->insertTable($panels, "SQ_panel_ranges");
        }
    } 


    public function addComponentsCosts($systemId, $newSystemId, $componentId, $newComponentId)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_costs');
        $sql->where('SQ_costs.active', '=', 1);
        $sql->where('SQ_costs.system_id', '=', $systemId);
        $sql->where('SQ_costs.component_id', '=', $componentId);
        $sql->where('SQ_costs.part_id', '=', 0);
        $sql->where('SQ_costs.option_id', '=', 0);

        $stmt = $sql->execute();

        $allCosts = $stmt->fetchAll();

        foreach ($allCosts as $costs) {
            $costs['system_id'] = $newSystemId;
            $costs['component_id'] = $newComponentId;
            unset($costs['id']);

            $this->insertTable($costs, "SQ_costs");
        }
    }

    public function addComponentsParts($systemId, $newSystemId, $componentId, $newComponentId)
    {

        $sql = $this->pdo->select(['*'])->from('SQ_parts');
        $sql->where('SQ_parts.active', '=', 1);
        $sql->where('SQ_parts.system_id', '=', $systemId);
        $sql->where('SQ_parts.component_id', '=', $componentId);
        $sql->where('SQ_parts.option_id', '=', 0);

        $stmt = $sql->execute();

        $allParts = $stmt->fetchAll();

        foreach ($allParts as $parts) {
            $parts['system_id'] = $newSystemId;
            $parts['component_id'] = $newComponentId;
            unset($parts['id']);

            $this->insertTable($parts, "SQ_parts");
        }
    }
}
