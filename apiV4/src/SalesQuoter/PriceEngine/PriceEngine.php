<?php

namespace SalesQuoter\PriceEngine;

use SalesQuoter\CustomAbstractCrud;

class PriceEngine
{

    
    public function calculatePrice($cacheData, $componentsArray, $uxRoutesData)
    {
 
        $totalPrice = 0;
       //$checkOptionName = array("width", "height", "panels", "swings");
        $parsedData  = unserialize($cacheData);
        $buildOptionsArray = array();
        $optionFormulaDetails = array();


       // Parse Sending Components array , assign all sending options in one array

        for ($count = 0; $count < sizeof($componentsArray); $count++) {
             $optionsArray  =  $componentsArray[$count]["options"];
            foreach ($optionsArray as $options) {
                //$buildOptionsArray[sizeof($buildOptionsArray)]['id'] = $options["id"];
                $buildOptionsArray[] = $options["id"];
            }
        }

        $optionsValue = $this->getOptionsValue(unserialize($uxRoutesData), $buildOptionsArray)  ;


       // Now parse in systemPrice cached data to find aboved selected options formula

        for ($count=0; $count < sizeof($parsedData); $count++) {
             $cachedOptions = $parsedData[$count]['options'];

            foreach ($cachedOptions as $optons) {
                if (in_array($optons['id'], $buildOptionsArray)) {
                     $buildDetails['option'] = $optons["name"];
                     $formulaDetils =  $this->parseFormulaPrice($optons['parts'], $optionsValue) ;
                     $totalPrice = $totalPrice + $formulaDetils["formula"];

                     $buildDetails['parts'] = $formulaDetils["parts"];
                     $optionFormulaDetails[] = $buildDetails;
                }
            }
        }

        return array("price" => $totalPrice, "details" => $optionFormulaDetails);
    }

    /**
     *
     * @SuppressWarnings(PHPMD.EvalExpression)
     *
     */
    public function parseFormulaPrice($parts, $optionsValue)
    {

        $formula = 0;
        $details = array();

        for ($count=0; $count < sizeof($parts); $count++) {
            $details["parts"]  = $parts[$count]["name"];

            $costs  = $parts[$count]['costs'];

            foreach ($costs as $cost) {
                $costArray['costs'] = $cost['name'];
                $costArray['value'] = 0;

                $formulaCosts = $cost["formula_parts"];
                foreach ($formulaCosts as $formulaC) {
                 // Replaceing Options value in Formula

                    foreach ($optionsValue as $key => $value) {
                        $formulaC['formula']  =  str_replace($key, $value, $formulaC['formula']);
                    }

                    $formula = $formula +  eval('return '.$formulaC['formula'].';');
                    ;
                    $costArray['value'] = $formula;
                }

                $totalCost[] = $costArray;
            }

            $details["costs"]  =  $totalCost ;
            $allDetails[] = $details;
        }

        return array( "formula" => $formula, "parts" => $allDetails );
    }

   // parse in uxRoutes data build array with option name with their traits value

    public function getOptionsValue($uxRoutesData, $buildOptionsArray)
    {

         $optionsValue  = array();

        foreach ($uxRoutesData as $components) {
            $options = $components["options"];
            foreach ($options as $option) {
                if (in_array($option['id'], $buildOptionsArray)) {
                    if (sizeof($option['traits']) > '0') {
                        $optionsValue[$option['name']] = $option['traits'][0]['value'];
                    } else {
                        $optionsValue[$option['name']] = 0;
                    }
                }
            }
        }

         return $optionsValue;
    }
}
