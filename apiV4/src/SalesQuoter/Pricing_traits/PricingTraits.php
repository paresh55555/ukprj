<?php

namespace SalesQuoter\Pricing_traits;

use SalesQuoter\CustomAbstractCrud;

class PricingTraits extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "name");
        $config = array("table" => 'SQ_pricing_traits', 'fields' =>$fields);
        parent::__construct($config);
    }
}
