<?php

namespace SalesQuoter\Cart;

use SalesQuoter\CustomAbstractCrud;
/**
 *  ComponentTypes Class
 *
 */
class Cart extends CustomAbstractCrud
{

    /**
     * Class constructor
     *
     */
    public function __construct()
    {

        $fields = array("id", "name", "kind");
        $config = array("table" => 'SQ_layout', 'fields' =>$fields);
        parent::__construct($config);
    }
}
