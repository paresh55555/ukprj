<?php

namespace SalesQuoter\Tax;

class SalesTax
{

    public function getRate( $zip, $city, $country ) {

        $client = \TaxJar\Client::withApiKey("16955df3fdfb7d449137f0ed68e94cb4");

        try {

	        $rates = $client->ratesForLocation($zip, [
	          'city' => $city,
	          'country' => $country
	        ]);

	        $rate = $rates->combined_rate;

	        return [ "status" => "success", "msg" => '' , "rate" =>  $rate ];

        }catch ( Exception $exp ) {

        	return [ "status" => "error",  "msg" => $exp->getMessage(),  "rate" =>  '' ];
        }

    }
}
