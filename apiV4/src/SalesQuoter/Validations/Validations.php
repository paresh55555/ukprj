<?php

namespace SalesQuoter\Validations;

use SalesQuoter\CustomAbstractCrud;

class Validations extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "trait_id", "name");
        $config = array("table" => 'SQ_validations', 'fields' =>$fields);
        parent::__construct($config);
    }
}
