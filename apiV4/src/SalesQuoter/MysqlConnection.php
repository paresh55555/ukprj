<?php

namespace SalesQuoter;

/**
 *
 * @SuppressWarnings(PHPMD)
 *
 * Class MysqlConnection
 *
*/
class MysqlConnection
{

    protected $server = 'localhost';
    protected $user = 'salesQuoter';
    protected $pass = 'QUu3!YjTdjJy8Ps$';
    protected $database = 'windowSystems';

   /**
     * function connect_db
     *
     * @return db instance
    *
    */
   //@codingStandardsIgnoreLine
    public function connect_db()
    {
        $dbhandle = mysqli_connect($this->server, $this->user, $this->pass, $this->database);

        if ($dbhandle) {
            return $dbhandle;
        } else {
            throw new Exception('Unable to connect to MySQL');
        }
    }

    /**
     * function getDatabase
     *
     * @return db name
    */
    public function getDatabase()
    {
        //phpunit -> export SITE_NAME=panoramicdoors; phpunit tests/Custom/<your test>

        if ($_SERVER['SITE_NAME'] == 'panoramicdoors') {
            return 'panoramic';
        } elseif ($_SERVER['SITE_NAME'] == 'panoramicdoorsUK') {
            return 'panoramicUK';
        } elseif ($_SERVER['SITE_NAME'] == 'panoramicGroup') {
            return 'panoramicGroup';
        } elseif ($_SERVER['SITE_NAME'] == 'panoramicdoorsUKtrade') {
            return 'panoramicUKtrade';
        } elseif ($_SERVER['SITE_NAME'] == 'magnalineSystems') {
            return 'magnaline';
        } elseif ($_SERVER['SITE_NAME'] == 'productionCopy') {
            return 'productioncopy';
        } elseif ($_SERVER['SITE_NAME'] == 'smartdoors') {
            return 'smartdoors';
        } elseif ($_SERVER['SITE_NAME'] == 'pd') {
            return 'pd';
        } elseif ($_SERVER['SITE_NAME'] == 'ces') {
            return 'ces';
        } elseif ($_SERVER['SITE_NAME'] == 'service') {
            return 'service';
        } elseif (preg_match('/^demo*/', $_SERVER['SITE_NAME'])) {
            return $_SERVER['SITE_NAME'];
        } else {
            return 'panoramic';
        }

        throw new Exception('Failed to Select a Database');
    }

    /**
     * function connect_db_pdo
     *
     * @return db instance
    */
    //@codingStandardsIgnoreLine
    public function connect_db_pdo()
    {
        $database = $this->getDatabase();
        $dsn = 'mysql:host='.$this->server.';dbname='.$database.';charset=utf8';
        
        $pdo = new \Slim\PDO\Database($dsn, $this->user, $this->pass);

        return $pdo;
    }

    /**
     * function connect_db_pdo
     *
     * @return db instance
     */
    //@codingStandardsIgnoreLine
    public function connect_db_pdo_special($database = null)
    {
        if (empty($database )) {
            $database = $this->getDatabase();
        }

        $dsn = 'mysql:host='.$this->server.';dbname='.$database.';charset=utf8';

        $pdo = new \Slim\PDO\Database($dsn, $this->user, $this->pass);

        return $pdo;
    }

    /**
     * function changeServer
     *
     * @param server $server server info
     *
    */
    public function changeServer($server)
    {
        $this->server = $server;
    }
}
