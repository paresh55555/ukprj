<?php

namespace SalesQuoter\Options;

use SalesQuoter\CustomAbstractCrud;

class Options extends CustomAbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "system_id", "component_id", "name", "allowDeleted","active");
        $config = array("table" => 'SQ_new_options', 'fields' =>$fields);
        parent::__construct($config);
    }


    public function getOptionTraitsBYComponents($componentId)
    {


        $sql = $this->pdo->select(["SQ_new_options.name,SQ_new_options.id"])->from("SQ_new_options");

        $sql->where('SQ_new_options.component_id', '=', $componentId);
        $sql->where('SQ_new_options.active', '=', 1);

        $stmt = $sql->execute();
        $data = $stmt->fetchAll();

        for ($count=0; $count < sizeof($data); $count++) {
              $data[$count]['traits'] = $this->getOptionTratis($data[$count]['id']);
        }

        return $data;
    }


    public function getOptionTratis($optionId)
    {

        $sql = $this->pdo->select(["*"])->from("SQ_options_traits");

        $sql->where('SQ_options_traits.option_id', '=', $optionId);
        $sql->where('SQ_options_traits.active', '=', 1);

        $stmt = $sql->execute();
        return  $stmt->fetchAll();
    }
}
