<?php

namespace SalesQuoter\Guest;

use SalesQuoter\AbstractCrud;

class Guest extends AbstractCrud
{

    public function __construct()
    {

        $fields = array("id", "name", "email", "zip", "phone","leadType");
        $config = array("table" => 'guest', 'fields' =>$fields);
        parent::__construct($config);
    }

    public function newGuest($data)
    {

        if (strlen($data['email']) < '4') {
            return array('status' => 'error', 'msg' => 'Given Email address in not Valid');
        } elseif (strlen($data['name']) < '2') {
            return array('status' => 'error', 'msg' => 'Given Name in not Valid');
        } elseif (strlen($data['zip']) < '2') {
            return array('status' => 'error', 'msg' => 'Given zipcode in not Valid');
        }


        $guestData = $this->create($data);
        $guestId = $guestData['id'];

        $token = $this->createToken($guestId);

        return array("id" => $guestId, "type" => "guest", "token"=> $token );
    }

    public function createToken($guestId)
    {

        $data['type'] = 'guest';
        $data['typeID'] = $guestId;
        $data['token'] =  uniqid();
        $data['expires'] = 'now() + INTERVAL 5 DAY';
        $data['super'] =  0;

        $insertStatement = $this->pdo->insert(array_keys($data))
            ->into('authorized')
            ->values(array_values($data));

        $insertStatement->execute(true);

        return $data['token'];
    }
}
