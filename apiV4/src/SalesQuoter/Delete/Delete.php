<?php

namespace SalesQuoter\Delete;

use SalesQuoter\MysqlConnection;

class Delete
{

    protected $pdo;
    protected $allowedTables = array("users" => "users",
                                      "fixed" => "optionsFixed",
                                      "frame" => "optionsSized",
                                      "panelRanges" => "panelRanges",
                                      "glass" => "optionsGlass",
                                      "percent" => "optionsPercentage",
                                      "systems" => "SQ_systems",
                                      "validations" => "SQ_validations",
                                      "components" => "SQ_components",
                                      "traits" => "SQ_traits",
                                      "permissions" => "SQ_permissions",
                                      "componentTypes" => "SQ_component_types",
                                      "pricingTraits" => "SQ_pricing_traits",
                                      "options" => "SQ_new_options",
                                      "optionsTraits" => "SQ_options_traits",
                                      "parts" => "SQ_parts",
                                      "costs" => "SQ_costs",
                                      "formulaParts" => "SQ_formula_parts"
                                );

    public function __construct()
    {
        $this->pdo = $this->mysqlConnection();
    }

    public function getAllEndPoint()
    {
        
        return array_keys($this->allowedTables);
    }

    public function hardDelete($tableName, $pId)
    {

        $checkTableNameExists = $this->checkTableNameExists($tableName);
        if ($checkTableNameExists === false) {
             return array('status' => 'error', 'message' => "Endpoint not Exists");
        }

        if (strlen($pId) == '0') {
             return array('status' => 'error', 'message' => "Id can not be Empty");
        }

        $allTables = $this->allowedTables;
        $deleteStatement = $this->pdo->delete()
            ->from($allTables[$tableName])
            ->where('id', '=', $pId);

        $affectedRows = $deleteStatement->execute();

        $result = array("status" => "success");

        if (empty($affectedRows)) {
            $result = array("status" => "error", "message" => "ID not exists or already Deleted");
        }
     
        return $result;
    }

    public function checkTableNameExists($tableName)
    {

        $allTableName =  array_keys($this->allowedTables);
        if (in_array($tableName, $allTableName)) {
            return true;
        }

        return false;
    }

    public function getTableAll($tableName, $items, $pageNo)
    {
 
        $checkTableNameExists = $this->checkTableNameExists($tableName);
        if ($checkTableNameExists === false) {
             return array('status' => 'error', 'message' => "Endpoint not Exists");
        }

        $allTableName =  $this->allowedTables;
        $actualTable = $allTableName[$tableName];

        $sql = $this->pdo->select(['*'])->from($actualTable);
        $sql->where('active', '=', 0);
        $sql->limit($items, $pageNo*$items);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return array("status" => "success" ,"total_items"=> $this->getTableAllCount($actualTable), "response_data" =>  $data );
    }


    public function getTableAllCount($tableName)
    {

        $sql = $this->pdo->select(['*'])->from($tableName);
        $sql->where('active', '=', 0);
        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        return sizeof($data);
    }


    protected function mysqlConnection()
    {
        $this->mysqlConnection = new MysqlConnection();
        $pdo = $this->mysqlConnection->connect_db_pdo();

        return $pdo;
    }
}
