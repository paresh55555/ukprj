<?php

namespace SalesQuoter\PanelRanges;

use SalesQuoter\CustomAbstractCrud;

/**
 *  Class PanelRanges
 *
 */
class PanelRanges extends CustomAbstractCrud
{

   /**
    * Constructor
    */
    public function __construct()
    {

        $fields = array("id", "system_id", "panels", "min_width", "max_width", "min_height", "max_height", "modified", "active");
        $config = array("table" => 'SQ_panel_ranges', 'fields' => $fields);
        parent::__construct($config);
    }

    /**
     * function getByHeightWidth
     *
     * @param systemId $systemId systems id
     * @param width    $width    width of panel ranges
     * @param height   $height   height of panel ranges
     *
     * @return array
     */
    public function getByHeightWidth($systemId, $width, $height)
    {

        $sql = $this->pdo->select(["panels"])->from("SQ_panel_ranges");
        $sql->where('SQ_panel_ranges.system_id', '=', $systemId);
        $sql->where('SQ_panel_ranges.active', '=', 1);

        $sql->where('SQ_panel_ranges.min_width', '<=', $width);
        $sql->where('SQ_panel_ranges.max_width', '>=', $width);

        $sql->where('SQ_panel_ranges.min_height', '<=', $height);
        $sql->where('SQ_panel_ranges.max_height', '>=', $height);

        $stmt = $sql->execute();

        $data = $stmt->fetchAll();

        $returnData = array();

        foreach ($data as $d) {
            $returnData[] = $d['panels'];
        }

        return $returnData;
    }
}
