<?php

namespace SalesQuoter;

/**
 *  Token Class
 *
 */

class Token
{
    public $decoded;

    /**
     * function hydrate
     *
     * @param decoded $decoded    string
     *
     */
    public function hydrate($decoded)
    {
        $this->decoded = $decoded;
    }

    /**
     * function hasScope
     *
     * @param scope $scope  array
     *
     */
    public function hasScope(array $scope)
    {
        return !!count(array_intersect($scope, $this->decoded->scope));
    }
}
