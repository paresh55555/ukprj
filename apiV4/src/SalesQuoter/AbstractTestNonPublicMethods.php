<?php

namespace SalesQuoter;

/**
 *
 *  @SuppressWarnings(PHPMD)
 *
 */
abstract class AbstractTestNonPublicMethods extends \PHPUnit_Framework_TestCase
{
    protected $testClass;


    /**
     *  Sets up local protected variable $reflection
     *
     */
    public function setUp()
    {
        $this->reflection = new \ReflectionClass($this->testClass);
    }

    /**
     *
     */
    protected function tearDown()
    {
        $refl = new \ReflectionObject($this);
        foreach ($refl->getProperties() as $prop) {
            if (!$prop->isStatic() && 0 !== strpos($prop->getDeclaringClass()->getName(), 'PHPUnit_')) {
                $prop->setAccessible(true);
                $prop->setValue($this, null);
            }
        }
    }

    /**
     * @param string $method
     *
     * @return mixed
     */
    public function getMethod($method)
    {
        $method = $this->reflection->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }

    /**
     * @param string $property
     *
     * @return mixed
     */
    public function getProperty($property)
    {
        $property = $this->reflection->getProperty($property);
        $property->setAccessible(true);

        return $property->getValue($this->testClass);
    }

    /**
     * @param string $property
     * @param mixed  $value
     *
     * @return mixed
     */
    public function setProperty($property, $value)
    {
        $property = $this->reflection->getProperty($property);
        $property->setAccessible(true);

        return $property->setValue($this->testClass, $value);
    }


    /**
     * @param string $property
     *
     * @return mixed
     */
    public function getConstant($property)
    {
        return $this->reflection->getConstant($property);
    }

    /**
     * Returns a Mock object with specified methods stubbed
     *
     * @param string $class   Fully qualified class name (with namespace)
     * @param mixed  $methods Single method as string or array
     *
     * @return Mock
     */
    public function getInstance($class, $methods = null)
    {
        if ($methods != null && !is_array($methods)) {
            $methods = array($methods);
        }

        return $this->getMockBuilder($class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }
}
