<?php

define ('JWT_SECRET', 'asd8fywerifh989w8asfaif98rq3oweiaw8erweir');
define ('SQPREFIX', 'SQ-admin');
define ('AWS_BUCKET', 'salesquoter-mediafiles');
define ('AWS_ACCESS_KEY', 'AKIAJYESVKI5P7D3G3EA');
define ('AWS_SECRET_KEY', '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');
define ('AWS_URL', 'https://s3-us-west-2.amazonaws.com/');

error_reporting(0);

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $file = __DIR__ . $_SERVER['REQUEST_URI'];
    if (is_file($file)) {
        return false;
    }
}


require __DIR__ . '/../vendor/autoload.php';

//session_start();/**/

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';


$app = new \Slim\App($settings);


//$app->add(function ($request, $response, $next) {
//
//    $token = '';
//    $allGetVars = $request->getQueryParams();
//    if (!empty($allGetVars['token'])) {
//        $token = $allGetVars['token'];
//    }
//
//    if ($token != "WIiOjM0NTY3ODiIxMkjM0NTY3ODwIiwi") {
//        $body = $response->getBody();
//        $body->write('{"authorized": "nope"}');
//
//        return $response->withHeader('Content-Type',
//            'application/json'
//        )->withBody($body);
//    } else {
//        $response = $next($request, $response);
//    }
//
//    return $response;
//});


// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes

require __DIR__ . '/../src/Routes/historyRoutes.php';
require __DIR__ . '/../src/Routes/accountRoutes.php';
require __DIR__ . '/../src/Routes/sampleDataRoutes.php';
require __DIR__ . '/../src/Routes/listBuilderRoutes.php';
require __DIR__ . '/../src/Routes/listOrderRoutes.php';
require __DIR__ . '/../src/Routes/listQuoteRoutes.php';
require __DIR__ . '/../src/Routes/cartRoutes.php';
require __DIR__ . '/../src/Routes/cartColumnRoutes.php';
require __DIR__ . '/../src/Routes/cartItemRoutes.php';
require __DIR__ . '/../src/Routes/cartLayoutRoutes.php';
require __DIR__ . '/../src/Routes/priceChangeRoutes.php';
require __DIR__ . '/../src/Routes/cartPriceRoutes.php';
require __DIR__ . '/../src/Routes/itemRoutes.php';

require __DIR__ . '/../src/Routes/quoteBuilderRoutes.php';
require __DIR__ . '/../src/Routes/quoteBuilderColumnRoutes.php';
require __DIR__ . '/../src/Routes/quoteBuilderItemRoutes.php';
require __DIR__ . '/../src/Routes/quoteBuilderLayoutRoutes.php';

require __DIR__ . '/../src/Routes/imagesRoutes.php';
require __DIR__ . '/../src/Routes/tokenRoutes.php';
require __DIR__ . '/../src/Routes/usersRoutes.php';
require __DIR__ . '/../src/Routes/modulesRoutes.php';
require __DIR__ . '/../src/Routes/companiesRoutes.php';
require __DIR__ . '/../src/Routes/windowSystemRoutes.php';
require __DIR__ . '/../src/Routes/reportsRoutes.php';
require __DIR__ . '/../src/Routes/pipelinerRoutes.php';
require __DIR__ . '/../src/Routes/siteRoutes.php';
require __DIR__ . '/../src/Routes/quotesRoutes.php';
require __DIR__ . '/../src/Routes/territoriesRoutes.php';
require __DIR__ . '/../src/Routes/validationsRoutes.php';
require __DIR__ . '/../src/Routes/customersRoutes.php';
require __DIR__ . '/../src/Routes/deleteRoutes.php';
require __DIR__ . '/../src/Routes/premadeRoutes.php';
require __DIR__ . '/../src/Routes/panelRangesRoutes.php';
require __DIR__ . '/../src/Routes/siteBrandingRoutes.php';
require __DIR__ . '/../src/Routes/systemRevertRoutes.php';

require __DIR__ . '/../src/Routes/versionRoutes.php';
require __DIR__ . '/../src/Routes/systemRoutes.php';
require __DIR__ . '/../src/Routes/systemPanelRanges.php';
require __DIR__ . '/../src/Routes/componentsTypesRoutes.php';
require __DIR__ . '/../src/Routes/traitsRoutes.php';
require __DIR__ . '/../src/Routes/permissionRoutes.php';
require __DIR__ . '/../src/Routes/pricingTraitsRoutes.php';
require __DIR__ . '/../src/Routes/optionsRoutes.php';
require __DIR__ . '/../src/Routes/optionTraitsRoutes.php';
require __DIR__ . '/../src/Routes/optionPartsRoutes.php';
require __DIR__ . '/../src/Routes/optionPartsCostsRoutes.php';
require __DIR__ . '/../src/Routes/formulaPartsRoutes.php';
require __DIR__ . '/../src/Routes/uxRoutes.php';
require __DIR__ . '/../src/Routes/newUsersRoutes.php';
require __DIR__ . '/../src/Routes/sytemPublishRoutes.php';
require __DIR__ . '/../src/Routes/componentPartsRoutes.php';
require __DIR__ . '/../src/Routes/componentCostsRoutes.php';

require __DIR__ . '/../src/Routes/newSystemsRoutes.php';
require __DIR__ . '/../src/Routes/systemGroupsRoutes.php';
require __DIR__ . '/../src/Routes/priceEngineRoutes.php';
require __DIR__ . '/../src/Routes/guestRoutes.php';
//Must be last as it has catchall routes
require __DIR__ . '/../src/Routes/generalRoutes.php';
require __DIR__ . '/../src/Routes/defaultSiteRoutes.php';

require __DIR__ . '/../src/Routes/serviceOrdersRoutes.php';
require __DIR__ . '/../src/Routes/serviceDoorsRoutes.php';
require __DIR__ . '/../src/Routes/serviceOrderDocsRoutes.php';
require __DIR__ . '/../src/Routes/serviceOrderCustomersRoutes.php';
require __DIR__ . '/../src/Routes/serviceDoorTraitsRoutes.php';
require __DIR__ . '/../src/Routes/serviceDoorNotesRoutes.php';
require __DIR__ . '/../src/Routes/serviceDoorCustomersRoutes.php';
require __DIR__ . '/../src/Routes/serviceJobsRoutes.php'; 
require __DIR__ . '/../src/Routes/serviceJobAddressRoutes.php'; 
require __DIR__ . '/../src/Routes/serviceJobCustomerRoutes.php'; 
require __DIR__ . '/../src/Routes/serviceJobDoorRoutes.php'; 
require __DIR__ . '/../src/Routes/serviceCustomersRoutes.php'; 
require __DIR__ . '/../src/Routes/serviceJobImagesRoutes.php';
require __DIR__ . '/../src/Routes/serviceJobNotesRoutes.php';
require __DIR__ . '/../src/Routes/serviceJobFilesRoutes.php'; 
require __DIR__ . '/../src/Routes/emailRoutes.php';
require __DIR__ . '/../src/Routes/serviceJobsEmailRoutes.php';
require __DIR__ . '/../src/Routes/taxRoutes.php';

$container = $app->getContainer();
   
$container['errorHandler'] = function ($container) {

    return function ($request, $response, $exception) use ($container) {

    	$error_response = array('status' => 'error', 'message' => $exception->getMessage(), 'line' => $exception->getLine() ,
    		                     'file' => $exception->getFile());

        return $container['response']->withStatus(500)
                                     ->withHeader('Content-Type', 'application/json')
                                     ->write(json_encode($error_response));
    };
};


$container['notAllowedHandler'] = function ($container) {
    return function ($request, $response, $methods) use ($container) {
   
        $error_response = array("status" => "error", "message" => "Method must be one of: " . implode(', ', $methods));

        return $container['response']->withStatus(405)
                                     ->withHeader('Content-type', 'application/json')
                                     ->write( json_encode($error_response) );
    };
};


$container['notFoundHandler'] = function ($container) {
    return function ($request, $response) use ($container) {

        $error_response = array("status" => "error", "message" => "Invalid Routes, Please check URl before API Call");

        return $container['response']->withStatus(404)
                                     ->withHeader('Content-Type', 'application/json')
                                     ->write( json_encode($error_response) );
    };
};



 $app->run();

