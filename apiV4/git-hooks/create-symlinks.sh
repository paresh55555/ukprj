#!/bin/bash

#this file should ONLY be executed from a project's root directory

PROJECT_ROOT_DIR=`pwd`

# if an override file exists in the project's root dir, then give it priority
OVERRIDE_CONFIG_FILE=${PROJECT_ROOT_DIR}/.git-hooks-config

if [ -e $OVERRIDE_CONFIG_FILE ]; then
    source $OVERRIDE_CONFIG_FILE
fi

COMMIT_MSG_PATH=./git-hooks/commit-msg.sh
COMMIT_MSG_LINK=.git/hooks/commit-msg

create_hooks_dir_if_not_exists() {
    if [ ! -d "${PROJECT_ROOT_DIR}/.git/hooks" ]; then
        mkdir ${PROJECT_ROOT_DIR}/.git/hooks
    else 
        rm ${PROJECT_ROOT_DIR}/.git/hooks/*
    fi
}

remove_all_symlinks() {
    PROJECT_HOOK_DIR=$1
    find ${PROJECT_HOOK_DIR} -maxdepth 1 -lname '*' -exec rm {} \;
}

create_hooks_dir_if_not_exists


ln -s ${PROJECT_ROOT_DIR}/git-hooks/commit-msg.sh ${PROJECT_ROOT_DIR}/.git/hooks/commit-msg
