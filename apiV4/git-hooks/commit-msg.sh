#!/bin/bash
#
# git prepare-commit-msg hook for automatically prepending an issue key
# from the current branch name to commit messages.

# parse default config

BRANCH_NAME=$(git symbolic-ref --short -q HEAD)

if [[ $GIT_BRANCH_NAME_PATTERN == "git-flow" ]] && [[ ${BRANCH_NAME} == *"feature"* || ${BRANCH_NAME} == *"bugfix"* || ${BRANCH_NAME} == *"hotfix"* ]]; then
    # trim '(feature/|bugfix/|hotfix/)' off of the branch name, leaving the JIRA ticket. Ex: EI-111
    ISSUE_KEY=`echo ${BRANCH_NAME}| cut -d'/' -f2`
elif [[ $GIT_BRANCH_NAME_PATTERN == "jira-long" ]]; then
    ISSUE_KEY=`echo ${BRANCH_NAME}| cut -d'-' -f1,2`
else
    ISSUE_KEY=$BRANCH_NAME
fi

if [ $? -ne 1 ]; then
    # issue key matched from branch prefix, prepend to commit message
    MSG=`cat $1`;
    echo "Adding ${ISSUE_KEY} to you message";
    echo "${ISSUE_KEY}: ${MSG}" > $1
fi
