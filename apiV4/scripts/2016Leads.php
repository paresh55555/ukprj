<?php

require_once ('./emailLeads.php');

$min = date("Y-m-d");

if (empty($argv[1]) or empty($argv[2])  or empty($argv[3])) {
    throw new Exception('Missing URLS or Email');
}

$urls = array ($argv[1], $argv[2]);

$currentYear  = date('Y-m-d', strtotime(date('Y-01-01')));


$config  = array (
    "urls" => $urls,
    "min" =>  "2016-07-22",
    "max" =>   "2016-12-31",
    "emails" => array ($argv[3])
);

$emailLeads = new EmailLeads($config);

$emailLeads->sendEmails();