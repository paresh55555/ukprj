#!/bin/bash

URL=$1
TOKEN=`cat data/token`

MIN=$2;
MAX=$3;

curl -X GET \
    --header 'Accept: application/json' \
    --header "Authorization: Bearer $TOKEN" \
    "$1/api/V4/reports/leads?min=$MIN&max=$MAX&unique=yes"

