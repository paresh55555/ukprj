#!/bin/sh

./getToken.sh https://demo1.salesquoter.com

generate_post_for_email()
{
  cat <<EOF
{
	"personalizations": [{
		"to": [{
			"email": "${TO}"
		}]
	}],
	"from": {
		"email": "${FROM}"
	},
	"subject": "${SUBJECT}",
	"content": [{
		"type": "text/plain",
		"value": "${TEXT}"
	}],
	"attachments" : [{
	    "content": "${FILE1}",
		"type":"text/csv",
		"filename":"pd_deliveried_leads.csv",
		"disposition":"attachment"
	},
	{
	    "content": "${FILE2}",
		"type":"text/csv",
		"filename":"ces_deliveried_leads.csv",
		"disposition":"attachment"
	}]
}
EOF
}

URL1=$1
URL2=$2
EMAIL=$3
PUSH=$4

DAYS_MAX=$((29 - $PUSH))
DAYS_MIN=$((35 - $PUSH))

TEXT_MAX="$DAYS_MAX days ago"
TEXT_MIN="$DAYS_MIN days ago"

MAX=$(date +"%Y-%m-%d" -d "$TEXT_MAX");
MIN=$(date +"%Y-%m-%d" -d "$TEXT_MIN");


SHOW_MAX=$(date +"%m-%d-%Y" -d "$TEXT_MAX");
SHOW_MIN=$(date +"%m-%d-%Y" -d "$TEXT_MIN");

./getDeliveriedLeads.sh $URL1 $MIN $MAX > data/pd_deliveried_leads.csv
./getDeliveriedLeads.sh $URL2 $MIN $MAX > data/ces_deliveried_leads.csv

TO=$EMAIL
SUBJECT="Here Yar Orders Delivered Between ($SHOW_MIN) -- ($SHOW_MAX)"
FROM=admin@salesquoter.com
TEXT="Yar People of Orders are attached\nSarvay them up good\n"

FILE1=`cat data/pd_deliveried_leads.csv | base64`
FILE2=`cat data/ces_deliveried_leads.csv | base64`

generate_post_for_email > "data/generate_post_for_email";


curl --request POST \
  --url https://api.sendgrid.com/v3/mail/send \
  --header 'Authorization: Bearer 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' \
  --header 'Content-Type: application/json' \
  --data @data/generate_post_for_email


