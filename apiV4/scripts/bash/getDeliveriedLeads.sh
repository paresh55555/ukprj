#!/bin/bash

URL=$1
TOKEN=`cat data/token`

MIN=$2;
MAX=$3;


generate_post_for_report()
{
  cat <<EOF
{
	"min":"$MIN",
	"max":"$MAX"
}
EOF
}

generate_post_for_report > "data/postDeliveryData";


curl -X POST \
    --header 'Accept: application/json' \
    --header "Authorization: Bearer $TOKEN" \
    --header 'Content-Type: application/json' \
    --url "$URL/api/V4/reports/email/delivered" \
    --data @data/postDeliveryData