#!/bin/bash

URL=$1
AUTH="ZGVtbzpkZW1v";

curl -X POST \
    --header 'Accept: application/json' \
    --header "Authorization: Basic $AUTH"  \
    "$URL/api/V4/token" | jq -r '.token' > ./data/token
