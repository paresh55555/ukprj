#!/bin/sh

./getToken.sh https://demo1.salesquoter.com

URL1=$1
URL2=$2
EMAIL=$3

MAX=$(date +"%Y-%m-%d" -d '1 days ago');
MIN=$(date +"%Y-%m-%d" -d '1 days ago');

MIN_SHOW=$(date +"%m-%d-%Y" -d '1 days ago');


./getLeads.sh $URL1 $MIN $MAX > data/pd.csv
./getLeads.sh $URL2 $MIN $MAX > data/ces.csv

TO=$EMAIL
SUBJECT="Daily Leads for $MIN_SHOW"
FROM=admin@salesquoter.com
TEXT="Here are the daily leads for $MIN_SHOW\n"

FILE1=`cat data/pd.csv | base64`
FILE2=`cat data/ces.csv | base64`

curl https://cronitor.link/KL0plF/run

generate_post_data()
{
  cat <<EOF
{
	"personalizations": [{
		"to": [{
			"email": "${TO}"
		}]
	}],
	"from": {
		"email": "${FROM}"
	},
	"subject": "${SUBJECT}",
	"content": [{
		"type": "text/plain",
		"value": "${TEXT}"
	}],
	"attachments" : [{
	    "content": "${FILE1}",
		"type":"text/csv",
		"filename":"pd.csv",
		"disposition":"attachment"
	},
	{
	    "content": "${FILE2}",
		"type":"text/csv",
		"filename":"ces.csv",
		"disposition":"attachment"
	}]
}
EOF
}

generate_post_data > "data/postData";


curl --request POST \
  --url https://api.sendgrid.com/v3/mail/send \
  --header 'Authorization: Bearer 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' \
  --header 'Content-Type: application/json' \
  --data @data/postData


curl https://cronitor.link/KL0plF/complete