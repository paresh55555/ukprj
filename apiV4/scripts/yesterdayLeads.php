<?php

require_once ('./emailLeads.php');

$min = date("Y-m-d");

if (empty($argv[1]) or empty($argv[2]) ) {
    throw new Exception('Missing URL or Email');
}

$urls = array ($argv[1]);

$config  = array (
    "urls" => $urls,
    "min" => date("Y-m-d", strtotime($min.' - 1 day')),
    "max" => date("Y-m-d", strtotime($min.' - 1 day')),
    "emails" => array ("$argv[2]")
);

$emailLeads = new EmailLeads($config);

$emailLeads->sendEmails();