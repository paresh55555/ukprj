<?php

require_once ('./emailLeads.php');

$min = date("Y-m-d");

if (empty($argv[1]) or empty($argv[2])  ) {
    throw new Exception('Missing URL or Email');
}

$urls = array ($argv[1]);

$end_week = strtotime("last sunday midnight");
$start_week = strtotime("-6 days",$end_week);

$config  = array (
    "urls" => $urls,
    "min" => date("Y-m-d", $start_week),
    "max" => date("Y-m-d", $end_week),
    "emails" => array ("$argv[2]")
);

$emailLeads = new EmailLeads($config);

$emailLeads->sendEmails();