<?php


require_once('./api.php');
require_once('./email.php');
require_once('./leadsInfo.php');

/*
 * Class for Emailing Leads
 *
 */

class EmailLeads
{
    protected $config;
    protected $required = array("urls", "min", "max", "emails");
    protected $email;
    protected $noLead = false;

    /**
     * EmailLeads constructor.
     * @param array $config
     * @throws Exception
     */
    public function __construct($config = null)
    {
        if (empty($config)) {
            $min = date("Y-m-d");
            $config = array(
                "urls" => array("https://local-flow4.rioft.com", "https://local-flow5.rioft.com"),
                "min" => date("Y-m-d", strtotime($min.' - 30 day')),
                "max" => date("Y-m-d"),
                "emails" => array("fracka@fracka.com")
            );
        }

        $this->config = $config;

        foreach ($this->required as $param) {
            if (empty($this->config[$param])) {
                throw new Exception('Missing '.$param);
            }
        }

        $this->email = new Email();

        if (!empty($config['noLeads'])) {
            $this->noLeads = true;
        }
    }

    /**
     * Does the sending of Emails by building the necessary data
     */
    public function sendEmails()
    {

        $humanMin = date('M/d/Y', strtotime($this->config['min']));
        $humanMax = date('M/d/Y', strtotime($this->config['max']));
        $dateRange = $humanMin." - ".$humanMax;


        foreach ($this->config['urls'] as $url) {
            $this->buildFromURL($url, $dateRange);
        }

        $this->email->setSubject("Leads from Date Range: ".$dateRange);

        foreach ($this->config['emails'] as $toEmail) {
            $this->email->addEmail($toEmail);
        }

        $this->email->send();
    }


    /**
     * Builds the URL / Site specific information for the emails
     *
     * @param string $url
     * @param string $dateRange
     */
    public function buildFromURL($url, $dateRange)
    {

        $api = new salesQuoterApi($url);

        $leadsInfo = new LeadsInfo($url, $dateRange);

        $salesPeople = $api->getSalesPeople();


        foreach ($salesPeople as $person) {

            $this->config['id'] = $person['id'];
            $this->config['name'] = $person['name'];
            $leads = $api->getLeadsBySalesPerson($this->config);

            if ($leads['total'] > 0 ) {
                $leadsInfo->addLead($leads['name'], $leads['total']);
                $fileName = preg_replace('/\s+/', '', $leads['name']).".csv";

                if (!$this->noLeads) {
                    $this->email->addCSVFile($fileName, $leads['file']);
                }
            }
        }


        $this->email->addHTML($leadsInfo->htmlOutput());
    }
}