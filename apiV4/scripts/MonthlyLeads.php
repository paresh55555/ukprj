<?php

require_once ('./emailLeads.php');



class MonthlyLeads
{

    private $url = '';
    private $email = '';
    private $month = '';
    private $noLeads = false;

    public function __construct($config = null)
    {
        $this->url = $config['url'];
        $this->email =  $config['email'];
        $this->month = $config['month'];
        $this->noLeads = $config['noLeads'];

    }

    public function send()
    {
        $firstDay = strtotime("first day of {$this->month}");
        $lastDay = strtotime("last day of {$this->month}");


        $firstDay = $firstDay -31557600 + 86400;
        $lastDay = $lastDay - 31557600 + 86400;

        $urls = array ($this->url);
        $emails = array ($this->email);

        $config  = array (
            "urls" => $urls,
            "min" => date('Y-m-d',$firstDay),
            "max" =>  date('Y-m-d',$lastDay),
            "emails" => $emails,
            "noLeads" => $this->noLeads
        );

        $emailLeads = new EmailLeads($config);

        $emailLeads->sendEmails();

    }

}