<?php

require_once ('./YearOldQuotes.php');

if (empty($argv[1]) or empty($argv[2])) {
    throw new Exception('Missing URL or Email');
}

$urls = array ($argv[1]);


$config  = array (
    "urls" => $urls,
    "emails" => array ($argv[2])
);

$emailLeads = new YearOldQuotes($config);

$emailLeads->sendEmails();