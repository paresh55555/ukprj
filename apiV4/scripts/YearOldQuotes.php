<?php


require_once ('./api.php');
require_once ('./email.php');
require_once ('./leadsInfo.php');

/*
 * Class for Emailing Leads
 *
 */
class YearOldQuotes
{
    protected $config;
    protected $required = array ("emails");
    protected $email;

    /**
     * EmailLeads constructor.
     * @param array $config
     * @throws Exception
     */
    public function __construct($config = null)
    {
        if (empty($config)) {
            $config  = array (
                "urls" => array("https://local-flow3.rioft.com"),
                "emails" => array ("fracka@fracka.com")
            );
        }

        $this->config = $config;

        foreach ($this->required as $param) {
            if (empty($this->config[$param])) {
                throw new Exception('Missing '.$param);
            }
        }

        $this->email = new Email();
    }

    /**
     * Does the sending of Emails by building the necessary data
     */
    public function sendEmails() {

        foreach ($this->config['urls'] as $url){
            $this->buildFromURL($url);
        }

        $html = "<p> Quotes that are greater than a year old are attached</p>";

        $this->email->addHTML($html);
        $this->email->setSubject("Quotes Greater than a year old");

        foreach ($this->config['emails'] as $toEmail) {
            $this->email->addEmail($toEmail);
        }

        $this->email->send();
    }


    /**
     * Builds the URL / Site specific information for the emails
     *
     * @param string $url
     */
    public function buildFromURL($url) {

        $api = new salesQuoterApi($url);

        $quotes = $api->getYearAgoQuotes();

        $this->email->addCSVFile("YearOldQuotes.csv", $quotes['file']);
    }
}