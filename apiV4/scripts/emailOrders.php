<?php


require_once ('./api.php');
require_once ('./email.php');
require_once ('./leadsInfo.php');

/*
 * Class for Emailing Leads
 *
 */
class EmailOrders
{
    protected $config;
    protected $required = array ( "min", "max", "emails");
    protected $email;

    /**
     * EmailLeads constructor.
     * @param array $config
     * @throws Exception
     */
    public function __construct($config = null)
    {
        if (empty($config)) {
            $min = date("Y-m-d");
            $config  = array (
                "urls" => array("https://local-flow4.rioft.com", "https://local-flow5.rioft.com"),
                "min" => date("Y-m-d", strtotime($min.' - 30 day')),
                "max" => date("Y-m-d"),
                "emails" => array ("fracka@fracka.com")
            );
        }

        $this->config = $config;

        foreach ($this->required as $param) {
            if (empty($this->config[$param])) {
                throw new Exception('Missing '.$param);
            }
        }

        $this->email = new Email();
    }

    /**
     * Does the sending of Emails by building the necessary data
     */
    public function sendEmails() {

        $humanMin = date('M/d/Y',strtotime($this->config['min']));
        $humanMax = date('M/d/Y',strtotime($this->config['max']));
        $dateRange = $humanMin." - ".$humanMax;


        foreach ($this->config['locations'] as $location){
            $this->buildFromURL($location, $dateRange);
        }

        $html = "<p> Your delivered orders from $dateRange are attached</p>";

        $this->email->addHTML($html);
        $this->email->setSubject("Delivered Orders from Date Range: ".$dateRange);

        foreach ($this->config['emails'] as $toEmail) {
            $this->email->addEmail($toEmail);
        }

        $this->email->send();
    }


    /**
     * Builds the URL / Site specific information for the emails
     *
     * @param string $location
     * @param string $dateRange
     */
    public function buildFromURL($location, $dateRange) {

        $api = new salesQuoterApi($location['url']);

        $deliveredLeads = $api->getDeliveredLeads($this->config['min'], $this->config['max']);

        $this->email->addCSVFile($location['name'], $deliveredLeads);
    }


    public function getDeliveredOrders($url, $csv = false)
    {

        $api = new salesQuoterApi($url);

        $deliveredOrders = $api->getDeliveredOrders($this->config['min'], $this->config['max'], $csv);

        return $deliveredOrders;
    }


    public function getCompletedOrders($url, $csv = false)
    {

        $api = new salesQuoterApi($url);

        $completedOrders = $api->getCompletedOrders($this->config['min'], $this->config['max'], $csv);

        return json_decode($completedOrders);
    }


    public function getDeliveredOrdersCSV($url)
    {

        return $this->getDeliveredOrders($url, "true");
    }



    public function getDeliveredOrdersHTML()
    {
        $response = $this->getDeliveredOrders($this->config['url']);

        $json = json_decode($response);
        $orders = $json->data;

        $html = '';
        $tableRow = '';

        setlocale(LC_MONETARY, 'en_US');

        foreach ($orders as $order) {
            $tableRow .= ''.
                '   <tr>' .
                '        <td width="200">'.$order->{'Job Number'}.'</td>' .
                '        <td width="200">'.$order->{'Customer Name'}.'</td>' .
                '        <td width="200">'.$order->{'Delivered Date'}.'</td>' .
                '        <td width="200">'.money_format('%.2n', $order->{'Total'}).'</td>' .
                '   </tr>' ;
        }

        $html = ''.
            '<table cellspacing="0" cellpadding="5" border="1" width="800">' .
            '   <tr>' .
            '        <td width="200">Job Number</td>' .
            '        <td width="200">Customer Name</td>' .
            '        <td width="200">Delivered Date</td>' .
            '        <td width="200">Total</td>' .
            '   </tr>' .
            $tableRow .
            '</table> <br><br><br>';

        return $html;
    }

    public function sendDeliveredOrders()
    {

        $html = $this->getDeliveredOrdersHTML();


        $ordersCSV = $this->getDeliveredOrdersCSV($this->config['url']);

        $this->email->addCSVFile("DeliveredOrders.csv", $ordersCSV);


        $humanMin = date('M/d/Y',strtotime($this->config['min']));
        $humanMax = date('M/d/Y',strtotime($this->config['max']));


        $subject = "Delivered orders from $humanMin to $humanMax";

        $this->email->addHTML($html);
        $this->email->setSubject($subject);

        foreach ($this->config['emails'] as $toEmail) {
            $this->email->addEmail($toEmail);
        }

        $this->email->send();

    }



    public function sendCompletedOrders()
    {
        $orders = $this->getCompletedOrders($this->config['url']);

        setlocale(LC_MONETARY, 'en_US.utf8');

        $tableRow = '';
        $totalDoors = 0;
        $totalPanels = 0;
        $grandTotal = 0;


        foreach ($orders as $order) {
            $tableRow .= ''.
                '   <tr>' .
                '        <td>'.$order->{'prefix'}.' '.$order->{'id'}.'</td>' .
                '        <td>'.$order->{'firstName'}.' '.$order->{'lastName'}.'</td>' .
                '        <td>'.$order->{'doors'}.'</td>' .
                '        <td>'.$order->{'panels'}.'</td>' .
                '        <td>'.money_format('%.2n', $order->{'total'}).'</td>' .
                '   </tr>' ;

            $totalPanels = $totalPanels + $order->{'panels'};
            $totalDoors = $totalDoors + $order->{'doors'} ;
            $grandTotal = $grandTotal + $order->{'total'};
        }


        $tableRow .= ''.
            '   <tr>' .
            '        <td colspan="2" align="right"><b>Totals</b></td>' .
            '        <td><b>'.$totalDoors.'</b></td>' .
            '        <td><b>'.$totalPanels.'</b></td>' .
            '        <td><b>'.money_format('%.2n', $grandTotal).'</b></td>' .
            '   </tr>' ;

        $html = ''.
            '<table cellspacing="0" cellpadding="5" border="1" width="800">' .
            '   <tr>' .
            '        <td width="200">Job Number</td>' .
            '        <td width="200">Customer Name</td>' .
            '        <td width="200">Doors</td>' .
            '        <td width="200">Panels</td>' .
            '        <td width="200">Total</td>' .
            '   </tr>' .
            $tableRow .
            '</table> <br><br><br>';


        $humanMin = date('M/d/Y',strtotime($this->config['min']));
        $humanMax = date('M/d/Y',strtotime($this->config['max']));

        $subject = "Completed orders from $humanMin to $humanMax";

        $this->email->addHTML($html);
        $this->email->setSubject($subject);

        foreach ($this->config['emails'] as $toEmail) {
            $this->email->addEmail($toEmail);
        }

        $this->email->send();



    }
}