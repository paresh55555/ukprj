<?php

require_once ('./emailLeads.php');

$min = date("Y-m-d");

if (empty($argv[1]) or empty($argv[2]) ) {
    throw new Exception('Missing URL or Email');
}

$urls = array ($argv[1]);

$currentYear  = date('Y-m-d', strtotime(date('Y-01-01')));


$config  = array (
    "urls" => $urls,
    "min" =>  date("Y-m-d", strtotime($currentYear.' - 1 year')),
    "max" =>   date("Y-m-d", strtotime($currentYear.' - 1 day')),
    "emails" => array ($argv[2])
);

$emailLeads = new EmailLeads($config);

$emailLeads->sendEmails();