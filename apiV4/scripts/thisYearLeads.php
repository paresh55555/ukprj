<?php

require_once ('./emailLeads.php');

$min = date("Y-m-d");

if (empty($argv[1]) or empty($argv[2])  or empty($argv[3])) {
    throw new Exception('Missing URLS or Email');
}

$urls = array ($argv[1], $argv[2]);

$config  = array (
    "urls" => $urls,
    "min" => date('Y-m-d', strtotime(date('Y-01-01'))),
    "max" =>  date('Y-m-d'),
    "emails" => array ($argv[3])
);

$emailLeads = new EmailLeads($config);

$emailLeads->sendEmails();