<?php

require_once ('./email.php');

$min = date("Y-m-d");
//
//$config  = array (
//    "urls" => $urls,
//    "min" => date("Y-m-d", strtotime($min.' - 1 day')),
//    "max" => date("Y-m-d", strtotime($min.' - 1 day')),
//    "emails" => array ("$argv[2]")
//);


$email = new Email();

$filename = 'PanoramicNumbersToSept.csv';
$data = file_get_contents($filename);


$html = "All Panoramic Numbers including Sept 2018";

$email->addCSVFile($filename, $data);
$email->addHTML($html);
$email->setSubject("All Panoramic Numbers including Sept 2018");
$email->addEmail('tprice@towerarch.com');
$email->addEmail('cshelton@towerarch.com');
$email->addEmail('jbarthold@advancedcfo.com');
$email->send();

$email->setEmail('fracka@fracka.com');
$email->send();