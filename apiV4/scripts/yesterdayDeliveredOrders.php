<?php

require_once ('./emailOrders.php');
require_once ('./api.php');



if (empty($argv[1]) or empty($argv[2]) ) {
    throw new Exception('Missing URLS or Email');
}


$now = date("Y-m-d");
$daysBack = ' - 1 day';
$range = 0;

if (!empty($argv[3])) {
    $daysBack = "- {$argv[3]} day";
}
$max = date("Y-m-d", strtotime($now.$daysBack));

if (!empty($argv[4])) {
    $range = $argv[4];
    $range = $argv[3] + $range;
    $daysBack = "- {$range} day";
}

$min = date("Y-m-d", strtotime($now.$daysBack));

$config  = array (
    "url" => $argv[1],
    "min" => $min,
    "max" => $max,
    "emails" => array ("$argv[2]")
);

$emailOrders = new EmailOrders($config);
$emailOrders->sendDeliveredOrders();


