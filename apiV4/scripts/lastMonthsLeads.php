<?php

require_once ('./emailLeads.php');

if (empty($argv[1]) or empty($argv[2]) ) {
    throw new Exception('Missing URL or Email');
}

$firstDay = strtotime('first day of last month');
$lastDay = strtotime('last day of last month');


if (!empty($argv[4])) {
    $firstDay = strtotime("-". $argv[4]." month", $firstDay);
    $lastDay = strtotime("-". $argv[4]." month", $lastDay);
}

$urls = array ($argv[1]);

$config  = array (
    "urls" => $urls,
    "min" => date('Y-m-d',$firstDay),
    "max" =>  date('Y-m-d',$lastDay),
    "emails" => array ($argv[2])
);

$emailLeads = new EmailLeads($config);

$emailLeads->sendEmails();