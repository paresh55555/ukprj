<?php

require_once('./MonthlyLeads.php');

if (empty($argv[1]) or empty($argv[2]) ) {
    throw new Exception('Missing URL or Email');
}

$config  = array (
    "url" => $argv[1],
    "email" =>$argv[2],
    "noLeads" => true
);

$months = array (
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
    );



foreach ($months as $month) {
    $config['month'] = $month;
    $monthlyLeads = new MonthlyLeads($config);
    $monthlyLeads->send();
}




