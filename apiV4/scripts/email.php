<?php

class email  {


    const TOKEN="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";

    protected $attachments = array();
    protected $url;
    protected $personalizations ;
    protected $subject;
    protected $html;
    protected $emails = array();

    protected $required = array ("subject", "emails", "html");

    /**
     * Allows you to add emails to send to
     *
     * @param string $email
     */
    public function addEmail($email) {

        array_push($this->emails, array("email" => "$email"));
    }

    /**
     * Reset emails to only this one
     *
     * @param string $email
     */
    public function setEmail($email) {

        $this->emails = array();
        $this->addEmail($email);
    }


    /**
     * Allows you to set the Subject of the email
     *
     * @param $subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * Let's you attach a CSV file to the email
     *
     * @param string $name
     * @param string $data
     */
    public function addCSVFile($name, $data)
    {
        $file = base64_encode($data);

        $attachment = array ("content" => $file, "type" => "text/csv", "filename" => "$name", "disposition" => "attachment" );

        array_push($this->attachments, $attachment);
    }

    /**
     * Allows you to add HTML to the email
     *
     * @param string $html
     */
    public function addHTML($html) {
      $this->html .= $html;
    }


    /**
     * Builds and Sends to the Emails
     *
     * @throws Exception
     */
    public function send()
    {
        foreach ($this->required as $param) {
            if (empty($this->{$param})) {
                throw new Exception('Missing '.$param);
            }
        }

        $this->personalizations = array ("to" => $this->emails);

        $endPoint = "https://api.sendgrid.com/v3/mail/send";
        $from = array ("email" => "no-reply@salesquoter.com", "name" => "Sales Quoter Reports");

        $content = array ("type" => "text/html", "value" => $this->html);
        $params = array (
            "personalizations" => array($this->personalizations),
            "from" => $from,
            "subject" => $this->subject,
            "content" => array($content)
        );

        if (!empty($this->attachments)) {
            $params['attachments'] = $this->attachments;
        }

        $encodedJSONData = json_encode($params);

        // Correct Params Format
        // $encodedJSONData = '{"personalizations":[{"to":[{"email":"fracka@fracka.com"}]}],"from":{"email": "fracka@fracka.com"},"subject": "Hello, World!","content":[{"type": "text/plain", "value": "Heya!"}]}';

        $ch = curl_init();

        $session = curl_init();

        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedJSONData);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: Bearer ".self::TOKEN) );
        curl_setopt($ch, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output=curl_exec($ch);

        curl_close($ch);

        print_r($output);
    }

//
//curl --request POST \
//--url  \
//--header 'Authorization: Bearer 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' \
//--header 'Content-Type: application/json' \
//--data @data/postData


}

