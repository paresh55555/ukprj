<?php


/**
 * Class SalesQuoterApi
 */
class SalesQuoterApi
{

    protected $token;
    protected $url;

    /**
     * Set the URL / Site to query
     *
     * salesQuoterApi constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url ;
        $this->token = $this->getToken();
    }

    /**
     * Get the List of Sales People from API
     * @return array
     */
    public function getSalesPeople()
    {
        $ch = curl_init();

        $endPoint = $this->url."/api/V4/users?type=salesPerson";

        curl_setopt($ch, CURLOPT_URL,$endPoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $this->setHeader($ch);

        $output=curl_exec($ch);

        curl_close($ch);

        $users = json_decode($output, true);

        $results = array();

        foreach ($users as $user) {
            if ( !empty($user['name']) and $user['name'] != 'Fracka Future') {
                array_push($results, array("name" => $user['name'], "id" => $user['id']));
            }
        }

        return $results;
    }


    /**
     * Get the Leads from the API based on a Sales Person's ID
     *
     * @param array $config
     * @return array
     */
    public function getLeadsBySalesPerson($config)
    {
        $min = $config['min'];
        $max = $config['max'];
        $id = $config['id'];

        $ch = curl_init();

        $endPoint = $this->url."/api/V4/reports/leads?min=$min&max=$max&unique=yes&salesPerson=$id";

        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $this->setHeader($ch);

        $output=curl_exec($ch);

        curl_close($ch);

        $results = array ( "file" => $output, "name" => $config['name'], "total" => substr_count( $output, "\n" ) - 1);

        return $results;
    }

    /**
     * Get Leads from Orders delivered
     * @return array
     */
    public function getDeliveredLeads($min, $max)
    {
        $ch = curl_init();

        $endPoint = $this->url."/api/V4/reports/email/delivered";

        $postBody = array("min" => $min, "max" => $max);
        $encodedJSONData = json_encode($postBody);

        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedJSONData);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Bearer ".trim($this->token)
        ) );

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }


    /**
     * Get  Orders delivered for a certain date range
     * @param $min
     * @param $max
     * @param $csv
     * @return mixed
     */
    public function getDeliveredOrders($min, $max, $csv)
    {
        $ch = curl_init();

        $endPoint = $this->url."/api/V4/reports/orders/delivered?min=$min&max=$max&csv=$csv";
        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Bearer ".trim($this->token)
        ) );

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }


    /**
     * Get  Completed orders for a certain date range
     * @param $min
     * @param $max
     * @param $csv
     * @return mixed
     */
    public function getCompletedOrders($min, $max, $csv)
    {
        $ch = curl_init();


        $endPoint = $this->url."/api/V4/reports/email/completed?min=$min&max=$max&csv=$csv";

        $postBody = array("min" => $min, "max" => $max);
        $encodedJSONData = json_encode($postBody);

        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedJSONData);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Bearer ".trim($this->token)
        ) );

        $output=curl_exec($ch);

        curl_close($ch);

        return $output;
    }


    /**
     * Get Leads from Orders delivered
     * @return array
     */
    public function getToken()
    {
        $ch = curl_init();

        $credentials = 'admin:test';

        if ($this->url == 'https://panoramicdoors.salesquoter.com'  ||
            $this->url == 'https://panoramicdoors.salesquoter.com' ||
            $this->url == 'https://panoramicdoors.salesquoter.co.uk') {

            $credentials = 'ApiLeadsKeep:ks$MYCnNWw]pypa$3XAyejXrnnTZyWyYEsy';

        }

        $endPoint = $this->url."/api/V4/token";

        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Accept: application/json",
            "Authorization: Basic ".base64_encode($credentials)
        ) );

        $output=curl_exec($ch);

        curl_close($ch);

        $result = json_decode($output, true);

        return $result['token'];
    }



    /**
     * Set the Bearer Token
     *
     * @param object $ch
     */
    public function setHeader($ch)
    {
        curl_setopt($ch,CURLOPT_HTTPHEADER,  array("Authorization: Bearer $this->token") );
    }



    /**
     * Returns quotes that haven't become and order that are over 1 year old
     * @return array
     */
    public function getYearAgoQuotes()
    {

        $ch = curl_init();

        $endPoint = $this->url."/api/V4/reports/quotes/yearOld";

        curl_setopt($ch,CURLOPT_URL, $endPoint);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $this->setHeader($ch);

        $output=curl_exec($ch);

        curl_close($ch);

        $results = array ( "file" => $output, "name" => 'YearOldQuotes', "total" => substr_count( $output, "\n" ) - 1);

        return $results;
    }

}

