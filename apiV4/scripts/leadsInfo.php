<?php

class leadsInfo  {

    protected $site;
    protected $dateRange;
    protected $leads = array();

    public function  __construct ($site, $dateRange) {
        $this->site = $site;
        $this->dateRange = $dateRange;
    }

    public function addLead($salesPerson, $numberOfLeads)
    {

        array_push($this->leads, array("SalesPerson" => $salesPerson, "NumberOfLeads" => $numberOfLeads));
    }


    public function htmlOutput()
    {
        $tableRow = '';
        $total = 0;
        foreach ($this->leads as $row) {
            $tableRow .= ''.
                '   <tr>' .
                '        <td width="200">'.$row['SalesPerson'].'</td>' .
                '        <td width="200">'.$row['NumberOfLeads'].'</td>' .
                '   </tr>' ;
            $total = $total + $row['NumberOfLeads'];
        }

        $html = ''.
            '<table cellspacing="0" cellpadding="5" border="0" width="400">' .
            '   <tr>' .
            '        <td width="400">Site: '.$this->site.'</td>' .
            '   </tr>' .
            '   <tr>' .
            '        <td width="400">Date Range: '.$this->dateRange.'</td>' .
            '   </tr>' .
            '</table><BR>'.
            '<table cellspacing="0" cellpadding="5" border="1" width="400">' .
            '   <tr>' .
            '        <td width="200">Sales Person</td>' .
            '        <td width="200">Number of Leads</td>' .
            '   </tr>' .
            $tableRow .
            '   <tr>' .
            '        <td colspan="2" width="200">&nbsp;</td>' .
            '   </tr>' .
            '   <tr>' .
            '        <td width="200">Total Leads</td>' .
            '        <td width="200">'.$total.'</td>' .
            '   </tr>' .
            '</table> <br><br><br>';

        return $html;

    }

}

