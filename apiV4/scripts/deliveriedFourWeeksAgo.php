<?php

require_once ('./emailOrders.php');
require_once ('./api.php');

$min = date("Y-m-d");

$daysAgo = -28;

if (empty($argv[1]) or empty($argv[2]) ) {
    throw new Exception('Missing URLS or Email');
}

if (!empty($argv[4])) {
    $daysAgo = $daysAgo + (7*$argv[4]);
}
$location1 = array("name" => "panoramic.csv", "url" => $argv[1]);


$ref_week = strtotime("last sunday midnight");

$daysAgoMax = $daysAgo - 6;
$end_week = strtotime("$daysAgo days",$ref_week);
$start_week = strtotime("$daysAgoMax days",$ref_week);

$config  = array (
    "locations" => array($location1),
    "min" => date("Y-m-d", $start_week),
    "max" => date("Y-m-d", $end_week),
    "emails" => array ("$argv[2]")
);

$emailOrders = new EmailOrders($config);

$emailOrders->sendEmails();