<!DOCTYPE html>
<html>
<head>
    <title>Promo</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script type="text/javascript" src="/jsPackages/jquery.min.js"></script>
    <script type="text/javascript" src="/jsPackages/jquery.cookie.js"></script>
    <script>

        siteName = '';

        <?php echo "siteName='".$_SERVER['SITE_NAME']."';";  ?>


        var query = window.location.search.substring(1),
                params = query && query.split('&'),
                i;
        for (i = 0; i < params.length; i++) {
            var pair = params[i].split("=");
            var date = new Date();
            var minutes = 43200;
            date.setTime(date.getTime() + (minutes * 60 * 1000));

            if (typeof pair[0] === "string" && pair[0] === 'id') {

                if (pair[1] == "83"  && siteName == 'pd') {
                    window.location.replace('https://panoramicdoors-ces.salesquoter.com/guest/?id='+pair[1]);
                    exit;
                }

                $.cookie('assignLead', decodeURIComponent(pair[1]),{path: '/', expires:  date  });
            }
        }
        window.location.replace('/?tab=guest');
    </script>
</head>
<body>
<div>Redirecting to http://panoramicdoors.com...</div>
</body>
</html>
