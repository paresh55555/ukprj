var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const VENDOR_LIBS = [

];

module.exports = {
    entry: {
        bundle: './js'
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        publicPath: '/',
        filename: '[name].[hash].js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['vendor','manifest']
        }),
        new HtmlWebpackPlugin({
            template: 'index.php'
        }),
        new ExtractTextPlugin('style.css')
    ],
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loaders: ['react-hot-loader']
        }, {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['es2015', 'react'],
                compact: false
            }
        }, { 
            test: /\.sass$/, 
            loader: ExtractTextPlugin.extract({
                fallbackLoader: "style-loader",
                loader: "css-loader!sass-loader",
            })
        },
        {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract({
                loader: 'css-loader',
            })
           
        }, {
            test: /\.woff|\.woff2|\.svg|.eot|\.ttf|\.gif/,
            loader: 'url?prefix=font/&limit=10000'
        }, 
        {
            test: /\.(jpe?g|png|gif|svg)$/,
            use: [
                {
                    loader: 'url-loader',
                    options: { limit: 40000 }
                },
                'image-webpack-loader'
            ]
        }]
    },
    node: {
        fs: "empty"
    }
};