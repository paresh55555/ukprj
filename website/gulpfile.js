var gulp = require('gulp'),
    gutil = require('gulp-util'),
    $ = require('gulp-load-plugins')(),
    gulpsync = $.sync(gulp),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    historyApiFallback = require('connect-history-api-fallback'),
    PluginError = $.util.PluginError,
    webpack = require('webpack'),
    WebpackDevServer = require("webpack-dev-server"),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    hash = require('gulp-hash-filename'),
    htmlreplace = require('gulp-html-replace'),
    stream = require('stream'),
    path = require('path'),
    del = require('del');

function hashFileName(options) {
    'use strict';
    var assemblyStream = new stream.Transform({objectMode: true});
    options = options || {};
    var format = options.format||"{name}-{hash}{ext}";

    assemblyStream._transform = function(file, unused, callback) {
        this.push(performHash(format, file));
        callback();
    };

    return assemblyStream;
}
//---------------
// TASKS
//---------------

var jsFiles = 'js/**/*.js',
    jsDest = 'dist/js',
    jsProdFile = '',
    cssFiles = 'css/**/*.css',
    cssDest = 'dist/css',
    cssProdFile  = '';


gulp.task('index', function() {
  gulp.src('index.php')
    .pipe(htmlreplace({
        'js': 'js/'+jsProdFile,
        'css': {
            src: cssProdFile,
            tpl: '<link rel="stylesheet" type="text/css" href="css/%s" charset="utf-8">'
        }
    }))
    .pipe(gulp.dest('dist/'));
});


gulp.task('scripts', function() {
    name = 'two';
    return gulp.src(jsFiles)
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(hash())
        .pipe(gulp.dest(jsDest))
        .pipe(getProdFileName());
});

gulp.task('stylecheets', function() {
    name = 'two';
    return gulp.src('css/output.css')
        .pipe(concat('output.css'))
        .pipe(hash())
        .pipe(gulp.dest(cssDest)) 
        .pipe(getCssProdFileName());
});

gulp.task('cleanDist', function(done) {
    log('Clean dist folder..');
    return del('./dist', {
        force: true // clean files outside current directory
    });
});


gulp.task('buildProd', gulpsync.sync([
    'cleanDist',
    'copyCss',
    'copyGuest',
    'copyImages',
    'copyHtml',
    'copyPromo',
    'copyJsPackages',
    'scripts',
    'stylecheets',
    'index'
]), done);

gulp.task('copyCss', function() {
    log('Copying Css ');
    return gulp.src('./css/**')
        .pipe(gulp.dest('./dist/css'));
});



gulp.task('copyJsPackages', function() {
    log('Copying JsPackages ');
    return gulp.src('./jsPackages/**')
        .pipe(gulp.dest('./dist/jsPackages'));
});


gulp.task('copyPromo', function() {
    log('Copying Promo ');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./promo/**')
        .pipe(gulp.dest('./dist/promo'));
});

gulp.task('copyHtml', function() {
    log('Copying Html ');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./html/**')
        .pipe(gulp.dest('./dist/html'));
});


gulp.task('copyGuest', function() {
    log('Copying Guest ');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./guest/**')
        .pipe(gulp.dest('./dist/guest'));
});

gulp.task('copyImages', function() {
    log('Copying images ');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./images/**')
        .pipe(gulp.dest('./dist/images'));
});


function getProdFileName() {

        var assemblyStream = new stream.Transform({objectMode: true});

        assemblyStream._transform = function(file, unused, callback) {

            var ext = path.extname(file.path);
            var fname = path.basename(file.path, ext);
            jsProdFile = fname + ext
            callback();
        };

        return assemblyStream;

}

function getCssProdFileName() {

    var assemblyStream = new stream.Transform({objectMode: true});

    assemblyStream._transform = function(file, unused, callback) {

        var ext = path.extname(file.path);
        var fname = path.basename(file.path, ext);
        cssProdFile = fname + ext
        callback();
    };

    return assemblyStream;

}




function log(msg) {
  $.util.log($.util.colors.blue(msg));
};

function done() {
    log('************');
    log('* V1 of Website is Ready');
    log('************');
}
