<!doctype html>
<html>
<head>
    <title>Sales Quoter</title>

    <script type="text/javascript" src="jsPackages/jquery.min.js"></script>
    <script type="text/javascript" src="jsPackages/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
    <script type="text/javascript" src="jsPackages/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="jsPackages/jquery.history.js"></script>

    <?php
    if (!empty($_SERVER['TRACK_JS'])) {

        echo ''.
            '<script type="text/javascript">window._trackJs = { token: \'a8fafd50663545568ecaf016e549adfc\' };</script>'.
            '<script type="text/javascript" src="https://cdn.trackjs.com/releases/current/tracker.js"></script>';
    }
    ?>

    <!-- build:js -->
    <!--replace-->
    <script type="text/javascript" src="js/builder.js"></script>
    <script type="text/javascript" src="js/dropzone.js"></script>
    <script type="text/javascript" src="js/builder/modules.js"></script>
    <script type="text/javascript" src="js/builder/extendedHalfButtons.js"></script>
    <script type="text/javascript" src="js/loadData.js"></script>
    <script type="text/javascript" src="js/accounting.js"></script>
    <script type="text/javascript" src="js/production.js"></script>
    <script type="text/javascript" src="js/initials.js"></script>

    <script type="text/javascript" src="js/leads.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/keepingTheFormPersistance.js"></script>
    <script type="text/javascript" src="js/selectionMade.js"></script>
    <script type="text/javascript" src="js/sizeAndSash.js"></script>
    <script type="text/javascript" src="js/openers.js"></script>
    <script type="text/javascript" src="js/glass.js"></script>
    <script type="text/javascript" src="js/installation.js"></script>
    <script type="text/javascript" src="js/acrossAllPages.js"></script>
    <script type="text/javascript" src="js/cart.js"></script>
    <script type="text/javascript" src="js/jquery.corners.js"></script>
    <script type="text/javascript" src="js/transactions.js"></script>
    <script type="text/javascript" src="js/account.js"></script>
    <script type="text/javascript" src="js/products/product.js"></script>
    <script type="text/javascript" src="js/stickyfloat.js"></script>


    <script type="text/javascript" src="js/quotes/quotes.js"></script>
    <script type="text/javascript" src="js/quotes/flow1.js"></script>
    <script type="text/javascript" src="js/quotes/flow2.js"></script>
    <script type="text/javascript" src="js/quotes/flow3.js"></script>

    <script type="text/javascript" src="js/sheet.js"></script>
    <script type="text/javascript" src="js/jquery.simplePagination.js"></script>
    <script type="text/javascript" src="js/date.js"></script>
    <script type="text/javascript" src="js/windowsBuilder.js"></script>
    <script type="text/javascript" src="js/windowsHorizontal.js"></script>
    <script type="text/javascript" src="js/windowsVertical.js"></script>

    <script type="text/javascript" src="js/site/loadSite.js"></script>
    <script type="text/javascript" src="js/site/setGlobals.js"></script>
    <script type="text/javascript" src="js/site/writeData.js"></script>
    <script type="text/javascript" src="js/site/readData.js"></script>

    <script type="text/javascript" src="js/extras/extras.js"></script>
    <script type="text/javascript" src="js/extras/extrasHTML.js"></script>
    <script type="text/javascript" src="js/extras/sill.js"></script>
    <script type="text/javascript" src="js/extras/extenders.js"></script>
    <script type="text/javascript" src="js/extras/trickle.js"></script>
    <script type="text/javascript" src="js/extras/screen.js"></script>
    <script type="text/javascript" src="js/extras/bayPoles.js"></script>

    <script type="text/javascript" src="js/fixed/doorWidth.js"></script>
    <script type="text/javascript" src="js/fixed/doorHeight.js"></script>
    <script type="text/javascript" src="js/fixed/fixedPanels.js"></script>
    <script type="text/javascript" src="js/fixed/swingDirection.js"></script>

    <script type="text/javascript" src="js/acrossMultiplePages/buttons.js"></script>
    <script type="text/javascript" src="js/acrossMultiplePages/panels.js"></script>
    <script type="text/javascript" src="js/acrossMultiplePages/swings.js"></script>
    <script type="text/javascript" src="js/acrossMultiplePages/time.js"></script>
    <script type="text/javascript" src="js/acrossMultiplePages/numbers.js"></script>

    <script type="text/javascript" src="js/api/apiCalls.js"></script>
    <script type="text/javascript" src="js/api/apiOrders.js"></script>
    <script type="text/javascript" src="js/api/apiQuotes.js"></script>
    <script type="text/javascript" src="js/api/validation.js"></script>
    <script type="text/javascript" src="js/api/apiAccounting.js"></script>
    <script type="text/javascript" src="js/api/apiPayments.js"></script>

    <script type="text/javascript" src="js/navigation/tabs.js"></script>
    <script type="text/javascript" src="js/navigation/header.js"></script>
    <script type="text/javascript" src="js/navigation/menu.js"></script>
    <script type="text/javascript" src="js/navigation/salesNav.js"></script>
    <script type="text/javascript" src="js/navigation/navBars.js"></script>
    <script type="text/javascript" src="js/navigation/buttons.js"></script>
    <script type="text/javascript" src="js/navigation/footer.js"></script>

    <script type="text/javascript" src="js/svg/icons.js"></script>
    <script type="text/javascript" src="js/users/guest.js"></script>
    <script type="text/javascript" src="js/users/sign.js"></script>
    <script type="text/javascript" src="js/renderPage/singlePage.js"></script>
    <script type="text/javascript" src="js/hardware/hardware.js"></script>

    <script type="text/javascript" src="js/customSize/customSize.js"></script>
    <script type="text/javascript" src="js/customSize/dimensions.js"></script>
    <script type="text/javascript" src="js/customSize/customPanels.js"></script>

    <script type="text/javascript" src="js/customers/customers.js"></script>
    <script type="text/javascript" src="js/customers/addCustomer.js"></script>
    <script type="text/javascript" src="js/customers/searchCustomers.js"></script>

    <script type="text/javascript" src="js/colors/vinyl.js"></script>
    <script type="text/javascript" src="js/colors/colorAndFinish.js"></script>
    <script type="text/javascript" src="js/colors/colorHTML.js"></script>

    <script type="text/javascript" src="js/orders/invoice.js"></script>
    <script type="text/javascript" src="js/orders/orders.js"></script>
    <script type="text/javascript" src="js/orders/creditCard.js"></script>

    <script type="text/javascript" src="js/cutSheets/windowCutSheets.js"></script>

    <script type="text/javascript" src="js/salesAdmin/salesAdmin.js"></script>
    <!-- endbuild -->


    <!-- build:css -->
    <link rel="stylesheet" type="text/css" href="css/output.css" charset="utf-8">
    <!-- endbuild -->
    <!--ToHere-->


    <link rel="stylesheet" href="jsPackages/jquery-ui-1.11.2.custom/jquery-ui.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat|PT+Serif" rel="stylesheet"/>

    <script>
        apiHost = '';
        apiHostV1 = '';
        apiHostUS = '';
        currentHost = '';
        siteName = '';

        <?php

        $envs = array(
            "apiHost" => "API_HOST",
            "apiHostV1" => "API_HOST_V1",
            "apiHostUS" => "API_HOST_US",
            "currentHost" => "CURRENT_HOST",
            "siteName" => "SITE_NAME",
        );

        foreach ($envs as $key => $value) {
            if (!empty($_SERVER[$value])) {
                echo "{$key}='{$_SERVER[$value]}'; \n";
            }
        }

        ?>
        $(document).ready(function () {
            if (empty(apiHost)) {
                alert("There is an error setting the API's Host");
            }
            loadSite();
        });
    </script>

    <?php

    if (!empty($_SERVER['TRACKING_CODE'])) {

        echo '<script>'.
            'window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;'.
            ' ga(\'create\', \''.$_SERVER['TRACKING_CODE'].'\', \'auto\');'.
            '  ga(\'require\', \'autotrack\');'.
            '   ga(\'send\', \'pageview\');'.
            '</script>'.
            '<script async src=\'https://www.google-analytics.com/analytics.js\'></script>'.
            '<script async src=\'jsPackages/autotrack.js\'></script>';
    }


    ?>

    <script type="text/javascript"
            src="https://salesquoter.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/hpkl5q/b/5/7ebd7d8b8f8cafb14c7b0966803e5701/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=f0070c38"></script>
    <?php parse_str($_SERVER['QUERY_STRING'], $output);
    if (isset($output['tab']) && $output['tab'] === 'guestForm'): ?>
        <script>
            writeLocalStorageWithKeyAndData("gForm", 'yes');
        </script>
    <?php elseif (!isset($output['tab']) || (isset($output['tab']) && ($output['tab'] === 'guest' || $output['tab'] === 'signIn' || $output['tab'] === 'guestSpecial'))): ?>
        <script>
            writeLocalStorageWithKeyAndData("gForm", 'no');
        </script>
    <?php endif; ?>
    <!-- Facebook Pixel Code -->
    <script>
        if (readKey('gForm') === 'yes') {
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '108238313218518');
            fbq('track', 'PageView');
        }
    </script>
    <!--        <noscript>-->
    <!--            <img height="1" width="1" style="display:none"-->
    <!--               src="https://www.facebook.com/tr?id=108238313218518&ev=PageView&noscript=1"-->
    <!--            /></noscript>-->
    <!-- End Facebook Pixel Code -->
</head>
<body>
<div id="companyHeader"></div>
<div id="loading" class="centeredLoader"><img src="/images/loading/ajax_loader_blue_128.gif" height="128" width="128">
</div>
<div id="startOfDynamicContent"></div>

</body>
</html>
