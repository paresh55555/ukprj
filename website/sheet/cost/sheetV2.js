var currency = "$";
var uom = '"';

function isUK() {

    var status = false
    if (siteName == 'panoramicdoorsUK') {
        status = true
    }
    return status;
}
function setCurrency() {

    if (isUK()) {
        currency = '&pound';
        uom = 'mm';
    }
}


function getTemplate() {

    var template = 'template.html';

    if (isUK()) {
        template = 'templateUK.html';
    }

    $.get(template, function (html) {
        $('body').append(html);
        sheetData();
    });
}


function buildHeader(header) {


//    $('#job').append(header.job);
    $('#material').html(header.material);
    $('#text').html(header.text);
    $('#image').attr("src", header.image);
    $('#totalTag').html(currency + header.total);


}


function buildDoorDetails(details) {

    $('#operation').html(details.Operation);
    $('#interiorFinish').html(details['Interior Finish']);
    $('#exteriorFinish').html(details['Exterior Finish']);
    $('#frame').html(details.Frame);
    $('#track').html(details.Track);
    $('#handles').html(details.Handles);

}


function buildGlass(glassList) {

    var pieceOfGlass = glassList.length;
    var tableRows = [];
    for (var a = 0; a < pieceOfGlass; a++) {

        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = ''
        }
        glass = glassList[a];

        if (isUK()) {
            glassSize = '' +
                '<td>' + glass['Glass Width Inches'] + '</td>' +
                '<td>' + glass['Glass Height Inches'] + '</td>';

        } else {

            glassSize = '' +
                '<td>' + glass['Glass Width Inches'] + '</td>' +
                '<td>' + glass['Glass Height Inches'] + '</td>' +
                '<td>' + glass['Glass Width MM'] + '</td>' +
                '<td>' + glass['Glass Height MM'] + '</td>';


        }

        var row =
            '<tr ' + alt + ' >' +
            '<td>' + glass.name + '</td>' +
            glassSize +
            '<td>' + glass.Quantity + '</td>' +
            '<td>' + currency + glass['Total Cost'] + '</td>' +
            '</tr>';

        $('#glass tr:last').after(row);
    }


}

function buildDesign(design) {

    $('#totalPanels').html(design.panels);
    $('#swingDoors').html(design['Swing Door']);
    $('#slidingLeft').html(design['sliding left']);
    $('#slidingRight').html(design['sliding right']);

    if (isUK()) {
        $('#widthMM').html(design['width inches']);
        $('#heightMM').html(design['height inches']);
    } else {
        $('#width').html(design['width inches']);
        $('#widthMM').html(design['width MM']);
        $('#height').html(design['height inches']);
        $('#heightMM').html(design['height MM']);
    }

}

function buildParts(partsList) {
    var numberOfParts = partsList.length;
    var tableRows = [];


    for (var a = 0; a < numberOfParts; a++) {

        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = ''
        }
        var part = partsList[a];
        if (!part.partNumber) {
            part.partNumber = '';
        }

        var size = '';

        if (isUK()) {
            size = '<td>' + part['Cut Size Inches'] + '</td>';
        } else {
            size = '' +
                '<td>' + part['Cut Size Inches'] + '</td>' +
                '<td>' + part['Cut Size MM'] + '</td>';
        }

        var row =
            '<tr ' + alt + ' >' +
            '<td>' + part.partNumber + '</td>' +
            '<td>' + part.name + '</td>' +
            size +
            '<td>' + part['Lengths'] + '</td>' +
            '<td>' + currency + part['Total Cost'] + '</td>' +
            '</tr>';

        if (part['Total Cost'] > 0 ) {
            tableRows.push(row);
        }

    }

    if (tableRows.length > 0) {
        $('#parts tr:last').after(tableRows);
    } else {
        $('#parts').html('');
    }
}

function buildItems(itemsList) {

    var numberOfItems = itemsList.length;
    tableRows = [];
    for (a = 0; a < numberOfItems; a++) {
        alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = ''
        }
        item = itemsList[a];

        peritem = item.cost / item.quantity;

        if (!item.partNumber) {
            item.partNumber = '';
        }
        var row =
            ['<tr ' + alt + ' >',
                '<td>' + item.partNumber + '</td>',
                '<td>' + item.name + '</td>',
                '<td>' + item.quantity + '</td>',
                '<td>' + currency + peritem.toFixed(2) + '</td>',
                '<td>' + currency + item.cost + '</td>',
                '</tr>'].join('\n');

        if (peritem > 0 ) {
            tableRows.push(row);
        }

    }
    $('#items tr:last').after(tableRows);

}


function buildColor(itemsList) {

    var numberOfItems = itemsList.length;
    var tableRows = [];
    for (a = 0; a < numberOfItems; a++) {
        alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = ''
        }
        item = itemsList[a];

        var row =
            ['<tr ' + alt + ' >',
                '<td>' + item.name + '</td>',
                '<td>' + currency + item.cost + '</td>',
                '</tr>'].join('\n');
        tableRows.push(row);
    }
    $('#color tr:last').after(tableRows);

}


function buildPanel(panelInfo) {

    var imageURLs = {
        "leftSwing": "../images/Swing-Left.gif",
        "left": "../images/Panel-Left.gif",
        "divider": "../images/Divider-Bar.gif",
        "rightSwing": "../images/Swing-Right.gif",
        "right": "../images/Panel-Right.gif"
    }

    if (panelInfo['Swing Door'] == "both") {
        buildPanelBoth(panelInfo.panels, panelInfo['sliding left'], imageURLs);
    }
    else if (panelInfo['Swing Door'] == "left") {
        buildPanelLeft(panelInfo.panels, imageURLs);
    }
    else if (panelInfo['Swing Door'] = "right") {
        buildPanelRight(panelInfo.panels, imageURLs);
    }

}

function buildPanelBoth(numberOfPanels, numberLeft, imageURLs) {

    jQuery('<img/>', {
        src: imageURLs.leftSwing,
        class: 'panelImage'
    }).appendTo('#panel');


    for (a = 0; a < numberLeft; a++) {

        jQuery('<img/>', {
            src: imageURLs.left,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.divider,
        class: 'panelImage'
    }).appendTo('#panel');

    for (a = 0; a < numberOfPanels - numberLeft - 2; a++) {

        jQuery('<img/>', {
            src: imageURLs.right,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.rightSwing,
        class: 'panelImage'
    }).appendTo('#panel');


}


function buildPanelLeft(numberOfPanels, imageURLs) {

    jQuery('<img/>', {
        src: imageURLs.leftSwing,
        class: 'panelImage'
    }).appendTo('#panel');

    for (a = 0; a < numberOfPanels - 1; a++) {

        jQuery('<img/>', {
            src: imageURLs.left,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.divider,
        class: 'panelImage'
    }).appendTo('#panel');


}

function buildPanelRight(numberOfPanels, imageURLs) {

    jQuery('<img/>', {
        src: imageURLs.divider,
        class: 'panelImage'
    }).appendTo('#panel');


    for (a = 0; a < numberOfPanels - 1; a++) {

        jQuery('<img/>', {
            src: imageURLs.right,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.rightSwing,
        class: 'panelImage'
    }).appendTo('#panel');


}


function sheetData() {

    var jobNumber = $.url().param('jobNumber');

    var url = "https://" + apiHostV1 + "/api/costSheet?jobNumber=" + jobNumber;

    $.getJSON(url, function (apiJSON) {
        buildParts(apiJSON.parts);
        buildItems(apiJSON.items);
        buildDesign(apiJSON.design);
        buildDoorDetails(apiJSON.details);
        buildGlass(apiJSON.glass);
        buildHeader(apiJSON.header)
        buildPanel(apiJSON.design);
        //buildColor(apiJSON.percentage);
        buildProfit(apiJSON.ProfitMargin);
    });
}

$(document).ready(function () {

    setCurrency();
    getTemplate();
});


function buildProfit(profit) {

    var tableRows = [];
    var alt = 'class="alt"';

    var row =
        ['<tr ' + alt + ' >',
            '<td>' + profit.percentage + '%</td>',
            '<td>' + currency + profit.totalPrice + '</td>',
            '</tr>'].join('\n');

    tableRows.push(row);

    $('#profit tr:last').after(tableRows);

}

