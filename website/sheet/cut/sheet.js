function getTemplate () {


    $.get('template.html', function (html) {
        $('body').append(html);
        sheetData();
    });
}

function buildJobInfo (jobInfo) {

    $('#job').html("Job Number: "+jobInfo.jobNumber);
    $('#productionDate').html("Production Date: "+jobInfo.productionDate);
    $('#dueDate').html("Due Date: "+jobInfo.dueDate);

}

function buildHeader (header, vinyl) {


    var moduleTitleConversion = {   "Clad":"Thermaclad","Vinyl":"Absolute Vinyl","Aluminum":"Signature Aluminum"};

    var title = '';
    if ( /Aluminum.*/i.test(header.material) ) {
        title = 'Signature Aluminum';
    }

    if ( /Vinyl.*/i.test(header.material) ) {
        title = 'Absolute Vinyl';
    }

    if ( /Clad.*/i.test(header.material) ) {
        title = 'Thermaclad';
    }

    var vinylColor = '';
    if (!empty(vinyl)) {
        if (!empty(vinyl[0])) {
            vinylColor = ' - ' + vinyl[0].name;
        }
    }

    if (vinylColor != ' - White Vinyl' && vinylColor != ' - Beige Vinyl') {
        vinylColor = '';
    }

    //console.log (header.material);
    //var title = moduleTitleConversion[header.material];


    $('#material').html(title +  vinylColor );
    $('#text').html(header.text);
    $('#image').attr("src",header.image);


}

function empty(variable) {


    var status = false;

    if (variable === undefined || variable === null ||  variable == '' ||  variable == 'undefined' ) {
        status = true;
    }
    return status;

}


function buildDoorDetails (details) {

    $('#operation').html(details.Operation);
    $('#interiorFinish').html(details['Interior Finish']);
    $('#exteriorFinish').html(details['Exterior Finish']);
    $('#frame').html(details.Frame);
    $('#track').html(details.Track);
    $('#handles').html(details.Handles);

}


function buildGlass (glassList) {

    var pieceOfGlass = glassList.length;
    var tableRows = [];
    for( var a=0; a < pieceOfGlass ; a++){

        alt ='class="alt"';
        if (a%2 == 0 ){
            alt=''
        }
        glass = glassList[a];
        var row =
            ['<tr '+alt+' >',
                '<td>'+glass.name+'</td>',
                '<td>'+glass['Glass Width Inches']+'</td>',
                '<td>'+glass['Glass Height Inches']+'</td>',
                '<td>'+glass['Glass Width MM']+'</td>',
                '<td>'+glass['Glass Height MM']+'</td>',
                '<td>'+glass.Quantity+'</td>',
                '</tr>'].join('\n');
        tableRows.push(row);
    }

    $('#glass tr:last').after(tableRows);

}

function buildDesign (design) {

    $('#totalPanels').html(design.panels);
    $('#swingDoors').html(design['Swing Door']);
    $('#slidingLeft').html(design['sliding left']);
    $('#slidingRight').html(design['sliding right']);

    $('#width').html(design['width inches']);
    $('#widthMM').html(design['width MM']);

    $('#height').html(design['height inches']);
    $('#heightMM').html(design['height MM']);

}

function buildParts (partsList) {
    var numberOfParts = partsList.length;
    var tableRows= [];
    for( a=0; a < numberOfParts ; a++){

        alt ='class="alt"';
        if (a%2 == 0 ){
            alt=''
        }

        part = partsList[a];
        if (!part.partNumber) {
            part.partNumber = '';
        }
        if (!part.material) {
            part.material = '';
        }
        var row =
            ['<tr '+alt+' >',
                '<td>'+part.partNumber+'</td>',
                '<td>'+part.name+'</td>',
                '<td>'+part.material+'</td>',
                '<td>'+part['Cut Size Inches']+'</td>',
                '<td>'+part['Cut Size MM']+'</td>',
                '<td>'+part['Lengths']+'</td>',
                '<td> &nbsp;</td>',
                '</tr>'].join('\n');
        tableRows.push(row);
    }

    $('#parts tr:last').after(tableRows);

}

function buildItems (itemsList) {

    var numberOfItems = itemsList.length;
    tableRows= [];
    for( a=0; a < numberOfItems ; a++){
        alt ='class="alt"';
        if (a%2 == 0 ){
            alt=''
        }
        item = itemsList[a];
        if (!item.partNumber) {
            item.partNumber = '';
        }
        var row =
            ['<tr '+alt+' >',
                '<td>'+item.partNumber+'</td>',
                '<td>'+item.name+'</td>',
                '<td>'+item.quantity+'</td>',
                '</tr>'].join('\n');
        tableRows.push(row);
    }
    $('#items tr:last').after(tableRows);

}

function buildPanel (panelInfo) {

    var imageURLs = {
        "leftSwing" : "../images/Swing-Left.gif",
        "left"      : "../images/Panel-Left.gif",
        "divider"   : "../images/Divider-Bar.gif",
        "rightSwing" : "../images/Swing-Right.gif",
        "right"      : "../images/Panel-Right.gif"
    }

    if (panelInfo['Swing Door'] == "both") {
        buildPanelBoth(panelInfo.panels,panelInfo['sliding left'], imageURLs);
    }
    else if (panelInfo['Swing Door'] == "left" ){
        buildPanelLeft(panelInfo.panels,imageURLs);
    }
    else if (panelInfo['Swing Door'] = "right") {
        buildPanelRight(panelInfo.panels,imageURLs);
    }

}

function buildPanelBoth (numberOfPanels, numberLeft, imageURLs) {

    jQuery('<img/>', {
        src: imageURLs.leftSwing,
        class: 'panelImage'
    }).appendTo('#panel');


    for( a=0; a < numberLeft ; a++){

        jQuery('<img/>', {
            src: imageURLs.left,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.divider,
        class: 'panelImage'
    }).appendTo('#panel');

    for( a=0; a < numberOfPanels - numberLeft - 2 ; a++){

        jQuery('<img/>', {
            src: imageURLs.right,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.rightSwing,
        class: 'panelImage'
    }).appendTo('#panel');


}


function buildPanelLeft (numberOfPanels, imageURLs) {

    jQuery('<img/>', {
        src: imageURLs.leftSwing,
        class: 'panelImage'
    }).appendTo('#panel');

    for( a=0; a < numberOfPanels - 1 ; a++){

        jQuery('<img/>', {
            src: imageURLs.left,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.divider,
        class: 'panelImage'
    }).appendTo('#panel');


}

function buildPanelRight (numberOfPanels, imageURLs) {

    jQuery('<img/>', {
        src: imageURLs.divider,
        class: 'panelImage'
    }).appendTo('#panel');


    for( a=0; a < numberOfPanels - 1 ; a++){

        jQuery('<img/>', {
            src: imageURLs.right,
            class: 'panelImage'
        }).appendTo('#panel');
    }

    jQuery('<img/>', {
        src: imageURLs.rightSwing,
        class: 'panelImage'
    }).appendTo('#panel');


}

function printCutSheet() {

    var html = $('#design').html();
    var url = "https://" + apiHostV1 + "/api/printCutSheet";

    $.download(url,html);


}

function sheetData () {

    var id = $.url().param('id');

    var url = "https://"+apiHostV1+"/api/cutSheet/"+id+'/?'+addSite();
    $.getJSON(url , function (apiJSON) {

        buildJobInfo(apiJSON.jobInfo);
        buildParts(apiJSON.parts);
        buildItems(apiJSON.items);
        buildDesign(apiJSON.design);
        buildDoorDetails(apiJSON.details);
        buildGlass(apiJSON.glass);
        buildHeader(apiJSON.header,apiJSON.percentage);
        buildPanel(apiJSON.design);


    });
}

getTemplate();

$(document).ready(function(){

    //getTemplate();
});