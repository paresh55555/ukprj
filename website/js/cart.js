'use strict'

var hardwareOption;

var moduleTitleConversion = {
    "1": "Thermaclad", "2": "Absolute Vinyl", "3": "Signature Aluminum",
    "4": "Thermaclad", "5": "Thermaclad", "6": "Absolute Vinyl",
    "7": "Absolute Vinyl", "8": "Signature Aluminum", "9": "Signature Aluminum",
    "10": "Bevelled PVC", "11": "Bevelled PVC", "12": "Bevelled PVC",
    "13": "Sculptured PVC", "14": "Sculptured PVC", "15": "Sculptured PVC"
};


function validCartItem(singleDoor) {

    return true;

}

function trackConv(google_conversion_id, google_conversion_label) {
    var image = new Image(1, 1);
    image.src = "//www.googleadservices.com/pagead/conversion/" + google_conversion_id + "/?label=" + google_conversion_label + "&script=0";
}

function buildCart() {

    setTab('cart');
    updateFormStatus();

    $('#screen').unbind("click").bind("click", salesAdminApplyChanges);

    var url = 'html/cart3.html';
    if (isUK()) {
        url = 'html/cartUK.html'
    }

    $.get(url, function (html) {

        if (isGuest() && location.host == 'panoramicdoors.salesquoter.com') {
            trackConv('994406588', 'TIgyCKnppmYQvOGV2gM')
        }

        var cartRow = 1;
        $('#mainContent').html(html);
        if (isGuest()) {
            $('#callText').html("A representative will assist you to complete your order.<BR>For your convenience, a copy of your quote has been emailed to you.");
        }
        $('#preMainContent').html('');
        $('#dynamicContent').hide();
        if (isAdmin() && !isEmptyObject(quote.Cart) && !isPart()) {
            var cutSheetButton = '<div id="cart-printCutSheetSales" class="quoteActionButtonsWide">Print Cut Sheet</div>';
            $('#salesPersonCartInfo').append(cutSheetButton);
        }

        var print = getParameter('print');
        $('#startOfDynamicContent').show();
        $('#dynamicContent').show();
        if (print == "yes") {
            user.type = "guest";
        }
        updateCartForSalesOrGuest();

        var backImage = backSVG();
        var html = '<div id="headerStartOverImage">' + backImage + '</div>' +
            '<div id="headerStartOver" onclick="newDoor()" ' + cssFont + '>New System</div>';

        $('#startOverButton').html(html);

        $('#footer').hide();
        $('#workingOnTitle').html('Cart');

        var border = {"border-color": "#FF2409", "border-width": "1px", "border-style": "solid"};
        var totalItemsInCart = getTotalItemsInCart(quote.Cart);

        var cartMainContent = $('#cartMainContent');

        for (var property in quote.Cart) {

            var quantity = quote.Cart[property].quantity;

            if (print == 'yes') {
                for (var a = 0; a < quantity; a++) {
                    quote.Cart[property].index = cartRow;
                    quote.Cart[property].totalItems = totalItemsInCart;
                    quote.Cart[property].quantity = 1;
                    var html = addCartRowHtmlWithRowNumber(quote.Cart[property]);

                    cartMainContent.append(html);
                    cartRow = cartRow + 1;
                }
            } else {
                var cartRowText = cartRow;
                if (quantity > 1) {
                    var cartRow2 = parseInt(quantity) + parseInt(cartRow) - 1;
                    cartRowText = cartRow + '-' + cartRow2;
                    cartRow = cartRow + quantity - 1;
                }
                quote.Cart[property].index = cartRowText;
                quote.Cart[property].totalItems = totalItemsInCart;

                if (!empty(quote.Cart[property].panelOptions)) {
                    var html = addCartRowHtmlWithRowNumber(quote.Cart[property]);
                    cartMainContent.append(html);
                    cartRow = cartRow + 1;
                    if(isWindowModule(quote.Cart[property])){
                        $("#cart-printCutSheetSales").hide();
                    }
                }
            }
        }

        var usePhone = siteDefaults.phone
        if (!empty(quote.phone)) {
            usePhone = quote.phone;
        }

        $('#callToOrder').html('Call to Order Now: ' + usePhone);

        if (empty(quote.prefix)) {
            quote.prefix = getParameter('prefix');
        }
        $('#referenceQuote').html('Reference Quote#: ' + quote.prefix + quote.id);

        if (hasCustomColorInCart()) {
            var workingDiscounts = quote.SalesDiscount;
            if (empty(workingDiscounts.Totals.additionalExtras)) {
                workingDiscounts.Totals.additionalExtras = [];
            }
            if (!workingDiscounts.Totals.additionalExtras.length ||
                !workingDiscounts.Totals.additionalExtras.find(function (extra) {
                    return extra.name === "Custom Color"
                })) {
                var addExtra = {"name": "Custom Color", "amount": siteDefaults.custom_color_price, "showOnQuote": true};
                workingDiscounts.Totals.additionalExtras.push(addExtra);
            }
        }
        $('#cartFooterCenter').html('Total ' + footerCartTotalCost() + footerNoteHTML());
        $('img.cartIcons').unbind("click").bind("click", evaluateRowIcons);
        $('.textButton').unbind("click").bind("click", evaluateRowIcons);

        attachEditableFieldsListeners();

        $(".cutSheetBoxDate").datepicker({gotoCurrent: true});

        if (print == "yes") {
            $('.quoteActionButtonsLeft').hide();
            $('.quoteActionButtons').hide();
            $('#companyHeader').hide();
            $('.cartRow').css({'margin-bottom': '0px'});
            //$('.fullPage').css({'height':'1100px'});
            $('#cartFooter').hide();
            $('#startOfDynamicContent').css({'margin-top': '-100px'});
        }

        var edit = getParameter('edit');
        if (edit == 'email') {
            var params = {};
            params.quote = {};
            params.quote.id = quoteID;
            params.selector = "#preMainContent";
            var html = buildGuestEmailQuote(quoteID);
            $('#preMainContent').html(html);
            $('.emailButtons', '#sendingEmail').unbind("click").bind("click", sendGuestEmailButtons);
        }
        if (edit == 'emailConfirmed') {
            buildGuestEmailConfirmed();
        }

        if (isEmptyObject(quote.Cart)) {
            $('#saveAsQuote').hide();
            $('#emailGuestQuote').hide();
            $('#printGuestQuote').hide();
            $('#referenceQuote').html('Reference Quote#:');
            $('#startOver-guest').hide();
        }
        setupAdminLauncher();
        showContentAndSaveState();
    });
}


function setupAdminLauncher() {

    if (isSalesPerson() || isAccounting()) {
        addSalesAdminWithLocation('#cartFooterRight');
    }
}


function setPreCost(totalInCart) {

    if (!empty(quote.SalesDiscount)) {
        if (!empty(quote.SalesDiscount.Totals)) {
            quote.SalesDiscount.Totals.preCost = totalInCart;
        }
    }
}


function loadSoloCart() {

    var print = getParameter('print');

    var cartRow = 1;
    var border = {"border-color": "#FF2409", "border-width": "1px", "border-style": "solid"};
    var totalInCart = getCostOfItemsInCart(quote.Cart);

    var totalItemsInCart = getTotalItemsInCart(quote.Cart);

    setPreCost(totalInCart);

    var cartMainContent = $('#mainContent');
    cartMainContent.html('');

    for (var property in quote.Cart) {

        var quantity = quote.Cart[property].quantity;

        if (print == 'yes') {
            for (var a = 0; a < quantity; a++) {
                quote.Cart[property].index = cartRow;
                quote.Cart[property].totalItems = totalItemsInCart;
                if (empty(quote.Cart[property].quantity)) {
                    quote.Cart[property].quantity = 1
                }
                var html = addCartRowHtmlWithRowNumber(quote.Cart[property]);

                cartMainContent.append(html);
                cartRow = cartRow + 1;
            }
        } else {
            var cartRowText = cartRow;
            if (quantity > 1) {
                var cartRow2 = parseInt(quantity) + parseInt(cartRow) - 1;
                cartRowText = cartRow + '-' + cartRow2;
                cartRow = cartRow + quantity - 1;
            }
            quote.Cart[property].index = cartRowText;
            quote.Cart[property].totalItems = totalItemsInCart;

            var html = addCartRowHtmlWithRowNumber(quote.Cart[property]);
            cartMainContent.append(html);
            cartRow = cartRow + 1;
        }
    }

    attachEditableFieldsListeners();


    $('#cartFooterCenter').html('Total ' + footerCartTotalCost() + footerNoteHTML());

    $('img.cartIcons').unbind("click").bind("click", evaluateRowIcons);

    if (siteDefaults.salesFlow == 1 || siteDefaults.salesFlow == 3) {
        $(".cutSheetBoxDate").datepicker({gotoCurrent: true});
        $(".viewOrderDateBox").datepicker({gotoCurrent: true, "dateFormat": "mm/dd/yy"});
    }
}

function attachEditableFieldsListeners() {
    $('.cartQuantity').keyup(updateQuantity);

    if (isAudit()) {
        $('.quoteNotes').prop('disabled', 'disabled');
        $('.productionNotes').prop('disabled', 'disabled');
    } else {
        $('.quoteNotes').keyup(updateNoteDelay).on('paste', processPaste);
        $('.productionNotes').keyup(updateProductionNotesPerQuote).on('paste', processPaste);

    }

}

function processPaste(e) {
    var $this = $(this),
        pastedText = e.originalEvent.clipboardData.getData('text'),
        field = $this.attr('class'),
        previousText = $this.val();

    setTimeout(function () {

        if (!empty(previousText) && !empty(pastedText)) {
            $this.val(previousText + pastedText);
        }

        if (!empty(pastedText) && empty(previousText)) {
            $this.val(pastedText);
        }

        if (field == 'productionNotes') {
            updateProductionNotesPerQuote.call($this);
        } else if (field == 'quoteNotes') {
            updateNoteDelay.call($this);
        } else if (field == 'paymentOrderNotes') {
            updatePaymentNoteDelay.call($this);
        }
    }, 100);
}

function loadCartTopNav() {

    var total = getTotalItemsInCart(quote.Cart);

    if (total < 1) {
        alert("Currently there are no items in your cart");
    } else {
        var status = isSizeComplete()
        if (status || !formStatus.unique) {
            trackAndDoClick('cart');
        } else{
            missingSelections()
        }
    }
}


function loadCart() {

    var quote = getParameter('quote');

    if (quote) {
        getCartFromQuote(quote);
    } else {
        buildCart();
    }


}


function getCartFromQuote(quoteID) {

    var guest = getParameter('guest');
    var token = getParameter('token');

    if (empty(quoteID)) {
        bounceUser();
        return;
    }

    var url = "https://" + apiHostV1 + "/api/quotes/" + quoteID + "/guest?guest=" + guest + "&token=" + token + addSite();
    $.getJSON(url, function (apiJSON) {
        validate(apiJSON);

        quote = apiJSON;

        writeQuote(quote);

        // quote.SalesDiscount = {};
        quote = setSalesDiscountToEmptyWithPassQuote(quote);
        // quote.SalesDiscount.Totals = {};
        // quote.SalesDiscount.Totals.code = "YZ-00";
        // if (siteName == "pd" || siteName == "ces") {
        //     quote.SalesDiscount.Totals.code = "YZ-08";
        // }

        buildCart();
    });
}


function adminPrint() {

    createQuoteForCutSheet();
}


function updateCartForSalesOrGuest() {

    if (isSalesPerson()) {
        $('#guestCartInfo').hide();
        $('#home-sales').unbind("click").bind("click", newDoor);
        $('#saveAsQuote').unbind("click").bind("click", salesPersonStartCreatingQuote);
        $('#startOver-sales').unbind("click").bind("click", salesCartStartOver);
        $('#viewQuotes-sales').unbind("click").bind("click", salesPersonCloseViewQuotes);
        $('#cart-printCutSheetSales').unbind("click").bind("click", adminPrint);
    }

    if (user.type == 'guest') {
        $('#salesPersonCartInfo').hide();
        $('.quoteActionButtonsLeft').unbind("click").bind("click", navigationClick);
        $('#startOver-guest').unbind("click").bind("click", guestStartOver);
        $('#emailGuestQuote').unbind("click").bind("click", emailGuestQuoteClick);
        $('#printGuestQuote').unbind("click").bind("click", printGuestQuote);

    }
}


function salesPersonCloseViewQuotes() {
    trackAndDoClick('viewQuotes');

}

function salesPersonStartCreatingQuote() {

    var isEmpty = false;
    $(".cartQuantity").each(function () {
        var quantity = $(this).val();
        if (empty(quantity)) {
            isEmpty = true;
        }
    });
    if (isEmpty) {
        alert("The field Qty can't be empty");
        return;
    }

    startSpinner();
    if (siteDefaults.customer_info == 1) {
        delete quote.id;
        trackAndDoClick('customer', '&newQuote=yes');
    }else{
        saveQuoteWithDealer();
    }

}

function emailGuestQuoteClick() {
    trackAndDoClick('cart', '&edit=email');

}

function sendGuestEmailButtons() {

    var id = $(this).attr('id');
    var quote = splitForFirstOption(id);
    var action = splitForSecondOption(id);

    if (action == "emailSend") {
        sendGuestEmail(quote);
    } else {
        trackAndDoClick('cart');
    }
}

function buildGuestEmailQuote(id) {

    var email = '';
    if (user.email) {
        email = escapeHtml(user.email);
    }

    var html = '' +
        '<div id="sendingEmail" class="emailBox">' +
        '<input class="textBox" type="text" id="emailAddresses" value="' + email + '"  placeholder="Enter Recipients ( Separate with comma )"/>' +
        '<div id="' + id + '-emailSend" class="emailButtons">Send</div>' +
        ' <TEXTAREA id="emailMessage" NAME=address ROWS=4 placeholder="Enter Message"></TEXTAREA>' +
        '<div id="' + id + '-emailCancel"  class="emailButtons">Cancel</div>' +
        '<div class="emailCheckBoxContainer">' +
        '</div>' +
        '</div>';

    return html;

}

var entityMap = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': '&quot;',
    "'": '&#39;',
    "/": '&#x2F;'
};

function escapeHtml(string) {
    return String(string).replace(/[&<>"'\/]/g, function (s) {
        return entityMap[s];
    });
}

function sendGuestEmail() {

    var debug = getParameter('debug');

    $('.emailButtons:first-of-type').replaceWith("<div class='emailQuoteSpinner'><img src='/images/icons/spinning.gif'></div>");
    $('.emailButtons:nth-of-type(2n)').css('visibility', 'hidden')

    var address = $('#emailAddresses').val();

    var addQuote = quote.id;

    var message = $('#emailMessage').val();

    var params = {"address": address, "message": message, "quote": addQuote, guest: user.id, token: user.token};
    var json = JSON.stringify(params);

    var url = "https://" + apiHostV1 + "/api/emailGuest?" + authorize() + addSite() + '&debug=' + debug;

    $.post(url, json, function (data) {

        validate(data);
        var response = jQuery.parseJSON(data);
        if (response.message == 'success') {
            trackAndDoClick('cart', '&edit=emailConfirmed');
        } else {
            alert("Error Sending the Email");
        }
    });
}


function buildGuestEmailConfirmed() {

    var html = '' +
        '<div id="sendingEmail" class="emailBox">' +
        '<div id="emailDone" class="emailButtons">Done</div>' +
        '<div class="emailInfo">Your email has been sent!</div>' +
        '</div>';

    $('#preMainContent').html(html);
    $('#emailDone').unbind("click").bind("click", emailDone);
}


function emailDone() {

    trackAndDoClick('cart');
}


function printGuestQuote() {

    var debug = getParameter('debug');

    var guest = $.cookie('user');

    var url = "https://" + apiHostV1 + "/api/guestQuotePrint/" + quote.id + "?prefix=" + quote.prefix + "&guest=" + guest.id + "&token=" + guest.token + addSite() + '&debug=' + debug;

    var newWin = window.open(url);
    if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
        alert("Popup window was blocked, turn off your popup blocker to allow for printing.");
    }
}


function newDoor() {

    trackAndDoClick('home');
}


function setQuoteActionsBox() {

    var quote = getParameter('quote');

    //Encase it gets called by order page
    if (empty(quote)) {
        quote = getParameter('order');
    }

    var params = {};
    params.quote = {};
    params.quote.id = quote;
    params.edit = 'item';

    if (!$('#savingQuote').attr('id')) {
        var html = buildQuoteActions(params);
        $('.quoteActionsBox').html(html);
        $('.quoteActionButtons', '.quoteDetailsBox').unbind("click").bind("click", quoteActions);

    }
}


function updateNoteDelay() {

    var selector = this;


    delay(function () {
        updateNote(selector);
    }, 200);

}

function updateNote(selector) {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    setQuoteActionsBox();

    var note = $(selector).val();
    var id = splitForFirstOption($(selector).attr('id'));


    quote.Cart[id].note = note;
    writeQuote(quote);
    formStatus = {};

    //formStatus = undefined;
    //saveToQuote(quote);

}


function updateProductionNotesPerQuote() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    setQuoteActionsBox();

    var productionNotes = $(this).val();
    var id = splitForFirstOption($(this).attr('id'));

    quote.Cart[id].productionNotes = productionNotes;
    writeQuote(quote);
    formStatus = {};
}


function updateQuantity() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    setQuoteActionsBox();

    var quantity = $(this).val();
    quantity = validQuantity(quantity);
    if (empty(quantity)) {
        return;
    }
    $(this).val(quantity);

    var id = $(this).attr('id');

    var subTotalSelector = $('#' + id + "-subTotal");
    var doorCost = subTotalSelector.attr('data-door-cost');
    subTotalSelector.html('Item Sub-total: ' + currency + formatMoney(doorCost * quantity, 2, '.', ','));

    quote.Cart[id].quantity = quantity;
    writeQuote(quote);
    formStatus = quote.Cart[id];

    var total = footerCartTotalCost();
    $('#cartFooterCenter').html('Total ' + total);
    $('#footerPrice').html('Total ' + total + footerNoteHTML());

    if (isGuest()) {
        clearTimeout(wto);
        wto = setTimeout(function () {
            updateQuote();
        }, 1000);
    }
}

function roundNumber(value) {
    return Number((value).toFixed(2));
}

function formatMoney(amount, c, d, t) {
    var j;
    var n = amount,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;

    var finalAmount = s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    if (finalAmount == "-0.00") {
        finalAmount = '0.00';
    }
    return finalAmount;
};


Number.prototype.formatMoney = function (c, d, t) {
    var j;
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function getTotalItemsInCart(cart) {

    var totalItemsCart = 0;

    for (var property in cart) {
        var row = cart[property];


        row.quantity = cleanQuantityNumber(row.quantity);

        totalItemsCart = totalItemsCart + row.quantity;
    }

    return totalItemsCart;

}

function cleanQuantityNumber(quantity) {

    quantity = Number(quantity);
    if (!quantity) {
        quantity = 1;
    }
    return quantity;

}

function getCostOfItemsInCart(cart) {

    var cost = 0;
    for (var property in cart) {
        var row = cart[property];

        var quantity = cleanQuantityNumber(row.quantity);
        cost = cost + (Number(row.total) * Number(quantity));
    }

    return cost;
}


function formHasAllTheValidItemsSelected(formStatus) {
    var status = true;

    if (empty(formStatus)) {
        return false;
    }

    if (isSalesPerson()) {
        if (!formStatus.width) {
            status = false;
        }
        if (!formStatus.height) {
            status = false;
        }
        if (!isSizeComplete) {
            status = false;
        }

        if (!empty(formStatus.panelOptions)) {
            if (!formStatus.panelOptions.track.selected) {
                status = false;
            }

            if (!formStatus.panelOptions.frame.selected) {
                status = false;
            }
        }
    }
    if (!empty(formStatus.panelOptions)) {
        if (!formStatus.panelOptions.panels.selected) {
            status = false;
        }
    }
    if (!formStatus.glassChoices && isAnOption('glass') === true) {
        status = false;
    }

    if (!formStatus.hardwareChoices) {
        status = false;
    }

    return status;
}


function cartButtonText(formStatus) {

    var buttonText = '';
    if (formStatus.unique) {
        buttonText = "Save Changes";
    } else {
        buttonText = "Add to Quote";
    }
    return buttonText;
}


function cartButtonAddToCartOrSaveChanges(formStatus) {

    var message = cartButtonText(formStatus);
    if (!formHasAllTheValidItemsSelected(formStatus)) {
        message = '';
    }

    return message;
}


function uniqueID() {

    var unique = new Date().getTime();
    unique = "Cart" + unique;
    return unique;
}


function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}


function updateGlobalCartWithFormStatus(formStatus) {

    if (isEmptyObject(formStatus)) {
        return quote.Cart;
    }
    if (isEmptyObject(quote.Cart)) {
        quote.Cart = {};
    }

    if (formStatus.unique) {
        quote.Cart[formStatus.unique] = formStatus;
    } else {
        var unique = uniqueID();
        formStatus.unique = unique;
        quote.Cart[unique] = formStatus;
    }
    if(!empty(windowsForm.windowType)){
        quote.Cart[formStatus.unique]['windowsData'] = windowsForm;
    }

    return quote.Cart;
}


function addToCart() {

    if (quote.id != null) {
        saveToQuote(quote.id);
    }
    else {
        var cart = updateGlobalCartWithFormStatus(formStatus);
        writeCart(cart);
        writeFormStatus();
        trackAndDoClick('cart');
    }
}


function evaluateRowIcons() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    var id = $(this).attr('id');
    var cart = readCart();
    var tab = $.cookie('tab');
    var quote = getParameter('quote');
    var order = getParameter('order');

    if (id) {
        var Array = id.split('-');

        var action = Array[0];
        var index = Array[1];

        if (empty(cart[index])) {
            var newTab = getParameter('tab');
            trackAndDoClick(newTab);
            return;
        }

        if (action === 'duplicate') {
            cart = duplicateRowFromCart(index);
            writeCart(cart);
            var quote = getParameter('quote');
            var order = getParameter('order');
            if (!empty(quote)) {
                formStatus = {};
                noCheckSaveQuote();
            } else if (!empty(order)) {
                formStatus = {};
                noCheckSaveQuote();
            } else {
                loadCart();
            }
        }
        if (action === 'delete') {
            deleteRowFromCart(index);
        }

        if (action === 'edit') {
            if (cart[index].windowsData) {
                moduleInfo.firstTab = 'sash';
            } else{
                moduleInfo.firstTab = 'size';
            }

            window.doorEditing = true;
            formStatus = cart[index];
            writeFormStatus();
            if(cart[index].windowsData){
                windowsForm = cart[index].windowsData;
                writeWindowsForm();
            }
            if (quote) {
                trackAndDoClick(tab, "&quote=" + quote + "&edit=item&cartItem=" + index + '&secondTab=' + moduleInfo.firstTab);
            } else if (order) {
                trackAndDoClick(tab, "&order=" + order + "&edit=item&cartItem=" + index + '&secondTab=' + moduleInfo.firstTab);
            } else {
                //formStatus = cart[index];
                //writeFormStatus();
                $.when(getModuleOptions()).done(function (moduleInfoFromApi) {
                    moduleInfo = moduleInfoFromApi;
                    trackAndDoClick(moduleInfo.firstTab);
                });
            }
        }
    }
}


function deleteRowFromCart(index) {

    if (confirm("Are you sure you want to delete this door?") == true) {

        delete quote.Cart[index];
        writeQuote(quote);
        formStatus = {};

        if (isGuest()) {
            return;
            savingGuestCart();
        }
        else {
            var quoteID = getParameter('quote');
            var orderID = getParameter('order');
            if (!empty(quoteID)) {
                noCheckSaveQuote();
            } else if (!empty(orderID)) {
                noCheckSaveQuote();
            } else {
                if(quote && !$.isEmptyObject(quote.Cart)){
                    trackAndDoClick('cart');
                }else{
                    salesStartOver();
                }
            }
        }
    }
}


function duplicateRowFromCart(index) {

    var cartRowText = JSON.stringify(quote.Cart[index]);
    var cartRow = JSON.parse(cartRowText);

    var unique = uniqueID();
    cartRow.unique = unique;
    quote.Cart[unique] = cartRow;

    return (quote.Cart);
}

function displayFinishes(row) {

    var exteriorFinish = buildColorName(row.extColor);
    var interiorFinish = buildColorName(row.intColor);

    if (empty(row.intNotSameExt)) {
        interiorFinish = exteriorFinish;
    } else if (row.intNotSameExt == false) {
        interiorFinish = exteriorFinish;
    }

    var extFinishHTML = '' +
        '<div class="cartDescriptionRowMain">Exterior Finish:</div>' +
        '<div class="cartDescriptionRowSecond">' + exteriorFinish + '</div>' + cartDescriptionInitials();

    var intFinishHTML = '' +
        '<div class="cartDescriptionRowMain">Interior Finish:</div>' +
        '<div class="cartDescriptionRowSecond">' + interiorFinish + '</div>' + cartDescriptionInitials();

    if (exteriorFinish == 'None' && row.showNoneOnCart == 0) {
        extFinishHTML = '';
    }
    if (interiorFinish == 'None' && row.showNoneOnCart == 0) {
        intFinishHTML = '';
    }

    return extFinishHTML + intFinishHTML;
}


function displayGlassOptions(row) {

    var option = optionsFromCartRow(row.glassOptions);

    var glassOptions = '' +
        '<div class="cartDescriptionRowMain">Glass Options:</div>' +
        '<div class="cartDescriptionRowSecond">' + option + '</div>' + cartDescriptionInitials();


    if (option == 'None' && row.showNoneOnCart == 0) {
        glassOptions = '';
    }

    return glassOptions;
}


function displayHardwareExtras(row) {

    var option = optionsFromCartRow(row.hardwareExtraChoices);
    var hardwareOptions = '<div class="cartDescriptionRowMain">Hardware Options:</div>' +
        '<div class="cartDescriptionRowSecond">' + option + '</div>' + cartDescriptionInitials();

    if (option == 'None' && row.showNoneOnCart == 0) {
        hardwareOptions = '';
    }

    return hardwareOptions;
}

function displayUSScreens(row) {

    var option = optionsFromCartRow(row.screenUS);
    var screenOptions = '<div class="cartDescriptionRowMain">Screen:</div>' +
        '<div class="cartDescriptionRowSecond">' + option + '</div>' + cartDescriptionInitials();

    if (option == 'None' && row.showNoneOnCart == 0) {
        screenOptions = '';
    }

    return screenOptions;
}

function optionsFromCartRow(options) {

    var displayOfItems = '';
    for (var item in options) {
        if (options.hasOwnProperty(item)) {
            if (options[item] == 'yes') {
                var ArrayItems = item.split('-');

                if (displayOfItems == '') {
                    displayOfItems = ArrayItems[1];
                }
                else {
                    displayOfItems = displayOfItems + ', ' + ArrayItems[1];
                }
            }
        }
    }

    if (!empty(displayOfItems)) {
        return displayOfItems;
    }
    else {
        return "None";
    }
    return displayOfItems;
}


function glassTypeFromCartRow(row) {

    var option = optionsFromCartRow(row.glassChoices);

    var glassOptions = '' +
        '<div class="cartDescriptionRowMain">Glass Type:</div>' +
        '<div class="cartDescriptionRowSecond">' + option + '</div>' + cartDescriptionInitials();


    if (option == 'None' && row.showNoneOnCart == 0) {
        glassOptions = '';
    }

    return glassOptions;
}


function buildCartPanels(row) {

    var numberOfPanels = row.numberOfPanels;
    var html = '<table class="centerTable" style="width:' + 20 * numberOfPanels + 'px"><tr>'
    for (var a = 1; a <= numberOfPanels; a++) {
        html = html + '<td>' + a + '</td>'
    }
    html = html + '</tr></table>';
    html = html + '<div class="cartItemBoxLabel">Panels</div>';

    return (html);
}


function findHardwareImage(hardwareName, row) {

    if (!row.hardwareToChooseFrom) {
        return;
    }
    var numberOfHardware = row.hardwareToChooseFrom.length;

    for (var a = 0; a <= numberOfHardware - 1; a++) {
        var hardware = row.hardwareToChooseFrom[a];
        if (hardware.name == hardwareName) {
            return hardware.url;
        }
    }
}


function buildHardwarePreview(hardwareName, row) {

    var image = findHardwareImage(hardwareName, row);

    var html = '';
    var width = 165;
    if(cartRowHasDoorStyle(row)){
        width = 99;
    }
    if (!empty(image)) {
        html = '<div class="hardwareCart " style="height: '+width+'px"><img src="' + image + '" height="'+width+'px" ></div>' +
            '<div class="cartItemBoxLabel">' + hardwareName + '</div> ';
    }

    return html;
}


function buildCartColorBarsWithColorAndLocation(colorOption, location) {

    if (empty(colorOption)) {
        return '';
    }

    if (empty(colorOption.customName) && empty(colorOption.name1)) {
        return '';
    }

    var info = determineColorInfo(colorOption);


    var html = '' +
        '<div class="cartItemBox">' +
        '   <div class="cartItemBoxColor" style="background-color: ' + info.color + '  ">' + info.img + '</div>' +
        '   <div class="cartItemBoxLabel">' + location + '</div> ' +
        '   <div class="cartItemBoxLabel">' + info.fullColorName + '</div> ' +
        '</div>' + cartFinishOptionsInitials();

    return (html);
}


function determineColorInfo(colorOption) {

    var img = ''

    if (!empty(colorOption.url)) {
        img = '<img src="' + colorOption.url + '" width="99" height="99" >';
    }

    var fullColorName = '';
    var color = '';
    if (colorOption.type == 'custom' && !empty(colorOption.customName)) {
        img = '<img src="/images/colors/customColor.jpg" width="99" height="99" >';
        fullColorName = colorOption.customName + ' (custom)';
    } else if (!empty(colorOption.name1)) {

        fullColorName = colorOption.name1;
        if (!empty(colorOption.name2)) {
            fullColorName = colorOption.name1 + " (" + colorOption.name2 + ")";
        }
        color = colorOption.hex;
    }

    var info = {};
    info.img = img;
    info.fullColorName = fullColorName;
    info.color = color;

    return info;
}

function buildPreviewButton(id) {

    var html = '<div id=""' + id + '" class="previewButton">View Larger</div>';

    return html;
}


function isEven(value) {
    if (value % 2 == 0)
        return true;
    else
        return false;
}


function setBackgroundRowColor(index) {

    var color = "#F7F7F7";
    if (isEven(index)) {
        color = "#FFFFFF";
    }
    return color;
}


function getHardwareOption(name) {

    var module = 1;
    var url = "https://" + apiHostV1 + "/api/hardware?module=" + module + "&name=" + name + addSite();
    $.getJSON(url, function (apiJSON) {
        hardwareOption = apiJSON;
    });
}


function getJobInfoFromRow(name, row) {

    if (row.jobInfo) {
        if (row.jobInfo[name]) {
            return row.jobInfo[name];
        } else {
            return '';
        }
    } else {
        return '';
    }
}


function panelMovementNumbers(row) {

    var panels = row.numberOfPanels
    var numberOfSwings = getNumberOfSwings(row);

    var type = row.panelOptions.swingDirection.selected;

    var slideLeft = 0;
    var slideRight = 0;

    if (type == 'right') {
        slideRight = panels - 1;
    } else if (type == 'left') {
        slideLeft = panels - 1;
    } else if (type == 'both') {
        slideLeft = splitForSecondOption(row.panelOptions.movement.selected);
        if (empty(slideLeft)) {
            slideLeft = row.panelOptions.movement.numberLeft;
        }
        if (empty(slideLeft)) {
            slideLeft = splitForSecondOption(row.panelMovement);
        }
        slideRight = panels - numberOfSwings - slideLeft
    }
    else {
        type = 'window';
    }

    var panelNumbers = {};
    panelNumbers.left = slideLeft;
    panelNumbers.right = slideRight;
    panelNumbers.panels = panels;
    panelNumbers.swings = numberOfSwings;
    panelNumbers.type = type;

    return panelNumbers;
}


function buildPanelMovement(row) {

    var panelNumbers = panelMovementNumbers(row);

    return "Left " + panelNumbers.left + " / Right " + panelNumbers.right;
}


function getNumberOfSwings(row) {

    if (row.panelOptions) {

        var swingDirection = row.panelOptions.swingDirection.selected;

        if (swingDirection == 'both') {
            return 2;
        } else if (swingDirection === 'left' || swingDirection === 'right') {
            return 1;
        } else {
            return 0;
        }
    }


    if (row.chooseSwings === 'both') {
        return 2;
    } else if (row.chooseSwings === 'left' || row.chooseSwings === 'right') {
        return 1;
    } else {
        return 0;
    }
}


function capitaliseFirstLetter(string) {

    if (!empty(string)) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}


function getCartImageURLs() {
    var imageURLs = {
        "leftSwing": "/images/panels/swingleft.svg",
        "left": "/images/panels/slideleft.svg",
        "divider": "/images/panels/divider.svg",
        "rightSwing": "/images/panels/swingright.svg",
        "right": "/images/panels/slideright.svg",
        "window": "/images/panels/window.svg"
    }

    return (imageURLs);
}

function getLeftImage(numberLeft) {

    var imageURLs = getCartImageURLs();

    var html = '<img class="panelImage" src="' + imageURLs.left + '">';

    if (numberLeft == '') {
        html = '<img class="panelImage"  src="' + imageURLs.window + '">';
    }

    return html;
}


function getRightImage(numberLeft) {

    var imageURLs = getCartImageURLs();

    var html = '<img class="panelImage" src="' + imageURLs.right + '">';
    if (numberLeft === '') {
        html = '<img class="panelImage"  src="' + imageURLs.window + '">';
    }

    return html;
}


function getDividerImage(numberLeft) {

    var imageURLs = getCartImageURLs();

    var html = '<img class="panelImage" src="' + imageURLs.divider + '">';
    if (!isUK()) {
        html = '';
    }

    if (numberLeft === '') {
        html = '';
    }

    return html;
}


function cartBuildPanelBoth(numberOfPanels, numberLeft) {

    var imageURLs = getCartImageURLs();
    var numberRight = numberOfPanels - numberLeft - 2;

    var html = '<img class="panelImage" src="' + imageURLs.leftSwing + '">';

    if (numberRight == "none") {
        for (var a = 0; a < numberOfPanels - 2; a++) {
            html = html + '<img class="panelImage" src="/images/panels/window.svg">';
        }
    }
    else {
        for (var a = 0; a < numberOfPanels - numberRight - 2; a++) {
            html = html + '<img class="panelImage" src="' + imageURLs.left + '">';
        }

        html = html + getDividerImage(numberLeft);

        for (var a = 0; a < numberRight; a++) {
            html = html + getRightImage(numberLeft);
        }
    }

    html = html + '<img class="panelImage" src="' + imageURLs.rightSwing + '">';

    return (html);
}


function cartBuildPanelLeft(numberOfPanels) {

    var imageURLs = getCartImageURLs();
    var html = '<img class="panelImage" src="' + imageURLs.leftSwing + '">';
    for (var a = 0; a < numberOfPanels - 1; a++) {
        html = html + '<img class="panelImage" src="' + imageURLs.left + '">';
    }
    if (numberOfPanels > 1) {
        if (isUK()) {
            html = html + '<img class="panelImage" src="' + imageURLs.divider + '">';
        }
    }
    return (html);
}

function cartBuildPanelBlank(numberOfPanels) {

    var imageURLs = getCartImageURLs();

    var html = '';

    for (var a = 0; a < numberOfPanels - 1; a++) {
        html = html + '<img class="panelImage" src="/images/panels/window.svg">';
    }
    html = html + '<img class="panelImage" src="' + imageURLs.rightSwing + '">';

    return (html);
}


function cartBuildPanelRight(numberOfPanels) {

    var imageURLs = getCartImageURLs();

    var html = '';
    if (numberOfPanels > 1) {
        html = '<img class="panelImage" src="' + imageURLs.divider + '">';
        if (!isUK()) {
            html = '';
        }
    }

    for (var a = 0; a < numberOfPanels - 1; a++) {
        html = html + '<img class="panelImage" src="' + imageURLs.right + '">';
    }
    html = html + '<img class="panelImage" src="' + imageURLs.rightSwing + '">';

    return (html);
}


function cartBuildPanelWindow(numberOfPanels) {

    var imageURLs = getCartImageURLs();
    var html = '';
    for (var a = 0; a < numberOfPanels; a++) {
        html = html + '<img class="panelImage" src="/images/panels/window.svg">';
    }

    return (html);
}


function buildSwingDirectionCart(row) {

    var swingDirection = '';
    var swingInOrOut = '';

    if(isWindowModule(row))return '';

    if (empty(row.panelOptions)) {
        swingDirection = chooseSwings;
        swingInOrOut = splitForSecondOption(row.swingDirectionSection);
    } else {
        swingDirection = row.panelOptions.swingDirection.selected;
        swingInOrOut = row.panelOptions.swingInOrOut.selected;
    }

    var swingDirectionImage = {
        left: {Inswing: "images/swing/Left In.svg", Outswing: "images/swing/Left Out.svg"},
        right: {Inswing: "images/swing/Right In.svg", Outswing: "images/swing/Right Out.svg"},
        both: {Inswing: "images/swing/Both In.svg", Outswing: "images/swing/Both Out.svg"}
    }
    
    if(cartRowHasDoorStyle(row)){
        swingDirectionImage = {
            left: {Inswing: "images/swing/Inside Left.svg", Outswing: "images/swing/Outside Left.svg"},
            right: {Inswing: "images/swing/Inside Right.svg", Outswing: "images/swing/Outside Right.svg"},
            both: {Inswing: "images/swing/Both In.svg", Outswing: "images/swing/Both Out.svg"}
        };
    }

    var swingsImage = '';
    if (!empty(swingDirection)) {
        swingsImage = '<img src="' + swingDirectionImage[swingDirection][swingInOrOut] + '" width="99px" >';
    }

    var swingBox = '<div class="cartItemBoxColor" ">' + swingsImage + '</div>';
    if (empty(swingInOrOut)) {
        swingBox = '';
    }


    var html = '' +
        swingBox +
        '<div class="cartItemBoxLabel">Operation </div> ' +
        '<div class="cartItemBoxLabel">' + swingDirection + '</div> ';

    return html;
}

function buidSwingImage(row) {

    if (empty(row.swingDirectionSection) && empty(row.panelOptions)) {
        return '';
    }

    if (empty(row.panelOptions.swingDirection.selected)) {
        return '';
    }

    if (isGuest() || isLead()) {
        return '';
    }

    var swingDirection = buildSwingDirectionCart(row);
    var swingDirectionRow = '' +
        '<div class="cartItemBox">' + swingDirection + '</div>' + cartFinishOptionsInitials();

    return swingDirectionRow;

}


function buildFinishOptionsColumn(row, extra) {

    if (empty(extra)) {
        extra = '';
    }
    var extBar = buildCartColorBarsWithColorAndLocation(row.extColor, 'Exterior');

    var intColorPassed = row.intColor;

    if (empty(row.intNotSameExt)) {
        intColorPassed = row.extColor;
    } else if (row.intNotSameExt == false) {
        intColorPassed = row.extColor;
    }

    var intBar = buildCartColorBarsWithColorAndLocation(intColorPassed, 'Interior');

    var hardwareName = splitForSecondOption(row.hardwareChoices);
    var hardwareImage = buildHardwarePreview(hardwareName, row);

    var swingDirectionRow = buidSwingImage(row);

    var vinylBar = '';
    if (row.vinylColor) {
        vinylBar = buildCartColorBarsWithColorAndLocation(row.vinylColor, '');
    }

    var html = '' +
        '<div class="cartItem navCartItems">' +
        '   ' + extra +
        '  ' + swingDirectionRow +
        '  ' + extBar +
        '  ' + intBar +
        '  ' + vinylBar +
        '   <div class="cartItemBox">' + hardwareImage + '</div>' + cartFinishOptionsInitials() +
        '</div>';

    return html;
}


function buildRowTitles(row) {

    var newTotal = markupTotal(Number(row.total));

    var newPrice = '' +
        '   <div class="cartRowTitleCostLabel">Factory Direct Price</div>' +
        '   <div class="cartRowTitleCost">' + currency + formatMoney(newTotal, 2, '.', ',') + '</div>';

    if (isUK()) {
        newPrice = '' +
            '   <div class="cartRowTitleCostLabel">Price</div>' +
            '   <div class="cartRowTitleCost">' + currency + formatMoney(newTotal, 2, '.', ',') + '</div>';

    }
    var purchaseCert = getParameter('purchaseCert');
    if (purchaseCert == 'yes') {
        newPrice = '';
    }

    var html = '' +
        '<div class="cartRowTitleLower">' +
        '   <div class="cartRowTitleFinish">Finish Options</div>' +
        '   <div class="cartRowTitleDescription">Description</div>' +
        '   ' + newPrice +
        '</div>';

    return html;
}


function makeQuoteTitle(quote, extra) {

    var title;

    if (empty(quote.prefix)) {
        quote.prefix = '';
    }
    if (empty(quote.postfix)) {
        quote.postfix = '';
    }

    var order = getParameter('yes');
    var postFix = quote.postfix;


    if (quote.version > 1 && order == 'yes' || quote.version > 1 && tab != 'viewQuote') {
        postFix = postFix + ' v' + quote.version.valueOf();
    }
    if (empty(extra)) {
        postFix = '';
    }


    if (empty(quote.orderName)) {
        title = '&nbsp;';
    } else {
        title = quote.orderName + postFix.valueOf();
    }
    //
    // if (isLead()) {
    //     title = 'SQ' + quote.id.valueOf();
    // }
    //
    // title = quote.orderName;

    return title;
}

function buildLetterForNumberOfDoors(doorNumber) {

    if (isNaN(doorNumber)) {
        var firstNumber = splitForFirstOption(doorNumber);
        var secondNumber = splitForSecondOption(doorNumber);

        var firstLetter = buildLetterForNumberOfDoors(firstNumber);
        var secondLetter = buildLetterForNumberOfDoors(secondNumber);

        return '-(' + firstLetter + '-' + secondLetter + ')';
    }

    var firstNumber = Math.floor(doorNumber / 26);
    var secondNumber = doorNumber % 26;

    if (secondNumber == 0) {
        firstNumber = firstNumber - 1;
        secondNumber = 26;
    }

    var firstLetter = convertNumberToLetter(firstNumber);
    var secondLetter = convertNumberToLetter(secondNumber);

    return firstLetter + secondLetter;
}


function convertNumberToLetter(doorNumber) {
    if (doorNumber == 0) {
        return '';
    }

    var letter = String.fromCharCode(97 + (doorNumber - 1));
    letter = letter.toUpperCase();

    return letter;
}


function buildRowHeader(row) {
    var letter = buildLetterForNumberOfDoors(row.index);
    var title = makeQuoteTitle(quote);

    if (empty(title) || title == '&nbsp;') {
        letter = '';
    } else {
        letter = '-' + letter;
    }


    var doorSystemInactive = '';
    var inActiveColor = '';


    var doorSystemInactive = '';
    var inActiveColor = '';

    if (inactiveModule(row.module) && 1 == 2) {
        doorSystemInactive = '<div class="doorSystemInactive">Door System Inactive</div>';
        inActiveColor = 'red';
    }

    var quoteRow = '<div class="cartRowHeaderItems ' + inActiveColor + '">' + doorSystemInactive + title + letter + '</div>';

    var html = '<div class="cartRowHeader">' +
        quoteRow +
        '<div class="cartRowHeaderTitle ' + inActiveColor + '" >' + row.moduleTitle + '</div>' +
        '<div class="cartRowHeaderItemsRight ' + inActiveColor + '" >Item ' + row.index + ' of ' + row.totalItems + ' </div>' +
        '</div>';
    return html;
}


function buildRowPanels(row) {

    var numberPanels = panelMovementNumbers(row);
    var panels = '';

    if (empty(numberPanels.left)) {
        numberPanels.left = splitForSecondOption(row.panelMovement);
    }

    if (numberPanels.type === 'both') {
        panels = cartBuildPanelBoth(numberPanels.panels, numberPanels.left);
    }
    else if (numberPanels.type === 'bothWindow') {
        panels = cartBuildPanelBoth(numberPanels.panels, 'none');
    }
    else if (numberPanels.type === 'left') {
        panels = cartBuildPanelLeft(numberPanels.panels);
    }
    else if (numberPanels.type === 'window') {
        panels = cartBuildPanelWindow(numberPanels.panels);
    }
    else if (numberPanels.type === 'right') {
        panels = cartBuildPanelRight(numberPanels.panels);
    }
    else {
        if (numberPanels.left < 1) {
            panels = cartBuildPanelBlank(numberPanels.panels);
        } else {
            panels = cartBuildPanelRight(numberPanels.panels);
        }

    }
    
    var width = 37 * numberPanels.panels + 20;
    
    var holderClass = 'cartRowPanelImageHolder';
    if(cartRowHasDoorStyle(row)){
        var data = $.grep(row.panelOptions.track.buttons, function(b){return b.name == row.panelOptions.track.selected});
        if(data.length){
            var leftExtender = '';
            var rightExtender = '';
            var addOns = row.addOnChoices;
            if (!empty(addOns)) {
                if (!empty(addOns['extender-leftExtender'])) {
                    leftExtender = '<img class="leftExtender" src="/images/extenders/uk/side_lite.svg">';
                }
                if (!empty(addOns['extender-rightExtender'])) {
                    rightExtender = '<img class="rightExtender" src="/images/extenders/uk/side_lite.svg">';
                }
            }
            panels = leftExtender+'<img class="panelImage" src="'+getOrientedDoorStyleImageUrl(data[0].url,row.panelOptions)+'">'+rightExtender;
            width = 150;
            holderClass += ' doorStyle';
        }
    }
    if(isWindowModule(row)){
        panels = '<div id="windowBuilder"></div>';
        holderClass += ' windows';
        width = 'auto';
        setTimeout(function() {
            renderWindowPassedType(row.windowsData.windowType, 'previewOnly')
            restoreOpeners(row.windowsData.openers)
        }, 100);
           
    }
    var html = '<div class="cartRowPanels">' +
        '<div class="'+holderClass+'" style="width: ' + width + 'px">' + panels + '</div>' +
        '</div>' +
        (isWindowModule(row) ? '' : '<div class="cartPanelsTitle">Viewed from Outside</div>');

    return html;
}


function buildIconsWithUniqueID(row) {
    var id = row.unique;
    var html = '<div class="cartIcons">' +
        '<img id="delete-' + id + '" class="cartIcons" class="delete" src="images/cart/icon-delete.svg" >' +
        '<img id="duplicate-' + id + '" class="cartIcons" class="duplicate" src="images/cart/icon-duplicate.svg" >' +
        '<img id="edit-' + id + '" class="cartIcons" class="edit" src="images/cart/icon-edit.svg" >' +
        '</div>';


    if (productionStarted() || isPurchasedSalesFlow2()) {
        html = '';
    }


    return html;

}

function buildColorName(color) {

    if (color.customName) {
        return color.customName + " (custom)";
    }

    if (!color.name1) {
        return "None";
    }

    var colorName2 = '';

    if (!empty(color.name2)) {
        colorName2 = "  (" + color.name2 + ")";
    }

    return color.name1 + colorName2;

}


//TODO -- Jacked needs to be in database now
function buildPanelWidth(width, panels, swings, module) {

    if (!isUK()) {
        width = width * 25.4;
    }

    var panelWidth;
    if (module == 1 || module == 5) {
        panelWidth = ((width - 135) / panels) - 6;
    }

    if (module == 2 || module == 6 || module == 10 || module == 11 || module == 12 || module == 13 || module == 14 || module == 15) {
        panelWidth = ((width - 83) / panels) - 7;
    }

    if (module == 7) {
        panelWidth = ((width - 123) / panels) - 7;
    }

    if (module == 9 || module == 3) {
        panelWidth = ((width - 124) - (6 * swings)) / panels;

    }

    if (!isUK()) {
        panelWidth = panelWidth / 25.4;
    }


    return formatMoney(panelWidth);
}


function netFrameDimensions(row) {

    var myOptions = {
        width: row.width,
        height: row.height,
        sill: buildSillForPriceEngine(row),
        addOns: buildAddOnsForPriceEngine(row)
    };

    myOptions = updateMyOptionWithModifiedHeightAndWidths(myOptions);
    var dimensions = myOptions.width + uom + ' width x ' + myOptions.height + uom + ' height';

    return dimensions;
}


function recommendedRoughOpeningDimensions(row) {

    var addSillHeightValue = addSillHeight(row.sillOptions);
    var addExtenderWidthValue = addExtenderWidth(row.addOnChoices);
    var addExtenderHeightValue = addExtenderHeight(row.addOnChoices);

    var height = parseFloat(row.height) + parseFloat(addSillHeightValue) + parseFloat(addExtenderHeightValue);
    var width = parseFloat(row.width) + parseFloat(addExtenderWidthValue);

    var roughOpening = (parseFloat(width) + 1) + uom + ' width x ' + (parseFloat(height) + 1) + uom + ' height';
    if (isUK()) {
        roughOpening = (parseFloat(width) + 25) + uom + ' width x ' + (parseFloat(height) + 25) + uom + ' height';
    }

    return roughOpening;
}


function userEnteredDimensions(row) {

    var dimensions = row.width + uom + ' width  x ' + row.height + uom + ' height';

    return dimensions;
}


function buildPrintQuantityRow(row) {

    if (!row.quantity) {
        row.quantity = 1;
    }

    var html = '' +
        '<div class="salesTaxNotePrint">Plus Applicable ' + salesTax + '</div>' +
        '<div class="cartQuantityWrapper">' +
        '   <div class="cartQuantityLabel">Qty: 1</div>' +
        '</div>';

    if (siteDefaults.preTax == 1 || siteDefaults.preTax == 2) {
        html = '' +
            '<div class="cartQuantityWrapper">' +
            '   <div class="cartQuantityLabel">Qty: 1</div>' +
            '</div>';
    }
    return html;
}


function buildQuantityRow(row) {

    if (!row.quantity) {
        row.quantity = 1;
    }
    var readOnly = '';

    var html = '' +
        '<div class="cartQuantityWrapper">' +
        '   <div class="cartQuantitySquare"></div>' +
        '   <input  id="' + row.unique + '" type="text" name="quantity" value="' + row.quantity + '" class="cartQuantity">' +
        '   <div class="cartQuantityLabel">Qty</div>' +

        '</div>';

    if (isLead() || productionStarted() || isPurchasedSalesFlow2() || isAudit()) {
        html = '' +
            '<div class="cartQuantityWrapper">' +
            '   <div class="cartQuantityLabel">Qty: ' + row.quantity + '</div>' +
            '</div>';
    }

    return html;
}


function isVinylDoor(row) {

    if (row.moduleTitle == 'Vinyl Door') {
        return true;
    }
    return false;
}


function convertForVinyl(row) {

    if (isVinylDoor(row)) {

        if (!row.vinylType) {
            row.vinylType = "vinyl-White Vinyl";
        }
        if (!row.paintedFinish) {
            return row;
        }
        var paintedFinishArray = row.paintedFinish.split('-');
        var whereToPaint = paintedFinishArray[2];
        if (whereToPaint == "interior") {
            row.extColor.name1 = '';
        }
        if (whereToPaint == "exterior") {
            row.intColor.name1 = '';
        }

    }

    return row;
}


function productionNoteHTML(row) {

    if (empty(row.productionNotes)) {
        row.productionNotes = '';
    }

    var readOnly = '';
    if (isLead()) {
        readOnly = 'readonly';
    }

    var html = '' +
        '<div class="noticeBlock"> ' +
        '    <div class="notesLabel">Production Notes:</div>' +
        '    <TEXTAREA ' + readOnly + ' id="' + row.unique + '-productionNotesPerDoor" class="productionNotes" NAME=notes ROWS=10 placeholder="">' + row.productionNotes + '</TEXTAREA>' +
        '</div>';

    if (siteDefaults.salesFlow == 2) {
        html = '';
    }

    if (isGuest()) {
        html = '';
    }
    return html;
}


function buildNoticeInfo(row) {

    if (row.note === undefined) {
        row.note = '';
    }

    var readOnly = '';
    if (isLead()) {
        readOnly = 'readonly';
    }
    var html = '' +
        '<div class="noticeBlock"> ' +
        '    <div class="notesLabel">Notes</div>' +
        '     <TEXTAREA ' + readOnly + ' id="' + row.unique + '-quoteNotes" class="quoteNotes" NAME=notes ROWS=10 placeholder="">' + row.note + '</TEXTAREA>' +
        '    <div class="quoteInstallNotes"> <b>*Basic Install:</b> <BR>' +
        '     Dilver, assemble and install the door(s) in a properly prepared opening.<BR>' +
        'Basic Install does not include the following: <BR>' +
        '1. Sill Pan - Sill pans are recommened for all jobs.<BR>' +
        '2. External/internal traim or stucco of any type.<BR>' +
        '3. Painting or staining of any wood on or around door(s). <BR>' +
        'Note: All wood cladding must be painted or stained within 3 days of delivery. Failure to preperly care for wood clad will void warranty for all wood componenets.<BR>' +
        '4. Flashing and sealants used for waterproofing.</div>' +
        '    <div class="quoteDealerNotes"><B>Dealer Optional Terms, etc here:</b> <BR>' +
        ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </div>' +
        '    <div class="quoteCustomerApproval"> Customer Approval: __________________</div>' +
        '    <div class="quoteCustomerApprovalNote">Initials</div>' +
        '</div>';

    html = '' +
        '<div class="noticeBlock"> ' +
        '    <div class="notesLabel">Notes</div>' +
        '    <TEXTAREA ' + readOnly + ' id="' + row.unique + '-quoteNotes" class="quoteNotes" NAME=notes ROWS=10 placeholder="">' + row.note + '</TEXTAREA>' +
        '</div>';

    var print = getParameter('print');
    if (print == 'yes') {
        html = '' +
            '<div class="noticeBlock"> ' +
            '    <div class="notesLabel">Notes</div>' +
            '    <div class="quoteNotesPrint">' + row.note.replace(/(\r\n|\n|\r)/g, "<br />") + '</div>' +
            '</div>';
    }

    if (siteDefaults.salesFlow == 2) {
        html = '';
    }

    return html;
}


function markupTotal(cost) {

    var markup = calculateMarkUp();

    var newTotal = Number(cost) + parseFloat(cost * markup / 100);

    return newTotal;
}


function buildItemSubTotal(row) {

    row.quantity = cleanQuantityNumber(row.quantity);
    var newTotal = markupTotal(Number(row.total));
    var subTotal = Number(row.quantity) * newTotal;

    var html = '<div id="' + row.unique + '-subTotal" data-door-cost="' + newTotal + '" class="cartItemSubTotal"> Item Sub-total: ' + currency + formatMoney(subTotal, 2, '.', ',') + '</div>' +
        '   <div class="salesTaxNote">Plus Applicable ' + salesTax + '</div>';


    if (siteDefaults.preTax == 1 || siteDefaults.preTax == 2) {
        html = '<div id="' + row.unique + '-subTotal" data-door-cost="' + newTotal + '" class="cartItemSubTotal"> Item Sub-total: ' + currency + formatMoney(subTotal, 2, '.', ',') + '</div>';
    }

    return html;
}


function buildGuestInfo() {

    var html = '' +
        '<div class="guestInfoBox">' +
        '   <div class="guestInfoTitle">Disclaimer:</div>' +
        '   <div class="guestInfoDescription">This quote is done per the specifications entered in by the customer. ' + siteDefaults.companyName + ' makes no guarantees as to the appropriateness of this product or size for the needed application. Nor do we suggest or guarantee that the product meets all local codes. It is the responsibility of the home/business owner to confirm the ordered product meets all codes. If you have any questions on which product is best for your application, please consult your contractor or qualified professional for advice prior to ordering.</div>' +
        '</div>';

    if (getParameter('order') == "yes" || tab == "viewOrder") {
        html = '' +
            '<div class="guestInfoBox">' +
            '   <div class="guestInfoTitle">Disclaimer:</div>' +
            '   <div class="guestInfoDescription">All doors & windows are custom built to the customers specifications. ' + siteDefaults.companyName + ' does not take responsibility of any doors or windows that are ordered incorrectly. If you have any questions regarding any detail on this page, please contact ' + siteDefaults.companyName + ' directly for clarification. No verbal changes will be recognized. If any changes are needed, the order must be corrected and and initialed approved version of this page must be received by ' + siteDefaults.companyName + '.<BR><BR>' +
            '   ' + siteDefaults.companyName + ' makes no guarantees as to the appropriateness of this product or size for the needed application.<BR> ' + cartDisclaimerInitials() + ' <BR><BR>' +
            '   <B>Rough Opening:</B><BR> Net Frame Dimensions are overall net width and height of door system. Recommended RO (Rough Opening) is one inch taller and one inch wider than Net Frame Dimensions.' +
            '   </div>' + cartDisclaimerInitials();

        //if (siteDefaults.salesFlow == 2) {
        //    html = '' +
        //        '' +
        //        '<div class="guestInfoBox">' +
        //        '   <div class="guestInfoTitle">Disclaimer:</div>' +
        //        '   <B>Rough Opening:</B><BR> Net Frame Dimensions are overall net width and height of door system. Recommended RO (Rough Opening) is one inch taller and one inch wider than Net Frame Dimensions.' +
        //        '</div>';
        //}
    }

    if (isUK()) {
        html = '' +
            '<div class="guestInfoBox">' +
            '   <div class="guestInfoTitle">Disclaimer:</div>' +
            '   <div class="guestInfoDescription"><b>All doors and windows are custom built to the customers specifications. ' + siteDefaults.companyName + ' can not take responsibility of any doors that are ordered incorrectly. If you have any questions regarding any detail on this page, please contact ' + siteDefaults.companyName + ' directly for clarification. No verbal changes will be recognized. If any changes are needed, the order must be corrected and and initialed approved version of this page must be received by ' + siteDefaults.companyName + '.<BR><BR>' +
            '   </b><BR> ' + cartDisclaimerInitials() + ' <BR>' +
            '   </div>' + cartDisclaimerInitials();

    }

    return html;
}


function buildGuestEdit(row) {

    var id = row.unique;
    var html = '<div class="guestEditBox">' +
        '<div  id="delete-' + id + '" class="guestDelete textButton" >Delete Door</div>' +
        '<div   id="edit-' + id + '" class="guestEdit textButton" > Edit Door</div>' +
        '</div>';

    return html;
}

function glassOpeningLimitText() {

    return '*Opening is limited to 1" Insulated Glass Only';
}


function buildDescriptionColumn(row) {

    var panelMovement = buildPanelMovement(row);
    var swing = empty(row.panelOptions.swingDirection.selected) ? 'none' : row.panelOptions.swingDirection.selected;
    var swingDirection = empty(row.panelOptions.swingInOrOut.selected) ? 'none' : row.panelOptions.swingInOrOut.selected;

    //if (row.swingDirectionSection === 'swing-Outswing') {
    //    swingDirection = "Out Swing";
    //} else if (row.swingDirectionSection === 'swing-Inswing') {
    //    swingDirection = "In Swing"
    //} else {
    //    swingDirection = 'None';
    //}

    var track = row.panelOptions.track.selected;
    var frame = row.panelOptions.frame.selected
    frame = 'PVC'; //Hack because  something is broken else where.

    var isWindow = isWindowModule(row);

    var finishes = displayFinishes(row);

    var glassType = glassTypeFromCartRow(row);
    var glassOptions = displayGlassOptions(row);

    var trackWarning = '';
    if (track == "Flush") {
        trackWarning = '<div class="cartDescriptionRowThird">* The flush track does not offer protection from water penetration</div>';
    }

    var hardwareName = splitForSecondOption(row.hardwareChoices);
    var hardwareOptions = displayHardwareExtras(row);

    var secondTitle = '';

    if (row.materialType == 'Vinyl') {
        if (empty(row.vinylColor)) {
            row.vinylColor = {};
            row.vinylColor.name1 = "White";
        }
    }

    if (row.vinylColor) {
        secondTitle = '<div class="cartDescriptionRowSecond">' + row.vinylColor.name1 + ' Vinyl</div>';
    }


    var numberOfSwings = 1;
    if (swing == 'both') {
        numberOfSwings = 2;
    }

    var panelWidth = '';
    if (empty(row.panelWidth)) {
        panelWidth = buildPanelWidth(row.width, row.numberOfPanels, numberOfSwings, row.module);
    } else {
        panelWidth = row.panelWidth;
    }


    var panelWidthRow = '<div class="cartDescriptionRowMain">Panel Width:</div>' +
        '<div class="cartDescriptionRowSecond">' + panelWidth + uom + '</div>' + cartDescriptionInitials();

    if(isWindow){
        panelWidthRow = '';
    }
    var clearOpeningAmount = 3.75;
    if (track == '5/8 Upstep' || track == '3/4 Upstep') {
        clearOpeningAmount = 4.5;
    }
    if (isUK()) {
        clearOpeningAmount = clearOpeningAmount + 25.4;
    }
    var clearOpenHeight = '';
    var swingDoorPosition = '';
    var swingDoorRow = '';
    var slideStackDirection = '';
    if(!isWindow){
        var clearOpenHeight = '<div class="cartDescriptionRowMain">Clear Opening Height:</div>' +
        '<div class="cartDescriptionRowSecond">' + (row.height - clearOpeningAmount) + uom + '</div>' + cartDescriptionInitials();

        var swingDoorPosition = '<div class="cartDescriptionRowMain">Swing Door(s) Position</div>' +
            '<div class="cartDescriptionRowSecond">' + capitaliseFirstLetter(swing) + '</div>' + cartDescriptionInitials();

        var swingDoorRow = '<div class="cartDescriptionRowMain">Swing Door Direction</div>' +
            '<div class="cartDescriptionRowSecond">' + swingDirection + '</div>' + cartDescriptionInitials();

        var slideStackDirection = '<div class="cartDescriptionRowMain">Panel Slide/Stack Direction</div>' +
            '<div class="cartDescriptionRowSecond">' + panelMovement + '</div>' + cartDescriptionInitials();
    }

    clearOpenHeight = ''; // Request Remove 5/18/2019

    var frameRow = '<div class="cartDescriptionRowMain">Frame Type:</div>' +
        '<div class="cartDescriptionRowSecond">' + frame + '</div>' + cartDescriptionInitials();

    var trackText = 'Track: Clear Anodized';
    var doorStyle = cartRowHasDoorStyle(row);
    if(doorStyle){
        trackText = 'Door Style:';
    }
    var trackRow = '<div class="cartDescriptionRowMain">' + trackText + '</div>' +
        '<div class="cartDescriptionRowSecond">' + track + '</div>' + cartDescriptionInitials() + trackWarning;

    var hardware = '<div class="cartDescriptionRowMain">Hardware:</div>' +
        '<div class="cartDescriptionRowSecond">' + hardwareName + '</div>' + cartDescriptionInitials();

    var screenRowUS = '';

    if (row.panelOptions.screens) {
        if (row.panelOptions.screens.selected) {
            screenRowUS = '<div class="cartDescriptionRowMain">Screen: </div>' +
                '<div class="cartDescriptionRowSecond">' + row.panelOptions.screens.selected + '</div>' + cartDescriptionInitials();
        }
    }


    if (isGuest()) {
        panelWidthRow = '';
        clearOpenHeight = '';
        swingDoorRow = '';
        frameRow = '';
        trackRow = '';
    }

    //if (row.numberOfPanels == 1) {
    //    swingDoorPosition = '';
    //    swingDoorRow = '';
    //    slideStackDirection = '';
    //    hardware = '';
    //    hardwareOptions = '';
    //}

    var sillHTML = sillCartHTML(row.sillOptions);
    var addOnHTML = addOnCartHTML(row.addOnChoices);
    var trickleVentHTML = trickleVentForCart(row);
    var screensHTML = screensForCart(row);

    var glassTypeNote = '';
    if (isMagnaline()) {
        glassTypeNote = '<div class="cartDescriptionRowThird">' + glassOpeningLimitText() + '</div>' + cartDescriptionInitials();
    }

    var doorType = 'Door';

    if (!empty(row.windowsOnly) || isWindow) {
        doorType = 'Window'
    }
    
    var panelsType = 'Panels';

    if(isWindow){
        panelsType = 'Squares';
    }
    
    var numberOfOpeners = '';
    if(isWindow){
        numberOfOpeners = '<div class="cartDescriptionRowMain">Number of Openers:</div>' +
        '<div class="cartDescriptionRowSecond">' + (row.panelOptions.swingDirection.selected -1 ) + '</div>' + cartDescriptionInitials();
    }
    
    var windowsDescription = '';
    if(isWindow){
        windowsDescription = getWindowsDescriptionHTML(row);
    }

    var userDimensions = '';
    if (isUK()) {
        userDimensions = '' +
            '<div class="cartDescriptionRowMain">User Entered Dimensions:</div>' +
            '<div class="cartDescriptionRowSecond">' + userEnteredDimensions(row) + '</div>' + cartDescriptionInitials();
    }

    var roughOpeningHTML = '';
    '<div class="cartDescriptionRowMain">Recommended Rough Opening</div>' +
    '<div class="cartDescriptionRowSecond">' + recommendedRoughOpeningDimensions(row) + '</div>' + cartDescriptionInitials() ;

    roughOpeningHTML = ''; //Removed 05/18/2019
    userDimensions = ''; //Removed 05/18/2019
    clearOpenHeight = ''; //Removed 05/18/2019
    trackRow = ''; //Removed 05/18/2019
    var html = '<div class="cartDescription">' +

        '<div class="cartDescriptionTitle">' + row.moduleTitle + '</div>' +
        ((doorStyle) ? trackRow : '') +
        '' + secondTitle + cartDescriptionInitials() +
        '<div class="cartDescriptionRowMain">Net Frame Dimensions of ' + doorType + '</div>' +
        '<div class="cartDescriptionRowSecond">' + netFrameDimensions(row) + ' </div>' + cartDescriptionInitials() +
        roughOpeningHTML +
        userDimensions +
        sillHTML +
        addOnHTML +
        '<div class="cartDescriptionRowMain">Number of ' + panelsType + ':</div>' +
        '<div class="cartDescriptionRowSecond">' + row.numberOfPanels + '</div>' + cartDescriptionInitials() +
        numberOfOpeners +
        windowsDescription +
        panelWidthRow +
        clearOpenHeight +
        swingDoorPosition +
        swingDoorRow +
        slideStackDirection +
        frameRow +
        ((!doorStyle && !isWindow) ? trackRow : '') +
        trickleVentHTML +
        screensHTML +
        finishes +
        glassType +
        glassTypeNote +
        glassOptions +
        hardware +
        hardwareOptions +
        screenRowUS +
        '</div>';

    return html;
}


function addCartRowHtmlWithRowNumber(row) {

    row = convertForVinyl(row);

    var rowIndex = row.totalItems;

    var bgColor = setBackgroundRowColor(row.index);

    var headerRow = buildRowHeader(row);
    var panelsRow = buildRowPanels(row);
    var titlesRow = buildRowTitles(row);

    var finishOptionsColumn = buildFinishOptionsColumn(row);
    var descriptionColumn = buildDescriptionColumn(row);

    var itemSubTotal = buildItemSubTotal(row);

    var cutSheetInfo = '';
    var guestInfo = buildGuestInfo();

    var myUser = $.cookie('user');

    var iconsRow = '';
    var guestEdit = '';
    var quantityRow = buildQuantityRow(row);

    var fullPageTop = '';
    var fullPageBottom = '';
    var print = getParameter('print');

    var noticeRow = buildNoticeInfo(row);
    var productionNotes = productionNoteHTML(row);


    if (isGuest()) {
        noticeRow = '';
    }

    if (isGuest() || print == 'yes' || isLead()) {
        if (print == 'yes') {
            productionNotes = '';
            quantityRow = buildPrintQuantityRow(row);
            fullPageBottom = '</div>';

            if (row.index == 1 && isGuest()) {
                fullPageTop = '<div class="fullPageLessGuest">';
            } else {
                fullPageTop = '<div class="fullPage">';
            }

            itemSubTotal = '';

        } else {
            guestEdit = buildGuestEdit(row);

        }
    } else {
        iconsRow = buildIconsWithUniqueID(row);

        if (user.extended == 'yes' && !isProduction()) {
            cutSheetInfo = buildCutSheetInfo(row);
        }
    }

    var html = '' +
        fullPageTop +
        '<div class="cartRow ' + (print ? 'print' : '') + ' keepTogether">' +
        headerRow +
        panelsRow +
        titlesRow +
        finishOptionsColumn +
        descriptionColumn +
        iconsRow +
        quantityRow +
        itemSubTotal +
        guestEdit +
        cutSheetInfo +
        noticeRow +
        productionNotes +
        guestInfo +
        '</div>' +
        fullPageBottom;

    return html;
}

function buildCutSheetInfo(row) {

    var jobNumber = getJobInfoFromRow('jobNumber', row);
    var productionDate = getJobInfoFromRow('productionDate', row);
    var dueDate = getJobInfoFromRow('dueDate', row);

    var html = '<div class="jobGroup"><input class="cutSheetBox" type="text" id="' + row.unique + '-jobNumber" value="' + jobNumber + '"/><div class="jobName">Job Number:</div></div>' +
        '<div class="jobGroup"><input class="cutSheetBoxDate" type="text" id="' + row.unique + '-productionDate" value="' + productionDate + '"/><div class="jobName">Production Date:</div></div>' +
        '<div class="jobGroup"><input class="cutSheetBoxDate" type="text" id="' + row.unique + '-dueDate" value="' + dueDate + '"/><div class="jobName">DueDate:</div></div>' +
        '<div class="viewCostSheet" onclick="showCostSheet(\'' + row.unique + '\')">View Cost Sheet</div>';

    html = '';

    return html;
}

function cartRowHasDoorStyle(row){
    return row && row.panelOptions && row.panelOptions.track &&
            row.panelOptions.track.buttons && hasDoorStyles(row.panelOptions.track.buttons)
}

function getWindowsDescriptionHTML(row){
    if(empty(row.windowsData))return '';
    var windowsDescription = [];
    var windowNum = 0;
    var wCounter = 0;
    var windowRowDescription = function(data, windowsData, rowWindowNum, type){
        var html = '';
        windowNum++;
        var hinge = windowsData.openers['window-'+data.number+'-'+rowWindowNum],
            hingeText = '';
        if(hinge && hinge !== 'none'){
            hingeText = ', Opener ' + hinge.replace('hinge','');
        }
        var width = 0,
            height = 0;
        if(type === 'rows'){
            if(data['width'+rowWindowNum]){
                width = data['width'+rowWindowNum];
                wCounter += width;
            }else{
                width = windowsData.width - wCounter;
            }
            height = data.height;
        }else{
            if(data['height'+rowWindowNum]){
                height = data['height'+rowWindowNum];
                wCounter += height;
            }else{
                height = windowsData.height - wCounter;
            }
            width = data.width;
        }
        html += '<div class="cartDescriptionRowMain">Square '+windowNum+':</div>' +
                '<div class="cartDescriptionRowSecond">' + width + 'mm x ' + height + 'mm ' + hingeText + '</div>' + cartDescriptionInitials();
        return html;
    }
    var data = row.windowsData.rows.length ? row.windowsData.rows : row.windowsData.columns;
    var type = row.windowsData.rows.length ? 'rows' : 'columns';
    for(var i=0; i<data.length; i++){
        var rowData = data[i];
        wCounter = 0;
        for(var r=1;r<=rowData.numberOfWindows;r++){
            windowsDescription.push(windowRowDescription(rowData, row.windowsData, r, type));
        }
    }
    return windowsDescription.join('');
}

//function showCutSheet(property) {
//
//    var myOptions = buildPriceEngineOptionsWithOptions(quote.Cart[property]);
//    myOptions.jobInfo = buildJobInfoOld(property);
//
//    quote.Cart[property].jobInfo = myOptions.jobInfo;
//    writeQuote(quote);
//
//    var json = JSON.stringify(myOptions);
//    var url = "https://" + apiHostV1 + "/api/buildCut?" + addSite();
//
//    $.post(url, json, function (data) {
//
//        var cut = jQuery.parseJSON(data);
//
//        window.open("https://" + apiHostV1 + "/sheet/cut/index.html?id=" + cut.cutSheet);
//    });
//}
