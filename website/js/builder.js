
function loadBuilder() {

    $('#footerPrice').hide();
    resetUIForTopNav();
    var subTabs = [];

    var url = "https://" + apiHostV1 + "/api/builder/"+tab +"?" + authorizePlusType('builder') + addSite();
    $.getJSON(url, function (tabs) {

        for (var a = 0; a < tabs.length; a++) {
            var value = tabs[a];
            subTabs.push(value.name);
        }

        var secondNav = buildSecondNav(subTabs);
        $('#mainContent').html(secondNav);

        loadBuilderMainData();

    });

    updateFormStatus();
    showContentAndSaveState();
}
function buildRow2(index,column) {

    var borderClass = 'borderLeftRightTop';
    if (index == '0') {
        borderClass = 'borderLeftTop';
    }

    var row =  '<div class="' + column.class + ' ' + borderClass + '">' + column.name + '</div>' ;

    return row;
}


function buildHeaderWithColumns(columns) {

    var html = '<div class="moduleTableHeader">' ;

    for (var index = 0; index < columns.length; index++) {
        html += buildRow(index,columns[index]);
    }

    html += '' +
        '   <div class="modulesFinalRow"></div>' +
        '</div>';

    return html;

}



function loadAllUsers() {

    var url = "https://" + apiHostV1 + "/api/users?" + authorizePlusType('builder') + addSite();
    $.getJSON(url, function (apiJSON) {

        var modules = apiJSON;

        var columns = [{name: 'id', class: 'moduleTitleWidth' }, {name: 'name', class: 'moduleTitleWidth'}];

        var html  = buildHeaderWithColumns(columns);

        $('#mainContent').append( html);
        $('.moduleTableRow').unbind("click").bind("click", loadModule);

    });



}
function loadAllModules() {

    var url = "https://" + apiHostV1 + "/api/modules?" + addSite();
    $.getJSON(url, function (apiJSON) {

        var modules = apiJSON;

        var rows = buildModuleRowTable(modules);
        var html = buildModuleHeaderTable(rows);
        //var secondNav = buildSecondNav(subTabs);


        $('#mainContent').append( html);
        $('.moduleTableRow').unbind("click").bind("click", loadModule);

    });

}
function loadBuilderMainData() {

    if (tab === 'users') {
        loadAllUsers();
    } else {
        loadAllModules();
    }


}

function buildSecondNav(tabs) {

    var html = '' +
        '<div id="builderSecondNav">' ;

        //'   <div class="builderSecondNavBlocksFirst"></div>';

    var tabSelected = '';

    for (var a = 0; a < tabs.length; a++) {
        var selected = '';
        var value = tabs[a];
        if (value == tabSelected) {
            selected = 'tabSelected';
        }

        html += '<div id="view'+value+'" class="builderSecondNavBlocks  '+selected+'"> ' + value + '</div>' ;
        var remainder = (a + 1) % 7;
        if (remainder == 0 && a != 0 ) {
            html +='<div class="builderSecondNavBlocksLast"></div>';
        }

    }
    html +='<div class="builderSecondNavBlocksLast"></div>';

    html += '</div>';


    return html;


}