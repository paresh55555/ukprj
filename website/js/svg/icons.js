'use strict'

function cartSVG() {

    var svg = '' +
        '<?xml version="1.0" encoding="utf-8"?>' +
        '<!-- Generator: Adobe Illustrator 15.0.2, SVG Export Plug-In  -->' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" [<!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/">' +
        '<svg version="1.1"' +
        'xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"' +
        'x="0px" y="0px" width="26px" height="24px" viewBox="-0.342 -0.001 26 24"' +
        'overflow="visible" enable-background="new -0.342 -0.001 26 24" xml:space="preserve">' +
        '    <defs>' +
        '    </defs>' +
        '    <path fill="' + siteDefaults.cssBorder + '" d="M25.316,6.463h-19.4L4.352,0H0v0.924h3.625l4.502,18.562c-0.961,0.279-1.666,1.158-1.666,2.207' +
        'C6.461,22.967,7.494,24,8.77,24c1.271,0,2.307-1.033,2.307-2.307c0-0.523-0.18-1-0.473-1.385h6.484' +
        'c-0.297,0.385-0.473,0.861-0.473,1.385c0,1.273,1.033,2.307,2.309,2.307c1.273,0,2.307-1.033,2.307-2.307' +
        'c0-1.275-1.033-2.309-2.307-2.309H9.051l-0.447-1.846h12.959L25.316,6.463z M10.154,21.693c0,0.764-0.621,1.385-1.385,1.385' +
        's-1.385-0.621-1.385-1.385s0.621-1.385,1.385-1.385S10.154,20.93,10.154,21.693 M20.307,21.693c0,0.764-0.619,1.385-1.383,1.385' +
        's-1.385-0.621-1.385-1.385s0.621-1.385,1.385-1.385S20.307,20.93,20.307,21.693 M8.377,16.615l-2.234-9.23h17.887l-3.129,9.23H8.377' +
        'z"/>' +
        '</svg>';

    return svg;
}


function backSVG() {

    var svg = '' +
        '<?xml version="1.0" encoding="utf-8"?>' +
        '<!-- Generator: Adobe Illustrator 15.0.2, SVG Export Plug-In  -->' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" [' +
        '<!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/">' +
        '<svg version="1.1"' +
        'xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"' +
        'x="0px" y="0px" width="12px" height="22px" viewBox="-0.168 -0.393 12 22"' +
        'overflow="visible" enable-background="new -0.168 -0.393 12 22" xml:space="preserve">' +
        '<defs>' +
        '</defs>' +
        '<polygon fill="' + siteDefaults.cssBorder + '" points="11.668,20.152 2.121,10.607 11.666,1.061 10.605,0 10.605,0 0,10.607 10.605,21.213 "/>' +
        '</svg>';

    return svg;
}


function transactionsSVG() {

    var svg = '' +
        '<?xml version="1.0" encoding="utf-8"?> ' +
        '<!-- Generator: Adobe Illustrator 15.0.2, SVG Export Plug-In  --> ' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" [ ' +
        '<!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/"> ' +
        '<svg version="1.1"' +
        'xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"' +
        'x="0px" y="0px" width="28px" height="28px" viewBox="0 0 28 28" overflow="visible" enable-background="new 0 0 28 28"' +
        'xml:space="preserve"> ' +
        '<defs> ' +
        '</defs> ' +
        '<g> <g> ' +
        '<path fill="' + siteDefaults.cssBorder + '" d="M14,0C6.269,0,0,6.268,0,14s6.269,14,14,14c7.73,0,14-6.268,14-14S21.73,0,14,0 M14,26.881' +
        'c-7.103,0-12.88-5.78-12.88-12.881S6.897,1.12,14,1.12c7.102,0,12.881,5.779,12.881,12.88S21.102,26.881,14,26.881"/> ' +
        '<path fill="' + siteDefaults.cssBorder + '" d="M21.003,9.002l-8.989,8.717l-3.149-3.149c-0.328-0.328-0.86-0.328-1.188,0s-0.328,0.861,0,1.189' +
        'l3.735,3.732c0.164,0.164,0.379,0.246,0.593,0.246c0.211,0,0.424-0.077,0.586-0.236l9.583-9.293' +
        'c0.334-0.323,0.341-0.854,0.018-1.188C21.867,8.687,21.337,8.68,21.003,9.002z"/> </g> </g> ' +
        '</svg>';

    return svg;
}


function customersSVG() {

    var svg = '' +
        '<?xml version="1.0" encoding="utf-8"?> ' +
        '<!-- Generator: Adobe Illustrator 15.0.2, SVG Export Plug-In  --> ' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" [ ' +
        '<!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/"> <svg version="1.1"' +
        'xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"' +
        'x="0px" y="0px" width="28px" height="28px" viewBox="0 0 28 28" overflow="visible" enable-background="new 0 0 28 28"xml:space="preserve"> ' +
        '<defs> </defs> <path fill="' + siteDefaults.cssBorder + '" d="M13.999,0C6.268,0,0,6.268,0,14c0,7.731,6.268,14,13.999,14C21.732,28,28,21.731,28,14' +
        'C28,6.268,21.732,0,13.999,0 M13.999,1.167c7.077,0,12.834,5.757,12.834,12.833c0,3.113-1.115,5.971-2.967,8.194' +
        'c-1.279-0.534-4.299-1.582-6.169-2.133c-0.16-0.051-0.185-0.059-0.185-0.723c0-0.548,0.227-1.1,0.445-1.567' +
        'c0.238-0.507,0.521-1.36,0.622-2.126c0.283-0.329,0.67-0.979,0.918-2.216c0.218-1.092,0.116-1.489-0.028-1.861' +
        'c-0.015-0.039-0.03-0.078-0.042-0.117c-0.055-0.255,0.021-1.585,0.208-2.614c0.128-0.709-0.034-2.214-1.008-3.458' +
        'c-0.615-0.787-1.792-1.752-3.942-1.887l-1.179,0.001C11.395,3.627,10.216,4.592,9.6,5.379C8.626,6.623,8.464,8.128,8.593,8.836' +
        'c0.188,1.03,0.262,2.36,0.208,2.61c-0.011,0.044-0.027,0.083-0.043,0.122c-0.144,0.372-0.246,0.77-0.027,1.861' +
        'c0.247,1.237,0.634,1.887,0.918,2.216c0.101,0.766,0.384,1.619,0.623,2.126c0.175,0.372,0.256,0.877,0.256,1.592' +
        'c0,0.663-0.025,0.671-0.174,0.719c-1.934,0.57-5.011,1.682-6.159,2.184C2.307,20.032,1.167,17.148,1.167,14' +
        'C1.167,6.924,6.924,1.167,13.999,1.167 M5.04,23.176c1.313-0.536,3.934-1.471,5.653-1.979c1.001-0.314,1.001-1.156,1.001-1.833' +
        'c0-0.562-0.039-1.388-0.367-2.088c-0.225-0.479-0.482-1.301-0.539-1.944c-0.014-0.149-0.084-0.288-0.198-0.388' +
        'c-0.165-0.146-0.501-0.675-0.715-1.744c-0.17-0.846-0.099-1.03-0.029-1.209c0.03-0.077,0.059-0.152,0.081-0.236' +
        'c0.141-0.512-0.016-2.194-0.186-3.128c-0.073-0.405,0.02-1.558,0.779-2.528c0.68-0.87,1.709-1.354,3.023-1.438l1.106-0.002' +
        'c1.348,0.086,2.378,0.57,3.06,1.44c0.759,0.971,0.851,2.123,0.776,2.529c-0.168,0.933-0.325,2.615-0.186,3.126' +
        'c0.024,0.085,0.052,0.16,0.081,0.237c0.069,0.179,0.141,0.363-0.027,1.209c-0.215,1.069-0.552,1.599-0.717,1.744' +
        'c-0.112,0.1-0.184,0.238-0.197,0.388c-0.056,0.644-0.313,1.465-0.538,1.944c-0.258,0.55-0.557,1.282-0.557,2.063' +
        'c0,0.677,0,1.52,1.011,1.837c1.646,0.486,4.278,1.391,5.668,1.938c-2.319,2.297-5.51,3.72-9.025,3.72C10.518,26.833,7.355,25.436,5.04,23.176"/> ' +
        '</svg>';

    return svg;
}


function nextSVG() {

    var svg = '' +
        '<?xml version="1.0" encoding="utf-8"?> ' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd" [ <!ENTITY ns_flows "http://ns.adobe.com/Flows/1.0/"> ' +
        '<svg version="1.1"xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"' +
        'x="0px" y="0px" width="39px" height="39px" viewBox="0 0 39 39" overflow="visible" enable-background="new 0 0 39 39"' +
        'xml:space="preserve"> <defs> </defs> ' +
        '<polygon fill="'+ siteDefaults.cssBorder+'" points="15.393,9.955 24.939,19.5 15.395,29.047 16.455,30.107 16.455,30.107 27.061,19.5 16.455,8.895 "/> ' +
        '<circle fill="none" stroke="'+ siteDefaults.cssBorder+'" stroke-miterlimit="10" cx="19.5" cy="19.5" r="19"/> ' +
        '</svg>';

    return svg;
}

