'use strict';


function clearQuote() {
    writeQuote({});
}


function selectTab(id) {

    $('.topNavSection').removeClass('topNavSelected');
    var productSelector = $('.productNavSection');
    var idSelector = $(id);

    resetAllNavItemsSelector(productSelector);
    selectNavItemsSelector(idSelector);
}


function selectNavItemsSelector(selector) {

    selector.css('background-color', siteDefaults.cssReverseBackground);
    selector.css('color', siteDefaults.cssReverseFont);
}

function resetAllNavItemsSelector(selector) {

    selector.css('background-color', siteDefaults.cssBackground);
    selector.css('color', siteDefaults.cssFont);
}


function checkChoosePanels() {

    if (formStatus.choosePanels) {

        var idSelector = "#" + formStatus.choosePanels;
        var numbers = formStatus.choosePanels.split("-");
        var numberOfPanels = numbers[1];

        var choosePanelSelector = $("#" + formStatus.choosePanels);
        buildSwing(numberOfPanels);
        displaySelected(choosePanelSelector);
    }
}


function checkChooseSwing() {

    if (formStatus.chooseSwings) {

        var idSelector = "#" + formStatus.chooseSwings;
        buildSwingDirection(formStatus.chooseSwings, formStatus.numberOfPanels);
        displaySelected(idSelector);
    }
}


function checkPanelMovement() {

    if (formStatus.panelMovement) {
        var idSelector = "[id='" + formStatus.panelMovement + "']";

        buildSwingInsideOutside();
        $("#swingDirection").show();
        checkSwingDirection();
        displaySelected(idSelector);
    }
}


function checkSwingDirection() {

    if (formStatus.swingDirection) {
        var idSelector = "#" + formStatus.swingDirection;
        $("#chooseFrame").show();

        displaySelected(idSelector);
    }
}


function checkChooseTrack() {

    if (formStatus.chooseTrackSection) {
        var idSelector = formStatus.chooseTrackSection;
        $("#nextColorFinish").show();

        var selector = buildIDSelector(idSelector);
        selector = $('.extendedHalfButtonImage', selector);

        displayExtendedSelected(selector);
        checkChoosePanels();
    }
}


function checkChooseFrame() {

    if (formStatus.chooseFrameSection) {
        var idSelector = formStatus.chooseFrameSection;

        buildTrack();
        checkChooseTrack();
        $("#chooseTrack").show();

        var selector = buildIDSelector(idSelector);
        selector = $('.extendedHalfButtonImage', selector);

        displayExtendedSelected(selector);
    }
}


function loadChoicesAlreadyMade() {
    if (formStatus.width > 0) {
        $("#width").val(formStatus.width);
        validateWidth();
    }
    if (formStatus.height > 0) {
        $("#height").val(formStatus.height);
        validateHeight();
    }

    if (isHeightValid(true) && isWidthValid(true)) {
        getPanels();
    }


    if (isHeightValid(true) && isWidthValid(true)) {
        if (formStatus.swing) {

            var location = '#swing-' + formStatus.swing;
            jQuery('<div/>', {"class": 'selectedDiv'}).appendTo($(location));


            jQuery('<img/>', {
                "src": 'images/check.png',
                "class": 'checkButton'
            }).appendTo($(".selectedDiv", location));
        }
    }
}


function saveHeightToCookie() {
    formStatus.height = $("#height").val();
    formStatus.height = numbersOnly(formStatus.height);
    $("#height").val(formStatus.height);

    if ($('#screensUS').is(":visible")) {
        $('#screensUS').replaceWith(screensHTML());
        $('.extendedHalfButton').unbind("click").bind("click", halfButtonClick);
    }

    writeFormStatus();
    delay(function () {
        validateHeight(true);

    }, 200);
}


var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


function buildFormStatus() {

    if (formStatus.swingDirection) {
        $("#chooseFrame").show();
    }

    if (formStatus.choosePanels) {
        var numbers = formStatus.choosePanels.split("-");
        var numberOfPanels = numbers[1];
        buildSwing(numberOfPanels);
    }

    if (formStatus.chooseSwings) {

        var idSelector = "#" + formStatus.chooseSwings;
        buildSwingDirection(formStatus.chooseSwings, formStatus.numberOfPanels);
        displaySelected(idSelector);
    }

    if (formStatus.panelMovement) {

        var idSelector = "[id='" + formStatus.panelMovement + "']";

        buildSwingInsideOutside();
        $("#swingDirection").show();
        checkSwingDirection();
        displaySelected(idSelector);
    }

    if (formStatus.swingDirection) {
        var idSelector = "#" + formStatus.swingDirection;
        $("#chooseFrame").show();
        checkChooseFrame();
        displaySelected(idSelector);
    }

    if (formStatus.chooseTrackSection) {
        var idSelector = formStatus.chooseTrackSection;

        $("#nextColorFinish").show();
        displayExtendedSelected(buildIDSelector(idSelector));
    }

    if (formStatus.chooseFrameSection) {
        var idSelector = formStatus.chooseFrameSection;
        //buildTrack();
        checkChooseTrack();
        $("#chooseTrack").show();
        displayExtendedSelected(buildIDSelector(idSelector));
    }
}


