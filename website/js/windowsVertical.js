function buildVerticalWindows(rowsPassed) {
    
    $('#windowBuilder').html('');
    
    buildHolders();
    buildVertical(rows);

    addDimensionsToWindows(rows);

    $('.dimension').keyup(updateDimensions);
    $('.dimensionVertical').unbind("keyup").bind("keyup", updateVerticalDimensions);

    $('#heightWindow').keyup(updateVerticalHeight);
    $('#widthWindow').keyup(updateVerticalWidth);

    $('.sash').unbind("click").bind("click", selectSash);
    deActivateOpeners();
    writeWindowsForm();
    // updateWindowPrice();
}


function buildTotalDimensionsVertical() {

    var margins = 5;
    var textFieldHeight = 26 + margins;
    var dimensionHeight = (frame.height  - textFieldHeight) / 2;

    if (chooseDimensionLeftRight == 'left') {
        buildTotalVerticalDimensionRightWithHeight(dimensionHeight);
    } else {
        buildTotalVerticalDimensionLeftWithHeight(dimensionHeight);
    }
}


function buildTotalVerticalDimensionLeftWithHeight(height) {

    $('#verticalSizes').html('');
    jQuery('<div>', {
        class: 'dimensionVerticalBox'
    }).append($('<div>', {
        class: 'rightDimensionTop dimensionBumpUpOne dimensionBorder',
        height: height
    })).append($('<input/>', {
        id: "heightWindow",
        type: 'text',
        class: 'dimensionVertical unSelectable unSelectableBumpLeft',
        readOnly: true,
        value: parseInt(frame.height * yMult)
    })).append($('<div>', {
        class: 'rightDimensionBottom dimensionBorder',
        height: height
    })).appendTo('#verticalSizes');
}

function buildTotalVerticalDimensionRightWithHeight(height) {


    $('#totalVertical').html('');
    jQuery('<div>', {
        class: 'dimensionVerticalBox'
    }).append($('<div>', {
        class: 'leftDimensionTop dimensionBorder',
        height: height
    })).append($('<input/>', {
        id: "heightWindow",
        type: 'text',
        class: 'dimensionVertical unSelectable unSelectableBumpLeft',
        readOnly: true,
        value: parseInt(frame.height * yMult)
    })).append($('<div>', {
        class: 'leftDimensionBottom dimensionBorder',
        height: height
    })).appendTo('#totalVertical');
}


function clearOutCorrectionDimensionForVertical() {

    if (chooseDimensionLeftRight == 'left') {
        $('#verticalSizes').html('');
        $('#totalVertical').removeClass('newTotalVertical');
    } else {
        var totalVertical = $('#totalVertical');
        totalVertical.addClass('newTotalVertical');
        totalVertical.html('');
    }
}


function buildDimensionsVertical(rows) {

    clearOutCorrectionDimensionForVertical();
    buildDimensionsForVertical(rows);
}


function buildDimensionsForVertical(rows) {

    var numberOfRows = rows.length;

    var unSelectable = '';
    var readOnly = false;
    if (numberOfRows == 1) {
        unSelectable = 'unSelectable';
        readOnly = true;
        $('#verticalSizes').css('visibility', 'hidden');
    }

    for (var a = 0; a < numberOfRows; a++) {

        var firstRow = 1;

        if (a == 0) {
            firstRow = 0;
        }

        var height = rows[a].height / yMult;

        var divHeight = ((height - textBoxHeight ) / 2);
        var divHeight1 = divHeight + firstRow;
        var columnNumber = 'na';
        if (!empty(rows[a].column)) {
            columnNumber = rows[a].column.number;
        }

        if (chooseDimensionLeftRight == 'left') {

            $('<div>', {
                class: 'dimensionVerticalBox'
            }).append($('<div>', {
                class: 'topLeft',
                height: divHeight1
            })).append($('<div>', {
                class: 'topRight hasBorderLeftTop dimensionBorder',
                height: divHeight1
            })).append($('<input/>', {
                id: "row-" + a + "-height-" + columnNumber,
                type: 'text',
                class: 'dimensionVertical dimensionVerticalLeft '+unSelectable ,
                readOnly: readOnly,
                value: parseInt(rows[a].height)
            })).append($('<div>', {
                class: 'bottomLeft',
                height: divHeight
            })).append($('<div>', {
                class: 'bottomRight hasBorderLeftBottom dimensionBorder',
                height: divHeight
            })).appendTo('#verticalSizes');

        } else {

            $('<div>', {
                class: 'dimensionVerticalBox'
            }).append($('<div>', {
                class: 'topLeft hasBorderRightTop dimensionBorder',
                height: divHeight1
            })).append($('<div>', {
                class: 'topRight',
                height: divHeight1
            })).append($('<input/>', {
                id: "row-" + a + "-height-" + columnNumber,
                type: 'text',
                class: 'dimensionVertical dimensionVerticalLeftPlus '+unSelectable ,
                readOnly: readOnly,
                value: parseInt(rows[a].height)
            })).append($('<div>', {
                class: 'bottomLeft hasBorderRightBottom dimensionBorder',
                height: divHeight
            })).append($('<div>', {
                class: 'bottomRightRight',
                height: divHeight
            })).appendTo('#totalVertical');
        }
    }
}

function updateVerticalWidth() {

    updateWidthDelay('vertical');
}


function updateWidthDelay(type) {

    var width = $('#widthWindow').val();
    if (width < 300) {
        $('#widthWindow').addClass('backgroundPink');
        return;
    }
    delay(function () {
        updateFrameDimensions(type);
    }, delayAmount);
}

function updateVerticalHeight() {
    updateHeightDelay('Vertical');
}


function updateHeightDelay(type) {

    var height = $('#heightWindow').val();
    if (height < 150) {
        return;
    }
    delay(function () {
        updateFrameDimensions(type);
    }, delayAmount);
}


function chooseWidthMultiplier(width, height) {

    xMult = width / maxUIWidth;
    yMult = 1 * xMult;
    frame.height = height / yMult;
    frame.width = width / xMult;

    tempLog(width, height)
}


function tempLog(width, height) {

    return;
    f1log(width, "Entered Width");
    f1log(height, "Entered Height");
    f1log(frame.width, "Frame Width");
    f1log(frame.height, "Frame Height");
    f1log(xMult, "xMult");
    f1log(yMult, "yMult");
    f1log(maxUIWidth, "max Width");
    f1log(maxUIHeight, "max Height");
}


function chooseHeightMultiplier(width, height) {

    yMult = height / maxUIHeight;
    xMult = 1 * yMult;
    frame.width = width / xMult;
    frame.height = height / yMult;

    tempLog(width, height);
}


function updateFrameDimensions(type) {

    setFrame();

    if (type == 'horizontal') {
        buildHorizontalWindows(columns);
    } else {
        buildVerticalWindows();
    }
}


function buildVerticalData(res) {

    var numberOfRow = res.length;
    var height = Math.round(frame.height * yMult  / numberOfRow);

    var rows = [];
    for (var a = 0; a < numberOfRow; a++) {

        var row = {"numberOfWindows": res[a], "height": height, "number": a};
        row = addWidthsToRow(row);
        rows.push(row);
    }

    return (rows);
}


function buildVertical(rows) {

    var numberOfRows = parseInt(rows.length);

    $('#mainFrame').html('');

    var windows = [];
    for (var a = 0; a < numberOfRows - 1; a++) {
        var row = rows[a];
        row.number = a + 1;
        windows.push({"height": row.height});
        buildRow(row, numberOfRows);
    }

    var height = remainingHeight(windows);


    var lastRow = rows[numberOfRows - 1];
    lastRow.number = numberOfRows;
    lastRow.height = height;

    buildRow(lastRow, numberOfRows);
}


function remainingWidthVertical(windows) {

    var widthOfWindows = getWidthOfWindows(windows);
    var remainingSpace = (frame.width * xMult) - (widthOfWindows);

    return remainingSpace;
}

function buildRow(row, numberOfRows) {

    var mySize = {
        "height": row.height,
        "place": '#mainFrame',
        "number": row.number,
        "numberOfWindows": row.numberOfWindows
    }

    var windows = [];
    for (var a = 1; a < row.numberOfWindows; a++) {

        var newWidth = row['width' + a]
        mySize.width = newWidth;
        mySize.column = a;
        windows.push({"width": mySize.width});
        addWindowVertical(mySize, numberOfRows);

    }

    mySize.column = row.numberOfWindows;
    mySize.width = remainingWidthVertical(windows);
    addWindowVertical(mySize, numberOfRows);

}


function addWindowVertical(size, numberOfRows) {

    var rowsMultiplier = parseFloat(numberOfRows);
    var frameHeightToRemove = ( (frame.marginHeight * rowsMultiplier) + (2 * border * rowsMultiplier) ) / numberOfRows;
    if (size.number == 1 || size.number == numberOfRows) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }

    var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

    var windowMultiplier = parseFloat(size.numberOfWindows);

    var frameWidthToRemove = ( (frame.marginWidth * windowMultiplier) + (2 * border * windowMultiplier) ) / size.numberOfWindows;

    if (size.numberOfWindows == 1) {
        frameWidthToRemove = frameWidthToRemove + 12;
    } else if (size.column == 1 || size.column == size.numberOfWindows) {
        frameWidthToRemove = frameWidthToRemove + 6;
    }

    var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);

    jQuery('<div/>', {
        class: 'frame',
        width: ( size.width / xMult) - frameWidthToRemove,
        height: (size.height / yMult) - frameHeightToRemove
    }).append($('<div>', {
        id: 'window-' + size.number + "-" + size.column,
        class: 'sash',
        width: (size.width / xMult) - sashWidthToRemove,
        height: (size.height / yMult) - sashHeightToRemove
    })).appendTo(size.place);

}
