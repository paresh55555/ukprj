'use strict'


function doorHeightHtml() {

    var html = '' +
        '<div id="chooseHeight">' +
        '   <div class="title">Choose Door Height:</div>' +
        '   <div id="heightSection" class="sectionBlock">' +
        '   </div>' +
        '</div>';


    return html
}


function addFixedPanelHeights() {

    var myPromise = getDoorFixedHeights(1);

    var id = '#heightSection';
    myPromise.done(function(fixedHeights) {

        buildHeightPanelsAndAttachId(fixedHeights, id);
        $('.extendedHalfButton').unbind("click").bind("click", doorHeightSelected);
        restoreDoorHeightSelection();
    })
}


function doorHeightSelected() {

    extendedHalfSelectedWithThis(this);
    $('#chooseSwings').show();
}



function restoreDoorHeightSelection() {

    if (formStatus.heightSection) {
        placeSelectedImageExtendedHalf(formStatus.heightSection);
        //displayExtendedSelected(id);
        $('#chooseSwings').show();
    }
}




function buildHeightPanelsAndAttachId(arrayOfPanels, id) {

    $(id).html('');
    var numberOfPanels = arrayOfPanels.length;

    var option = 'panelHeight';
    var disabled = false;
    var buttons='';

    for (var a = 0; a < numberOfPanels; a++) {

        var button = arrayOfPanels[a];
        button.extra = button.height;
        buttons = buttons +  buildExtendedHalfButton(button, option, disabled);
    }

    $(id).html(buttons);
}