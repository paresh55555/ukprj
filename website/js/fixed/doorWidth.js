'use strict'


function doorWidthHtml() {

    var html = '' +
        '<div id="choosePanels" style="display: block;">' +
        '   <div class="title">Choose Door Width:</div>' +
        '   <div id="panelSection" class="sectionBlock">' +
        '   </div>' +
        '</div>';

    return html;
}


function restoreDoorWidthSelection() {

    if (formStatus.choosePanels) {
        var id = '#'+formStatus.choosePanels;
        displaySelected(id);
        $('#chooseHeight').show();
        restoreSwingSelection();
    }
}




function selectFixedWidth() {

    processFixedWidth(this);
}


function processFixedWidth(selector) {

    var groupOfButtons = $(selector).parent();
    var nextSection = $(selector).parent().parent().next();
    var thisSection = $(selector).parent().parent();

    var numberOfPanels = $(selector).data('button');
    var id = $(selector).attr('id');

    var width = splitForSecondOption(id);

    formStatus.width = width;
    formStatus.numberOfPanels = numberOfPanels;
    var section = $(thisSection).attr('id');

    buildSwing(numberOfPanels);

    $('#chooseHeight').show();
    formStatus[section] = id;

    writeFormStatus();
    displaySelected(selector);

    //scrollToNextPositon(nextSection);
    //updateFormPrice();
}


function addFixedPanels() {

    var myPromise = getDoorFixedWidths(1);

    var id = '#panelSection';
    myPromise.done(function (fixedWidths) {
        buildFixedPanelsAndAttachId(fixedWidths, id);

        $('.button').unbind("click").bind("click", selectFixedWidth);
        restoreDoorWidthSelection();
    })
}


function buildFixedPanelsAndAttachId(arrayOfPanels, id) {

    $(id).html('');
    var numberOfPanels = arrayOfPanels.length;
    var doorMult = (arrayOfPanels[0].panels * 50) / arrayOfPanels[0].width;

    for (var a = 0; a < numberOfPanels; a++) {

        var door = arrayOfPanels[a];
        var doorWidth = doorMult * door.width;
        var doorImages = buildDoorPanels(door.panels, doorWidth);
        var buttonID = 'fixedPanel-' + door.width + '" data-button="' + door.panels;
        var label = parseInt(door.width / 12) + ' feet';

        attachButton(label, doorImages, id, buttonID);
    }
}