'use strict'


function swingDirectionHTML() {

    var html = '' +
        '<div id="chooseSwings" >' +
        '   <div class="title">Swing Direction:</div>' +
        '   <div id="swingSection" class="sectionBlock">' +
        '   </div>' +
        '</div>';



    return html;
}


function restoreSwingSelection() {

    buildSwing(formStatus.numberOfPanels);
    placeSelectedImage(formStatus.chooseSwings);

}