'use strict'


function loadFixedPanels() {


    var html = doorWidthHtml();
    html += doorHeightHtml();
    html += swingDirectionHTML()
    html += fixedPanelsNextSectionHTML();

    buildAndAttachCorePage(html);
    attachStartOverButtonWithId('startOverButton');

    addFixedPanels();
    addFixedPanelHeights();

    selectTab('#fixed');

    //$('#mainContent').css({"margin-top": "144px"});

    makeNextSectionButton();
    showContentAndSaveState();
}


function fixedPanelsNextSectionHTML() {

    var html = '' +
        '<div id="nextSection" >' +
        '</div>';

    return html;
}


function mainContentPageSafeId() {

    var htmlId = 'mainContent';
    var id = buildIDSelector(htmlId);

    return id;
}