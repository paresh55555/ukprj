'use strict';


function isOrder() {

    if (quote.type == 'order' ) {
        return true;
    }

    return false;
}

function isPrint() {

    var print = getParameter('print');

    if (print == 'yes') {
        return true;
    }

    return false;
}



function showContentAndSaveState() {

    saveState();
    stopSpinner();

    $('#startOfDynamicContent').show();
    scrollTopOfPage();
    $('#dynamicContent').fadeIn(300);
}


function debugAlert(error) {
    alert(error);
}


function startSpinner() {

    $("#loading").show();
}


function stopSpinner() {

    $("#loading").hide();
}


function hideContent() {
    $('#startOfDynamicContent').hide();
}


function resetFormAndCookies() {

    clearAllCookies();
    quote = {};
    user = {};
    defaults = {};
    formStatus = formStatusDefaults();
    windowsForm = windowFormDefaults();
    moduleInfo = {};
    uiSettings = {};

    writeModuleInfo();
    writeFormStatus();
}


function validFormStatus(fromTab) {

}


function updateFormStatus() {

    var numberOfItems = 0;
    if (!empty(quote)) {
        numberOfItems = numberOfItemInCart(quote.Cart);
    }

    $('#cartTotal').html(numberOfItems);
}

function resetErrors(errorType) {

    if (empty(formStatus.errors)) {
        formStatus.errors = {};
    }
    delete formStatus.errors[errorType];

    //if (!isEmptyObject(formStatus.errors)) {
    //    return false;
    //}

    return true;

}

function hasThisOption(option) {

    var hasThisOption = false;

    if (empty(moduleInfo.nav)) {
        return false;
    }

    for (var a = 0; a < moduleInfo.nav.length; a++) {
        var navMenu = moduleInfo.nav[a];

        if (navMenu.idTag == option) {
            hasThisOption = true;
        }
    }

    return hasThisOption;
}


function isGlassComplete() {

    var status = true;

    if (!resetErrors('glass')) {
        return false;
    }

    if (!hasThisOption('glass')) {
        return true;
    }

    if (empty(formStatus.glassChoices)) {
        formStatus.errors['glass'] = "Please select a type of Glass";
        return false;
    }

    return status;
}

function isExtraComplete() {

    var status = true;

    if (!resetErrors('extra')) {
        return false;
    }

    if (!hasThisOption('extra')) {
        return true;
    }

    formStatus.errors['extra'] = "test broken:";


    return false;

}

function isColorComplete() {

    var status = true;
    var message = "Please select an external "+colorText;
    var messageVinyl = "Please select a vinyl "+colorText;
    var messageInternal = "Please select an internal "+colorText;

    if (!resetErrors('color')) {
        return false;
    }

    if (!hasThisOption('color')) {
        return true;
    }

    if (empty(formStatus.extColor) && moduleInfo.isVinyl == 0) {
        formStatus.errors['color'] = message;
        status = false;
    } else if (formStatus.extColor.kind == 'custom' && empty(formStatus.extColor.customName)) {
        formStatus.errors['color'] = message;
        status = false;
    }


    if (empty(formStatus.vinylColor) && moduleInfo.isVinyl == 1) {
        formStatus.errors['color'] = messageVinyl;
        status = false;
    }
    if(formStatus.intNotSameExt && empty(formStatus.intColor)){
        formStatus.errors['color'] = messageInternal;
        status = false;
    }

    return status;
}


function isSizeComplete() {

    var status = true;
    var currentOption = moduleInfo.firstTab;
    
    if (!resetErrors(currentOption)) {
        return false;
    }

    if (!hasThisOption(currentOption)) {
        return true;
    }

    if (currentOption == 'sash') {
        //Bypassed because getting called at the wrong time and breaking form
        //return true;
        formStatus.errors.width = '';
        formStatus.errors.height = '';
        formStatus.errors.windowError = '';
        if (!isValidWidthSash()) {
            //debugAlert("YO");
            formStatus.errors.width = 'Please enter a valid width';
            return false;
        }

        if (!isValidHeightSash()) {
            formStatus.errors.height = 'Please enter a valid height';
            return false;
        } 
        if (!windowsForm.windowType) {
           formStatus.errors.windowError = 'Please select window';
            return false; 
        }
        return true;
    } else {
        if (!isWidthValid('strict')) {
            formStatus.errors.width = 'Please enter a valid width';
            return false;
        }

        if (!isHeightValid('strict')) {
            formStatus.errors.height = 'Please enter a valid height';
            return false;
        } 
    }
    
    for (var property in moduleInfo.panelOptions) {

        if (empty(formStatus.panelOptions)) {
            continue;
        }

        if (empty(formStatus.panelOptions[property])) {
            continue;
        }

        if (!empty(formStatus.panelOptions[property].nameOfOption)) {
                if (formStatus.panelOptions[property].nameOfOption == 'next') {
                    continue;
                }
        }
        
        // Skip movement check for new door type (with styles)
        if(formStatus.doorStyle && property == 'movement'){
            continue;
        }


        var selected = formStatus.panelOptions[property].selected;
        var forGuest = moduleInfo.panelOptions[property].forGuest;

        if (isGuest()) {
            if (empty(selected) && forGuest == 1) {
                formStatus.errors.size = 'You must complete all the selections on the current page to continue';
                return false;
            }
        } else {
            if (empty(selected) && !empty(property)) {

                if ( (property == 'swingDirection' || property == 'movement' || property == 'swingInOrOut' || property == 'screens' ) && moduleInfo.windowsOnly == 1) {
                    continue;
                }

                formStatus.errors.size = 'You must complete all the selections on the current page to continue';
                return false;
            }
        }
    }

    return status;
}


function missingSelections() {

    stopSpinner();

    var errors = '';
    for (var property in formStatus.errors) {
        errors = errors + formStatus.errors[property];
    }

    alert(errors);
}


function formComplete() {

    var status = false;
    //if (isSizeComplete() && (formStatus.extColor || formStatus.module == 2 || formStatus.module == 10 || formStatus.module == 13) && formStatus.glassChoices && formStatus.hardwareChoices) {
    status = true;
    //}
    return status;
}


function lockDownTabs() {

}


function authorizePlusAccounting() {

    return "&accounting=" + user.id + "&token=" + user.token + '&';
}


function authorizePlusProduction() {

    return "&production=" + user.id + "&token=" + user.token + '&';
}


function authorizePlusType(type) {

    return "&" + type + "=" + user.id + "&token=" + user.token + '&';
}


function authorizePlusLeads() {

    return "&leads=" + user.id + "&token=" + user.token + '&';
}


function authorizePlusSalesPerson() {

    if (empty(user.id) || empty(user.token)) {
        bounceUser();
    }
    return "&salesPerson=" + user.id + "&token=" + user.token + '&';
}


function authorize() {

    return "&token=" + user.token + '&';
}


function setPaginationWithSelectorAndTotal(selector, total, resetPage) {

    var pageLimit = pageLimitValues();

    if (!empty(resetPage)) {
        pageLimit.page = 1;
    }

    $(selector).pagination({
        items: total,
        itemsOnPage: pageLimit.limit,
        cssStyle: 'light-theme',
        currentPage: pageLimit.page
    });
}


function orderTableResultsWithTab() {

    var order = $(this).attr('id');
    var direction = getDirection();

    trackAndDoClick(tab, '&order=' + order + '&direction=' + direction + pageLimitText() + addSearch());
}


function isNumeric(n) {

    return !isNaN(parseFloat(n)) && isFinite(n);
}


function validNumber(value, selector) {

    if (!isNumeric(value)) {
        value = '';
    }

    if (value == 0) {
        value == '';
    }

    if (!empty(selector)) {
        $(selector).val(value);
    }

    return value;
}


function pageLimitValues() {

    return pageLimitWithType('values');
}


function pageLimitText(search) {

    return pageLimitWithType('text', search);
}


function pageLimitWithType(type, search) {
    var page = parseInt(getParameter('page'));

    var limit = uiState.limit;

    if (isNaN(page) || isNaN(limit)) {
        page = 1;
        limit = uiState.limit;
    }

    if (!empty(search)) {
        page = 1;

    }

    if (!isAccounting() && limit == 'All') {
        limit = 10;
    }
    var pageLimit = {'page': page, 'limit': limit};

    if (type == 'text') {
        return "&page=" + page + "&limit=" + limit;
    } else {
        return pageLimit;
    }
}


function buildPaginationBox() {

    var limitPullDown = buildLimitList();
    var pagination = '' +
        '<div id="paginationBox" class="paginationBoxLeads">' +
        '   <div id="limitPullDown" ><div id="limitPullDownText">Show: </div>' + limitPullDown + '</div> ' +
        '   <div id="pagination"></div>' +
        '</div>';


    return pagination;
}


function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function validatedSalesPersonLogin(data) {

    var valid = true;
    if (data) {
        if (data.status) {
            if (data.status === 'not authorized') {
                valid = false;
                resetFormAndCookies();
                trackAndDoClick('signIn', '&error="Yes"');
            }
        }
    }
    return valid;
}


function justValidate(data) {

    var valid = true;
    if (data) {
        if (data.status) {
            if (data.status === 'not authorized') {
                valid = false;
            }
        }
    }
    return valid;
}

function numberOfItemInCart(cart) {

    var count = 0;
    for (var property in cart) {
        count = count + Number(cart[property].quantity)
    }

    return count;
}


function cartHasNote(cart) {

    var status = false;
    for (var property in cart) {
        if (cart[property].note) {
            status = true;
        }
    }

    return status;
}


function getNumberOfPanels(cart) {

    var totalPanels = 0;
    for (var property in cart) {
        var panels = parseInt(cart[property].numberOfPanels);
        var quantity = parseInt(cart[property].quantity) ;
        totalPanels = totalPanels + (panels * quantity)
    }

    return totalPanels;
}


function getMaterial(cart) {

    var trackModule = 0;
    var type = '';

    for (var property in cart) {

        if (trackModule == 0) {
            trackModule = cart[property].module;
        }
        if (trackModule != cart[property].module) {
            return 'Mixed';
        }
        type = cart[property].materialType;
    }

    return type;
}


function getWood(cart) {

    var wood = 'no';
    for (var property in cart) {
        var door = cart[property]
        if (!empty(door.extColor)) {
            if (door.extColor.type == 'woodStandard' || door.extColor.type == 'woodPremium') {
                wood = 'Yes';
            }
            if (door.intNotSameExt == false) {
                if (!empty(door.intColor)) {
                    if (door.intColor.type == 'woodStandard' || door.intColor.type == 'woodPremium') {
                        wood = 'Yes';
                    }
                }
            }
        }
    }

    return wood;
}


function getNumberOfDoors(cart) {

    var count = numberOfItemInCart(cart);

    return count;
}


function depositDue() {

    var total = getTotalForSalesAdmin(quote.SalesDiscount);

    if (siteDefaults.salesFlow == 2) {
        return total;
    }
    if (siteDefaults.salesFlow == 3) {
        return total  * (.5);
    }

    total = total / 2;
    return total;
}


function footerTotalCost() {

    var total = calculateCostFromDiscount();
   
    total = currency + formatMoney(total);

    return total;
}


function checkEmptySalesDiscount() {

    if (empty(quote.SalesDiscount)) {
        setSalesDiscountToEmpty();
    }
}


function setSalesDiscountToEmpty() {

    quote = setSalesDiscountToEmptyWithPassQuote(quote);

}


function setSalesDiscountToEmptyWithPassQuote(quote) {

    quote.SalesDiscount = {};
    quote.SalesDiscount.Totals = {};
    quote.SalesDiscount.Totals.code = "YZ-00";

    if (siteName == 'panoramicdoors') {
        quote.SalesDiscount.Totals.code = "YZ-08";
    }

    quote.SalesDiscount.Discounts = {};
    quote.SalesDiscount.Payments = [];
    quote.SalesDiscount.Totals.discountTotal = {};
    quote.SalesDiscount.Totals.discountTotal.showOnQuote = true;
    quote.SalesDiscount.Totals.subTotal = {};
    quote.SalesDiscount.Totals.subTotal.showOnQuote = true;
    quote.SalesDiscount.Totals.screens = {};
    quote.SalesDiscount.Totals.screens.showOnQuote = true;
    quote.SalesDiscount.Totals.tax = {};
    quote.SalesDiscount.Totals.tax.showOnQuote = true;
    quote.SalesDiscount.Totals.shipping = {};
    quote.SalesDiscount.Totals.shipping.showOnQuote = true;
    quote.SalesDiscount.Totals.installation = {};
    quote.SalesDiscount.Totals.installation.showOnQuote = true;
    quote.SalesDiscount.Totals.extra = {};
    quote.SalesDiscount.Totals.extra.showOnQuote = true;
    quote.SalesDiscount.Totals.finalTotal = {};
    quote.SalesDiscount.Totals.finalTotal.showOnQuote = true;
    quote.SalesDiscount.Totals.cost = {};
    quote.SalesDiscount.Totals.cost.showOnQuote = true;

    return quote;

}

function checkEmptySalesDiscountWithQuote(quotePassed) {

    if (empty(quotePassed.SalesDiscount)) {
        quotePassed = setSalesDiscountToEmptyWithPassQuote(quotePassed);
    }

    return quotePassed;
}


function getDepositPaid() {

    if (empty(quote.SalesDiscount.Totals.amountPaid)) {
        quote.SalesDiscount.Totals.amountPaid = 0;
    }

    return quote.SalesDiscount.Totals.amountPaid;
}


function getLastFour() {

    if (empty(quote.SalesDiscount.Totals.lastFour)) {
        quote.SalesDiscount.Totals.lastFour = '';
    }

    return quote.SalesDiscount.Totals.lastFour;
}


function getPaymentNote() {

    if (empty(quote.SalesDiscount.Totals.paymentNote)) {
        quote.SalesDiscount.Totals.paymentNote = '';
    }

    return quote.SalesDiscount.Totals.paymentNote;
}


function getDeposit() {

    var total = calculateCostFromDiscount();
    var deposit = total / 2;
    return deposit;
}


function getAmountBySelector(selector) {

    var val = $(selector).val();

    if (empty(val)) {
        val = 0;
    }
    return parseFloat(val);
}


function createTableSpaceOnEmpty(value) {

    if (empty(value)) {
        value = '&nbsp;';
    }

    return value;
}


//TODO -- remove
function getQuoteNumber() {

    var prefix = 'SQ';
    var print = getParameter('print');
    if (print == 'yes') {
        prefix = getParameter('prefix');
    } else {
        if (isSalesPerson()) {
            prefix = user.prefix;
        }
    }

    var quoteNumber = ' &nbsp;';
    if (quote.id) {
        quoteNumber = prefix + quote.id;
    }

    return quoteNumber;
}


function getTotalCost() {

    if (empty(quote.SalesDiscount)) {
        return 0;
    }
    if (empty(quote.SalesDiscount.Totals)) {
        return 0;
    }
    quote.SalesDiscount.Totals.preCost =  getCostOfItemsInCart(quote.Cart);
    var total = getTotalForSalesAdmin(quote.SalesDiscount);

    return total;
}


function footerNoteHTML() {

    var html = '<div class="footerNote">plus applicable tax and shipping</div>';

    if ( siteDefaults.salesFlow != 1) {
        html = '';
    } else {
        if (siteDefaults.preTax == 1 || siteDefaults.preTax == 2 ) {
            html = '<div class="footerNote">plus applicable shipping</div>';

        }
    }



    return html;

}


function footerCartTotalCost() {

    var total = getTotalCost();

    if (isGuest()) {
        total = markupTotal(getTotalCost());

    }
    total = currency + formatMoney(total) ;

    return total;
}

function processStartOverButtonVisibility(show){
    show = show || false;
    $('#startOverButton').toggleClass('hide', !show);
}

function startOver() {
    $('#footerPrice').html('')
    startSpinner();
    resetCartAndForm();
    writeLocalStorageWithKeyAndData('quoteState', {'started':true});
    trackAndDoClick('home');
}


function resetCartAndForm() {

    if (isGuest()) {
        resetGuestDiscount();
    } else {
        resetSalesPersonDiscount();
    }

    formStatus = formStatusDefaults();
    windowsForm = windowFormDefaults();
    writeFormStatus();
    writeWindowsForm();

    updateFormStatus();
}


function salesStartOver() {

    startSpinner();
    startOver();

}

function salesCartStartOver() {
    if (confirm("Are you sure you want to delete everything in your cart and start over?") == true) {
        startSpinner();
        startOver();
    }
}


function guestStartOver() {

    if (confirm("Are you sure you want to delete this quote and start over?") == true) {
        startOver();
    }
}


function writeSelectionToFormRebuildingCookie() {

    var thisSection = $(this).parent().parent();
    var optionSelected = $(this).attr('id');
    var section = $(thisSection).attr('id');
    formStatus[section] = optionSelected;

    writeFormStatus();
}


function addFooter() {

    $.get('html/footer.html', function (html) {
        $('#copyWriteFooter').html(html);
    });
}


function screenClear() {

    $('#screen').hide();
    $('body').css({'overflow': 'visible'});
}


function screenDim() {


    $('#screen').css({opacity: 0.4, 'width': $(document).width(), 'height': $(document).height()}).show();
    $('body').css({'overflow': 'hidden'});
}


function signOut() {
    setUser({});
    trackAndDoClick('signIn');
}


function scrollTopOfPage() {

    var tab = getParameter('tab');
    var edit = getParameter('edit');
    var topOfPage = $("#startOfDynamicContent");

    if (tab === 'viewQuote' && edit || tab === 'viewOrder' && edit) {
        topOfPage = $("#mainContent");
        scrollToPositionAndOffset(topOfPage, -280, true);
    } else if (tab === 'viewQuote') {
        scrollToPositionAndOffset(topOfPage, -200, true);
    } else {
        scrollToPositionAndOffset(topOfPage, -200, true);
    }
}


function scrollToPositionAndOffset(nextSection, offset, avoidByPass) {


    if (empty(avoidByPass)) {
        return; //By-Passed
    }

    var id = nextSection.attr('id');

    if (nextSection) {
        var position = nextSection.position();

        if (position) {
            var scrollTo = position.top
            if (!empty(offset)) {
                scrollTo = position.top + offset;
            }
            $.scrollTo({top: scrollTo + 'px', left: '0px'}, {duration: 500});
        }
    }
}


function scrollToError(nextSection) {

    scrollTopOfPage();
}


function scrollToTextBox(nextSection) {

    scrollToPositionAndOffset(nextSection, -200);
}


function scrollToNextPositon(nextSection) {

    scrollToPositionAndOffset(nextSection, -100)
}


function resetFormPrice() {

    $('#footerPrice').html("T.B.D.");
}


function returnCleanedGlassOptions(options) {

    var cleanedOptions = [];

    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            if (options[key] == "yes") {
                var keyArray = key.split('-');
                cleanedOptions.push(keyArray[1]);
            }

        }
    }

    return cleanedOptions;
}


function hasMinimumPriceEngineOptions() {

    //TODO
    //formStatus.numberOfPanels = splitForSecondOption(formStatus.choosePanels);
    if (isWidthValid(true) && isHeightValid(true) && formStatus.numberOfPanels > 0) {
        return true
    } else {

        return false;
    }
}

function hasMinimumPriceEngineOptionsSash() {
    if (isValidWidthSash() && isValidHeightSash()) {
        return true
    } else {
        return false;
    }
}


function addButtonToFooterForCart() {

    //var addToCartOrSaveChanges = cartButtonAddToCartOrSaveChanges(formStatus);
    //var footerRight = $('#footerRight');
    //footerRight.unbind("click");
    //if (addToCartOrSaveChanges) {
    //    footerRight.bind("click", updateQuote).css({"cursor": "pointer"});
    //}
    //else {
    //    footerRight.css({"cursor": "none"});
    //}
    //footerRight.html(addToCartOrSaveChanges);

}


function addBottonToInstallationForCart() {

    var addToCartOrSaveChanges = cartButtonAddToCartOrSaveChanges(formStatus);

    var nextHardware = $('#nextHardware');
    if (addToCartOrSaveChanges) {
        nextHardware.show();
        $('#nextHardwareTitle').html(addToCartOrSaveChanges).show();
        $('.nextImage').show();
    }
}


function updateFormPrice() {

    if (moduleInfo.firstTab == 'sash') {
        if (!hasMinimumPriceEngineOptionsSash()) {
            $('#footerPrice').html('T.B.D');
            return;
        }
    } else {
       if (!hasMinimumPriceEngineOptions()) {
            $('#footerPrice').html('T.B.D');
            return;
        } 
    }

    var myOptions = buildPriceEngineOptionsWithOptions(formStatus);

    var json = JSON.stringify(myOptions);

    addButtonToFooterForCart();
    addBottonToInstallationForCart();
    showAddToQuoteButton();

    var url = "https://" + apiHostV1 + "/api/doors?" + addSite();

    requestUrlAndLogIfErrorPromiseForPost(url,json,'Error getting price').then(function(data){
        var pricing = jQuery.parseJSON(data);
        
        discounts.Totals.preCost = pricing.cost;
        formStatus.panelWidth = pricing.panelWidth;

        var amount = footerTotalCost();
        
        $('#footerPrice').html(amount).show();

        formStatus.total = pricing.cost;
        writeFormStatus();
    });
}

function buildWindowRows() {

    var rows = windowsForm.rows;

    var apiRows = [];

    for (var a = 0; a < rows.length; a++) {
       var row  = buildSingleWindowRow(a);
        apiRows.push(row);
   }

    return apiRows;
}

function getOpener(row, column) {

    var opener = '';

    if (!empty(windowsForm.openers)) {
        if (!empty(windowsForm.openers['window-'+row+'-'+column])) {
            opener = windowsForm.openers['window-'+row+'-'+column];
        }
    }

    if (opener=='none') {
        opener = '';
    }

    return opener;
}

function buildSingleWindowRow(index) {

    var rows = windowsForm.rows;
    var row = rows[index];
    var apiRow = {};

    var widths = [];
    var openers = [];

    var widthTotal = 0;
    apiRow.height = row.height;
    for (var a = 1; a < row.numberOfWindows; a++) {
        var width = row['width'+a] ;
        var opener = getOpener(index + 1, a);
        widthTotal = widthTotal + width;
        widths.push(width)
        openers.push(opener);
    }
    var finalWidth = windowsForm.width - widthTotal;
    widths.push(finalWidth);
    openers.push(getOpener(index + 1, row.numberOfWindows));
    apiRow.widths = widths;
    apiRow.columns = row.numberOfWindows;
    apiRow.openers = openers;

    return apiRow;
}



function buildPriceEngineOptionsForWindows(options) {

    var myOptions = {
        module: options.module,
        width: windowsForm.width,
        height: windowsForm.height,
        rows: buildWindowRows(),
        type: "vertical",
        systemType: windowsForm.windowType,
        pricingType: moduleInfo.type
    };

    return myOptions;
    
}


function hasMinimumWidowOptions() {

    if (empty(windowsForm.windowType)) {

        return false;
    } else {

        return true;
    }
}


function updateWindowsPrice() {

    if (!hasMinimumWidowOptions()) {
        $('#footerPrice').html('T.B.D-Windows');
        return;
    }
    var myOptions = buildPriceEngineOptionsForWindows(windowsForm);

    var json = JSON.stringify(myOptions);

    
    // var myPromise = getWindowsPrice(myOptions);
    //
    // myPromise.done(function (data) {
    //     var pricing = jQuery.parseJSON(data);
    //     var total = currency + formatMoney(pricing.cost);
    //     $('#footerPrice').html(total).show();
    // }).error(function (err) {
    //     $('#footerPrice').html("Pricing Error").show();
    // });
}


function buildNumberOfColors(options) {

    var paintedFinishArray;
    if (options.paintedFinish) {
        paintedFinishArray = options.paintedFinish.split('-');
    } else {
        return "NA";
    }

    var numberOfColors = paintedFinishArray[2];

    if (numberOfColors === "no") {
        return "none";
    }
    if (numberOfColors === "interior" || numberOfColors === "exterior") {
        return "One Color";
    }

    if (numberOfColors === "both") {
        return isTheSameFinish(options);
    }

    return "NA";
}


function isTheSameFinish(options) {

    if (options.extColor.name1 == options.intColor.name1 || !options.intNotSameExt) {
        return "One Color";
    } else {
        return "Two Color";
    }
}


function buildFinishOptions(options) {

    var numberOfSides = "oneSide";
    var sameType = "no";

    if (!options.extColor) {
        return;
    }
    if (options.extColor.name1 == options.intColor.name1) {
        numberOfSides = "bothSides";
        sameType = "yes";
    }
    if (options.extColor.kind == options.intColor.kind) {
        sameType = "yes";
    }
    if ((options.extColor.kind === 'arch' && options.intColor.kind == 'ral') || (options.extColor.kind === 'ral' && options.intColor.kind == 'arch')) {
        sameType = "yes";
    }
    if (options.extColor.kind === 'woodStandard' && options.intColor.kind === 'woodStandard') {
        sameType = "yes";
        numberOfSides = "bothSides";
    }
    if (options.extColor.kind === 'woodStandard' && options.intColor.kind === 'woodPremium') {
        sameType = "no";
        numberOfSides = "swpw";
        if (isUK()) {
            numberOfSides= "oneSide";
        }
    }
    if (options.extColor.kind === 'woodPremium' && options.intColor.kind === 'woodStandard') {
        sameType = "no";
        numberOfSides = "swpw";
        if (isUK()) {
            numberOfSides= "oneSide";
        }
    }
    if (options.extColor.kind === 'woodPremium' && options.intColor.kind === 'woodPremium') {
        sameType = "yes";
        numberOfSides = "bothSides";
    }
    if (options.extColor.kind === 'custom') {
        numberOfSides = "oneSide";
        if (options.extColor.customName != options.intColor.customName) {
            sameType = 'no';
        }
    }


    return {sameType: sameType, numberOfSides: numberOfSides};
}


function splitForGlass(option) {

    var arrayOption = '';
    if (option) {
        arrayOption = option.split(/glass-/);
    }

    if (arrayOption[1]) {
        return arrayOption[1];
    } else {
        return '';
    }
}


function splitForSecondOption(option) {

    return getOptionIndex(option, 1);
}

function splitForFirstOption(option) {

    return getOptionIndex(option, 0);
}


function splitForThirdOption(option) {
    
    return getOptionIndex(option, 2);
}


function splitForFourthOption(option) {

    return getOptionIndex(option, 3);
}


function getOptionIndexOld(option, index) {

    var arrayOption = '';
    if (option) {
        arrayOption = option.split(/-(.+)?/);
    }

    if (arrayOption[index]) {
        return arrayOption[index];
    } else {
        return '';
    }
}


function splitOptionFirstSpace(option) {

    return getOptionIndexWithSpace(option, 0);
}


function getOptionIndexWithSpace(option, index) {

    var arrayOption = '';
    if (option) {
        arrayOption = option.split(/ (.+)?/);
    }

    if (arrayOption[index]) {
        return arrayOption[index];
    } else {
        return '';
    }
}


function buildPanelMovementRight(options) {

    var swings ='';

    var left = 0;
    if (options.panelOptions) {
        swings = options.panelOptions.swingDirection.selected;
        left  = Number(options.panelOptions.movement.slideLeft);
        if (isNaN(left)) {
            left  = Number(options.panelOptions.movement.numberLeft);
        }
        if (isNaN(left)) {
            left  = '';
        }
    } else {
        swings = options.chooseSwings
    }

    var panelMovementRight = '';

    if (swings == "right") {
        panelMovementRight = options.numberOfPanels - 1;
    }
    if (swings == "left") {
        panelMovementRight = 0;
    }
    if (swings == "both") {
        panelMovementRight = Number(options.numberOfPanels) - Number(left) - getNumberOfSwings(options);
    }

    return panelMovementRight;
}

function convertColorTypeToKind(options) {

    if (!empty(options.extColor)) {
        if (!empty(options.extColor.type)) {
            options.extColor.kind = options.extColor.type;
        }
    }
    if (!empty(options.intColor)) {
        if (!empty(options.intColor.type)) {
            options.intColor.kind = options.intColor.type;
        }
    }

    return options;
}

function buildPriceEngineOptionsWithOptions(options) {
    var glassChoices = returnCleanedGlassOptions(options.glassChoices);
    var hardware = splitForSecondOption(options.hardwareChoices);
    var glassOptions = returnCleanedGlassOptions(options.glassOptions);
    var installOptions = splitForSecondOption(options.installationOptions);
    var trackOption = options.panelOptions.track.selected;

    var screenUS = '';

    if (!empty(options.panelOptions.screens)) {
        screenUS = options.panelOptions.screens.selected;
    }
    var frameOption = options.panelOptions.frame.selected;
    if (empty(frameOption)) {
        frameOption = options.frame;
    }

    var numberOfColors = buildNumberOfColors(options);
    var swingDirection = options.panelOptions.swingInOrOut.selected
    var panelMovementRight = buildPanelMovementRight(options);

    if (isGuest()) {
        if (formStatus.module == "3") {
            frameOption = "Aluminum Block";
        }
    }

    var finishes = {};

    if (empty(options.intNotSameExt)) {
        options.intColor = options.extColor;
    } else if (options.intNotSameExt == false) {
        options.intColor = options.extColor;
    }

    options = convertColorTypeToKind(options);

    finishes.ext = options.extColor;
    finishes.int = options.intColor;
    finishes.paintedColor = numberOfColors;
    finishes.finishOptions = buildFinishOptions(options);

    if (!empty(options.vinylColor)) {
        finishes.vinylColor = options.vinylColor.name1;
    } else {
        if (options.materialType == 'Vinyl') {
            finishes.vinylColor = "White";
        }
    }

    var hardwareExtraChoices = optionsFromCartRow(options.hardwareExtraChoices);

    var myOptions = {
        module: options.module,
        width: options.width,
        height: options.height,
        panels: options.numberOfPanels,
        swingDoor: options.panelOptions.swingDirection.selected,
        glassChoice: glassChoices,
        glassOptions: glassOptions,
        finishes: finishes,
        hardware: hardware,
        hardwareExtras: hardwareExtraChoices,
        installOptions: installOptions,
        track: trackOption,
        frame: frameOption,
        swingDirection: swingDirection,
        panelMovementRight: panelMovementRight,
        subModules: options.subModules,
        productionNotes: options.productionNotes,
        note: options.note,
        sill: buildSillForPriceEngine(options),
        addOns: buildAddOnsForPriceEngine(options),
        trickle: buildTrickleForPriceEngine(options),
        bayPole: buildBayPoleForPriceEngine(options),
        screen: buildScreenForPriceEngine(options),
        screenUS: screenUS
    };
    
    myOptions = updateMyOptionWithModifiedHeightAndWidths(myOptions);

    return myOptions;
}


function updateMyOptionWithModifiedHeightAndWidths(myOptions) {

    var heightLessFromSill = sillHeightDeduct(myOptions.sill);
    var widthLessLeftRightExtenders = leftRightExtendersDeduct(myOptions.addOns.leftExtender, myOptions.addOns.rightExtender);
    var heightLessTopExtender = topExtenderDeduct(myOptions.addOns.topExtender);

    if (!isNaN(heightLessFromSill) || !isNaN(heightLessTopExtender) ) {
        myOptions.height = myOptions.height - heightLessFromSill - heightLessTopExtender;
    }

    if (!isNaN(widthLessLeftRightExtenders)) {
        myOptions.width = myOptions.width - widthLessLeftRightExtenders;
    }

    return myOptions;
}


function buildJobInfoOld(property) {

    var jobNumber = $('#' + property + '-jobNumber').val();
    var productionDate = $('#' + property + '-productionDate').val();
    var dueDate = $('#' + property + '-dueDate').val();

    if (empty(jobNumber)) {
        jobNumber = 1;
    }
    var jobInfo = {jobNumber: jobNumber, productionDate: productionDate, dueDate: dueDate};

    return (jobInfo);
}


function showCostSheet(property) {

    var myOptions = buildPriceEngineOptionsWithOptions(quote.Cart[property]);

    myOptions.jobInfo = buildJobInfoOld(property);

    quote.Cart[property].jobInfo = myOptions.jobInfo;
    writeQuote(quote);

    var json = JSON.stringify(myOptions);
    var url = "https://" + apiHostV1 + "/api/buildCut?" + addSite();

    $.post(url, json, function (data) {


        var cut = jQuery.parseJSON(data);

        window.open("https://" + apiHostV1 + "/sheet/cost/index.html?jobNumber=" + cut.cutSheet);
    });
}


function buildExtendedButtons(buttonData, option) {
    var buttons = '';
    for (var a = 0; a < buttonData.length; a++) {

        var buttonInfo = buttonData[a];
        var button = buildExtendedButton(buttonInfo, option);
        buttons = buttons + button;
    }

    return buttons;


}


//function buildExtendedButton(button, option) {
//
//    var id = option + '-' + button.highlight;
//
//    var button =
//        ['<div id="' + id + '" class="extendedButton">',
//            '<div class="extendedButtonImage"><img src="' + button.url + '"> </div>',
//            '<div class="extendedButtonTitle">' + button.name + '</div>',
//            '<div class="extendedButtonDescription">' + button.description + '</div>',
//            '</div>'
//        ].join('\n');
//
//    return (button);
//}

function buildExtendedButton(button, option, defaultSelected) {

    defaultSelected = defaultSelected || false;
    var id = option + '-' + button.name;

    var button =
        ['<div id="' + id + '" class="buttonRef extendedButton">',
            '<div class="extendedButtonImage buttonImageRef">'+
                '<img src="' + button.url + '">'+
                (defaultSelected ? '<div class="extendedSelectedDiv selectedDivRef"><img src="images/icon-selected.svg"></div>' : '')+
            '</div>',
            '<div class="extendedButtonTitle">' + button.name + '</div>',
            '<div class="extendedButtonDescription">' + button.description + '</div>',
            '</div>'
        ].join('\n');

    return (button);
}

function buildExtendedMaxButton(button, option, defaultSelected) {

    defaultSelected = defaultSelected || false;
    var id = option + '-' + button.name;

    var button =
        ['<div id="' + id + '" class="buttonRef button">',
            '<div class="buttonImage buttonImageRef">'+
                '<img src="' + button.url + '">'+
                (defaultSelected ? '<div class="selectedDiv selectedDivRef"><img src="images/icon-selected.svg"></div>' : '')+
            '</div>',
            '<div class="buttonLabel">' + button.name + '</div>',
            '</div>'
        ].join('\n');

    return (button);
}


function reloadExtendHalfButtons(id, option) {

    var extendedButtonsData = readKey(option);

    var buttons = buildExtendedHalfButtons(extendedButtonsData, option);

    $(id).html(buttons);
    //$(id).append(buttons);
    $('.extendedHalfButton').unbind("click").bind("click", extendedHalfSelected);

    placeSelectedImageExtendedHalf(formStatus.chooseFrameSection);
}


function addExtendedHalfButtons(id, option) {

    var myPromise;

    var url = "https://" + apiHostV1 + "/api/extendedButtons?module=" + formStatus.module + "&option=" + option + authorize() + addSite();
    myPromise = $.getJSON(url, function (apiJSON) {

        validate(apiJSON);
        var extendedButtonsData = apiJSON;

        writeLocalStorageWithKeyAndData(option, extendedButtonsData);
        reloadExtendHalfButtons(id, option);
    });

    return myPromise;
}


function buildExtendedHalfButtons(buttonData, option, disabled) {

    var buttons = [];
    for (var a = 0; a < buttonData.length; a++) {

        var buttonInfo = buttonData[a];

        if (buttonInfo.maxSize >= formStatus.width || empty(buttonInfo.maxSize)) {
            var button = buildExtendedHalfButton(buttonInfo, option, disabled);
            buttons.push(button);
        } else {
            var selection = splitForSecondOption(formStatus.chooseFrameSection);
            if (selection == buttonInfo.name) {
                formStatus.chooseFrameSection = '';
            }
        }

    }

    return (buttons);
}


function extendedHalfSelected() {

    extendedHalfSelectedWithThis(this);
}


function findOptions(option) {

    for (var property in moduleInfo.options) {
        if (moduleInfo.options[property].nameOfOption == option) {

            var newProperty = parseInt(property) + 1;

            if (!empty(moduleInfo.options[newProperty])) {
                return moduleInfo.options[newProperty].nameOfOption;
            }
        }
    }

    return '';
}


function hasOption(option) {

    for (var property in moduleInfo.options) {
        if (moduleInfo.options[property].nameOfOption == option) {

            var newProperty = parseInt(property);

            if (!empty(moduleInfo.options[newProperty])) {
                return moduleInfo.options[newProperty].nameOfOption;
            }
        }
    }

    return '';
}


function extendedHalfSelectedWithThis(newThis) {

    var thisSelector = $(newThis);

    var thisSection = thisSelector.parent();
    var thisGroup = thisSection.parent().attr('id');
    var optionSelected = thisSelector.attr('id');
    var section = $(thisSection).attr('id');
    var nextSection = thisSelector.parent().parent().next();


    if (section == 'heightSection') {
        formStatus.height = thisSelector.attr('data-extra');
    }

    if (section === "chooseTrackSection2") {
        section = "chooseTrackSection";
    }
    formStatus[section] = optionSelected;
    writeFormStatus();

    var groupOfButtons = thisSection;

    var selectedDiv = $(".extendedSelectedDiv", groupOfButtons);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
    }

    if (nextSection.attr('id') === 'chooseTrack') {
        buildTrack();
        buildSwingInsideOutside();
    }

    var nextOption = findOptions(thisGroup);

    $('#' + nextOption).show();

    var selector = $('.extendedHalfButtonImage', newThis);
    displayExtendedSelected(selector);
    scrollToNextPositon(nextSection);

    updateFormPrice();
}


function displayExtendedHalfSelected(selector) {

    jQuery('<div/>', {
        class: 'extendedHalfSelectedDiv'
    }).appendTo($(selector));

    jQuery('<img/>', {
        src: 'images/icon-selected.svg'
    }).appendTo($(".extendedHalfSelectedDiv", selector));
}


function print(data) {
    console.log("######################");
    console.log(data);
}


function empty(variable) {

    var status = false;

    if (variable === undefined || variable === null || variable == '' || variable == 'null' || variable == 'undefined') {
        status = true;
    }

    return status;
}

function isEmptyObject(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}


function returnTableSpaceIfEmpty(variable) {

    if (empty(variable)) {

        return '&nbsp;';
    }

    return variable;
}


function returnBlankIfEmpty(variable) {

    if (variable === undefined || variable === null || variable == '') {
        variable = '';
    }

    return variable;
}


function validateField(attr) {

    var status = true;
    if (!attr.value) {

        $('#' + attr.name + 'Error').show();
        status = false;
    }

    return status;
}


function flog(value, label) {

    if (!empty(label)) {
        console.log("**********" + label + "**********");
    }
    console.log(value);
}


function removeNoCart(quote) {
    if (!empty(quote.noCart)) {
        delete quote.noCart;
    }
    return quote;
}

function getGuestTab(){
    var isGuestForm = (tab === 'guestForm') || user.guestForm || false;
    return isGuestForm ? 'guestForm' : 'guest';
}

function isHideDoorRestrictor(){
    return (formStatus.width / formStatus.numberOfPanels) < 28;
}

/*
 * Check for door style
 * @param array buttons
 * @returns bool
 */ 
function hasDoorStyles(trackOptions){
    var hasDoorStyles = false;
    var result = $.grep(trackOptions, function(option){ return option.name == 'Alicante'; });
    if(result.length){
        hasDoorStyles = true;
    }
    return hasDoorStyles;
}

function getSelectedDoorStyle(panelOptions){
    var data = [];
    if(panelOptions && hasDoorStyles(panelOptions.track.buttons)){
        data = $.grep(panelOptions.track.buttons, function(b){return b.name == panelOptions.track.selected});
    }
    return data;
}

function getOrientedDoorStyleImageUrl(buttonUrl, panelOptions){
    var url = '';
    if(panelOptions && panelOptions.swingDirection && panelOptions.swingDirection.selected === "right"){
        url = buttonUrl.replace('.svg','-right.svg');
    }else if(panelOptions.swingDirection.selected === "left"){
        url = buttonUrl.replace('-right.svg','.svg');
    }else{
        url = buttonUrl;
    }
    return url;
}
function groupBy(arr, property) {
  return arr.reduce(function(memo, x) {
    if (!memo[x[property]]) { memo[x[property]] = []; }
    memo[x[property]].push(x);
    return memo;
  }, {});
}

function isWindowModule(row){
    return !empty(row.windowsData);
}