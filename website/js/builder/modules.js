var moduleData;


function loadViewModules() {

    tab = 'viewModules';

    $('#footerPrice').hide();

    resetUIForTopNav();

    updateFormStatus();


    var url = "https://" + apiHostV1 + "/api/modules" + '?' + addSite();
    $.getJSON(url, function (apiJSON) {

        var modules = apiJSON;

        var rows = buildModuleRowTable(modules);
        var html = buildModuleHeaderTable(rows);
        $('#mainContent').html(html);
        $('.moduleTableRow').unbind("click").bind("click", loadModule);

    });

    showContentAndSaveState();

}


function loadViewModule() {

    var id = getParameter('moduleID');
    var url = "https://" + apiHostV1 + "/api/modules/" + id + '?' + addSite();
    $.getJSON(url, function (apiJSON) {

        moduleData = apiJSON;
        var form = buildForm(moduleData);
        var html = modulePreviewWithModuleAndForm(moduleData, form);

        $('#mainContent').html(html);

        $('.inputField').unbind("keyup").bind("keyup", updateForm);
        $('#save').unbind("click").bind("click", saveModuleForm);
        $('#cancel').unbind("click").bind("click", cancelModuleForm);

        var myDropzone = $(".dropzone").dropzone({url: "/api/images/upload/" + moduleData.moduleID}).get(0).dropzone;


        myDropzone.on("success", function (file, responseText) {


            var response = JSON.parse(responseText);

            var name = 'images/uploads/' + response.file;
            moduleData.tempUrl = name;

            //$('#previewImage').attr('src',name );
            var preview = buildProducts(moduleData);
            $('.modulePreview').html(preview);


        });
    });

    showContentAndSaveState();


}
function saveModuleForm() {

    var json = JSON.stringify(moduleData);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/modules/" + moduleData.moduleID + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {

                trackAndDoClick('viewModules');

            } else {
                alert("Module Save Failed");
            }
        }
    });
}

function cancelModuleForm() {

    trackAndDoClick('viewHalfButtons');
    //trackAndDoClick('viewModules');

}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


function updateForm() {


    $(':input').each(function () {
        var id = $(this).attr("id");
        var value = $(this).val();
        moduleData[id] = value;
    });

    var preview = buildProducts(moduleData);
    $('.modulePreview').html(preview);


}
function modulePreviewWithModuleAndForm(module, form) {

    var preview = buildProducts(module);

    var html = '' +
        '<div class="moduleForm">' + form + '</div>' +
        '<div class="modulePreview">' + preview + '</div>' +
        '<div class="moduleSaveForm">' +
        '   <div id="save" class="moduleButtons myButton selectedButton">Save Changes</div>' +
        '   <div id="cancel" class="moduleButtons myButton selectedButton">Cancel</div>' +
        '</div>';


    return html;


}


function buildForm(data) {


    var formRows = '';
    for (var property in data) {
        if (data.hasOwnProperty(property)) {
            var row = buildFormElement(property, data[property]);
            formRows = formRows + row
        }
    }
    formRows =
        '<form action="" method="post">' +
        '   ' + formRows +
        '</form>' +
        '<div class="imageUpload"> ' +
        '   <div id ="myDropzone" class="dropzone"></div>' +
        '</div>';


    return (formRows);


}
function buildFormElement(key, value) {

    if (key == 'description') {
        return buildTextAreaWithValue(key, value);
    } else if (key == 'url') {
        return '';
    } else {
        return buildTextFieldWithValue(key, value);
    }

}

function buildTextAreaWithValue(key, value) {

    var input = '' +
        '<div class="formDiv">' +
        '   <div class="moduleLabel">' + key + '</div>' +
        '   <TEXTAREA class="moduleFormTextArea inputField"  ID="' + key + '">' +
        value +
        '   </TEXTAREA>' +
        '</div>';

    return input;
}


function buildTextFieldWithValue(key, value) {

    var input = '' +
        '<div class="formDiv">' +
        '   <div class="moduleLabel">' + key + '</div>' +
        '   <input class="moduleForm inputField" type="text" id="' + key + '" value="' + value + '"/>' +
        '</div>';

    return input;
}


function loadModule() {

    var id = $(this).attr('id');
    trackAndDoClick('viewModule', '&moduleID=' + id);

}
function buildModuleRowTable(modules) {

    var rows = '';
    for (var a = 0; a < modules.length; a++) {
        var row = '' +
            '<div id=' + modules[a].moduleID + ' class="moduleTableRow">' +
            '   <div  class="idTableWidth borderLeftTop">' + modules[a].moduleID + '</div>' +
            '   <div class="moduleTitleWidth borderLeftRightTop">' + modules[a].title + '</div>' +
            '</div>';

        rows = rows + row;
    }
    return rows;

}


function buildModuleHeaderTable(rows) {

    var html = '' +
        '<div class="moduleTableHeader">' +
        '   <div class="idTableWidth borderLeftTop" >Module #</div>' +
        '   <div class="moduleTitleWidth borderLeftRightTop" >Amount</div>' +
        '</div>' +
        rows +
        '<div class="modulesFinalRow"></div>';

    return html;
}
