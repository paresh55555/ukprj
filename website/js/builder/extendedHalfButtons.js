var moduleData;



function loadHalfButtons() {

    tab = 'viewHalfButtons';

    $('#footerPrice').hide();

 resetUIForTopNav();

    updateFormStatus();


    var url = "https://" + apiHostV1 + "/api/halfButtons?option=track" + addSite() ;
    $.getJSON(url, function (apiJSON) {

        var halfButtons = apiJSON;

        var rows = buildHalfButtonRowTable(halfButtons);
        var html = buildHalfButtonHeaderTable(rows);
        $('#mainContent').html(html);
        $('.moduleTableRow').unbind("click").bind("click", viewHalfButton);

    });

    showContentAndSaveState();

}

function viewHalfButton() {

    var id = $(this).attr('id');
    trackAndDoClick('viewHalfButton', '&halfButtonID=' + id);

}

function loadHalfButton() {

    var id = getParameter('halfButtonID');
    var url = "https://" + apiHostV1 + "/api/halfButtons/" + id + '?'+addSite();
    $.getJSON(url, function (apiJSON) {

        var buttonData = apiJSON;
        var form = buildForm(buttonData);
        var html = halfButtonPreviewWithModuleAndForm(buttonData, form);

        $('#mainContent').html(html);

        $('.inputField').unbind("keyup").bind("keyup", updateForm);
        $('#save').unbind("click").bind("click", saveModuleForm);
        $('#cancel').unbind("click").bind("click", cancelModuleForm);

        var myDropzone = $(".dropzone").dropzone({url: "/api/images/upload/" + buttonData.halfButtonID}).get(0).dropzone;


        myDropzone.on("success", function (file, responseText) {

            var response = JSON.parse(responseText);
            var name =  'images/uploads/'+response.file;
            moduleData.tempUrl = name;

            //$('#previewImage').attr('src',name );
            var preview = buildProducts(moduleData);
            $('.modulePreview').html(preview);

            // Handle the responseText here. For example, add the text to the preview element:
            //file.previewTemplate.appendChild(document.createTextNode(responseText));

        });
    });

    showContentAndSaveState();




}

function halfButtonPreviewWithModuleAndForm(button, form) {

    var preview = buildExtendedHalfButton(button, button.halfButtonID, null) ;


    var html = '' +
        '<div class="moduleForm">' + form + '</div>' +
        '<div class="modulePreview">' + preview + '</div>' +
        '<div class="moduleSaveForm">' +
        '   <div id="save" class="moduleButtons myButton selectedButton" >Save Changes</div>' +
        '   <div id="cancel" class="moduleButtons myButton selectedButton">Cancel</div>' +
        '</div>' ;


    return html;


}

function buildHalfButtonRowTable(modules) {

    var rows = '';
    for (var a = 0; a < modules.length; a++) {
        var row = '' +
            '<div id=' + modules[a].halfButtonID + ' class="moduleTableRow">' +
            '   <div  class="idTableWidth borderLeftTop">' + modules[a].halfButtonID + '</div>' +
            '   <div class="moduleTitleWidth borderLeftRightTop">' + modules[a].name + '</div>' +
            '</div>';

        rows = rows + row;
    }
    return rows;

}


function buildHalfButtonHeaderTable(rows) {

    var html = '' +
        '<div class="moduleTableHeader">' +
        '   <div class="idTableWidth borderLeftTop" >ID #</div>' +
        '   <div class="moduleTitleWidth borderLeftRightTop" >Name</div>' +
        '</div>' +
        rows +
        '<div class="modulesFinalRow"></div>' ;

    return html;
}

