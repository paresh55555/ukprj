'use strict'


function getColorNoteHTML() {

    var colorNoteMainHTML = '';
    var colorNoteSecondaryHTML = '';

    if (!empty(moduleInfo.colorNoteMain)) {
        colorNoteMainHTML = '<div id="colorNote" class="colorNote">' + moduleInfo.colorNoteMain + '</div> ';
    }
    if (!empty(moduleInfo.colorNoteSecondary)) {
        colorNoteSecondaryHTML = '<div class="colorNote2">' + moduleInfo.colorNoteSecondary + '</div> ';
    }

    var html = '' +
        colorNoteMainHTML +
        colorNoteSecondaryHTML;

    return html;
}


function getColorHTML() {

    var vinylColor = getVinylColorHTML();
    var sameAsExt = getSameAsExtHTML();

    var extColor = getExternalColorHTML();
    if (empty(extColor)) {
        sameAsExt = '';
    }

    var intColor = getInternalColorHTML();
    if (empty(intColor)) {
        sameAsExt = '';
    }

    var footer = getColorFooterHTML();

    var html = '' +
        '<div id="dynamicContent"> ' +
        vinylColor +
        extColor +
        sameAsExt +
        intColor +
        footer +
        '</div>';

    return html;
}


function getColorFooterHTML() {

    var html = '' +
        '<div id="nextSection"></div> ' +
        '<div class="spacer"></div> ' +
        '<div id="copyWriteFooter"></div> ' +
        '<div class="spacer"></div>';

    return html;
}


function getExternalColorHTML() {

    if (empty(colorChoicesByModule[formStatus.module].extColor)) {
        return '';
    }

    var ralColors = '';
    if (formStatus.extColor.kind == 'ral') {
        ralColors = buildSuperSectionButtons(superSectionsRAL, 'extColor');
    }

    var externalColors = colorChoicesByModule[formStatus.module].extColor;
    if(isGuest()){
        delete externalColors['woodStandard'];
        delete externalColors['woodPremium'];
    }
    var extButtons = buildFinishGroup(externalColors, 'extColor');

    var selectedColorOptions = buildSelectedColorOptions(externalColors, 'extColor');

    var colorGroupTitle = getColorOptionsTitle(externalColors);
    var previewImage = getPreviewHTML();
    var extNote = getColorNoteHTML('ext');

    var html = '' +
        '<div id="extColorContent" class="content"> ' +
        '   <div class="menu2_wrap"> ' +
        '       <div id="previewImageBox" class="menu2 previewExt"> ' +
        '           <div id="extPreviewImagePng"></div> ' +
        '           <div id="extPreviewImage">' + previewImage + '</div> ' +
        '       </div> ' +
        '   </div> ' +
        '   <div class="wrap"> ' +
        '       <div id="chooseExtFinish"> ' +
        '          <div class="wrapper"> ' +
        '               <div class="title">Choose Exterior '+ capitaliseFirstLetter(colorText) + ' or Laminate</div> ' +
        '               ' + extNote +
        '               <div class="sectionBlock"> ' +
        '                   <div id="extFinishGroup" class="finishMenu">' +
        '                       ' + extButtons +
        '                   </div> ' +
        '                   <div id="extColorOptionsTitle" class="finishOptionsTitle">' +
        '                   ' + colorGroupTitle +
        '                   </div> ' +
        '                   <div id="extColorSuperSectionRAL" class="superSection">' +
        '                       ' + ralColors +
        '                   </div> ' +
        '                   <div id="extColorOptions" class="finishOptions">' +
        '                   ' + selectedColorOptions +
        '                   </div> ' +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div> ';

    return html;
}


function getInternalColorHTML() {

    if (empty(colorChoicesByModule[formStatus.module].intColor)) {
        return '';
    }

    var internalColors = colorChoicesByModule[formStatus.module].intColor;

    var colorArray = Object.keys(internalColors)
    var hasRal = colorArray.indexOf("ral");

    var ralColors = '';
    if (formStatus.intColor.kind == 'ral' && hasRal > 0) {
        ralColors = buildSuperSectionButtons(superSectionsRAL, 'intColor');
    }

    var hideCss = '';
    if (!formStatus.intNotSameExt) {
        hideCss = ' style="display:none" ';
    }


    var intButtons = buildFinishGroup(internalColors, 'intColor');

    var selectedColorOptions = buildSelectedColorOptions(internalColors, 'intColor');
    var colorGroupTitle = getColorOptionsTitle(internalColors);

    var previewImage = getPreviewHTML();
    var html = '' +
        '<div id="intColorContent" class="content" ' + hideCss + '> ' +
        '   <div class="menu3_wrap"> ' +
        '       <div id="previewImageBox2"  class="menu3 previewInt"> ' +
        '           <div id="intPreviewImagePng"></div> ' +
        '           <div id="intPreviewImage">' + previewImage + ' </div> ' +
        '       </div> ' +
        '   </div> ' +
        '   <div class="wrap"> ' +
        '       <div id="chooseIntFinish"> ' +
        '           <div class="title">Choose Interior Color or Laminate</div> ' +
        '           <div class="sectionBlock"> ' +
        '               <div id="intFinishGroup" class="finishMenu">' +
        '                  ' + intButtons +
        '               </div> ' +
        '               <div id="intColorOptionsTitle" class="finishOptionsTitle">' +
        '               ' + colorGroupTitle +
        '               </div> ' +
        '               <div id="intColorSuperSectionRAL" class="superSection">' +
        '                   ' + ralColors +
        '               </div> ' +
        '               <div id="intColorOptions" class="finishOptions">' +
        '                   ' + selectedColorOptions +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div>' +
        '</div>';

    return html;
}

function getSameAsExtHTML() {

    var noSelected = '';
    var yesSelected = 'selectedColorGroup';
    if (formStatus.intNotSameExt) {
        noSelected = 'selectedColorGroup';
        yesSelected = '';
    }

    var html = '' +
        '    <div id="sameAsExterior"> ' +
        '        <div class="title">Interior finish same as exterior?</div> ' +
        '        <div class="sectionBlock"> ' +
        '            <div class="finishMenu"> ' +
        '                <div id="yes" class="finishMenuItemQuestionYesNo ' + yesSelected + '" data-finish="Yes">Yes</div> ' +
        '                <div id="no" class="finishMenuItemQuestionYesNo ' + noSelected + '" data-finish="No">No</div> ' +
        '            </div> ' +
        '        </div> ' +
        '    </div> ';

    return html;
}


function getPreviewHTML() {

    var html = '' +
        '<?xml version="1.0" encoding="utf-8"?> ' +
        '<!-- Generator: Adobe Illustrator 16.0.4, SVG Export Plug-In . SVG Version: 6.00 Build 0)  --> ' +
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"> ' +
        '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"' +
        'width="207px" height="423px" viewBox="0 0 600 1223" enable-background="new 0 0 600 1229.883" xml:space="preserve"> ' +
        '   <g id="Layer_1_1_"> ' +
            //'      <g id="Layer_3"> ' +
            //'          <rect x="1.137" y="1218.252" opacity="0.4" fill="#666666" stroke="#666666" stroke-miterlimit="10" enable-background="new    " width="597.971" height="8.771"/> ' +
            //'          <rect x="50.846" y="1171.08" opacity="0.4" fill="#999999" stroke="#666666" stroke-miterlimit="10" enable-background="new    " width="497.091" height="8.771"/> ' +
            //'          <rect x="545.014" y="37.025" opacity="0.7" fill="#999999" stroke="#666666" stroke-miterlimit="10" enable-background="new    " width="14.618" height="1185.475"/> ' +
            //'      </g> ' +
        '      <image overflow="visible" enable-background="new    " width="660" height="1500" xlink:href="/images/glass/backgroundGlass.png"  transform="matrix(1.0135 0 0 0.886 -4.6147 -0.1025)"> </image> ' +
            //'      <rect x="1.137" y="0.474" fill="#333333" stroke="#666666" stroke-miterlimit="10" width="4.385" height="1218.765"/> ' +
            //'      <rect x="594.722" y="0.474" fill="#333333" stroke="#666666" stroke-miterlimit="10" width="4.387" height="1218.765"/> ' +
        '   </g> ' +
        '   <rect id="left" x="5.5" y="0.5"  stroke="#000000" stroke-miterlimit="10" width="46" height="1223"/> ' +
        '   <rect id="bottom" x="51.5" y="1175.5"  stroke="#000000" stroke-miterlimit="10" width="497" height="47"/> ' +
        '   <rect id="right" x="548.5" y="0.5"  stroke="#000000" stroke-miterlimit="10" width="46" height="1223"/> ' +
        '   <rect id="top_1_" x="51.5" y="0.5"  stroke="#000000" stroke-miterlimit="10" width="497" height="47"/> ' +
        '</svg>';

    return html;
}


function getVinylColorHTML() {

    if (empty(colorChoicesByModule[formStatus.module].vinylColor)) {
        return '';
    }

    var vinylColors = colorChoicesByModule[formStatus.module].vinylColor.vinyl.colors;
    var location = 'vinyl';
    var vinylButtonColors = buildButtons(vinylColors, location);

    var html = '' +
        '<div id="vinylColorContent" class="content"> ' +
        '       <div id="chooseVinylFinish"> ' +
        '           <div class="title">Choose Vinyl Color</div> ' +
        '           <div class="sectionBlock"> ' +
        '               <div id="vinylOptions" class="finishOptions">' +
        '               ' + vinylButtonColors +
        '               </div> ' +
        '           </div> ' +
        '       </div> ' +
        '</div>';

    return html;
}
