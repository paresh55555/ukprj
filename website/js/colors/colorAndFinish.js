'use strict';

var colorChoices = {};
var superSectionsRAL = [];
var colorOptions = {};
var colorOptionsSpecial = {};


function loadColorAndFinish() {

    startSpinner();
    updateFormStatus();

    $.when(getColorOptions(), getSuperSections()).done(function (colors, superSections) {

        saveColorsForModule(colors[0]);

        var colorHTML = getColorHTML();
        buildAndAttachCorePage(colorHTML);

        //Robert Request this remove 6/19/2018
        // if (formStatus.panelOptions.swingInOrOut.selected === 'Inswing' &&
        //         (formStatus.extColor.kind !== 'woodStandard' && formStatus.extColor.kind !== 'woodPremium')) {
        //     hideIntWoodColors();
        // }

        selectTab('#color');

        //Select the type of finish or color group
        $('.finishMenuItem', '#extFinishGroup').unbind("click").bind("click", chooseFinish);
        $('.finishMenuItem', '#intFinishGroup').unbind("click").bind("click", chooseFinish);

        //Selects the sub group for RAL colors
        $('.superSectionButton').unbind("click").bind("click", chooseSuperSection);

        //Select the color
        $('.finishOptionsItem').unbind("click").bind("click", chooseColor);

        //Setup Yes/No Button
        $('.finishMenuItemQuestionYesNo').click(yesNoButtonPressed);


        $('#extColor-customColor').unbind("keyup").bind("keyup", customColorTextFormAction);
        $('#intColor-customColor').unbind("keyup").bind("keyup", customColorTextFormAction);



        restoreColorToForm();

        makeNextSectionButton(1);
        stopSpinner();
        showContentAndSaveState();
        updateFormPrice();

        addFooter();
        if (!isIE()) {
            $('.menu2').stickyfloat();
            $('.menu2').stickyfloat('update', {duration: 0});
        }

        scrollTopOfPage();
    });


}


function buildFinishGroup(colors, location) {

    var buttons = '';

    for (var property in colors) {
        var color = colors[property];
        color.location = location;
        color.kind = property;
        var button = buildFinishGroupButton(color);
        buttons = buttons + button;
    }

    return buttons;
}


function buildSelectedColorOptions(colors, location) {

    var kind = formStatus[location].kind;

    if (!empty(colors[kind])) {
        var colorGroup = colors[kind];
        var colorsButtons = buildButtons(colorGroup.colors);

        if (kind == 'ral' && !empty(colorGroup)) {
            var ralGroup = splitForThirdOption(formStatus[location].id);
            if (!empty(ralGroup)) {
                colorsButtons = buildButtons(colors[kind]['colors'][ralGroup]);
            }
        }

        return colorsButtons;
    }

    var colorsButtons = '';
    for (var property in colors) {
        var colorGroup = colors[property];
        colorGroup.kind = property;

        if (colorGroup.isDefault == 1) {
            colorsButtons = buildButtons(colorGroup.colors);
        }
    }

    return colorsButtons;
}


function getColorOptionsTitle(colors) {

    var title = '';

    for (var property in colors) {
        var colorGroup = colors[property];
        colorGroup.kind = property;
        if (empty(formStatus[colorGroup.location])) {
            if (colorGroup.isDefault == 1) {
                title =  colorGroup.nameOfOption;
                if (colorGroup.kind == 'custom') {
                    title = '';
                }
            }
        }
        if (formStatus[colorGroup.location].kind == colorGroup.kind) {
            title =  colorGroup.nameOfOption;
            if (colorGroup.kind == 'custom') {
                title = '';
            }
        }
    }

    return title;
}


function saveColorsForModule(colors) {

    var colorOptions = {};
    colors.modified = currentTimeInSeconds();


    colorChoicesByModule[formStatus.module] = colors;
    writeColorChoicesByModule();
}


function attachFinishWithFinishSector(selector) {

    var selection = $(selector).attr('id');

    formStatus['finishType' + extOrInt] = selection;

    var selectedFinish = $(selector).data('finish');
    var id = "[id='" + extOrInt + "OptionsTitle']";

    var extOrInt = splitForFirstOption(selection);
    var colorChoice = splitForSecondOption(selection);

    if (colorChoice == "ral") {
        attachSuperSectionRALWithLocation(extOrInt);
        $(id).text("RAL " + capitalizeFirstLetter(colorText) + " Finishes");

        var superSectionSelector = '#' + extOrInt + '-Yellow';
        var ralSection = 'ral' + extOrInt + 'Section';

        if (formStatus[ralSection]) {
            superSectionSelector = "#" + formStatus[ralSection];
        }
        chooseSuperSectionWithSelector(superSectionSelector);
    } else {
        if(colorChoice === 'custom' && extOrInt === 'extColor' && formStatus.panelOptions.swingInOrOut.selected === 'Inswing'){
            hideIntWoodColors();
        }
        removeSuperSectionRALWithLocation(extOrInt);
        var groupOfColors = colorChoicesByModule[formStatus.module][extOrInt][colorChoice]['colors'];
        attachColorsWithLocation(groupOfColors, extOrInt);

    }


    var title = buildColorChoiceTitle(selection);
    $('#' + extOrInt + 'OptionsTitle').text(title);
}


function buildColorChoiceTitle(selection) {

    var extOrInt = splitForFirstOption(selection);
    var colorChoice = splitForSecondOption(selection);


    var title = colorChoicesByModule[formStatus.module][extOrInt][colorChoice]['nameOfOption'];
    if (colorChoicesByModule[formStatus.module][extOrInt].kind == 'custom') {
        title = '';
    }

    return title;
}

function attachColorsWithLocation(colors, location) {

    var id = "#" + location + "Options";
    var buttons = buildButtons(colors);
    $(id).html('');
    $(id).append(buttons);
    $('.finishOptionsItem').unbind("click").bind("click", chooseColor);
}


function buildButtons(colors) {

    var buttons = '';
    for (var a = 0; a < colors.length; a++) {
        var color = colors[a];
        //color.location = location;
        var button = buildFinishButton(color);
        buttons = buttons + button;
    }

    return buttons;
}


function buildFinishButton(color) {

    var button = '';
    color.id = colorSelectionID(color);

    if (color.url) {
        button = buildFinishButtonImage(color);
    }
    if (color.hex) {
        button = buildFinishButtonHex(color);
    }

    return button;
}

function buildSelectedButton(color) {


    var selected = '';

    if (empty(formStatus[color.location])) {
        return '';
    }
    if (formStatus[color.location].id == color.id) {
        selected = '' +
            '<div class="colorSelectedDiv" > ' +
            '   <img src="images/icon-selected.svg" /> ' +
            '</div> ' ;
    }

    return selected;
}


function buildNameOfOption(color) {

    var ralInfo = '';
    if (color.ralColor) {
        ralInfo = '<div class=\"optionsItemRAL\">' + color.ralColor + '</div>';
    }

    var nameOfOption = '<div class="optionsItemText" >' + color.nameOfOption + '</div>';
    var colorStyle = ' style="background-color:#44ac61;color:#FFFFFF" ';

    if (!empty(formStatus[color.location])) {
        if (formStatus[color.location].id == color.id) {
            if (color.ralColor) {
                ralInfo = '<div class="optionsItemRAL" ' + colorStyle + '>' + color.ralColor + '</div>';
            }
            nameOfOption = '<div class="optionsItemText" ' + colorStyle + ' >' + color.nameOfOption + '</div>';
        }
    }
    return ralInfo + nameOfOption;
}


function buildFinishButtonImage(color) {

    var selected = buildSelectedButton(color);
    var nameOfOption = buildNameOfOption(color);

    var button = '' +
        '<div id="' + color.id + '" class="finishOptionsItem">' +
        '   <div class="optionsItemImage">' +
        '       <img src="' + color.url + '" >' +
        '   </div>' +
        nameOfOption +
            selected +
        '</div>';

    return button;
}


function buildFinishButtonHex(color) {

    var selected = buildSelectedButton(color);
    var nameOfOption = buildNameOfOption(color);

    var button =
        '<div id ="' + color.id + '" class="finishOptionsItem">' +
        '   <div class="optionsItemImage" style="background-color:' + color.hex + '"></div>' +
        nameOfOption +
        selected +
        '</div>';

    return button;
}


function isIE() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        return true;
    } else {
        return false;
    }
}


function afterHTMLTemplateIsLoaded() {
}


function buildFinishGroupOld(type, special) {

    var colors = colorOptions.colors;

    if (special == true) {
        colors = colorOptionsSpecial.colors;
    }
    var buttons = [];

    for (var a = 0; a < colors.length; a++) {
        var color = colors[a];
        color.location = location;
        color.type = type;
        var button = '';
        if (type == 'int' && color.forInt == 1) {
            button = buildFinishGroupButton(color);
        }
        if (type == 'ext' && color.forExt == 1) {
            button = buildFinishGroupButton(color);
        }
        buttons.push(button);
    }
    return buttons;
}


function buildFinishGroupButton(colorGroup) {

    var id = colorGroup.location + '-' + colorGroup.kind;

    var selected = '';

    if (colorGroup.isDefault == 1) {
        selected = 'selectedColorGroup';
    }

    if (formStatus[colorGroup.location]) {
        if (formStatus[colorGroup.location].kind == colorGroup.kind) {
            selected = 'selectedColorGroup';
        } else {
            selected = '';
        }
    }

    var button = '<div id="' + id + '" class="finishMenuItem ' + selected + '" data-finish="' + colorGroup.id + '">' + colorGroup.nameOfOption + '</div>';

    if (colorGroup.kind === "custom") {

        var myStyle = '';
        if (empty(selected)) {
            myStyle = ' style="display:none" ';
        }
        var colorName = formStatus[colorGroup.location].customName;

        if (empty(colorName)) {
            colorName = '';
        }

        var customColorDescription = '* Enter a reference name for your custom ' + colorText + '. Please note that we will need a ' + colorText + ' chip sent to us to do the ' + colorText + ' match. Custom ' + colorText + 's require an additional 4 weeks.';

        button = '' +
            '<div id="' + colorGroup.location + '-customColorBox" >' +
            '   ' + button +
            '   <input class="textBox" ' + myStyle + ' type="text" id="' + colorGroup.location + '-customColor" placeholder="Please enter ' + colorText + ' name"  value="' + colorName+ '"/> ' +
            '   <div id="' + colorGroup.location + '-customDescription" ' + myStyle + ' >' + customColorDescription + '</div>' +
            '</div>';
    }

    return button;
}


function chooseVinylType() {

    formStatus.vinylType = $(this).attr('id');
    makeButtonSelection(this);
    writeFormStatus();
}


function customColorButtonSelected(selector) {

    var buttonCheck = $(selector).attr('id');
    var extOrInt = splitForFirstOption(buttonCheck);

    if (extOrInt === 'extColor') {
        if (buttonCheck == "extColor-custom") {
            if(!formStatus.extColor){
                formStatus.extColor = {};
            }
            formStatus.extColor.buttonID = "custom";
            formStatus.extColor.type = "custom";
            formStatus.extColor.kind = "custom";
            selector = $(selector).parent();

            var id = buildIDSelector('extColor-customColor');
            $('#extColor-customColor').show();
            $('#extColor-customDescription').show();
            customColorText(id);
            $('#'+extOrInt+'OptionsTitle').html('');

        } else {
            $('#extColor-custom').css({"background-color": "#FFFFFF", "color": "#000000", "opacity": "1.0"});
            $('#extColor-customColor').hide();
            $('#extColor-customDescription').hide();
        }
    }

    if (extOrInt === 'intColor') {
        if (buttonCheck == "intColor-custom") {
            if(!formStatus.intColor){
                formStatus.intColor = {};
            }
            formStatus.intColor.buttonID = "custom";
            formStatus.intColor.type = "custom";
            formStatus.intColor.kind = "custom";
            selector = $(selector).parent();
            $('#intColor-customColor').show();
            $('#intColor-customDescription').show();
            var id = buildIDSelector('intColor-customColor');
            customColorText(id);
            $('#'+extOrInt+'OptionsTitle').html('');
        } else {
            $('#intColor-custom').css({"background-color": "#FFFFFF", "color": "#000000", "opacity": "1.0"});
            $('#intColor-customColor').hide();
            $('#intColor-customDescription').hide();
        }
    }



    writeFormStatus();
    updateFormPrice();

    return (selector);


}

function makeButtonSelection(selector) {

    $(selector).css({"background-color": "#43A85F", "opacity": "1.0", "color": "#ffffff"});
    var newSelector = customColorButtonSelected(selector);
    $(newSelector).siblings().css({"background-color": "#FFFFFF", "color": "#000000", "opacity": "1.0"});
}


function restoreColorToForm() {

    if (!empty(formStatus.extColor)) {
        markColorSelected('extColor');
    }

    if (formStatus.intNotSameExt) {
        markColorSelected('intColor');
    }
}


function markColorSelected(name) {

    var id = formStatus[name].id;
    if (empty(id)) {
        return;
    }
    var options = id.split('-');
    var location = options[0];
    var colorName1 = options[3];
    var colorHex = options[5];

    updatePreview(colorHex, location, colorName1);
}


function checkColorStatus() {

    var choosenFinishTypeID;
    var selectedID;

    var vinylType = formStatus.vinylType;

    var extColor = formStatus.extColor;
    var intColor = formStatus.intColor;

    var extType = formStatus.finishTypeext;
    var intType = formStatus.finishTypeint;
    var intNotSameExt = formStatus.intNotSameExt;


    if (extType === undefined) {
        extType = "ext-basic";
    }
    if (intType === undefined) {
        intType = "int-basic";
    }

    if (vinylType) {
        makeButtonSelection($("[id='" + vinylType + "']"));
    } else {
        makeButtonSelection($("[id='vinyl-White Vinyl']"));
    }

    if (formStatus.paintedFinish) {
        paintedStatusChoice(formStatus.paintedFinish);
    }

    if (!empty(intNotSameExt)) {
        makeButtonSelection(buildIDSelector('vinyl-0-noExt'));
        $('#intColorContent').show();
        $('#chooseIntFinish').show();
    }

    if (extColor) {
        if (extColor.type === 'custom' && extColor.customName != "undefined") {
            $('#ext-customColor').val(extColor.customName);
        }
        else {
            $('#ext-customColor').val('');
        }

        choosenFinishTypeID = buildIDSelector('ext-' + extColor.type);
        chooseFinishWithSelector(choosenFinishTypeID);

        extColor.location = 'ext';

        updatePreview(extColor.hex, extColor.location, extColor.name1);

        var selectedID = storeColorSelectionID(extColor);
        displayColorSelected(buildIDSelector(selectedID));

    } else {

        if (isUK() && formStatus.module != '1') {
            chooseFinishWithSelector('#ext-basicUK');
        } else {

            var optionTypeFirst = getDefaultColorOptionForType('ext');
            chooseFinishWithSelector('#ext-' + optionTypeFirst);

        }

    }


    if (intColor) {
        if (intColor.type === 'custom' && intColor.customName != "undefined") {
            $('#int-customColor').val(intColor.customName);
        }
        choosenFinishTypeID = $('#int-' + intColor.type);
        chooseFinishWithSelector(choosenFinishTypeID);
        selectedID = "[id='int-" + intColor.type + "-" + intColor.name + "']";
        intColor.location = 'int';
        var selectedID = storeColorSelectionID(intColor);
        displayColorSelected(buildIDSelector(selectedID));

        updatePreview(intColor.hex, intColor.location, intColor.name1);

    } else {
        if (isUK()) {
            chooseFinishWithSelector('#int-basicUK');
        } else {
            var optionTypeFirst = getDefaultColorOptionForType('int');
            chooseFinishWithSelector('#int-' + optionTypeFirst);
        }
    }

//    else if (intType) {
//
//        var choosenFinishTypeID = $("[id='" + intType + "']");
//        chooseFinishWithSelector(choosenFinishTypeID);
//    }

    updateFormPrice();


}

function getDefaultColorOptionForType(type) {

    var colors = colorOptions.colors;

    for (var a = 0; a < colors.length; a++) {
        var color = colors[a];
        if (color[type + 'Default'] == 1) {
            return color.kind;
        }
    }

    return colors[0].kind;

}


function buildIDSelector(id) {

    var selector = "[id='" + id + "']";
    return selector;

}
function storeColorSelectionID(color) {

    var selectedID = color.location + '-' + color.type + '-' + color.subSectionID + '-' + color.name1 + '-' + color.name2 + '-' + color.hex + '-' + color.url;

    return (selectedID);
}


function setButtonOpacityAndSiblings(selector) {
    selector.css({opacity: 1.0});
    selector.siblings().css({opacity: 0.5});
}


function chooseFinishWithSelector(selector) {

    attachFinishWithFinishSector(selector);
    makeButtonSelection(selector);

}

function chooseFinish() {

    var selection = this;
    chooseFinishWithSelector(selection);
}


function chooseIfColor() {

    var id = $(this).attr('id');

    formStatus.paintedFinish = id;
    writeFormStatus();

    var idBreakDownArray = id.split('-');
    displayExtIntColorChoices(idBreakDownArray[2]);
    chooseFinishWithSelector(this);

}


function paintedStatusButtonPress() {

    paintedStatusChoice($(this).attr('id'));
    updateFormPrice();

}


function paintedChoiceFormUpdate(id) {

    if (id) {
        formStatus.paintedFinish = id;
        writeFormStatus();
    }

}


function paintedStatusChoice(id) {

    paintedChoiceFormUpdate(id);
    updateQuestionsStatus(id);

}


function yesNoButtonPressed() {

    var id = $(this).attr('id');

    updateQuestionsStatus(id);
    updateFormPrice();
}


function updateQuestionsStatus(id) {

    if (id == 'yes') {
        $('#intColorContent').hide();
        $('#yes').addClass('selectedColorGroup');
        $('#no').removeClass('selectedColorGroup');
        formStatus.intNotSameExt = false;
        formStatus.intColor = formStatus.extColor;

    } else {
        $('#intColorContent').show();
        $('#yes').removeClass('selectedColorGroup');
        $('#no').addClass('selectedColorGroup');
        formStatus.intNotSameExt = true;
        formStatus.intColor = "";
    }

    writeFormStatus();
}

function displayExtIntColorChoices(choice) {


    if (choice === "exterior") {
        $('#chooseExtFinish').show();
        $('#chooseIntFinish').hide();
        $('#sameAsExterior').show();
        $('#extColorContent').show();
        $('#intColorContent').hide();


    } else if (choice === "interior") {
        $('#chooseExtFinish').hide();
        $('#chooseIntFinish').show();
        $('#sameAsExterior').hide();
        $('#extColorContent').hide();
        $('#intColorContent').show();

    } else if (choice === "both") {
        $('#chooseExtFinish').show();
        $('#sameAsExterior').show();
        $('#extColorContent').show();

        chooseFinishWithSelector('#vinyl-0-yesExt');
        $('#chooseIntFinish').hide();
        $('#intColorContent').hide();
        formStatus.intColor = formStatus.extColor;
        writeFormStatus();

    }
    else if (choice === "noExt") {
        $('#chooseIntFinish').show();
        $('#intColorContent').show();
        formStatus.intNotSameExt = true;
        writeFormStatus();
    }
    else if (choice === "yesExt") {
        $('#chooseIntFinish').hide();
        $('#intColorContent').hide();
        $('#intPreviewImage').hide();
        formStatus.intNotSameExt = false;
        formStatus.intColor = formStatus.extColor;
        writeFormStatus();
    }
    else {

        $('#extColorContent').hide();
        $('#intColorContent').hide();
        $('#chooseExtFinish').hide();
        $('#chooseIntFinish').hide();
        $('#sameAsExterior').hide();
    }


}

function chooseSuperSection(colors) {
    chooseSuperSectionWithSelector(this);
}

function chooseSuperSectionWithSelector(selector) {

    $(selector).siblings().css({opacity: 0.5});
    $(selector).css({opacity: 1.0});

    var colorChoice = $(selector).attr('id');
    var choiceArray = colorChoice.split("-");
    var extOrInt = choiceArray[0];
    var superSection = choiceArray[1];

    formStatus.ralextSection = colorChoice;

    var colors = colorChoicesByModule[formStatus.module][extOrInt].ral.colors[superSection];
    attachColorsWithLocation(colors, extOrInt);
    writeFormStatus();
}


function customColorTextFormAction() {

    customColorText(this)
    updateFormPrice();
}


function customColorText(selector) {

    var extOrIntArray = $(selector).attr('id').split('-');
    var name = $(selector).val();
    if (empty(name)) {
        name = '';
    }
    if (extOrIntArray[0] === 'extColor') {
        formStatus.extColor = {};
        formStatus.extColor.buttonID = "custom";
        formStatus.extColor.type = "custom";
        formStatus.extColor.name1 = undefined;
        formStatus.extColor.name2 = undefined;
        formStatus.extColor.customName = name;
        formStatus.extColor.kind = "custom";
    }
    if (extOrIntArray[0] === 'intColor' || !formStatus.intNotSameExt) {
        formStatus.intColor = {};
        formStatus.intColor.buttonID = "custom";
        formStatus.intColor.type = "custom";
        formStatus.intColor.name1 = undefined;
        formStatus.intColor.name2 = undefined;
        formStatus.intColor.customName = name;
        formStatus.intColor.kind = "custom";
    }

    writeFormStatus();
}


function removeSuperSectionRALWithLocation(location) {

    var id = "#" + location + "SuperSectionRAL";
    $(id).html('');
}


function removeExtSuperSectionRAL() {

    var id = "#extColorSuperSectionRAL";
    $(id).html('');
}

function removeIntSuperSectionRAL() {

    var id = "#intColorSuperSectionRAL";
    $(id).html('');

}

function removeSuperSectionRAL(id) {
    $(id).html('');

}


function attachSuperSectionRALWithLocation(location) {

    var id = "#" + location + "SuperSectionRAL";
    var buttons = buildSuperSectionButtons(superSectionsRAL, location);

    removeSuperSectionRAL(id);

    $(id).append(buttons);
    $('.superSectionButton').unbind("click").bind("click", chooseSuperSection);
}


function attachIntSuperSectionRAL(colors) {

    var id = "#intSuperSectionRAL";
    var buttons = buildSuperSectionButtons(superSectionsRAL, "int");
    removeIntSuperSectionRAL();
    $(id).append(buttons);
    $('.superSectionButton').unbind("click").bind("click", chooseSuperSection);
}


function buildSuperSectionButtons(colors, location) {

    var buttons = [];
    for (var a = 0; a < colors.length; a++) {
        var color = colors[a]
        color.location = location;
        var button = buildRALButton(color);
        buttons = buttons + button;
    }

    return buttons;
}


function colorSelectionID(color) {

    var id = color.location + '-' + color.kind + '-' + color.superSection + '-' + color.nameOfOption + '-' + color.ralColor + '-' + color.hex + '-' + color.url;

    return id;
}


function buildRALButton(superSection) {

    var button = '' +
        '<div id="' + superSection.location + '-' + superSection.name + '"class="superSectionButton">' +
        '   <div class="superSectionImage"><img src="' + superSection.url + '" ></div>' +
        '   <div class="superSectionText">' + superSection.name + '</div>' +
        '</div>';

    return (button);
}


function getColorOptions() {

    var module = formStatus.module;

    var url = "https://" + apiHostV1 + "/api/colors?module=" + module + authorize() + addSite();

    var colorChoiceOptions = $.getJSON(url, function (apiJSON) {
        validate(apiJSON);
    });

    return colorChoiceOptions;
}


function getColorOptionsSpecial() {

    var module = formStatus.module;

    var url = "https://" + apiHostV1 + "/api/colors?module=" + module + authorize() + addSite();
    var colorChoiceOptions = $.getJSON(url, function (apiJSON) {
        validate(apiJSON);
        colorOptionsSpecial = apiJSON;
    });

    return colorChoiceOptions;
}


function getColorChoices(type) {

    var url = "https://" + apiHostV1 + "/api/finishes?kind=" + type + authorize() + addSite();

    var colorChoicePromise = $.getJSON(url, function (apiJSON) {
        validate(apiJSON);

        if (type == "ral") {
            colorChoices[type] = apiJSON;
        }
        else {
            colorChoices[type] = apiJSON.all
        }
    });

    return colorChoicePromise;
}

function getSuperSections() {

    var url = "https://" + apiHostV1 + "/api/finishes?kind=superSectionRAL" + authorize() + addSite();
    var superSectionsPromise = $.getJSON(url, function (apiJSON) {
        validate(apiJSON);

        superSectionsRAL = apiJSON;
    });

    return superSectionsPromise;
}


function updatePreview(colorHex, location, colorName1) {

    if ('null' != colorHex) {
        if (location == 'extColor') {
            $('#extPreviewImage').show();
            $('#extPreviewImage').css({"color": colorHex});
            $('#extPreviewImagePng').hide();
        }
        if (location == 'intColor') {
            $('#intPreviewImage').show();
            $('#intPreviewImage').css({"color": colorHex});
            $('#intPreviewImagePng').hide();
        }
    }

    if ('null' == colorHex || colorHex == undefined) {
        if (location == 'extColor') {
            $('#extPreviewImage').hide();
            var html = '<img src="/images/finishPreview/' + colorName1 + '.png" width="207" height="437" >';
            $('#extPreviewImagePng').html(html);
            $('#extPreviewImagePng').show();
        }

        if (location == 'intColor') {
            $('#intPreviewImage').hide();
            var html = '<img src="/images/finishPreview/' + colorName1 + '.png" width="207" height="437" >';
            $('#intPreviewImagePng').html(html);
            $('#intPreviewImagePng').show();
        }
    }
}


function chooseColor() {

    var selector = $(this).attr('id');
    var options = selector.split('-');
    var location = options[0];
    var kind = options[1];
    var superSection = options[2];
    var colorName1 = options[3];
    var colorName2 = options[4];
    var colorHex = options[5];
    var colorURL = options[6];

    updatePreview(colorHex, location, colorName1);

    var nextSection;
    var extOrInt;

    //if (location == 'ext') {
    //    extOrInt = 'extColor'
    //    $('#extFinishPreview').css({"color": colorHex});
    //    nextSection = $('#chooseIntFinish');
    //
    //}
    //if (location == 'int') {
    //    extOrInt = "intColor";
    //}
    //
    if (location == 'trickle') {
        formStatus.trickleColor = colorName1;
    }

    formStatus[location] = {
        id: selector,
        kind: kind, name1: colorName1, name2: colorName2,
        hex: colorHex, buttonID: location + '-' + kind, subSectionID: superSection, url: colorURL
    };

    if (!formStatus.intNotSameExt && location != 'trickle') {
        formStatus['intColor'] = {
            id: selector,
            kind: kind, name1: colorName1, name2: colorName2,
            hex: colorHex, buttonID: location + '-' + kind, subSectionID: superSection, url: colorURL
        };
    }

    writeFormStatus();

    var groupOfButtons = $(this).parent();
    var selectedDiv = $(".colorSelectedDiv", groupOfButtons);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
        $('.optionsItemText', groupOfButtons).css({"background-color": "#FFFFFF", "color": "#000000"});
        $('.optionsItemRAL', groupOfButtons).css({"background-color": "#FFFFFF", "color": "#000000"});
    }

    var extColor = formStatus.extColor;

    //Robert Requested this removed 6/19/2018
    // if(formStatus.panelOptions.swingInOrOut.selected === 'Inswing' && extColor &&
    //     (extColor.kind !== 'woodStandard' && extColor.kind !== 'woodPremium')){
    //     hideIntWoodColors();
    // }else{
        $('#intColor-woodStandard').show();
        $('#intColor-woodPremium').show();
    // }

    displayColorSelected(this);
    updateFormPrice();

    lockDownTabs();
}

function hideIntWoodColors(){
    $('#intColor-woodStandard').hide();
    $('#intColor-woodPremium').hide();
    var intColor = formStatus.intColor,
        extColor = formStatus.extColor;
    if(formStatus.intNotSameExt && intColor &&
        ((intColor.kind === 'woodStandard' || intColor.kind === 'woodPremium'))){
        formStatus.intColor = '';
        $('#intColorOptions').empty();
        $('#intColorOptionsTitle').empty();
        $('#intColor-basic').click();
        writeFormStatus();
    }
}


function displayColorSelected(selector) {

    jQuery('<div/>', {
        class: 'colorSelectedDiv'
    }).appendTo($(selector));


    jQuery('<img/>', {
        src: 'images/icon-selected.svg'
    }).appendTo($(".colorSelectedDiv", selector));

    $('.optionsItemText', selector).css({"background-color": "#44ac61", "color": "#FFFFFF"});

    var RALSector = $('.optionsItemRAL', selector);
    if (RALSector.length > 0) {
        RALSector.css({"background-color": "#44ac61", "color": "#FFFFFF"});
    }

}
