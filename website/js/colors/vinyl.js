'use strict'


function vinylColorHTML() {

    var html = '' +
        '<div id="dynamicContent"> ' +
        '   <div id="chooseVinyl"> ' +
        '       <div class="title">Choose Vinyl Color</div> ' +
        '       <div class="sectionBlock"> ' +
        '           <div id="vinylType" class="finishMenu"> ' +
        '               <div id="vinyl-White Vinyl" class="finishMenuItem vinylColorsWidth" data-finish="white">White Vinyl</div> ' +
        '               <div id="vinyl-Beige Vinyl" class="finishMenuItem vinylColorsWidth" data-finish="beige">Beige Vinyl</div> ' +
        '               <div id="vinyl-Clay Vinyl" class="finishMenuItem vinylColorsWidth" data-finish="clay">Clay Vinyl</div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div>' +
        '   <div id="nextSection"> </div> ' +
        '   <div class="spacer"></div> ' +
        '   <div id="copyWriteFooter"></div> ' +
        '   <div class="spacer"></div>' +
        '</div>';

    return html;
}