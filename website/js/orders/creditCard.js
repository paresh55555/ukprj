function payWithCreditCard() {

    if (!hasCart()) {
        alert("Please add at least one door to your order");
        return;
    }

    if (!hasCustomer()) {
        return;
    }

    if (!document.getElementById('termsChecked').checked) {
        alert("Please check that you agree to the terms and conditions.");
        return;
    }

    
    ccValues = {};
    ccValues.company = quote.Customer.companyName;
    ccValues.ccName = $("#ccName").val();
    ccValues.ccNumber = $("#ccNumber").val();
    ccValues.cardCode = $("#ccCVV").val();
    ccValues.ccMonth = $("#ccMonth").val();
    ccValues.ccYear = $("#ccYear").val();
    ccValues.ccZip = $("#ccZip").val();
    ccValues.ccAmount = quote.total;
    ccValues.id = quote.id;
    ccValues.salesPerson = quote.SalesPerson;
    ccValues.billingAddress1 = $("#billingAddress1").val();
    ccValues.billingAddress2 = $("#billingAddress2").val();
    ccValues.billingCity = $("#billingCity").val();
    ccValues.billingState = $("#billingState").val();
    ccValues.email = $("#email").val();
    ccValues.phone = $("#phone").val();
    ccValues.contact = $("#contact").val();
    ccValues.token = user.token;
    ccValues.site = site;


    if (siteDefaults.takesCreditCards == 2) {
        ccValues.ccAmount = 1;
    }

    if (!checkCCValues(ccValues)) {
        return;
    }

    var message = "Confirm to purchase this door for " + currency + formatMoney(ccValues.ccAmount);
    if (numberOfItemInCart(quote.Cart) > 1) {
        message = "Confirm to purchase these doors for " + currency + formatMoney(ccValues.ccAmount);
    }

    if (confirm(message) == true) {
        startSpinner();
        purchaseDoor(ccValues).done(function (result) {
            stopSpinner();
            var response = jQuery.parseJSON(result);

            if (response.status == "Success") {
                alert("Your purchase was successfull. \nClick the Print Order button the following page for a copy of your paid Invoice ");
                trackAndDoClick("viewOrder", "&order=" + quote.id);
            } else {
                alert("Please check your credit card information.\n Your purchase was not authorized");
                //}     //if (response.status == "Success") {
                //    printSalesOrder(id,debug);
                //} else {
                //    alert("Order Failed to Print");
            }

        });
    }
}


function checkCCValues(ccValues) {

    if (empty(ccValues.ccName)) {
        alert("Please enter the name on the credit card");
        return false;
    }


    if (empty(ccValues.ccNumber)) {
        alert("Please enter your credit card number");
        return false;
    }

    if (empty(ccValues.cardCode)) {
        alert("Please enter your security code");
        return false;
    }

    if (empty(ccValues.ccZip)) {
        alert("Please enter your billing " + zipShort);
        return false;
    }

    return true;
}