'use strict';


function printInvoice() {

    getInvoice().done(function (invoiceData) {

        quote = invoiceData;
        buildInvoicesOrdersQuotes();
        removeNonPrintInfo();
        showContentAndSaveState();
    })
}


function buildInvoicesOrdersQuotes() {

    var cartRow = 1;
    var totalItemsInCart = getTotalItemsInCart(quote.Cart);
    var cartMainContent = $('#mainContent');
    var collection = getParameter('collection');
    cartMainContent.html('');

    var invoicesHTML = '';

    var title = 'INVOICE';
    if (quote.type == 'Confirming Payment') {
        title = 'ORDER';
    }
    if (quote.type == 'quote') {
        title = 'QUOTE';
    }
    if (collection == 'yes' && siteDefaults.salesFlow == 2) {
        title = 'COLLECTION NOTE';
    }
    if (collection == 'yes' && (siteDefaults.salesFlow == 1 || siteDefaults.salesFlow == 3) ) {
        title = 'DELIVERY NOTE';
    }

    for (var property in quote.Cart) {

        if (empty(quote.Cart[property].quantity)) {
            quote.Cart[property].quantity = 1
        }
        var quantity = quote.Cart[property].quantity;

        for (var a = 0; a < quantity; a++) {
            quote.Cart[property].index = cartRow;
            quote.Cart[property].totalItems = totalItemsInCart;
            quote.Cart[property].count = a;
            quote.Cart[property].invoiceTitle = title;
            quote.Cart[property].invoiceDate = quote.orderDate;
            quote.Cart[property].customerRef = quote.Customer.customerREF;
            quote.Cart[property].customerPO = quote.Customer.customerPO;
            quote.Cart[property].quoteID = quote.prefix + quote.id;
            quote.Cart[property].customerName = returnBlankIfEmpty(quote.Customer.firstName) + ' ' + returnBlankIfEmpty(quote.Customer.LastName);
            quote.Cart[property].companyName = quote.Customer.companyName
            var html = addInvoiceRowHtmlWithRowNumber(quote.Cart[property]);

            cartMainContent.append(html);
            cartRow = cartRow + 1;
        }
    }
}


function buildInvoiceRowHeader(row) {

    var secondTitle = '';
    if (row.vinylColor) {
        secondTitle = ' -- ' + row.vinylColor.name1 + ' Vinyl';
    }

    var html = '' +
        '<div class="cartRowInvoiceHeaderTitle">' + row.moduleTitle + secondTitle +' </div>' ;

    return html;
}


function buildInvoiceSeparator(row) {

    var html = '' +
        '<div class="cartRowInvoiceSeparator"></div>';

    return html;
}


function buildInvoiceFinishOptionsColumn(row) {

    var extra = '<div class="cartInvoiceRowTitleFinish">Finish Options</div>';
    var finishOptionsColumn = buildFinishOptionsColumn(row, extra);

    return finishOptionsColumn;
}


function getItemCost(row) {

    if (isProduction()) {
        return '';
    }

    var newTotal = markupTotal(Number(row.total));

    var html = '' +
        '<div class="invoiceTotalCost">Item Cost: ' + currency + formatMoney(newTotal, 2, '.', ',') + '</div>';


    return html;
}


function invoiceTermsOfConditions() {

    var html = '<div class="invoiceTerms">*All orders are subject to ' + siteDefaults.companyName + ' Terms and Conditions</div>';

    if (isProduction()) {

        var companyLine = '' +
            '   <div class="signatureRight signatureUnderLine"></div>  ' +
            '   <div class="signatureLeft">Company Representative</div>' +
            '   <div class="signatureRight">Date</div>  ';

        var packsDelivered = '';

        if (isUK()) {
            companyLine =  '<div class="signatureLeft">Print Name</div>' ;
            packsDelivered = '<div class="invoiceTerms">Number of Packs Delivered</div><div class="packsBox"></div>'
        }

        html = '' +
            '<div class="signatureBox"> ' +
            packsDelivered +
            '<div class="invoiceTerms">By signing this collection note, you are confirming that the item\'s size and options are as ordered and that the product is undamaged.</div>' +
            '   <div class="signatureLeft signatureUnderLine"></div>' +
            '   <div class="signatureRight signatureUnderLine"></div>  ' +
            '   <div class="signatureLeft">Customer Signature</div>' +
            '   <div class="signatureRight">Date</div>  ' +
            '   <div class="signatureLeft signatureUnderLine"></div>' +
            companyLine +
            '</div> ' ;
    }


    var collection = getParameter('collection');
    if (collection == 'yes' && (siteDefaults.salesFlow == 1 ) ) {
        html = '';
    }


    return html;
}


function addInvoiceRowHtmlWithRowNumber(row) {

    row = convertForVinyl(row);

    var rowIndex = row.totalItems;

    var bgColor = setBackgroundRowColor(row.index);

    var headerRow = buildInvoiceRowHeader(row);
    var panelsRow = buildRowPanels(row);
    var titlesRow = buildInvoiceSeparator(row);

    var finishOptionsColumn = buildInvoiceFinishOptionsColumn(row);
    var descriptionColumn = buildInvoiceDescriptionColumn(row);
    var glassSizeHMTL = glassSize(row);

    var itemCostHTML = getItemCost(row);
    var terms = invoiceTermsOfConditions();

    var fullPageTop = '<div class="fullPage">';
    var fullPageBottom = '</div>';

    var invoiceHeaderHTML = invoiceHeader(row);

    var html = '' +
        fullPageTop +
        invoiceHeaderHTML +
        headerRow +
        panelsRow +
        titlesRow +
        '<div class="invoiceSpecifics">' +
        finishOptionsColumn +
        descriptionColumn +
        glassSizeHMTL +
        itemCostHTML +
        terms +
        '</div>' +
        fullPageBottom;

    return html;
}

function glassSize(row) {

    var inHTML = '';
    var inValuesHTML = '';

    if (!empty(row.glassWidthIn) && !empty(row.glassHeightIn)) {
        inHTML = '' +
            '<div class="invoiceGlassColumnTitle">Width Inches</div>' +
            '<div class="invoiceGlassColumnTitle">Height Inches</div>';

        inValuesHTML = '' +
            '   <div class="invoiceGlassColumn">' + row.glassWidthIn + ' in.</div>' +
            '   <div class="invoiceGlassColumn">' + row.glassHeightIn + ' in.</div>';
    }

    var glassLimitTextHTML = '';
    if (isMagnaline()) {
        glassLimitTextHTML = '<div class="quoteSheetLimit">' +  glassOpeningLimitText() +'</div>'
    }

     var html = '' +
        '<div class="invoiceGlassTitle">Glass Size: </div>' +
        glassLimitTextHTML +
        '<div class="invoiceGlass">' +
        '   ' + inHTML +
        '   <div class="invoiceGlassColumnTitle">Width MM</div>' +
        '   <div class="invoiceGlassColumnTitle">Height MM</div>' +
        '   <div class="invoiceGlassColumnTitleThin invoiceGlassColumnLast">Qty</div>' +
        '</div>' +
        '<div class="invoiceGlass">' +
        '   ' + inValuesHTML +
        '   <div class="invoiceGlassColumn">' + row.glassWidth + ' MM</div>' +
        '   <div class="invoiceGlassColumn">' + row.glassHeight + ' MM</div>' +
        '   <div class="invoiceGlassColumnThin invoiceGlassColumnLast">' + row.numberOfPanels+ '</div>' +
        '</div>';


    //Alan wish this back on for quote as well -- 2/12/16
    //if (row.invoiceTitle != 'INVOICE' && row.invoiceTitle != 'COLLECTION NOTE' ) {
    //    html = '';
    //}

    if (siteDefaults.salesFlow == '1' ) {
        html = '';
    }
    if (isUK()) {
        html ='';
    }

    return html;
}


function invoiceHeader(row) {


    var companyAddressHTML = invoiceCompanyAddress();
    var customerAddressHTML = invoiceCustomerAddress();
    var invoiceInfoHTML = invoiceInfo(row);

    var html = '' +
        '<div class="invoiceLeft">' +
        '   ' + companyAddressHTML +
        '   ' + customerAddressHTML +
        '</div>' +
        '<div class="invoiceRight">' +
        '' +
        '   ' + invoiceInfoHTML +
        '</div>';


    return html;

}

function removeNonPrintInfo() {
    $('#companyHeader').hide();
    $('#preMainContent').hide();

    $('#mainContent').css({'margin-top': '50px'});
    $('#headerNav').hide();
    $('#salesNav').hide();
    $('.cartRow').css({'margin-bottom': '323px'});
    $('#footer').hide();
}


function invoiceCompanyAddress() {

    var address2 = '';

    if (!empty(siteDefaults.address2)) {
        address2 = '<div class="invoiceCompanyInfo">' + siteDefaults.address2 + '</div>';
    }

    var address = '' +
        '   <img id="invoicePrintLogo" src="' + siteDefaults.printLogoURL + '"> ' +
        '   <div class="invoiceCompanyInfo">' + siteDefaults.address1 + '</div>' +
        '   ' + address2 +
        '   <div class="invoiceCompanyInfo">' + siteDefaults.city + ', ' + siteDefaults.state + ' ' + siteDefaults.zip + '</div>' +
        '   <div class="invoiceCompanyInfo">' + siteDefaults.phone + '</div>';


    return address;
}


function invoiceCustomerAddress() {

    var address = quote.address;

    if(!address) return '';

    var address2 = '';

    if (!empty(address.shipping_address2)) {
        address2 = '<div class="invoiceCustomerInfo">' + returnBlankIfEmpty(address.shipping_address2) + '</div>';
    }

    var customerTitle = 'Customer Information';
    if (siteDefaults.salesFlow == 1 && getParameter('collection') == 'yes') {
        customerTitle = 'Delivery Address';
    }

    var address = '' +
        '   <div class="invoiceCustomerInfoTitle">' + customerTitle + '</div>' +
        '   <div class="invoiceCustomerInfo">' + returnBlankIfEmpty(quote.Customer.companyName) + '</div>' +
        '   <div class="invoiceCustomerInfo">' + returnBlankIfEmpty(address.shipping_address1) + '</div>' +
        '   ' + address2 +
        '   <div class="invoiceCustomerInfo">' + returnBlankIfEmpty(address.shipping_city) + ', ' + returnBlankIfEmpty(address.shipping_state) + ' ' + returnBlankIfEmpty(address.shipping_zip) + '</div>' +
        '   <div class="invoiceCustomerInfo">' + returnBlankIfEmpty(address.phone) + '</div>';

    return address;
}


function calculateInvoiceDueDate() {

    var largestTotal = 0;
    for (var property in quote.Cart) {
        var row = quote.Cart[property]
        var addDays = 10;
        var bonusDays = 0;

        if (row.module == "21") {
            addDays = Number(addDays) + 10;
        }

        if (row.extColor.kind == 'custom' || row.intColor.kind == 'custom') {
            bonusDays = 20;
        } else if (row.extColor.kind == 'ral' || row.extColor.kind == 'arch' || row.extColor.kind == 'woodPremium' || row.extColor.kind == 'woodStandard') {
            bonusDays = 10;
        } else if (row.intColor.kind == 'ral' || row.intColor.kind == 'arch' || row.intColor.kind == 'woodPremium' || row.intColor.kind == 'woodStandard') {
            bonusDays = 10;
        } else if (row.extColor.name1 != row.intColor.name1) {
            bonusDays = 10;
        }

        var tempTotal = Number(addDays) + Number(bonusDays);

        if (tempTotal > largestTotal) {
            largestTotal = tempTotal;
        }

    }

    var newDate = add_business_days(quote.orderDate, largestTotal);

    return newDate;
}

function invoiceInfo(row) {

    var invoiceNumber = '&nbsp;';
    var customerRef = '&nbsp;';
    var customerPO = '&nbsp;';

    if (!empty(row.invoiceNumbers)) {
        invoiceNumber = row.invoiceNumbers[row.count];
    }
    if (!empty(quote.po)) {
        customerPO = quote.po;
    }
    if (!empty(row.customerRef) ) {
        customerRef = row.customerRef;
    }

    var paidInFullHTML = '';

    if (quote.paidInFull == 'yes') {
        paidInFullHTML = '<div class="paidInFull "><img src="/images/icons/paid.svg" alt="Paid"></div>';
    }

    var invoiceHTML = '';
    if (row.invoiceTitle == 'INVOICE'  || row.invoiceTitle == 'COLLECTION NOTE') {
        var invoiceDueDate = calculateInvoiceDueDate(row);
        invoiceHTML = '' +
            '<div class="invoiceInfoRight invoiceClear">' + row.invoiceDate + '</div><div class="invoiceInfoLeft ">Invoice Date:</div>' +
            '<div class="invoiceInfoRight invoiceClear">' + invoiceDueDate + '</div><div class="invoiceInfoLeft ">Will Be Completed By:</div>' +
            '<div class="invoiceInfoRight invoiceClear">' + invoiceNumber + '</div>' +
            '<div class="invoiceInfoLeft invoice ">' + paidInFullHTML + 'Invoice #:</div>' +
            '';
    }

    if (row.invoiceTitle == 'ORDER'  ) {
        invoiceHTML = '' +
            '<div class="invoiceInfoRight invoiceClear">Pending Payment</div><div class="invoiceInfoLeft ">Invoice Date:</div>' +
            '<div class="invoiceInfoRight invoiceClear">Pending Payment</div><div class="invoiceInfoLeft ">Will Be Completed By:</div>' +
            '<div class="invoiceInfoRight invoiceClear">Pending Payment</div><div class="invoiceInfoLeft ">Invoice #:</div>';
    }

    var customerHTML = ''+
        '<div class="invoiceInfoRight invoiceClear">' + row.quoteID + '</div><div class="invoiceInfoLeft ">Job #:</div>' +
        '<div class="invoiceInfoRight invoiceClear">' + returnTableSpaceIfEmpty(quote.Customer.firstName)+ ' ' + returnBlankIfEmpty(quote.Customer.lastName) + '</div><div class="invoiceInfoLeft ">Customer Name:</div>' +
        '<div class="invoiceInfoRight invoiceClear">' + handleBlankText(quote.Customer.companyName) + '</div><div class="invoiceInfoLeft ">Company Name:</div>' +
        '<div class="invoiceInfoRight invoiceClear">' + customerPO + '</div><div class="invoiceInfoLeft ">PO# / Cust Ref:</div>' +
        // quoteBillingAddressInvoice() +
        '';

    if (isUK() && row.invoiceTitle == 'DELIVERY NOTE') {

        customerHTML = ''+
            '<div class="invoiceInfoRight invoiceClear">' + handleBlankText(row.companyName) + '</div><div class="invoiceInfoLeft ">Company Name:</div>' +
            '<div class="invoiceInfoRight invoiceClear">' + customerRef + '</div><div class="invoiceInfoLeft ">Customer Ref:</div>' +
            '<div class="invoiceInfoRight invoiceClear">' + row.quoteID + '</div><div class="invoiceInfoLeft ">Job #:</div>' +
            '';
    }

    var invoiceInfo = '' +
        '<div class="invoiceTitle2">' + row.invoiceTitle + '</div>' +
        invoiceHTML +
        customerHTML;



    return invoiceInfo;
}

function quoteBillingAddressInvoice()  {

    var address = quote.address;

    var address2 = '';

    if (!empty(address.billing_address2)) {
            address2 = '' +
                '<div class="invoiceInfoRight invoiceClear">' + returnTableSpaceIfEmpty(address.billing_address2)+ '</div>' +
                '<div class="invoiceInfoLeft ">&nbsp;</div>' ;

    }

    var html = ''+
        '<div class="invoiceInfoRight invoiceClear">' + returnTableSpaceIfEmpty(quote.address.billing_address1)+ '</div>' +
        '<div class="invoiceInfoLeft ">Billing Address:</div>' +
        address2 +
        '<div class="invoiceInfoRight invoiceClear">' + returnBlankIfEmpty(address.billing_city) + ', ' + returnBlankIfEmpty(address.billing_state) + ' ' + returnBlankIfEmpty(address.billing_zip) + '</div>' +
        '<div class="invoiceInfoLeft ">&nbsp;</div>' ;

    return html;

}


function buildInvoiceDescriptionColumn(row) {

    var panelMovement = buildPanelMovement(row);
    var swing = empty(row.panelOptions.swingDirection.selected) ? 'none' : row.panelOptions.swingDirection.selected;
    var swingDirection = empty(row.panelOptions.swingInOrOut.selected) ? 'none' : row.panelOptions.swingInOrOut.selected;

    var track = row.panelOptions.track.selected;
    var frame = row.panelOptions.frame.selected

    var finishes = displayFinishes(row);

    var glassType = glassTypeFromCartRow(row);
    var glassOptions = displayGlassOptions(row);

    var trackWarning = '';
    if (track == "Flush") {
        trackWarning = '<div class="cartDescriptionRowThird">* The flush track does not offer protection from water penetration</div>';
    }

    var hardwareName = splitForSecondOption(row.hardwareChoices);
    var hardwareOptions = displayHardwareExtras(row);



    if (row.materialType == 'Vinyl') {
        if (empty(row.vinylColor)) {
            row.vinylColor = {};
            row.vinylColor.name1 = "White";
        }
    }

    var numberOfSwings = 1;
    if (swing == 'both') {
        numberOfSwings = 2;
    }

    var panelWidth = '';
    if (empty(row.panelWidth)) {
        panelWidth = buildPanelWidth(row.width, row.numberOfPanels, numberOfSwings, row.module);
    } else {
        panelWidth = row.panelWidth;
    }

    var panelWidthRow = '<div class="cartDescriptionRowMain">Panel Width:</div>' +
        '<div class="cartDescriptionRowSecond">' + panelWidth + uom + '</div>';

    var clearOpeningAmount = 4;
    if (isUK()) {
        clearOpeningAmount = clearOpeningAmount + 25.4;
    }
    var clearOpenHeight = '<div class="cartDescriptionRowMain">Clear Opening Height:</div>' +
        '<div class="cartDescriptionRowSecond">' + (row.height - clearOpeningAmount) + uom + '</div>';

    var swingDoorPosition = '<div class="cartDescriptionRowMain">Swing Door(s) Position</div>' +
        '<div class="cartDescriptionRowSecond">' + capitaliseFirstLetter(swing) + '</div>';

    var swingDoorRow = '<div class="cartDescriptionRowMain">Swing Door Direction</div>' +
        '<div class="cartDescriptionRowSecond">' + swingDirection + '</div>';

    var slideStackDirection = '<div class="cartDescriptionRowMain">Panel Slide/Stack Direction</div>' +
        '<div class="cartDescriptionRowSecond">' + panelMovement + '</div>';

    var frameRow = '<div class="cartDescriptionRowMain">Frame Type:</div>' +
        '<div class="cartDescriptionRowSecond">' + frame + '</div>';

    var trackRow = '<div class="cartDescriptionRowMain">Track: Clear Anodized</div>' +
        '<div class="cartDescriptionRowSecond">' + track + '</div>' + trackWarning;

    var hardware = '<div class="cartDescriptionRowMain"Hardware:</div>' +
        '<div class="cartDescriptionRowSecond">' + hardwareName + '</div>';


    var screenRowUS = '';

    if (!empty(row.panelOptions.screens)) {
        if (!empty(row.panelOptions.screens.selected)) {
            screenRowUS = '<div class="cartDescriptionRowMain">Screen: </div>' +
                '<div class="cartDescriptionRowSecond">' + row.panelOptions.screens.selected + '</div>' + cartDescriptionInitials();
        }
    }

    if (isGuest()) {
        panelWidthRow = '';
        clearOpenHeight = '';
        swingDoorRow = '';
        frameRow = '';
        trackRow = '';
    }

    //if (row.numberOfPanels == 1) {
    //    swingDoorPosition = '';
    //    swingDoorRow = '';
    //    slideStackDirection = '';
    //    hardware = '';
    //    hardwareOptions = '';
    //}

    var sillHTML = sillCartHTML(row.sillOptions);
    var addOnHTML = addOnCartHTML(row.addOnChoices);
    var trickleVentHTML = trickleVentForCart(row);

    var doorType = 'Door';

    if (!empty(row.windowsOnly)) {
        doorType = 'Window'
    }

    var html = '' +
        '<div class="cartInvoiceDescriptionTitle">Description</div>' +
        '<div class="cartInvoiceDescription">' +
        '   <div class="cartDescriptionRowMain">Net Frame Dimensions of '+doorType+'</div>' +
        '   <div class="cartDescriptionRowSecond">' + netFrameDimensions(row) + '</div>' +
        '   <div class="cartDescriptionRowMain">Recommended Rough Opening</div>' +
        '   <div class="cartDescriptionRowSecond">' + recommendedRoughOpeningDimensions(row) + '</div>' +
        // '   <div class="cartDescriptionRowMain">User Entered Dimensions:</div>' +
        // '   <div class="cartDescriptionRowSecond">' + userEnteredDimensions(row) + '</div>' +
        '   <div class="cartDescriptionRowMain">Number of Panels:</div>' +
        '   <div class="cartDescriptionRowSecond">' + row.numberOfPanels + '</div>' +
        '   ' + sillHTML +
        '   ' + addOnHTML +
        '   ' + panelWidthRow +
        '   ' + clearOpenHeight +
        '   ' + swingDoorPosition +
        '   ' + swingDoorRow +
        '   ' + slideStackDirection +
        '</div>' +
        '<div class="cartInvoiceDescription">' +
        '   ' + frameRow +
        '   ' + trackRow +
        '   ' + trickleVentHTML +
        '   ' + finishes +
        '   <div class="cartDescriptionRowMain">Glass Type:</div>' +
        '   <div class="cartDescriptionRowSecond">' + glassType + '</div>' +
        '   ' + glassOptions +
        '   ' + hardware +
        '   ' + hardwareOptions +
        '</div>';

    return html;
}

