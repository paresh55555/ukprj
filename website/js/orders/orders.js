'use strict';


function loadOrders(status) {

    startSpinner();
    if (empty(status)) {
        status = uiState.orderStatus;
    }

    if (empty(status)) {
        status = '';
    }

    quote = {};
    formStatus = {};
    setTab(tab);
    selectTab('#viewOrders');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();

    var topButtons = buildOrderTopButtons();
    var searchBox = buildSearchBox();
    $('#preMainContent').html(topButtons + searchBox);

    $('#search').unbind("keyup").bind("keyup", orderSearchDelay);
    $('#newQuote').unbind("click").bind("click", salesStartOver);

    $('#cartButton').hide();
    updateFormStatus();
    orderSearch(status);
}


function orderSearch(status) {

    var search = $('#search').val();
    var order = getOrder();

    if (empty(status)) {
        status = '';
    }

    var url = "https://" + apiHostV1 + "/api/orders?" + authorizePlusSalesPerson() +
        '&status=' + status + pageLimitText() + order + addSite() + '&search=' + search + '&fullStatus=' + siteDefaults.showFullOrderStatus
        + addSuperToken();

    $.getJSON(url, function (apiJSON, status, xhr) {

        var total = xhr.getResponseHeader('Total');
        if(!validate(apiJSON))return;

        var html = buildViewOrders(apiJSON);
        $('#mainContent').html(html);

        if (siteDefaults.salesFlow == 1 || siteDefaults.salesFlow == 3 ) {
            $(".orderDateBox").datepicker({gotoCurrent: true, minDate: 0});
        }
        $('.orderIconsActions').unbind("click").bind("click", orderActions);
        $('.orderStatus').unbind("click").bind("click", viewOrderBasedOnStatus);
        $('.sortable').unbind("click").bind("click", orderTableResultsWithTab);

        $('#preMainContent').show();
        setPaginationWithSelectorAndTotal('#pagination', total);
        showContentAndSaveState();

    });
}


function orderSearchDelay() {

    delay(function () {
        orderSearch();
    }, 300);
}


function viewOrderBasedOnStatus() {

    var status = $(this).attr('id');

    startSpinner();
    uiState.orderStatus = status;
    writeStateUI();

    chooseViewOrderType(status);
}


function chooseViewOrderType(status) {

    if (status == 'needsPayment') {
        trackAndDoClick('viewOrdersNeedsPayment');
    } else if (status == 'holdOrder') {
        trackAndDoClick('viewOrdersHold');
    } else if (status == 'accounting') {
        trackAndDoClick('viewOrdersAccounting');
    } else if (status == 'preProduction') {
        trackAndDoClick('viewOrdersPreProduction');
    } else if (status == 'production') {
        trackAndDoClick('viewOrdersProduction');
    } else if (status == 'completed') {
        trackAndDoClick('viewOrdersCompleted');
    } else if (status == 'delivered') {
        trackAndDoClick('viewOrdersDelivered');
    } else {
        trackAndDoClick('viewOrders');
    }
}


function buildViewOrders(data) {

    var row;
    var rows = '';

    for (var a = 0; a < data.length; a++) {
        var order = data[a];

        row = orderRow(order, a);
        rows = rows + row;
    }

    var html = orderRowHeader(rows);

    return html;
}


function buildOrderTopButtons() {

    var accounting = '<div id="accounting" class="orderStatus accounting">Accounting</div>' ;
    if (isUKsurvey() ) {
        accounting = '<div id="accounting" class="orderStatus accounting"><div class="accountDoubleRowTopBar">Accounting / </div>  <div class="accountDoubleRow">Needs Survey</div></div>' ;
    }
    var html = '' +
        '<div  class="orderStatus orderStatusLeft">View All</div>' +
        '<div id="needsPayment" class="orderStatus order">Needs Payment</div>' +
        '<div id="holdOrder" class="orderStatus holdOrder">Hold </div>' +
        accounting +
        '<div id="preProduction" class="orderStatus preProduction">Pre-Production</div>' +
        '<div id="production" class="orderStatus production">Production</div>' +
        '<div id="completed" class="orderStatus completed">Completed</div>' +
        '<div id="delivered" class="orderStatus delivered">Delivered</div>';


    if (siteDefaults.showFullOrderStatus == '0') {
        var size = 940 / 5;
        var specialStyle = 'style="width: ' + size + 'px"';

        html = '' +
            '<div  class="orderStatus orderStatusLeft" ' + specialStyle + '>View All</div>' +
            '<div id="accounting" class="orderStatus accounting" ' + specialStyle + '>Payments Pending</div>' +
            '<div id="production" class="orderStatus production" ' + specialStyle + '>Production</div>' +
            '<div id="completed" class="orderStatus completed" ' + specialStyle + '>Completed</div>' +
            '<div id="delivered" class="orderStatus delivered" ' + specialStyle + '>Delivered</div>';
    }

    return html;
}


function orderRowHeader(rows) {

    var limitPullDown = buildLimitList();
    var pagination = '' +
        '<div id="paginationBox">' +
        '   <div id="limitPullDown" ><div id="limitPullDownText">Show: </div>' + limitPullDown + '</div> ' +
        '   <div id="pagination"></div>' +
        '</div>';

    var amountDueHTML = '<div id="depositDue" class="orderCell7 borderLeftTop sortable">Deposit Due</div>';
    var companyHTML = '<div id="companyName" class="orderCell3 borderLeftTop sortable">Company </div>';
    var dueHTML = '<div id="dueDate" class="orderCell8 borderLeftTop sortable">Due</div>';
    var estCompHTML = '   <div id="estComplete" class="orderCell9 borderLeftRightTop sortable">Completed</div>';
    var soldHTML = '   <div id="balanceDue" class="orderCell8 borderLeftTop sortable">Sold</div>';
    if (siteDefaults.salesFlow == 2) {
        amountDueHTML = '';
        companyHTML = '<div id="companyName" class="orderCell3 borderLeftTop sortable" style="width:161px" >Company </div>';
        estCompHTML = '   <div id="estComplete" class="orderCell9 borderLeftRightTop sortable">Comp. Date</div>';
        dueHTML = '<div id="dueDate" class="orderCell8 borderLeftTop sortable">Paid</div>';
        soldHTML = '   <div id="balanceDue" class="orderCell8 borderLeftTop sortable">Quoted</div>';
    }

    var html =
        pagination +
        '<div id="subTabAlert" class="' + uiState.orderStatus + '"></div>' +
        '<div class="orderRowTitle">' +
        '   <div id="id" class="orderCell1 borderLeftTop sortable">Order #</div>' +
        '   <div id="status" class="orderCell2 borderLeftTop sortable">Status</div>' +
        '   <div id="lastName" class="orderLastName borderLeftTop sortable">Last</div>' +
        '   <div id="firstName" class="orderFirstName borderLeftTop sortable">First</div>' +
        '   ' + companyHTML +
        '   <div id="quantity" class="rowQuantity borderLeftTop sortable">Qty.</div>' +
        '   ' + amountDueHTML +
        '   <div id="balanceDue" class="orderCell7 borderLeftTop sortable">Balance Due</div>' +
        '   ' + soldHTML +
        '   ' + dueHTML +
        '   ' + estCompHTML +
        '</div>' +
        rows +
        '<div class="finalRow"></div>';

    return html;
}


function cleanDate(date) {

    if (empty(date)) {
        date = '--';
    }
    if (date == '01/01/1970') {
        date = '--';
    }
    if (date == '11/30/-0001') {
        date = '--';
    }
    if (date == '04/16/0015') {
        date = '--';
    }
    if (date == '12/31/1969') {
        date = '--';
    }


    return date;
}


function updateOrderDueDateWithCountAndId(count, id) {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    var dueDate = $("#dueDate-" + count).val();

    if(quote.quoted){
        var dueDateTime = new Date(dueDate).getTime(),
            quoted = new Date(quote.quoted).getTime();

        if(dueDateTime < quoted){
            alert('Due Date can not be before the Quote Date');
            $("#dueDate-" + count).val('');
            return;
        }
    }

    var dueDateObj = {'dueDate': dueDate};

    var json = JSON.stringify(dueDateObj);

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/dueDate?" + authorizePlusSalesPerson() + addSite() + addSuperToken();

    if (isProduction()) {
        url = "https://" + apiHostV1 + "/api/quotes/" + id + "/dueDate?" + authorizePlusProduction() + addSite();
    }

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                quote.dueDate = dueDate;
                writeQuote();
                //writeCart(cart);
                //writeFormStatus();
                //writeQuoteID(quoteID);

            } else {
                alert("Quotes DueDate Update Save Failed");

            }
        }
    });
}


function lastNameFirstNameWithRow(row) {

    var name = createTableSpaceOnEmpty();
    var lastName = createTableSpaceOnEmpty(row.lastName);
    var firstName = createTableSpaceOnEmpty(row.firstName);


    if (empty(row.lastName) && empty(row.firstName)) {
        return name;
    } else if (!empty(row.lastName) && !empty(row.firstName)) {
        name = lastName + ', ' + firstName;
    } else if (!empty(row.lastName)) {
        name = lastName;
    } else if (!empty(row.firstName)) {
        name = firstName;
    }

    return name;


}
function orderRow(row, count) {

    row.balanceDue = getBalanceDue(row);
    var depositDue = getDepositDueAccountingOrders(row);

    var statusLabels = {
        'order': 'Needs Payment', 'Confirming Payment': 'Accounting', 'Needs Survey' : 'Needs Survey',
        'Ready For Production': 'Pre-Production', 'In Production': 'Production',
        'Completed': 'Completed', 'Delivered': 'Delivered'
    };

    if (siteDefaults.salesFlow == 2) {
        statusLabels = {
            'order': 'Needs Payment', 'Confirming Payment': 'Pending Payments',
            'Ready For Production': 'Production', 'In Production': 'Production',
            'Completed': 'Completed', 'Delivered': 'Delivered'
        };
    }

    var statusClass = {
        'order': 'order', 'Confirming Payment': 'accounting', 'Needs Survey': 'accounting', 'Ready For Production': 'preProduction',
        'In Production': 'production', 'Completed': 'completed', 'Delivered': 'delivered', 'holdOrder':'holdOrder'
    };

    var orderNumber = makeQuoteTitle(row);
    var companyName = createTableSpaceOnEmpty(row.companyName);
    var phone = createTableSpaceOnEmpty(row.phone);
    var alt = buildAlt(count);

    var dueDate = cleanDate(row.dueDate);
    var sold = cleanDate(row.orderDate);
    var estComplete = cleanDate(row.completedDate);
    var quoted = cleanDate(row.quoted);

    var lastName = createTableSpaceOnEmpty(row.lastName);
    var firstName = createTableSpaceOnEmpty(row.firstName);


    var dueDateHTML = '   <div class="orderCell8 borderLeftTop" >' +
        '       <input id="dueDate-' + count + '"  onChange="updateOrderDueDateWithCountAndId(' + count + ',' + row.id + ')" class="orderDateBox" type="text"  value = "' + dueDate + '"/>' +
        '   </div>';


    var amountDue = '<div class="orderCell7 borderLeftTop"  >' + currency + formatMoney(depositDue) + '</div>';
    var companyHTML = '<div class="orderCell3 borderLeftTop"  >' + companyName + '</div>';
    var soldHTML = '<div class="orderCell8 borderLeftTop">' + sold + '</div>';

    if (siteDefaults.salesFlow == 2) {
        amountDue = '';
        companyHTML = '<div class="orderCell3 borderLeftTop" style="width:161px"  >' + companyName + '</div>';
        dueDateHTML = '<div class="orderCell8 borderLeftTop" >' + sold + '</div>';
        soldHTML = '<div class="orderCell8 borderLeftTop">' + quoted + '</div>';
    }

    var idRowHTML = '<div id="' + row.id + '-viewLink" class="orderIconsActions orderCell1 borderLeftTop ' + statusClass[row.type] + '"  >' + orderNumber + '</div>' ;
    if (row.leadQuality == 'holdOrder') {
        idRowHTML = '<div id="' + row.id + '-viewLink" class="orderIconsActions orderCell1 borderLeftTop holdOrder"  >' + orderNumber + '</div>' ;
    }


    var html =
        '<div class="orderRow ' + alt + '">' +
        '   ' + idRowHTML +
        '   <div class="orderCell2 borderLeftTop"  >' + statusLabels[row.type] + '</div>' +
        '   <div class="orderLastName borderLeftTop" >' + lastName + '</div>' +
        '   <div class="orderFirstName borderLeftTop" >' + firstName + '</div>' +
        '   ' + companyHTML +
        '   <div class="rowQuantity borderLeftTop"  >' + row.quantity + '</div>' +
        '   ' + amountDue +
        '   <div class="orderCell7 borderLeftTop"  >' + currency + formatMoney(row.balanceDue) + '</div>' +
        '   ' + soldHTML +
        '   ' + dueDateHTML +
        '   <div class="orderCell9 borderLeftRightTop" >' + estComplete + '</div>' +
            //'   <div class="orderCell10 borderLeftRightTop" >' +
            //'       <img id="' + row.id + '-view" class="orderIcons orderIconsActions" src="/images/quotes/icon-view-121.svg">' +
            //'       <img id="' + row.id + '-printOrder" class="orderIcons orderIconsActions" src="/images/quotes/icon-print-121.svg">' +
            //'       <img id="' + row.id + '-emailOrder" class="orderIcons orderIconsActions" src="/images/quotes/icon-mail-121.svg">' +
            //'       <img id="' + row.id + '-deleteOrder"class="orderIcons orderIconsActions" src="/images/quotes/icon-delete-121.svg">' +
            //'   </div>' +
        '</div>';

    return html;


}


function orderActions() {


    var thisSelector = $(this);
    var id = $(thisSelector).attr('id');

    var orderID = splitForFirstOption(id);
    var action = splitForSecondOption(id);

    if (action === 'deleteJob') {
        deleteOrderProduction(id);
    }
    if (action === 'printGlassSheet') {
        startSpinner();
        printGlassSheetForProduction(quote.id);
    }
    if (action === 'printGlassLabels') {
        startSpinner();
        printGlassLabelsForProduction(quote.id);
    }
    if (action === 'printCutSheet') {
        startSpinner();
        printCutSheetForProduction(quote.id);
    }
    if (action === 'printCutSheetSales') {
        startSpinner();
        printCutSheetForSales(quote.id);
    }

    if (action === 'printCollection') {
        stopSpinner();
        getInvoiceToPrintForProduction(quote.id, site);
    }

    if (action === 'closeJob') {
        if (empty(lastTab)) {
            lastTab = 'viewPreProduction'
        }
        trackAndDoClick(lastTab);
    }
    if (action === 'deleteOrder') {
        deleteOrder(orderID);
    }
    if (action === 'view' || action === 'viewLink') {
        trackAndDoClick('viewOrder', '&order=' + orderID);
    }
    if (action === 'emailOrder') {
        trackAndDoClick("viewOrder", "&order=" + orderID + "&edit=email");
    }
    if (action === 'printInvoice') {
        var order = getParameter('order');
        getInvoiceToPrint(order, site);
    }
    if (action === 'printOrder' || action === 'printJob' || action === 'printOrderCert') {
        doOrderPrinting(orderID, action);
    }
    if (action === 'manifest') {
        doManifestPrinting(orderID);
    }
    if (action === 'saveToOrder') {
        saveToOrder(orderID);
    }
    if (action === 'cancelOrderChanges') {
        cancelorderChanges(orderID);
    }
    if (action === 'closeOrder') {
        trackAndDoClick("viewOrders");
    }
    if (action === 'closeAccounting') {
        var secondTab = getParameter('secondTab');
        if (empty(secondTab)) {
            secondTab = 'confirmOrders';
        }
        trackAndDoClick(secondTab);
    }
    if (action === 'addDoor') {
        trackAndDoClick("viewOrder", "&order=" + orderID + "&edit=item&cartItem=formStatus&secondTab=addDoor");
    }
}

function deleteOrder(quoteID) {

    var x;

    stopSpinner();
    if (confirm("Are you sure you want to delete this order?") == true) {
        startSpinner();
        var quote = getParameter('quote');
        $.ajax({
            url: "https://" + apiHostV1 + "/api/quotes/" + quoteID + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
            type: "DELETE",
            success: function (response) {
                if (tab == 'viewOrder') {
                    tab = 'viewOrders';
                }
                trackAndDoClick(tab);

            }
        });

    }

}

function loadViewOrder(newUrl) {

    var order = getParameter('order');
    var edit = getParameter('edit');
    var print = getParameter('print');

    $('#footerRight').show();

    getOrderAPIWithNewUrlAndOrder(newUrl, order).done(function (apiJSON) {

        if (!validate(apiJSON)) {
            return;
        }

        quote = apiJSON;
        quote.noCart = 'yes';
        if (!empty(quote.SalesDiscount)) {
            discounts = quote.SalesDiscount;
        } else {
            discounts = jQuery.extend({}, defaults);
            quote.SalesDiscount = jQuery.extend({}, defaults);
        }

        var params = {};
        params.quote = quote;

        if (!empty(quote.Customer)) {
            params.customerID = quote.Customer.customerID;
        }
        params.selector = "#preMainContent";
        params.edit = edit;

        if (edit === 'item') {
            loadQuoteAndCarEditingItem(params);
        } else {
            loadOrderAndCart(params);
        }

        if (print == 'yes') {
            $('#headerNav').hide();
            $('#salesNav').hide();
            $('#preMainContent').hide();
            $('#footer').hide();
            $('.cartRow').css({'margin-bottom': '323px'});
        }

        $('#sendToProduction').unbind("click").bind("click", paymentConfirmed);

        showAddToQuoteButton();
        updateCssForViewQuote();

        if (isProduction()) {
            $('#startOfDynamicContent').css({'margin-top': '-44px'});
        }

        if ((siteDefaults.salesFlow == 3 || siteDefaults.salesFlow == 1) && print != 'yes') {
            var html = buildCustomerAndQuoteOverView(quote);
            $('#mainContent').prepend(html);

            // $('#pizza_kind').prop('disabled', 'disabled');

            if (isAudit()) {
                $('.fieldUpdate').prop("disabled", 'disabled');
                $('#salesTerms').prop("disabled", 'disabled');
            } else {
                $('.fieldUpdate').unbind("keyup").bind("keyup", updateCustomerDelay);
                $('.fieldUpdate').unbind("change").bind("change", updateCustomerNoDelay);
                $('#salesTerms').unbind("change").bind("keyup", updateSalesTermsDelay);
            }


        }

        $('#preMainContent').show();
        $('#footerPrice').show();
    });

}

function confirmBilling() {

    trackAndDoClick('paymentInfo', '&type=confirm');


}

function chooseOrderDetailsPath(params) {

    if (siteDefaults.salesFlow == 3) {
        addQuoteDetailsWithParametersFlow3(params, 'order');
    }
    else if (siteDefaults.salesFlow == 2) {
        addQuoteDetailsWithParametersFlow2(params);
    } else {
        addOrdersDetailsWithParameters(params);
    }

}


function loadOrderAndCart(params) {

    if (params.edit == 'email') {
        buildEmailQuote(params)
    } else if (params.edit == 'emailConfirmed') {
        buildEmailConfirmed(params)
    } else {
        chooseOrderDetailsPath(params);
    }

    loadSoloCart();

    var total = getTotalForSalesAdmin(quote.SalesDiscount);

    $('#footerPrice').html('Total ' + currency + formatMoney(total) + footerNoteHTML());

    $('#footerRight').html('');
    $('#cartButton').hide();
    processStartOverButtonVisibility(false);


    if (isSalesPerson() || isAccounting() ) {
        addSalesAdminWithLocation('#footerRight');
        if(isUserHasSalesAdmin()){
            $('#footerRight').html('<img onclick="launchSalesAdmin()" id="saleAdminLauncher" src="/images/footer/gears.png">');
        }
    }

    updateWorkingOnTitle();
    showContentAndSaveState();
    scrollTopOfPage();


}

function getProductionNotes(qoute) {

    var html = '';

    //if (tab == 'viewProductionOrder') {
    //    html = '' +
    //    '<div class="showProductionNotes">' +
    //    '   <div class="productionNotesTitle">Comments</div>' +
    //    '   <textarea onkeyup="productionNotesKeyUp()" id="productionNotes"  placeholder=""  name="productionNotes">' + quote.productionNotes + '</textarea>' +
    //    '</div>';
    //}
    return html;
}
function addOrdersDetailsWithParameters(params) {

    var quote = params.quote;
    var customerID = params.customerID;
    var selector = params.selector;
    var edit = params.edit;

    var quoteDetails = buildQuoteDetails(quote);
    var customerDetails = buildCustomerDetails(quote.Customer);

    var uploadContractDetails = buildUploadContractDetails(quote);


    var quoteNotes = buildQuoteNotes(quote);

    var actions = buildOrdersActions(params);
    var paymentHistory = getPaymentHistory();
    var productionNotes = getProductionNotes(quote);

    var quoteDetails = '' +
        '<div class="quoteDetailsColumn">' + quoteDetails + '</div>' +
        '<div class="quoteDetailsColumn">' + customerDetails + uploadContractDetails + '</div>' +
        '<div class="quoteDetailsColumn">' + quoteNotes + '</div>';

    var statusBorder = getStatusBorder(quote);

    var html = '' +
        '<div class="quoteDetailsBox ' + statusBorder + '">' +
        '   ' + quoteDetails +
        '   ' + productionNotes +
        '   ' + actions +
        '</div>';

    $(selector).html(html);


    initContracts();

    $('.quoteActionButtons', '.quoteDetailsBox').unbind("click").bind("click", orderActions);
    $('#enterPaymentInfo').unbind("click").bind("click", enterPaymentInfo);
    $(".estCompleteDateBoxLarge").datepicker({gotoCurrent: true, minDate: 0, "dateFormat": "mm/dd/yy"});
}

function getStatusBorder(quote) {

    var status = quote.type;

    var cssClass = '';

    if (status == 'order') {

        cssClass = 'orderBorder';
    }
    else if (status == 'Confirming Payment' ) {

        cssClass = 'verifyDepositBorder';
    }
    else if (status == 'Ready For Production') {

        cssClass = 'preProductionBorder';

    } else if (status == 'In Production') {

        cssClass = 'productionBorder';

    } else if (status == 'Completed') {

        cssClass = 'completedBorder';

    } else if (status == 'Delivered') {

        cssClass = 'deliveredBorder';

    } else {

        cssClass = '';

    }

    return cssClass;


}

function enterPaymentInfo() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    trackAndDoClick('paymentInfo');

}
function loadPaymentInfo() {

    var customerHtml = buildPaymentInfo('payment');

    $('#preMainContent').hide();
    $('#mainContent').html(customerHtml);
    $('#footer').hide();
    formatHeaderForCustomer();
    $('.editButtons').unbind("click").bind("click", customerButtonsPress);

    $('#mainContent').css("margin-top", "165px");

    $('#customerViewNote').hide();

    $('.paymentOptionsBox').unbind("click").bind("click", selectCheckOrCreditCardType);
    $('.addPaymentButton').unbind("click").bind("click", addPayment);
    $("#paymentDate").datepicker({gotoCurrent: true});
    $("#paymentOrderNotes").keyup(updatePaymentNoteDelay).on('paste', processPaste);


    showContentAndSaveState();


}

function updatePaymentNoteDelay() {

    var paymentNotesSelector = $(this),
        paymentNotes = paymentNotesSelector.val(),
        id = quote.id;

    delay(function () {

        var data = {'id': id, 'paymentNotes': paymentNotes};
        var json = JSON.stringify(data);

        var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusSalesPerson() + addSite();

        if (isProduction()) {
            url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusProduction() + addSite();
        }

        $.ajax({
            url: url,
            data: json,
            type: "PUT",
            success: function (result) {

                var response = jQuery.parseJSON(result);

                if (response.status != "Success") {
                    alert("Failed to Save Quote Notes");
                } else {
                    quote.paymentNotes = paymentNotes;
                    writeQuote(quote);
                    paymentNotesSelector.css('border-color', 'green');
                    delay(function () {
                        paymentNotesSelector.css('border-color', '#e7e7e7');
                    }, 1000);
                }
            }
        });
    }, 200);

}

function cleanNumber(num){
    if(parseFloat(num) === 0) return 0;
    num = num.replace(',','');
    var N = Number(num);
    if(parseFloat(num)=== N) return N;
    return Number(num.replace(/\D+/g, '')) || 0;
}


function addPayment() {


    if (empty(quote.SalesDiscount.Payments)) {
        quote.SalesDiscount.Payments = [];
    }
    
    if (quote.SalesDiscount.Payments.length == siteDefaults.maxNumberPayments) {
        stopSpinner();
        alert("Sorry you can only add a maximum of " + siteDefaults.maxNumberPayments + " payments");
        return;
    }

    var lastFour = $('#lastFour').val();
    var amount = cleanNumber($('#paymentAmount').val());
    var date = $('#paymentDate').val();
    var type = $('.colorSelectedDivCC').prev().attr('id');


    if (typeof type != 'string') {
        alert("Please select type of payment method");
        return
    }

    if (lastFour.length < 1 && (type != 'Cash' && type != 'Wire' )) {
        alert("Please enter a check number or last 4 of CC");
        return
    }

    amount = parseFloat(amount);
    if (empty(amount)) {
        alert("Please enter an amount");
        return
    }

    if (date.length < 1) {
        alert("Please enter the date of payment");
        return
    }

    if (empty(lastFour)) {
        lastFour = ' ';
    }


    var payment = {};
    payment.date = date;
    payment.amount = amount;
    payment.type = type;
    payment.number = lastFour;
    payment.lastFour = lastFour;
    payment.quoteID = quote.id;
    payment.kind = type;


    apiMakePayment(payment).done(function (result) {

        var values = JSON.parse(result);
        payment.quoteID = values.insertedID;
        payment.id = values.insertedID;
        quote.SalesDiscount.Payments.push(payment);
        writeQuote(quote);

        updatePaymentHistory();
    });


    //justSaveOrderToDBPayment('Failed to Add Payment');


}
function updatePaymentHistory() {

    var paymentBoxRight = getPaymentHistory();
    $('.paymentBoxRight').replaceWith(paymentBoxRight);

}


function deletePayment(id, index) {

    quote.SalesDiscount.Payments.splice(index, 1);

    var payment = {};
    payment.id = id;

    $.when(
        apiDeletePayment(payment)
    ).done(function () {
        updatePaymentHistory();
    });

    writeQuote(quote);
}

function selectCheckOrCreditCardType() {

    quote.SalesDiscount.Totals.ccPaymentType = $(this).text();

    var groupOfButtons = $(this).parent();
    var selectedDiv = $(".colorSelectedDivCC", groupOfButtons);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
        $('.optionsItemText', groupOfButtons).css({"background-color": "#FFFFFF", "color": "#000000"});
        $('.optionsItemRAL', groupOfButtons).css({"background-color": "#FFFFFF", "color": "#000000"});
    }

    jQuery('<div/>', {
        class: 'colorSelectedDivCC'
    }).appendTo($(this));


    jQuery('<img/>', {
        src: 'images/icons/icon-selected-mini.svg'
    }).appendTo($(".colorSelectedDivCC", this));

    $('.optionsItemText', this).css({"background-color": "#44ac61", "color": "#FFFFFF"});


}

function buildUploadContractDetails(){
    var uploadContractButton =  '<div id="uploadContract" class="myButton contractButton selectedButton">Upload contract</div>',
        deleteContractButton = '<div id="deleteContract" class="myButton contractButton">Delete contract</div>',
        uploadContractInput = '<input id="contractFile" name="contract" type="file" />';
    //Temp Disable
//    uploadContract = '';

    if (isAudit())  {
        uploadContractButton = '';
        deleteContractButton = '';
    }

    var html =
        '<div id="contractDetails" class="customerBoxDetails">' +
            '<img id="contractPreview" src="/images/contractPreview.png" alt="Contract" />' +
            '<a id="downloadLink" href=""></a>' +
            (!isLimitedAccount() ? (
                uploadContractInput +
                uploadContractButton +
                deleteContractButton
            ): '') +
        '</div>';
    return html;
}

function initContracts(){
    if(!isSurvey()){
        getContractID(quote.id).done(function(response){
            if(response.id){
                updateContractBox(response.id);
            }
            $('#uploadContract').unbind("click").bind("click", function(){
                $(this).siblings('input').click();
            });
            $('#deleteContract').unbind("click").bind("click", function(){
                if (confirm("Are you sure you want to delete this contract?") == true) {
                    startSpinner();
                    deleteContractFile(quote.id).done(function(response){
                        updateContractBox(false);
                        stopSpinner();
                    });

                }
            });
            initContractUpload();
            initContractDownload();
        });
    }
}

function initContractUpload(){
    $("#contractFile").unbind("change").bind("change", function(e) {
        var file = e.target.files[0],
            preview = $('#contractPreview'),
            processResponse = function(response){
                updateContractBox(response.id);
                stopSpinner();
            };
        if(file.type === 'application/pdf' || file.type === 'image/jpeg'){
            startSpinner();
            var data = new FormData();
            data.append('contract', file);
            if(preview.data('cid')){
                updateContractFile(quote.id, preview.data('cid'), data).done(processResponse);
            }else{
                uploadContractFile(quote.id, data).done(processResponse);
            }
        }else{
            alert('Invalid type file. Only pdf or jpeg files allowed');
        }
    });
}

function initContractDownload(){

    $('#contractPreview').unbind('click').bind('click', function(e){
        startSpinner();


        var url = '/api/V4/quotes/'+quote.id+'/contracts/download?_=' + new Date().getTime();

        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.setRequestHeader("Cache-Control", "no-cache");
        request.setRequestHeader("Authorization", 'Bearer '+user.v4Token);
        request.responseType = "blob";

        request.onload = function (oEvent) {
            var type = request.getResponseHeader('content-type').toLowerCase(),
                ext = (type === 'application/pdf' ? '.pdf' : '.jpg');
            if (this.status === 200) {
                if (navigator.appVersion.toString().indexOf('.NET') > 0){
                    window.navigator.msSaveBlob(this.response, quote.prefix + quote.id + ext);
                }else{
                    var file = window.URL.createObjectURL(this.response);
                    var anchor = $('#downloadLink');
                    anchor.prop('href', file);
                    anchor.prop('download', quote.orderName + ext);
                    anchor.get(0).click();
                    window.URL.revokeObjectURL(url);
                }
            };
            stopSpinner();
        };

        request.send();
    });
}

function updateContractBox(id){
    $('#contractPreview').data('cid', id);
    $('#contractDetails').toggleClass('hasContract', id ? true : false);
    $("#contractFile").val('');
}

function buildPaymentInfo(type) {

    var data = quote.Customer;

    data = validateCustomer(data);

    var customerTitleAndButtons = getPaymentTitleAndButtons();
    var customerDetails = getCustomerDetails(data);
    var paymentInformation = getPaymentInformation(data);
    var paymentHistory = getPaymentHistory(data);
    //var paymentInfoBox = getPaymentInfoBox(type);

    if (siteDefaults.salesFlow == 2) {
        customerDetails = '';
    }
    var html = '' +
        '<div id="takePayment">' +
        '   ' + customerTitleAndButtons +
        '   <div class="paymentInfoGroupBox">' +
        '       ' + customerDetails +
        '       ' + paymentInformation +
        '       ' + paymentHistory +
        '   </div>' +
        '</div>';

    return html;


}

function getPaymentHistoryTitle(data) {

    var html = '   <div class="customerBoxDetailsTitle">Payment History</div>';
    return html;
}

function getAmountDue() {

    var total = getTotalForSalesAdmin(quote.SalesDiscount);

    var html = '' +
        '<div class="amountDueBox">' +
        '   <div class="paymentsAmountDueLabel">Total Amount Due</div>' +
        '   <div class="paymentsAmountDue">' + currency + formatMoney(total) + '</div>' +
        '</div>';

    return html;
}
function getSeparator() {

    var html = '<div class="paymentSeparator"></div>';
    return html;
}
function getPaymentsMade(noDelete) {

    var payments = [];
    if (!empty(quote.SalesDiscount.Payments)) {
        payments = quote.SalesDiscount.Payments;
    }

    var html = '';

    for (var a = 0; a < payments.length; a++) {

        var payment = payments[a];
        var deleteHTML = ' ' +
            '   <div class="paymentLineBox">' +
            '      <div class="paymentsLeft deletePayment" onclick="deletePayment(' + payment.id + ', '+ a +')">Delete</div>' +
            '   </div>';


        if (!empty(noDelete)) {
            deleteHTML = '';
        }

        if (isAudit()) {
            deleteHTML = '';
        }

        var verified = '--';
        if (payment.verified == 1) {
            verified = 'Yes';
            // if (!isAccounting()) {
                deleteHTML ='';
            // }
        }
        var paidNote = 'Paid Amount:';
        var paidAmount = payment.amount;
        var red = '';
        if (paidAmount < 0) {
            paidNote = 'Credit Given:';
            paidAmount = -1 * paidAmount ;
            red = 'red';
        }

        var row = '' +
            '<div class="paymentsGroupBox">' +
            '   <div class="paymentLineBox">' +
            '      <div class="paymentsLeft">Payment Date: </div>' +
            '      <div class="paymentsRight">' + payment.date + '</div>' +
            '   </div>' +
            '   <div class="paymentLineBox">' +
            '      <div class="paymentsLeft">'+paidNote+'</div>' +
            '      <div class="paymentsRight ' + red + '">' + currency + formatMoney(paidAmount) + '</div>' +
            '   </div>' +
            '   <div class="paymentLineBox">' +
            '      <div class="paymentsLeft">Payment Type: </div>' +
            '      <div class="paymentsRight">' + payment.kind + ' ' + payment.lastFour + '</div>' +
            '   </div>' +
            '   <div class="paymentLineBox">' +
            '      <div class="paymentsLeft">Verified: </div>' +
            '      <div class="paymentsRight">' + verified + '</div>' +
            '   </div>' +
            '   ' + deleteHTML +
            '</div>';

        html += row;
    }

    return html;
}


function totalPayments() {


    if (empty(quote.SalesDiscount.Payments)) {
        return 0;
    }

    var payments = quote.SalesDiscount.Payments;

    var total = 0;
    for (var a = 0; a < payments.length; a++) {
        total = parseFloat(payments[a].amount) + parseFloat(total);

    }

    return total;

}


function getDepositDue() {

    var deposit = depositDue();
    var paid = totalPayments();

    deposit = deposit - paid;
    if (deposit < 0) {
        deposit = 0;
    }

    var amountDueTitle = 'Deposit Due';
    if (siteDefaults.salesFlow == 2) {
        amountDueTitle = 'Total Amount Due';
    }

    var html = '' +
        '<div class="amountDueBox">' +
        '   <div class="paymentsAmountDueLabel">' + amountDueTitle + '</div>' +
        '   <div class="paymentsAmountDue">' + currency + formatMoney(deposit) + '</div>' +
        '</div>';

    return html;
}
function getBalance() {

    var total = roundNumber(getTotalForSalesAdmin(quote.SalesDiscount));
    var paid = roundNumber(totalPayments());


    total = total - paid;

    var red = '';
    if (total < 0) {
        red = 'class="redText"';
    }

    total = currency + formatMoney(total);

    var html = '' +
        '<div class="amountDueBox">' +
        '   <div class="paymentsAmountDueLabel">Balance Due</div>' +
        '   <div class="paymentsAmountDue"><span ' + red + '>' + total + '</span></div>' +
        '</div>';

    return html;
}
function getPaymentHistory() {

    var paymentHistoryTitle = getPaymentHistoryTitle();
    var amountDue = getAmountDue();
    var paymentsSeparator = getSeparator();
    var paymentsMade = getPaymentsMade();
    var depositDue = getDepositDue();
    var balanceDue = getBalance();


    var html = '' +
        '<div class="paymentBoxRight">' +
        '   ' + paymentHistoryTitle +
        '   ' + amountDue +
        '   ' + paymentsSeparator +
        '   ' + paymentsMade +
        '   ' + depositDue +
        '   ' + paymentsSeparator +
        '   ' + balanceDue +
        '</div>';

    return html;
}


function getPaymentHistoryFlow2() {

    var paymentHistoryTitle = getPaymentHistoryTitle();
    var amountDue = getAmountDue();
    var paymentsSeparator = getSeparator();
    var paymentsMade = getPaymentsMade("noDelete");

    var balanceDue = getBalance();

    var addPaymentSuper = '';

    if ( (user.superToken  && tab == 'viewOrder') || ( tab == 'viewAccountingOrder' && isAccounting())  ) {
        addPaymentSuper = '' +
            '<div id="enterPaymentInfoSuper">Enter Payment Info</div>';
    }



    var html = '' +
        '<div class="paymentBoxRightFlow2">' +
        '   ' + paymentHistoryTitle +
        '   ' + amountDue +
        '   ' + paymentsMade +
        '   ' + paymentsSeparator +
        '   ' + balanceDue +
        '   ' + addPaymentSuper +
        '</div>';

    return html;
}


function buildPaymentOptions() {

    var titles = ['Check', 'Visa', 'MasterCard', 'Amex', 'Cash', "Wire"];
    var html = '';

    for (var a = 0; a < titles.length; a++) {
        var row = '' +
            '<div class="paymentOptionsBox">' +
            '   <div class="paymentImage"><img src="/images/icons/' + titles[a] + '.png" /></div>' +
            '   <div id=' + titles[a] + ' class="paymentLabel">' + titles[a] + '</div>' +
            '</div>';

        html += row;
    }


    return html;


}
function getAddNowButton(data) {

    var html = '<div class="addPaymentButton myGreen">Add Payment</div>';

    return html;
}

function getPaymentInformationTitle() {

    var html = '<div class="customerBoxDetailsTitle">Payment Information</div>';
    return html;
}

function getAmountPaidBox() {

    var amountPaid = getDepositPaid();

    var html = '' +
        '<div class="paymentAmountBox">' +
        '   <div class="paymentRight"> ' +
        '       <input id="paymentAmount" class="billingAmountTextBox" type="text" value="" name="depositAmount">' +
        '   </div>' +
        '   <div class="paymentLeft">Payment Amount:</div>' +
        '</div>';

    return html;
}

function getPaymentDate() {


    var html = '' +
        '<div class="paymentAmountBox">' +
        '   <div class="paymentRight"> ' +
        '       <input  id="paymentDate" class="billingAmountTextBox" type="text" value="' + '' + '" name="depositAmount">' +
        '   </div>' +
        '   <div class="paymentLeft">Payment Date:</div>' +
        '</div>';

    return html;

}
function getPaymentInformationNoteTitle() {

    var html = '<div class="customerBoxDetailsTitle">Order/Payment Notes</div>';
    return html;
}

function getPaymentOrderNotes() {

    var paymentNotes = quote.paymentNotes || '';
    var html = '<textarea placeholder="" rows="10" name="notes" id="paymentOrderNotes" class="paymentOrderNotes">' + paymentNotes + '</textarea>';
    return html;

}

function getEnteringLastFour(data) {


    var html = '' +
        '<div class="lastFourBox"> ' +
        '   <input maxlength="8" id="lastFour" class="creditLastFour" type="text" value="" name="ccLastFour">' +
        '   <div class="creditCardLabel">Check # or Last 4 Digits of Credit Card:</div>' +
        '</div>';

    return html;

}

function getPaymentInformation(data) {


    //var data = quote.Customer;
    //
    var paymentOptions = buildPaymentOptions();

    var paymentLastFour = getEnteringLastFour();
    var paymentInformationTitle = getPaymentInformationTitle();
    var amountPaidBox = getAmountPaidBox();
    var paymentDate = getPaymentDate();
    var addNowButton = getAddNowButton();

    var paymentInformationNoteTitle = getPaymentInformationNoteTitle();
    var paymentOrderNotes = getPaymentOrderNotes();


    var html = '' +
        '<div class="paymentVerticalLine"></div>' +
        '<div class="paymentBoxCenter">' +
        '   ' + paymentInformationTitle +
        '   ' + paymentOptions +
        '   ' + paymentLastFour +
        '   ' + amountPaidBox +
        '   ' + paymentDate +
        '   ' + addNowButton +
        '   ' + paymentInformationNoteTitle +
        '   ' + paymentOrderNotes +
        '   </div>';


    return html;

}


function getPaymentTitleAndButtons() {


    var type = getParameter('type');

    //var saveButton = '<div id="' + quote.id + '-SavePayment" class="editButtons">Save Payment</div>';
    var saveButton = '';

    var closeButton = '<div id="' + quote.id + '-ClosePayment" class="editButtons  myGreen">Close</div>';
    var editButton = '<div id="' + quote.id + '-EditPayment" class="editButtons">Edit Customer</div>';
    editButton = '';
    var title = "Date Required:";

    if (type == 'confirm') {
        closeButton = '<div id="' + quote.id + '-ClosePayment" class="editButtons">Close</div>';
        saveButton = '<div id="' + quote.id + '-PaymentConfirmed" class="editButtons myGreen">Confirm Payment</div>';
        title = 'Confirm Payment and Move To Production';
    }
    if (siteDefaults.salesFlow == 2) {
        editButton = '';
    }

    var html = '' +
        '<div class="title">' + title + '</div>' +
        '<div class="customerViewButtons">' +
        editButton +
        closeButton +
        saveButton +
        '</div>';

    return html;

}

function buildOrdersActions(params) {

    var edit = params.edit;

    var quoteID = params.quote.id;
    var cartItem = getParameter('cartItem');

    var sendToProduction = '<div id="sendToProduction" class="quoteActionButtons extraWide myGreen">Complete and Send To Production</div>';
    if (quote.type == 'Confirming Payment') {
        sendToProduction = '';
    }

    var extraPrintCutSheet = '';

    var addDoor = '<div id="' + quoteID + '-addDoor" class="quoteActionButtons">Add Door</div>';
    if (siteDefaults.salesFlow == 2 || isLimitedAccount()) {
        addDoor = '';
    }
    var deleteOrder = '';

    if (isAdmin()) {
        deleteOrder = '<div id="' + quoteID + '-deleteOrder" class="quoteActionButtons">Delete Order</div>';
    }

    if (productionStarted() && !isAdmin()) {
        addDoor = '';
        deleteOrder = '';
    }

    var printOrdersHTML = '<div id="' + quoteID + '-printOrder" class="quoteActionButtons">Print Order</div>';
    var certPurchase = '<div id="' + quoteID + '-printOrderCert" class="quoteActionButtons">Cert Purchase</div>';
    var emailOrderHTML = '<div id="' + quoteID + '-emailOrder" class="quoteActionButtons">Email Order</div>';

    if (siteDefaults.salesFlow == 2) {
        printOrdersHTML = '<div id="' + quoteID + '-printInvoice" class="quoteActionButtons">Print Order</div>';
        emailOrderHTML = '<div id="' + quoteID + '-emailOrder" class="quoteActionButtons">Email Order</div>';
        if (quote.type == 'Confirming Payment') {
            printOrdersHTML = '<div id="' + quoteID + '-printInvoice" class="quoteActionButtons">Print Order</div>';
            emailOrderHTML = '<div id="' + quoteID + '-emailOrder" class="quoteActionButtons">Email Order</div>';
        } else {
            deleteOrder = '';
        }
    }

    if (isEmptyObject(quote.Cart)) {
        printOrdersHTML = '';
        emailOrderHTML = '';
    }
    var html = '' +
        '<div class="quoteActionsBox">' +
        '   <div class="customerTitle">Choose Actions</div>' +
        '   <div class="customerBox">' +
        '       ' + extraPrintCutSheet +
        '       ' + addDoor +
        '      <div id="' + quoteID + '-closeOrder" class="quoteActionButtons">Close Order</div>' +
        '       ' + printOrdersHTML +
        '       ' + emailOrderHTML +
        '       ' + deleteOrder +
        '       ' + certPurchase +
        '   </div>' +
        '</div>';


    var deleteJob = '';
    if (isAdmin()) {
        deleteJob = '      <div id="' + quoteID + '-deleteJob" class="quoteActionButtons floatRight">Delete Job</div>';
    }

    var glassPrint = '';
    if(siteDefaults.salesFlow === 1 || siteDefaults.salesFlow === 3  ){
        glassPrint = '<div id="' + quoteID + '-printGlassLabels" class="quoteActionButtons productionActionButtons">Print Labels</div>';
    }

    if( siteDefaults.salesFlow === 2  ){
        glassPrint = '<div id="' + quoteID + '-printGlassSheet" class="quoteActionButtons productionActionButtons">Print Glass Sheet</div>';
    }

    if( siteDefaults.salesFlow === 3  ){
        glassPrint = '' +
          '<div id="' + quoteID + '-printGlassLabels" class="quoteActionButtons productionActionButtons">Print Labels</div>' +
          '<div id="' + quoteID + '-printGlassSheet" class="quoteActionButtons productionActionButtons">Print Glass Sheet</div>';
    }


    if (tab == 'viewProductionOrder') {
        html = '' +
            '<div class="quoteActionsBox">' +
            '   <div class="customerTitle">Choose Actions</div>' +
            '   <div class="customerBox">' +
            '      <div id="' + quoteID + '-printCutSheet" class="quoteActionButtons">Print Cut Sheet</div>' +
                    glassPrint +
            '      <div id="' + quoteID + '-printJob" class="quoteActionButtons">Print Job</div>' +
            '      <div id="' + quoteID + '-closeJob" class="quoteActionButtons">Close Job</div>' +
            '      <div id="' + quoteID + '-manifest" class="quoteActionButtons">Manifest</div>' +
            '      <div id="' + quoteID + '-printCollection" class="quoteActionButtons">Delivery Note</div>' +
                    deleteJob +
            '   </div>' +
            '</div>';

        if (quote.type == 'Needs Survey') {
            html = '' +
                '<div class="quoteActionsBox">' +
                '   <div class="customerTitle">Choose Actions</div>' +
                '   <div class="customerBox">' +
                '      <div id="' + quoteID + '-printJob" class="quoteActionButtons">Print Job</div>' +
                '      <div id="' + quoteID + '-closeJob" class="quoteActionButtons">Close Job</div>' +
                '      <div id="' + quoteID + '-manifest" class="quoteActionButtons">Manifest</div>' +
                '      <div id="' + quoteID + '-printCollection" class="quoteActionButtons">Delivery Note</div>' +
                deleteJob +
                '   </div>' +
                '</div>';
        }

        // if (isLimitedAccount()) {
        //     html = '' +
        //         '<div class="quoteActionsBox">' +
        //           '      <div id="' + quoteID + '-closeJob" class="quoteActionButtons">Close Job</div>' +
        //         '</div>';
        // }

    }

    if (edit == "item") {
        if (cartItem == 'formStatus') {
            html = '' +
                '<div class="quoteActionsBox">' +
                '<div class="customerTitle">Choose Actions</div>' +
                '<div class="customerBox">' +
                '<div id="' + quoteID + '-saveToQuote" class="quoteActionButtons addToQuote">Add To Quote</div>' +
                '<div id="' + quoteID + '-cancelQuoteChanges" class="quoteActionButtons">Cancel</div>' +
                '</div>' +
                '</div>';

        } else {
            
            html = '' +
                '<div id="savingQuote" class="quoteActionsBox">' +
                '<div class="customerTitle">Choose Actions</div>' +
                '<div class="customerBox">' +
                '<div id="' + quoteID + '-saveToQuote" class="quoteActionButtons">Save To Quote</div>' +
                '<div id="' + quoteID + '-cancelQuoteChanges" class="quoteActionButtons">Cancel</div>' +
                '</div>' +
                '</div>';

        }


    }

    if (tab == 'viewAccountingOrder' ) {

        var revertOrder = '';
        if (isUK() && !productionStarted()) {
           revertOrder =  '      <div id="' + quoteID + '-revertToQuote" class="quoteActionButtons">Revert to Quote</div>';
        }


        html = '' +
            '<div class="quoteActionsBox">' +
            '   <div class="customerTitle">Choose Actions</div>' +
            '   <div class="customerBox">' +
            '      <div id="' + quoteID + '-closeAccounting" class="quoteActionButtons">Close Order</div>' +
            '   ' + revertOrder +
            '   </div>' +
            '</div>';
    }


    return html;

}


function convertToSale() {
    
    if (siteDefaults.salesFlow == 2) {
        SaveChangesAndConvertFlow2();
    } else {
        if (siteDefaults.salesFlow == 3) {
            convertQuoteToOrderConfirmPayment();
        } else {
            convertQuoteToOrder();
        }
        // sendToConfirmPayment;
        // saveCustomerUpdateWithAction(action);
    }

    // showConfirmCustomer();
    // var quoteNumber = getQuoteNumber();
    // $('#workingOnTitle').html('Save ' + quoteNumber + " as Order");
}


function convertQuoteToOrderConfirmPayment() {


    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/sendToConfirmPayment?" + authorizePlusSalesPerson() + addSite();
    $.getJSON(url, function (apiJSON) {
        var response = apiJSON;

        validate(response);

        quote.version = 1;
        var title = makeQuoteTitle(quote);

        var myAlert = title + " has been converted to an Order";
        
        alert(myAlert);
        trackAndDoClick('viewQuotes');
    });

}


function convertQuoteToOrder() {


    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/convertToOrder?" + authorizePlusSalesPerson() + addSite();
    $.getJSON(url, function (apiJSON) {
        var response = apiJSON;

        validate(response);

        quote.version = 1;
        var title = makeQuoteTitle(quote);

        var myAlert = title + " has been converted to an Order";

        if (siteDefaults.salesFlow == 2) {
            myAlert = title + " has been converted to an Order\n Once payment has been received, it will move to production";
        }
        alert(myAlert);
        trackAndDoClick('viewQuotes');
    });

}


function convertOrderToProduction() {

    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/convertToProduction?" + authorizePlusSalesPerson() + addSite();
    $.getJSON(url, function (apiJSON) {
        var response = apiJSON;

        var title = makeQuoteTitle(quote);

        validate(response)

        var myAlert = title + ' has been sent to production';
        stopSpinner();
        if (siteDefaults.salesFlow == 2) {
            myAlert = 'Congratulations ' + title + ' has been made into an order.';
        }
        alert(myAlert);

        trackAndDoClick('viewOrder', '&order=' + quote.id);
    });
}


function convertOrderToPendingProduction() {

    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/convertToPendingProduction?" + authorizePlusSalesPerson() + addSite() + addSuperToken();
    $.getJSON(url, function (apiJSON) {
        var response = apiJSON;

        var title = makeQuoteTitle(quote);

        validate(response)

        var myAlert = title + ' has been sent to production';
        stopSpinner();
        if (siteDefaults.salesFlow == 2) {
            myAlert = 'Congratulations ' + title + ' has been made into an order.\nOnce payment has been recieved, production for ' + title + ' will start';
            if (empty(siteDefaults.takesCreditCards)) {
                myAlert = 'Congratulations ' + title + ' has been made into an order.';
            }
        }
        alert(myAlert);

        trackAndDoClick('viewOrder', '&order=' + quote.id);
    });
}


function updateQuoteWithPaymentInfo() {

    var total = calculateCostFromDiscount();
    quote.SalesDiscount.Totals.amountPaid = getAmountBySelector('#depositPaid');
    quote.SalesDiscount.Totals.lastFour = getAmountBySelector('#lastFour');
    quote.SalesDiscount.Totals.paymentNote = $('#paymentNote').val();
    quote.SalesDiscount.Totals.total = total;
    quote.SalesDiscount.Totals.depositDue = total / 2;
    quote.SalesDiscount.Totals.paymentDate = now();
    writeQuote(quote);
}


function savePayment() {

    updateQuoteWithPaymentInfo();
    saveOrderToDB();
}


function paymentConfirmed() {

    //updateQuoteWithPaymentInfo();
    var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
    if (!quote.dueDate.match(re)) {
        stopSpinner();
        alert("Date Due must be set before sending order to production");
        return;
    }

    var validationPassed = true;
    $('.fieldUpdate').each(function( index ) {

        if ($( this ).attr('id') != 'billing_address2' && $( this ).attr('id') != 'shipping_address2' ) {
            if ( empty($( this ).val()) ) {
                alert("Please fully fill out Billing and Contact information before sending to production.");
                validationPassed = false;
                return false;
            }
        }
    });


    if (!validationPassed) {
        return;
    }

    //var deposit = depositDue();
    //var paid = totalPayments();
    //
    //
    //var json = JSON.stringify(quote);
    //
    //
    //$.ajax({
    //    url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "?" + authorizePlusSalesPerson(),
    //    data: json,
    //    type: "PUT",
    //    success: function (result) {
    //
    //        var response = jQuery.parseJSON(result);
    //
    //        if (response.status == "Success") {
    convertOrderToPendingProduction();
    //
    //        } else {
    //            alert("Order Save Failed");
    //        }
    //    }
    //});
}

function justSaveOrderToDB(message) {

    if (empty(message)) {
        message = 'Failed to Save Order';
    }
    quote = removeNoCart(quote);
    var json = JSON.stringify(quote);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "?" + authorizePlusSalesPerson() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);

            if (response.status != "Success") {
                stopSpinner();
                alert(message);

            }
        }
    });

}


function justSaveOrderToDBPayment(message) {

    if (empty(message)) {
        message = 'Failed to Save Order';
    }

    quote = removeNoCart(quote);
    var json = JSON.stringify(quote);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/payment?" + authorize() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);

            if (response.status == "Success") {
                updatePaymentHistory();
            } else {
                stopSpinner();
                alert(message);
            }
        }
    });
}