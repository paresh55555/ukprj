'use strict';


function loadAccountingOrders(type) {

    selectTab('#confirmOrders');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();
    //$('#workingOnTitle').html('');
    //$('#headerStartOver').html('')
    //$('#headerStartOverImage').html('');

    updateFormStatus();

    var topButtons = buildAccountTopButtons();
    var searchBox = buildSearchBox();
    $('#preMainContent').html(topButtons + searchBox);
    //$('#preMainContent').html(topButtons );

    //$('#preMainContent').html(searchBox);
    //
    accountingSearch(type);
}


function accountingSearchDelay() {

    var selector = this;
    delay(function () {
        var newSearch = 'yes';
        accountingSearch(tab);
    }, 300);

}

function exportAccounting () {

    var orders  = readLocalStorageWithKey('accountingOrders');

    var csv = convertArrayOfObjectsToCSV({
        data: orders
    });

    if (csv == null) return;

    downloadCSV(csv);
}


function downloadCSV(csv) {
    var data;

    if (!csv.match(/^data:text\/csv/i)) {
        csv = 'data:text/csv;charset=utf-8,' + csv;
    }
    data = encodeURI(csv);

    window.open( data)

}

function decodeEntities(encodedString) {
    var textArea = document.createElement('textarea');
    textArea.innerHTML = encodedString;
    return textArea.value;
}


function convertArrayOfObjectsToCSV(args) {
    var result, ctr, keys, columnDelimiter, lineDelimiter, data;

    data = args.data || null;
    if (data == null || !data.length) {
        return null;
    }

    columnDelimiter = args.columnDelimiter || ',';
    lineDelimiter = args.lineDelimiter || '\n';

    keys = Object.keys(data[0]);

    result = '';
    result += keys.join(columnDelimiter);
    result += lineDelimiter;

    data.forEach(function(item) {
        ctr = 0;
        keys.forEach(function(key) {
            if (ctr > 0) result += columnDelimiter;

            var value = '';
            if (!empty(item[key])) {
                value = item[key].toString();
            }

            var commaSafeValue = value.replace(/,/g, "");
            commaSafeValue = commaSafeValue.replace(/#/g, "");
            commaSafeValue = decodeEntities(commaSafeValue);
            result += commaSafeValue;
            ctr++;
        });
        result += lineDelimiter;
    });

    return result;
}


function accountingSearch(type) {

    var search = $('#search').val();
    var order = getOrder();

    var status;

    if (empty(status)) {
        status = '';
    }

    var url = "https://" + apiHostV1 + "/api/orders/" + type + "?" + authorizePlusAccounting() + addSite() + addSearch() + '&status=' + status + pageLimitText()

    $.getJSON(url, function (apiJSON, status, xhr) {

        var total = xhr.getResponseHeader('Total');

        if(!validate(apiJSON))return;
        var total = apiJSON['total'];
        var data = apiJSON['data'];
        var html = buildConfirmOrders(data);
        $('#mainContent').html(html);
        $('.quoteIcons').unbind("click").bind("click", leadActions);

        $('#cartButton').hide();
        $('.orderStatus').unbind("click").bind("click", tabClick);

        $('.export').unbind("click").bind("click", exportAccounting);


        setPaginationWithSelectorAndTotal('#pagination', total);

        showContentAndSaveState();

        $('#search').keyup(accountingSearchDelay);
    });

    //
    //var url = "https://" + apiHostV1 + "/api/orders?" + authorizePlusSalesPerson() +
    //    '&status=' + status + pageLimitText() + order + addSite() + '&search=' + search + '&fullStatus=' + siteDefaults.showFullOrderStatus
    //    + addSuperToken();
    //
    //$.getJSON(url, function (apiJSON, status, xhr) {
    //
    //    var total = xhr.getResponseHeader('Total');
    //    validate(apiJSON);
    //
    //    var html = buildViewOrders(apiJSON);
    //
    //    $('#mainContent').html(html);
    //
    //    if (siteDefaults.salesFlow == 1) {
    //        $(".orderDateBox").datepicker({gotoCurrent: true, minDate: 0});
    //    }
    //    $('.orderIconsActions').unbind("click").bind("click", orderActions);
    //    $('.orderStatus').unbind("click").bind("click", viewOrderBasedOnStatus);
    //    $('.sortable').unbind("click").bind("click", orderTableResultsWithTab);
    //
    //    $('#preMainContent').show();
    //    setPaginationWithSelectorAndTotal('#pagination', total);
    //    showContentAndSaveState();
    //
    //});
}


function loadConfirmOrders() {


    setTab('viewPaidOrder');
    selectTab('#confirmOrders');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();


    $('#workingOnTitle').html('');
    $('#headerStartOver').html('')
    $('#headerStartOverImage').html('');

    updateFormStatus();


    var url = "https://" + apiHostV1 + "/api/orders/confirm?" + authorizePlusAccounting() + addSite();

    $.getJSON(url, function (apiJSON) {

        validate(apiJSON);
        var html = buildConfirmOrders(apiJSON);
        $('#mainContent').html(html);
        $('.quoteIcons').unbind("click").bind("click", leadActions);

        $('#cartButton').hide();

        showContentAndSaveState();
    });

}


function buildConfirmOrders(data) {

    var pagination = accountingPagination();
    var titles = buildAccountingRowTitles();
    var rows = buildAccountingRows(data);

    var totalsHTML = buildAccountingTotals(data);


    var html =
        pagination +
        totalsHTML +
        '<div class="finalRow"></div>' +
        '   <div id="subTabAlertAccounting" class=' + tab + '></div>' +
        titles +
        rows +
        '<div class="finalRow"></div>' +
        '<div class="accountingSpace"></div>' +
        totalsHTML +
        '<div class="finalRow"></div>' +
        '<div class="accountingSpace"></div>';


    return html;
}


function buildAccountingTotals(data) {

    var totalBalance = 0;
    var jobTotal = 0;
    for (var a = 0; a < data.length; a++) {
        var quote = data[a];
        quote.balanceDue = getBalanceDue(quote);
        // jobTotal = jobTotal +  roundNumber(Number(quote.SalesDiscount.Totals.cost.amount));
        jobTotal = jobTotal + roundNumber(quote.total);
        totalBalance = Number(quote.balanceDue) + totalBalance
    }

    var canExport = '   <div class="accountCell11 borderLeftRightTop" ></div>' ;

    // if (isAudit()) {
        canExport = '   <div class="accountCell11 borderLeftRightTop export" >Export</div>' ;
    // }

    var html =
        '<div class="accountRowTotals">' +
        '   <div class="accountCell1Clean borderLeftTop">' +
        '       <div class="stackedText">Total Jobs</div>' +
        '       <div class="stackedText">' + data.length + '</div>' +
        '   </div>' +
        '   <div class="accountCell2 borderLeftTop">&nbsp;</div>' +
        '   <div class="accountCell3 borderLeftTop">&nbsp;</div>' +
        '   <div class="accountCell4 borderLeftTop">Revenue Total:</div>' +
        '   <div class="accountCell5 borderLeftTop"><B>' + currency + formatMoney(jobTotal) + '</B></div>' +
        '   <div class="accountCell7 borderLeftTop">&nbsp;</div>' +
        '   <div class="accountCell8 borderLeftTop">&nbsp;</div>' +
        '   <div class="accountCell9 borderLeftTop">' +
        '       <div class="stackedText">Balance Due</div>' +
        '       <div class="stackedText">Total</div>' +
        '   </div>' +
        '   <div class="accountCell9 borderLeftTop"><B>' + currency + formatMoney(totalBalance) + '</B></div>' +
        '   <div class="accountCell10 borderLeftTop">&nbsp;</div>' +
        canExport +
        '</div>';

    return html;


}
function accountingPagination() {

    var limitPullDown = buildLimitList();
    var pagination = '' +
        '<div id="paginationBox">' +
        '   <div id="limitPullDown" ><div id="limitPullDownText">Show: </div>' + limitPullDown + '</div> ' +
        '   <div id="pagination"></div>' +
        '</div>';

    return pagination;
}


function buildAccountingRows(data) {

    var ordersAndHTML;
    var rows = '';
    var orders = [];

    writeLocalStorageWithKeyAndData('accountingOrders', []);


    for (var a = 0; a < data.length; a++) {
        var quote = data[a];
        var ordersAndHTML = buildAccountingRow(quote);

        rows = rows + ordersAndHTML.html;
        orders.push(ordersAndHTML.order);

    }

    writeLocalStorageWithKeyAndData('accountingOrders', orders);

    return rows;
}


function buildAccountTopButtons() {

    var verifyAction = '' +
        '   <div class="stackedTextMenu">Verify</div>' +
        '   <div class="stackedTextMenu">Deposit</div>' ;

    if (isUK()) {
        verifyAction = '' +
            '   <div class="stackedTextMenu">Confirm</div>' +
            '   <div class="stackedTextMenu">Order</div>' ;
    }

    var survey = isUK() ? 'survey' : '';
    var html = '' +
        '<div class="verticalSpaceSlim"></div>' +
        '<div id="accountingAll" class="orderStatus ' + survey + ' orderStatusSmall accoutingAll">ALL</div>' +
        '<div id="verifyDeposit" class="orderStatus ' + survey + ' depositDue">' +
        '   ' + verifyAction +
        '</div>' +
        (isUKsurvey() ? '<div id="accountingNeedsSurvey" class="orderStatus ' + survey + ' prodNeedsSurvey">Needs Survey</div>' : '') +
        '<div id="accountingPreProduction" class="orderStatus ' + survey + ' prodPreProduction">Pre-Production</div>' +
        '<div id="accountingProduction" class="orderStatus ' + survey + ' readyForProduction">Production</div>' +
        '<div id="accountingCompleted" class="orderStatus ' + survey + ' prodCompleted ">Completed</div>' +
        '<div id="balanceDue" class="orderStatus ' + survey + ' balanceDue ">' +
        '   <div class="stackedTextMenu">Verify</div>' +
        '   <div class="stackedTextMenu">Final Payment</div>' +
        '</div>' +
        '<div id="accountingDelivered" class="orderStatus ' + survey + ' prodDelivered ">Delivered</div>' +
        '<div id="accountingHold" class="orderStatus ' + survey + ' prodHold ">Hold</div>';


    return html;
}


function buildAccountingRowTitles() {

    var moneyTitleHTML = moneyTitle();

    
    var payment = ''+
        '   <div class="accountCell7 borderLeftTop">Deposit</div>' +
        '   <div class="accountCell8 borderLeftTop">' +
        '       <div class="accountDoubleTitle">Final</div>' +
        '       <div class="accountDoubleTitle">Payment 1</div>' +
        '   </div>' +
        '   <div class="accountCell9 borderLeftTop">' +
        '       <div class="accountDoubleTitle">Final</div>' +
        '       <div class="accountDoubleTitle">Payment 2</div>' +
        '   </div>' ;
    
    if (isUK()) {
        payment  = '   <div class="ukPaymentSpecial borderLeftTop">Payments</div>';
    }
    
    
    var html =
        '<div class="accountRowTitle">' +
        '   <div class="accountCell1 borderLeftTop">Order #</div>' +
        '   <div class="accountCell2 borderLeftTop">Last Name</div>' +
        '   <div class="accountCell3 borderLeftTop"> First Name</div>' +
        '   <div class="accountCell4 borderLeftTop">Company</div>' +
        '   <div class="accountCell5 borderLeftTop">Job Total</div>' +
        '   ' + payment +
        '   <div class="accountCell9 borderLeftTop">Balance Due</div>' +
        '   ' + moneyTitleHTML +
        '   <div class="accountCell11 borderLeftRightTop" >&nbsp;</div>' +
        '</div>';
    
    

    return html;
}


function moneyTitle() {

    var moneyColumn = '' +
        '   <div class="accountCell10 borderLeftTop">' +
        '       <div class="accountDoubleTitle">Paid In</div>' +
        '       <div class="accountDoubleTitle">Full</div>' +
        '   </div>';

    if (tab == 'balanceDue') {
        moneyColumn = '' +
            '   <div class="accountCell10 borderLeftTop">' +
            '       <div class="accountDoubleTitle">Is Paid</div>' +
            '       <div class="accountDoubleTitle">In Full</div>' +
            '   </div>';
    }
    if (tab == 'verifyDeposit') {
        moneyColumn = '' +
            '   <div class="accountCell10 borderLeftTop">' +
            '       <div class="accountDoubleTitle">Deposit</div>' +
            '       <div class="accountDoubleTitle">Satisfied</div>' +
            '   </div>';

        if (isUK()) {
            moneyColumn = '' +
                '   <div class="accountCell10 borderLeftTop">' +
                '       <div class="accountDoubleTitle">Confirm</div>' +
                '       <div class="accountDoubleTitle">Order</div>' +
                '   </div>';
        }
    }

    return moneyColumn;
}


function buildAccountAction(quote) {

    var paidInFull = "--";
    var revertPaidInFull = '&nbsp;';


    if (quote.paidInFull == "yes") {
        paidInFull = "Yes";
        revertPaidInFull = '' +
            '<div class="accountMultipleRows">' +
            '   <div class="accountDoubleRow">Revert Paid</div>' +
            '   <div class="accountDoubleRow">In Full</div>' +
            '</div>';
    }

    var long = '';
    if (isUK()) {
        long = 'long'
    }
    var action = '' +
        '<div class="accountCell10 borderLeftTop '+long+'"  >' + paidInFull + '</div>' +
        '<div class="accountCell11 borderLeftRightTop ' + (revertPaidInFull === '&nbsp;' ? '' : 'confirmPaymentLink') +long+'" id="sq-' + quote.id + '">' +
        '   <div ' + (revertPaidInFull === '&nbsp;' ? '' : 'onclick="revertPaidInFull(' + quote.id + ')"') + ' class="accountAction" >' +
        '   ' + revertPaidInFull +
        '   </div>' +
        '</div>';

    if (tab == 'balanceDue') {
        var yesNoConfirm = buildConfirmFullPaymentPullDown(quote);

        action = '' +
            '<div class="accountCell10 borderLeftTop '+long+'"  >' + yesNoConfirm + '</div>' +
            '<div class="accountCell11 borderLeftRightTop confirmPaymentLink '+long+'" id="sq-' + quote.id + '">' +
            '   <div class="accountAction" >&nbsp;</div>' +
            '</div>';
    }


    if (tab == 'verifyDeposit') {
        var yesNoConfirm = buildConfirmOrderPullDown(quote.id);

        if (user.type == 'audit') {
            yesNoConfirm = 'No';
        }

        action = '' +
            '<div class="accountCell10 borderLeftTop '+long+'"  >' + yesNoConfirm + '</div>' +
            '<div class="accountCell11 borderLeftRightTop confirmPaymentLink '+long+'" id="sq-' + quote.id + '">' +
            '   <div class="accountAction" >&nbsp;</div>' +
            '</div>';
    }


    if (tab == 'accountingPreProduction' || tab == 'accountingNeedsSurvey') {

        var actionToTake = '' +
        '           <div class="accountDoubleRow">Revert Deposit</div>' +
        '           <div class="accountDoubleRow">Satisified</div>' ;
        if (isUK()) {
            actionToTake = '' +
                '           <div class="accountDoubleRow">Revert Order</div>' +
                '           <div class="accountDoubleRow">Confirmed</div>' ;
        }

        action = '' +
            '<div class="accountCell10 borderLeftTop '+long+'">' + paidInFull + '</div>' +
            '<div class="accountCell11 borderLeftRightTop confirmPaymentLink '+long+'" id="sq-' + quote.id + '">' +
            '   <div class="accountMultipleRows">' +
            '       <div onclick="revertDepositSatisfied(' + quote.id + ')" class="accountAction" >' +
            '       ' + actionToTake +
            '       </div>' +
            '   </div>' +
            '</div>';
    }

    return action;
}

function buildAccountingRow(quote) {


    var quoteTitle = makeQuoteTitle(quote);
    var rowColor = getAccountingRowColor(quote);

    var actionHTML = buildAccountAction(quote);

    if (empty(quote.SalesDiscount.Payments)) {
        quote.SalesDiscount.Payments = [];
    }


    var payment1 = getPaymentAmountForIndex(1, quote);
    var payment2 = getPaymentAmountForIndex(2, quote);
    var payment3 = getPaymentAmountForIndex(3, quote);
    var payment4 = getPaymentAmountForIndex(4, quote);
    var payment5 = getPaymentAmountForIndex(5, quote);
    var payment6 = getPaymentAmountForIndex(6, quote);


    var companyName = handleBlankText(quote.companyName);
    var firstName = handleBlankText(quote.firstName);
    var lastName = handleBlankText(quote.lastName);


    var ces = '';
    if (!empty(quote.ces)) {
        ces = 'yes';
    }

    var onClick = 'onClick="trackAndDoClick(\'viewAccountingOrder\',\'&secondTab=' + tab + '&ces='+ ces+'&order=' + quote.id + '\')"';

    var amountDue = currency + formatMoney(quote.total / 2);
    if (siteDefaults.salesFlow == 2) {
        amountDue = currency + formatMoney(quote.total);
    }
    if (siteDefaults.salesFlow == 3) {
      amountDue = currency + formatMoney(quote.total / 2);
   }

    quote.balanceDue = getBalanceDue(quote);

    var depositPaid = "No";
    if ((quote.total / 2) <= (quote.total - quote.balanceDue )) {
        depositPaid = "Yes"
    }

    if (siteDefaults.salesFlow == 3) {
        if ((quote.total * .5) <= (quote.total - quote.balanceDue )) {
            depositPaid = "Yes"
        }
    }

    var total = quote.total;
    var long = '';

    var paymentHTML = ''+
        '   <div class="accountCell7 borderLeftTop"  >' + payment1 + '</div>' +
        '   <div class="accountCell8 borderLeftTop"  >' + payment2 + '</div>' +
        '   <div class="accountCell9 borderLeftTop"  >' + payment3 + '</div>' ;

    if (isUK()) {
        long = 'long';
        paymentHTML = '' +
            '<div class="ukPaymentSpecial borderLeftTop "  >' +
            '   <div class="accountCellUKPayment borderLeftTop "  >' + payment1 + '</div>' +
            '   <div class="accountCellUKPayment borderLeftTop"  >' + payment2 + '</div>' +
            '   <div class="accountCellUKPayment borderLeftTop"  >' + payment3 + '</div>' +
            '   <div class="accountCellUKPayment borderLeftTop"  >' + payment4 + '</div>' +
            '   <div class="accountCellUKPayment borderLeftTop"  >' + payment5 + '</div>' +
            '   <div class="accountCellUKPayment borderLeftTop"  >' + payment6 + '</div>' +
            '</div>' ;
    }

    var row = '' +
        '<div class="accountRow">' +
        '   <div class="accountCell1 borderLeftTop  ' + long +' ' + rowColor + '"  ' + onClick + ' >' + quoteTitle + '</div>' +
        '   <div class="accountCell2 borderLeftTop '+long+'"  >' + lastName + '</div>' +
        '   <div class="accountCell3 borderLeftTop '+long+'"  >' + firstName + '</div>' +
        '   <div class="accountCell4 borderLeftTop '+long+'"  >' + companyName + '</div>' +
        '   <div class="accountCell5 borderLeftTop '+long+'"  ><b>' + currency + formatMoney(total) + '</b></div>' +
        '   ' + paymentHTML +
        '   <div class="accountCell9 borderLeftTop '+long+'"  ><b>' + currency + formatMoney(quote.balanceDue) + '</b></div>' +
        '   ' + actionHTML +
        '</div>';


    var order = {
        "Order Date ": cleanDate(quote.orderDate),
        "Delivery Date" :cleanDate(quote.delivered),
        "Order Number":quoteTitle,
        "Status": quote.type,
        "Last Name":quote.lastName,
        "First Name": quote.firstName,
        "Company": quote.companyName,
        "City": bestCity(quote),
        "State": bestState(quote),
        "Job Total": quote.total,

        "Deposit Date": cleanDate(getPaymentDateForAccounting(1, quote)),
        "Deposit Type" : getPaymentTypeForAccounting(1, quote),
        "Deposit Amount" : getPaymentAmount(1, quote),


        "Final Payment 1 Date": cleanDate(getPaymentDateForAccounting(2, quote)),
        "Final Payment 1 Type": getPaymentTypeForAccounting(2, quote),
        "Final Payment 1 Amount": getPaymentAmount(2, quote),

        "Final Payment 2 Date": cleanDate(getPaymentDateForAccounting(3, quote)),
        "Final Payment 2 Type": getPaymentTypeForAccounting(3, quote),
        "Final Payment 2 Amount": getPaymentAmount(3, quote),

        "Final Payment 3 Date": cleanDate(getPaymentDateForAccounting(4, quote)),
        "Final Payment 3 Type": getPaymentTypeForAccounting(4, quote),
        "Final Payment 3 Amount": getPaymentAmount(4, quote),

        "Final Payment 4 Date": cleanDate(getPaymentDateForAccounting(5, quote)),
        "Final Payment 4 Type": getPaymentTypeForAccounting(5, quote),
        "Final Payment 4 Amount": getPaymentAmount(5, quote),


        "Balance Due": roundNumber(quote.balanceDue)
    };

    var data = {"html":row,"order":order };

    return data;
}

function bestCity(quote) {

    if (!empty(quote.city1)) {
        return quote.city1;
    }
    if (!empty(quote.city2)) {
        return quote.city2;
    }
    if (!empty(quote.city3)) {
        return quote.city3;
    }

    return '--';
}

function bestState(quote) {
    if (!empty(quote.state1) && quote.state1 !="Select State") {
        return quote.state1;
    }
    if (!empty(quote.state2)&& quote.state2 !="Select State") {
        return quote.state2;
    }
    if (!empty(quote.state3)&& quote.state3 !="Select State") {
        return quote.state3;
    }

    return '--';

}

function getAccountingRowColor(quote) {

    var rowColor = '';
    if (quote.type == 'Completed') {
        rowColor = 'prodCompleted';
    } else if (quote.type == 'Delivered') {
        rowColor = 'prodDelivered';
    } else if (quote.type == 'In Production') {
        rowColor = 'prodProduction';
    } else if (quote.type == 'Ready For Production') {
        rowColor = 'prodPreProduction';
    } else if (quote.type == 'Confirming Payment') {
        rowColor = 'verifyDeposit';
    } else if (quote.type == 'Needs Survey') {
        rowColor = 'prodNeedsSurvey';
    }

    if (tab == 'balanceDue') {
        rowColor = 'balanceDue';
    }

    if (quote.status == 'hold') {
        rowColor = 'prodHold';
    }

    return rowColor;

}

function getBalanceDue(quote) {

    var total = parseFloat(quote.total);
    var payment1 = getPaymentAmount(1, quote);
    var payment2 = getPaymentAmount(2, quote);
    var payment3 = getPaymentAmount(3, quote);
    var payment4 = getPaymentAmount(4, quote);
    var payment5 = getPaymentAmount(5, quote);
    var payment6 = getPaymentAmount(6, quote);

    var amountLeft = total - (payment1 + payment2 + payment3 + payment4 + payment5 + payment6);

    return amountLeft;
}

function getDepositDueAccountingOrders(quote) {

    var total = parseFloat(quote.total);
    var deposit = total / 2;

    //Per Louise on 09/25/2018
    // if (isUK()) {
    //     deposit = total * (.45);
    // }

    var payment1 = getPaymentAmount(1, quote);
    var payment2 = getPaymentAmount(2, quote);
    var payment3 = getPaymentAmount(3, quote);
    var payment4 = getPaymentAmount(4, quote);
    var payment5 = getPaymentAmount(5, quote);
    var payment6 = getPaymentAmount(6, quote);

    var amountLeft = deposit - payment1 - payment2 - payment3 - payment4 - payment5 - payment6;

    if (amountLeft < 0) {
        amountLeft = 0;
    }

    return amountLeft;
}


function getPaymentAmount(index, quote) {

    if (!empty(quote.SalesDiscount.Payments[index - 1])) {
        if (!empty(quote.SalesDiscount.Payments[index - 1].amount)) {
            var amount = quote.SalesDiscount.Payments[index - 1].amount;
            return parseFloat(amount);
        }
    }

    return parseInt(0);
}

function getPaymentDateForAccounting(index, quote) {

    if (!empty(quote.SalesDiscount.Payments[index - 1])) {
        if (!empty(quote.SalesDiscount.Payments[index - 1].date)) {
            var date = quote.SalesDiscount.Payments[index - 1].date;
            return date;
        }
    }

    return '--';
}

function getPaymentTypeForAccounting(index, quote) {

    if (!empty(quote.SalesDiscount.Payments[index - 1])) {
        if (!empty(quote.SalesDiscount.Payments[index - 1].kind)) {
            var kind = quote.SalesDiscount.Payments[index - 1].kind;
            if (!empty(quote.SalesDiscount.Payments[index - 1].lastFour)) {
                kind = kind+' '+quote.SalesDiscount.Payments[index - 1].lastFour;
            }
            return kind;
        }
    }

    return '--';
}

function getPaymentIdForAccounting(index, quote) {

    if (!empty(quote.SalesDiscount.Payments[index - 1])) {
        if (!empty(quote.SalesDiscount.Payments[index - 1].id)) {
            var id = quote.SalesDiscount.Payments[index - 1].id;
            return id;
        }
    }

    return '';
}


function getPaymentVerifiedForAccounting(index, quote) {

    if (!empty(quote.SalesDiscount.Payments[index - 1])) {
        if (!empty(quote.SalesDiscount.Payments[index - 1].verified)) {
            var verified = quote.SalesDiscount.Payments[index - 1].verified;
            return verified;
        }
    }

    return 0;
}

function verifyPaymentAccounting(id) {

    var payment = {};
    payment.id = id;

    $.when(
        apiVerifiedPayment(payment)
    ).done(function () {
        var stuff = $('#'+id).html();
        var amountHTML = '' +
            getNotVerifyPaymentAccountingOnClick(id) +
            '   ' + stuff +
            '</div>';
        $('#'+id).replaceWith(amountHTML);
        $('#'+id+'-link').html('Click to Revert');
    });

}

function notVerifyPaymentAccounting(id) {

    var payment = {};
    payment.id = id;

    $.when(
        apiNotVerifiedPayment(payment)
    ).done(function () {
        var stuff = $('#'+id).html();
        var amountHTML = '' +
            getVerifyPaymentAccountingOnClick(id) +
            '   ' + stuff +
            '</div>';
        $('#'+id).replaceWith(amountHTML);
        $('#'+id+'-link').html('Click to Verify');
    });

}

function limitedAccessAccount() {

    var alertMessage = "This is a limited access account, action not performed";

    alert(alertMessage);
}

function getVerifyPaymentAccountingOnClick(id) {

    var html = '<div id = "' + id + '" class="accountMultipleRows verifiedPaymentLink" onclick="verifyPaymentAccounting('+id+')">';
    if (isAccountingLimited()) {
        html = '<div id = "' + id + '" class="accountMultipleRows verifiedPaymentLink" onclick="limitedAccessAccount()" >';
    }


    return html;
}

function getNotVerifyPaymentAccountingOnClick(id) {

    var html = '<div id = "' + id + '" class="accountMultipleRows verified verifiedPaymentLink"  onclick="notVerifyPaymentAccounting('+id+')">' ;

    if (isAccountingLimited()) {
        html = '<div id = "' + id + '" class="accountMultipleRows verified verifiedPaymentLink" onclick="limitedAccessAccount()" >' ;
    }

    return html;
}

function getPaymentAmountForIndex(index, quote) {

    var amount = getPaymentAmount(index, quote);
    if (empty(amount)) {
        return '--';
    }

    var amountFormatted = currency + formatMoney(amount, 2, '.', ',');
    var paymentDate = getPaymentDateForAccounting(index, quote);
    var paymentKind = getPaymentTypeForAccounting(index, quote);
    var verified = getPaymentVerifiedForAccounting(index, quote);
    var id = getPaymentIdForAccounting(index, quote);

    var amountHTML = '' +
        getVerifyPaymentAccountingOnClick(id) +
        '   <div class="accountTripleRow"><b>' + amountFormatted +'</b></div>' +
        '   <div class="accountTripleRow">' + paymentDate + '</div>' +
        '   <div class="accountTripleRow">' + paymentKind + '</div>' +
        '   <div  id = "' + id + '-link" class="accountTripleRow " > Click to Verify </div>' +
        '</div>' ;


    if (verified == 1) {
        amountHTML = '' +
            getNotVerifyPaymentAccountingOnClick(id) +
            '   <div class="accountTripleRow"><b>' + amountFormatted +'</b></div>' +
            '   <div class="accountTripleRow">' + paymentDate + '</div>' +
            '   <div class="accountTripleRow">' + paymentKind + '</div>' +
            '   <div id = "' + id + '-link" class="accountTripleRow "> Click to Revert </div>' +
            '</div>' ;
    }

    return amountHTML;
}


function viewAccountingOrder() {

    var order = getParameter('order');

    var url = "https://" + apiHostV1 + "/api/orders/" + order + "?" + authorizePlusAccounting() + '&accounting=yes' + addSite();
    
    loadViewOrder(url);

}
function revertToConfirm(id) {

    if (confirm("Are you sure you want to remove this \norder from the production que?") == true) {

        var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/sendToConfirmPayment?" + authorizePlusAccounting() + addSite();
        $.getJSON(url, function (apiJSON) {
            var response = apiJSON;

            validate(response)
            trackAndDoClick(tab);
        });
    }

}


function revertPayment(id) {

    $.when(
        getRevertFullPayment(id)
    ).done(function () {
        trackAndDoClick(tab);
    });
}


function revertDepositSatisfied(id) {

    if (isAccountingLimited()) {
        limitedAccessAccount();
        return
    }
    var message = "Are you sure you want to revert deposit satisfied?\nThis will pull the order out of the production queue.";
    if (isUK()) {
        message = "Are you sure you want to revert order confirmed?\nThis will pull the order out of the production queue.";
    }
    if (confirm(message) == true) {
        revertPayment(id);
    }
}


function beginConfirmPayment(id) {

    var yesNo = $('#pullDown-' + id).val();

    var nextAction = 'Deposit Satisfied';
    if (isUK()) {
        nextAction = 'Order Confirmed';
    }

    var html = '<div class="accountAction" onClick="confirmPayment(' + id + ')" >' + nextAction + '</div>';

    if (isAccountingLimited()) {
        html = '<div class="accountAction" onClick="limitedAccessAccount()" >' + nextAction + '</div>';
    }
    if (yesNo != 'Yes') {
        html = '<div class="accountAction" ></div>';
    }

    $('#sq-' + id).html(html);


}


function beginConfirmFullPayment(id, balanceDue, email) {

    var yesNo = $('#pullDown-' + id).val();

    var html = '<div class="accountAction" onClick="markFullPaymentCheckingAmountDue(' + id + ',' + balanceDue + ', \'' + email +'\')" >Mark Paid In Full</div>';

    if (isAccountingLimited()) {
        html = '<div class="accountAction" onClick="limitedAccessAccount()" >Mark Paid In Full</div>';

    }
    if (yesNo != 'Yes') {
        html = '<div class="accountAction" ></div>';
    }

    $('#sq-' + id).html(html);


}


function confirmPayment(id) {

    startSpinner();

    // Setting this via the database and by module, first in the cart
    // var method = (isUKsurvey()) ? 'sendToNeedsSurvey' : 'sendToProduction'
    var method = 'sendToNeedsSurvey';

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/" + method + "?" + authorizePlusAccounting() + addSite();
    $.getJSON(url, function (apiJSON) {
        var response = apiJSON;

        if (siteDefaults.salesFlow == '2') {

            var params = {
                "from": siteDefaults.salesEmail,
                "address": ["fracka@fracka.com"],
                "message": ' ',
                "subject": "Invoice Paid",
                "quote": id,
                "token": user.token
            };
            //var json = JSON.stringify(params);
            //
            //url = "https://" + apiHostV1 + "/api/quotes/" + id + "/emailInvoice?" + authorizePlusAccounting() + addSite();
            //$.post(url, json, function (data) {
                stopSpinner();
                trackAndDoClick(tab);

            //});
        } else {

            trackAndDoClick(tab);
        }

    });

}

function markFullPaymentCheckingAmountDue(id, balanceDue, email) {
    if (balanceDue > 0 ) {

        if (confirm("A Balance Due of " + currency + formatMoney(balanceDue) + " is still shown, are you sure you want to mark as Paid in Full?") == true) {
            if (siteDefaults.salesFlow == 2) {
                markFullPaymentAndEmailInvoice(id, email);
            } else {
                markFullPayment(id);
            }
        }
    } else {
        if (siteDefaults.salesFlow == 2) {
            markFullPaymentAndEmailInvoice(id, email);
        } else {
            markFullPayment(id);
        }
    }
}


function markFullPayment(id) {

    startSpinner();

    setPaidInFull(id).done(function (result) {
        trackAndDoClick(tab);
    });
}

function markFullPaymentAndEmailInvoice(id, email) {

    startSpinner();

    var config = {};
    config.id = id;
    config.token = user.token;
    config.site = site;
    config.email = email;
    var json = JSON.stringify(config);

    postPaidInFullAndEmailInvoice(id, json).done(function (result) {
        trackAndDoClick(tab);
    });
}




function revertPaidInFull(id) {

    if (isAccountingLimited()) {
        limitedAccessAccount();
        return;
    }
    startSpinner();

    setRevertPaidInFull(id).done(function (result) {
        trackAndDoClick(tab);
    });
}

function buildConfirmOrderPullDown(id) {

    var html =
        '<select id="pullDown-' + id + '" class="ordersPullDown" onchange="beginConfirmPayment(' + id + ')">' +
        '   <option value="No">No</option>' +
        '   <option value="Yes">Yes</option>' +
        '</select>';


    return html;

}

function buildConfirmFullPaymentPullDown(quote) {
    quote.balanceDue = getBalanceDue(quote);

    var html =
        '<select id="pullDown-' + quote.id + '" class="ordersPullDown" onchange="beginConfirmFullPayment(' + quote.id + ', ' + quote.balanceDue + ', \'' + quote.email +'\')">' +
        '   <option value="No">No</option>' +
        '   <option value="Yes">Yes</option>' +
        '</select>';

    return html;
}
