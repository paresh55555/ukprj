
function isUKsurvey() {

    if (isUK() ) {
        return true;
    }

    return false;
}

function isUKTrade() {

    var status = false
    if ( siteName == 'panoramicdoorsUKtrade') {
        status = true
    }

    return status;
}

function isProductionSite() {

    if (window.location.hostname.toLowerCase() == 'panoramicdoors.salesquoter.co.uk' ||
        window.location.hostname.toLowerCase() == 'panoramicdoors.salesquoter.com'  ||
        window.location.hostname.toLowerCase() == 'panoramicgroup.salesquoter.co.uk' )  {

        return true;
    }
    return false;
}

function isUKGroup() {


    return true; //Hack for now
    if (siteName == 'panoramicGroup') {
        return true;
    }

    return false;
}


function isUK() {
    var location = getParameter('location');

    var status = false
    if (window.location.hostname.toLowerCase() == 'dev-pduk.salesquoter.com' ||
        window.location.hostname.toLowerCase() == 'pduk.salesquoter.co.uk' ||
        window.location.hostname.toLowerCase() == 'panoramicdoorsuk.ifracka.com' ||
        window.location.hostname.toLowerCase() == 'panoramicdoors.salesquoter.co.uk' ||
        window.location.hostname.toLowerCase() == 'local-flow3.rioft.com' ||
        window.location.hostname.toLowerCase() == 'dev-flow3.rioft.com' ||
        window.location.hostname.toLowerCase() == 'qa-flow3.rioft.com' ||
        window.location.hostname.toLowerCase() == 'dev-pduk.salesquoter.com' ||
        location == 'UK' ||
        siteName == 'panoramicdoorsUK' ||
        siteName == 'panoramicdoorsUKtrade' ||
        siteName == 'panoramicGroup') {
        status = true
    }
    return status;
}


function allowGuests() {
    
    if (siteDefaults.allowGuest == '1') {
        return true;
    }

    return false
}


function isMagnaline() {

    if (siteName == 'magnalineSystems' ) {
        return true;
    }
    return false;
}
function setCurrency() {

    if (isUK()) {
        currency = '\u00A3';
        uom = 'mm';
        uomText = 'mm';
        tax = 'VAT';
        colorText = 'colour';
        city = 'Town / City';
        state = 'County';
        zipText = 'Post Code';
        zipShort = 'P. C.'
        shipping = 'Delivery';
        salesTax = "VAT";
        siteURL = 'panoramicdoors.co.uk';
    }

}

function siteFromPath() {

    var uri = '';
    if (!empty(location.pathname)) {
        uri = location.pathname;
        uri = uri.replace(/\//g, '');

        site= uri;
    }


    return uri
}


function siteFromURL() {

    var newSite = getParameter('site');

    if (!empty(newSite)) {
        site = newSite;
    }else if (!empty(location.host)) {
        site = location.host;
    }
}


function setSite() {

    var uri = siteFromPath();

    if (!empty(uri)) {
        site = uri;
    } else if (isUK()) {
        site = 'uk';
    } else {
        site = getParameter('site');
        if (empty(site)) {
            site = 'sq';
        }
    }
}


function addSite() {

    return '&site='+site;
}


function addSuperToken() {

    if (!empty(user.superToken)) {

        return '&superToken='+user.superToken;
    }

    return '';
}


function cssForBrowser() {

    if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
        $("html").addClass("safari");
    }

}
function setDiscounts() {
    var discounts = quote.SalesDiscount;

    return discounts;
}

function loadDataForAccounting() {

    renderPage();
}


function loadDataForProduction() {

    renderPage();
}

function loadDataForLeads() {

    renderPage();
}

function loadDataForGuest() {

    if (siteDefaults.allowGuest != '1') {

        loadDataForSalesPerson();
    }

    renderPage();
}

function prepForNewGuest() {

    quote = {};
    formStatus = formStatusDefaults();
    defaults = {};
    discount = jQuery.extend({}, defaults);
    writeLead({});
}


function loadDataForNewGuest() {


    user = {'type': 'guest'};
    writeUser(user);
    prepForNewGuest();
    var url = "https://" + apiHostV1 + "/?tab=guest";;
    location.replace(url);
}


function loadDataForSalesPerson() {

    if (empty(defaults)) {
        resetSalesPerson();
    } else if (empty(defaults.Totals)) {
        resetSalesPerson();
    } else if (empty(defaults.Discounts)) {
        resetSalesPerson();
    }

    renderPage();
}


function resetSalesPerson() {

    prepForNewGuest();
    user = {'type': 'salesPerson'};


}



function windowFormDefaults() {
    var windowsForm = {
        width: '',
        height: '',
        openers: {},
        columns: '',
        rows: ''

    };

    return windowsForm;
}


function readWindowForm() {

    var windowsForm = readLocalStorageWithKey('windowsForm');
    if (empty(windowsForm)) {
        windowsForm = windowFormDefaults();
    }
    return windowsForm;
}


function formStatusDefaults() {
    var formStatus = {
        errors: {},
        width: 0,
        height: 0,
        choosePanels: 0,
        glassChoices: {},
        glassOptions: {},
        hardwareChoices: '',
        hardwareExtraChoices: '',
        installationOptions: '',
        vinylType: '',
        extColor: '',
        intColor: '',
        module: '',
        validSize: false,
        firstTime: true,
        moduleTitle: '',
        materialType: '',
        note: '',
        quantity: 1,
        subModules: 1,
        panelWidth: '',
        sillOptions: '',
        addOnChoices: {},
        heightSection: ''
    };

    return formStatus;


}

function readQuote() {

    var quote = readLocalStorageWithKey('quote');

    if (empty(quote)) {
        return resetQuote();
    }

    if (empty(quote.SalesDiscount)) {
        return resetQuote();
    }

    if (quote.SalesDiscount.Discounts === undefined) {
        return resetQuote();
    }

    if (quote.SalesDiscount.Totals === undefined) {
        return resetQuote();
    }

    return quote;
}


function resetQuote() {

    var quote = {};
    quote.Cart = {};

    // quote.SalesDiscount = {};
    quote = setSalesDiscountToEmptyWithPassQuote(quote);
    // quote.SalesDiscount.Totals = {};
    // quote.SalesDiscount.Totals.code = "YZ-00";
    // if (siteName == 'pd'  || siteName == 'ces') {
    //     quote.SalesDiscount.Totals.code = "YZ-08";
    // }
    // quote.SalesDiscount.Discounts = {};
    quote.id = null;


    return quote;
}


//writeLocalStorageWithKeyAndData("user", user);
function readUser() {

    // readKey('user');
    // var user = $.cookie('user');
    var user = readKey('user');


    if (validUser(user)) {
        return user;
    }


    var guest = getParameter('guest');
    var print = getParameter('print');

    if (print === 'yes') {
        if (guest) {
            user = {type: 'guest'};
            return user;
        } else {
            user = {type: 'salesPerson'};
            return user;
        }
    } else {
        user = {type: 'new'};

    }

    return user;

}

function validUser(user) {

    if (empty(user)) {
        return false;
    }

    if (empty(user.type)) {
        return false;
    }

    if (user.type != 'builder' && user.type != 'guest' && user.type != 'salesPerson' && user.type != 'salesPersonSurvey' && user.type != 'productionLimited'
        && user.type != 'leads' && user.type != 'accounting'  && user.type != 'audit' && user.type != 'accountingLimited' && user.type != 'production' && user.type != 'survey' && user.type != 'surveyLimited') {
        return false;
    }

    if (currentTimeInSeconds() > user.expire && user.type != 'guest') {
        return false;
    }


    return true;

}

function getSalesAdminDefaults() {

    var url = "https://" + apiHostV1 + "/api/salesperson/" + user.id + "/defaults?" + authorizePlusSalesPerson() + addSite();
    $.getJSON(url, function (apiJSON) {
        validate(apiJSON);
        discounts = JSON.parse(apiJSON);
        discounts.Totals.cost = {amount: formStatus.total, showOnQuote: true};
        writeDiscount(discounts);
        discounts.Totals.preCost = getCostOfItemsInCart(cart);
        buildSalesAdmin('#salesAdminMasterBox');

    });

}





function readQuoteID() {

    return $.cookie('quoteID');
}

function readCart() {


    var cart = quote.Cart;
    //var cookies = $.cookie();
    //
    //if (typeof(cookies) == "undefined") {
    //    return {};
    //}
    //
    //var re = new RegExp("Cart");
    //for (var row in cookies) {
    //    if (row.match(re)) {
    //       cart[row] = cookies[row];
    //    }
    //}
    return cart;

}