'use strict'

function buildHorizontalWindows(columnsPassed) {

    columns = columnsPassed;
    $('#windowBuilder').html('');

    var changedRows = translateColumnsToDimensions(columns);

    buildHolders();
    buildHorizontal(columns);
    addDimensionsToWindows(changedRows);

    $('.dimension').unbind("keyup").bind("keyup", updateDimensionsHorizontal);
    $('.dimensionVertical').unbind("keyup").bind("keyup", updateVerticalDimensionsHorizontal);

    $('#heightWindow').keyup(updateHorizontalHeight);
    $('#widthWindow').keyup(updateHorizontalWidth);

    $('.sash').unbind("click").bind("click", selectSash);
    deActivateOpeners();
}


function numberOfWindowsByType(options, type) {

    if (type == 'vertical') {
        return rows.length;
    } else {
        var columnNumber = getOptionIndex(options, 3);
        var numberOfWindows = columns[columnNumber].numberOfWindows;
        return numberOfWindows;
    }
}


function findRemainingSpaceLeftForRowsForHorizontal(options) {

    var rowNumber = getOptionIndex(options, 1);
    var columnNumber = parseInt(getOptionIndex(options, 3))  + 1;
    var row = rows[rowNumber - 1]

    var skipColumn = columnNumber ;
    if (columnNumber == row.numberOfWindows ) {
        skipColumn = 1;
    }

    var width = 0;
    for (var a = 1; a <= row.numberOfWindows; a++) {
         if (a != skipColumn && a != row.numberOfWindows) {
            width = width + parseInt(row['width'+a]);
        }
    }

    width = width + parseInt(minWidth);
    var currentWindowSize = $('#widthWindow').val();

    return currentWindowSize - width;
}


function findRemainingSpaceLeftForRows(currentRow) {

    var skipRow = currentRow;

    if (currentRow == rows.length - 1) {
        skipRow = 0;
    }

    var height = 0;
    for (var a = 0; a < rows.length; a++) {
        if (a != skipRow && a != rows.length - 1) {
            height = height + parseInt(rows[a].height);
        }
    }
    height = height + parseInt(minHeight);
    var currentWindowSize = $('#heightWindow').val();

    return currentWindowSize - height;
}


function isValidVerticalDimensionWithSelectorAndType(selector, type) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var rowNumber = getOptionIndex(options, 1);
    var maxWindowHeight = findRemainingSpaceLeftForRows(rowNumber);

    if (isNaN(value)) {
        return false;
    }
    if (value < minHeight) {
        return false;
    }
    if (value > maxHeight || value > maxWindowHeight) {
        return false;
    }

    return true;
}


function updateVerticalDimensionsHorizontal() {

    var selector = $(this);

    delay(function () {
        if (isValidVerticalDimensionWithSelectorAndType(selector, 'horizontal')) {
            updateVerticalWindowsHorizontal(selector);
            restoreDimensions();
        } else {
            selector.addClass('backgroundPink');
        }
    }, delayAmount);
}


function updateDimensionsHorizontal() {

    var selector = $(this);

    delay(function () {

        if (isValidHorizontalDimensionWithSelectorAndType(selector, 'horizontal')) {
            updateWindowsHorizontal(selector);
            restoreOpeners();

        } else {
            selector.addClass('backgroundPink');
        }
    }, delayAmount);
}


function findRemainingSpaceLeftForColumns(currentColumn) {

    var skipColumn = currentColumn;
    if (currentColumn == columns.length - 1) {
        skipColumn = 0;
    }

    var width = 0;
    for (var a = 0; a < columns.length; a++) {

        if (currentColumn)

            if (a != skipColumn && a != columns.length - 1) {
                width = width + parseInt(columns[a].width);
            }
    }

    width = width + parseInt(minWidth);
    var currentWindowSize = $('#widthWindow').val();

    return currentWindowSize - width;
}


function isValidHorizontalDimensionWithSelectorAndType(selector, type) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var numberOfWindows = numberOfWindowsByType(options, type);
    var maxWindowWidth = 0;

    if (type == 'vertical') {
        maxWindowWidth = findRemainingSpaceLeftForRowsForHorizontal(options);
    } else {
        var columnNumber = getOptionIndex(options, 3);
        maxWindowWidth = findRemainingSpaceLeftForColumns(columnNumber);
    }

    if (isNaN(value)) {
        return false;
    }
    if (value < minWidth) {
        return false;
    }
    if (value > maxWidth || value > maxWindowWidth) {
        return false;
    }

    return true;
}


function updateVerticalWindowsHorizontal(selector) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var row = getOptionIndex(options, 1);
    row = parseInt(row) + 1
    var type = getOptionIndex(options, 2);

    var columnNumber = getOptionIndex(options, 3);
    var numberOfRowsInColumn = columns[columnNumber].numberOfWindows;

    var heightField = 'height' + row;

    if (row == numberOfRowsInColumn) {

        var lastHeight = dimensionForLastHeightHorizontal(columnNumber);

        var amountToRemoveFromFirstWidth = value - lastHeight;
        value = columns[columnNumber]['height1'] - amountToRemoveFromFirstWidth;

        heightField = 'height1';
        columns[columnNumber][heightField] = value;
    } else {
        columns[columnNumber][heightField] = value;
    }

    windowsForm.columns = columns;
    writeWindowsForm();

    buildHorizontalWindows(columns);
}


function dimensionForLastHeightHorizontal(columnNumber) {

    var numberOfRowsInColumn = columns[columnNumber].numberOfWindows;
    var height;
    var windows = [];
    for (var a = 1; a < numberOfRowsInColumn; a++) {
        height = columns[columnNumber]['height' + a];
        windows.push({"height": height});
    }
    var lastHeight = remainingHeight(windows);
    return lastHeight;

}


function updateWindowsHorizontal(selector) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var columnNumber = getOptionIndex(options, 3);
    columnNumber = parseInt(columnNumber);

    if (columnNumber == columns.length - 1) {
        var lastWidth = dimensionForLastWidthHorizontal();
        columns[columnNumber]['width'] = value;
        var amountToRemoveFromFirstWidth = value - lastWidth;
        value = columns[0]['width'] - amountToRemoveFromFirstWidth;
        columnNumber = 0;
        columns[columnNumber]['width'] = value;
    } else {
        columns[columnNumber]['width'] = value;
        var lastColumn = columns.length - 1;
        columns[lastColumn]['width'] = dimensionForLastWidthHorizontal();
    }

    windowsForm.columns = columns;
    writeWindowsForm();

    buildHorizontalWindows(columns);
}


function dimensionForLastWidthHorizontal() {

    var width;
    var windows = [];

    for (var a = 0; a < columns.length - 1; a++) {
        width = columns[a]['width'];
        windows.push({"width": width});
    }

    var lastWidth = remainingWidthVertical(windows);

    return lastWidth;
}


function updateHorizontalHeight() {

    updateHeightDelay('horizontal');
}


function updateHorizontalWidth() {

    updateWidthDelay('horizontal');
}


function buildHorizontalData(res) {

    var numberOfColumns = res.length;
    var width = Math.round(frame.width * xMult / numberOfColumns);

    var columns = [];
    for (var a = 0; a < numberOfColumns; a++) {

        var column = {"numberOfWindows": res[a], "width": width, "number": a};
        column = addHeightToColumn(column);
        columns.push(column);
    }

    return (columns);
}


function chooseBestRowForDimensionsAndSetTopBottom(rows) {

    var maxWindows = 0;
    var rowWithMax;
    for (var a = 0; a < rows.length; a++) {

        var row = rows[a];
        if (row.numberOfWindows > maxWindows) {
            maxWindows = row.numberOfWindows;
            rowWithMax = a;
        }
    }
    chooseDimensionTopBottom = 'top';
    if (rowWithMax == rows.length - 1 && rows.length > 1) {
        chooseDimensionTopBottom = 'bottom';
    }
    var rowWithMostWindows = rows[rowWithMax];

    return rowWithMostWindows;
}


function setUpHorizontalDimensionBasedOnTopOrBottom() {

    if (chooseDimensionTopBottom == 'top') {
        var horizontalSizes = $('#horizontalSizes');
        horizontalSizes.html('');
        horizontalSizes.addClass('newHorizontalSizes');
        $('#bottomDimension').removeClass('bottomDimensionOverOne');

    } else {
        $('#horizontalSizes').removeClass('newHorizontalSizes');
        $('#bottomDimension').html('');
        $('#bottomDimension').addClass('bottomDimensionOverOne');
    }
}


function buildDimensionsHorizontal(rows) {

    var row = chooseBestRowForDimensionsAndSetTopBottom(rows);
    setUpHorizontalDimensionBasedOnTopOrBottom();

    var windows = [];

    var unSelectable = '';
    var readOnly = false;

    if (row.numberOfWindows == 1) {
        unSelectable = 'unSelectable';
        readOnly = true;
        $('#topDimension').css('visibility', 'hidden');
    }

    for (var a = 0; a < row.numberOfWindows; a++) {

        var item = 'width' + parseInt(a + 1);

        var width = row[item];
        if (!isNaN(width)) {
            windows.push({"width": width});
        }

        if (a == row.numberOfWindows - 1) {
            width = remainingWidthVertical(windows);
        }

        var widthShown = width;
        width = width / xMult;
        var firstDim = -1;

        if (row.numberOfWindows == 3) {
            firstDim = -3;
        }
        if (row.numberOfWindows == 4) {
            firstDim = -2;
        }

        if (a == 0) {
            firstDim = 0;
            if (row.numberOfWindows == 4) {
                firstDim = -1;
            }
        }

        var push = parseInt(row.numberOfWindows) - 2;
        if (push < 0) {
            push = 0;
        }
        push = 0

        var divWidth = ((parseInt(width) + parseInt(push)) / 2 ) - 33;
        var divWidth1 = divWidth - firstDim;
        var id = "row-" + row.number + "-width-" + parseInt(a);

        if (chooseDimensionTopBottom == 'top') {

            $('<div>', {
                class: 'horizontalBox'
            }).append($('<div>', {
                class: 'leftHorizontal dimensionBorder',
                width: parseInt(divWidth)
            })).append($('<input/>', {
                id: id,
                type: 'text',
                class: 'middleTextHorizontal dimension ' + unSelectable,
                readonly: readOnly,
                value: parseInt(widthShown)
            })).append($('<div>', {
                class: 'rightHorizontal dimensionBorder',
                width: parseInt(divWidth1)
            })).appendTo('#horizontalSizes');
        } else {
            $('<div>', {
                class: 'horizontalBox'
            }).append($('<div>', {
                class: 'leftHorizontalBottom dimensionBorder',
                width: parseInt(divWidth)
            })).append($('<input/>', {
                id: id,
                type: 'text',
                class: 'middleTextHorizontalBottom dimension ' + unSelectable,
                readonly: readOnly,
                value: parseInt(widthShown)
            })).append($('<div>', {
                class: 'rightHorizontalBottom dimensionBorder',
                width: parseInt(divWidth1)
            })).appendTo('#bottomDimension');
        }
    }
}


function addHeightToColumn(column) {

    for (var a = 1; a < column.numberOfWindows; a++) {
        column['height' + a] = Math.round(frame.height * yMult / column.numberOfWindows)
    }

    return column;
}


function getColumnOfWindowsForDimensions(columns) {

    var column = 0;

    for (var a = 0; a < columns.length; a++) {
        var now = columns[a].numberOfWindows;
        var then = columns[column].numberOfWindows
        if (now > then) {
            column = a;
        }
    }

    chooseDimensionLeftRight = 'left';
    if (parseInt(column) + 1 == columns.length && columns.length != 1) {
        chooseDimensionLeftRight = 'right';
    }

    return columns[column];
}


function addWidthToDimensions(columns) {

    //var column = getColumnOfWindowsForDimensions(columns);
    //var width = column.width;

    var numberOfColumns = columns.length;

    var row = {"numberOfWindows": numberOfColumns};
    for (var a = 0; a < columns.length; a++) {
        row['width' + (parseInt(a) + 1)] = columns[a].width;
    }

    row['width' + numberOfColumns] = remainingWidthForColumns(columns);


    return row;
}


function translateColumnsToDimensions(columns) {

    var column = getColumnOfWindowsForDimensions(columns);
    var numberOfWindows = column.numberOfWindows;

    //Builds for Horizontal Dimensions
    var row = addWidthToDimensions(columns);

    //Builds for Vertical Dimensions
    var changedRows = [];
    for (var a = 1; a < numberOfWindows; a++) {
        row.height = column['height' + a];
        row.number = parseInt(a);
        row.column = column;
        var newRow = JSON.parse(JSON.stringify(row));
        changedRows.push(newRow);
    }

    var lastHeight = remainingHeight(changedRows);
    row.number = parseInt(numberOfWindows);
    row.height = lastHeight;
    var newRow2 = JSON.parse(JSON.stringify(row));
    changedRows.push(newRow2);

    return changedRows;
}


function remainingWidthForColumns(windows) {

    var widthOfWindows = getWidthOfWindows(windows);
    var remainingSpace = (frame.width * xMult) - widthOfWindows;

    return remainingSpace;
}


function buildHorizontal(columns) {

    var windows = [];

    for (var a = 0; a < columns.length - 1; a++) {
        var column = columns[a];
        windows.push({"width": column.width});
        column.columnNumber = parseInt(a) + 1;
        buildColumn(column, columns.length);
    }

    var width = remainingWidthForColumns(windows);
    var lastColumn = columns[columns.length - 1];
    lastColumn.columnNumber = columns.length;
    lastColumn.width = width;

    buildColumn(lastColumn, columns.length);
}


function buildColumn(column, numberOfColumns) {

    var frameWidthToRemove = widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, column.columnNumber);

    jQuery('<div/>', {
        id: 'column' + column.number,
        class: 'holder',
        width: parseInt(column.width / xMult) - parseInt(frameWidthToRemove) + 12,
        height: frame.height
    }).appendTo('#mainFrame');

    var size = {
        "columnNumber": column.columnNumber,
        "width": column.width,
        "place": '#column' + column.number,
        "column": column.number,
        "numberOfWindows": parseInt(column.numberOfWindows)
    };

    var windows = [];
    for (var a = 1; a < column.numberOfWindows; a++) {

        var height = column['height' + a];

        windows.push({"height": height});
        size.number = parseInt(a);
        size.height = height;
        size.column = a;
        size.row = column.number;
        addWindowHorizontal(size, numberOfColumns);
    }

    size.column = column.numberOfWindows;
    size.number = parseInt(column.numberOfWindows);
    size.height = remainingHeight(windows);
    size.numberOfWindows = parseInt(column.numberOfWindows);
    size.row = column.number;

    addWindowHorizontal(size, numberOfColumns);
}


function widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, columnNumber) {

    var numberOfMargins = parseInt(numberOfColumns) + 1;

    var frameWidthToRemove = parseInt(frame.marginWidth) + parseInt(2 * border) - 1;
    if (columnNumber == 1) {
        frameWidthToRemove = ((frame.marginWidth * numberOfMargins ) / numberOfColumns ) + (border * 2 * numberOfColumns) - 1;
    }
    if (columnNumber == numberOfColumns) {
        frameWidthToRemove = ((frame.marginWidth * numberOfMargins ) / numberOfColumns ) + (border * 2 * numberOfColumns) - 2;
    }

    return frameWidthToRemove;
}


function addWindowHorizontal(size, numberOfColumns) {

    var numberOfRows = parseInt(size.numberOfWindows);

    var rowsMultiplier = parseInt(numberOfRows);
    var frameHeightToRemove = ( (frame.marginHeight * rowsMultiplier) + (2 * border * rowsMultiplier) ) / numberOfRows;

    if (size.number == 1) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }
    if (size.number == numberOfRows) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }

    var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

    var frameWidthToRemove = widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, size.columnNumber);
    var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);

    jQuery('<div/>', {
        class: 'frame',
        width: parseInt(size.width / xMult) - parseInt(frameWidthToRemove),
        height: parseInt(size.height / yMult) - frameHeightToRemove
    }).append($('<div>', {
        id: 'window-' + size.row + "-" + size.column,
        class: 'sash',
        width: (size.width / xMult) - sashWidthToRemove,
        height: (size.height / yMult) - sashHeightToRemove
    })).appendTo(size.place);
}


function buildTotalDimensionsHorizontal() {

    var margins = 13;
    var textFieldWidth = 52 + margins;
    var dimensionWidth = (frame.width - textFieldWidth) / 2;

    if (chooseDimensionTopBottom == 'bottom') {
        buildTotalDimensionsForBottom(dimensionWidth);
    } else {
        buildTotalDimensionsForTop(dimensionWidth)
    }
}


function buildTotalDimensionsForTop(dimensionWidth) {

    $('#bottomDimension').html('');
    jQuery('<div>', {
        id: 'totalHorizontal',
        class: 'totalHorizontalDimension',
        width: frame.width + 2
    }).append($('<div>', {
        class: 'bottomDimensionLeft dimensionBorder',
        width: dimensionWidth - 1
    })).append($('<input/>', {
        id: "widthWindow",
        type: 'text',
        class: 'dimensionBottom unSelectable unSelectableBumpDown',
        readOnly: true,
        value: parseInt(frame.width * xMult)
    })).append($('<div>', {
        class: 'bottomDimensionRight dimensionBorder',
        width: dimensionWidth
    })).appendTo('#bottomDimension');

}


function buildTotalDimensionsForBottom(dimensionWidth) {

    $('#horizontalSizes').html('');

    jQuery('<div>', {
        id: 'totalHorizontal',
        class: 'totalHorizontalDimension',
        width: frame.width + 2
    }).append($('<div>', {
        class: 'topDimensionLeft dimensionBorder',
        width: dimensionWidth
    })).append($('<input/>', {
        id: "widthWindow",
        type: 'text',
        class: 'dimensionTop unSelectable unSelectableBumpUp',
        readOnly: true,
        value: parseInt(frame.width * xMult)
    })).append($('<div>', {
        class: 'topDimensionRight dimensionBorder',
        width: dimensionWidth
    })).appendTo('#horizontalSizes');

}


