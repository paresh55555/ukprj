'use strict'

var hardware = [];


function loadHardware() {

    startSpinner();
    updateFormStatus();

    var html = hardwareHTML();

    buildAndAttachCorePage(html);
    $('#cartNav').show();
    $('#hardWarePreview').hide();
    $('.nextImage').hide();
    selectTab('#hardware');

    getHardwareOptions();
    getHardwareExtras();

    addFooter();
    updateFormPrice();


    addBottonToInstallationForCart();
    showAddToQuoteButton();
    //makeNextSectionButton(2);

    showContentAndSaveState();

}


function hardwareHTML() {

    var nextImage = nextSVG();

    var html = '' +
        '<div id="dynamicContent"> ' +
        '   <div id="myHardware"> ' +
        '       <div class="title">Choose Hardware:</div> ' +
        '       <div id="hardwareChoices" class="sectionBlockPreview"></div> ' +
        '       <div id="hardWarePreview">' +
        '           <img src="images/hardwarePreview.png" id="hardwareImagePreview">' +
        '       </div> ' +
        '   </div> ' +
        '   <div id="myExtras"> ' +
        '       <div class="title">Choose Hardware Options:</div> ' +
        '       <div id="hardwareExtraChoices" class="sectionBlockPreview"></div> ' +
        '   </div> ' +
        '   <div id="nextInstallation"> ' +
        '       <div id="nextButtonHardware" > ' +
        '           <div id="nextHardwareTitle" class="nextTitle" ' +  cssFont + '></div> ' +
        '           <div class="nextImage"> ' +
        '               ' + nextImage +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '   <div id="copyWriteFooter"></div> ' +
        '</div>';



    return html;
}

/// nextButtonHardware  (This is where the save is)
function hardWareSaveQuote() {

    startSpinner();
    if (quote.id) {
        if (isGuest()) {
            completeGuestSaveQuote()
        } else {
            saveToQuote(quote.id);
        }
    } else {
        updateQuote();
    }
}


function checkGHardwareChoices() {

    if (formStatus.hardwareChoices) {
        var id = "[id='" + formStatus.hardwareChoices + "']";
        displayVerticalSelected(id);
    }
}


function createHardwareButtons(id) {
    $('.verticalButton', id).unbind("click").bind("click", verticalSelected);
}


function verticalSelected() {

    var thisSection = $(this).parent();
    var optionSelected = $(this).attr('id');
    var section = $(thisSection).attr('id');

    formStatus[section] = optionSelected;
    writeFormStatus();

    if(optionSelected === 'hardware-Panic Hardware'){
        if (typeof formStatus.hardwareExtraChoices['extras-Door Restrictor'] !== 'undefined') {
            formStatus.hardwareExtraChoices['extras-Door Restrictor'] = 'no';
        }
    }
    if(optionSelected === 'hardware-Yale Electronic'){
        if (typeof formStatus.hardwareExtraChoices['extras-Ultion 3'] !== 'undefined' && formStatus.hardwareExtraChoices['extras-Ultion 3'] == 'yes') {
            alert('Ultion 3 can not be added to the a Yale Electronic Lock');
            formStatus.hardwareExtraChoices['extras-Ultion 3'] = 'no';
        }
    }
    if(optionSelected !== 'hardware-Panic Left' &&
        optionSelected !== 'hardware-Panic Right' &&
        optionSelected !== 'hardware-Panic Both') {

        if (typeof formStatus.hardwareExtraChoices['extras-Outside Left'] !== 'undefined') {
            formStatus.hardwareExtraChoices['extras-Outside Left'] = 'no';
        }

        if (typeof formStatus.hardwareExtraChoices['extras-Outside Right'] !== 'undefined') {
            formStatus.hardwareExtraChoices['extras-Outside Right'] = 'no';
        }

    }
    addHardwareExtra();
    createExtraButtonsOptions();
    checkGHardwareExtraChoices();
    writeFormStatus();

    var selectedDiv = $(".verticalSelectedDiv", thisSection);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
        $('.verticalButtonName', thisSection).css({"background-color": "#FFFFFF", "color": "#000000"});
        $('.verticalButtonNote', thisSection).css({"background-color": "#FFFFFF", "color": "#000000"});
    }

    displayVerticalSelected(this);
    updateFormPrice();
    scrollToNextPositon($('#nextInstallation'));
}


function displayVerticalSelected(selector) {

    var imageSelector = $('.verticalButtonImage', selector);

    jQuery('<div/>', {
        class: 'verticalSelectedDiv'
    }).appendTo($(imageSelector));

    jQuery('<img/>', {
        src: 'images/icon-selected.svg'
    }).appendTo($(".verticalSelectedDiv", imageSelector));


    $('.verticalButtonName', selector).css({"background-color": "#44ac61", "color": "#FFFFFF"});
    $('.verticalButtonNote', selector).css({"background-color": "#44ac61", "color": "#FFFFFF"});
}


function addHardwareChoices() {
    var id = "#hardwareChoices";
    var buttons = buildHardwareButtons(hardware);
    $(id).html('');
    $(id).append(buttons);
}


function buildHardwareButtons(buttonData) {
    var buttons = [];

    var swingDirection = '';

    if (!empty(formStatus.panelOptions.swingDirection) && !empty(formStatus.panelOptions.swingDirection.selected)) {
        swingDirection = formStatus.panelOptions.swingDirection.selected;
    }

    for (var a = 0; a < buttonData.length; a++) {
        var hardware = buttonData[a];
        if(swingDirection === 'left' && (hardware.name === 'Panic Right' || hardware.name === 'Panic Both'))continue;
        if(swingDirection === 'right' && (hardware.name === 'Panic Left' || hardware.name === 'Panic Both'))continue;
        var button = buildVerticalButton(hardware);
        buttons.push(button);
    }
    return (buttons);
}


function buildVerticalButton(hardware) {

    var id = 'hardware-' + hardware.name;

    var button =
        ['<div id="' + id + '" class="verticalButton">',
            '<div class="verticalButtonImage"><img src="' + hardware.url + '"></div>',
            '<div class="verticalButtonName">' + hardware.name + '</div>',
            '<div class="verticalButtonNote">' + hardware.info + '</div>',
            '</div>'
        ].join('\n');

    return (button);
}


function getHardwareOptions(type) {

    if (empty(formStatus) || isEmptyObject(formStatus)) {
        trackAndDoClick('home');
        return;
    }

    var url = "https://" + apiHostV1 + "/api/hardware?module=" + formStatus.module + authorize() + addSite();

    $.getJSON(url, function (apiJSON) {
        validate(apiJSON)
        hardware = apiJSON;
        formStatus.hardwareToChooseFrom = hardware;
        writeFormStatus();

        addHardwareChoices();
        checkGHardwareChoices();
        createHardwareButtons();
        scrollTopOfPage();
    });
}


function getHardwareExtras(type) {

    var url = "https://" + apiHostV1 + "/api/hardware/extra?module=" + formStatus.module + authorize() + addSite();

    $.getJSON(url, function (apiJSON) {
        validate(apiJSON)

        formStatus.hardwareExtras = apiJSON;
        writeFormStatus();

        addHardwareExtra();
        createExtraButtonsOptions();
        checkGHardwareExtraChoices();
    });
}


function createExtraButtonsOptions(id) {
    if(formStatus.doorStyle){
        $('.extendedButton', id).unbind("click").bind("click", extendedOptionSelectedSingle);
    }else{
        $('.extendedButton', id).unbind("click").bind("click", extendedOptionSelected);
    }
}


function checkGHardwareExtraChoices() {

    var options = formStatus.hardwareExtraChoices;

    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            if (options[key] == "yes") {
                var id = "[id='" + key + "']";
                var idSelector = $('.extendedButtonImage', id);
                displayExtendedSelected(idSelector);
            }
        }
    }
}


function addHardwareExtra() {

    var id = "#hardwareExtraChoices";
    var buttons = buildHardwareExtraButtons(formStatus.hardwareExtras);
    $(id).html('');

    if (formStatus.hardwareExtras.length >= 1 && !isGuest()) {
        $(id).append(buttons);
        $('#myExtras').show();
    } else {
        $('#myExtras').hide();
    }

}


function buildHardwareExtraButtons(buttonData) {
    var buttons = [];
    for (var a = 0; a < buttonData.length; a++) {
        var extras = buttonData[a];
        var button = buildExtendedButton(extras, 'extras');
        if(isHideDoorRestrictor() && extras.name === 'Door Restrictor')continue;
        if(formStatus.hardwareChoices === 'hardware-Panic Hardware' && extras.name === 'Door Restrictor')continue;
        if(formStatus.hardwareChoices === 'hardware-Panic Left' && extras.name === 'Outside Right')continue;
        if(formStatus.hardwareChoices === 'hardware-Panic Right' && extras.name === 'Outside Left')continue;
        if((formStatus.hardwareChoices !== 'hardware-Panic Left' &&
            formStatus.hardwareChoices !== 'hardware-Panic Right' &&
            formStatus.hardwareChoices !== 'hardware-Panic Both') && (extras.name === 'Outside Left' || extras.name === 'Outside Right'))continue;
        buttons.push(button);
    }
    return (buttons);
}


