'use strict';

var lastTab = 'viewPreProduction';

function loadProduction(passedTab) {

    startSpinner();
    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();

    var topButtons = buildProductionTopButtons();
    var searchBox = buildSearchBox();
    $('#preMainContent').html(topButtons + searchBox);

    $('#search').keyup(productionSearchDelay);

    productionSearch(passedTab);

}


function productionSearchDelay() {

    var selector = this;

    delay(function () {
        var newSearch = 'yes';
        productionSearch(tab);
    }, 300);
}


function productionSearch(passedTab) {
    setTab(passedTab);
    lastTab = passedTab;

    var option = 'viewAll';

    if (passedTab == 'viewProduction') {
        option = 'inProduction';
        uiState.productionStatus = 'prodProduction';
    } else if (passedTab == 'viewAllSurvey') {
        option = 'viewAllSurvey';
        uiState.productionStatus = '';
    } else if (passedTab == 'viewNeedsSurvey') {
        option = 'needsSurvey';
        uiState.productionStatus = 'prodNeedsSurvey';
    } else if (passedTab == 'viewCompleted') {
        option = 'completed';
        uiState.productionStatus = 'prodCompleted';
    } else if (passedTab == 'viewPreProduction') {
        option = 'preProduction';
        uiState.productionStatus = 'prodPreProduction';
    } else if (passedTab == 'viewDelivered') {
        option = 'delivered';
        uiState.productionStatus = 'prodDelivered';
    } else if (passedTab == 'viewHold') {
        option = 'hold';
        uiState.productionStatus = 'prodHold';
    } else {
        uiState.productionStatus = '';
    }

    var order = getOrder();

    var url = "https://" + apiHostV1 + "/api/orders/" + option + "?" + authorizePlusProduction() + pageLimitText() + order + addSite() + addSearch();

    $.getJSON(url, function (apiJSON, status, xhr) {

        var total = xhr.getResponseHeader('Total');
        if(!validate(apiJSON))return;
        var html = buildPreProduction(apiJSON);

        $('#headerStartOver').html('');
        $('#mainContent').html(html);
        $('.quoteIcons').unbind("click").bind("click", leadActions);
        $('.productionActions').unbind("click").bind("click", navigationClick);
        $(".estCompleteDateBox").datepicker({gotoCurrent: true, minDate: 0, "dateFormat": "mm/dd/y"});
        $("#prodReportDateInput").datepicker({gotoCurrent: true, minDate: 1, "dateFormat": "yy-mm-dd"});
        $('#cartButton').hide();
        $('.sortable').unbind("click").bind("click", orderTableResultsWithTab);

        setPaginationWithSelectorAndTotal('#pagination', total);

        showContentAndSaveState();
        scrollTopOfPage();
    });
}


function buildTopButtonsProduction() {

    var html = '' +
        '<div id="viewAll" class="leadActions leadActionsLeft">View All</div>' +
        '<div id="new" class="leadActions newQuote">New</div>' +
        '<div id="hot" class="leadActions hot">HOT</div>' +
        '<div id="medium" class="leadActions medium">Medium</div>' +
        '<div id="cold" class="leadActions cold">Cold</div>' +
        '<div id="hold" class="leadActions hold">Hold</div>' +
        '<div id="archived" class="leadActions archived">Archived</div>';


    return html;


}


function viewProductionOrder() {

    startSpinner();
    var order = getParameter('order');
    var url = "https://" + apiHostV1 + "/api/orders/" + order + "?" + authorizePlusProduction() + '&production=yes' + addSite();

    loadViewOrder(url);

}
function deleteOrderProduction(id) {

    var x;
    stopSpinner();
    if (confirm("Are you sure you want to delete this order?") == true) {
        startSpinner();
        var quote = getParameter('quote');
        $.ajax({
            url: "https://" + apiHostV1 + "/api/quotes/" + id + "/production?" + authorizePlusProduction() + addSite(),
            type: "DELETE",
            success: function (response) {

                var response = jQuery.parseJSON(response);
                if (response.status != true) {
                    alert("Failed to Save Production Notes");

                }
                trackAndDoClick(lastTab);
            }
        });
    }
}



function makeJobInfo(quote, cart) {
    var moduleTitleConversion = {
        "1": "Thermaclad", "2": "Absolute Vinyl", "3": "Signature Aluminum",
        "4": "Thermaclad", "5": "Thermaclad", "6": "Absolute Vinyl",
        "7": "Absolute Vinyl", "8": "Signature Aluminum", "9": "Signature Aluminum",
        "10": "Bevelled PVC", "13": "Sculptured PVC"
    };

    var jobInfo = {};
    var title = makeQuoteTitle(quote);
    jobInfo.companyName = quote.Customer.companyName;
    jobInfo.ref = quote.po;
    jobInfo.firstName = quote.Customer.firstName;
    jobInfo.lastName = quote.Customer.lastName;
    jobInfo.orderDate = quote.orderDate;
    jobInfo.dueDate = quote.dueDate;
    jobInfo.estComplete = quote.estComplete;
    jobInfo.id = title;
    jobInfo.doorType = cart.moduleTitle;
    jobInfo.city = quote.Customer.jobCity;
    jobInfo.state = quote.Customer.jobState;

    return (jobInfo);
}


function buildSheets() {
    var sheets = [];


    for (var property in quote.Cart) {

        var count = 0;

        while (quote.Cart[property].quantity > count) {
            var sheet = buildPriceEngineOptionsWithOptions(quote.Cart[property]);
            sheet.jobInfo = makeJobInfo(quote, quote.Cart[property]);
            if (quote.Cart[property].glassChoices && quote.Cart[property].glassChoices['glass-Unglazed'] != 'yes') {
                sheet.jobInfo.glass = 'Glazed'
            }
            sheets.push(sheet);
            count++;
        }
    }

    return sheets;
}


function printCutSheetForSales(id) {

    var debug = getParameter('debug');

    if (empty(debug)) {
        debug = '';
    } else {
        debug = '&debug=' + debug;
    }

    getCreateCutSheets().done(function (result) {
        stopSpinner();
        var response = jQuery.parseJSON(result);

        if (response.status == "Success") {
            printSalesOrder(id, debug);
        } else {
            alert("Order Failed to Print");
        }

    });
}


function printCutSheetForProduction(id) {

    var sheets = buildSheets();

    putCutSheet(sheets, id).done(function (result) {

        var response = jQuery.parseJSON(result);
        if (response.status == "Success") {
            printProductionOrder(id);
            stopSpinner();
        } else {
            alert("Order Failed to Print");
        }
    });
}


function printGlassSheetForProduction(id) {

    var sheets = buildSheets();
    putCutSheet(sheets, id).done(function (result) {

        var response = jQuery.parseJSON(result);
        if (response.status == "Success") {
            printProductionGlassSheet(id);
            stopSpinner();
        } else {
            alert("Order Failed to Print");
        }
    });
}

function printGlassLabelsForProduction(id) {

    var sheets = buildSheets();

    putCutSheet(sheets, id).done(function (result) {

        var response = jQuery.parseJSON(result);
        if (response.status == "Success") {
            printProductionGlassLabels(id);
            stopSpinner();
        } else {
            alert("Order Failed to Print");
        }
    });
}


function buildDebug() {

    var debug = getParameter('debug');
    if (empty(debug)) {
        debug = '';
    } else {
        debug = '&debug=' + debug;
    }

    return debug;
}

function printProductionGlassSheet(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printGlassSheets?&token=" + user.token + buildDebug() + addSite();

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);
}

function printProductionGlassLabels(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printGlassLabels?" + authorizePlusProduction() + buildDebug() + addSite();

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);
}


function printProductionOrder(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printCutSheets?&token=" + user.token + buildDebug() + addSite();

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);
}


function printSalesOrder(id, debug) {

    stopSpinner();

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printCutSheetsSales?&token=" + user.token + debug + addSite();

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);

}


function blockingPopupsAlert(newWin) {
    if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
        alert("Popup window was blocked, turn off your popup blocker to allow for printing.");
    }
}


function buildProductionNotes(data) {

    if (!data) {
        return '';
    }

    var html = '';

    //if (tab == 'viewProductionOrder') {
    //
    //    html = '' +
    //    '<div class="customerDetails">' +
    //    '   <div class="customerTitle">Production Notes</div>' +
    //    '   <div class="customerBox">' +
    //    '       <div class="customerBoxDetails">' +
    //    '           <textarea onkeyup="productionNotesKeyUp()" id="productionNotes"  placeholder=""  name="productionNotes">' + data.productionNotes + '</textarea>' +
    //    '       </div>' +
    //    '   </div>' +
    //    '</div>' ;
    //
    //}

    return html;

}

function buildPreProduction(data) {

    var row;
    var rows = '';

    for (var a = 0; a < data.length; a++) {
        var quote = data[a];
        row = buildPreProductionRow(quote, a);
        rows = rows + row;
    }

    var html = buildPreProductionTable(rows, data);

    return html;

}
function getProductionRowColor(quote) {

    var rowColor = '';
    if (quote.type == 'Completed') {
        rowColor = 'prodCompleted';
    } else if (quote.type == 'Delivered') {
        rowColor = 'prodDelivered';
    } else if (quote.type == 'In Production') {
        rowColor = 'prodProduction';
    } else if (quote.type == 'Ready For Production') {
        rowColor = 'prodPreProduction';
    } else if (quote.type == 'Needs Survey') {
        rowColor = 'prodNeedsSurvey';
    }

    if (quote.status == 'hold') {
        rowColor = 'prodHold';
    }

    return rowColor;

}
function ifHasNotes(quote, onClick) {


    var notes = '<div ' + onClick + ' class="prodCell13 borderLeftRightTop" >Yes</div>';
    if (siteDefaults.productionFlow == 2) {
        notes = '<div ' + onClick + ' class="prodTable-Flow2-Cell15 borderLeftRightTop" >Yes</div>';
    } else {
        if (empty(quote.productionNotes) && !cartHasNote(quote.Cart)) {
            notes = '<div class="prodCell13 borderLeftRightTop" >&nbsp;</div>';
        }
    }

    return notes;
}


function buildPreProductionRow(quote, count) {
    var yesNoConfirm = buildConfirmOrderPullDown(quote.id);

    var action = '<td >' + yesNoConfirm + '</td>' +
        '<td class="confirmPaymentLink" id="sq-' + quote.id + '"><div class="fixedWideColumn" ></div></td>';

    if (tab == 'readyForProduction') {
        action = '<td class="confirmPaymentLink" id="sq-' + quote.id + '"><div onclick="revertToConfirm(' + quote.id + ')" class="fixedWideColumn" >Revert Production</div></td>';
    }
    var productionStatus = buildProductionStatus(quote);
    var glassOrdered = buildGlassOrdered(quote);
    var wood = buildWoodForProduction(quote);

    var onClick = 'onClick="trackAndDoClick(\'viewProductionOrder\',\'&order=' + quote.id + '\')"';
    var notes = ifHasNotes(quote, onClick);
    var orderTitle = makeQuoteTitle(quote);


    if (empty(quote.SalesDiscount)) {
        quote = setSalesDiscountToEmptyWithPassQuote(quote);
        // quote.SalesDiscount = {};
        // quote.SalesDiscount.Totals = {};
        // quote.SalesDiscount.Totals.code = "YZ-00";
        // if (siteName == 'pd' || siteName == 'ces') {
        //     quote.SalesDiscount.Totals.code = "YZ-08";
        // }
        // quote.SalesDiscount.Discounts = {};

    }

    var rowColor = getProductionRowColor(quote);
    var lastName = quote.lastName ? quote.lastName : '&nbsp;'
    var companyName = quote.companyName ? quote.companyName : '&nbsp;'

    var paidInFull = '--';
    if (quote.paidInFull == "yes") {
        paidInFull = "Yes";
    }

    if (quote.daysInProduction > 736414) {
        quote.daysInProduction = '--';
    }

    var row = '';

    if (siteDefaults.productionFlow == 2) {
        row = '' +
            '<div class="accountRow">' +
            '   <div ' + onClick + ' class="prodTable-Flow2-Cell1 borderLeftTop pointer ' + rowColor + '">' + orderTitle + '</div>' +
            '   <div class="prodTable-Flow2-Cell2 borderLeftTop">' + lastName + '</div>' +
            '   <div class="prodTable-Flow2-Cell3 borderLeftTop">' + createTableSpaceOnEmpty(quote.po) + '</div>' +
            '   <div class="prodTable-Flow2-Cell4 borderLeftTop">' + companyName + '</div>' +
            '   <div class="prodTable-Flow2-Cell5 borderLeftTop">' + quote.quantity + '</div>' +
            '   <div class="prodTable-Flow2-Cell6 borderLeftTop">' + quote.numberOfPanels + '</div>' +
            '   <div class="prodTable-Flow2-Cell7 borderLeftTop">' + quote.startedProd + '</div>' +
            '   <div class="prodTable-Flow2-Cell8 borderLeftTop">' + quote.daysInProduction + '</div>' +
            '   <div class="prodTable-Flow2-Cell9 borderLeftTop">' + quote.dueDate + '</div>' +
            '   <div class="prodTable-Flow2-Cell10 borderLeftTop">' +
            '       <input id="estComplete-' + count + '"  onChange="updateEstCompeteDateWithCountAndId(' + count + ',' + quote.id + ')" class="estCompleteDateBox" type="text"  value = "' + quote.estComplete + '"/>' +
            '   </div>' +
            '   <div class="prodTable-Flow2-Cell11 borderLeftTop">' + glassOrdered + '</div>' +
            '   <div class="prodTable-Flow2-Cell12 borderLeftTop">' + wood + '</div>' +
            '   <div id="startedProd" class="prodTable-Flow2-Cell13 borderLeftTop">' + paidInFull + '</div>' +
            '   <div class="prodTable-Flow2-Cell14 borderLeftTop">' + productionStatus + '</div>' +
            '   ' + notes +
            '</div>';

    }
    else {
        row = '' +
            '<div class="accountRow">' +
            '   <div ' + onClick + ' class="prodCell1 borderLeftTop pointer ' + rowColor + '">' + orderTitle + '</div>' +
            '   <div class="prodCell2_v1 borderLeftTop">' + lastName + '</div>' +
            '   <div class="prodCell3_v1 borderLeftTop">' + companyName + '</div>' +
            '   <div class="prodCell4_v1 borderLeftTop">' + quote.quantity + '</div>' +
            '   <div class="prodCell5_v1 borderLeftTop">' + quote.numberOfPanels + '</div>' +
            '   <div class="prodCell6_v1 borderLeftTop">' + quote.startedProd + '</div>' +
            '   <div class="prodCell7_v1 borderLeftTop">' + quote.daysInProduction + '</div>' +
            '   <div class="prodCell8_v1 borderLeftTop">' + quote.dueDate + '</div>' +
            // '<div class="prodCell9 borderLeftTop">' +
            // '       <input id="estComplete-' + count + '"  onChange="updateEstCompeteDateWithCountAndId(' + count + ',' + quote.id + ')" class="estCompleteDateBox" type="text"  value = "' + quote.estComplete + '"/>' +
            // '   </div>' +
            // '   <div class="prodCell10 borderLeftTop">' + glassOrdered + '</div>' +
            // '   <div class="prodCell11 borderLeftTop">' + wood + '</div>' +
            '   <div id="startedProd" class="prodCell7_v1 borderLeftTop">' + paidInFull + '</div>' +
            '   <div class="prodCell12 borderLeftTop">' + productionStatus + '</div>' +
            '   ' + notes +
            '</div>';
    }

    return row;
}


function buildProductionTopButtons() {

    if(isSurvey()){
        var html = '' +
            '<div id="viewAllSurvey" class="productionActions survey orderStatusLeft ">View All</div>' +
            '<div id="viewNeedsSurvey" class="productionActions survey prodNeedsSurvey ">Needs Survey</div>' +
            '<div id="viewPreProduction" class="productionActions survey prodPreProduction ">Pre-Production</div>';
    }else{
        var html = '' +
            '<div id="viewAllProduction" class="productionActions orderStatusLeft ">View All</div>' +
            '<div id="viewPreProduction" class="productionActions prodPreProduction ">Pre-Production</div>' +
            '<div id="viewProduction" class="productionActions prodProduction ">Production</div>' +
            '<div id="viewCompleted" class="productionActions prodCompleted ">Completed</div>' +
            '<div id="viewDelivered" class="productionActions prodDelivered ">Delivered</div>' +
            '<div id="viewHold" class="productionActions prodHold ">Hold</div>';
    }


    return html;

}

function buildProductionTableFlow2(pagination, cssClass, rows) {


    var html =
        pagination +
        '   <div id="subTabAlert" ' + cssClass + '></div>' +
        '<div class="accountRowTitle">' +
        '   <div id="id" class="prodTable-Flow2-Cell1 borderLeftTop sortable">Order #</div>' +
        '   <div id="lastName" class="prodTable-Flow2-Cell2 borderLeftTop sortable">Last Name</div>' +
        '   <div id="refNumber" class="prodTable-Flow2-Cell3 borderLeftTop sortable">Ref #</div>' +
        '   <div id="companyName" class="prodTable-Flow2-Cell4 borderLeftTop sortable">Company</div>' +
        '   <div id="quantity" class="prodTable-Flow2-Cell5 borderLeftTop ">' +
        '       <div class="stackedText">Item</div>' +
        '       <div class="stackedText">Qty</div>' +
        '   </div>' +
        '   <div id="total" class="prodTable-Flow2-Cell6 borderLeftTop ">' +
        '       <div class="stackedText">Panel</div>' +
        '       <div class="stackedText">Qty</div>' +
        '   </div>' +
        '   <div id="startedProd" class="prodTable-Flow2-Cell7 borderLeftTop sortable">' +
        '       <div class="stackedText">Date to</div>' +
        '       <div class="stackedText">Prod</div>' +
        '   </div>' +
        '   <div id="startedProd" class="prodTable-Flow2-Cell8 borderLeftTop sortable">' +
        '       <div class="stackedText">Days in</div>' +
        '       <div class="stackedText">Prod</div>' +
        '   </div>' +
        '   <div id="dueDate" class="prodTable-Flow2-Cell9 borderLeftTop sortable">' +
        '       <div class="stackedText">Date</div>' +
        '       <div class="stackedText">Due</div>' +
        '   </div>' +
        '   <div id="estComplete" class="prodTable-Flow2-Cell10 borderLeftTop sortable">' +
        '       <div class="stackedText">Est.</div>' +
        '       <div class="stackedText">Comp</div>' +
        '   </div>' +
        '   <div class="prodTable-Flow2-Cell11 borderLeftTop">' +
        '       <div class="stackedText">Glass</div>' +
        '       <div class="stackedText">Ordered</div>' +
        '   </div>' +
        '   <div class="prodTable-Flow2-Cell12 borderLeftTop">Wood</div>' +
        '   <div class="prodTable-Flow2-Cell13 borderLeftTop sortable" id="startedProd" >' +
        '       <div class="stackedText">Paid in</div>' +
        '       <div class="stackedText">Full</div>' +
        '   </div>' +
            (isSurvey() ?
                '   <div class="prodTable-Flow2-Cell14 borderLeftTop">Survey Complete</div>'
                :
                '   <div class="prodTable-Flow2-Cell14 borderLeftTop">Status</div>') +
        '   <div class="prodTable-Flow2-Cell15 borderLeftRightTop" id="company"  >Notes</div>' +
        '</div>' +
        rows +
        '<div class="finalRow"></div>';

    return html;
}


function buildProductionTableFlow1(pagination, cssClass, rows) {

    var html =
        pagination +
        '   <div id="subTabAlert" ' + cssClass + '></div>' +
        '<div class="accountRowTitle">' +
        '   <div id="id" class="prodCell1 borderLeftTop sortable">Order #</div>' +
        '   <div id="lastName" class="prodCell2_v1 borderLeftTop sortable">Last Name</div>' +
        '   <div id="companyName" class="prodCell3_v1 borderLeftTop sortable">Company</div>' +
        '   <div id="quantity" class="prodCell4_v1 borderLeftTop ">' +
        '       <div class="stackedText">Item</div>' +
        '       <div class="stackedText">Qty</div>' +
        '   </div>' +
        '   <div id="total" class="prodCell5_v1 borderLeftTop ">' +
        '       <div class="stackedText">Panel</div>' +
        '       <div class="stackedText">Qty</div>' +
        '   </div>' +
        '   <div id="startedProd" class="prodCell6_v1 borderLeftTop sortable">' +
        '       <div class="stackedText">Date to</div>' +
        '       <div class="stackedText">Prod</div>' +
        '   </div>' +
        '   <div id="startedProd" class="prodCell7_v1 borderLeftTop sortable">' +
        '       <div class="stackedText">Days in</div>' +
        '       <div class="stackedText">Prod</div>' +
        '   </div>' +
        '   <div id="dueDate" class="prodCell8_v1 borderLeftTop sortable">' +
        '       <div class="stackedText">Date</div>' +
        '       <div class="stackedText">Due</div>' +
        '   </div>' +
        // '   <div id="estComplete" class="prodCell9 borderLeftTop sortable">' +
        // '       <div class="stackedText">Est.</div>' +
        // '       <div class="stackedText">Comp</div>' +
        // '   </div>' +
        // '   <div class="prodCell10 borderLeftTop">' +
        // '       <div class="stackedText">Glass</div>' +
        // '       <div class="stackedText">Ordered</div>' +
        // '   </div>' +
        // '   <div class="prodCell11 borderLeftTop">Wood</div>' +
        '   <div id="startedProd" class="prodCell7_v1 borderLeftTop sortable">' +
        '       <div class="stackedText">Paid in</div>' +
        '       <div class="stackedText">Full</div>' +
        '   </div>' +
        '   <div class="prodCell12 borderLeftTop">Status</div>' +
        '   <div id="company" class="prodCell13 borderLeftRightTop " >Notes</div>' +
        '</div>' +
        rows +
        '<div class="finalRow"></div>';

    return html;
}


function buildPreProductionTable(rows, data) {

    var action = '  <th >Confirm</th>' +
        '           <th >Action</th>';

    if (tab == 'readyForProduction') {
        action = '<th >Action</th>';
    }

    var twoWeeksFromCurrent = new Date.today().addWeeks(2).toString('yyyy-MM-dd')
    var limitPullDown = buildLimitList();
    var pagination = '' +
        '<div id="paginationBox">' +
        '   <div id="limitPullDown" ><div id="limitPullDownText">Show: </div>' + limitPullDown + '</div> ' +
        '   <div id="pagination"></div>' +
        '   <div class="productionReport">'+
        '       <input id="prodReportDateInput" type="text"  value = "' + twoWeeksFromCurrent + '"/>' +
        '       <div id="generateProdReportButton" onClick="generateProductionReport()" class="myButton selectedButton contractButton">Production Report</div>' +
        '   </div>'+
        '</div>';


    var cssClass = 'class="' + uiState.productionStatus + '"';

    var html = '';
    if (siteDefaults.productionFlow == 2) {
        html = buildProductionTableFlow2(pagination, cssClass, rows);
    } else {
        html = buildProductionTableFlow1(pagination, cssClass, rows);
    }


    return html;
}

function buildGlassOrdered(quote) {

    var statusLabels = ['No', 'Yes'];


    var html = '<select id="glassPullDown-' + quote.id + '" class="prodGlassPullDown" onchange="updateGlass(' + quote.id + ')">';

    for (var a = 0; a < statusLabels.length; a++) {
        var selected = '';
        if (statusLabels[a] == quote.glassOrdered) {
            selected = 'selected';
        }
        html += '<option ' + selected + ' value="' + statusLabels[a] + '">' + statusLabels[a] + '</option>';
    }
    html += '</select>';

    if (isSurvey()) {
        if (quote.glassOrdered == 'Yes' ) {
            html = 'Yes';
        } else {
            html = 'No';
        }
    }

    return html;
}

function buildProductionStatus(quote) {

    if(isSurvey()){
        var statusLabels = ['No', 'Yes'];
        var status = ['Needs Survey', 'Ready For Production'];
    }else{
        var statusLabels = ['PreProd', 'Production', 'Completed', 'Delivered', 'Hold'];
        var status = ['Ready For Production', 'In Production', 'Completed', 'Delivered', 'Hold'];
    }

    var html = '<select id="pullDown-' + quote.id + '" class="prodStatusPullDown"'+
            ' onfocus="this.setAttribute(\'data-value\', this.value)"'+
            ' onchange="changeProductionStatus(' + quote.id + ', this)">';

    for (var a = 0; a < statusLabels.length; a++) {
        var selected = '';
        if (status[a] == quote.type) {
            selected = 'selected';
        }
        if (statusLabels[a] == 'Hold' && quote.status == 'hold') {
            selected = 'selected';
        }

        html += '<option ' + selected + ' value="' + status[a] + '">' + statusLabels[a] + '</option>';
    }
    html += '</select>';

    if (quote.type == 'Ready For Production' && isSurvey()) {
        html = 'Yes'
    }

    if (quote.type == 'Needs Survey' && isSurveyLimited()) {
        html = 'No'
    }


    return html;

}


function changeProductionStatus(id, _this) {

    if (isLimitedAccount()) {
        _this.value = _this.getAttribute('data-value');
        limitedAccessAccount();
        return;
    }
    var status = $('#pullDown-' + id).val();

    if (status == 'Needs Survey'){
        updateStatus(id, 'needsSurvey');
    }
    if (status == 'In Production') {
        updateStatus(id, 'inProduction');
    }
    if (status == 'Ready For Production') {
        updateStatus(id, 'preProduction');
    }
    if (status == 'Completed') {
        updateCompleted(id);
    }
    if (status == 'Delivered') {
        updateStatus(id, 'delivered');
    }

    if (status == 'Hold') {
        updateStatus(id, 'onHold');
    }
}


function buildWoodForProduction(quote) {

    var statusLabels = ['NA', 'Fir', 'Alder', 'White Oak', 'Cherry', 'Mahogany', 'Walnut', 'Mixed'];
    var html = '<select id="woodPullDown-' + quote.id + '" class="prodWoodPullDown" onchange="updateWood(' + quote.id + ')">';

    for (var a = 0; a < statusLabels.length; a++) {
        var selected = '';
        if (statusLabels[a] == quote.woodOrdered) {
            selected = 'selected';
        }
        html += '<option ' + selected + ' value="' + statusLabels[a] + '">' + statusLabels[a] + '</option>';
    }
    html += '</select>';

    if (isSurvey()) {
        if (!empty(quote.woodOrdered) ) {
            html = quote.woodOrdered;
        } else {
            html = '--';
        }
    }

    return html;
}


function updateStatus(id, status) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/" + status + "?" + authorizePlusProduction() + addSite();

    $.getJSON(url, function (apiJSON) {
        validate(apiJSON);
        trackAndDoClick(tab)
    });
}



function updateCompleted(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/completed?" + authorizePlusProduction() + addSite();

    $.getJSON(url, function (apiJSON) {
        validate(apiJSON);

        var order = apiJSON.data;
        var orderJSON = JSON.stringify(order);

        trackAndDoClick(tab);
    });
}


function updateWood(id) {

    var wood = $('#woodPullDown-' + id).val();
    var data = {'wood': wood, 'id': id};
    var json = JSON.stringify(data);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/orders/" + id + "/woodOrdered?" + authorizePlusProduction() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status != "Success") {
                alert("Failed to Save Wood Choice");

            }
        }
    });
}


function updateGlass(id) {

    var glass = $('#glassPullDown-' + id).val();
    var data = {'glass': glass, 'id': id};
    var json = JSON.stringify(data);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/orders/" + id + "/glassOrdered?" + authorizePlusProduction() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status != "Success") {
                alert("Failed to Save Glass Choice");

            }
        }
    });
}


function productionNotesKeyUp() {

    delay(function () {
        updateProductionNotes(quote.id);
    }, 350);

}

function updateProductionNotes(id) {

    var notes = $('#productionNotes').val();

    var data = {'notes': notes, 'id': id};

    var json = JSON.stringify(data);


    $.ajax({
        url: "https://" + apiHostV1 + "/api/orders/" + id + "/productionNotes?" + authorizePlusProduction() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status != "Success") {
                alert("Failed to Save Production Notes");

            }
        }
    });

}


function updateEstCompeteDateWithCountAndId(count, id) {


    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    var estComplete = $("#estComplete-" + count).val();


    var estComplete = {'estComplete': estComplete};

    var json = JSON.stringify(estComplete);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + id + "/estComplete?" + authorizePlusProduction() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {

                //writeCart(cart);
                //writeFormStatus();
                //writeQuoteID(quoteID);

            } else {
                alert("Quotes estComplete Update Save Failed");

            }
        }
    });


}

function generateProductionReport(){

    var reportDate = $("#prodReportDateInput").val();

    generateProductionReportApi(reportDate).done(function(csv){
        downloadCSV(csv);
    });
}

// function updatestartedProdDateWithCountAndId(count, id) {
//
//
//     var startedProd = $("#startedProd-" + count).val();
//
//     startedProd = {'startedProd': startedProd};
//
//     var json = JSON.stringify(startedProd);
//
//     $.ajax({
//         url: "https://" + apiHostV1 + "/api/quotes/" + id + "/startedProd?" + authorizePlusProduction() + addSite(),
//         data: json,
//         type: "PUT",
//         success: function (result) {
//
//             var response = jQuery.parseJSON(result);
//             if (response.status == "Success") {
//
//                 //writeCart(cart);
//                 //writeFormStatus();
//                 //writeQuoteID(quoteID);
//
//             } else {
//                 alert("Quotes estComplete Update Save Failed");
//
//             }
//         }
//     });
//
//
// }