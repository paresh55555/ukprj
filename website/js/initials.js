'use strict'


function initialText() {

    var text = 'Int.____';
    text = '';
    //Removed per Robert 08/02/2017
    return text;
}

function cartFinishOptionsInitials() {

    var html = '';

    if (isOrder() && isPrint()) {
        html = '<div class="finishedOptions-initials">' + initialText() + '</div>';
    }

    return html;
}


function cartDescriptionInitials() {
    
    var html = '';

    if (isOrder() && isPrint()) {
        html = '<div class="description-initials">' + initialText() +'</div>';
    }

    return html;
}


function cartDisclaimerInitials() {

    var html = '';

    if (isOrder() && isPrint()) {
        html = '<div class="disclaimer-initials">' + initialText() +'</div>';
    }

    return html;
}