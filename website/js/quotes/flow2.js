"use strict";

function renderFlow2() {

}

function addQuoteDetailsWithParametersFlow2(params) {

    var customerID = params.customerID;
    var selector = params.selector;
    var edit = params.edit;

    var companyInfo = companyInfoFlow2(quote);
    var customerInfo = customerInfoFlow2(quote);
    var submitOrder = submitOrderFlow2();
    var payWithCreditCardHTML = payCreditCardButton();
    var paymentBoxRight = '';

    if (isProduction()) {
        companyInfo = companyInfoFlow2Production(quote);
    }

    var actions = buildQuoteActions(params);

    if (tab == 'viewOrder' || tab == 'viewAccountingOrder') {
        actions = buildOrdersActions(params);
        paymentBoxRight = getPaymentHistoryFlow2();
    }
    
    var quoteDetails = '' +
        '<div class="customerFlow2">' + companyInfo + '</div>' +
        '<div class="rightSideQuote">' +
        '   <div class="customerFlow2">' + customerInfo + '</div>' +
        '   <div class="customerFlow2">' + paymentBoxRight + '</div>' +
        '   <div class="customerFlow2Long">' + submitOrder + '</div>' +
        '   <div id= "payWithCC" class="customerFlow2">' + payWithCreditCardHTML + '</div>' +
        '</div>';


    //var border = qualityBorderColor(quote);
    //if (tab == 'viewOrder') {
    //    border = getStatusBorder(quote);
    //}
    //

    var border = 'archivedBorder';

    var html = '<div id ="quoteDetailBoxID" class="quoteDetailsBox ' + border + '">' + quoteDetails + actions + '</div>';

    $(selector).html(html);

    $('#payWithCC').hide();
    $('#SaveChangesAndConvert').unbind("click").bind("click", SaveChangesAndConvert);
    $('#payWithCreditCard').unbind("click").bind("click", openPayWithCreditCard);
    $('#submitPayment').unbind("click").bind("click", payWithCreditCard);
    $('.fieldUpdate').unbind("keyup").bind("keyup", updateCustomerDelayOld);
    $('.fieldUpdate').unbind("change").bind("change", updateCustomerNoDelayOld);

    $('#enterPaymentInfoSuper').unbind("click").bind("click", enterPaymentInfo);
    $('.quoteActionButtons', '.quoteDetailsBox').unbind("click").bind("click", quoteActions);
    $('.convertToSale').unbind("click").bind("click", quoteActions);

    showAddToQuoteButton();
}


function openPayWithCreditCard() {
    $('#payWithCC').show();
}


function companyInfoFlow2(quote) {

    if (empty(quote)) {
        return '';
    }

    var html = '' +
        '<div class="quoteDetailsTitleFlow2">Company Information</div> ' +
        '<div class="customerBoxDetails">' +
        '    <div class="customerBoxDetailsLeftFlow2">Company Name:</div><div class="customerBoxDetailsRightFlow2">' +
        '      ' + quote.Customer.companyName +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Contact:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="contact" class="fieldUpdate" type="text"  value="' + quote.Customer.contact + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Address 1:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="billingAddress1"  class="fieldUpdate" type="text"  value="' + quote.Customer.billingAddress1 + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Address 2:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="billingAddress2"   class="fieldUpdate" type="text"  value="' + quote.Customer.billingAddress2 + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">City:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="billingCity"   class="fieldUpdate" type="text"  value="' + quote.Customer.billingCity + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">State:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="billingState"   class="fieldUpdate" type="text"  value="' + quote.Customer.billingState + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Zipcode:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="billingZip"   class="fieldUpdate" type="text"  value="' + quote.Customer.billingZip + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">eMail:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="email"   class="fieldUpdate" type="text"  value="' + quote.Customer.email + '" />' +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Phone #:</div><div class="customerBoxDetailsRightFlow2">' +
        '       <input id="phone"   class="fieldUpdate" type="text"  value="' + quote.Customer.phone + '" />' +
        '    </div>' +
        '</div>';

    return html;
}


function companyInfoFlow2Production(quote) {

    var html = '' +
        '<div class="quoteDetailsTitleFlow2">Company Information</div> ' +
        '<div class="customerBoxDetails">' +
        '    <div class="customerBoxDetailsLeftFlow2">Company Name:</div><div class="customerBoxDetailsRightFlow2">' +
        '      ' + quote.Customer.companyName +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Contact:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.contact +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Address 1:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.billingAddress1 +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Address 2:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.billingAddress2 +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">City:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.billingCity +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">State:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.billingState +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Zipcode:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.billingZip +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">eMail:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.email +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Phone #:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.Customer.phone +
        '    </div>' +
        '</div>';

    return html;
}

function customerInfoFlow2(quote) {

    var paidInFullHTML = '';

    if (quote.paidInFull == 'yes') {
        paidInFullHTML = '<div class="paidInFullHeader"><img src="/images/icons/paid.svg" alt="Paid"></div>';
    }

    var dateHTML = '' +
        '<div class="customerBoxDetailsLeftFlow2Relative">' + paidInFullHTML + 'Origination Date:</div><div class="customerBoxDetailsRightFlow2">' +
        '   ' + cleanDate(quote.orderDate) +
        '</div>' +
        '<div class="customerBoxDetailsLeftFlow2">Will Be Completed:</div><div class="customerBoxDetailsRightFlow2">' +
        '   ' + cleanDate(quote.estComplete) +
        '</div>';

    if (quote.type == 'Confirming Payment') {
        dateHTML = '' +
            '<div class="customerBoxDetailsLeftFlow2">Quote Date:</div><div class="customerBoxDetailsRightFlow2">' +
            '   ' + cleanDate(quote.quoted) +
            '</div>' +
            '<div class="customerBoxDetailsLeftFlow2">Will Be Completed:</div><div class="customerBoxDetailsRightFlow2">' +
            'Pending Payment' +
            '</div>'
    }

    if (quote.type == 'quote') {
        dateHTML = '' +
            '<div class="customerBoxDetailsLeftFlow2">Quote Date:</div><div class="customerBoxDetailsRightFlow2">' +
            '   ' + cleanDate(quote.quoted) +
            '</div>';
    }

    var customerPOHTML = '<input id="customerPO" class="fieldUpdate fieldUpdateSmall" type="text"  value= "' + quote.Customer.customerPO + '" />';
    var customerRefHTML = '<input id="customerREF" class="fieldUpdate fieldUpdateSmall" type="text"  value= "' + quote.Customer.customerREF + '" />';


    if (isProduction()) {
        customerPOHTML = quote.Customer.customerPO;
        customerRefHTML = quote.Customer.customerREF;
    }

    var html = '' +
        '<div class="quoteDetailsTitleFlow2">Job Information</div> ' +
        '<div class="customerBoxDetails">' +
        '   ' + dateHTML +
        '    <div class="customerBoxDetailsLeftFlow2">Job#:</div><div class="customerBoxDetailsRightFlow2">' +
        '       ' + quote.prefix + quote.id +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Customer PO#:</div><div class="customerBoxDetailsRightFlow2">' +
        '   ' + customerPOHTML +
        '    </div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Customer Ref:</div><div class="customerBoxDetailsRightFlow2">' +
        '   ' + customerRefHTML +
        '    </div>' +
        '</div>';


    return html;
}


function createExpMonth() {

    var html  = '' +
        '<select name="ccMonth" id="ccMonth" onchange="" size="1">'+
        '   <option value="01">January (01)</option>'+
        '   <option value="02">February (02)</option>'+
        '   <option value="03">March (03)</option>'+
        '   <option value="04">April (04)</option>'+
        '   <option value="05">May (05)</option>'+
        '   <option value="06">June (06)</option>'+
        '   <option value="07">July (07)</option>'+
        '   <option value="08">August (08)</option>'+
        '   <option value="09">September (09)</option>'+
        '   <option value="10">October (10)</option>'+
        '   <option value="11">November (11)</option>'+
        '   <option val ue="12">December (12)</option>'+
        '</select>';

    return html;
}


function createExpYear() {

    var html  = '' +
        '<select name="ccYear" id="ccYear" onchange="" size="1">'+
        '   <option value="2016">2016</option>'+
        '   <option value="2017">2017</option>'+
        '   <option value="2018">2018</option>'+
        '   <option value="2019">2019</option>'+
        '   <option value="2020">2020</option>'+
        '   <option value="2021">2021</option>'+
        '   <option value="2022">2022</option>'+
        '   <option value="2023">2023</option>'+
        '   <option value="2024">2024</option>'+
        '   <option value="2025">2025</option>'+
        '   <option value="2026">2026</option>'+
        '   <option value="2027">2027</option>'+
        '   <option value="2028">2028</option>'+
        '   <option value="2029">2029</option>'+
        '   <option value="2030">2030</option>'+
        '   <option value="2031">2031</option>'+
        '   <option value="2032">2032</option>'+
        '   <option value="2033">2033</option>'+
        '   <option value="2034">2034</option>'+
        '   <option value="2035">2035</option>'+
        '   <option value="2036">2036</option>'+
        '   <option value="2037">2037</option>'+
        '   <option value="2038">2038</option>'+
        '   <option value="2039">2039</option>'+
        '   <option value="2040">2040</option>'+
        '</select>';

    return html;
}


function currentTotalDiscount() {


    //var amount = getDiscountTotals(quote.SalesDiscount);

    quote.SalesDiscount.Totals.cost.amount = calculateCostFromCodeSA(quote.SalesDiscount.Totals.code);

    var discountTotal = getDiscountTotals( quote.SalesDiscount);

    var html = " (You are receiving " + currency + formatMoney(discountTotal)+" in discounts)";
    if (discountTotal < 1) {
        html = '';
    }

    return html;
}

function payCreditCardButton() {

    var nameHTML = '<input id="ccName" class=" fieldUpdateSmall" type="text"  value= "" />';
    var ccHTML = '<input id="ccNumber" class=" fieldUpdateSmall" type="text"  value= "" />';
    var cvvHTML = '<input id="ccCVV" class=" fieldUpdateSmall" type="text"  value= "" />';
    var monthHTML = createExpMonth();
    var yearHTML = createExpYear();
    var zipHTML = '<input id="ccZip" class=" fieldUpdateSmall" type="text"  value= "' + quote.Customer.billingZip + '" />';

    var discountHTML = currentTotalDiscount();

    var ccFields = ''+
        '<div class="customerBoxDetails">' +
        '    <div class="customerBoxDetailsLeftFlow2">Name on Card</div>' +
        '    <div class="customerBoxDetailsRightFlow2">' + nameHTML + '</div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Credit Card #</div>' +
        '    <div class="customerBoxDetailsRightFlow2">' + ccHTML + '</div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Security Code</div>' +
        '    <div class="customerBoxDetailsRightFlow2">' + cvvHTML + '</div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Exp Month</div>' +
        '    <div class="customerBoxDetailsRightFlow2">' + monthHTML + '</div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Exp Year</div>' +
        '    <div class="customerBoxDetailsRightFlow2">' + yearHTML + '</div>' +
        '    <div class="customerBoxDetailsLeftFlow2">Billing ' + zipShort + '</div>' +
        '    <div class="customerBoxDetailsRightFlow2">' + zipHTML + '</div>' +
        '</div>' +
        '<div class="chargeDiscountBox">' +
        '    <div class="customerBoxDetailsLeftFlow2">Charge Amount</div>' +
        '    <div class="customerBoxDetailsRightFlow2"><b>'+ currency + formatMoney(quote.total)+'</b></div>' +
        '    <div class="chargeDiscountNotice">'+ discountHTML +'</div>' +
        '</div>';

    var submitOrderHTML = '' +
        ccFields +
        '<div id="submitPayment" class="convertToSale2 myButton selectedButton">Submit Payment</div>' ;


    if ( (tab == 'viewOrder' || isProduction() || isAccounting()) && quote.paidInFull == 'yes' ) {
        submitOrderHTML = '';
    }

    return submitOrderHTML;
}


function submitOrderFlow2() {


    var payLater = '<div id="SaveChangesAndConvert" class="convertToSale2 myButton selectedButton">Pay Later</div>' ;

    if (( tab == 'viewOrder' || isProduction() || isAccounting() ) && quote.paidInFull != 'yes') {
        payLater = '';
    }

    var payWithCreditCardHTML  = '<div id="payWithCreditCard" class="convertToSale2 myButton selectedButton">Pay With Credit Card</div>';
    if (empty(siteDefaults.takesCreditCards)) {
        payWithCreditCardHTML ='';
        payLater =  '<div id="SaveChangesAndConvert" class="convertToSale2 myButton selectedButton">Convert To Order</div>' ;
    }

    var submitOrderHTML = '' +
        '<div id="terms"  >' +
        '   <input id="termsChecked" type="checkbox" name="TermsCheck" value="yes">' +
        '   I agree and accept <a href="' + siteDefaults.termsURL + '" target="_blank" >' + siteDefaults.companyName + ' Term and Conditions</a>' +
        '</div>' +
        '<div class="invoiceShipping" ><B>Shipping:</B> All orders are Will Call only.</div>' +
        payWithCreditCardHTML +
        payLater ;

    if (( tab == 'viewOrder' || isProduction() || isAccounting() ) && (quote.paidInFull == 'yes' || empty(siteDefaults.takesCreditCards) )) {
            submitOrderHTML = '';
    }

    return submitOrderHTML;
}

function updateCustomerDelayOld() {


    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    var selector = this;

    $(this).css('border-color', 'red');
    //$('#savingMessageComment').html("Saving...");
    delay(function () {
            updateCustomerOld(selector);
    }, 350);
}


function updateCustomerDelay() {


    if (isLimitedAccount() ) {
        limitedAccessAccount();
        return;
    }

    var selector = this;

    $(this).css('border-color', 'red');
    //$('#savingMessageComment').html("Saving...");
    delay(function () {
        updateCustomer(selector);
    }, 350);
}

function updateCustomerNoDelay() {
    var selector = this;
    updateCustomer(selector);
}

function updateCustomerNoDelayOld() {
    var selector = this;
    updateCustomerOld(selector);
}

function updateCustomerOld(selector) {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    var value = $(selector).val();
    var field = $(selector).attr('id');

    var update = {};

    // quote.address[field] = value;
    // writeQuote(quote);

    update['field'] = field;
    update['value'] = value;

    // var json = JSON.stringify(update);
    //
    putCustomerUpdate(update).done(function (result) {
        $('#'+field).css('border-color', '#e7e7e7');
    });

    // putCustomerUpdate(update);
}

function updateCustomer(selector) {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }

    if(empty(quote)){
        return;
    }

    var value = $(selector).val();
    var field = $(selector).attr('id');

    var update = {};
    update[field] = value;

    quote.address[field] = value;
    writeQuote(quote);

    if(field == 'shipping_zip' || field == 'shipping_city'){
        updateTax();
    }

    update['sales_person_id'] = user.id;

    var json = JSON.stringify(update);
    updateAddressesAPI(quote.id, json).done(function (result) {
        $('#'+field).css('border-color', '#e7e7e7');
        generateAndSendDocument(quote.id);
    });

    //
    // putCustomerUpdate(update).done(function (result) {
    //     $('#'+field).css('border-color', '#e7e7e7');
    // });

    // putCustomerUpdate(update);
}

function updateTax(){
    var city = $('#shipping_city').val(),
        zip = $('#shipping_zip').val();

    if(city && zip){
        var resale = discounts.Totals && discounts.Totals.tax && discounts.Totals.tax.resale
        if(resale)return;
        getSalesTax(city, zip).then(function(response){
            discounts.Totals.tax.amount = response.rate;
            writeDiscount(discounts)
            saveQuoteToDB();
        });
    }
}

function hasCart() {

    if (isEmptyObject(quote.Cart)) {
        return false;
    }

    return true;
}


function hasCustomer() {

    if (empty($('#contact').val())) {
        alert("Please add a contact name");
        return false;
    }

    if (empty($('#billingAddress1').val())) {
        alert("Please add your address ");
        return false;
    }

    if (empty($('#billingCity').val())) {
        alert("Please add your city");
        return false;
    }

    if (empty($('#billingState').val())) {
        alert("Please add your state");
        return false;
    }

    if (empty($('#billingZip').val())) {
        alert("Please add your zipcode");
        return false;
    }

    if (empty($('#email').val())) {
        alert("Please add your email");
        return false;
    }

    if (empty($('#phone').val())) {
        alert("Please add your phone");
        return false;
    }

    return true;
}

function SaveChangesAndConvertFlow2() {


    if (!hasCart()) {
        alert("Please add at least one door to your order");
        return;
    }

    if (!hasCustomer()) {
        return;
    }

    if (!document.getElementById('termsChecked').checked) {
        alert("Please check that you agree to the terms and conditions.");
        return;
    }

    startSpinner();

    if (empty(quote.prefix)) {
        quote.prefix = user.prefix;
    }
    quote.postfix = 'S';

    writeQuote(quote);

    quote = removeNoCart(quote);

    // if (!empty(quote.address)) {
        delete quote.address;
        delete quote.followUp;
        delete quote.secondFollowUp;
        delete quote.estComplete;
        delete quote.startedProd;
        delete quote.contacted;
        delete quote.address;
    // }

    var json = JSON.stringify(quote);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                convertOrderToPendingProduction();
            } else {
                alert("Quotes Save Failed");
            }
        }
    });


}