"use strict";


function loadQuotes(quality) {

    startSpinner();
    if (empty(quality)) {
        quality = uiState.quality;
    }

    selectTab('#viewQuotes');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();
    formStatus = {};
    quote = {};

    var topButtons = buildTopButtons();
    var searchBox = buildSearchBox();
    $('#preMainContent').html(topButtons + searchBox);

    $('#search').keyup(quoteSearchDelay);

    $('#newQuote').unbind("click").bind("click", salesStartOver);

    $('#cartButton').hide();
    updateFormStatus();
    quotesSearch(quality);
}


function buildSearchBox() {

    var searchFields = buildSearchFields();

    var search = getParameter('search');
    if (empty(search)) {
        search = '';
    }

    var html =
        '<div id="searchBoxQuote">' +
        '   <img id="searchIcon" src=" /images/icons/search.svg">' +
        '   <input id="search" type="text" value="' + search + '" placeholder="Search" name="search">' +
        '</div>';

    return html;

}

function addSearch() {

    var search = $('#search').val();
    var uriString = '&search=' + search;

    return uriString;
}


function quotesSearch(quality, newSearch) {

    if (empty(quality)) {
        quality = '';
    }

    if (quality == 'All') {
        quality = '';
    }

    if (siteDefaults.salesFlow == 2) {
        quality = '';
    }

    var order = getOrder();

    if (quality == 'medium') {
        quality = 'warm';
    }
    var url = "https://" + apiHostV1 + "/api/quotes?" + authorizePlusSalesPerson() + '&lead=' +
        quality + pageLimitText(newSearch) + order + addSite() + addSearch() + addSuperToken();

    $.getJSON(url, function (apiJSON, status, xhr) {

        if(!validate(apiJSON))return;
        var total = xhr.getResponseHeader('Total');

        var html = buildViewQuotes(apiJSON);
        $('#mainContent').html(html);
        $('.quoteActions').unbind("click").bind("click", quoteActions);
        $('.leadActions').unbind("click").bind("click", viewQuoteBasedOnLead);
        $(".quoteDateBoxList").datepicker({gotoCurrent: true});

        $('.sortable').unbind("click").bind("click", orderTableResultsWithTab);


        setPaginationWithSelectorAndTotal('#pagination', total, newSearch);
        //setQuotesTitleRowColor('.quoteRowTitle');


        showContentAndSaveState();


    });


}

function quoteSearchNoDelay() {

    var selector = this;
    var newSearch = 'yes';
    startSpinner();
    quotesSearch('', newSearch);
}

function quoteSearchDelay() {

    var selector = this;

    delay(function () {
        var newSearch = 'yes';
        startSpinner();
        quotesSearch('', newSearch);
    }, 300);

}

//function orderQuoteResults() {
//
//    var order = $(this).attr('id');
//    var direction = getDirection();
//
//    trackAndDoClick('viewQuotes','&order='+order+'&direction='+direction+pageLimitText());
//
//}


function getOrder() {

    var order = getParameter('order');

    var direction = getDirection();

    if (empty(order)) {
        order = 'id';
        direction = 'desc';
    }
    var orderString = '&order=' + order + '&direction=' + direction;

    return orderString;
}

function getDirection() {

    var direction = getParameter('direction');


    if (direction == 'asc') {
        direction = 'desc';
    } else {
        direction = 'asc';
    }

    return direction;

}


function paginationSelect(page) {

    var limit = getParameter('limit');
    if (empty(limit)) {
        limit = 10;
    }

    var search = $('#search').val();

    trackAndDoClick(tab, '&page=' + page + '&limit=' + limit + '&search=' + search);

}


function setQuotesTitleRowColor(selector) {

    var cssClass = '';
    var css = {"background-color": "#b8b8b8", "color": "#393939"};
    if (tab == 'viewQuotesHot') {
        cssClass = 'hot';
        css = {"background-color": "#e2080b", "color": "#FFF"};
    } else if (tab == 'viewQuotesMedium') {
        cssClass = 'medium';
        css = {"background-color": "#f2d930", "color": "#393939"};
    } else if (tab == 'viewQuotesCold') {
        cssClass = 'cold';
        css = {"background-color": "#0ab306", "color": "#393939"};
    } else if (tab == 'viewQuotesHold') {
        cssClass = 'hold';
        css = {"background-color": "#afafaf", "color": "#393939"};
    } else if (tab == 'viewQuotesArchived') {
        cssClass = 'archived';
        css = {"background-color": "#575757", "color": "#FFF"};
    } else if (tab == 'viewQuotesNew') {
        cssClass = 'newQuote';
        css = {"background-color": "#0598d5", "color": "#FFF"};
    }
    else {
        cssClass = '';
        css = {"background-color": "#FFF", "color": "#393939"};
    }


    $(selector).css(css);
    //return (cssClass);


}


function loadViewQuote() {

    var quoteID = getParameter('quote');
    var salesPerson = getParameter('salesPerson');
    var production = getParameter('production');
    var accounting = getParameter('accounting');
    var print = getParameter('print');

    $('#footerRight').show();

    // if (empty(quoteID)) {
    //     trackAndDoClick('viewQuotes', "&blankQuote");
    //     return;
    // }

    var url = '';

    if (print === 'yes') {
        var token = getParameter('token');
        var site = getParameter('site');
        var superToken = getParameter('superToken');
        url = "https://" + apiHostV1 + "/api/quotes/" + quoteID + "?&salesPerson=" +
            salesPerson + "&token=" + token + '&production=' + production + '&accounting=' + accounting + '&site=' + site + '&print=yes&superToken=' + superToken;
    } else {
        url = "https://" + apiHostV1 + "/api/quotes/" + quoteID + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken();
    }

    getViewQuote(url).done(function (data) {
        if (validate(data)) {
            if (data.type == 'order' && print != 'yes') {
                window.location = "https://"+location.hostname+"/?tab=viewOrder&order="+quoteID;
                exit();
            }
            renderFlow1(data)
        } else {
            trackJs.track(data);
        }
    })
}


function viewQuoteBasedOnLead() {

    var quality = $(this).attr('id');

    uiState.quality = quality;
    writeStateUI();
    startSpinner();
    chooseQuoteViewType(quality);


}

function chooseQuoteViewType(quality) {

    if (quality == 'hot') {
        trackAndDoClick('viewQuotesHot');
    } else if (quality == 'new') {
        trackAndDoClick('viewQuotesNew');
    } else if (quality == 'warm') {
        trackAndDoClick('viewQuotesMedium');
    } else if (quality == 'cold') {
        trackAndDoClick('viewQuotesCold');
    } else if (quality == 'hold') {
        trackAndDoClick('viewQuotesHold');
    } else if (quality == 'archived') {
        trackAndDoClick('viewQuotesArchived');
    } else {
        trackAndDoClick('viewQuotes');
    }

}
function qualityBorderColor(quote) {

    var quality = quote.leadQuality;

    var cssClass = '';

    if (quality == 'Hot') {
        cssClass = 'hotBorder';
    } else if (quality == 'New') {
        cssClass = 'newQuoteBorder';
    } else if (quality == 'Warm') {
        cssClass = 'mediumBorder';
    } else if (quality == 'Cold') {
        cssClass = 'coldBorder';
    } else if (quality == 'Hold') {
        cssClass = 'holdBorder';
    } else if (quality == 'Archived') {
        cssClass = 'archivedBorder';
    } else {
        cssClass = '';
    }

    return cssClass;


}
function now() {

    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();

    if (day < 10) {
        []
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }

    var date = month + "/" + day + "/" + year;
    if (isUK()) {
       date =  day + "/" + month + "/" + year;
    }
    return date

}

function buildItemTotalForPrint() {

    var itemTotalHTML = '';
    if (quote.SalesDiscount.Totals.cost.showOnQuote) {
        var itemTotal = calculateCostFromDiscount();
        itemTotalHTML = '<div class="full"><div class="leftProjectLongLeft">ITEM SUBTOTAL:</div><div class="dollar">' + currency + '</div><div class="rightMoney" >' + formatMoney(itemTotal) + '</div></div>';
    }

    return itemTotalHTML;
}

function buildDiscountForPrint(SalesDiscount) {

    var data = SalesDiscount.Discounts;
    var preCost = SalesDiscount.Totals.preCost;
    var cost = SalesDiscount.Totals.cost.amount;

    var html = '';
    for (var a = 0; a < data.length; a++) {

        var discount = data[a];
        if (discount.showOnQuote) {
            if (!empty(discount.amount)) {
                var row = '<div class="full"><div class="discount leftProjectLongLeft red">' + discount.name + ':</div><div class="dollar">' + currency + '</div><div class="rightMoney">-' + formatMoney(discount.amount) + '</div></div>';
                if (discount.type == 'percentage') {
                    row = '<div class="full"><div class="discount leftProjectLongLeft red">' + discount.name + ' (' + Number(discount.amount).toFixed(2) + '%):</div><div class="dollar">' + currency + '</div><div class="rightMoney">-' + formatMoney(discount.amount * cost / 100) + '</div></div>';
                }
                if (discount.amount == 0) {
                    row = '';
                }
                html = html + row;
            }
        }
    }

    return html;


}

function buildDiscountTotalForPrint() {

    var discountTotals = getDiscountTotals(quote.SalesDiscount);
    var row = '';



    if (quote.SalesDiscount.Totals.discountTotal === undefined) {
        quote.SalesDiscount.Totals.discountTotal = {};
        quote.SalesDiscount.Totals.discountTotal.showOnQuote = false;
    }

    if (empty(quote.SalesDiscount.Totals.discountTotal)) {
        return '';
    }

    if (!empty(quote.SalesDiscount.Totals.discountTotal.showOnQuote)) {
        if (!empty(quote.SalesDiscount.Discounts)) {

            row = '<div class="full"><div class="leftProjectLongLeft red">Discount Total:</div><div class="dollar">' + currency + '</div><div class="rightMoney">-' + formatMoney(discountTotals) + '</div></div>';
        }
    }

    return row;
}

function buildSubTotalForPrint() {

    var subTotalHTML = '';
    if (quote.SalesDiscount.Totals.subTotal === undefined) {
        quote.SalesDiscount.Totals.subTotal = {};
        quote.SalesDiscount.Totals.discountTotal.showOnQuote = true;
    }

    if (quote.SalesDiscount.Totals.subTotal.showOnQuote) {
        var subtotal = getSubTotalForSalesAdmin(quote.SalesDiscount);
        subTotalHTML = '<div class="full"><div class="leftProjectLongLeft">Item SubTotal:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(subtotal) + '</div></div>';
    }

    return subTotalHTML;

}


function buildTaxesForPrint() {

    var taxesHTML = '';

    if (quote.SalesDiscount.Totals.tax === undefined) {
        quote.SalesDiscount.Totals.tax = {};
        quote.SalesDiscount.Totals.tax.showOnQuote = false;
    }

    //Hard Coding Tax not to show on paper work
    // Removed -- Harding to show -- 05/09/18
    if (!isUK()) {
        quote.SalesDiscount.Totals.tax.showOnQuote = true;
    }

    if (quote.SalesDiscount.Totals.tax.showOnQuote && siteDefaults.preTax != 1) {
        var taxAmount = getTaxAmount(quote.SalesDiscount);
        var resale = quote.SalesDiscount.Totals.tax.resale

        taxesHTML = '<div class="full"><div class="leftProjectLongLeft">' + tax + ' (' + quote.SalesDiscount.Totals.tax.amount + '%):</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(taxAmount) + '</div></div>';

        if (!empty(resale) ) {
            taxesHTML = '<div class="full"><div class="leftProjectLongLeft">' + tax + ' (' + quote.SalesDiscount.Totals.tax.amount + '%)</div>' +
                '<div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(taxAmount) + '</div>' +
                '<div class="leftProjectLongLeft"> Resale #: '+resale+'</div>' +
        '</div>';

        }
            }

    return taxesHTML;
}

function buildShippingForPrint() {

    var HTML = '';

    if (quote.SalesDiscount.Totals.shipping === undefined) {
        quote.SalesDiscount.Totals.shipping = {};
        quote.SalesDiscount.Totals.shipping.showOnQuote = false;
    }
    if (quote.SalesDiscount.Totals.shipping.showOnQuote) {
        if (isUK()) {
            HTML = '<div class="full"><div class="leftProjectLongLeft">Delivery:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(quote.SalesDiscount.Totals.shipping.amount) + '</div></div>';
        } else {
            HTML = '<div class="full"><div class="leftProjectLongLeft">Shipping:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(quote.SalesDiscount.Totals.shipping.amount) + '</div></div>';

        }
    }

    return HTML;
}


function buildInstallForPrint() {

    var HTML = '';
    if (quote.SalesDiscount.Totals.installation === undefined) {
        quote.SalesDiscount.Totals.installation = {};
        quote.SalesDiscount.Totals.installation.showOnQuote = false;
    }
    if (quote.SalesDiscount.Totals.installation.showOnQuote) {
        HTML = '<div class="full"><div class="leftProjectLongLeft">' + returnBlankIfUndefined(quote.SalesDiscount.Totals.installation.type) + ' Install:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(quote.SalesDiscount.Totals.installation.amount) + '</div></div>';
    }
    return HTML;
}

function buildExtraForPrint() {

    var HTML = ''
    if (quote.SalesDiscount.Totals.extra === undefined) {
        quote.SalesDiscount.Totals.extra = {};
        quote.SalesDiscount.Totals.extra.showOnQuote = false;
    }
    if (quote.SalesDiscount.Totals.extra.showOnQuote) {
        HTML = '<div class="full"><div class="leftProjectLongLeft">' + quote.SalesDiscount.Totals.extra.name + '</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(quote.SalesDiscount.Totals.extra.amount) + '</div></div>';
    }
    return HTML;
}

function buildAdditionalExtraForPrint() {

    var HTML = ''

    if (empty(quote.SalesDiscount.Totals.additionalExtras)) {
        return HTML;
    }

    for (var a = 0; a < quote.SalesDiscount.Totals.additionalExtras.length; a++) {

        var additionalExtra = quote.SalesDiscount.Totals.additionalExtras[a];

        if (additionalExtra.showOnQuote) {
            HTML = HTML +  '<div class="full"><div class="leftProjectLongLeft">' + additionalExtra.name + '</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(additionalExtra.amount) + '</div></div>';
        }
    }

    return HTML;
}


function buildScreensForPrint() {

    var HTML = '';
    if (quote.SalesDiscount.Totals.screens === undefined) {
        quote.SalesDiscount.Totals.screens = {};
        quote.SalesDiscount.Totals.screens.showOnQuote = true;
    }


    if (quote.SalesDiscount.Totals.screens.showOnQuote) {
        HTML = '<div class="full"><div class="leftProjectLongLeft">Screens</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(quote.SalesDiscount.Totals.screens.amount) + '</div></div>';
    }
    return HTML;
}


function buildGrandTotalForPrint() {

    var HTML = '';
    var total = getTotalForSalesAdmin(quote.SalesDiscount);

    if (quote.SalesDiscount.Totals.finalTotal === undefined) {
        quote.SalesDiscount.Totals.finalTotal = {};
        quote.SalesDiscount.Totals.finalTotal.showOnQuote = true;
    }
    if (quote.SalesDiscount.Totals.finalTotal.showOnQuote) {
        HTML = '<div class="full"><div class="leftProjectLongLeft">Grand Total:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(total) + '</div></div>';
    }
    return HTML;
}


function buildDepositForPrint() {

    var total = getTotalForSalesAdmin(quote.SalesDiscount);
    var deposit = total / 2;

    var HTML = '<div class="full"><div class="leftProjectLongLeft">Deposit:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(deposit) + '</div></div>';

    if (siteDefaults.salesFlow == 2 || siteDefaults.salesFlow == 3) {
        HTML = '';
    }
    return HTML;
}


function buildPaymentForPrint() {

    var total = getTotalForSalesAdmin(quote.SalesDiscount);
    var deposit = total / 2;

    if (empty(quote.SalesDiscount.Payments)) {
        return '';
    }

    var payments = quote.SalesDiscount.Payments;
    var paymentsTotal = 0;
    var HTML = '';

    for (var a = 0; a < payments.length; a++) {
        var payment = payments[a];
        paymentsTotal = parseFloat(paymentsTotal) + parseFloat(payment.amount);
        var count = a + 1;
        HTML = HTML + '<div class="full"><div class="leftProjectLongLeft red">Payment' + count + ':</div><div class="dollar">' + currency + '</div><div class="rightMoney">-' + formatMoney(payment.amount) + '</div></div>';

    }

    HTML = HTML + '<div class="full"><div class="leftProjectLongLeft red">Payments Total:</div><div class="dollar">' + currency + '</div><div class="rightMoney">-' + formatMoney(paymentsTotal) + '</div></div>';

    var costTotal = getTotalForSalesAdmin(quote.SalesDiscount);
    var amountDue = parseFloat(costTotal) - parseFloat(paymentsTotal);

    HTML = HTML + '<div class="full"><div class="leftProjectLongLeft">Remaining Balance:</div><div class="dollar">' + currency + '</div><div class="rightMoney">' + formatMoney(amountDue) + '</div></div>';

    return HTML;
}


function urldecode(str) {

    return decodeURIComponent((str + '')
        .replace(/%(?![\da-f]{2})/gi, function () {
            return '%25';
        })
        .replace(/\+/g, '%20'));
}


function pad(n) {

    return (n < 10) ? ("0" + n) : n;
}


function addTwoDay(passedDate) {

    var goodDate = cleanDate(passedDate);

    if (goodDate == '--' || empty(goodDate)) {
        return '--';
    }

    var newDate = Date.parse(goodDate).add({days: 2}).toString('MM/dd/yyyy')

    return newDate;
}


function changePickupDelivery() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    var pickupDelivery = $("#pickupDelivery").val();
    var json = JSON.stringify({"pickupDelivery": pickupDelivery});

    quote.pickupDelivery = pickupDelivery;
    writeQuote(quote);

    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/pickupDelivery?" + authorize() + addSite() + addSuperToken();

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                generateAndSendDocument(quote.id);
            } else {
                alert("Failed to Save Pickup /Delivery")
            }
        }
    });
}


function changeOrderOnHold(id) {
    //
    // if (isLimitedAccount()) {
    //     limitedAccessAccount();
    //     return;
    // }

    var onHold = $('#onHold').val();
    var status = 'new';

    if (onHold == 'Yes') {
        status = 'hold';
    }

    var json = JSON.stringify(status);

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateHoldStatus?" + authorizePlusSalesPerson() + addSite() + addSuperToken();

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            updateLeadStatusFromRow(status, id);
            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                if (!isProduction()) {
                    trackAndDoClick('viewOrder', '&order=' + id);
                }
            } else {
                alert("Failed to Hold Quote")
            }
        }
    });

    return;


    var pickupDelivery = $("#pickupDelivery").val();
    var json = JSON.stringify({"pickupDelivery": pickupDelivery});

    quote.pickupDelivery = pickupDelivery;
    writeQuote(quote);

    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/pickupDelivery?" + authorize() + addSite() + addSuperToken();

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                generateAndSendDocument(quote.id);
            } else {
                alert("Failed to Save Pickup /Delivery")
            }
        }
    });
}

function buildPickupDeliveryPullDown() {


    var statusLabels = ['', 'Pickup', 'Delivery'];
    var html = '<select id="pickupDelivery" class="leadStatusPullDown" onchange="changePickupDelivery(' + quote.id + ')">';

    for (var a = 0; a < statusLabels.length; a++) {
        var selected = '';
        if (statusLabels[a] == quote.pickupDelivery) {
            selected = 'selected';
        }
        html += '<option ' + selected + ' value="' + statusLabels[a] + '">' + statusLabels[a] + '</option>';
    }
    html += '</select>';

    if (isAudit()) {
        html = quote.pickupDelivery;
    }
    return html;

}
function buildQuoteTitleInfo(quote) {

    var quoteData = splitOptionFirstSpace(quote.quoted);
    var printDate = now();
    var quoteTitle = makeQuoteTitle(quote);

    var order = getParameter('order');
    var print = getParameter('print');
    var purchaseCert = getParameter('purchaseCert');

    var type = 'Quote';
    if (order == 'yes') {
        type = 'Order';
        quoteData = quote.orderDate;
    }


    var manifest = getParameter('manifest');

//    var customerEstimate = addTwoDay(quote.dueDate);
    var customerEstimate = quote.dueDate;

    var po = quote.po;
    var poText = "PO#:";

    if ((siteDefaults.salesFlow == 3 || siteDefaults.salesFlow == 1) && print != 'yes') {
        po = '<input id="po" onkeyup="updatePoDelay(' + quote.id + ')"  class="poBox" type="text"  value= "' + quote.po + '" />';
        poText = "PO / Cust. Ref#:";

        if (isAudit()) {
            po = '<input id="po" onkeyup="updatePoDelay(' + quote.id + ')"  class="poBox" type="text"  value= "' + quote.po + '" disabled />';

        }
    }

    var quoteTitleInfo = '' +
        '           <div class="leftProject">' + type + ' #:</div><div class="rightProjectShort">' + quoteTitle + '</div>' +
        '           <div class="leftProject">' + type + ' Date:</div><div class="rightProjectShort">' + quoteData + '</div>' +
        '           <div class="leftProject">Print Date:</div><div class="rightProjectShort">' + printDate + '</div>' +
        '           <div class="leftProject">' + poText + '</div><div class="rightProjectShort">' + po + '</div>' +
        '           <div class="leftProject">Est. Completion:</div><div class="rightProjectShort">' + customerEstimate + '</div>';

    if (purchaseCert == 'yes') {

        var completedDate = splitOptionFirstSpace(quote.completedDate);
     
        quoteTitleInfo = '' +
            '           <div class="leftProject">' + type + ' #:</div><div class="rightProjectShort">' + quoteTitle + '</div>' +
            '           <div class="leftProject">Print Date:</div><div class="rightProjectShort">' + printDate + '</div>' +
            '           <div class="leftProject">Completed Date</div><div class="rightProjectShort">' + completedDate + '</div>';

    }


    if (manifest == "yes") {

        var title = makeQuoteTitle(quote);
        quoteTitleInfo = '' +
            '           <div class="leftProject">Order #:</div><div class="rightProjectShort">' + title + '</div>' +
            '           <div class="leftProject">PO #:</div><div class="rightProjectShort">' + quote.po + '</div>';
    }

    return quoteTitleInfo;
}


function getCompanyAddress(quote) {

    var address2 = '';

    if (!empty(siteDefaults.address2)) {
        address2 = '<div class="companyInfo">' + siteDefaults.address2 + '</div>';
    }

    var phone = quote && quote.SalesPersonPhone;
    if (empty(phone)) {
        phone = siteDefaults.phone;
    }


    var address = '' +
        '       <div class="companyInfo">' + siteDefaults.address1 + '</div>' +
        address2 +
        '       <div class="companyInfo">' + siteDefaults.city + ', ' + siteDefaults.state + ' ' + siteDefaults.zip + '</div>' +
        '       <div class="companyInfo">' + phone + '</div>';

    if (isUK()) {
        address = '' +
            '       <div class="companyInfo">' + siteDefaults.address1 + '</div>' +
            '       <div class="companyInfo">' + siteDefaults.city + '</div>' +
            '       <div class="companyInfo">' + siteDefaults.zip + '</div>' +
            '       <div class="companyInfo">' + siteDefaults.phone + '</div>';
    }

    return address;

}
function buildCustomerAndQuoteOverView(quote) {

    if (empty(quote.address)) {
        quote.address = createEmptyAddress();
    }

    var customer = quote.Customer;

    var print = getParameter('print');
    var prefix = getParameter('prefix');

    if (!empty(customer)) {
        var name = customer.firstName + " " + customer.lastName;
    }


    var orderTitle = '';
    var order = getParameter('order');
    if (order == 'yes') {
        orderTitle = '<div class="rightHalfTitle">Invoice</div> ';
    }

    var companyAddress = getCompanyAddress(quote);
    var quoteTitleInfo = buildQuoteTitleInfo(quote);
    var shipping = '<div class="projectHalfTitle">Shipping Information</div>';

    var siteTerms = '<div class="manifestTerms">By signing, you agree to all ' + siteDefaults.companyName + ' Terms & Conditions. ' + siteDefaults.termsURL + '  </div>';

    if (isUK()) {
        shipping = '<div class="projectHalfTitle">Delivery Information</div>';
        siteTerms = '<div class="ukSiteTerms"><b>By signing, you agree to all ' + siteDefaults.companyName + ' Terms & Conditions. ' + siteDefaults.termsURL + '</b></div>';
    }

    var dealerLogo = '<img id="printLogo" src="' + siteDefaults.printLogoURL + '">';

    var salesRep = '';
    if (siteDefaults.salesFlow == 1) {
        salesRep = '<div class="leftProject">Sales Rep:</div><div class="rightProject">' + returnBlankIfEmpty(quote.SalesPersonNameOnQuote) + '</div>'
    }

    customer.shipping = shipping;


    var billingInfoHTML = quoteBillingInfo(customer, quote.address);
    var shippingInfoHTML = quoteShippingInfo(customer, quote.address);

    var pickupDeliveryHTML = quote.pickupDelivery;
    var comments = quote.productionNotes;
    var print = getParameter('print');
    var saved = '';
    var companyName = returnBlankIfEmpty(customer.companyName);
    var updateCustomer = '';

    if (empty(comments) && isUK()) {
        comments = "Quote is valid for 30 days from quote date";
    }

    var commentsEditTmpl = '' +
        '<textarea onkeyup="quoteCommentKeyUp()"  id="productionDetailNotes88" class="quoteDetailNotesFlow3" ' +
        'placeholder=""  name="productionNotes" >' + comments + '</textarea>';


    if (isAudit()) {
        commentsEditTmpl = '' +
            '<textarea onkeyup="quoteCommentKeyUp()"  id="productionDetailNotes88" class="quoteDetailNotesFlow3" ' +
            'placeholder=""  name="productionNotes" disabled>' + comments + '</textarea>';

    }



    if ((siteDefaults.salesFlow == 3) && print != 'yes') {
        pickupDeliveryHTML = buildPickupDeliveryPullDown();
        comments = commentsEditTmpl;

        // name = '<input id="lastName" class="fieldUpdate" type="text"  value="' + customer.lastName + '" />';
        // companyName = '<input id="companyName" class="fieldUpdate" type="text"  value="' + customer.companyName + '" />';

        updateCustomer  = changeCustomerHTML();
    }

    if (siteDefaults.salesFlow == 1 && print != 'yes') {
        pickupDeliveryHTML = buildPickupDeliveryPullDown();
        comments = commentsEditTmpl;

        // name = '<input id="lastName" class="fieldUpdate" type="text"  value="' + customer.lastName + '" />';
        // companyName = '<input id="companyName" class="fieldUpdate" type="text"  value="' + customer.companyName + '" />';
    }


    var deliveryAndNotes = '' +
        '           <div class="leftProject">Delivery/Pickup:</div><div class="rightProject">' + pickupDeliveryHTML + '</div>' +
        '           <div class="projectSpacer"></div>' +
        '           <div class="projectCommentsTitle">Comments:</div>' +
        '           <div class="projectComments">' + comments + '</div>' ;
    var purchaseCert = getParameter('purchaseCert');

    var mainTitle = 'Project Information';
    if (purchaseCert == 'yes') {
        deliveryAndNotes = '';
        mainTitle = 'Certificate of Purchase';
    }

    if (isUKGroup()) {
        updateCustomer = '';
        deliveryAndNotes = '' +
            '           <div class="leftProject">Delivery/Pickup:</div><div class="rightProject">' + pickupDeliveryHTML + '</div>' +
            '           <div class="projectSpacer"></div>' +
            '           <div class="projectCommentsTitle">Notes:</div>' +
            '           <div class="projectComments">' + comments + '</div>' ;
        siteTerms = '<div class="manifestTerms"> All orders per ' + siteDefaults.companyName + ' Terms & Conditions. ' + siteDefaults.termsURL + '  </div>';

    }

    var html = '' +
        '<div class="fullPage">' +
        '<div class="quoteOverView keepTogether ">' +
        '   <div class="cartRowHeader"></div>' +
        '   <div class="cartRowHeaderLeft">' + dealerLogo + '</div>' +
        '       <div class="companyInfoTitle">' + siteDefaults.companyName + '</div>' +
        companyAddress +
        '   <div class="quoteOverViewBox">' +
        '       <div class="cartRowHeader">' +
        '           <div class="leftHalfTitle">'+ mainTitle +'</div>' +
        '           ' + orderTitle +
        '       </div>' +
        '       <div class="leftProjectBox">' +
        '           <div class="leftProject">Customer:</div><div class="rightProject">' + name + '</div>' +
        '           <div class="leftProject">Company:</div><div class="rightProject">' + companyName + '</div>' +
        '           ' + updateCustomer +
        '           <div class="projectSpacer"></div>' +
        '           ' + salesRep +
        '           ' + deliveryAndNotes +
        '       </div>' +
        '       <div class="rightProjectBox"> ' +
        '           ' + quoteTitleInfo +
        '       </div> ' +
        '   </div>' +
        '   <div class="quoteOverViewBoxNoPadding">' +
        '       ' + billingInfoHTML +
        '       <div class="projectHalfVerticalLine"></div>' +
        '       ' + shippingInfoHTML +
        '   </div>';

    var endBox = productInformation(siteTerms);

    if (purchaseCert == 'yes') {
        endBox =  '<div class="projectSpacerThin"></div>';
    }

    var manifest = getParameter('manifest');
    if (manifest == 'yes') {
        endBox = itemsShipped(siteTerms);
    }

    return html + endBox;

}


function quoteShippingInfo(customer, address) {

    var jobAddress2 = '';
    if (address.shipping_address2) {
        jobAddress2 = '           <div class="leftAddress"></div><div class="rightAddress">' + returnBlankIfEmpty(address.shipping_address2) + '</div>';
    }

    var shippingInfoHTML = '' +
        '<div class="projectHalf">' +
        '           <div class="leftAddress">Contact:</div><div class="rightAddress">' + returnBlankIfEmpty(address.shipping_contact)+ '</div>' +
        '           <div class="leftAddress">Address:</div><div class="rightAddress">' + returnBlankIfEmpty(address.shipping_address1) + '</div>' +
        jobAddress2 +
        '           <div class="leftAddress"> </div><div class="rightAddress">' + returnBlankIfEmpty(address.shipping_city) + ', ' + returnBlankIfEmpty(correctState(address.shipping_state)) + ' ' + returnBlankIfEmpty(address.shipping_zip) + '</div>' +

        '           <div class="projectSpacer"></div>' +
        '           <div class="leftAddress">Phone:</div><div class="rightAddress">' + returnBlankIfEmpty(address.shipping_phone) + '</div>' +
        '           <div class="leftAddress">Email:</div><div class="rightAddress">' + returnBlankIfEmpty(address.shipping_email) + '</div>' +
        '</div>';

    // if (isUK()) {
    //     shippingInfoHTML = '' +
    //         '<div class="projectHalf">' +
    //         '           <div class="leftAddress">Contact:</div><div class="rightAddress">' + customer.shippingName + '</div>' +
    //         '           <div class="leftAddress">Address:</div><div class="rightAddress">' + customer.jobAddress1 + '</div>' +
    //         jobAddress2 +
    //         '           <div class="leftAddress"></div><div class="rightAddress">' + customer.jobCity + '</div>' +
    //         '           <div class="leftAddress"></div><div class="rightAddress">' + correctState(customer.jobState) + '</div>' +
    //         '           <div class="leftAddress"></div><div class="rightAddress">' + customer.jobZip + '</div>' +
    //         '           <div class="projectSpacer"></div>' +
    //         '           <div class="leftAddress">Phone:</div><div class="rightAddress">' + customer.jobPhone + '</div>' +
    //         '           <div class="leftAddress">Email:</div><div class="rightAddress">' + customer.shippingEmail + '</div>' +
    //         '</div>';
    //
    // }

    var print = getParameter('print');
    // if (print != 'yes' && siteDefaults.salesFlow == '3') {
    //     shippingInfoHTML = quoteShippingInfoEditable(customer);
    // }
    if (print != 'yes' && siteDefaults.salesFlow == '1' || print != 'yes' && siteDefaults.salesFlow == '3') {
        shippingInfoHTML = quoteShippingInfoEditableV3(quote.address);
    }


    return shippingInfoHTML;
}

function createEmptyAddress() {

    var address = {};

    address.billing_contact = '';
    address.billing_address1 = '';
    address.billing_address2 = '';
    address.billing_city = '';
    address.billing_state = '';
    address.billing_zip = '';
    address.billing_email = '';
    address.billing_phone = '';

    address.shipping_contact = '';
    address.shipping_address1 = '';
    address.shipping_address2 = '';
    address.shipping_city = '';
    address.shipping_state = '';
    address.shipping_zip = '';
    address.shipping_email = '';
    address.shipping_phone = '';

    return address;
}

function quoteShippingInfoEditableV3(address) {

    if (empty(address)) {
        address = createEmptyAddress();
    }

    var shippingInfoHTML = '' +
        '<div class="projectHalf">' +
        '   <div class="leftAddress">Contact:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_contact" class="fieldUpdate" type="text"  value="' + returnBlankIfEmpty(address.shipping_contact) + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Address:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_address1" class="fieldUpdate" type="text" placeholder="Address1" value="' + returnBlankIfEmpty(address.shipping_address1) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"></div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_address2" class="fieldUpdate" type="text" placeholder="Address2" value="' + returnBlankIfEmpty(address.shipping_address2) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_city" class="fieldUpdate" type="text" placeholder="' + city + '" value="' + returnBlankIfEmpty(address.shipping_city) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_state" class="fieldUpdate" type="text" placeholder="' + state + '" value="' + returnBlankIfEmpty(correctState(address.shipping_state)) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_zip" class="fieldUpdate" type="text" placeholder="' + zipText + '" value="' + returnBlankIfEmpty(address.shipping_zip) + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '   <div class="leftAddress">Phone:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_phone" class="fieldUpdate" type="text"  value="' + returnBlankIfEmpty(address.shipping_phone) + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Email:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="shipping_email" class="fieldUpdate" type="text"  value="' + returnBlankIfEmpty(address.shipping_email) + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '</div>';

    return shippingInfoHTML;
}

function quoteShippingInfoEditable(customer) {

    var shippingInfoHTML = '' +
        '<div class="projectHalf">' +
        '   <div class="leftAddress">Contact:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="shippingName" class="fieldUpdate" type="text"  value="' + customer.shippingName + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Address:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="jobAddress1" class="fieldUpdate" type="text" placeholder="Address1" value="' + customer.jobAddress1 + '" />' +
        '       </div>' +
        '   <div class="leftAddress"></div>' +
        '       <div class="rightAddress">' +
        '           <input id="jobAddress2" class="fieldUpdate" type="text" placeholder="Address2" value="' + customer.jobAddress2 + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="jobCity" class="fieldUpdate" type="text" placeholder="' + city + '" value="' + customer.jobCity + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="jobState" class="fieldUpdate" type="text" placeholder="' + state + '" value="' + correctState(customer.jobState) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="jobZip" class="fieldUpdate" type="text" placeholder="' + zipText + '" value="' + customer.jobZip + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '   <div class="leftAddress">Phone:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="jobPhone" class="fieldUpdate" type="text"  value="' + customer.jobPhone + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Email:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="shippingEmail" class="fieldUpdate" type="text"  value="' + customer.shippingEmail + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '</div>';

    return shippingInfoHTML;
}

function correctState(state) {

    if (state == 'Select State') {
        state = '';
    }

    return state;
}


function quoteBillingInfo(customer, address) {

    var billingAddress2 = '';

    if (empty(address)) {
        address = createEmptyAddress();
    }

    if (!empty(address.billing_address2)) {
        billingAddress2 = '<div class="leftAddress"></div><div class="rightAddress">' + address.billing_address2 + '</div>';
    }

    var shippingTitle = '<div class="projectHalfTitle">Shipping Information</div>';
    if (isUK()) {
        shippingTitle = '<div class="projectHalfTitle">Delivery Information</div>'
    }

    var billingInfoHTML = '' +
        '       <div class="cartRowHeader"> ' +
        '           <div class="projectHalfTitle">Billing Information</div>' +
        '           ' + shippingTitle +
        '       </div> ' +
        '      <div class="projectHalf">' +
        '           <div class="leftAddress">Contact:</div><div class="rightAddress">' + address.billing_contact + '</div>' +
        '           <div class="leftAddress">Address:</div><div class="rightAddress">' + address.billing_address1 + '</div>' +
        billingAddress2 +
        '           <div class="leftAddress"> </div><div class="rightAddress">' + address.billing_city + ', ' + correctState(address.billing_state) + ' ' + address.billing_zip + '</div>' +
        '           <div class="projectSpacer"></div>' +
        '           <div class="leftAddress">Phone:</div><div class="rightAddress">' + address.billing_phone + '</div>' +
        '           <div class="leftAddress">Email:</div><div class="rightAddress">' + address.billing_email + '</div>' +
        '      </div>';

    // if (isUK()) {
    //
    //     billingInfoHTML = '' +
    //         '       <div class="cartRowHeader"> ' +
    //         '           <div class="projectHalfTitle">Billing Information </div>' +
    //         '           ' + customer.shipping +
    //         '       </div> ' +
    //         '      <div class="projectHalf">' +
    //         '           <div class="leftAddress">Contact:</div><div class="rightAddress">' + customer.billingName + '</div>' +
    //         '           <div class="leftAddress">Address:</div><div class="rightAddress">' + customer.billingAddress1 + '</div>' +
    //         billingAddress2 +
    //         '           <div class="leftAddress"></div><div class="rightAddress">' + customer.billingCity + '</div>' +
    //         '           <div class="leftAddress"></div><div class="rightAddress">' + correctState(customer.billingState) + '</div>' +
    //         '           <div class="leftAddress"></div><div class="rightAddress">' + customer.billingZip + '</div>' +
    //         '           <div class="projectSpacer"></div>' +
    //         '           <div class="leftAddress">Phone:</div><div class="rightAddress">' + customer.phone + '</div>' +
    //         '           <div class="leftAddress">Email:</div><div class="rightAddress">' + customer.billingEmail + '</div>' +
    //         '      </div>';
    //
    // }

    var print = getParameter('print');
    // if (print != 'yes' && siteDefaults.salesFlow == '3') {
    //     billingInfoHTML = quoteBillingInfoEditable(customer);
    // }
    if (print != 'yes' && siteDefaults.salesFlow == '1' || print != 'yes' && siteDefaults.salesFlow == '3') {
        billingInfoHTML = quoteBillingInfoEditableAddress(quote.address);
    }

    return billingInfoHTML;
}


function quoteBillingInfoEditableAddress(address) {


    var shippingTitle = '<div class="projectHalfTitle">Shipping Information</div>';
    if (isUK()) {
        shippingTitle = '<div class="projectHalfTitle">Delivery Information</div>'
    }

    var billingInfoHTML = '' +
        ' <div class="cartRowHeader"> ' +
        '     <div class="projectHalfTitle">Billing Information </div>' +
        '     ' + shippingTitle +
        ' </div> ' +
        '<div class="projectHalf">' +
        '   <div class="leftAddress">Contact:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_contact" class="fieldUpdate" type="text"  value="' + returnBlankIfEmpty(address.billing_contact) + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Address:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_address1" class="fieldUpdate" type="text" placeholder="Address1" value="' + returnBlankIfEmpty(address.billing_address1) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"></div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_address2" class="fieldUpdate" type="text" placeholder="Address2" value="' + returnBlankIfEmpty(address.billing_address2) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_city" class="fieldUpdate" type="text" placeholder="' + city + '" value="' + returnBlankIfEmpty(address.billing_city) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_state" class="fieldUpdate" type="text" placeholder="' + state + '" value="' + returnBlankIfEmpty(correctState(address.billing_state)) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_zip" class="fieldUpdate" type="text" placeholder="' + zipText + '" value="' + returnBlankIfEmpty(address.billing_zip) + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '   <div class="leftAddress">Phone:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_phone" class="fieldUpdate" type="text"  value="' + returnBlankIfEmpty(address.billing_phone) + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Email:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billing_email" class="fieldUpdate" type="text"  value="' + returnBlankIfEmpty(address.billing_email) + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '</div>';

    return billingInfoHTML;
}


function quoteBillingInfoEditable(customer) {

    var billingInfoHTML = '' +
        ' <div class="cartRowHeader"> ' +
        '     <div class="projectHalfTitle">Billing Information </div>' +
        '     ' + customer.shipping +
        ' </div> ' +
        '<div class="projectHalf">' +
        '   <div class="leftAddress">Contact:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingName" class="fieldUpdate" type="text"  value="' + customer.billingName + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Address:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingAddress1" class="fieldUpdate" type="text" placeholder="Address1" value="' + customer.billingAddress1 + '" />' +
        '       </div>' +
        '   <div class="leftAddress"></div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingAddress2" class="fieldUpdate" type="text" placeholder="Address2" value="' + customer.billingAddress2 + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingCity" class="fieldUpdate" type="text" placeholder="' + city + '" value="' + customer.billingCity + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingState" class="fieldUpdate" type="text" placeholder="' + state + '" value="' + correctState(customer.billingState) + '" />' +
        '       </div>' +
        '   <div class="leftAddress"> </div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingZip" class="fieldUpdate" type="text" placeholder="' + zipText + '" value="' + customer.billingZip + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '   <div class="leftAddress">Phone:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="phone" class="fieldUpdate" type="text"  value="' + customer.phone + '" />' +
        '       </div>' +
        '   <div class="leftAddress">Email:</div>' +
        '       <div class="rightAddress">' +
        '           <input id="billingEmail" class="fieldUpdate" type="text"  value="' + customer.billingEmail + '" />' +
        '       </div>' +
        '     <div class="projectSpacer"></div>' +
        '</div>';

    return billingInfoHTML;
}

function buildCartStats(cart) {

    var totalItemsCart = 0;
    var panels = 0;
    var swings = 0;
    var totalWindows = 0;

    for (var property in cart) {
        var row = cart[property];

        row.quantity = cleanQuantityNumber(row.quantity);
        panels = panels + (row.numberOfPanels * row.quantity);
        swings = swings + (getNumberOfSwings(row) * row.quantity);
        totalItemsCart = totalItemsCart + row.quantity;

        if (!empty(row.windowsOnly)) {
            totalWindows = totalWindows + row.quantity;
        }
    }

    var totalDoors = totalItemsCart - totalWindows;

    var stats = {
        "panels": panels, "items": totalItemsCart, "topRails": panels * 2,
        "bottomRails": panels * 2, "screens": panels, "handleSets": swings, "totalDoors":totalDoors,
        "totalWindows": totalWindows
    };
    return stats;

}


function itemsShipped(siteTerms) {

    var itemTotalHTML = buildItemTotalForPrint();
    var discountsHTML = buildDiscountForPrint(quote.SalesDiscount);

    var discountTotalHTML = buildDiscountTotalForPrint();
    var subTotalHTML = buildSubTotalForPrint();
    var screensHTML = buildScreensForPrint();

    var taxesHTML = buildTaxesForPrint();
    var shippingHTML = buildShippingForPrint();
    var installHTML = buildInstallForPrint();
    var extraHTML = buildExtraForPrint();
    var grandTotalHTML = buildGrandTotalForPrint();
    var depositHTML = buildDepositForPrint();

    var cartStats = buildCartStats(quote.Cart);

    var terms = '<div class="manifestTerms"> IMPORTANT: By signing this form, you confirm that you have inspected the delivered items to confirm the delivery matches the item number above. ' +
        'You also confirm that all items have been recieved undamaged and in proper condition unless otherwise noted in the "Notes" section above. If any item is' +
        ' damaged or missing, please list those items in the "Notes" section before signing this form. </div>';

    var html = '' +
        '       <div class="cartRowHeader">Items Shipped</div>' +
        '       <div class="projectHalf">' +
        '           <div class="leftProjectLong">Total Number of Items:</div><div class="rightProjectShorter">' + cartStats.items + '</div>' +
        '           <div class="leftProjectLong">Door Systems:</div><div class="rightProjectShorter">' + cartStats.totalDoors + '</div>' +
        '           <div class="leftProjectLong">Window Systems:</div><div class="rightProjectShorter">' + cartStats.totalWindows + '</div>' +
        '           <div class="projectSpacer"></div>' +
        '           <div class="full"><div class="leftProjectLong">Shipping Contents</div><div class="rightMiddle"></div> <div class="rightEnd">Customer Initials</div></div>' +
        '           <div class="full"><div class="leftProjectLongLight">Top / Bottom Tracks</div><div class="rightMiddle">' + cartStats.items + '</div> <div class="rightEnd">______</div></div>' +
        '           <div class="full"><div class="leftProjectLongLight">Jamb Sets</div><div class="rightMiddle">' + cartStats.items + '</div> <div class="rightEnd"><b>______</b></div></div>' +
        '           <div class="full"><div class="leftProjectLongLight">Number of Panels</div><div class="rightMiddle">' + cartStats.panels + '</div> <div class="rightEnd">______</div></div>' +
        '           <div class="full"><div class="leftProjectLongLight">Number of Handle Sets</div><div class="rightMiddle">' + cartStats.handleSets + '</div> <div class="rightEnd">______</div></div>' +
        '           <div class="full"><div class="leftProjectLongLight">Number of Screens</div><div class="rightMiddle"></div> <div class="rightEnd">______</div></div>' +
        '       </div>' +
        '       <div class="projectHalf">' +
        '           <textarea class="manifestNotes" placeholder="" name="notes"></textarea>' +
        '       </div>' +
        '           ' + terms + siteTerms +
        '       <div class="projectHalf">Received by: (Print Name) _________________________________ </div>' +
        '       <div class="projectHalf">Delivered by: (Print Name) ________________________________ </div>' +
        '       <div class="projectHalf">Signature: _________________________________________________ </div>' +
        '       <div class="projectHalf">Date: ___________ Time: ___________ </div>';

    return html;

}

function ukPriceTerms() {

    var salesTerms = '';
    if (!empty(quote.salesTerms)) {
        salesTerms = quote.salesTerms;
    }
    var html = '';
    if (siteDefaults.salesFlow == 3) {
        if (empty(salesTerms)) {
            salesTerms = '50% on order\n50% 1 Week prior to installation';
        }
        if (isUKGroup()) {
            salesTerms = '';
        }
        html = '' +
            '<div class="projectSpacer"></div>' +
            '<div class="UkTermsLeft">Standard Terms:</div>' +
            '<div class="UkTermsRight">' +
            '     <textarea id="salesTerms" class="textAreaFieldUpdate" >' + salesTerms + '</textarea> ' +
            '</div>';
    }

    return html;
}


function productInformation(siteTerms) {


    var terms = '<div class="projectComments">IMPORTANT: A signed cover sheet (this page), a signed item description page for each item ordered, as well along with a deposit' +
        ' of 50% are required for the items to be put into production. Any delay in ' + siteDefaults.companyName + ' receiving any of the above mentioned items will result in a ' +
        'delay to the delivery of the items.</div>';


    if (siteDefaults.salesFlow == 2) {
        terms = '<div class="projectComments">IMPORTANT: A signed cover sheet (this page), a signed item description page for each item ordered, as well as a full' +
            ' payment are required for the items to be put into production. Any delay in ' + siteDefaults.companyName + ' receiving any of the above mentioned items will result in a ' +
            'delay to the delivery of the items.</div>'
    }

    if (siteDefaults.salesFlow == 3) {
        terms = '';
    }

    var itemTotalHTML = buildItemTotalForPrint();
    var discountsHTML = buildDiscountForPrint(quote.SalesDiscount);


    var discountTotalHTML = buildDiscountTotalForPrint();
    var subTotalHTML = buildSubTotalForPrint();
    var screensHTML = buildScreensForPrint();

    var taxesHTML = buildTaxesForPrint();

    var shippingHTML = buildShippingForPrint();
    var installHTML = buildInstallForPrint();
    var extraHTML = buildExtraForPrint();
    var additionalExtraHTML = buildAdditionalExtraForPrint();
    var grandTotalHTML = buildGrandTotalForPrint();
    var depositHTML = buildDepositForPrint();
    var totalItems2 = getTotalItemsInCart(quote.Cart);
    var paymentHTML = buildPaymentForPrint();

    var ukPriceTermsHTML = ukPriceTerms();

    var salesRepLine = '';

    if (siteDefaults.salesFlow == 1) {
        salesRepLine = '<div class="leftProjectLongShorter font14">SALES REP SIGNATURE:</div><div class="rightProjectShort">______________________________________</div>';
    }

    var usTaxHTML = taxesHTML;
    var ukTaxHTML = '';

    if (isUK()) {
        usTaxHTML = '';
        ukTaxHTML = taxesHTML;
    }

    var launchAdmin = '';
    var launchAdminCss = ''
    if (siteDefaults.salesFlow == '3') {
        launchAdmin = 'onclick="launchSalesAdmin()" ';
        launchAdminCss = 'flow3SalesAdminButton';
    }


    var cartStats = buildCartStats(quote.Cart);

    var customerSignature = '' +
        '       <div class="projectHalf">' +
        '           <div class="customerSignatureLeft font14">CUSTOMER SIGNATURE:</div><div class="customerSignatureRight">___________________________________</div>' +
        '           ' + salesRepLine +
        '           <div class="customerSignatureLeft font14">DATE:</div><div class="customerSignatureRight">___________________________________</div>' +
        '       </div>' +
        '       <div class="projectHalf">' +
        '           <div class="leftProjectLongShorter font14">PAYMENT TYPE:</div><div class="rightProjectShort">___________________________________</div>' +
        '           <div class="leftProjectLongShorter font14">PAYMENT AMOUNT:</div><div class="rightProjectShort">___________________________________</div>' +
        '       </div>' ;

    if (isUKGroup()) {
        customerSignature = '';
    }



    var html = '' +
        '   <div class="itemsOverViewBox" >' +
        '       <div class="cartRowHeader">Project Information</div>' +
        '       <div class="projectHalf">' +
        '           <div class="leftProjectLong">Total Number of Items:</div><div class="rightProjectShorter">' + cartStats.items + '</div>' +
        '           <div class="leftProjectLong">Door Systems:</div><div class="rightProjectShorter">' + cartStats.totalDoors + '</div>' +
        '           <div class="leftProjectLong">Window Systems:</div><div class="rightProjectShorter">' + cartStats.totalWindows + '</div>' +
        '           <div class="termsAndConditionsOnQuote">' +
        '               ' + terms +
        '           </div>' +
        '       </div>' +
        '       <div class="projectHalf"  > ' +
        '           <div class="' + launchAdminCss + '" ' + launchAdmin + ' > ' +
        '               ' + itemTotalHTML +
        '               ' + discountsHTML +
        '               ' + discountTotalHTML +
        '               ' + subTotalHTML +
        '               ' + screensHTML +
        '               ' + usTaxHTML +
        '               ' + shippingHTML +
        '               ' + installHTML +
        '               ' + extraHTML +
        '               ' + additionalExtraHTML +
        '               ' + ukTaxHTML +
        '               ' + grandTotalHTML +
        '               ' + depositHTML +
        '               ' + paymentHTML +
        '           </div> ' +
        '           ' + ukPriceTermsHTML +
        '       </div> ' +
        '           <div class="projectSpacer"></div>' +
        '           ' + customerSignature +
        '           ' + siteTerms +
        '   </div>' +
        '</div>' +
        '</div></div>' +
        '<div class="projectSpacer"></div>';

    return html;
}


function deleteQuote(quoteID) {

    var x;

    stopSpinner();
    if (confirm("Are you sure you want to delete this quote?") == true) {
        startSpinner();
        var quote = getParameter('quote');
        $.ajax({
            url: "https://" + apiHostV1 + "/api/quotes/" + quoteID + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
            type: "DELETE",
            success: function (response) {

//                deletePipelinerAPI(quoteID).done(function(){
//                });

                if (tab == 'viewQuote') {
                    tab = 'viewQuotes';
                }
                trackAndDoClick(tab);

            }
        });
    }
}


function viewQuote(quoteID) {

    trackAndDoClick('viewQuote', '&quote=' + quoteID);
}


function quoteActions() {

    startSpinner();

    var thisSelector = $(this);
    var id = $(thisSelector).attr('id');

    var quoteID = splitForFirstOption(id);
    var action = splitForSecondOption(id);

    if (action === 'delete' || action === 'deleteQuote') {
        deleteQuote(quoteID);
    }
    if (action === 'printGlassLabels') {
        startSpinner();
        printGlassLabelsForProduction(quote.id);
    }
    if (action === 'deleteOrder') {
        deleteOrder(quoteID);
    }
    if (action === 'view' || action == 'viewLink') {
        viewQuote(quoteID);
    }
    if (action === 'printCutSheetSales') {
        startSpinner();
        printCutSheetForSales(quote.id);
    }
    if (action === 'printCutSheet') {
        startSpinner();
        printCutSheetForProduction(quote.id);
    }
    if (action === 'printGlassSheet') {
        startSpinner();
        printGlassSheetForProduction(quote.id);
    }
    if (action === 'deleteJob') {
        deleteOrderProduction(id);
    }
    if (action === 'convertToSale') {

        if (isLimitedAccount()) {
            limitedAccessAccount();
            stopSpinner();
            return;
        }
        if (siteDefaults.salesFlow == 1 || siteDefaults.salesFlow == 3) {
            var due = cleanDate(quote.dueDate);

            var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
            if (!due.match(re)) {
                stopSpinner();
                alert("Date Due must be set before creating an order");
                return;
            }
        }

        if (quote.formComplete == 0) {
            stopSpinner();
            alert("Items in your cart are not complete, please complete them before continuing");
        } else {
            stopSpinner();
            if (confirm("Are you sure you want to convert this quote to an order?") == true) {
                startSpinner();
                trackAndDoClick("convertToSale");
            }
        }
    }
    if (action === 'emailQuote' || action === 'mail') {
        trackAndDoClick("viewQuote", "&quote=" + quoteID + "&edit=email");
    }
    if (action === 'emailOrder') {
        trackAndDoClick("viewOrder", "&order=" + quoteID + "&edit=email");
    }
    if (action === 'printQuote') {
        stopSpinner();
        printQuote(quoteID);
    }
    if (action === 'saveToQuote') {

        var checkTab = tab;
        var secondTab = getParameter('secondTab');
        if (!empty(secondTab)) {
            checkTab = secondTab;
        }

        if (!validatePageOptions(checkTab)) {
            return;
        }

        if (isLimitedAccount()) {
            limitedAccessAccount();
            return;
        }

        if(tab == 'viewOrder'){
            getContractID(quoteID).done(function(response){
                if(!response.id){
                    saveToQuote(quoteID);
                    stopSpinner();
                    return;
                }
                if (isUK()) {
                    saveToQuote(quoteID);
                    stopSpinner();
                    return;
                }

                if(confirm('Saving changes to this order will results in your uploaded contract to be deleted')){
                    deleteContractFile(quoteID).done(function(response){
                        saveToQuote(quoteID);
                    });
                }else{
                    stopSpinner();
                }
            });
        }else{
            saveToQuote(quoteID);
        }
    }
    if (action === 'printOrder' || action === 'printJob' || action === 'printOrderCert') {
        doOrderPrinting(quoteID, action);
    }
    if (action === 'emailOrder') {
        trackAndDoClick("viewOrder", "&order=" + quoteID + "&edit=email");
    }
    if (action === 'cancelQuoteChanges') {
        cancelQuoteChanges(quoteID);
    }
    if (action === 'printInvoice') {
        stopSpinner();
        getInvoiceToPrint(quoteID, site);
    }
    if (action === 'printCollection') {
        stopSpinner();
        getInvoiceToPrintForProduction(quoteID, site);
    }
    if (action === 'printInvoiceForQuote') {
        stopSpinner();
        getQuoteToPrint(quoteID, site);
    }
    if (action === 'closeJob') {
        if (empty(lastTab)) {
            lastTab = 'viewPreProduction'
        }
        trackAndDoClick(lastTab);
    }
    if (action === 'cancelOrderChanges') {

        cancelOrderChanges(quoteID);
    }
    if (action === 'closeOrder') {
        trackAndDoClick("viewOrders");
    }
    if (action === 'closeAccounting') {
        var secondTab = getParameter('secondTab');
        if (empty(secondTab)) {
            secondTab = 'confirmOrders';
        }
        trackAndDoClick(secondTab);
    }
    if (action === 'revertToQuote') {
        stopSpinner();
        if (!empty(quote.SalesDiscount.Payments)) {
            alert("Orders with attached payments can not be reverted");
            return;
        }

        if (confirm("Are you sure you want to revert this order to a quote?") == true) {
            startSpinner();
            apiRevertToQuote().done(function (result) {
                stopSpinner()
                var response = jQuery.parseJSON(result);
                alert("This Order has been reverted to a quote");

                var secondTab = getParameter('secondTab');
                if (empty(secondTab)) {
                    secondTab = 'confirmOrders';
                }
                trackAndDoClick(secondTab);

            });
        }

    }

    if (action === 'closeQuote') {
        trackAndDoClick("viewQuotes");
    }
    if (action === 'addDoor') {
        trackAndDoClick("viewQuote", "&quote=" + quoteID + "&edit=item&cartItem=formStatus&secondTab=addDoor");
    }
    if (action === 'manifest') {
        doManifestPrinting(quoteID);
    }
}


function printQuote(quoteID) {

    var debug = getParameter('debug');

    var url = "https://" + apiHostV1 + "/api/quotePrint/" + quoteID + "?" + authorizePlusSalesPerson() + '&site=' + site + '&debug=' + debug + addSuperToken();

    var newWin = window.open(url);
    if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
        alert("Popup window was blocked, turn off your popup blocker to allow for printing.");
    }
}


function doOrderPrinting(quoteID, action) {

    var prod = getParameter('tab');
    var debug = getParameter('debug');


    stopSpinner();
    var url = "https://" + apiHostV1 + "/api/quotePrint/" + quoteID + "?" + authorizePlusSalesPerson() + '&order=yes' + addSite() + '&debug=' + debug + addSuperToken();
    if (prod == 'viewProductionOrder') {
        url = "https://" + apiHostV1 + "/api/quotePrint/" + quoteID + "?" + authorizePlusProduction() + '&order=yes&production=yes' + addSite() + '&debug=' + debug + addSuperToken();
    }
    if (action == 'printOrderCert') {
        url = url + "&purchaseCert=yes";
    }

    var newWin = window.open(url);
    if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
        alert("Popup window was blocked, turn off your popup blocker to allow for printing.");
    }
}


function doManifestPrinting(quoteID) {

    var prod = getParameter('tab');
    var debug = getParameter('debug');

    var url = "https://" + apiHostV1 + "/api/quotePrint/" + quoteID + "?" + authorizePlusProduction() + '&order=yes&production=yes&manifest=yes' + addSite() + '&debug=' + debug;

    var newWin = window.open(url);
    stopSpinner();
    if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
        alert("Popup window was blocked, turn off your popup blocker to allow for printing.");
    }
}


function cancelQuoteChanges(quoteID) {

    if (tab == 'viewAccountingOrder') {

        trackAndDoClick('viewAccountingOrder', '&order=' + quoteID);

    } else if (tab == 'viewOrder') {

        trackAndDoClick('viewOrder', '&order=' + quoteID);

    } else if (tab == 'viewQuote') {

        trackAndDoClick('viewQuote', '&quote=' + quoteID);

    } else if (tab == 'viewProductionOrder') {

        trackAndDoClick('viewProductionOrder', '&order=' + quoteID);

    } else {
        alert("Cancel For that screen not defined yet");
    }
}


function cancelOrderChanges(orderID) {

    trackAndDoClick('viewOrder', '&order=' + orderID);
}


function getRowColor(quote, alt) {

    var className = '';

    if (quote.leadQuality == 'Hot') {
        className = 'hot' + alt;
    } else if (quote.leadQuality == 'Warm') {
        className = 'medium' + alt;
    } else if (quote.leadQuality == 'New') {
        className = 'newQuote' + alt;
    }
    else if (quote.leadQuality == 'Cold') {
        className = 'cold' + alt;
    } else if (quote.leadQuality == 'Hold') {
        className = 'hold' + alt;
    } else if (quote.leadQuality == 'Archived') {
        className = 'archived' + alt;
    }

    return className;
}


function buildViewQuotesSalesGuestRow(quote, count) {

    var prefix = 'SQ'

    var print = getParameter('print');
    if (print == 'yes') {
        prefix = getParameter('prefix');
    } else {
        if (isSalesPerson()) {
            prefix = user.prefix;
        }
    }

    var alt = buildAlt(count);

    var companyName = createTableSpaceOnEmpty(quote.companyName);
    var phone = createTableSpaceOnEmpty(quote.phone);

    var lastName = createTableSpaceOnEmpty(quote.lastName);
    var firstName = createTableSpaceOnEmpty(quote.firstName);
    var zip = createTableSpaceOnEmpty(quote.zip);
    var columnColor = getRowColor(quote, '');

    var quoteTitle = makeQuoteTitle(quote);

    var leadStatus = buildQuoteLeadStatus(quote);
    var leadType = createTableSpaceOnEmpty(quote.leadType);

    var notComplete = '*'
    if (quote.formComplete == 1) {
        notComplete = '';
    }

    var leadHTML = '<div class="quoteCell2 borderLeftTop" >' + leadStatus + '</div>';
    var leadTypeHTML = '<div class="quoteCell2 borderLeftTop " >' + leadType + '</div>';
    var followUpHTML = '' +
        '<div class="quoteCell8 borderLeftTop" >' +
        '       <input id="followUp-' + count + '"  onChange="updateQuoteFollowUps(' + count + ',' + quote.id + ')" class="quoteDateBoxList" type="text"  value = "' + quote.followUp + '"/>' +
        '   </div>' +
        '   <div class="quoteCell9 borderLeftTop" >' +
        '       <input id="secondFollowUp-' + count + '"  onChange="updateQuoteFollowUps(' + count + ',' + quote.id + ')" class="quoteDateBoxList" type="text"  value = "' + quote.secondFollowUp + '"/>' +
        '   </div>';

    var printQuoteHTML = '<img id="' + quote.id + '-printQuote" class="quoteIcons quoteActions" src="/images/quotes/icon-print-121.svg">';
    var emailQuoteHTML = '<img id="' + quote.id + '-mail" class="quoteIcons quoteActions" src="/images/quotes/icon-mail-121.svg">';

    var extendWidth = '';
    var quotedHTML = '';

    if (siteDefaults.salesFlow == 2) {
        var quoted = cleanDate(quote.quoted);
        leadTypeHTML = '';
        leadHTML = '';
        followUpHTML = '';
        extendWidth = ' rowExtendFlow2 ';
        columnColor = '';
        printQuoteHTML = '<img id="' + quote.id + '-printInvoiceForQuote" class="quoteIcons quoteActions" src="/images/quotes/icon-print-121.svg">';
        quotedHTML = '<div id="quoteDate" class="quoteCell5 borderLeftTop sortable " >'+ quoted +'</div>';

    }

    if (quote.total == "0.00") {
        printQuoteHTML = '';
        emailQuoteHTML = '';
    }

    var topHtml =
        '<div class="quoteRow ' + alt + '">' +
        '   <div id="' + quote.id + '-viewLink" class="quoteCell1 borderLeftTop quoteActions ' + columnColor + '"  >' + notComplete + quoteTitle + '</div>' +
        '   ' + leadHTML +
        '   ' + leadTypeHTML +
        '   <div class="quoteLastName borderLeftTop ' + extendWidth + '" >' + lastName + '</div>' +
        '   <div class="quoteFirstName borderLeftTop ' + extendWidth + '"  >' + firstName + '</div>' +
        '   <div class="quoteCompany borderLeftTop ' + extendWidth + '" >' + companyName + '</div>' +
        '   ' + quotedHTML +
        '   <div class="quoteCell5 borderLeftTop" >' + phone + '</div>' +
        '   <div class="quoteZip borderLeftTop" >' + zip + '</div>' +
        '   <div class="quoteCell7 borderLeftTop" >' + currency + quote.total + '</div>' +
        '   ' + followUpHTML +
        '   <div class="quoteCell10 borderLeftRightTop" >' +
        '       <img id="' + quote.id + '-delete" title="Delete" class="quoteIcons quoteActions" src="/images/quotes/icon-delete-121.svg">' +
        '   </div>' +
        '</div>';

    return topHtml;
}


function buildQuoteLeadStatus(quote) {

    var statusLabels = ['New', 'Hot', 'Warm', 'Cold', 'Hold', 'Archived'];
    //var status = ['Ready For Production', 'In Production', 'Completed', 'Delivered', 'Hold'];

    var html = '<select id="pullDown-' + quote.id + '" class="leadStatusPullDown" onchange="changeLeadStatus(' + quote.id + ')">';

    for (var a = 0; a < statusLabels.length; a++) {
        var selected = '';
        if (statusLabels[a] == quote.leadQuality) {
            selected = 'selected';
        }
        html += '<option ' + selected + ' value="' + statusLabels[a] + '">' + statusLabels[a] + '</option>';
    }
    html += '</select>';

    return html;

}


function updateLeadStatusFromRow(status, quoteId) {

    if (!empty(user.pipelinerId)) {
        var lead = {"ownerId":user.pipelinerId, "prefix":user.prefix, "quoteId": quoteId, "quality": status}

//        updatePipelinerAPI(lead).done(function (result) {
//        })
    }
}

function updateLeadStatus(status) {

    if (!empty(user.pipelinerId)) {
        var lead = {"ownerId":user.pipelinerId, "prefix":user.prefix, "quoteId": quote.id, "quality": status}

//        updatePipelinerAPI(lead).done(function (result) {
//        })
    }
}


function changeLeadStatus(id) {

    var status = $('#pullDown-' + id).val();
    var json = JSON.stringify(status);


    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/leadQuality?" + authorizePlusSalesPerson() + addSite() + addSuperToken();

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            updateLeadStatusFromRow(status, id);
            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                if (tab == 'viewQuote') {
                    quote.leadQuality = status;
                    var cssClass = qualityBorderColor(quote);
                    $('#quoteDetailBoxID').removeClass().addClass('quoteDetailsBox ' + cssClass);


                } else {
                    trackAndDoClick(tab);
                }
            } else {
                alert("Failed to Save Wood Choice")
            }
        }
    });


}


function buildTopButtons() {

    var html = '';

    if (siteDefaults.showLeadStatus == 1) {
        html = '' +
            '<div id="viewAll" class="leadActions leadActionsLeft">View All</div>' +
            '<div id="new" class="leadActions newQuote">New</div>' +
            '<div id="hot" class="leadActions hot">HOT</div>' +
            '<div id="medium" class="leadActions medium">Warm</div>' +
            '<div id="cold" class="leadActions cold">Cold</div>' +
            '<div id="hold" class="leadActions hold">Hold</div>' +
            '<div id="archived" class="leadActions archived">Archived</div>';
    }

    return html;
}


function changeLimit() {

    startSpinner();

    var limit = $('#myLimit').val();

    if (limit != 'All') {
        limit = parseInt(limit);
    }

    var search = $('#search').val();

    uiState.limit = limit;
    writeLocalStorageWithKeyAndData("uiState", uiState);

    trackAndDoClick(tab, '&page=1&limit=' + limit + '&search=' + search);
}

function buildSearchFields() {

    var html = '' +
        '<select id="searchFields" class="searchPullDown" >' +
        '   <option value="all">all</option>' +
        '   <option value="name">Name</option>';
    '</select>';

    return html;

}
function buildLimitList() {

    if (empty(uiState.limit)) {
        uiState.limit = 10;
    }
    var html = '' +
        '<select id="myLimit" class="paginationPullDown" onchange="changeLimit()">' +
        '   <option value="--">--</option>';
    for (var a = 10; a < 101; a = a + 10) {

        var selected = '';
        if (a == uiState.limit) {
            selected = 'selected';
        }
        html += '<option  value="' + a + '" ' + selected + '>' + a + ' Items</option>';

    }
    selected = '';
    if (isAccounting()) {
        if (uiState.limit == 'All') {
            selected = 'selected';
        }
        html += '<option  value="All" ' + selected + '>All Items</option>';
    }
    html += '</select>';
    return html;

}


function buildViewQuotesSalesGuestTable(rows, data) {

    var cssColor = uiState.quality;

    var limitPullDown = buildLimitList();
    var pagination = '' +
        '<div id="paginationBox">' +
        '   <div id="limitPullDown" ><div id="limitPullDownText">Show: </div>' + limitPullDown + '</div> ' +
        '   <div id="pagination"></div>' +
        '</div>';


    var leadQuality = '   <div id="leadQuality" class="quoteCell2 borderLeftTop sortable" >Quality</div>';
    var leadType = '   <div id="leadType" class="quoteCell2 borderLeftTop" >Type</div>';
    var quoteDate = '';
    var extendWidth = '';
    if (siteDefaults.salesFlow == 2) {
        leadType = '';
        leadQuality = '';
        extendWidth = ' rowExtendFlow2 ';
        cssColor = '';
        quoteDate = '<div id="quoteDate" class="quoteCell5 borderLeftTop sortable " >Quoted</div>';
    }

    var topHtml =
        '<div id="subTabAlert" class="' + cssColor + '"></div>' +
        '<div class="quoteRowTitle">' +
        '   <div id="id" class="quoteCell1 borderLeftTop sortable" >Quote #</div>' +
        '   ' + leadQuality +
        '   ' + leadType +
        '   <div id="lastName" class="quoteLastName borderLeftTop sortable ' + extendWidth + '" >Last </div>' +
        '   <div id="firstName" class="quoteFirstName borderLeftTop sortable ' + extendWidth + '" >First</div>' +
        '   <div id="companyName" class="quoteCompany borderLeftTop sortable ' + extendWidth + '" >Company</div>' +
            quoteDate +
        '   <div id="phone" class="quoteCell5 borderLeftTop sortable" >Phone</div>';

    if (!empty(data)) {
        if (data[0].type === "guestQuote") {
            topHtml =
                '<div class="quoteRow">' +
                '   <div class="quoteCell1" >Quote #</div>' +
                '   <div class="quoteCell2" class="quoteLeft" >Name</div>' +
                '   <div class="quoteCell3" >' + zipShort + '</div>' +
                '   <div class="quoteCell4" >Phone</div>';
        }
    }

    var followUp = '' +
        '<div id="followUp" class="quoteCell8 borderLeftTop sortable" >Contacted</div>' +
        '<div id="secondFollowUp" class="quoteCell9 borderLeftTop sortable" >Follow Up</div>';
    if (siteDefaults.salesFlow == 2) {
        followUp = '';
    }

    var html =
        topHtml +
        '   <div id="billingZip" class="quoteCell6 borderLeftTop sortable" >' + zipShort + '</div>' +
        '   <div id="total" class="quoteCell7 borderLeftTop sortable" >Amount</div>' +
        '   ' + followUp +
        '   <div class="quoteCell10 borderLeftRightTop" ></div>' +
        '</div>' +
        rows +
        '<div class="finalRow"></div>';

    return pagination + html;
}
function buildAlt(count) {

    var alt = 'Alt';
    if (count % 2 == 0) {
        alt = ''
    }
    return alt;

}

function buildViewQuotes(data) {

    var row;
    var rows = '';

    for (var a = 0; a < data.length; a++) {
        var quote = data[a];
        row = buildViewQuotesSalesGuestRow(quote, a);
        rows = rows + row;
    }

    var html = buildViewQuotesSalesGuestTable(rows, data);

    return html;

}


function buildSavedQuote(quote) {


    var numberItemsInCart = getTotalItemsInCart(quote.Cart);
    var name = '';
    if(!empty(quote.Customer)){
        name = quote.Customer.firstName + ' ' + quote.Customer.lastName;
    }
    var html = '' +
        '<div class="quoteSavedBox">' +
        '   <div class="quoteSavedTitle"> </div>' +
        '   <div class="quoteSavedInfoText">Quote# ' + quote.id + '</div>' +
        '   <div class="quoteSavedInfoText">Quote Name: ' + name + '</div>' +
        '   <div class="quoteSavedStatusBox">Quote Saved</div>' +
        '   <div class="quoteSavedButtonBox">' +
        '       <div id="' + quote.id + '-emailQuote" class="quoteEmailButton quoteButtons" ' + cssBorder + '>Email Quote</div>' +
        '       <div id="' + quote.id + '-view" class="quoteViewButton quoteButtons" title="View Quote" ' + cssBorder + ' >View Quote</div>' +
        '   </div>' +
        '</div>';

    return html;

}

function showSavedQuote() {

    var quote = {};
    var customer = getParameter('customer');
    quote.id = getParameter('quote');

    $('#workingOnTitle').html('');
    var url = "https://" + apiHostV1 + "/api/customers/" + customer + '?' + addSite();
    $.getJSON(url, function (apiJSON) {

        quote.Customer = apiJSON;
        var savedQuoteHtml = buildSavedQuote(quote);
        $('#mainContent').html(savedQuoteHtml);
        $('.quoteButtons').unbind("click").bind("click", quoteActions);


        clearQuote({});
        showContentAndSaveState();

    });
}

function saveQuoteSalesFlow2() {

    var customerJson = {
        "leadSource": "self",
        "firstName": dealer.firstName,
        "lastName": dealer.lastName,
        "companyName": dealer.companyName,
        "email": user.email,
        "phone": dealer.phone,
        "jobAddress1": dealer.address1,
        "jobAddress2": dealer.address2,
        "jobCity": dealer.city,
        "jobState": dealer.state,
        "jobZip": dealer.zip,
        "billingAddress1": dealer.address1,
        "billingAddress2": dealer.address2,
        "billingCity": dealer.city,
        "billingState": dealer.state,
        "billingZip": dealer.zip
    };

    newQuoteForCustomer(JSON.stringify(customerJson));
}

function saveQuoteWithDealer(){
    var customerJson = {
        "leadSource": "self",
        "firstName": dealer.firstName,
        "lastName": dealer.lastName,
        "companyName": dealer.companyName,
        "email": user.email,
        "phone": dealer.phone,
        "jobAddress1": dealer.address1,
        "jobAddress2": dealer.address2,
        "jobCity": dealer.city,
        "jobState": dealer.state,
        "jobZip": dealer.zip,
        "billingAddress1": dealer.address1,
        "billingAddress2": dealer.address2,
        "billingCity": dealer.city,
        "billingState": dealer.state,
        "billingZip": dealer.zip
    };

    var json = JSON.stringify(customerJson);
    var url = "https://" + apiHostV1 + "/api/customers?" + authorize() + addSite();
    $.post(url, json, function (data) {
        validate(data);

        var response = jQuery.parseJSON(data);

        if (!isNaN(response.id)) {
            var address = {
                shipping_address1: dealer.address1,
                shipping_address2: dealer.address2,
                shipping_city: dealer.city,
                shipping_state: dealer.state,
                shipping_zip: dealer.zip,
                shipping_phone: dealer.phone,
                billing_address1: dealer.address1,
                billing_address2: dealer.address2,
                billing_city: dealer.city,
                billing_state: dealer.state,
                billing_zip: dealer.zip,
                billing_phone: dealer.phone,
                sales_person_id: response.id
            }
            addQuoteAndAddress(response.id, address, 'yes');
        }
    });
}

function saveQuote() {


    var leadSource = $('#leadSource').val();

    if (empty(leadSource)) {
        stopSpinner();
        alert("Please enter your lead source");
        return;
    }

    var allInputs = $(":input");

    var customerFields = {};
    for (var a = 0; a < allInputs.length; a++) {
        var attr = allInputs[a];
        customerFields[attr.name] = attr.value;
    }
    if (!empty(customerFields['email'])) {
        customerFields['billingEmail'] = customerFields['email'];
    }

    var customerJson = JSON.stringify(customerFields);

    newQuoteForCustomer(customerJson);
}


function newQuoteForCustomer(customerJson) {

    var url = "https://" + apiHostV1 + "/api/customers?" + authorize() + addSite();
    $.post(url, customerJson, function (data) {
        validate(data);

        var response = jQuery.parseJSON(data);

        if (!isNaN(response.id)) {
            createQuote(response.id);
        }
    });
}

function saveNewCustomerToNewQuote() {

    saveNewCustomerToQuote("yes");
}


function saveNewCustomerToQuote(newQuote) {

    startSpinner();
    var leadSource = $('#leadSource').val();

    if (empty(leadSource)) {
        stopSpinner();
        alert("Please enter your lead source");
        return;
    }

    $('#saveNewCustomerToNewQuote').unbind("click");
    $('#saveNewCustomerToQuote').unbind("click");

    if (empty(newQuote)) {
        newQuote = 'no';
    }


    var allInputs = $(":input");

    var customerFields = {};
    for (var a = 0; a < allInputs.length; a++) {
        var attr = allInputs[a];
        customerFields[attr.name] = attr.value;
    }

    customerFields.sales_person_id = user.id;
    delete customerFields.search;

    var quoteAddressFields = jQuery.extend({}, customerFields);

    delete quoteAddressFields.email;
    delete quoteAddressFields.companyName;
    delete quoteAddressFields.firstName;
    delete quoteAddressFields.lastName;
    delete quoteAddressFields.leadSource;
    delete quoteAddressFields.phone;
    delete quoteAddressFields.search;
    delete quoteAddressFields.lastName;

    customerFields.billingAddress1 = quoteAddressFields.billing_address1;
    delete customerFields.billing_address1;


    customerFields.billingAddress2 = returnBlankIfEmpty(quoteAddressFields.billing_address2);
    delete customerFields.billing_address2;
    customerFields.billingCity = quoteAddressFields.billing_city;
    delete customerFields.billing_city;
    customerFields.billingState = quoteAddressFields.billing_state;
    delete customerFields.billing_state;
    customerFields.billingZip = quoteAddressFields.billing_zip;
    delete customerFields.billing_zip;
    customerFields.billingName = quoteAddressFields.billing_contact;
    delete customerFields.billing_contact;
    customerFields.billingEmail = quoteAddressFields.billing_email;
    delete customerFields.billing_email;
    customerFields.billingPhone = quoteAddressFields.billing_phone;
    delete customerFields.billing_phone;

    var checkbox = $("#checkbox").is(':checked');
    if (checkbox) {
        quoteAddressFields.shipping_address1 = quoteAddressFields.billing_address1;
        quoteAddressFields.shipping_address2 = quoteAddressFields.billing_address2;
        quoteAddressFields.shipping_city = quoteAddressFields.billing_city;
        quoteAddressFields.shipping_state = quoteAddressFields.billing_state;
        quoteAddressFields.shipping_zip = quoteAddressFields.billing_zip;
        quoteAddressFields.shipping_contact = quoteAddressFields.billing_contact;
        quoteAddressFields.shipping_email = quoteAddressFields.billing_email;
        quoteAddressFields.shipping_phone = quoteAddressFields.billing_phone;
    } else {
        quoteAddressFields.shipping_address1 = '';
        quoteAddressFields.shipping_address2 = '';
        quoteAddressFields.shipping_city = '';
        quoteAddressFields.shipping_state = '';
        quoteAddressFields.shipping_zip = '';
        quoteAddressFields.shipping_contact = '';
        quoteAddressFields.shipping_email = '';
        quoteAddressFields.shipping_phone = '';
    }

    // customerFields.jobAddress1 = customerFields.shipping_address1;
    // delete customerFields.shipping_address1;
    // customerFields.jobAddress2 = customerFields.shipping_address2;
    // delete customerFields.shipping_address2;
    // customerFields.jobCity = customerFields.shipping_city;
    // delete customerFields.shipping_city;
    // customerFields.jobState = customerFields.shipping_state;
    // delete customerFields.shipping_state;
    // customerFields.jobZip = customerFields.shipping_zip;
    // delete customerFields.shipping_zip;
    // customerFields.shippingName = customerFields.shipping_contact;
    // delete customerFields.shipping_contact;
    // customerFields.shippingEmail = customerFields.shipping_email;
    // delete customerFields.shipping_email;
    // customerFields.shippingPhone = quoteAddressFields.shipping_phone
    // delete customerFields.shipping_phone;


    delete customerFields.shipping_address1;
    delete customerFields.shipping_address2;
    delete customerFields.shipping_city;
    delete customerFields.shipping_state;
    delete customerFields.shipping_zip;
    delete customerFields.shipping_contact;
    delete customerFields.shipping_email;
    delete customerFields.shipping_phone;
    delete customerFields.useShippingAddress;
    delete quoteAddressFields.useShippingAddress;

    var customerJson = JSON.stringify(customerFields);


    delete quoteAddressFields.email;
    delete quoteAddressFields.companyName;
    delete quoteAddressFields.firstName;
    delete quoteAddressFields.lastName;
    delete quoteAddressFields.leadSource;
    delete quoteAddressFields.phone;
    delete quoteAddressFields.search;
    delete quoteAddressFields.lastName;

    newCustomerAPI(customerJson).done(function (data) {

        var response = jQuery.parseJSON(data);
        addQuoteAndAddress(response.id, quoteAddressFields, newQuote);
    });
}

function addQuoteTypeToCart(cart) {

    for (var key in cart) {
        cart[key].type = "quote";
    }
    return cart;
}


function createQuoteForCutSheet() {

    var newQuote = quote;
    quote.Cart = onlyValidCartItems(quote.Cart);

    if (validCart(quote.Cart)) {
        quote.formComplete = 1;
    }

    newQuote.Customer = 1;
    newQuote.Cart = addQuoteTypeToCart(quote.Cart);
    newQuote.total = getCostOfItemsInCart(quote.Cart);
    newQuote.quantity = getTotalItemsInCart(quote.Cart);

    var json = JSON.stringify(newQuote);

    var url = "https://" + apiHostV1 + "/api/quotes?" + authorizePlusSalesPerson() + addSite() + "&admin=yes";
    $.post(url, json, function (data) {

        var response = jQuery.parseJSON(data);
        if (response.status == "Success") {
            newQuote.id = response.id;
            writeQuote(newQuote);
            printCutSheetForSales(newQuote.id);
        } else {
            alert("Create Quote Failed");

        }
    });
}

function addAddress(quoteAddressFieldsJson, isNewQuote, response, customerID) {

    var quote_id = quote.id;
    if (!empty(response)) {
        quote_id = response.id;
    }


    attachAddressesToQuoteAPI(quote_id, quoteAddressFieldsJson, customerID).done(function () {
        if (isNewQuote) {
            resetCartAndForm();
            // newQuote.id = response.id;
            trackAndDoClick('viewSavedQuoteConfirmation', "&quote=" + response.id + "&customer=" + customerID);
            // showSavedQuote(newQuote);
        } else {
            trackAndDoClick("viewQuote", "&quote=" + quote.id);
        }
    });
}

function addSalesPersonQuoteToPipeliner(customerID) {

    if (empty(user.pipelinerId)) {
        return;
    }

    getCustomer(customerID).done(function (data) {
        var lead = {
            "email":returnBlankIfEmpty(data.email),
            "name":returnBlankIfEmpty(data.firstName) + ' ' + returnBlankIfEmpty(data.lastName) ,
            "phone":returnBlankIfEmpty(data.phone),
            "prefix": user.prefix,
            "quoteId": quote.id,
            "amount":  getTotalCost(),
            "pipelinerId": user.pipelinerId,
            "quality": "new"
        }
    })
}

function addQuoteAndAddress(customerID, quoteAddressFields, isNewQuote) {

    var quoteAddressFieldsJson = JSON.stringify(quoteAddressFields);

    if (isNewQuote != 'yes') {
        addAddress(quoteAddressFieldsJson, false, '', customerID);
        return;
    }

    var newQuote = quote;

    quote.Cart = onlyValidCartItems(quote.Cart);

    if (validCart(quote.Cart)) {
        quote.formComplete = 1;
    }

    newQuote.Customer = customerID;
    newQuote.Cart = addQuoteTypeToCart(quote.Cart);
    newQuote.total = getCostOfItemsInCart(quote.Cart);
    newQuote.quantity = getTotalItemsInCart(quote.Cart);

    var json = JSON.stringify(newQuote);

    var url = "https://" + apiHostV1 + "/api/quotes?" + authorizePlusSalesPerson() + addSite();
    $.post(url, json, function (data) {
        var response = jQuery.parseJSON(data);
        if (response.status == "Success") {
            quote.id = response.id;
            addAddress(quoteAddressFieldsJson, true, response, customerID);
        } else {
            alert("Create Quote Failed");
        }

    });

}


//Old
function createQuote(customerID) {

    var newQuote = quote;

    quote.Cart = onlyValidCartItems(quote.Cart);

    if (validCart(quote.Cart)) {
        quote.formComplete = 1;
    }

    newQuote.Customer = customerID;
    newQuote.Cart = addQuoteTypeToCart(quote.Cart);
    newQuote.total = getCostOfItemsInCart(quote.Cart);
    newQuote.quantity = getTotalItemsInCart(quote.Cart);

    var json = JSON.stringify(newQuote);

    var url = "https://" + apiHostV1 + "/api/quotes?" + authorizePlusSalesPerson() + addSite();
    $.post(url, json, function (data) {

        var response = jQuery.parseJSON(data);

        if (response.status == "Success") {

            resetCartAndForm();
            newQuote.id = response.id;
            trackAndDoClick('viewSavedQuoteConfirmation', "&quote=" + newQuote.id + "&customer=" + customerID);
            showSavedQuote(newQuote);

        } else {
            alert("Create Quote Failed");

        }

    });

}

function updateQuoteFollowUps(count, id) {


    if (empty(id)) {
        id = quote.id
    }

    var followUp = $("#followUp-" + count).val();

    var secondFollowUp = $("#secondFollowUp-" + count).val();

    var followUps = {'followUp': followUp, 'secondFollowUp': secondFollowUp};

    quote.secondFollowUp = secondFollowUp;
    quote.followUp = followUp;

    writeQuote(quote);

    var json = JSON.stringify(followUps);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + id + "/updateFollowUps?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {

                //writeCart(cart);
                //writeFormStatus();
                //writeQuoteID(quoteID);

            } else {
                alert("Quotes Save Failed");

            }
        }
    });


}

function buildVersionInfo(quote) {

    var html = '<select class="versionPullDown" >';
    var options = quote.versions;

    if (empty(options)) {
        return '';
    }

    if (options.length < 1) {
        return '';
    }
    for (var a = 0; a < options.length; a++) {

        if (options[0].version == 1) {
            return '';
        }
        if (options[a].version == 1) {
            break;
        }
        html += "<option value=''>Version: " + options[a].version + " -- Changed By " + options[a].name + "</option>"
    }

    html += "</select>";

    return html;
}




function leadInfoHTML() {

    var leadInfo = '';
    var leadStatus = buildQuoteLeadStatus(quote);
    if (tab == 'viewQuote' && siteDefaults.salesFlow == 1) {
        leadInfo = '' +
            '<div class="customerBoxDetails">' +
            '    <div class="customerBoxDetailsLeft">Quote Created:</div><div class="customerBoxDetailsRight">' + quote.quoted + '</div>' +
            '    <div class="customerBoxDetailsLeft">Quality:</div><div class="customerBoxDetailsRight">' + leadStatus + '</div>' +
            '    <div class="customerBoxDetailsLeft">Last Contacted:</div><div class="customerBoxDetailsRight">' +
            '       <input id="followUp-0" onChange="updateQuoteFollowUps(0)"  class="quoteDateBox" type="text"  value= "' + quote.followUp + '" />' +
            '    </div>' +
            '    <div class="customerBoxDetailsLeft">Next Follow Up:</div><div class="customerBoxDetailsRight">' +
            '       <input id="secondFollowUp-0"  onChange="updateQuoteFollowUps(0)" class="quoteDateBox" type="text"  value = "' + quote.secondFollowUp + '"/>' +
            '   </div>' +
            '    <div class="customerBoxDetailsLeftPlus">Lead Source:</div><div class="customerBoxDetailsRightPlus">' + quote.Customer.leadSource +
            '   </div>' +
            '</div>';
    }

    return leadInfo;
}

function onHoldPullDownHTML() {

    var html = '<select id="onHold" class="leadStatusPullDown" onchange="changeOrderOnHold(' + quote.id + ')">';

    if (quote.status == 'hold') {
        html += '<option selected value="Yes">Yes</option>';
        html += '<option value="No">No</option>';
    } else {
        html += '<option value="Yes">Yes</option>';
        html += '<option selected value="No">No</option>';
    }

    html += '</select>';

    return html;
}


function buildQuoteDetails(quote) {

    if (isEmptyObject(quote.Cart)) {
        return '';
    }
    var totalPrice = footerTotalCost();
    var totalItems = getTotalItemsInCart(quote.Cart);
    var quoteTitle = makeQuoteTitle(quote);

    var numberOfDoors = getNumberOfDoors(quote.Cart);
    var numberOfPanels = getNumberOfPanels(quote.Cart);
    var material = getMaterial(quote.Cart);
    var wood = getWood(quote.Cart)

    var versionInfo = buildVersionInfo(quote)
    if (tab == 'viewQuote') {
        versionInfo = '';
    }

    quote = checkEmptySalesDiscountWithQuote(quote);

    quote.SalesDiscount.Totals.preCost = getCostOfItemsInCart(quote.Cart);
    var totalPrice = getTotalForSalesAdmin(quote.SalesDiscount);


    var estCompletion = cleanDate(quote.estComplete);
    if (isProduction()) {
        estCompletion = '       <input id="estComplete-0"  onChange="updateEstCompeteDateWithCountAndId(0,' + quote.id + ')" class="estCompleteDateBoxLarge" type="text"  value = "' + quote.estComplete + '"/>';
    }

    var startedProd = cleanDate(quote.startedProd);

    var orderNotes = '';
    var orderInfo = '';
    if (tab == 'viewQuote' || tab == 'viewOrder' || tab == 'viewProductionOrder' || tab == 'viewAccountingOrder') {

        if (siteDefaults.salesFlow == 1) {
            orderNotes = '' +
                '<div class="orderNotes">Follow Up Notes</div> ' +
                '<textarea onkeyup="quoteNotesKeyUp()" id="quoteNotes" class="orderDetailNotes" placeholder="" name="notes">' + quote.quoteNotes + '</textarea>' +
                '<div id="savingMessageNote"></div>';

            if (isAudit()) {
                orderNotes = '' +
                    '<div class="orderNotes">Follow Up Notes</div> ' +
                    '<textarea onkeyup="quoteNotesKeyUp()" id="quoteNotes" class="orderDetailNotes" placeholder="" name="notes" disabled>' + quote.quoteNotes + '</textarea>' +
                    '<div id="savingMessageNote"></div>';

            }
            var res = quote.quoted.split("/");

            var quoteDate=new Date();
            quoteDate.setFullYear(res[2],res[0],res[1]);

            var checkDate = new Date();
            checkDate.setFullYear(2016,9,5);

            // if (quoteDate > checkDate) {
            //     orderNotes = '';
            // }
        }

        //var dueDate = '' +
        //    '    <div class="customerBoxDetailsLeft">Date Due:</div><div class="customerBoxDetailsRight">' +
        //    '        ' + quote.dueDate +
        //    '    </div>';

        //if (tab == 'viewOrder') {

        var dueDate = '' +
            '    <div class="customerBoxDetailsLeft">Date Due:</div><div class="customerBoxDetailsRight">' +
            '       <input id="dueDate-' + quote.id + '"  onChange="updateOrderDueDateWithCountAndId(' + quote.id + ',' + quote.id + ')" class="viewOrderDateBox" type="text"  value = "' + cleanDate(quote.dueDate) + '"/>' +
            '    </div>'
        //}

        if (tab == 'viewOrder' && quote.type != 'order' ) {
            dueDate = '' +
                '    <div class="customerBoxDetailsLeft">Date Due:</div><div class="customerBoxDetailsRight">' +
                '    '+    cleanDate(quote.dueDate)  +
                '    </div>';
        }

        if (isAudit()) {
            dueDate = cleanDate(quote.dueDate);
        }

        if (siteDefaults.salesFlow == 2) {
            dueDate = '';
        }

        var pickupDeliveryPullDown = buildPickupDeliveryPullDown();

        var estComp = '' +
        '    <div class="customerBoxDetailsLeft">Est. Completion:</div>' +
        '    <div class="customerBoxDetailsRight">' +
        '   ' + estCompletion +
        '    </div>' +
        '    <div class="estCompletion">Estimated completion date does not include shipping time. Please allow 3-5 days for shipping.</div>' ;

        if (siteDefaults.salesFlow == 1) {
            estComp = '';
        }

        var onHold = '' +
        '    <div class="customerBoxDetailsLeft">On Hold:</div>' +
        '   ' + onHoldPullDownHTML() ;

        if (accountingStarted() && !isProduction() && !isSalesPerson()) {
            onHold = '';
        }

        orderInfo = '' +
            '<div class="customerBoxDetails">' +
            '    <div class="customerBoxDetailsLeft">Date Ordered:</div><div class="customerBoxDetailsRight">' + cleanDate(quote.orderDate) + '</div>' +
            '   ' + dueDate +
            // '    <div class="customerBoxDetailsLeft">Pickup / Delivery</div><div class="customerBoxDetailsRight">' + pickupDeliveryPullDown + '</div>' +
            '    <div class="customerBoxDetailsLeft">Date To Prod:</div>' +
            '    <div class="customerBoxDetailsRight">' +
            '   ' + startedProd +
            '    </div>' +
            estComp +
            onHold +
            '</div>';
    }

    var leadInfo = leadInfoHTML();
    // var leadStatus = buildQuoteLeadStatus(quote);
    // if (tab == 'viewQuote' && siteDefaults.salesFlow == 1) {
    //     leadInfo = '' +
    //         '<div class="customerBoxDetails">' +
    //         '    <div class="customerBoxDetailsLeft">Quote Created:</div><div class="customerBoxDetailsRight">' + quote.quoted + '</div>' +
    //         '    <div class="customerBoxDetailsLeft">Quality:</div><div class="customerBoxDetailsRight">' + leadStatus + '</div>' +
    //         // '    <div class="customerBoxDetailsLeft">Last Contacted:</div><div class="customerBoxDetailsRight">' +
    //         // '       <input id="followUp-0" onChange="updateQuoteFollowUps(0)"  class="quoteDateBox" type="text"  value= "' + quote.followUp + '" />' +
    //         // '    </div>' +
    //         // '    <div class="customerBoxDetailsLeft">Next Follow Up:</div><div class="customerBoxDetailsRight">' +
    //         // '       <input id="secondFollowUp-0"  onChange="updateQuoteFollowUps(0)" class="quoteDateBox" type="text"  value = "' + quote.secondFollowUp + '"/>' +
    //         // '   </div>' +
    //         '    <div class="customerBoxDetailsLeftPlus">Lead Source:</div><div class="customerBoxDetailsRightPlus">' + quote.Customer.leadSource +
    //         '   </div>' +
    //         '</div>';
    // }

    var expired = doorIsExpired(quote.Cart);

    var html = '' +
        quoteDetailsTitle(expired) +
        '<div class="customerBoxDetails">' +
        '    <div class="customerBoxDetailsLeft">Quote Amount:</div><div class="customerBoxDetailsRight">' + currency + formatMoney(totalPrice, 2, '.', ',') + '</div>' +
        // '    <div class="customerBoxDetailsLeft">PO:</div><div class="customerBoxDetailsRight">' +
        // '       <input id="po" onkeyup="updatePoDelay(' + quote.id + ')"  class="poBox" type="text"  value= "' + quote.po + '" />' +
        // '    </div>' +
        '</div>' +
        customerOrderInfoHTML(totalItems, material, numberOfDoors, numberOfPanels, wood) +
        orderInfo +
        // leadInfo +
        versionInfo +
        orderNotes;

    return html;
}

function customerOrderInfoHTML(totalItems, material, numberOfDoors, numberOfPanels, wood) {

    var html = '' +
        '<div class="customerBoxDetails">' +
        '    <div class="customerBoxDetailsLeft">Number of Items:</div><div class="customerBoxDetailsRight">' + totalItems + '</div>' +
        '    <div class="customerBoxDetailsLeft">Doors</div><div class="customerBoxDetailsRight">' + numberOfDoors + '</div>' +
        '    <div class="customerBoxDetailsLeft">Panels:</div><div class="customerBoxDetailsRight">' + numberOfPanels + '</div>' +
        '    <div class="customerBoxDetailsLeft">Material:</div><div class="customerBoxDetailsRight">' + material + '</div>' +
        '    <div class="customerBoxDetailsLeft">Wood:</div><div class="customerBoxDetailsRight">' + wood + '</div>' +
        '</div>';


    if (siteDefaults.salesFlow == 1) {
        html = '';
    }

    return html;
}

function doorIsExpired(cart) {

    for (var property in cart) {

        var door = cart[property];
        if (inactiveModule(door.module)) {
            return true;
        }
    }

    return false;
}

function inactiveModule(module) {

    if (empty(siteDefaults)) {
        return false;
    }

    for (var a = 0; a < siteDefaults.modules.length; a++) {
        if (siteDefaults.modules[a] == module) {
            return false
        }
    }

    return true;
}


function quoteDetailsTitle(expired) {


    var text = 'Quote';
    var order = getParameter('order');
    if (!empty(order)) {
        text = 'Order';
    }
    var quoteTitleExtra = makeQuoteTitle(quote, "yes");
    var expiredModule = '';
    var specialColor = '';

    if (expired) {
        expiredModule = '<div class="quoteDetailsOldModule">(Built using an inactive door system)</div>';
        var specialColor = 'red';
    }

    var html = '<div class="quoteDetailsTitle ' + specialColor + '">' + text + ' Details: <span class="quoteDetailsQuote">' + quoteTitleExtra + '</span></div> ' +
        expiredModule;

    return html;

}

function updateSalesTermsDelay() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    $('#salesTerms').css('border-color', 'red');
    delay(function () {
        updateSalesTerms();
    }, 300);

}

function updateSalesTerms() {

    var salesTerms = $('#salesTerms').val();
    var data = {'salesTerms': salesTerms};
    quote.salesTerms = salesTerms;
    writeQuote();

    apiUpdateSalesTerms(data).done(function () {

        $('#salesTerms').css('border-color', 'green');

        delay(function () {
            $('#salesTerms').css('border-color', '#e7e7e7');
        }, 1000);
    });
}


function updatePoDelay() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    $('#po').css('border-color', 'red');
    delay(function () {
        updatePo();
    }, 300);
}


function updatePo() {
    var po = $('#po').val();

    var data = {'po': po};
    var json = JSON.stringify(data);

    quote.po = po;
    writeQuote();

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/po?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
        data: json,
        type: "PUT",
        success: function (result) {
            $('#po').css('border-color', 'green');
            delay(function () {
                $('#po').css('border-color', '#e7e7e7');
            }, 1000);
            generateAndSendDocument(quote.id);
            var response = jQuery.parseJSON(result);
            if (response.status != "Success") {
                alert("Failed to Save Quote Notes");
            }
        }
    });
}


function buildQuoteActions(params) {

    var edit = params.edit;

    var quoteID = params.quote.id;
    var cartItem = getParameter('cartItem');

    var extraPrintCutSheet = '';

    var printQuoteHTML = '<div id="' + quoteID + '-printQuote" class="quoteActionButtons">Print Quote</div>';
    if (siteDefaults.salesFlow == 2) {
        printQuoteHTML = '<div id="' + quoteID + '-printInvoiceForQuote" class="quoteActionButtons">Print Quote</div>';
    }

    var emailQuoteHTML = '<div id="' + quoteID + '-emailQuote" class="quoteActionButtons" title="Email Quote">Email Quote</div>';
    var deleteQuoteHTML = '<div id="' + quoteID + '-deleteQuote" class="quoteActionButtons" title="Delete Quote">Delete Quote</div>';

    if (isLimitedAccount()) {
        deleteQuoteHTML= '';
    }

    if (isEmptyObject(quote.Cart)) {
        extraPrintCutSheet = '';
        printQuoteHTML = '';
        emailQuoteHTML = '';
    }

    var addDoor = '<div id="' + quoteID + '-addDoor" class="quoteActionButtons" title="Add Door">Add Door</div>';
    if (isLimitedAccount()) {
        addDoor = '';
    }
    var html = '' +
        '<div class="quoteActionsBox">' +
        '<div class="customerTitle">Choose Actions</div>' +
        '<div class="customerBox">' +
        extraPrintCutSheet +
        addDoor +
        printQuoteHTML +
        emailQuoteHTML +
        '<div id="' + quoteID + '-closeQuote" class="quoteActionButtons" title="Close Quote">Close Quote</div>' +
        deleteQuoteHTML +
        '</div>' +
        '</div>';


    if (edit == "item") {
        if (cartItem == 'formStatus') {
            html = '' +
                '<div class="quoteActionsBox">' +
                '<div class="customerTitle">Choose Actions</div>' +
                '<div class="customerBox">' +
                '<div id="' + quoteID + '-saveToQuote" class="quoteActionButtons addToQuote" title="Add To Quote">Add To Quote</div>' +
                '<div id="' + quoteID + '-cancelQuoteChanges" class="quoteActionButtons" title="Cancel">Cancel</div>' +
                '</div>' +
                '</div>';

        } else {

            var saveActions = '' +
                '<div id="' + quoteID + '-saveToQuote" class="quoteActionButtons" title="Save To Quote">Save To Quote</div>' +
                '<div id="' + quoteID + '-cancelQuoteChanges" class="quoteActionButtons" title="Cancel">Cancel</div>';

            if (isLimitedAccount()) {
                saveActions = '<div id="' + quoteID + '-cancelQuoteChanges" class="quoteActionButtons" title="Cancel">Cancel</div>';
            }
            if (tab == 'viewOrder') {
                saveActions = '' +
                    '<div id="' + quoteID + '-saveToQuote" class="quoteActionButtons" title="Save To Order">Save To Order</div>' +
                    '<div id="' + quoteID + '-cancelOrderChanges" class="quoteActionButtons" title="Cancel">Cancel</div>';
                if (isLimitedAccount()) {
                    saveActions =  '<div id="' + quoteID + '-cancelOrderChanges" class="quoteActionButtons" title="Cancel">Cancel</div>';
                }
            }

            html = '' +
                '<div id="savingQuote" class="quoteActionsBox">' +
                '<div class="customerTitle">Choose Actions</div>' +
                '<div class="customerBox">' +
                saveActions +
                '</div>' +
                '</div>';
        }
    }


    var deleteJob = '';
    if (isAdmin()) {
        deleteJob = '      <div id="' + quoteID + '-deleteJob" class="quoteActionButtons floatRight">Delete Job</div>';
    }

    if (isProduction() && siteDefaults.salesFlow == 2) {
        html = '' +
            '<div class="quoteActionsBox">' +
            '   <div class="customerTitle">Choose Actions</div>' +
            '   <div class="customerBox">' +
            '      <div id="' + quoteID + '-printCutSheet" class="quoteActionButtons">Print Cut Sheet</div>' +
            '      <div id="' + quoteID + '-printGlassLabels" class="quoteActionButtons productionActionButtons">Print Labels</div>' +
            '      <div id="' + quoteID + '-printCollection" class="quoteActionButtons quoteActionButtonsWide">Print Collection Note</div>' +
            '      <div id="' + quoteID + '-closeJob" class="quoteActionButtons">Close Job</div>' +
            deleteJob +
            '   </div>' +
            '</div>';

    }

    return html;
}


function showAddToQuoteButton() {

    var readyForCheckout = formHasAllTheValidItemsSelected(formStatus);
    if (readyForCheckout) {
        $('.addToQuote').show();
        $('#nextButtonHardware').unbind("click").bind("click", hardWareSaveQuote);

    } else {
        $('.addToQuote').hide();
        $('#nextHardwareTitle').unbind("click");
    }
}


function quoteNotesKeyUp() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    $('#quoteNotes').css('border-color', 'red');
    $('#savingMessageNote').html("Saving...");
    delay(function () {
        updateQuoteNotes(quote.id);
    }, 350);
}


function quoteCommentKeyUp() {


    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    $('#productionDetailNotes88').css('border-color', 'red');
    $('#savingMessageComment').html("Saving...");
    delay(function () {
        updateCommentNotes(quote.id);
    }, 350);
}


function updateCommentNotes(id) {


    var productionNotesSelector = $('#productionDetailNotes88');

    var productionNotes = productionNotesSelector.val();
    productionNotesSelector.css('border-color', 'red');

    quote.productionNotes = productionNotes;

    var data = {'id': id, 'productionNotes': productionNotes};
    var json = JSON.stringify(data);

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusSalesPerson() + addSite();

    if (isProduction()) {
        url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusProduction() + addSite();
    }
    if(isAccounting()){
        url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusAccounting() + addSite();
    }

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);

            if (response.status != "Success") {
                alert("Failed to Save Quote Notes");
            } else {
                generateAndSendDocument(id);
                productionNotesSelector.css('border-color', 'green');
                $('#savingMessageComment').html("Saved");
                delay(function () {
                    productionNotesSelector.css('border-color', '#e7e7e7');
                }, 1000);
            }
        }
    });

}


function updateQuoteNotes(id) {

    var quoteNoteSelector = $('#quoteNotes');
    var quoteNotes = quoteNoteSelector.val();
    quoteNoteSelector.css('border-color', 'red');

    quote.quoteNotes = quoteNotes;

    var data = {'id': id, 'quoteNotes': quoteNotes};
    var json = JSON.stringify(data);

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusSalesPerson() + addSite();

    if (isProduction()) {
        url = "https://" + apiHostV1 + "/api/quotes/" + id + "/updateNotes?" + authorizePlusProduction() + addSite();
    }

    $.ajax({
        url: url,
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);

            if (response.status != "Success") {
                alert("Failed to Save Quote Notes");
            } else {
                generateAndSendDocument(id);
                $('#savingMessageNote').html("Saved");
                quoteNoteSelector.css('border-color', 'green');
            }
        }
    });

}

function buildQuoteNotes(quote) {

    var totalPrice = calculateCostFromDiscount();

    var buttonName = 'Convert to Sale';
    if (siteDefaults.salesFlow == 2) {
        buttonName = 'Purchase';
    }

    var html = '<div id="' + quoteID + '-convertToSale" class="convertToSale myButton selectedButton">' + buttonName + '</div>';
    if (siteDefaults.salesFlow == 3) {
        html = '<div id="' + quoteID + '-convertToSale" class="convertToSale convertToSaleRight myButton selectedButton">' + buttonName + '</div>';
    }

    if (isEmptyObject(quote.Cart)) {
        html = '';
    }

    if (doorIsExpired(quote.Cart)) {
        html = '';
    }
    if (tab == 'viewOrder' || tab == 'viewProductionOrder' || tab == 'viewAccountingOrder') {

        var sendToProduction = '';


        if (quote.type == 'order' && !isEmptyObject(quote.Cart) && quote.leadQuality != 'holdOrder') {
            sendToProduction = '<div id="sendToProduction" class="sendToProduction myButton selectedButton">Send To Production</div>';
            if (doorIsExpired(quote.Cart)) {
                sendToProduction = '';
            }
        }
        if (quote.type == 'order' && !isEmptyObject(quote.Cart) && siteDefaults.salesFlow == 3) {
            sendToProduction = '<div id="sendToProduction" class="sendToProductionRight myButton selectedButton">Send To Production</div>';
        }

        var paymentHistory = getPaymentHistory();

        var enterPaymentInfo = '<div id="enterPaymentInfo">Enter Payment Info</div>' ;

        if (isAudit()) {
            enterPaymentInfo = '';
        }

        html = '' +
            '<div class="customerBoxDetails">' +
            paymentHistory +
            '</div>' +
            enterPaymentInfo +
            sendToProduction;

        if (siteDefaults.salesFlow == 2 && user.type != 'accounting') {
            html = '';
        }
    }

    var preHtml =
        '<div class="quoteCommentSpacer">Comments for Cover Sheet</div> ' +
        '<textarea onkeyup="quoteCommentKeyUp()"  id="productionDetailNotes" class="quoteDetailNotes" placeholder=""  name="productionNotes">' + quote.productionNotes + '</textarea>' +
        '<div id="savingMessageComment"></div>';

    if (siteDefaults.salesFlow == 3 || siteDefaults.salesFlow == 1) {
        preHtml = '';
    }

    html = preHtml + html;

    return html;
}

function setQuotePrefix() {

    setGuestQuotePrefix();
    setSalesPersonQuotePrefix();

}

function setSalesPersonQuotePrefix() {

    if (!empty(quote)) {
        if (isSalesPerson()) {
            quote.prefix = user.prefix;
            quote.postfix = 'Q';
        }
    }

}

function updateQuote() {

    if (!formComplete()) {
        missingSelections();
        return;
    }

    setQuotePrefix();

    var cart = updateGlobalCartWithFormStatus(formStatus);
    quote.Cart = cart;
    writeQuote(quote);

    if (isSalesPerson()) {
        trackAndDoClick('cart');
    } else {
        completeGuestSaveQuote();
    }
}


function onlyValidCartItems(cart) {

    for (var property in cart) {
        var door = cart[property];

        if (door.total < 1) {
            delete cart[property];
        }


    }

    return (cart);
}


function setGuestQuotePrefix() {

    if (isGuest() && empty(quote.prefix)) {
        quote.prefix = 'SQ';
        quote.postfix = '';
    }
}


function completeGuestSaveQuote() {

    var cart = updateGlobalCartWithFormStatus(formStatus);

    quote.Cart = onlyValidCartItems(cart);
    setQuotePrefix();


    writeQuote(quote);
    // formStatus = formStatusDefaults();

    if (empty(quote.Cart)) {
        trackAndDoClick('home');
    }

    savingGuestCart();

}

//Fracka -- Save Guest Cart ---
function savingGuestCart() {

    var saveQuoteData = {};
    saveQuoteData.Cart = addQuoteTypeToCart(quote.Cart);
    saveQuoteData.SalesDiscount = quote.SalesDiscount;
    saveQuoteData.total = getCostOfItemsInCart(quote.Cart);
    saveQuoteData.quantity = getTotalItemsInCart(quote.Cart);
    saveQuoteData.guest = user.id;
    saveQuoteData.guestEmail = user.email;
    saveQuoteData.type = 'quote';
    saveQuoteData.token = user.token;
    saveQuoteData.site = site;
    saveQuoteData.guestForm = user.guestForm || false;
    if($.cookie('assignLead')){
        saveQuoteData.assignLead = $.cookie('assignLead');
    }

    var json = JSON.stringify(saveQuoteData);

    var quoteID = readQuoteID();

    if (quote.id) {
        guestSaveQuoteUpdate(json, quote.id);
    } else {
        guestSaveQuoteNew(json);
    }
}


function setTransferToSalesPersonData(response) {
    if (!empty(response.salesPerson)) {
        quote.prefix = response.salesPerson.prefix;
        if (!empty(response.salesPerson.phone)) {
            quote.phone = response.salesPerson.phone;
        }
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function guestSaveQuoteNew(json) {
    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/guest?" + authorize() + addSite(),
        data: json,
        type: "POST",
        success: function (result) {
            var response = jQuery.parseJSON(result);

            if (response.status == "Success") {

                if (!empty(response.salesPerson)) {
                    var lead = {
                        "email": response.salesPerson.guestEmail,
                        "name": response.salesPerson.guestName,
                        "phone": response.salesPerson.guestPhone,
                        "prefix": response.salesPerson.prefix,
                        "quoteId": response.id,
                        "amount": response.amount,
                        "pipelinerId": response.salesPerson.pipelinerId,
                        "quality": response.salesPerson.leadQuality
                    }
                    quote.prefix = response.salesPerson.prefix;
                    if (empty(response.salesPerson.prefix)) {
                        quote.prefix = 'SQ';
                    }
                    quote.phone = response.salesPerson.phone;
                    writeLead(lead);
                } else {
                    quote.prefix = 'SQ';
                }
                quote.id = response.id;
                writeCart()
                trackAndDoClick('cart');
            } else {
                alert("Quotes Save Failed");
            }
        }
    });
}

function updatePipelinerTotalGuest() {

    var lead = readLead();

    if (!empty(lead.pipelinerId)) {
        var lead = {"ownerId":lead.pipelinerId, "prefix":lead.prefix, "quoteId": lead.quoteId, "amount": getTotalCost()}

//        updatePipelinerAPI(lead).done(function (result) {
//            generateAndSendDocument(lead.quoteId);
//        })
    }
}

function guestSaveQuoteUpdate(json, quoteID) {

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quoteID + "/guest?" + authorize() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {
            updatePipelinerTotalGuest();
            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {
                trackAndDoClick('cart');

            } else {
                alert("Quotes Save Failed");

            }
        }
    });
}

function updatePipelinerTotal() {

    if (!empty(user.pipelinerId)) {
        var lead = {"ownerId":user.pipelinerId, "prefix":user.prefix, "quoteId": quote.id, "amount": getTotalCost()}

//        updatePipelinerAPI(lead).done(function (result) {
//            generateAndSendDocument(lead.quoteId);
//        })
    }
}

function saveQuoteToDB() {

    quote = removeNoCart(quote);
    var noAddressQuote = jQuery.extend({}, quote);
    delete noAddressQuote.address;
    var json = JSON.stringify(noAddressQuote);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "?" + authorize() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);

            updatePipelinerTotal();

            if (response.status == "Success") {
                if (tab == 'viewQuote') {
                    trackAndDoClick('viewQuote', '&quote=' + quote.id + '&');
                } else if (tab == 'viewOrder') {
                    trackAndDoClick('viewOrder', '&order=' + quote.id + '&');
                } else if (tab == 'viewAccountingOrder') {
                    trackAndDoClick('viewAccountingOrder', '&order=' + quote.id + '&');
                } else if (tab == 'viewProductionOrder') {
                    trackAndDoClick('viewProductionOrder', '&order=' + quote.id + '&');
                }
                else {
                    alert("Error: saveQuoteToDB " + tab);
                }
            } else {
                stopSpinner();
                if (response.message == 'Production Already Started') {

                    alert("Production Already Started");
                } else {
                    alert("Quotes Save Failed");
                }

            }
        }
    });

}

function saveOrderToDB() {

    quote = removeNoCart(quote);
    var json = JSON.stringify(quote);

    $.ajax({
        url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "?" + authorize() + addSite(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);

            if (response.status == "Success") {
                trackAndDoClick('viewOrder', '&order=' + quote.id + '&');

            } else {
                alert("Quotes Save Failed 2");
            }
        }
    });

}


function validCart(Cart) {

    var status = true;

    for (var formData in Cart) {
        formStatus = Cart[formData];
        if (!isSizeComplete()) {
            return false;
        }
    }

    return status;
}


function runCheckingWidth(callback) {

    $.when(
        getPanelGroupsByWidth(formStatus.width)
    ).done(function (arrayOfPanels) {
        delete formStatus.newWidth;
        updatePanelInformation(arrayOfPanels);
        callback(newTab);
    });

}
function noCheckSaveQuote() {
    quote.Cart = updateGlobalCartWithFormStatus(formStatus);
    quote.formComplete = 1;
    writeQuote(quote);
    saveQuoteToDB();
}

function saveToQuote(quoteID) {

    if (empty(formStatus.newWidth)) {
        validateAndSaveCart(quoteID);

    } else if (formStatus.newWidth == 'yes') {
        $.when(
            getPanelGroupsByWidth(formStatus.width)
        ).done(function (arrayOfPanels) {
            delete formStatus.newWidth;
            updatePanelInformation(arrayOfPanels);
            validateAndSaveCart(quoteID);
        });

    } else {
        validateAndSaveCart(quoteID);
    }
}


function validateAndSaveCart(quoteID) {


    //if (isSizeComplete()) {
    quote.Cart = updateGlobalCartWithFormStatus(formStatus);
    quote.formComplete = 1;
    writeQuote(quote);
    saveQuoteToDB(quoteID);
    //}   else {
    //    missingSelections();
    //}
}


function buildEmailQuote(params) {

    var id = params.quote.id;

    var toEmail = '';
    var salesPersonEmail = '';
    var attachment = 'quote';

    if (!empty(params.customerID.email)) {
        toEmail = params.customerID.email;
    }

    if (!empty(params.quote.Customer.email)) {
        toEmail = params.quote.Customer.email;
        salesPersonEmail = ", " + user.email;
    }
    if (tab == 'viewOrder') {
        attachment = 'order';
    }

    var attachDoc = '' +
        '<div id="' + id + '-emailAddFiles" class="emailButtons">Attach Doc</div>' +
        '   <input id="emailFiles" hidden="true" multiple="true" name="emailFiles" type="file" />' +
        '   <div id="emailFilesNames"></div>' +
        '</div>';

    if (siteDefaults.salesFlow == 2) {
        attachDoc = '';
    }

    var html = '' +
        '<div id="sendingEmail" class="emailBox">' +
        '<input class="textBox" type="text" id="emailAddresses" value="' + toEmail + salesPersonEmail + '"  placeholder="Enter Recipients ( Separate with comma )"/>' +
        '<div id="' + id + '-emailSend" class="emailButtons">Send</div>' +
        '<input class="textBox" type="text" id="emailSubject" value="' + siteDefaults.companyName + '"  placeholder="Enter Subject"/>' +
        '<div id="' + id + '-emailCancel"  class="emailButtons">Cancel</div>' +
        ' <TEXTAREA id="emailMessage" NAME=address ROWS=3 placeholder="Enter Message"></TEXTAREA>' +
        '<div class="emailCheckBoxContainer">' +
        '   <input id="addQuoteCheckBox" type="checkbox" name="addQuote" value="yes" checked> Attach ' + attachment +
        '</div>' +
        attachDoc;



    var selector = params.selector;
    $(selector).html(html);
    $('.emailButtons', '#sendingEmail').unbind("click").bind("click", sendEmailButtons);
    clearEmailAttachments(id);
    $("#emailFiles").unbind("change").bind("change", function(e) {
        var files = e.target.files;
        if(files.length){
            startSpinner();
            var data = new FormData();
            for (var i = 0; i < files.length; i++) {
                data.append("filesToUpload[]", files[i]);
            }
            attachEmailFiles(id, data).done(function(response){
                stopSpinner();
                $.each(response.files, function(i, file){
                    $('#emailFilesNames').append('<p class="attachment">'+file+' <button class="deleteAttachment" data-file="' + file + '"> X </button></p>');
                    $('.deleteAttachment').unbind("click").bind("click", {id:id}, deleteAttachment);
                });
            });
        }
    });
    scrollTopOfPage();


}
function buildEmailConfirmed(params) {

    var text = "Quote";
    var order = getParameter('order');

    if (!empty(order)) {
        text = "Order"
    }

    var id = params.quote.id;
    var html = '' +
        '<div id="sendingEmail" class="emailBox">' +
        '<div id="' + id + '-emailCancel" class="emailButtons">Return To ' + text + '</div>' +
        '<div class="emailInfo">Your email has been sent!</div>' +
        '</div>';


    var selector = params.selector;
    $(selector).html(html);
    $('.emailButtons', '#sendingEmail').unbind("click").bind("click", sendEmailButtons);


}
function sendEmailButtons() {

    var id = $(this).attr('id');
    var quote = splitForFirstOption(id);
    var action = splitForSecondOption(id);
    var order = getParameter('order');

    if (action == "emailSend") {
        sendEmail(quote);
    } else if(action == "emailAddFiles"){
        $('#emailFiles').click();
    } else {
        clearEmailAttachments(quote);
        if (empty(order)) {
            trackAndDoClick('viewQuote', '&quote=' + quote);
        } else {
            trackAndDoClick('viewOrder', '&order=' + quote);
        }
    }
}


function okToSendQuoteEmail() {

    var status = true;

    if (!$('#emailAddresses').val()) {
        stopSpinner();
        alert("Please enter at least one email address");
        status = false;
    } else if (!$('#emailSubject').val()) {
        stopSpinner();
        alert("Please enter a subject")
        status = false;
    }
    return status;
}

function deleteAttachment(event){
    if (confirm("Are you sure you want to remove this attachment?") == true) {
        var $this = $(this),
            id = event.data.id,
            data = { 'file': $this.data('file') },
            json = JSON.stringify(data);
        startSpinner();
        deleteEmailAttachment(id, json).done(function (response) {
            stopSpinner();
            $this.parent('.attachment').remove();
        }).fail(function () {
            alert("error");
        });
    }
}

function sendEmail(quote) {

    if (!okToSendQuoteEmail()) {
        return;
    }

    $('.emailButtons:first-of-type').replaceWith("<div class='emailQuoteSpinner'><img src='/images/icons/spinning.gif'></div>");
    $('.emailButtons:nth-of-type(2n)').css('visibility', 'hidden')


    var addresses = $('#emailAddresses').val().split(/,/);

    var addQuote = quote;

    if (!$('#addQuoteCheckBox').is(':checked')) {
        addQuote = '';
    }

    var message = $('#emailMessage').val() + '  ';
    var subject = $('#emailSubject').val();

    var sendQuote = '';

    var params = {
        "addresses": addresses,
        "message": message,
        "subject": subject,
        "quote": addQuote,
        "salesPerson": user.id,
        "token": user.token
    };
    var json = JSON.stringify(params);

    var debug = getParameter('debug');

    var order = getParameter('order');
    var hasOrder = '';
    if (!empty(order)) {
        hasOrder = '&order=yes';
    } else {
        hasOrder = '&quote=yes';
    }


    var url = "https://" + apiHostV1 + "/api/email?" + authorizePlusSalesPerson() + addSuperToken() + addSite() + '&debug=' + debug + hasOrder;

    if (siteDefaults.salesFlow == 2) {
        url = "https://" + apiHostV1 + "/api/quotes/" + quote + "/emailInvoice?" + authorizePlusSalesPerson() + addSuperToken() + addSite() + '&debug=' + debug + hasOrder;
    }

    $.post(url, json, function (data) {
        var response = JSON.parse(data);

        if (debug == 'yes') {
            alert(response);
        }
        if (response.message == 'success') {

            if (!empty(order)) {
                trackAndDoClick('viewOrder', '&order=' + quote + '&edit=emailConfirmed');
            } else {
                trackAndDoClick('viewQuote', '&quote=' + quote + '&edit=emailConfirmed');
            }

        } else {
            alert("Error Sending the Email");
            trackAndDoClick('viewQuote', '&quote=' + quote + '&edit=email');
        }
    });
}


function chooseQuoteDetailsPath(params) {

    if (siteDefaults.salesFlow == 3) {
        addQuoteDetailsWithParametersFlow3(params);
    }
    else if (siteDefaults.salesFlow == 2) {
        addQuoteDetailsWithParametersFlow2(params);
    }
    else {
        addQuoteDetailsWithParametersFlow1(params);
    }
}


function loadQuoteAndCart(params) {

    if (params.edit == 'email') {
        buildEmailQuote(params)
    } else if (params.edit == 'emailConfirmed') {
        buildEmailConfirmed(params)
    } else {
        chooseQuoteDetailsPath(params);
    }

    updateWorkingOnTitle();

    loadSoloCart();

    var total = getTotalForSalesAdmin(quote.SalesDiscount);

    $('#footerPrice').html('Total ' + currency + formatMoney(total) + footerNoteHTML());
    

    if (isSalesPerson()) {
        addSalesAdminWithLocation('#footerRight');
        if(isUserHasSalesAdmin()){
            $('#footerRight').html('<img onclick="launchSalesAdmin()" id="saleAdminLauncher" src="/images/footer/gears.png">');
        }
    }

    if (siteDefaults.salesFlow == 1) {
        $(".quoteDateBox").datepicker({gotoCurrent: true});
    }


    $('#cartButton').hide();
    processStartOverButtonVisibility(false);

}

function loadQuoteAndCarEditingItem(params) {

    var cartItem = getParameter('cartItem');

    if (!empty(cartItem) && cartItem != 'formStatus') {

        if (quote.Cart[cartItem].unique != formStatus.unique) {
            formStatus = quote.Cart[cartItem];
        }
    }


    $.when(getModuleOptions()).done(function (moduleInfoFromApi) {

        moduleInfo = moduleInfoFromApi;

        secondTab = getParameter('secondTab');
        if (empty(secondTab)) {
            secondTab = moduleInfo.firstTab;
        }

        if (secondTab == 'addDoor') {
            formStatusDefaults();
        }

        if (siteDefaults.salesFlow == 3) {
            addQuoteDetailsWithParametersFlow3(params);
        } else if (siteDefaults.salesFlow == 2) {
            addQuoteDetailsWithParametersFlow2(params);
        } else {
            addQuoteDetailsWithParametersFlow1(params);
        }

        if (secondTab == 'color') {
            loadColorAndFinish();
        } else if (secondTab == 'glass') {
            loadGlass();
        } else if (secondTab == 'hardware') {
            loadHardware();
        } else if (secondTab == 'extras') {
            loadExtras();
        } else if (secondTab == 'installation') {
            loadInstallation();
        } else if (secondTab == 'addDoor') {
            loadProducts();
        } else {
            if (moduleInfo.firstTab == 'sash') {
                loadSizeAndSash();
            } else {
                loadSizeAndPanels();
            }
        }

        updateWorkingOnTitle();
        updateFormPrice();
        $('#cartButton').hide();
        processStartOverButtonVisibility(false);

    })
}


function updateWorkingOnTitle() {

    var title = makeQuoteTitle(quote);
    var uriTab = getParameter('tab');

    if (uriTab == 'viewQuote') {
        title = 'Viewing Quote: ' + title;
    } else {
        title = 'Viewing Order: ' + title;
    }

    $('#workingOnTitle').html(title);
}

function loadDepositConfirmedMail() {
    $('#companyHeader').hide();
    var id = getParameter('id');
    var token = getParameter('token');
    var accounting = getParameter('accounting');

    var url = "https://" + apiHostV1 + "/api/orders/" + id + "?&token=" + token + "&accounting=" + accounting + addSite();
    $.getJSON(url, function (orderInfo) {
        stopSpinner();
        var cutomerName = orderInfo.Customer.firstName + ' ' + orderInfo.Customer.lastName;
        var html = '' +
            '<div id="mailPrint">' +
                '<div class="wrapper">' +
                    '<div class="image">' +
                        '<img border="0" src="/images/company/pano-logo-blk-blue.png" alt="" width="400" />' +
                    '</div>' +
                    '<div class="header-text">' +
                        '<span style="font-size: 20px; font-style:italic;">Here at panoramic Doors we take pride in doing things properly, so we are writing to say...</span>'+
                    '</div>'+
                    '<div class="mail-spacer"></div><div class="mail-spacer"></div>'+
                    '<div class="header-text">'+
                        '<span style="font-size:26px; font-family: \'PT Serif\', serif; font-weight: 700;">T H A N K&nbsp;&nbsp;&nbsp;Y O U</span>'+
                    '</div>'+
                    '<div class="mail-spacer"></div>'+
                    '<div style="width:470px; margin: 0 auto; height: 3px; background-color: #c2c2c2"></div>'+
                    '<div class="mail-spacer"></div><div class="mail-spacer"></div>'+
                    '<div style="width: 100%; padding: 0 20px;">'+
                        '<div>Order ID: ' + orderInfo.prefix + orderInfo.id + '</div>'+
                        '<div>Dear: ' + cutomerName + '</div>'+
                        '<br /><br />'+
                        '<div>Thank you for choosing Panoramic Doorsfor your project, yourpurchase is very important to us and keeping you informed of the process of your order is fundamental to a satisfactory and successful completion, please be so kind to read this informative guide.</div>'+
                    '</div>'+
                    '<div class="mail-spacer"></div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/calendar.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>YOUR ESTIMATED MANUFACTURING COMPLETION DATEIS:</strong> ' + cleanDate(orderInfo.dueDate) + '<br />'+
                            'You will be contacted 3 weeks before to confirm or modify this date if your project is delayed. Please allowed additional time for delivery.'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/payment.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>FINAL PAYMENT</strong> is required uponmanufacturing completion of the product and before items are shipped.'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/storage.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>STORAGE COSTS</strong> If for any reason you are not ready to receive your orderfor delivery, Storage costs of $200 per system perweek will incur. Any storage fees due, must be paid before systems will ship.'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/delivery.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>PD DELIVERY IS A CURBSIDE DELIVERY ONLY!</strong> Please arrange to have the product transported into your property. Arrival times are ESTIMATED'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/inspect.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>INSPECT YOUR PRODUCT</strong> Inspect product for signs of damage. Immediately report any damages or missing parts. Take pictures and call at 760.722.1250 ext. 231 All damages should be reported before installation to be covered under warranty.'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/manual.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>PLEASE READ THE MANUAL OF INSTALLATION BEFORE YOU START.</strong> Preparation is the key to a trouble-free installation. The manual and Video tutorials are available online <a href="http://www.panoramicdoors.com">www.panoramicdoors.com</a> - support tab.'+
                        '</div>'+
                    '</div>'+
                    '<div style="text-align: center;"><strong>FOR ANY TECHNICAL QUESTIONS PLEASE CALL 760.722.1250</strong></div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/wood.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>All unfinished wood components should not be exposed to water and should be sealed within 5days from unwrapping</strong>'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/list.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>REGISTER YOUR SYSTEM(S)</strong> If your system was ordered by a builder, contractor, architect, it is very important you register your system(s) online, to keep the correct information of the owner in our database.'+
                        '</div>'+
                    '</div>'+
                    '<div class="clearfix">'+
                        '<div class="column image">'+
                            '<img border="0" src="/images/email/installer.png" alt="" />'+
                        '</div>'+
                        '<div class="column content">'+
                            '<strong>WHEN HIRING AN INSTALLER,</strong> werecommend that you inquire about:'+
                            '<ul>'+
                                '<li>Their Installation Warranty terms and length.</li>'+
                                '<li>Do they perform adjustmentsand maintenance.</li>'+
                                '<li>Adjustment and maintenance costs. </li>'+
                            '</ul>'+
                        '</div>'+
                    '</div>'+
                    '<div>'+
                        '<p>If you have any questions about your order please do not hesitate to contact me.</p><br /><br />'+
                        '<p>Kind Regards</p><br />'+
                        '<p>Name: '+orderInfo.SalesPersonInfo.name+'</p>'+
                        '<p>Email: '+orderInfo.SalesPersonInfo.email+'</p>'+
                        '<p>Cell: '+orderInfo.SalesPersonInfo.phone+'</p>'+
                    '</div>'+
                    '<div class="spacer"></div>'+
                    '<div class="footer-line"></div>'+
                    '<div class="footer-text">'+
                        'Our passion is the happiness we bring to the people whose spaces we help to transform. We strive to provide you with a quality product and the best customer experience, so you are confident of your purchase and proud to own a Panoramic Door.'+
                        '<br /><br />'+
                        'Transforming one space at a time!'+
                    '</div>'+
                    '<div class="spacer"></div>'+
                    '<div class="image">'+
                        '<img border="0" src="/images/company/pano-logo-blk-blue.png" alt="" width="400" />'+
                    '</div>'+
                '</div>' +
            '</div>';
        $('body').append(html);

    }).error(function (data) {
        reLoginError(data);
    }).fail(function () {
        bounceUser();
    });
}