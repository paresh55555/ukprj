"use strict";

function renderFlow1(apiJSON) {

    var salesPerson = getParameter('salesPerson');
    var production = getParameter('production');
    var accounting = getParameter('accounting');

    var edit = getParameter('edit');
    var print = getParameter('print');

    quote = apiJSON;
    quote.noCart = 'yes';

    // if (empty(quote)) {
    //     trackAndDoClick('viewQuotes', "&blankQuote");
    //     return;
    // }

    if (!empty(quote.SalesDiscount)) {
        discounts = quote.SalesDiscount;
    } else {
        quote = setSalesDiscountToEmptyWithPassQuote(quote);
        discounts = quote.SalesDiscount;
        // discounts = jQuery.extend({}, defaults);
        // quote.SalesDiscount = jQuery.extend({}, defaults);
    }

    writeQuote(quote);
    writeQuoteID(quote.id);

    var params = {};
    params.quote = quote;
    params.customerID = quote.Customer;
    params.selector = "#preMainContent";
    params.edit = edit;


    if (edit === 'item') {
        loadQuoteAndCarEditingItem(params);
    } else {
        loadQuoteAndCart(params);
    }

    $('#preMainContent').show();

    if (print === 'yes') {
        stopSpinner();
        $('#companyHeader').hide();
        $('#headerNav').hide();
        $('#salesNav').hide();
        $('#preMainContent').hide();
        $('#footer').hide();

        var extraHeight = '228px';
        if (salesPerson) {
            extraHeight = '230px';

            var manifest = getParameter('manifest');

            if (manifest == 'yes') {
                var html = buildCustomerAndQuoteOverView(quote, true);
                $('#mainContent').html(html);
            } else {
                var html = buildCustomerAndQuoteOverView(quote);
                $('#mainContent').prepend(html);
            }
        }

        var manifest = getParameter('manifest');
        if (manifest == 'yes') {
            $('.cartRow').css({'margin-bottom': extraHeight - 10});
        } else {
            $('.cartRow').css({'margin-bottom': extraHeight + 35});
        }
    }

    if ( (siteDefaults.salesFlow == 3 || siteDefaults.salesFlow == 1)  && print != 'yes') {
        var mainHTML = buildCustomerAndQuoteOverView(quote);
        $('#mainContent').prepend(mainHTML);
        $('.fieldUpdate').unbind("keyup").bind("keyup", updateCustomerDelay);
        $('.fieldUpdate').unbind("change").bind("change", updateCustomerNoDelay);
        $('#salesTerms').unbind("keyup").bind("keyup", updateSalesTermsDelay);
    }

    updateCssForViewQuote();
    showContentAndSaveState();
    $('#footerPrice').show();
    scrollTopOfPage();
}


function addQuoteDetailsWithParametersFlow1(params) {

    var customerID = params.customerID;
    var selector = params.selector;
    var edit = params.edit;

    var quoteDetails = buildQuoteDetails(quote);
    var customerDetails = buildCustomerDetails(quote.Customer);
    var quoteNotes = buildQuoteNotes(quote);
    var actions = buildQuoteActions(params);

    var quoteDetails = '' +
        '<div class="quoteDetailsColumn">' + quoteDetails + '</div>' +
        '<div class="quoteDetailsColumn">' + customerDetails + '</div>' +
        '<div class="quoteDetailsColumn">' + quoteNotes + '</div>';

    var border = qualityBorderColor(quote);
    if (tab == 'viewOrder') {
        border = getStatusBorder(quote);
    }

    if (siteDefaults.salesFlow == 2 ) {
        border = 'archivedBorder';
    }

    var html = '<div id ="quoteDetailBoxID" class="quoteDetailsBox ' + border + '">' + quoteDetails + actions + '</div>';

    $(selector).html(html);
    $('.quoteActionButtons', '.quoteDetailsBox').unbind("click").bind("click", quoteActions);
    $('.convertToSale').unbind("click").bind("click", quoteActions);

    showAddToQuoteButton();
}


