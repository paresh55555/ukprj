function addQuoteDetailsWithParametersFlow3(params, type) {

    var customerID = params.customerID;
    var selector = params.selector;
    var edit = params.edit;

    var quoteDetails = flow3BuildCustomerDetails(quote);
    var uploadContractDetails = buildUploadContractDetails(quote);
    var paymentHistory = getPaymentHistory();
    var paymentHistoryHTML = '';
    var type = type || 'quote';

    if (tab != 'viewQuote') {
        paymentHistoryHTML =
            '<div class="customerBoxDetails">' +
            '   ' + paymentHistory +
            '</div>' +
            '<div id="enterPaymentInfo">Enter Payment Info</div>';
    }

    var actionButton = flow3ActionButton(quote);
    var actions = buildQuoteActions(params);
    if (tab == 'viewOrder' || tab == 'viewProductionOrder' || tab == 'viewAccountingOrder') {
        actions = buildOrdersActions(params);
    }

    var quoteDetails = '' +
        '<div class="quoteDetailsColumn">' + quoteDetails + '</div>' +
        '<div class="quoteDetailsColumn">' + paymentHistoryHTML + ((type === 'order' && !isPrint())? uploadContractDetails : '') + '</div>' +
        '<div class="quoteDetailsColumn">' + actionButton + '</div>';

    var border = qualityBorderColor(quote);
    if (tab == 'viewOrder') {
        border = getStatusBorder(quote);
    }

    var html = '<div id ="quoteDetailBoxID" class="quoteDetailsBox ' + border + '">' + quoteDetails + actions + '</div>';

    $(selector).html(html);

    if (tab == 'viewOrder' || tab == 'viewProductionOrder' || tab == 'viewAccountingOrder') {
        $('.quoteActionButtons', '.quoteDetailsBox').unbind("click").bind("click", orderActions);
    } else {
        $('.quoteActionButtons', '.quoteDetailsBox').unbind("click").bind("click", quoteActions);
    }

    if(!isPrint()){
        initContracts();
    }

    $('.convertToSale').unbind("click").bind("click", quoteActions);
    $('.quoteActionButtons').unbind("click").bind("click", quoteActions);

    $('#enterPaymentInfo').unbind("click").bind("click", enterPaymentInfo);
    showAddToQuoteButton();
}

function flow3BuildCustomerDetails(quote) {

    if (isEmptyObject(quote.Cart)) {
        return '';
    }

    var html = '';
    if (tab == 'viewQuote' || tab == 'viewOrder' || tab == 'viewProductionOrder' || tab == 'viewAccountingOrder') {

        var dueText = 'Date Due:';
        if (isUKGroup()) {
            dueText = 'Requested Date:';
        }
        var dueDate = '' +
            '    <div class="customerBoxDetailsLeft">'+dueText+'</div><div class="customerBoxDetailsRight">' +
            '       <input id="dueDate-' + quote.id + '"  onChange="updateOrderDueDateWithCountAndId(' + quote.id + ',' + quote.id + ')" class="viewOrderDateBox"  type="text"  value = "' + cleanDate(quote.dueDate) + '"/>' +
            '    </div>';

        var estComp = '' +
            '    <div class="customerBoxDetailsLeft">Est. Complete:</div><div class="customerBoxDetailsRight">' +
            '       ' + cleanDate(quote.estComplete) +
            '    </div>';

        var onHold = '' +
            '    <div class="customerBoxDetailsLeft">On Hold:</div>' +
            '   ' + onHoldPullDownHTML() ;


        if (tab == 'viewQuote') {
            onHold = '';
        }

        if (isProduction()) {

            dueDate = '' +
                '    <div class="customerBoxDetailsLeft">Date Due:</div><div class="customerBoxDetailsRight">' +
                '      ' + cleanDate(quote.dueDate) +
                '    </div>';

            estComp = '' +
                '    <div class="customerBoxDetailsLeft">Est. Complete:</div><div class="customerBoxDetailsRight">' +
                '       <input id="estComplete-' + quote.id + '"  onChange="updateEstCompeteDateWithCountAndId(' + quote.id + ',' + quote.id + ')" class="viewOrderDateBox" type="text"  value = "' + cleanDate(quote.estComplete) + '"/>' +
                '    </div>';
        }

        if (isAccounting()) {
            dueDate = '' +
                '    <div class="customerBoxDetailsLeft">Date Due:</div><div class="customerBoxDetailsRight">' +
                '      ' + cleanDate(quote.dueDate) +
                '    </div>';
            var estComp = '' +
                '    <div class="customerBoxDetailsLeft">Est. Complete:</div><div class="customerBoxDetailsRight">' +
                '       ' + cleanDate(quote.estComplete) +
                '    </div>';
        }

        html = '' +
            '<div class="customerBoxDetails">' +
            '   ' + dueDate +
            '   ' + estComp +
            '   ' + onHold + 
            '</div>';
    }

    return html;

}
function flow3ActionButton(quote) {

    var buttonName = 'Convert to Sale';
    var html = '';

    if (quote.type == 'quote') {
        html = '<div id="' + quoteID + '-convertToSale" class="convertToSale convertToSaleRight myButton selectedButton">' + buttonName + '</div>';
    }
    
    if (isEmptyObject(quote.Cart)) {
        html = '';
    }

    if (tab == 'viewOrder' || tab == 'viewProductionOrder' || tab == 'viewAccountingOrder') {
        if (quote.type == 'order' && !isEmptyObject(quote.Cart)) {
            html = '<div id="sendToProduction" class="sendToProductionRight myButton selectedButton">Send To Production</div>';
        }
    }
    

    return html;
}

