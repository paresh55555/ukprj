'use strict'

var sash = {"marginHeight":5, "marginWidth":5};
var frame = {"width": 700, "height": 400, "marginHeight":10, "marginWidth":10, "border": 1};
var xMult = 1;
var yMult = 1;

var textBoxMargin = 3;
var textBoxHeight = 25 + (textBoxMargin * 2);
var textBoxWidth = 26 + (textBoxMargin * 2);
var border = 1;
var maxUIHeight = 400;
var maxUIWidth = 700;

var delayAmount = 300;
var chooseDimensionLeftRight = 'left';
var chooseDimensionTopBottom = 'top';
var selectedWindow;


function addOpener () {

    buildAddOpener(this);
    updateWindowPrice();
}


function buildAddOpener (selector) {

    var imageType = $(selector).attr('id');
    if (!imageType) {
        imageType = (selector.split('#'))[1]
    }
    $('.selectedOption').removeClass('selectedOption');

    var selector =$('#'+selectedWindow);
    var parent = selector.parent();

    windowsForm.openers[selectedWindow] = imageType;
    writeWindowsForm();
    if(formStatus.panelOptions && formStatus.panelOptions.swingDirection){
        formStatus.panelOptions.swingDirection.selected = getOpenersCount();
    }

    var openerMarginHeight = 5;
    var openerMarginWidth = 5;

    if (imageType == 'none') {
        openerMarginHeight = sash.marginHeight ;
        openerMarginWidth = sash.marginWidth ;
        selector.css({'margin-left':sash.marginWidth+'px','margin-right':sash.marginWidth+'px',
                      'margin-top':sash.marginHeight+'px','margin-bottom':sash.marginHeight+'px'});
    } else {
        selector.css({'margin-left':openerMarginWidth+'px','margin-right':openerMarginWidth+'px',
            'margin-top':openerMarginHeight+'px','margin-bottom':openerMarginHeight+'px'});
    }

    var height = parent.height() - (openerMarginHeight * 2) - 2;
    var width = parent.width() -  (openerMarginWidth * 2) - 2 ;

    selector.height(height);
    selector.width(width);

    selector.html('<img align="left" src="/images/openers/white/'+imageType+'White.svg" height="'+height+'px" width="'+width+'px">');
    selector.removeClass('selectedWindow');
    deActivateOpeners();
}

function getOpenersCount(){
    var count = 1; //Use the database formulas to reduce the number of swings for correct pricing.

    for(var opener in windowsForm.openers){
        if (windowsForm.openers.hasOwnProperty(opener) && windowsForm.openers[opener] !== 'none') {
            count++;
        }
    }

    return count;
}

function addDimensionsToWindows (rows) {

    //Order is Important. Top/Bottom Left/Right Placement needs to be decided before added Total Dimensions
    buildDimensionsVertical(rows);
    buildDimensionsHorizontal(rows);

    buildTotalDimensionsVertical();
    buildTotalDimensionsHorizontal();
}


function deActivateOpeners() {

    var selector = $('.operation');
    selector.css({ opacity: 0.3 });

    selector.unbind("click").bind("click", disabledOpeners);
}


function disabledOpeners() {

    alert("Select a window first and then choose the direction of opener you wish for that window");
}


function activateOpenerSelection() {

    var selector = $('.operation');
    selector.css({ opacity: 1.0, cursor: 'pointer' });
    selector.unbind("click").bind("click", addOpener);
}


function selectSash () {

    $('.selectedOption').removeClass('selectedOption');
    $('.sash').removeClass('selectedWindow');
    $(this).addClass('selectedWindow');
    selectedWindow = $(this).attr('id');

    var imagePath = $(this).children().first().attr('src');

    if (!empty(imagePath)) {
        var image = imagePath.split("white\/");
        var type = image[1].split("White.svg");
        $('#'+type[0]).addClass('selectedOption');
    }

    activateOpenerSelection();
}


function empty(variable) {

    var status = false;

    if ( variable == 'NaN' || variable === undefined || variable === null || variable == '' || variable == 'null' || variable == 'undefined') {
        status = true;
    }
    return status;
}


function updateDimensions() {

    var selector = $(this);

    delay(function(){
        if (isValidHorizontalDimensionWithSelectorAndType(selector, 'vertical')) {
            updateWindows(selector);
            restoreDimensions();
        } else {
            selector.addClass('backgroundPink');
        }
    }, delayAmount );
}


function updateVerticalDimensions() {

    var selector = $(this);

    delay(function(){
        if (isValidVerticalDimensionWithSelectorAndType(selector, "vertical")) {
            updateVerticalWindows(selector);
            restoreDimensions();
        } else {
            selector.addClass('backgroundPink');
        }

    }, delayAmount );
}


function updateVerticalWindows(selector) {

    var value = parseFloat(selector.val()) ;
    var options = selector.attr('id');

    var row = parseInt(getOptionIndex(options, 1));
    var type = getOptionIndex(options, 2);

    if (row == rows.length - 1) {

        row = 0;
        var windows = [];
        for (var a = 0; a < rows.length - 1; a++) {
            windows.push({"height": rows[a].height });
        }

        var height = remainingHeight(windows);
        var addHeight = height - value;
        var firstHeight = rows[row].height;
        value = firstHeight + addHeight;

    }

    rows[row][type] = value;

    windowsForm.rows = rows;
    writeWindowsForm();
    buildVerticalWindows(rows);
}


function updateWindows(selector) {

    var value = parseFloat(selector.val()) ;
    var options = selector.attr('id');

    var row = getOptionIndex(options, 1);
    row = parseInt(row) - 1;
    var type = getOptionIndex(options, 2);
    var windowNumber = getOptionIndex(options, 3);
    windowNumber = parseInt(windowNumber) + 1;

    if (windowNumber == rows[row].numberOfWindows) {
        var width;
        var windows = [];

        for (var a = 1; a < rows[row].numberOfWindows; a++) {
            width = rows[row]['width'+a] ;
            windows.push({"width":width});
        }

        width = remainingWidthVertical(windows);
        var firstWidth = width - value;
        var width1 = rows[row].width1;
        value = width1 + firstWidth;
        windowNumber = 1;

    }

    rows[row][type+windowNumber] = value

    windowsForm.rows = rows;
    writeWindowsForm();
    buildVerticalWindows(rows);
}


function splitForSecondOption(option) {

    return getOptionIndex(option, 1);
}


function splitForFirstOption(option) {

    return getOptionIndex(option, 0);
}


function getOptionIndex(option, index) {

    var arrayOption = '';
    if (option) {
        arrayOption = option.split(/-/);
    }


    if (arrayOption[index]) {
        return arrayOption[index];
    } else {
        return '';
    }
}


function getWidthOfWindows (windows) {

    var width = 0
    for (var a = 0; a < windows.length; a++) {
        width = windows[a].width + width;
    }
    return width;
}


function getHeightOfWindows (windows) {

    var height = 0
    for (var a = 0; a < windows.length; a++) {
        height = windows[a].height  + height;
    }
    return height;
}


function remainingHeight (windows) {

    var heigthOfWindows = getHeightOfWindows(windows);

    var remainingSpace = (frame.height * yMult) - heigthOfWindows ;
    return remainingSpace;
}


function lessWidth() {

    var lessWidth = frame.marginWidth  - border * 3;
    return lessWidth;
}


function lessHeight() {

    var lessHeight = frame.marginHeight  - border * 3 ;
    return lessHeight;
}


function buildHolders() {

    //Order is Important
    horizontalDimensionsHolder();
    verticalDimensionsHolder();
    windowsHolder();
    verticalTotalDimensionHolder();
    horizontalTotalDimensionHolder();
}


function horizontalDimensionsHolder() {

    jQuery('<div/>', {
        id: 'topDimension'
    }).append( $('<div>', {
        id: 'horizontalSizes',
        class: 'sizes',
        width: frame.width + border*3
    })).appendTo('#windowBuilder');
}


function verticalDimensionsHolder() {

    jQuery('<div/>', {
        id: 'verticalSizes',
        class: 'sizes',
        height: frame.height
    }).appendTo('#windowBuilder');
}


function windowsHolder() {

    jQuery('<div/>', {
        id: 'mainFrame',
        width: frame.width - lessWidth(),
        height: frame.height - lessHeight()
    }).appendTo('#windowBuilder');
}


function verticalTotalDimensionHolder() {

    jQuery('<div/>', {
        id: 'totalVertical',
        class: 'totalVerticalDimension',
        width: 100,
        height: frame.height
    }).appendTo('#windowBuilder');
}


function horizontalTotalDimensionHolder() {

    jQuery('<div/>', {
        id: 'bottomDimension'
    }).appendTo('#windowBuilder');
}