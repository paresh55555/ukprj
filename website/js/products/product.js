'use strict';

function updateCssForViewQuote() {

    var tab = getParameter('tab');
    if (tab === 'viewQuote' || tab == 'viewOrder' || tab == 'viewAccountingOrder' || tab == 'viewProductionOrder') {
        $('#mainContent').css({"margin-top": "0px"});
    }
    //$('#preMainContent').show();

}


function loadProducts() {

    startSpinner();
    tab = 'home';
    selectTab('#door');
    formStatus = formStatusDefaults();
    resetUIForTopNav();
    updateFormStatus();

    var html = productsHTML();

    $('#mainContent').html(html);
    $('#preMainContent').show();
    //$('#mainContent').css({"margin-top": "144px"});

    updateCssForViewQuote();

    $('#headerRightMenu').html(user.name);

    // updateFormPrice();
    addFooter();

    var myPromise = getProducts();

    myPromise.done(function (products) {


        var productsHTML = buildProducts(products);


        $('#doors').html(productsHTML);
        $('.productButton').unbind("click").bind("click", selectModule);

        showContentAndSaveState();
    });
}


function resetUIForTopNav() {
    $('#footerPrice').hide();
    $('#workingOnTitle').html('');
    $('#headerStartOver').hide();
    $('#headerStartOverImage').hide();
    $('#preMainContent').html('');
}


function productsHTML() {

    var html = '' +
        '<div id="chooseProduct">' +
        '   <div class="title">Select a System Type:</div>' +
        '   <div id ="doors" class="sectionBlock">' +
        '   </div>' +
        '</div>' +
        '<div id="copyWriteFooter"></div>' +
        '<div class="spacer"></div>';

    return html;
}


function makeRowGroup(productGroup) {

    var row = '' +
        '<div class="moduleRowGroup"> ' +
        '   ' + productGroup +
        '</div>';

    return row;
}


function buildProducts(products) {

    var html = '';
    var productGroup = '';
    var count = 0;
    for (var a = 0; a < products.length; a++) {
        count++;
        var product = buildSingleProduct(products[a]);
        productGroup = productGroup + product;
        if (count == siteDefaults.numberOfProductsPerLine) {
            html = html + makeRowGroup(productGroup);
            count = 0;
            productGroup ='';
        }
    }

    if (!empty(productGroup)) {
        html = html + makeRowGroup(productGroup);
    }

    
    return html;
}


function buildSingleProduct(product) {

    var image = product.url;

    if (!empty(product.tempUrl)) {
        image = product.tempUrl;
    }

    var numberAcrossClass = numberOfProductsAcrossClass();

    var html = '' +
        '   <div class="' + numberAcrossClass + '">' +
        '       <div class="productButton" ' + cssBorder + '  id="' + product.title + '-' + product.moduleID + '">' +
        '           <div class="productImage" ><img title="' + product.title + '" src="' + image + '" width="250" height="250"></div>' +
        '           <div class="productLabel">' + product.title + '</div>' +
        '       </div>' +
        '       <div class="productNote1" ' + cssFont + ' >' + product.infoLineOne + '</div>' +
        '       <div class="productNote2">' + product.infoLineTwo + '</div>' +
        '       <div class="productNote3">' + product.description + '</div>' +
        '   </div>';


    return html;
}


function numberOfProductsAcrossClass() {

    if (siteDefaults.numberOfProductsPerLine == '2') {
        return 'moduleTwoAcross';
    }

    return 'moduleThreeAcross';
}


function loadViewCustomers() {


    setTab('viewCustomers');
    selectTab('#viewCustomers');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();

    updateFormStatus();
    $('#preMainContent').html("");
    $('#mainContent').html("Coming Soon -- Customers");
    showContentAndSaveState();

}

function loadCommunications() {


    setTab('communications');
    selectTab('#communications');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();

    updateFormStatus();

    $('#mainContent').html("Coming Soon - Communications (Or Maybe not)");
    $('#preMainContent').html("");
    showContentAndSaveState();

}
function selectModule() {
    var idTag = $(this).attr('id');
    var idArray = idTag.split('-');
    resetCartAndForm();
    writeLocalStorageWithKeyAndData('quoteState', {'started':true});
    renderModuleWithTitle(idArray[1], idArray[0]);
}


function renderModuleWithTitle(id, title) {


    formStatus.module = id;
    formStatus.moduleTitle = title;

    startSpinner();

    $.when(getModuleOptions()).done(function (moduleInfoFromApi) {

        moduleInfo = moduleInfoFromApi;

        var newValues = JSON.parse(moduleInfo.defaultFormChoices)
        jQuery.extend(formStatus, newValues);

        formStatus.showNoneOnCart = moduleInfo.showNoneOnCart;
        formStatus.materialType = moduleInfo.type;
        formStatus.panelOptions = JSON.parse(JSON.stringify(moduleInfo.panelOptions));
        formStatus.windowsOnly = moduleInfo.windowsOnly;

        writeFormStatus();
        writeModuleInfo();

        var quoteID = getParameter('quote');
        var orderID = getParameter('order');

        if (!empty(quoteID)) {
            trackAndDoClick('viewQuote', '&quote=' + quoteID + '&edit=item&cartItem=formStatus&secondTab=' + moduleInfo.firstTab);
        } else if (!empty(orderID)) {
            trackAndDoClick('viewOrder', '&order=' + orderID + '&edit=item&cartItem=formStatus&secondTab=' + moduleInfo.firstTab);
        } else {
            trackAndDoClick(moduleInfo.firstTab);
        }
    });
}


function getModuleInfoFromAPI() {

    if (typeof formStatus == 'undefined') {
        trackAndDoClick('home');
    }
    if (typeof formStatus == 'undefined') {
        trackAndDoClick('home');
    }

    var url = "https://" + apiHostV1 + "/api/moduleInfo?module=" + formStatus.module + addSite();
    var myPromise = $.getJSON(url, function (apiJSON) {

        module = apiJSON;

        var options = module.panelOptions;
        var newOptions = [];
        for (var a = 0; a < options.length; a++) {
            if (options[a].show == 0) {
                formStatus[options[a].formStatusName] = options[a].default;
            } else {
                newOptions.push(options[a]);
            }
        }
        module.panelOptions = newOptions;
        moduleInfo = module;
        formStatus.subModules = module.subModules;

        writeFormStatus();
        writeLocalStorageWithKeyAndData("moduleInfo", moduleInfo);
    });

    return myPromise;
}


function setDefaultModuleChoices() {

    if (!empty(moduleInfo.defaultOptions)) {
        var options = JSON.parse(moduleInfo.defaultOptions);
        if (!empty(options)) {

            //TODO -- hack and needs updating for entire site
            //formStatus.chooseFrameSection = "frame-"+options.frame;
            setFormOptionIfNotSet('chooseTrackSection', "track-" + options.track);
            setFormOptionIfNotSet('glassChoices', "glass-" + options.glassChoice);
        }
    }
}


function setFormOptionIfNotSet(option, value) {

    if (empty(formStatus[option])) {
        formStatus[option] = value;
    }

}

