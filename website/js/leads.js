'use strict';

var salesPeople;

function loadLeads() {

    setTab('viewLeads');
    selectTab('#viewLeads');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();

    resetUIForTopNav();

    updateFormStatus();

    addLeadSearchBox();
    $('#search').unbind("keyup").bind("keyup", leadSearchDelay);

    loadLeadsSearch();
}

function addLeadSearchBox() {

    var searchBox = buildSearchBox();
    $('#preMainContent').html( searchBox);

}


function leadSearchDelay() {

    delay(function () {
        loadLeadsSearch('yes');
    }, 300);

}


function loadLeadsSearch(newSearch) {



    var url = "https://" + apiHostV1 + "/api/salesperson?" + authorizePlusLeads() + addSite();
    $.getJSON(url, function (apiJSON) {

        validate(apiJSON);
        var salesPeopleList = apiJSON;

        salesPeople  = '';

        for (var i = 0; i < salesPeopleList.length; i++) {
            var salesPerson = salesPeopleList[i];
            salesPeople += '<option value="' + salesPerson.id + '">' + salesPerson.name + '</option>';
        }

        var url = "https://" + apiHostV1 + "/api/leads?" + authorizePlusLeads() + pageLimitText(newSearch) + addSite() + addSearch();
        $.getJSON(url, function (apiJSON, status, xhr) {

            if(!validate(apiJSON))return;


            var html = buildViewLeads(apiJSON);
            $('#mainContent').html( html);
            $('.leadLink').unbind("click").bind("click", leadActions);
            $('.quoteIcons').unbind("click").bind("click", leadActions);
            $('#cartButton').hide();


            var total = xhr.getResponseHeader('Total');

            setPaginationWithSelectorAndTotal('#pagination', total, newSearch);
            $('.paginationBoxLeads').css("margin-top","0px");

            showContentAndSaveState();
        });


    });

}

function loadAssignedLeads() {


    setTab('viewAssignedLeads');
    selectTab('#viewAssignedLeads');

    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();
    $('#cartButton').hide();

    resetUIForTopNav();

    updateFormStatus();

    addLeadSearchBox();
    $('#search').unbind("keyup").bind("keyup", assignedLeadSearchDelay);


    assignedLoadLeadsSearch();


}


function assignedLeadSearchDelay() {

    delay(function () {
        assignedLoadLeadsSearch('yes');
    }, 300);

}


function assignedLoadLeadsSearch(newSearch) {



    var url = "https://" + apiHostV1 + "/api/leads/transferred?" + authorizePlusLeads() + pageLimitText(newSearch) + addSite() + addSearch();

    $.getJSON(url, function (apiJSON, status, xhr) {

        validate(apiJSON);
        var html = buildAssignedLeads(apiJSON);
        $('#mainContent').html(html);
        $('.quoteIcons').unbind("click").bind("click", leadActions);

        var total = xhr.getResponseHeader('Total');

        setPaginationWithSelectorAndTotal('#pagination', total, newSearch);
        $('.paginationBoxLeads').css("margin-top","0px");

        showContentAndSaveState();
    });

}




function loadViewQuoteForLeads() {

    var quoteLookup = getParameter('quote');
    $('#topNav').hide();
    $('#productNav').show();
    $('#footer').hide();
    $('#searchBoxQuote').hide();


    var url = "https://" + apiHostV1 + "/api/quotes/" + quoteLookup + "/guest?" + authorizePlusLeads() + addSite();


    $.getJSON(url, function (apiJSON) {

        quote = apiJSON;

        if (!quote) {
            trackAndDoClick('viewQuotes', "&blankQuote");
            return;
        }
        //cart = JSON.parse(quote.Cart);
        //writeCart(cart);
        //writeQuoteID(quote.id);

        var params = {};
        params.quote = quote;
        params.customerID = quote.Customer;
        params.selector = "#preMainContent";

        checkEmptySalesDiscount();

        loadSoloCart();


        var quoteNumber = "SQ" + params.quote.id;

        $('#workingOnTitle').html('Viewing Quote: ' + makeQuoteTitle(quote));

        $('#footerPrice').html(currency + formatMoney(params.quote.total));
        $('#footerRight').html('');

        $('#cartButton').hide();
        processStartOverButtonVisibility(false);
        $('.guestEditBox').hide();
        //$('.cartIcons').hide();

        showContentAndSaveState();
        //updateCssForViewQuote();


    });

}

function leadActions() {

    var thisSelector = $(this);
    var id = $(thisSelector).attr('id');

    var quoteID = splitForFirstOption(id);
    var action = splitForSecondOption(id);

    if (action === 'delete') {
        deleteLead(quoteID);
    }
    if (action === 'revoke') {
        revokeLead(quoteID);
    }
    if (action === 'view' || action === 'viewLink') {
        trackAndDoClick("viewLeadQuote", "&quote=" + quoteID);
    }

}

function deleteLead(quoteID) {

    if (confirm("Are you sure you want to delete this quote?") == true) {
        var quote = getParameter('quote');
        var url = "https://" + apiHostV1 + "/api/leads/" + quoteID + "?" + authorizePlusLeads() + addSite();
        $.ajax({
            url: url,
            type: "DELETE",
            success: function (resultJson) {
                var result = JSON.parse(resultJson);
                if (result.success == "true") {
                    trackAndDoClick(tab)
                }
                trackAndDoClick(tab)
            }
        });

    }


}

function revokeLead(quoteID) {


    if (confirm("Are you sure you want to revoke this lead?") == true) {
        var quote = getParameter('quote');
        var url = "https://" + apiHostV1 + "/api/leads/" + quoteID + "/revoke?" + authorizePlusLeads() + addSite();
        $.ajax({
            url: url,
            type: "PUT",
            success: function (resultJson) {
                var result = JSON.parse(resultJson);

                if (result.success == "true") {
                    trackAndDoClick(tab)
                }
                trackAndDoClick(tab)

            }
        });

    }


}



function buildViewLeads(data) {

    var row;
    var rows = '';


    for (var a = 0; a < data.length; a++) {
        var quote = data[a];
        row = buildLeadsRow(quote);
        rows = rows + row;
    }

    var html = buildLeadsTable(rows, data);

    return html;

}

function buildLeadsRow(quote) {

    var checkBox = buildCheckBoxPullDown(quote.number);

    var row = '' +
        '<div class="leadRow">' +
        '   <div class="leadIcons borderLeftTop">' +
//        '       <img id="' + quote.number + '-view" title="View" class="quoteIcons" src="/images/quotes/icon-view.svg">' +
        '       <img id="' + quote.number + '-delete" title="Delete" class="quoteIcons" src="/images/quotes/icon-delete.svg">' +
        '   </div>' +
        '   <div id="' + quote.number + '-viewLink" title="View" class="leadQuoteCell borderLeftTop leadLink">' + makeQuoteTitle(quote) + '</div>' +
        '   <div class="leadFullName borderLeftTop" >' + quote.name + '</div>' +
        '   <div class="leadPhone borderLeftTop">' + quote.phone + '</div>' +
        '   <div class="leadEmail borderLeftTop">' + quote.email + '</div>' +
        '   <div class="leadQty borderLeftTop">' + quote.quantity + '</div>' +
        '   <div class="leadCell5 borderLeftTop">' + currency + quote.total + '</div>' +
        '   <div class="leadCell5 borderLeftTop">' + quote.quoted + '</div>' +
        '   <div class="leadZip borderLeftTop">' + quote.zip + '</div>' +
        '   <div class="leadCounty borderLeftTop">' + quote.county + '</div>' +
        '   <div class="leadState borderLeftTop">' + quote.state + '</div>' +
        '   <div id="sq-' + quote.number + '" class="leadsLastColumn borderLeftRightTop" >' + checkBox + '</div>' +
        '</div>';

    return row;
}

//'   <div id="sq-' + quote.number + '"> sfsfsf</div>' +
//'   <div id="test" class="transferListCell">' + salesPeopleList + '</div>' +
//class="leadsTransfer"

function buildLeadsTable(rows, data) {


    var salesPeopleList = buildSalesPullDown();

    var pagination = buildPaginationBox();

    var html = '' +
            '<div id="leadSalesPersons"><div id="transferToText">Transfer To:</div>' + salesPeopleList + '<div id="transferTo"></div></div>' +
        '<div class="leadRowTitle">' +
        '   <div id="actions" class="leadIconsText borderLeftTop sortable" ></div>' +
        '   <div id="id" class="leadQuoteCell borderLeftTop sortable" >Quote #</div>' +
        '   <div id="name2" class="leadFullName borderLeftTop sortable" >Name</div>' +
        '   <div id="phone" class="leadPhone borderLeftTop sortable" >Phone</div>' +
        '   <div id="email" class="leadEmail borderLeftTop sortable" >Email</div>' +
        '   <div id="itemQty" class="leadQty borderLeftTop sortable" >Qty.</div>' +
        '   <div id="amount" class="leadCell5 borderLeftTop sortable" >Amount</div>' +
        '   <div id="quoted" class="leadCell5 borderLeftTop sortable" >Quoted</div>' +
        '   <div id="zip" class="leadZip borderLeftTop sortable" >' + capitalizeFirstLetter(zipShort) + ' </div>' +
        '   <div id="county" class="leadCounty borderLeftTop sortable" >County</div>' +
        '   <div id="state" class="leadState borderLeftTop sortable" >ST</div>' +
        '   <div id="transfer" class="leadsLastColumn borderLeftRightTop sortable" > &nbsp; </div>' +
        '</div>';

    var lastRow = '<div class="finalRow"></div>';

    return  pagination +
            '<div id="leadsNew">' +
                html +
                rows +
                lastRow +
            '</div>';
}

function beginTransferLead() {

    var salesPerson = $('#pullDown').val();
    var html = '<div class="transferText" onClick="transferLead(' + salesPerson + ')" >Transfer</div>';
    $('#transferTo').html(html);

}
function buildCheckBoxPullDown(id) {

    var html = '<input id="' + id +'" type="checkbox" >';

    return html;

}
function buildSalesPullDown() {

    var html = '<select id="pullDown" class="salesPullDown" onchange="beginTransferLead()">' +
        '<option value="">  </option>';

    html += salesPeople;

    html += '</select>';

    return html;

}

function doLeadTransferPromise (lead,salesPerson) {

    var url = "https://" + apiHostV1 + "/api/leads/" + lead + "/transfer/" + salesPerson + "?" + authorizePlusLeads() + addSite();

    var promise = $.getJSON(url, function (apiJSON) {

        validate(apiJSON);
        //trackAndDoClick('viewLeads');
    });

    return promise;

}

function transferLead(salesPerson) {


    $( "input:checked" ).each(function() {

       var lead =  $( this ).attr( "id" ) ;

        $.when(
            doLeadTransferPromise(lead, salesPerson)
        ).done(function () {

        });

    });


    trackAndDoClick('viewLeads');


}


function buildAssignedLeads(data) {

    var row;
    var rows = '';

    for (var a = 0; a < data.length; a++) {
        var quote = data[a];

        row = buildAssignedLeadsRow(quote);
        rows = rows + row;
    }

    var html = buildAssignedLeadsTable(rows, data);

    return html;

}

function buildAssignedLeadsRow(quote) {

    var row = '' +
        '<div class="leadRow">' +
        '   <div class="leadQuoteCell borderLeftTop">' + makeQuoteTitle(quote) + '</div>' +
        '   <div class="assignedLeadFullName borderLeftTop" >' + quote.name + '</div>' +
        '   <div class="assignedLeadZip borderLeftTop">' + quote.zip + '</div>' +
        '   <div class="leadPhone borderLeftTop">' + quote.phone + '</div>' +
        '   <div class="leadQty borderLeftTop">' + quote.quantity + '</div>' +
        '   <div class="leadCell5 borderLeftTop">' + currency + quote.total + '</div>' +
        '   <div class="leadCell5 borderLeftTop">' + quote.quoted + '</div>' +
        '   <div class="leadsSalesPerson borderLeftTop">' + quote.salesPersonName + '</div>' +
        '   <div id="test" class="assignedleadIcons borderLeftRightTop">' +
        '       <img id="' + quote.number + '-view" title="View" class="quoteIcons" src="/images/quotes/icon-view.svg">' +
        '       <img id="' + quote.number + '-delete" title="Delete" class="quoteIcons" src="/images/quotes/icon-delete.svg">' +
        '       <img id="' + quote.number + '-revoke" title="Revoke" class="quoteIcons" src="/images/leads/revoke.png">' +
        '   </div>' +
        '</div>';


    return row;
}

function buildAssignedLeadsTable(rows, data) {

    var pagination = buildPaginationBox();

    var html = '' +
        '<div class="quoteRowTitle leads">' +
        '   <div id="id" class="leadQuoteCell borderLeftTop " >Quote #</div>' +
        '   <div id="name2" class="assignedLeadFullName borderLeftTop " >Name</div>' +
        '   <div id="zip" class="assignedLeadZip borderLeftTop " >' + capitalizeFirstLetter(zipShort) + ' </div>' +
        '   <div id="phone" class="leadPhone borderLeftTop " >Phone</div>' +
        '   <div id="itemQty" class="leadQty borderLeftTop " >Qty.</div>' +
        '   <div id="amount" class="leadCell5 borderLeftTop " >Amount</div>' +
        '   <div id="quoted" class="leadCell5 borderLeftTop " >Quoted</div>' +
        '   <div id="actions" class="leadsSalesPerson borderLeftTop " >Sales Person</div>' +
        '   <div id="transfer" class="assignedleadIconsText borderLeftRightTop " > Action </div>' +
        '</div>';

    var lastRow = '<div class="finalRow leads"></div>';


    return pagination + html + rows + lastRow;
}