'use strict';

function loadCustomers(customerID) {
    setTab('customer');

    var customerID = getParameter('customerID');
    var show = getParameter('show');

    //TODO --refactor for UK
    // var url = 'html/customers1.html';
    // if (isUK()) {
    //     url = 'html/customersUK1.html';
    // }

    var html = customersHTML(show);
// html ='';
    $('#mainContent').html(html);
    $('#preMainContent').html('');
    $('#topNav').css({"text-align": "center"});
    $('.finishMenuItem').unbind("click").bind("click", selectCustomerTab);
    $('#search').keyup(customerSearchDelay);
    
    $('#saveNewCustomerToQuote').unbind("click").bind("click", saveNewCustomerToQuote);
    $('#saveNewCustomerToNewQuote').unbind("click").bind("click", saveNewCustomerToNewQuote);


    $('#footerPrice').hide();

    if (show == 'search') {
        loadPreviousCustomer();
        customerSearch(this);
    }

    showContentAndSaveState();

}


function customersHTML(show) {

    var chooseCustomer = chooseCustomersHTML(show);
    var addCustomer = addCustomerHTML();
    var searchCustomer = searchCustomerHTML();
    var newQuote = getParameter('newQuote');

    var jsToUse = "saveNewCustomerToQuote";
    if (newQuote == 'yes') {
        jsToUse = "saveNewCustomerToNewQuote";
    }

    var nextImage = nextSVG();
    var html = ' ' +
        '' + chooseCustomer +
        '' + addCustomer +
        '' + searchCustomer +
        '<div id="saveQuote"> ' +
        '   <div id="' + jsToUse +'" class="nextButton" > ' +
        '       <div class="nextTitle" ' + cssFont + '>Attach To Quote</div> ' +
        '       <div class="nextImage"> ' +
        '           ' + nextImage +
        '       </div> ' +
        '   </div> ' +
        '</div> ' +
        '<div class="spacer"></div>';

    return html;
}


function chooseCustomersHTML(show) {

    var previous = '';
    var newCustomer = 'myGreen'
    if (show == 'search') {
        previous = 'myGreen';
        newCustomer = '';
    }

    var closeButton = '           <div id="close" class="finishMenuItem" data-finish="exterior">Close</div> ';
    if (empty(quote.id)) {
        closeButton = '';
    }

    var html = '' +
        '<div id="chooseCustomer"> ' +
        '   <div class="title">Choose Customer Type</div> ' +
        '   <div class="sectionBlock"> ' +
        '       <div id="customerTypes" class="finishMenu"> ' +
        '           <div id="previousCustomer" class="finishMenuItem ' + previous + '" data-finish="no">Search Customers</div> ' +
        '           <div id="newCustomer" class="finishMenuItem ' + newCustomer + '" data-finish="exterior">New Customer</div> ' +
        '           ' + closeButton +
        '       </div> ' +
        '   </div> ' +
        '</div> ';

    return html;
}


function selectCustomerTab() {

    var id = $(this).attr('id');

    if (id == 'previousCustomer') {
        loadPreviousCustomer();
    } else if (id == 'close') {
        trackAndDoClick("viewQuote", "&quote=" + quote.id);
    } else {
        loadNewCustomer();
    }

}

function loadNewCustomer() {

    $('#addNewCustomer').show();
    $('#searchCustomer').hide();
    $('#saveQuote').show();

    $('#newCustomer').addClass('myGreen');
    $('#previousCustomer').removeClass('myGreen');


}


function customerSearchDelay() {

    var selector = this;

    delay(function () {
        customerSearch(selector);
    }, 300);

}

function customerSearch() {

    var val = $('#search').val();
    var json = JSON.stringify(val);

    var url = "https://" + apiHostV1 + "/api/search/customer?" + authorizePlusSalesPerson() + addSite();
    $.post(url, json, function (data) {

        var response = JSON.parse(data);
        displayCustomerSearch(response);
    });
}

function displayCustomerSearch(data) {

    var rows = buildCustomerRow(data);

    var html = buildCustomerColumnTitles(rows);


    $('#searchResults').html(html);
    $('.attachToQuote').unbind("click").bind("click", attachToQuote);

}

function attachToQuote(newQuote) {

    var id = $(this).attr('id');
    var checkbox = $("#checkbox-" + id).is(':checked');

    getCustomerAPI(id).done(function (data) {

        data.billing_contact = data.firstName + ' ' + data.lastName;
        data.billing_address1 = data.billingAddress1;
        data.billing_address2 = returnBlankIfEmpty(data.billingAddress2);
        data.billing_city = data.billingCity;
        data.billing_state = data.billingState;
        data.billing_zip = data.billingZip;
        data.billing_email = data.billingEmail;
        data.billing_phone = data.billingPhone;

        if (checkbox) {
            data.shipping_contact = data.firstName + ' ' + data.lastName;
            data.shipping_address1 = data.billingAddress1;
            data.shipping_address2 = data.billingAddress2;
            data.shipping_city = data.billingCity;
            data.shipping_state = data.billingState;
            data.shipping_zip = data.billingZip;
            data.shipping_email = data.billingEmail;
            data.shipping_phone = data.billingPhone;
        } else {
            data.shipping_contact = '';
            data.shipping_address1 = '';
            data.shipping_address2 = '';
            data.shipping_city = '';
            data.shipping_state = '';
            data.shipping_zip = '';
            data.shipping_contact = '';
            data.shipping_email = '';
            data.shipping_phone = '';
        }

        delete data.firstName;
        delete data.lastName;
        delete data.phone;
        delete data.companyName;
        delete data.sales_person_id;
        delete data.billingAddress1;
        delete data.billingAddress2;
        delete data.billingCity;
        delete data.billingState;
        delete data.billingZip;
        delete data.billingName;
        delete data.email;
        delete data.phone;
        delete data.billingEmail;
        delete data.billingPhone;

        data.sales_person_id = user.id;

        var json = JSON.stringify(data);

        if (empty(quote.id)) {

            addQuoteAndAddress(id, data, 'yes');

        } else {
            attachAddressToQuote(quote.id, json, id);
        }
    })
}

function attachAddressToQuote(quote_id, json, customer_id) {

    attachAddressesToQuoteAPI(quote_id, json, customer_id).done(function () {
        // if (newQuote == 'yes') {
        //     createQuote(id);
        // } else {
        trackAndDoClick("viewQuote", "&quote=" + quote.id);
        // }
    });

}


function cityStateZipHTML(row) {

    var cityStateZip = returnBlankIfEmpty(row.city) + ', ' + returnBlankIfEmpty(row.state) + ' ' + returnBlankIfEmpty(row.zip);

    return cityStateZip;
}


function buildCustomerRow(data) {

    var html = '';
    for (var a = 0; a < data.length; a++) {
        var row = data[a];

        if (!empty(quote.Customer)) {
            if (quote.Customer.customerID == row.id) {
                continue;
            }
        }

        var secondAddress = '';

        var secondAddressDelete = '';
        if (!empty(row.address2)) {
            secondAddress = '       <div class="customerSearchRowResults">' + returnBlankIfEmpty(row.address2) + '</div>';
            secondAddressDelete = returnBlankIfEmpty(row.address2) + "\n";
        }

        html += '' +
            '<div class="customerSearchRow">' +
            '   <div class="customerSearchRowLeft">' +
            '       <div class="customerSearchRowResultsBold"><b>' + returnBlankIfEmpty(row.firstName) + ' ' + returnBlankIfEmpty(row.lastName) + '</b></div>' +
            '       <div class="customerSearchRowResults">' + returnBlankIfEmpty(row.address1) + '</div>' +
            '       ' + secondAddress +
            '       <div class="customerSearchRowResults">' + cityStateZipHTML(row) + '</div>' +
            '       <div class="customerSearchRowResults">' + returnBlankIfEmpty(row.phone) + '</div>' +
            '       <div class="customerSearchRowResults">' + returnBlankIfEmpty(row.email) + '</div>' +
            '       <div class="customerSearchRowResultsButtons"><div class="customerEdit" onclick="newEditCustomerWithReturn(' + row.id + ')" >Edit</div></div>' +
            '       <div class="customerSearchRowResultsButtons"><div class="customerEdit" onclick="deleteCustomerWithReturn(' + row.id + ')" >Delete</div></div>' +
            '   </div>' +
            '   <div class="customerSearchRowRight">' +
            '       <div class="customerSearchRowResults"> <input id="checkbox-' + row.id + '" type="checkbox" name="useShippingAddress" value="yes"> Use for Shipping Address</div>' +
            '       <div id="' + row.id + '" class="attachToQuote myGreen"> Attach To Quote</div>' +
            '   </div>' +
            '</div>';
    }

    return html;
}


function buildCustomerColumnTitles(rows) {

    var html =
        '<div class="customerSearchTitle"> Search Results </div>' +
        rows;


    return html;

}


function loadPreviousCustomer() {

    $('#previousCustomer').addClass('myGreen');
    $('#newCustomer').removeClass('myGreen');
    $('#addNewCustomer').hide();
    $('#searchCustomer').show();
    $('#saveQuote').hide();

}

function loadEditCustomer() {
    setTab('customerEdit');
    startSpinner();
    var customerID = getParameter('customer');

    editCustomer(customerID);
}


function loadViewCustomer(customerID) {
    setTab('customerView');

    startSpinner();

    //var customerID = getParameter('customerID');
    var action = getParameter('action');

    if (action == "edit") {
        showCustomerEdit(quote.Customer.id);
    } else {
        showCustomer();
    }

}
function sameAsShipping() {

    if ($("#sameAsShipping").is(':checked')) {

        $('#billingAddress').hide();
    } else {
        $('#billingAddress').show();
    }
}


function showCustomerEdit(customer_id) {

    getCustomer(customer_id).done(function (customer) {

        var customerHtml = buildEditCustomer(customer);
        $('#mainContent').html(customerHtml);

        $('#sameAsShipping').unbind("click").bind("click", sameAsShipping);
        $('#preMainContent').html('');
        $('#footer').hide();
        $('.editButtons').unbind("click").bind("click", customerButtonsPress);

          // if (quote.Customer.sameAsShipping == '1') {
        //     $('#billingAddress').hide();
        // }

        showContentAndSaveState();
    })
}


function showConfirmCustomer() {

    var customerHtml = buildEditCustomer('confirmCustomer');
    $('#mainContent').html(customerHtml);
    $('#preMainContent').html('');
    $('#footer').hide();
    $('.editButtons').unbind("click").bind("click", customerButtonsPress);
    $('#SaveChangesAndConvert').unbind("click").bind("click", SaveChangesAndConvert);
    //
    //
    showContentAndSaveState();
    scrollTopOfPage();

}

function showCustomer() {

    var customerHtml = buildViewCustomer();
    $('#mainContent').html(customerHtml);
    $('#footer').hide();
    formatHeaderForCustomer();
    $('.editButtons').unbind("click").bind("click", customerButtonsPress);

    showContentAndSaveState();

}

function customerButtonsPress() {

    var id = $(this).attr('id');
    var quoteID = splitForFirstOption(id);
    var action = splitForSecondOption(id);
    var customerID = getParameter('customerID');
    var returnOrder = getParameter('returnOrder');

    if (action === 'Close' || action === 'CancelSaveChanges') {

        if (isProduction()) {
            trackAndDoClick("viewProductionOrder", "&order=" + quote.id);
        } else if (returnOrder == 'viewOrder') {
            trackAndDoClick(returnOrder, "&order=" + quote.id);
        } else {

            trackAndDoClick("viewQuote", "&quote=" + quote.id);
        }
    }

    if (action === 'ClosePayment') {
        if (isProduction()) {
            trackAndDoClick("viewProductionOrder", "&order=" + quoteID);
        } else if (isAccounting()) {
            trackAndDoClick("viewAccountingOrder", "&order=" + quoteID);
        } else {
            trackAndDoClick("viewOrder", "&order=" + quoteID);
        }

    }

    if (action === 'SavePayment') {
        savePayment();
    }

    if (action === 'PaymentConfirmed') {
        paymentConfirmed();
    }

    if (action == 'Cancel') {

        if (isProduction()) {
            trackAndDoClick("viewProductionOrder", "&order=" + quote.id);
        }
        else if (isAccounting()) {
            trackAndDoClick("viewAccountingOrder", "&order=" + quote.id);
        }
        else if (returnOrder == 'viewOrder') {
            trackAndDoClick(returnOrder, "&order=" + quote.id);
        }
        else if (tab == 'customer') {
            trackAndDoClick(tab, "&show=search");
        }
        else {

            trackAndDoClick("viewQuote", "&quote=" + quote.id);
        }

    }

    if (action === 'Edit' || action === 'EditPayment') {
        trackAndDoClick("customerView", "&action=edit");
    }

    if (action === 'SaveChanges') {

        var action = function () {

            if (isProduction()) {
                trackAndDoClick("viewProductionOrder", "&order=" + quote.id);
            }
            else if (isAccounting()) {
                trackAndDoClick("viewAccountingOrder", "&order=" + quote.id);
            }
            else if (returnOrder == 'viewOrder') {
                trackAndDoClick(returnOrder, "&order=" + quote.id);
            }
            else if (tab == 'customer') {
                trackAndDoClick(tab, "&show=search");
            }
            else {
                trackAndDoClick("viewQuote", "&quote=" + quote.id);
            }
        };

        saveCustomerUpdateWithAction(action, 'no');
    }


    if (action === 'SaveChangesAttach') {

        var action = function () {

            if (isProduction()) {
                trackAndDoClick("viewProductionOrder", "&order=" + quote.id);
            }
            else if (isAccounting()) {
                trackAndDoClick("viewAccountingOrder", "&order=" + quote.id);
            }
            else if (returnOrder == 'viewOrder') {
                trackAndDoClick(returnOrder, "&order=" + quote.id);
            }
            else if (tab == 'customer') {
                trackAndDoClick(tab, "&show=search");
            }
            else {
                trackAndDoClick("viewQuote", "&quote=" + quote.id);
            }
        };

        saveCustomerUpdateWithAction(action, 'yes');
    }
}


function SaveChangesAndConvert() {

    if (siteDefaults.salesFlow == 2) {
        SaveChangesAndConvertFlow2();
    } else {
        var action = convertQuoteToOrder;
        if (siteDefaults.salesFlow == 3) {
            action = convertQuoteToOrderConfirmPayment;
        }
        saveCustomerUpdateWithAction(action);
    }
}

function saveCustomerUpdateWithAction(action, attach) {

    var customerFields = {};
    var allInputs = $(":input");

    var requireFields = {
        'firstName': 'yes', 'lastName': 'yes', 'email': 'yes', 'zip': 'yes', 'jobAddress1': 'yes',
        'jobCity': 'yes', 'jobState': 'yes', 'jobZip': 'yes'
    };

    var requireFields = {
        'leadSource': 'yes'
    };

    var finalCheck = true;
    for (var a = 0; a < allInputs.length; a++) {
        var attr = allInputs[a];

        if (!empty(requireFields[attr.name])) {
            var status = validateField(attr);
            if (!status) {
                finalCheck = false;
            }
        }
        customerFields[attr.name] = escapeHtml(attr.value);
    }

    if (!finalCheck) {
        scrollToError($('#errorMessageCustomer'));
        return;
    }

    var quoteAddress = {};

    if ($("#checkbox-"+customerFields.id).is(':checked')) {
        quoteAddress.shipping_contact = customerFields.billingName;
        quoteAddress.shipping_address1 = customerFields.billingAddress1;
        quoteAddress.shipping_address2 = customerFields.billingAddress2;
        quoteAddress.shipping_city = customerFields.billingCity;
        quoteAddress.shipping_state = customerFields.billingState;
        quoteAddress.shipping_zip = customerFields.billingZip;
        quoteAddress.shipping_email = customerFields.billingEmail;
        quoteAddress.shipping_phone = customerFields.billingPhone;
        customerFields.sameAsShipping = 1;
    } else {
        customerFields.sameAsShipping = 0;
    }


    quoteAddress.billing_contact = customerFields.billingName;
    quoteAddress.billing_address1 = customerFields.billingAddress1;
    quoteAddress.billing_address2 = customerFields.billingAddress2;
    quoteAddress.billing_city = customerFields.billingCity;
    quoteAddress.billing_state = customerFields.billingState;
    quoteAddress.billing_zip = customerFields.billingZip;
    quoteAddress.billing_email = customerFields.billingEmail;
    quoteAddress.billing_phone = customerFields.billingPhone;
    quoteAddress.sales_person_id = user.id;

    delete(quoteAddress.useShippingAddress);
    delete(customerFields.useShippingAddress);

    var quoteAddressJson = JSON.stringify(quoteAddress);

    var json = JSON.stringify(customerFields);



    $.ajax({
        url: "https://" + apiHostV1 + "/api/customers/" + customerFields.id + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
        data: json,
        type: "PUT",
        success: function (result) {

            var response = jQuery.parseJSON(result);
            if (response.status == "Success") {

                if (attach == 'yes') {
                    attachAddressesToQuoteAPI(quote.id, quoteAddressJson, customerFields.id).done(function () {
                        action();
                    });
                } else {
                    action();
                }


                updateCustomerPhonePipeliner(customerFields.phone);

                // if (empty(quote.prefix)) {
                //     quote.prefix = user.prefix;
                // }
                // quote.Customer = customerFields;
                // quote.postfix = 'S';
                //
                // writeQuote(quote);
                //
                // quote = removeNoCart(quote);
                // var json = JSON.stringify(quote);
                //
                //
                // $.ajax({
                //     url: "https://" + apiHostV1 + "/api/quotes/" + quote.id + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken(),
                //     data: json,
                //     type: "PUT",
                //     success: function (result) {
                //
                //         var response = jQuery.parseJSON(result);
                //         if (response.status == "Success") {
                //
                //             action();
                //
                //         } else {
                //             alert("Quotes Save Failed");
                //         }
                //     }
                // });


            } else {
                alert("Customer Save Failed");

            }
        }
    });
}


function updateCustomerPhonePipeliner(phone) {

    if (!empty(user.pipelinerId)) {
        var lead = {"ownerId":user.pipelinerId, "prefix":user.prefix, "quoteId": quote.id, "phone": phone}

//        updatePipelinerAPI(lead).done(function (result) {
//        })
    }
}

function formatHeaderForCustomer() {
    processStartOverButtonVisibility(false);
}


// function getCustomer(id, callback) {
//
//     if (Boolean(id)) {
//         var url = "https://" + apiHostV1 + "/api/customers/" + id + '/?' + addSite();
//         $.getJSON(url, callback);
//     }
//     else {
//         callback('');
//     }
//
// }
function removeSelectState(state) {
    if (state == 'Select State') {
        state = '';
    }

    return state;
}

function newEditCustomerWithReturn(customer_id) {

    showCustomerEdit(customer_id);

    // startSpinner();
    // trackAndDoClick('customerEdit', '&customer=' + customerID + '&returnOrder=' + tab);

}

function buildAlertAddress(data) {

    var address2 = '';
    if (!empty(data.billingAddress2)) {
        address2 = data.billingAddress2 + "\n";
    }
    var address = '' +
        returnBlankIfEmpty(data.firstName) + ' ' + returnBlankIfEmpty(data.lastName) + '\n' +
        returnBlankIfEmpty(data.billingAddress1) + returnBlankIfEmpty(address2) +
        returnBlankIfEmpty(data.billingCity) + ', ' + returnBlankIfEmpty(data.billingState) + ' ' + returnBlankIfEmpty(data.billingZip) + '\n' +
        returnBlankIfEmpty(data.phone) + '\n' +
        returnBlankIfEmpty(data.email);

    return address;
}


function deleteCustomerWithReturn(customerID, customAction) {

    startSpinner();

    getCustomer(customerID).done(function (result) {

        var address = buildAlertAddress(result);
        stopSpinner();
        if (confirm(address + "\n\nAre you sure you want to delete this address?") == true) {
            startSpinner();
            deleteCustomerAPI(customerID).done(function (result) {
                customerSearch();
                stopSpinner();
            });
        }
    });
}


function editCustomerWithReturn() {

    trackAndDoClick('customerView', '&action=edit&returnOrder=' + tab);

}


function editCustomerWithReturn(customer) {

    trackAndDoClick('customerView', '&action=edit&returnOrder=' + tab);

}

function changeCustomerHTML() {

    var attach = '<div id="changeCustomer" onclick="trackAndDoClick(\'customer\')" class="changeCustomer myButton selectedButton">Attach Different Customer</div>';
    var edit = '<div id="changeCustomer" onclick="editCustomerWithReturn()" class="editCustomer myButton selectedButton">Edit</div>';

    var html = edit + attach;
    if (isAccounting() || isProduction()  || isSurvey()) {
        html = '';
    }

    return html;
}

function buildCustomerDetails(data) {
    if (!data) {
        return '';
    }

    data = validateCustomer(data);

    var id = getParameter('quote');

    var edit = '<div class="customerEdit" onclick="editCustomerWithReturn()" ' + cssFont + '> &nbsp;&nbsp;&nbsp;&nbsp;</div>';
    // var change = '<div class="customerEdit" onclick="trackAndDoClick(\'customer\')" ' + cssFont + '>change</div>';

    var jobState = removeSelectState(data.jobState);
    var billingState = removeSelectState(data.billingState);

    var html = '' +
        '<div class="customerBoxDetails">' +
        '   <div class="customerInfoTitle">Customer Info</div>' + edit +
        '   <div class="customerBoxDetailsLeft">Name:</div><div class="customerBoxDetailsRight">' + returnBlankIfEmpty(data.firstName) + ' ' + returnBlankIfEmpty(data.lastName) + '</div>' +
        '   <div class="customerBoxDetailsLeft">Phone:</div><div class="customerBoxDetailsRight">' + data.phone + '</div>' +
        '   <div class="customerBoxDetailsLeft">eMail:</div><div class="customerBoxDetailsRight">' + data.email + '</div>' +
        '   ' + changeCustomerHTML() +
        '</div>';

    var address = '' +
        '<div class="customerBoxDetails">' +
        '   <div class="customerBoxDetailsTitle">' + capitaliseFirstLetter(shipping) + ' Address</div>' +
        '   <div class="customerBoxDetailsLeft">Name:</div><div class="customerBoxDetailsRight">' + data.shippingName + '</div>' +
        '   <div class="customerBoxDetailsLeft">Address1:</div><div class="customerBoxDetailsRight">' + data.jobAddress1 + '</div>' +
        '   <div class="customerBoxDetailsLeft">Address2:</div><div class="customerBoxDetailsRight">' + data.jobAddress2 + '</div>' +
        '   <div class="customerBoxDetailsLeft">' + city + ':</div><div class="customerBoxDetailsRight">' + data.jobCity + '</div>' +
        '   <div class="customerBoxDetailsLeft">' + state + ':</div><div class="customerBoxDetailsRight">' + jobState + '</div>' +
        '   <div class="customerBoxDetailsLeft">' + zipText + ':</div><div class="customerBoxDetailsRight">' + data.jobZip + '</div>' +
        '   <div class="customerBoxDetailsLeft">Email:</div><div class="customerBoxDetailsRight">' + data.shippingEmail + '</div>' +
        '</div>' +
        '<div class="customerBoxDetails">' +
        '   <div class="customerBoxDetailsTitle">Billing Address</div>' +
        '   <div class="customerBoxDetailsLeft">Name:</div><div class="customerBoxDetailsRight">' + data.billingName + '</div>' +
        '   <div class="customerBoxDetailsLeft">Address1:</div><div class="customerBoxDetailsRight">' + data.billingAddress1 + '</div>' +
        '   <div class="customerBoxDetailsLeft">Address2:</div><div class="customerBoxDetailsRight">' + data.billingAddress2 + '</div>' +
        '   <div class="customerBoxDetailsLeft">' + city + ':</div><div class="customerBoxDetailsRight">' + data.billingCity + '</div>' +
        '   <div class="customerBoxDetailsLeft">' + state + ':</div><div class="customerBoxDetailsRight">' + billingState + '</div>' +
        '   <div class="customerBoxDetailsLeft">' + zipText + ':</div><div class="customerBoxDetailsRight">' + data.billingZip + '</div>' +
        '   <div class="customerBoxDetailsLeft">Email:</div><div class="customerBoxDetailsRight">' + data.billingEmail + '</div>' +
        '</div>';

    if (siteDefaults.salesFlow == 1) {
        address = '';
        html = html + leadInfoHTML();
    }

    return html + address;

}
function validateCustomer(data) {

    var values = ["lastName", "firstName", "phone", "email", "jobAddress1", "jobAddress2", "jobCity", "jobState", "jobZip",
        "billingAddress1", "billingAddress2", "billingCity", "billingState", "billingZip", "customerNotes", "billingEmail", "billingName", "shippingName", "shippingEmail"];

    if (empty(data)) {
        data = {};
    }

    for (var a = 0; a < values.length; a++) {
        var value = values[a];

        if (empty(data[value])) {
            data[value] = '';

        }
    }
    return data;


}


function updateDepositPaid() {


    var depositVal = getDeposit();
    var amountPaid = getAmountBySelector('#depositPaid');

    var balanceLeft = depositVal - amountPaid;

    balanceLeft = '$' + formatMoney(balanceLeft);

    $('#balanceLeft').html(balanceLeft);

    quote.SalesDiscount.Totals.depositAmount = amountPaid;

}


function buildPaymentInfoOld() {

    var paymentOption = buildPaymentOptions();
    var totalPrice = footerTotalCost();
    var depositNeeded = getDeposit();
    var amountPaid = getDepositPaid();
    var lastFour = getLastFour();

    var remaingDue = depositNeeded - amountPaid;

    var html = '' +
        '<div class="customerViewBoxDetails">' +
        '   <div id="totalAmount" class="paymentLeft">Total Amount Due:</div><div class="paymentRight">' + totalPrice + '  </div>' +
        '   <div id="depositDue"class="paymentLeft">Deposit Due Now:</div><div class="paymentRight"> ' + currency + formatMoney(depositNeeded) + ' </div>' +
        '   <div class="paymentLeft">Amount Paid:</div><div     class="paymentRight"> <input onkeyup="updateDepositPaid()" id="depositPaid" class="billingAmountTextBox" type="text" value="' + amountPaid + '" name="depositAmount"></div>' +
        '   <div id="balancePaid" class="paymentLeft">Balance  Due:</div><div id="balanceLeft" class="paymentRight"> ' + currency + formatMoney(remaingDue) + '</div>' +
        '</div>' +
        '<div class="paymentViewBoxDetails">' +
        '   <div class="customerBoxDetailsTitle">Payment Information</div>' +
        paymentOption +
        '   </div>' +
        '   <div class="creditCardLabel">Check # or Last 4 Digits of Credit Card</div> <input maxlength="4" id="lastFour" class="creditLastFour" type="text" value="' + lastFour + '" name="ccLastFour">  ';


    return html;


}
function buildViewCustomer(type) {

    var data = quote.Customer;

    data = validateCustomer(data);

    var customerTitleAndButtons = getCustomerTitleAndButtons(data);
    var customerDetails = getCustomerDetails(data);
    var paymentInfoBox = getPaymentInfoBox(type);

    var html = '' +
        '<div id="addNewCustomer">' +
        customerTitleAndButtons +
        customerDetails +
        paymentInfoBox +
        '   <div id="customerViewNote">' +
        '   <div class="customerViewNoteTitle">Customer Notes</div>' +
        '   <textarea readOnly class="customerNotes" placeholder=""  name="customerNotes">' + data.customerNotes + '</textarea>' +
        '   </div>' +
        '</div>';

    return html;
}


function getPaymentInfoBox(type) {

    var paymentInfoBox = '';

    if (type == 'payment') {

        var paymentInfo = buildPaymentInfoOld();
        var paymentNote = getPaymentNote();

        paymentInfoBox = '' +
            '   <div class="customerView">' +
            paymentInfo +
            '   </div>' +
            '   <div class="paymentNotesBox">' +
            '       <div class="customerViewNoteTitle">Payment Notes</div>' +
            '       <textarea id="paymentNote" class="paymentNotes" placeholder=""  name="paymentNote">' + paymentNote + '</textarea>' +
            '   </div>';


    }

    return paymentInfoBox;

}

function getCustomerTitleAndButtons(data) {

    var paymentInfoBox = '';
    var saveButton = '';
    var editButton = '<div id="' + quote.id + '-Edit" class="editButtons">Edit</div>';
    var closeButton = '<div id="' + quote.id + '-Close" class="editButtons  myGreen">Close</div>';

    var title = 'Customer Information';

    if (type == 'payment') {
        var type = getParameter('type');

        var paymentInfo = buildPaymentInfoOld();
        var paymentNote = getPaymentNote();
        saveButton = '<div id="' + quote.id + '-SavePayment" class="editButtons">Save Payment</div>';
        closeButton = '<div id="' + quote.id + '-ClosePayment" class="editButtons  myGreen">Close</div>';
        editButton = '<div id="' + quote.id + '-EditPayment" class="editButtons">Edit</div>';
        title = "Update Payment Information";


        if (type == 'confirm') {
            closeButton = '<div id="' + quote.id + '-ClosePayment" class="editButtons">Close</div>';
            saveButton = '<div id="' + quote.id + '-PaymentConfirmed" class="editButtons myGreen">Confirm Payment</div>';
            title = 'Confirm Payment and Move To Production';
        }

        paymentInfoBox = '' +
            '   <div class="customerView">' +
            paymentInfo +
            '   </div>' +
            '   <div class="paymentNotesBox">' +
            '       <div class="customerViewNoteTitle">Payment Notes</div>' +
            '       <textarea id="paymentNote" class="paymentNotes" placeholder=""  name="paymentNote">' + paymentNote + '</textarea>' +
            '   </div>';

    }

    var html = '' +
        '   <div class="title">' + title + '</div>' +
        '   <div class="customerViewButtons">' +
        editButton +
        closeButton +
        saveButton +
        '   </div>';


    return html;

}

function getCustomerDetails(data) {

    data.jobState = removeSelectState(data.jobState);
    data.billingState = removeSelectState(data.billingState);

    var html = '' +
        '   <div class="customerView">' +
        '       <div class="customerViewBoxDetails">' +
        '         <div class="customerBoxDetailsTitle">Customer Info</div>' +
        '         <div class="cVBDLeft">Company Name:</div><div class="cVBDRight">' + returnBlankIfEmpty(data.companyName) + '</div>' +
        '         <div class="cVBDLeft">Last Name:</div><div class="cVBDRight">' + data.lastName + '</div>' +
        '         <div class="cVBDLeft">First Name:</div><div class="cVBDRight">' + data.firstName + '</div>' +
        '         <div class="cVBDLeft">Cell:</div><div class="cVBDRight">' + data.phone + '</div>' +
        '         <div class="cVBDLeft">eMail:</div><div class="cVBDRight">' + data.email + '</div>' +
        '      </div>' +
        '      <div class="customerViewBoxDetails">' +
        '         <div class="customerBoxDetailsTitle">' + capitaliseFirstLetter(shipping) + ' Address</div>' +
        '         <div class="cVBDLeft">Name:</div><div class="cVBDRight">' + data.shippingName + '</div>' +
        '         <div class="cVBDLeft">Address1:</div><div class="cVBDRight">' + data.jobAddress1 + '</div>' +
        '         <div class="cVBDLeft">Address2:</div><div class="cVBDRight">' + data.jobAddress2 + '</div>' +
        '         <div class="cVBDLeft">' + city + ':</div><div     class="cVBDRight">' + data.jobCity + '</div>' +
        '         <div class="cVBDLeft">' + state + ':</div><div    class="cVBDRight">' + data.jobState + '</div>' +
        '         <div class="cVBDLeft">' + zipText + ':</div><div  class="cVBDRight">' + data.jobZip + '</div>' +
        '         <div class="cVBDLeft">Email:</div><div  class="cVBDRight">' + data.shippingEmail + '</div>' +
        '      </div>' +
        '      <div class="customerViewBoxDetails">' +
        '         <div class="customerBoxDetailsTitle">Billing Address</div>' +
        '         <div class="cVBDLeft">Name:</div><div class="cVBDRight">' + data.billingName + '</div>' +
        '         <div class="cVBDLeft">Address1:</div><div class="cVBDRight">' + data.billingAddress1 + '</div>' +
        '         <div class="cVBDLeft">Address2:</div><div class="cVBDRight">' + data.billingAddress2 + '</div>' +
        '         <div class="cVBDLeft">' + city + ':</div><div     class="cVBDRight">' + data.billingCity + '</div>' +
        '         <div class="cVBDLeft">' + state + ':</div><div    class="cVBDRight">' + data.billingState + '</div>' +
        '         <div class="cVBDLeft">' + zipText + ':</div><div  class="cVBDRight">' + data.billingZip + '</div>' +
        '         <div class="cVBDLeft">Email:</div><div class="cVBDRight">' + data.billingEmail + '</div>' +
        '      </div>' +
        '   </div>';

    return html;

}

function buildLeadSource (data) {

    var leadSourcesUK = ["GDL17 NEC", "GDL16 NEC", "GDL18 Excel", "GDL17 Excel", "HBR18 NEC", "HBR17 NEC",  "ProBuild17 Event City",  "Referral", "Remake", "Trade", "Website"];

    var leadSourcesUS = [
        "Google",
        "Client Referral",
        "Walk-In",
        "Dream List",
        "Home Show",
        "Contractor Referral",
        "Architect Referral",
        "Designer Referral",
        "Self Generated",
        "Builder and Developer",
        "Repeat Customer - Contractor",
        "Repeat Customer - Builder",
        "Repeat Customer - Designer",
        "Repeat Customer - Home Owner",
        "Truck Signage",
        "Yelp",
        "BBB"
        ]





    var leadSources = leadSourcesUS;
    if (isUK()) {
        leadSources = leadSourcesUK;
    }

    var options = '   <option value="" ></option>';
    var found = false;

    for (var a = 0; a < leadSources.length; a++) {

        var selected = '';
        if (leadSources[a] == data.leadSource) {
            selected = 'selected';
            found = true;
        }
        var option = '   <option value="'+leadSources[a]+'"  '+selected+'>'+leadSources[a]+'</option>';
        options = options + option;
    }

    if (!empty(data.leadSource) && found == false) {
        options = options + '   <option value="'+data.leadSource+'"  selected >'+data.leadSource+'</option>';
    }


    var dropDown = '' +
        '<select id="leadSource"  name="leadSource" class="editInput">' +
        '   ' + options +
        '</select>' ;



    // only the UK stie should be forcing the leads pull down
    if (isUKTrade()) {
        dropDown = '   <input id="leadSource" onChange="removeErrorWithSelector(\'leadSourceError\')" class="editInput" type="text"  name="leadSource"  value="' + data.leadSource + '"/>' ;
    }

    var leadSourceHTML =    '' +
        '<div class="editLeft">* Lead Source:</div>' +
        '<div class="editRight">' +
        '   ' + dropDown +
        '</div>' +
        '<div  id="leadSourceError" class="errorMessageCustomer">Please enter the lead Source</div>';

    if (!empty(data.newCustomer)) {
        leadSourceHTML =    '' +
            '           <div class="customerLabel">Lead Source (Required)</div> ' +
            '   ' + dropDown ;
    }

    return leadSourceHTML;
}

function buildEditCustomer(customer) {

    var type = '';
    var quoteID = quote.id;
    var data = validateCustomer(customer);

    var sameAsShippingChecked = '';
     if (quote.Customer.sameAsShipping == '1') {
         sameAsShippingChecked = 'checked';
     }

    var addShipping = '       <div class="customerSearchRowResults"> <input id="checkbox-' + customer.id + '" type="checkbox" '+sameAsShippingChecked+' name="useShippingAddress" value="yes"> Use for Shipping Address</div>' ;

    var title = 'Customer Info';
    var saveButton = '<div id="' + quoteID + '-SaveChanges" class="editButtons myGreen floatRightFlush">Save Changes</div>';
    var reattachButton = '<div id="' + quoteID + '-SaveChangesAttach" class="editButtons myGreen floatRightFlush editButtonsMedium">Save Changes & Reattach</div>';
    var cancelButton = '<div id="' + quoteID + '-Cancel" class="editButtons">Cancel</div>';

    if (tab == 'customer') {
        reattachButton = '';
        addShipping = '';
    }

    if (type == 'confirmCustomer') {
        title = "Confirm Customer Information";
        var buttonName = 'Confirm Customer and Convert to Order';
        if (siteDefaults.salesFlow == 2) {
            buttonName = 'Confirm Personal Info and Purchase';
        }
        saveButton = '<div id="SaveChangesAndConvert" class="editButtons editButtonsLong myGreen floatRight">' + buttonName + '</div>';
        cancelButton = '<div id="' + quoteID + '-CancelSaveChanges" class="editButtons">Cancel</div>';
    }

    if (empty(data.leadSource)) {
        data.leadSource = '';
    }

    var leadSourceHMTL =  buildLeadSource(data);

    if (siteDefaults.salesFlow == 2) {
        leadSourceHMTL = '';
    }

    var html = '' +
        '<div id="addNewCustomer">' +
        '<div class="title">' + title + '</div>' +
        '<div class="sectionBlock">' +
        '   <div class="customerLeft"> ' +
        '       <input  class="editInput" type="hidden" name="id"   value="' + data.id + '"/>' +
        '       <div class="editSectionTitles"><B>Contact Information</B></div>' +
        '       <div class="editBox">' +
        '       ' + leadSourceHMTL +
        '           <div class="editLeft">Company Name </div>' +
        '           <div class="editRight"><input  class="editInput" type="text" name="companyName"   value="' + returnBlankIfEmpty(data.companyName) + '"/></div>' +
        '           <div class="editLeft"> First Name: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'firstNameError\')" class="editInput" type="text"  name="firstName"  value="' + data.firstName + '"/></div>' +
        '           <div  id="firstNameError" class="errorMessageCustomer">Please enter the customer\'s first name</div>' +
        '           <div class="editLeft"> Last Name: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'lastNameError\')" class="editInput" type="text"  name="lastName"  value="' + data.lastName + '"/></div>' +
        '           <div  id="lastNameError" class="errorMessageCustomer">Please enter the customer\'s last name</div>' +
        '           <div class="editLeft"> Email: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'emailError\')" class="editInput" type="text"  name="email"  value="' + data.email + '"/></div>' +
        '           <div  id="emailError" class="errorMessageCustomer">Please enter the customer\'s email</div>' +
        '           <div class="editLeft"> Phone: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'phoneError\')" class="editInput" type="text"  name="phone"  value="' + data.phone + '"/></div>' +
        '           <div  id="phoneError" class="errorMessageCustomer">Please enter the customer\'s phone</div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '<div class="sectionBlock">' +
        '   <div class="customerLeft"> ' +
        '       <div class="editSectionTitles"><B>Main Address </B> </div>' +
        '       <div id="billingAddress" class="editBox">' +
        '           <div class="editLeft">Billing Name:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingName"   value="' + data.billingName + '"/></div>' +
        '           <div class="editLeft">Billing Email:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingEmail"   value="' + data.billingEmail + '"/></div>' +
        '           <div class="editLeft">Billing Phone:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingPhone"   value="' + data.billingPhone + '"/></div>' +
        '           <div class="editLeft">Billing Address 1:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingAddress1"   value="' + data.billingAddress1 + '"/></div>' +
        '           <div class="editLeft">Billing Address 2:</div>' +
        '           <div class="editRight"><input class="editInput" type="text"  name="billingAddress2"  value="' + data.billingAddress2 + '"/></div>' +
        '           <div class="editLeft">Billing ' + city + ': </div>' +
        '           <div class="editRight"><input class="editInput" type="text"  name="billingCity"  value="' + data.billingCity + '"/></div>' +
        '           <div class="editLeft">Billing ' + state + ': </div>' +
        '           <div class="editRight">' + statesSelectHTML(data.billingState, "billingState") + '</div>' +
        '           <div class="editLeft">Billing ' + zipText + ': </div>' +
        '           <div class="editRight"><input class="editInput" type="text"  name="billingZip"  value="' + data.billingZip + '"/></div>' +
        '           ' + addShipping +
        '       </div>' +
        '   </div>' +
        '   <div class="customerLeft extraTopSpace extraBottomSpace"> ' +
        reattachButton + saveButton +
        '   </div> ' +
        '   <div class="customerRight extraTopSpace extraBottomSpace"> ' +
        cancelButton +
        '   </div> ' +
        '</div>' +
        '</div>';

    return html;
}

function displayCustomerEdit(customerData) {

    var quoteID = quote.id;
    var data = quote.Customer;
    data = validateCustomer(data);

    var sameAsShippingChecked = '';
    // if (quote.Customer.sameAsShipping == '1') {
    //     sameAsShippingChecked = 'checked';
    // }

    var title = 'Customer Info';
    var saveButton = '<div id="' + quoteID + '-SaveChanges" class="editButtons myGreen floatRight">Save Changes</div>';
    var cancelButton = '<div id="' + quoteID + '-Cancel" class="editButtons">Cancel</div>';

    if (type == 'confirmCustomer') {
        title = "Confirm Customer Information";
        var buttonName = 'Confirm Customer and Convert to Order';
        if (siteDefaults.salesFlow == 2) {
            buttonName = 'Confirm Personal Info and Purchase';
        }
        saveButton = '<div id="SaveChangesAndConvert" class="editButtons editButtonsLong myGreen floatRight">' + buttonName + '</div>';
        cancelButton = '<div id="' + quoteID + '-CancelSaveChanges" class="editButtons">Cancel</div>';
    }

    if (empty(data.leadSource)) {
        data.leadSource = '';
    }


    var leadSourceHMTL = buildLeadSource(data);

    if (siteDefaults.salesFlow == 2) {
        leadSourceHMTL = '';
    }

    var html = '' +
        '<div id="addNewCustomer">' +
        '<div class="title">' + title + '</div>' +
        '<div class="sectionBlock">' +
        '   <div class="customerLeft"> ' +
        '       <div class="editSectionTitles"><B>Contact Information</B></div>' +
        '       <div class="editBox">' +
        '       ' + leadSourceHMTL +
        '           <div class="editLeft">Company Name </div>' +
        '           <div class="editRight"><input  class="editInput" type="text" name="companyName"   value="' + data.companyName + '"/></div>' +
        '           <div class="editLeft"> First Name: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'firstNameError\')" class="editInput" type="text"  name="firstName"  value="' + data.firstName + '"/></div>' +
        '           <div  id="firstNameError" class="errorMessageCustomer">Please enter the customer\'s first name</div>' +
        '           <div class="editLeft"> Last Name: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'lastNameError\')" class="editInput" type="text"  name="lastName"  value="' + data.lastName + '"/></div>' +
        '           <div  id="lastNameError" class="errorMessageCustomer">Please enter the customer\'s last name</div>' +
        '           <div class="editLeft"> Email: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'emailError\')" class="editInput" type="text"  name="email"  value="' + data.email + '"/></div>' +
        '           <div  id="emailError" class="errorMessageCustomer">Please enter the customer\'s email</div>' +
        '           <div class="editLeft"> Phone: </div>' +
        '           <div class="editRight"><input onChange="removeErrorWithSelector(\'phoneError\')" class="editInput" type="text"  name="phone"  value="' + data.phone + '"/></div>' +
        '           <div  id="phoneError" class="errorMessageCustomer">Please enter the customer\'s phone</div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '<div class="sectionBlock">' +
        '   <div class="customerLeft"> ' +
        '       <div class="editSectionTitles"><B>Main Address </B> </div>' +
        '       <div id="billingAddress" class="editBox">' +
        '           <div class="editLeft">Billing Name:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingName"   value="' + data.billingName + '"/></div>' +
        '           <div class="editLeft">Billing Address 1:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingAddress1"   value="' + data.billingAddress1 + '"/></div>' +
        '           <div class="editLeft">Billing Address 2:</div>' +
        '           <div class="editRight"><input class="editInput" type="text"  name="billingAddress2"  value="' + data.billingAddress2 + '"/></div>' +
        '           <div class="editLeft">Billing ' + city + ': </div>' +
        '           <div class="editRight"><input class="editInput" type="text"  name="billingCity"  value="' + data.billingCity + '"/></div>' +
        '           <div class="editLeft">Billing ' + state + ': </div>' +
        '           <div class="editRight">' + statesSelectHTML(data.billingState, "billingState") + '</div>' +
        '           <div class="editLeft">Billing ' + zipText + ': </div>' +
        '           <div class="editRight"><input class="editInput" type="text"  name="billingZip"  value="' + data.billingZip + '"/></div>' +
        '           <div class="editLeft">Billing Email:</div>' +
        '           <div class="editRight"><input class="editInput" type="text" name="billingEmail"   value="' + data.billingEmail + '"/></div>' +
        '       </div>' +
        '   </div>' +
        '   <div class="customerLeft extraTopSpace extraBottomSpace"> ' +
        saveButton +
        '   </div> ' +
        '   <div class="customerRight extraTopSpace extraBottomSpace"> ' +
        cancelButton +
        '   </div> ' +
        '</div>' +
        '</div>';

    return html;
}


function editCustomer(customer) {

    getCustomer(customer).done(function (result) {
        displayCustomerEdit(result)
    });

}


function removeErrorWithSelector(selector) {

    $('#' + selector).hide();
}

function statesSelectHTML(currentState, name) {

    var stateList = new Array("Select State", "AK", "AL", "AR", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MH", "MI", "MN", "MO", "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "PW", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VI", "VT", "WA", "WI", "WV", "WY");

    var html = '<select onChange="removeErrorWithSelector(\'' + name + 'Error\')" class="statesPullDown" name="' + name + '">';

    for (var a = 0; a < stateList.length; a++) {
        var state = stateList[a];
        if (state == currentState) {
            if (state == 'Select State') {
                html += '<option value="" selected>' + state + '</option>';
            }else{
                html += '<option value="' + state + '" selected>' + state + '</option>';
            }
        } else {
            html += '<option value="' + state + '">' + state + '</option>';
        }
    }

    html += "</select>";

    if (isUK()) {
        html = '<input type="text" value="' + currentState + '" name="' + name + '" class="editInput" >';
    }

    return html;
}