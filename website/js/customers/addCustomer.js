'use strict'


function addCustomerHTML() {

    var data = {"leadSource":"","newCustomer":true};
    var leadSourceHTML = buildLeadSource(data);


    var billingStateHTML = 'Billing State';
    var billingZipHtml = 'Billing Zipcode';

    if (isUK()) {
        billingStateHTML = 'Billing County';
        billingZipHtml = 'Billing Postcode'
    }


    var html = '' +
        '<div id="addNewCustomer"> ' +
        '   <div class="title">Customer Info</div> ' +
        '   <div class="sectionBlock"> ' +
        '       <div class="leftSideAddressGroup"> ' +
        '           <div class="sectionTitle">Personal Info</div> ' +
        '           ' + leadSourceHTML +
        '           <div class="customerLabel">First Name</div> ' +
        '           <input class="customerInput" type="text" name="firstName"  value=""/> ' +
        '           <div class="customerLabel">Last Name</div> ' +
        '           <input class="customerInput" type="text"  name="lastName"  value=""/> ' +
        '           <div class="customerLabel">Company Name (optional)</div> ' +
        '           <input class="customerInput" type="text" name="companyName"   value=""/> ' +
        '           <div class="customerLabel">Email</div> ' +
        '           <input class="customerInput" type="text"  name="email"   value=""/> ' +
        '           <div class="customerLabel">Phone (optional)</div> ' +
        '           <input class="customerInput" type="text"  name="phone"   value=""/> ' +
        '       </div> ' +
        '       <div class="rightSideAddressGroup"> ' +
        '           <div class="sectionTitle">Billing</div> ' +
        '           <div class="customerLabel">Billing Contact</div> ' +
        '           <input class="customerInput" type="text"  name="billing_contact"   value=""/> ' +
        '           <div class="customerLabel">Billing Email</div> ' +
        '           <input class="customerInput" type="text"  name="billing_email"   value=""/> ' +
        '           <div class="customerLabel">Billing Phone</div> ' +
        '           <input class="customerInput" type="text"  name="billing_phone"   value=""/> ' +
        '           <div class="customerLabel">Billing Address1</div> ' +
        '           <input class="customerInput" type="text" name="billing_address1"   value=""/> ' +
        '           <div class="customerLabel">Billing Address2</div> ' +
        '           <input class="customerInput" type="text" name="billing_address2"   value=""/> ' +
        '           <div class="customerLabel">Billing City</div> ' +
        '           <input class="customerInput" type="text" name="billing_city"   value=""/> ' +
        '           <div class="customerLabel">'+billingStateHTML+'</div> ' +
        '           <input class="customerInput" type="text" name="billing_state"   value=""/> ' +
        '           <div class="customerLabel">'+billingZipHtml+'</div> ' +
        '           <input class="customerInput" type="text"  name="billing_zip"   value=""/> ' +
        '           <div class="customerLabel">' +
        '               <input id="checkbox" type="checkbox" name="useShippingAddress" value="yes">' +
        '               Use For Shipping Address' +
        '           </div>' +
        '       </div> ' +
        '</div>' +
        '   <div class="sectionBlock"> ' +
        // '       <div class="rightSideAddressGroup"> ' +
        // '           <div class="sectionTitle">'+capitaliseFirstLetter(shipping)+'</div> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' Contact</div> ' +
        // '           <input class="customerInput" type="text"  name="shipping_contact"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' Email</div> ' +
        // '           <input class="customerInput" type="text"  name="shipping_email"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' Phone</div> ' +
        // '           <input class="customerInput" type="text"  name="shipping_phone"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' Address1</div> ' +
        // '           <input class="customerInput" type="text" name="shipping_address1"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' Address2</div> ' +
        // '           <input class="customerInput" type="text" name="shipping_address2"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' City</div> ' +
        // '           <input class="customerInput" type="text" name="shipping_city"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' State</div> ' +
        // '           <input class="customerInput" type="text" name="shipping_state"   value=""/> ' +
        // '           <div class="customerLabel">'+capitaliseFirstLetter(shipping)+' Zipcode</div> ' +
        // '           <input class="customerInput" type="text"  name="shipping_zip"   value=""/> ' +
        // '       </div> ' +
        '   </div> ' +
        '</div>';

    return html;
}