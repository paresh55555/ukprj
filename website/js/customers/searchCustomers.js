'use strict'


function searchCustomerHTML() {

    var html = '' +
        '<div id="searchCustomer"> ' +
        '   <div class="title">Choose Previous Customer</div> ' +
        '   <div class="sectionBlock"> ' +
        '       <div id="searchBox"> ' +
        '           <img id="searchIcon" src="/images/icons/search.svg"> ' +
        '           <input id="search" type="text" name="search"  placeholder="Search customer name" value=""/> ' +
        '       </div> ' +
        '   <div id="searchResults"></div> ' +
        '   </div> ' +
        '</div>';

    return html;
}


