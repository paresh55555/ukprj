'use strict'

function loadTransactions () {

    setTab('color');
    selectTab('#color');
    updateFormStatus();

    $.get('html/transactions.html', function (html) {

        $('body').html(html);

    });

}