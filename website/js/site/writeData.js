function setTab(myTab) {

    if (empty(myTab)) {
        tab = getGuestTab();
    } else {
        tab = myTab;
    }

    $.cookie('tab', tab, {expires: 5, path: '/'});

}

function saveState() {

    writeLocalStorageWithKeyAndData('quote', quote);
    writeLocalStorageWithKeyAndData('defaults', defaults);

    writeLocalStorageWithKeyAndData('formStatus', formStatus);
    $.cookie('user', user, {expires: 3, path: '/'});


}


//###########################


function writeCookie() {

    writeFormStatus();

}

function writeDefaults(discount) {

    defaults = discount;
    writeLocalStorageWithKeyAndData("defaults", defaults);

}

function writeQuote(quote) {

    writeLocalStorageWithKeyAndData("quote", quote);
}


function writeDiscount(discount) {

    var tab = getParameter('tab');

    quote.SalesDiscount = discount;
    writeLocalStorageWithKeyAndData("quote", quote);
}


function writeFormStatus() {

    if (typeof(formStatus) == "undefined") {
        formStatus = formStatusDefaults();
    } else {
        writeLocalStorageWithKeyAndData("formStatus", formStatus)
    }
}


function writeModuleInfo() {

    if (typeof(moduleInfo) == "undefined") {
        moduleInfo ={};
    } else {
        writeLocalStorageWithKeyAndData("moduleInfo", moduleInfo)
    }
}


function writeColorChoicesByModule() {

    if (typeof(colorChoicesByModule) == "undefined") {
        colorChoicesByModule = {};
    }
    writeLocalStorageWithKeyAndData("colorChoicesByModule", colorChoicesByModule)
}


function writeWindowsForm() {

    if (typeof(windowsForm) == "undefined") {
        windowsForm = windowFormDefaults();
    } else {
        writeLocalStorageWithKeyAndData("windowsForm", windowsForm);
    }
}


function writeStateUI() {

    writeLocalStorageWithKeyAndData("uiState", uiState);

}
function writeLocalStorageWithKeyAndData(key, data) {

    if (getParameter('print')) {
        return;
    }

    if (typeof(Storage) !== "undefined") {
        try {
            localStorage[key] = JSON.stringify(data);
        } catch (e) {
            alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Sales Quoter is not compatible with "Private Browsing Mode"');
        }


    }

}


function writeQuoteID(quoteID) {

    if (typeof(quoteID) == "undefined") {
        quoteID = '';
    }
    //$.cookie('quoteID', quoteID, { expires: 1, path: '/' });
    quote.id = quoteID;
    writeQuote(quote);

}

function writeCustomer(customer) {

    writeLocalStorageWithKeyAndData('customer', customer);

}

function writeLead(lead) {
    writeLocalStorageWithKeyAndData('lead',lead);
}

function readLead() {
    var lead = readKey('lead');

    return lead;
}

function writeCart(cart) {


    //quote.Cart = cart
    //writeQuote(quote);

    //writeLocalStorageWithKeyAndData('cart',cart);
    //
    //clearCart();
    //for (var row in cart) {
    //    $.cookie(row, cart[row], { expires: 7, path: '/' });
    //}
    //

}
function writeUser(user) {

    if (empty(user) ) {
        user = {type: 'guest'};
    }

    var expire = currentTimeInSeconds()+ (86400 * 3);
    user.expire = expire;


    writeLocalStorageWithKeyAndData('user', user);
    // $.cookie('user', user, {expires: 3, path: '/'});

}


function clearCart() {

    writeLocalStorageWithKeyAndData('cart', {});
    //
    //
    //var cookies = $.cookie();
    //var re = new RegExp("Cart");
    //for (var row in cookies) {
    //    if (row.match(re)) {
    //        $.removeCookie(row);
    //    }
    //}
}

function clearAllCookies() {
    var cookies = $.cookie();
    for (var row in cookies) {
        $.removeCookie(row);
    }
}

function setUser(user) {

    writeUser(user);

}

function salesAdminDefaults() {

    var discountJSON = JSON.stringify(defaults);

    $('#SaveChanges').html('Saving');

    $.ajax({
        url: "https://" + apiHostV1 + "/api/salesperson/" + user.id + "/defaults?" + authorizePlusSalesPerson() + addSite(),
        data: discountJSON,
        type: "PUT",
        success: function (result) {

            discounts = quote.SalesDiscount;

            var response = jQuery.parseJSON(result);

            if (response.status == "Success") {

                setTimeout(function () {
                    trackAndDoClick("viewAccount");
                }, 300);

            } else {
                alert("Defaults Not Saved");

            }
        }
    });

}