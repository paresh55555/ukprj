'use strict'

function loadSite() {

    loadGlobals();
    $.cookie.json = true;

    $.when(setSiteDefaults(),getUISettings()).done(function (defaultsResponse, uiSettingsResponse) {
      
        uiSettings = uiSettingsResponse[0];
        loadLocalData();
    });
}


function setSiteDefaults() {

    if (!(true)) {
        return $.Deferred().resolve().promise();
    }

    var myPromise = getSiteDefaults();

    myPromise.done(function(data) {
        siteDefaults = data;
        cssFont = 'style="color:'+ siteDefaults.cssFont +'"';
        cssReverseFont = 'style="color:'+ siteDefaults.cssReverseFont +'"';
        cssBorder = 'style="border-color:' + siteDefaults.cssBorder + '"';
        cssBackground = 'style="background-color:' + siteDefaults.cssBackground + '"';
        cssReverseBackground = 'style="background-color:' + siteDefaults.cssReverseBackground + '"';
    }).error(function(err) {
        alert("Site Defaults Failed To Set");
    });

    return myPromise;
}


function loadLocalData() {

    siteFromURL();
    setCurrency();
    cssForBrowser();

    uiState = readUI();
    dealer = readDealer();
    user = readUser();
    quote = readQuote();
    discounts = setDiscounts();
    formStatus = readFormStatus();
    defaults = readDefaults();
    moduleInfo = readModuleInfo();
    windowsForm = readWindowForm();

    if (isSalesPerson()) {
        loadDataForSalesPerson();
    } else if (isGuest()) {
        loadDataForGuest();
    } else if (isAccounting()) {
        loadDataForAccounting();
    } else if (isProduction()) {
        loadDataForProduction();
    } else if (isLead()) {
        loadDataForLeads();
    }
    else {
        if (allowGuests()) {
            loadDataForNewGuest();
        } else {
            loadDataForSalesPerson();
        }
    }
}
