var module = {};
var formStatus = {};
var siteDefaults = {};
var windowsForm = {};
var colorChoicesByModule = {};
var user = {};
var tab = {};
var newTab = {};
var secondTab = {};
var discounts = {};
var discount = {};
var quoteID = '';
var defaults = {};
var quote = {};
var moduleInfo = {};
var uiSettings = {};
var uiState = {};
var extenderChoices = {};
var cssFont = '';
var cssBorder = '';
var cssReverseFont ='';
var cssBackground = '';
var cssReverseBackground = '';
var dealer ='';

var cartConf = {
    headerNav: "on",
    cartNav: "on",
    salesNaV: "off",
    leadsNav: "off",
    menuOptionsBox: "on"
}

var salesConf = {
    headerNav: "on",
    cartNav: "off",
    salesNav: "on",
    leadsNav: "off",
    menuOptionsBox: "on"
}

var builderConf = {
    headerNav: "on",
    builderNav: "on",
    menuOptionsBox: "on"
}

var orderConf = {
    headerNav: "on",
    cartNav: "reduced",
    salesNav: "on",
    leadsNav: "off",
    menuOptionsBox: "on"
}


var signInConf = {
    headerNav: "off",
    cartNav: "off",
    salesNav: "off",
    leadsNav: "off",
    menuOptionsBox: "off"
}

var inCartConf = {
    headerNav: "off",
    cartNav: "off",
    salesNav: "off",
    leadsNav: "off",
    menuOptionsBox: "off"
}

var leadsConf = {
    headerNav: "on",
    cartNav: "off",
    salesNav: "off",
    leadsNav: "on",
    menuOptionsBox: "on"
}

var accountingConf = {
    headerNav: "on",
    cartNav: "off",
    accountingNav: "off",
    menuOptionsBox: "on"
}

var productionConf = {
    headerNav: "on",
    productionNav: "off",
    menuOptionsBox: "on"
}

var currency = "$";
var uom = '"';
var uomText = 'in.'
var tax = 'Tax';
var salesTax = 'Sales Tax';
var colorText = 'color';
var zipText = 'Zip Code';
var city = 'city';
var state = 'state';
var zipShort = 'zip';
var shipping = 'shipping';
var siteURL = '';
var site = 'sq';
var wto = '';


function loadGlobals() {

}