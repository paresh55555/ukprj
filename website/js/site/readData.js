'use strict'



function readLocalStorageWithKey(key) {

    if (getParameter('print')) {
        return;
    }

    var data = undefined;
    if (typeof(Storage) !== "undefined") {
        if (typeof(localStorage) !== "undefined") {
            if (localStorage[key] !== "undefined") {
                if (localStorage[key]) {
                    data = JSON.parse(localStorage[key]);
                }

            }
        }
    }
    return data;

}



function readKey(key) {

    var keyData = readLocalStorageWithKey(key);

    if (empty(keyData)) {
        keyData = {};
    }

    return keyData;
}


function readUI() {

    return readKey('uiState');

}


function readDealer() {

    var dealer = readKey('dealer');

    return dealer;
}


function readDefaults() {

    return readKey('defaults');

}


function readModuleInfo() {

    return readKey('moduleInfo');

}


function readFormStatus() {

    var formStatus = readLocalStorageWithKey('formStatus');
    if (empty(formStatus)) {
        formStatus = formStatusDefaults();
    }
    return formStatus;
}


function readDiscount() {


    if (typeof discounts == 'undefined' || discounts == null) {
        getJustSalesAdminDefaults();
    }


}