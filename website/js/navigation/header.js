function mainContentHTML() {

    var html = '' +
        '<div id="preMainContent"></div>' +
        '<div id="mainContent"></div>';

    return html;
}


function setIframe() {

    if(readKey('gForm') === 'yes'){
        siteDefaults.link1 = 'www.usa.panoramicdoors.com';
    }
    var html = '' +
        '<div id="companyContent">' +
        '   <div id="companyLogo"><img src="' + siteDefaults.logoURL + '"></div> ' +
        '   <div class="companyLinks"><a href="http://' + siteDefaults.link2 + '" '+ cssFont +'>' + siteDefaults.link2text + '</a> </div> ' +
        '   <div class="companyLinks"><a href="http://' + siteDefaults.link1 + '/"  '+ cssFont +'>' + siteDefaults.link1text + '</a> </div> ' +
        '   </div>';


    $('#companyHeader').html(html);
    $('#companyHeader').css({"background-color": siteDefaults.cssBackgroundColor});
}


function resetGuestDiscount() {

    writeQuote({});
    setupGuestQuote();

}


function resetSalesPersonDiscount() {

    writeQuote({});
    setupQuote();
}
function setupGuestQuote() {

    quote = readQuote();

    quote.SalesDiscount.Totals.code = "YZ-00";

    if (siteName == 'panoramicdoors') {
        quote.SalesDiscount.Totals.code = "YZ-08";
    }

    discounts = quote.SalesDiscount;
    defaults = {};
    writeDefaults(defaults);
    writeQuote(quote);
}

function setupQuote() {

    quote = readQuote();

    quote.SalesDiscount = jQuery.extend({}, defaults);
    discounts = quote.SalesDiscount;
    writeQuote(quote);


}


function isSalesPerson() {

    if (user) {
        if (user.type === "salesPerson" || user.type === "salesPersonSurvey" ) {
            return true;
        }
    }

    return false;
}

function isLimitedAccount() {

    if (user) {
        if (user.type === "productionLimited" ) {
            return true;
        }
    }
    return false;
}

function isAudit() {

    if (user) {
        if (user.type === "audit" ) {
            return true;
        }
    }
    return false;
}


function isSurvey() {

    if (user) {
        if ((user.type === "survey" || user.type === "surveyLimited") ) {
            return true;
        }
    }

    return false;
}

function isSurveyLimited() {

    if (user) {
        if ( user.type === "surveyLimited" ) {
            return true;
        }
    }

    return false;
}


function isPart() {

    if (user) {
        if (user.extended == "part") {
            return true;
        }
    }

    return false;

}

function isAdmin() {

    if (user) {
        if (user.extended == "yes") {
            return true;
        }
    }

    return false;

}


function isGuest() {

    if (user) {
        if (user.type == "guest") {
            return true;
        }
    }

    return false;

}
function isAccounting() {

    if (user) {
        if (user.type == "accounting" || user.type == 'accountingLimited' || user.type == 'audit') {
            return true;
        }
    }

    return false;
}

function isAccountingLimited() {

    if (user) {
        if (user.type == "accountingLimited" || user.type == 'audit') {
            return true;
        }
    }

    return false;
}

function isPurchasedSalesFlow2() {


    if (siteDefaults.salesFlow == 2 ) {
        if (quote.type == 'Ready For Production' || quote.type == 'Confirming Payment') {
            return true;
        };
    }

    return false;

}

function accountingStarted() {

    if (user.anytimeEdits === 1 &&  quote.type != 'Completed' && quote.type != 'Delivered'  ) {
        return false;
    }

    if ( quote.type == 'Confirming Payment' || quote.type == 'Ready For Production' || quote.type == 'In Production' || quote.type == 'Completed' || quote.type == 'Delivered') {
        return true;
    }
    return false;
}

function productionStarted() {


    if (isAudit()) {
        return true;
    }


    if (user.anytimeEdits === 1 &&  quote.type != 'Completed' && quote.type != 'Delivered'  ) {
        return false;
    }

    if ( quote.type == 'In Production' || quote.type == 'Completed' || quote.type == 'Delivered') {
        return true;
    }


    if (isUK() & quote.type == 'Ready For Production') {
        return true;
    }

    return false;
}


function isProduction() {

    var collection = getParameter('collection');

    if (user) {
        if (user.type == "production" || user.type == "productionLimited" || isSurvey()) {
            return true;
        }
    }

    if (collection == 'yes') {
        return true;
    }

    return false;
}



function isLead() {

    if (user) {
        if (user.type == "leads") {
            return true;
        }
    }

    return false;

}

function quoteStarted() {

    var quoteState = readKey('quoteState');

    if(!empty(quoteState)) {
        return true;
    }

    return false;
}

function leavingQuoteOrder(passedTab) {

    if (passedTab == 'viewQuotes' || passedTab == 'viewOrders' || passedTab == 'signIn') {
        return true;
    }

    return false;
}


function editingQuote() {

    if (tab == 'size' ) {
        return true;
    }
    if (tab == 'color' ) {
        return true;
    }
    if (tab == 'extras' ) {
        return true;
    }
    if (tab == 'glass' ) {
        return true;
    }
    if (tab == 'hardware' ) {
        return true;
    }
    return false;
}


function leaveStartedQuote(passedTab) {

    if (quoteStarted()) {

        if (leavingQuoteOrder(passedTab)) {

            if (editingQuote()) {
                if(confirm('Quote has not be saved, are you sure you want to leave?')){
                    writeLocalStorageWithKeyAndData('quoteState', {'started':false});
                    return false
                }else{
                    return true;
                }
            }
        }
    }

    return false;
}

function trackAndDoClick(passedTab, parameters) {


    if (typeof(parameters) == "undefined") {
        parameters = '';
    }

    if (leaveStartedQuote(passedTab)) {
        return;
    }

    var random = '&' + Math.floor(Math.random() * (100000 - 1)) + 1;
    tab = passedTab;
    History.pushState({tab: tab, state: tab, rand: Math.random()}, tab, "?tab=" + tab + parameters + random);
}


//History.pushState
function buildHistory() {

    (function (window, undefined) {

        var State = History.getState();

        // History.log('initial:', State.data, State.title, State.url);

        History.Adapter.bind(window, 'statechange', function () {

            if (!window.innerDocClick) { // SQFES-15
                if(!validatePageOptions(tab)){
                    history.forward();
                    return false;
                }
            }

            var State = History.getState();

            // History.log('statechange:', State.data, State.title, State.url);

            if (typeof(State.data.tab) != "undefined") {
                if (State.data.tab != $.cookie('tab')) {
                    setTab(State.data.tab);

                }
            }

            loadTab();

        });

    })(window);

    document.onmouseover = function() {
        window.innerDocClick = true; //User's mouse is inside the page.
    };

    document.onmouseleave = function() {
        window.innerDocClick = false; //User's mouse has left the page.
    };
}

function getParameter(param) {

    var vars = [], hash;
    var q = document.URL.split('?')[1];
    if (q != undefined) {
        q = q.split('&');
        for (var i = 0; i < q.length; i++) {
            hash = q[i].split('=');
            vars.push(hash[1]);
            vars[hash[0]] = hash[1];
        }
    }
    return vars[param];

}


function getTab() {

    var tab = getParameter('tab');

    //if (!tab) {
    //    tab = $.cookie('tab');
    //}

    if (empty(tab)) {
        tab = 'guest';
    }

    return tab;

}


function buildNavigation(conf) {

    setCartNavState(conf.cartNav);
    setHeaderNavState(conf.headerNav);
    setMenuOptionsBoxState(conf.menuOptionsBox);

    //setLeadsNavState(conf.leadsNav);
    //setProductionNavState(conf.productionNav);
    //setSalesNavState(conf.salesNav);
    //setAccoutingNavState(conf.accountingNav);

    setClickableNavWithSelectorAndStatus('leadsNav', conf.leadsNav);
    setClickableNavWithSelectorAndStatus('accountingNav', conf.accountingNav);
    setClickableNavWithSelectorAndStatus('salesNav', conf.salesNav);
    setClickableNavWithSelectorAndStatus('productNavSection', conf.productionNav);
    setClickableNavWithSelectorAndStatus('builderNav', conf.builderNav);

}

function setMenuOptionsBoxState(state) {


    if (state == "on") {
        $('#menuOptionsBox').show();
        var menuOptions = $('#menuOptions');
        menuOptions.corner("10px");
        $('#screen').unbind("click").bind("click", showMenuOptions);
        if (!empty(user.name)) {
            $('#headerRightMenu').html(user.name);
        }


    } else {
        $('#menuOptionsBox').hide();
    }

}

function setHeaderNavState(state) {

    if (state == "on") {
        $('#headerNav').show();
    } else {
        $('#headerNav').hide();
    }

}

function setLeadsNavState(state) {

    if (state == "on") {
        $('.productNavSection').unbind("click").bind("click", tabClick);
        $('#leadsNav').show();
    } else {
        $('#leadsNav').hide();
    }

}


function setAccoutingNavState(state) {

    if (state == "on") {
        $('.productNavSection').unbind("click").bind("click", tabClick);
        $('#accountingNav').show();
    } else {
        $('#accountingNav').hide();
    }

}

function setProductionNavState(state) {

    if (state == "on") {
        $('.productNavSection').unbind("click").bind("click", tabClick);
        $('#productionNav').show();
    } else {
        $('#productionNav').hide();
    }

}

function setClickableNavWithSelectorAndStatus(selector, status) {

    if (status == "on") {
        $('.productNavSection').unbind("click").bind("click", tabClick);
        $('#' + selector).show();
    } else {
        $('#' + selector).hide();
    }
}

function setSalesNavState(state) {

    if (state == "on") {
        $('.productNavSection').unbind("click").bind("click", tabClick);
        $('#salesNav').show();
    } else {
        $('#salesNav').hide();
    }

}

function setCartNavState(state) {


    if (state == "on") {
        $('.topNavSection').unbind("click").bind("click", tabClick);
        $('#cartNav').show();
        processStartOverButtonVisibility(true);
        $('#cartButton').show();
    } else if (state == "reduced") {
        $('.topNavSection').unbind("click").bind("click", tabClick);
        $('#cartNav').show();
        processStartOverButtonVisibility(false);
        $('#cartButton').hide();
    } else {
        $('#cartNav').hide();
    }

}


function checkHeaderBeenLoaded() {

    if (!$('#screen').attr('id')) {
        renderPage();
    }


    readDealer()
}
function determineUserState() {

    //alert ("Stupid DetermineState Called Again");

    tab = getTab();
    var print = getParameter('print');
    if (print === 'yes') {
        return;
    }

    if (user.type == 'new') {
        user = {type: 'guest'};
        tab = "guest";
    }

    if (empty(user.token)) {

        if ( isSalesPerson() || tab === 'signIn') {
            tab = "signIn";
            setTab(tab);
        } else {
            tab = "guest";
            setTab(tab);
        }

    }

    //
    //
    //if (typeof(user) == "undefined") {
    //    user = {type: 'guest'};
    //    tab = "guest";
    //    writeUser(user);
    //} else {
    //    if (!user.token) {
    //        if (user.type === 'salesPerson' || tab === 'signIn') {
    //            tab = "signIn";
    //            setTab(tab);
    //        } else {
    //            tab = "guest";
    //            setTab(tab);
    //        }
    //    }
    //}

    if (getTab() != tab && (tab != 'guest' && tab != 'signIn')) {

        trackAndDoClick(tab);
    }

}


function determineTheProperStateOfTheForm() {

    determineUserState();

    formStatus = readLocalStorageWithKey("formStatus");
    if (typeof(formStatus) == "undefined") {
        formStatusDefaults();
    }

}


function userDefaults() {
    user = {};

}



function buildAndAttachCorePage(html) {


    var firstHTML = cartNavHTML();

    $('#mainContent').html(firstHTML);
    $('.topNavSection').unbind("click").bind("click", tabClick);

    $('#mainContent').append(html);
    updateCssForViewQuote();

    $('#dynamicContent').hide();

}

function isSalesAdminLimited(){
    if (siteDefaults.hasSalesAdmin == 1 && user.account == 'limited' ) {
        return true;
    }
    return false;
}

function isUserHasSalesAdmin(){
    if (user.account == 'on') {
        return true;
    }
    return false;
}