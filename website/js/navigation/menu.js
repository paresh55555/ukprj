'use strict'

function showMenuOptions() {

    var salesAdmin = $('#salesAdminMasterBox');

    if (salesAdmin.is(":visible")) {
        clearSalesAdmin()
    } else {
        var menuOptions = $('#menuOptions');

        if (menuOptions.is(":visible")) {
            menuOptions.hide();
            screenClear();
        }
        else {
            menuOptions.show();
            screenDim();
        }
    }
}


function userMenuHTML() {

    if (isSalesPerson()) {
        return salesPersonMenuHTML();
    }
    if (isLead()) {
        return leadsMenuHTML();
    }
    if (isAccounting()) {
        return accountingMenuHTML();
    }
    if (isProduction()) {
        return productionMenuHTML();
    }

    return guestMenuHTML();
}


function salesPersonMenuHTML() {

    var size = 175;

    var customerHTML = '' +
        '<div onClick="showMenuOptions(); trackAndDoClick(\'viewCustomers\');" class="menuRows">' +
        '   <div class="menuImageSVG">' + customersSVG() + '</div>' +
        '   <div class="menuText" ' + cssFont + '>Customers</div>' +
        '</div>';


    if (siteDefaults.showCustomers == 0) {
        customerHTML = '';
        size = size - 44;
    }


    var html = '' +
        '<div id="menuOptions" style="height: ' + size + 'px" >' +
        '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewQuotes\');" class="menuRows">' +
        '      <div class="menuImageSVG">' + transactionsSVG() + '</div>' +
        '      <div class="menuText" ' + cssFont + '>Quotes</div>' +
        '   </div>' +
        '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewOrders\');" class="menuRows">' +
        '      <div class="menuImageSVG">' + transactionsSVG() + '</div>' +
        '      <div class="menuText" ' + cssFont + '>Orders</div>' +
        '   </div>' +
        customerHTML +
        '   <div onClick="showMenuOptions(); signOut()" class="menuRowBottom">' +
        '      <img src="images/account/icon-x.svg" class="menuImage">' +
        '      <div class="menuText" ' + cssFont + '>Sign Out</div>' +
        '   </div>' +
        '</div>';


    return html;
}


function guestMenuHTML() {

    var html = '' +
        '<div id="menuOptions" style="height: 88px" >' +
        '   <div onClick="showMenuOptions(); trackAndDoClick(\'signIn\'); " class="menuRowBottom">' +
        '      <img src="images/account/icon-transactions.svg" class="menuImage">' +
        '      <div class="menuText">Sign In</div>' +
        '   </div>' +
        '   <div onClick="showMenuOptions(); trackAndDoClick(\''+(getGuestTab())+'\')" class="menuRowBottom">' +
        '      <img src="images/account/icon-transactions.svg" class="menuImage">' +
        '      <div class="menuText">New Guest</div>' +
        '   </div>' +
        '</div>';

    return html;
}


function leadsMenuHTML() {

    var html = '' +
        '<div id="menuOptions" style="height: 132px" >' +
        '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewLeads\');" class="menuRows">' +
        '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
        '       <div class="menuText">New</div>' +
        '   </div>' +
        '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewAssignedLeads\');" class="menuRows">' +
        '      <img src="images/account/icon-transactions.svg" class="menuImage" >' +
        '      <div class="menuText">Assigned</div>' +
        '   </div>' +
        '   <div onClick="showMenuOptions(); signOut()" class="menuRowBottom">' +
        '      <img src="images/account/icon-x.svg" class="menuImage">' +
        '      <div class="menuText">Sign Out</div>' +
        '   </div>' +
        '</div>';

    return html;
}


function accountingMenuHTML() {

    var index;
    if(isUK()){
        var menuLinks = ["accountingAll", "verifyDeposit", "accountingNeedsSurvey", "accountingPreProduction", "accountingProduction",
            "accountingCompleted", "balanceDue", "accountingDelivered", "accountingHold"];
        var menuNames = ["All", "Verify Deposit", "Needs Survey", "Pre-Production", "Production",
            "Completed", "Verify Final Payment", "Delivered", "Hold"];
    }else{
        var menuLinks = ["accountingAll", "verifyDeposit", "accountingPreProduction", "accountingProduction",
            "accountingCompleted", "balanceDue", "accountingDelivered", "accountingHold"];

        var menuNames = ["All", "Verify Deposit", "Pre-Production", "Production",
            "Completed", "Verify Final Payment", "Delivered", "Hold"];
    }

    var menuItemsHTML = '';

    for (index = 0; index < menuLinks.length; ++index) {

        var  menuHTML  = ''  +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\''+ menuLinks[index] +'\');" class="menuRows">' +
            '      <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '      <div class="menuText">' + menuNames[index] + '</div>' +
            '   </div>' ;
        menuItemsHTML = menuItemsHTML + menuHTML;
    }


    var html = '' +
        '<div id="menuOptions" style="height: ' + (menuLinks.length + 1) * 44  +'px" >' +
        '   ' + menuItemsHTML +
        '   <div onClick="showMenuOptions(); signOut()" class="menuRowBottom">' +
        '       <img src="images/account/icon-x.svg" class="menuImage">' +
        '       <div class="menuText">Sign Out</div>' +
        '   </div>' +
        '</div>';

    return html;
}


function productionMenuHTML() {

    if(isSurvey()){
        var html = '' +
            '<div id="menuOptions" style="height: 134px" >' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewNeedsSurvey\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Needs Survey</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewPreProduction\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Pre Production</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); signOut()" class="menuRowBottom">' +
            '       <img src="images/account/icon-x.svg" class="menuImage">' +
            '       <div class="menuText">Sign Out</div>' +
            '   </div>' +
            '</div>';
    }else{
        var html = '' +
            '<div id="menuOptions" style="height: 264px" >' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewPreProduction\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Pre Production</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewProduction\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Production</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewCompleted\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Completed</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewDelivered\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Delivered</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); trackAndDoClick(\'viewHold\');" class="menuRows">' +
            '       <img src="images/account/icon-transactions.svg" class="menuImage" >' +
            '       <div class="menuText">Hold</div>' +
            '   </div>' +
            '   <div onClick="showMenuOptions(); signOut()" class="menuRowBottom">' +
            '       <img src="images/account/icon-x.svg" class="menuImage">' +
            '       <div class="menuText">Sign Out</div>' +
            '   </div>' +
            '</div>';
    }

    return html;
}