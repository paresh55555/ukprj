'use strict'

function setTitle() {

    $('#footerRight').hide();
    if (!empty(formStatus)) {
        if (!empty(formStatus.moduleTitle)) {
            var secondTab = getParameter('secondTab');
            if (empty(secondTab) && tab != 'viewOrder' && tab != 'viewQuote') {
                $('#workingOnTitle').html(formStatus.moduleTitle);
            }
        }
    }

}

function loadTab() {

    screenClear();
    $('#menuOptions').hide();

    tab = getTab();



    if (tab === 'guestSpecial') {

        var jsonURLencoded = getParameter('json');
        var json = decodeURIComponent(jsonURLencoded);
        var user = JSON.parse(json);

        var url = "https://" + currentHost + "/api/guest?" + addSite();
        $.post(url, json, function (data) {
            loginGuest(data, user.email);
        });
        return;
    }

    var uri = window.location.href;

    if (tab === 'guest' || tab === 'guestForm') {

        buildNavigation(signInConf);
        if (allowGuests()){
            loadGuest();
        } else {
            loadSignIn();
        }
        return;
    }  else if (tab === 'signIn') {
        buildNavigation(signInConf);
        loadSignIn();
        return;
    }

    if (nonValidUser()) {
        return; 
    };

    if ((tab === 'home' || tab === 'size' || tab === 'glass' || tab === 'hardware' || tab === 'extra' ) && !empty(quote.noCart)  ) {
        formStatus = {};
        quote = {};
        tab = 'home';
    }

    setTitle();

    $('#footer').show();
    $('#cartButton').unbind("click").bind("click", loadCartTopNav);

    selectTab('#' + tab);

    if (tab === 'viewQuote') {
        var secondTab = getParameter('secondTab');
        if (empty(secondTab)) {
            buildNavigation(salesConf);
        }
        loadViewQuote();
        return;
    }
    else if (tab === 'printWindowCutSheet') {
        var id = getParameter('id');
        getCutSheetParts(id);
    }
    else if (tab === 'color') {
        buildNavigation(cartConf);
        loadColorAndFinish();
    } else if (tab === 'viewProductionOrder') {
        buildNavigation(productionConf);
        viewProductionOrder();
    }
    else if (tab === 'accounting' || tab === 'confirmOrders' || tab === 'accountingProduction'
        || tab === 'balanceDue' || tab === 'paymentCompleted' || tab === 'accountingPreProduction'
        || tab === 'accountingCompleted' || tab === 'accountingDelivered' || tab === 'accountingNeedsSurvey'
        || tab === 'accountingHold' || tab == 'verifyDeposit' || tab === 'accountingAll' ) {

        buildNavigation(accountingConf);
        loadAccountingOrders(tab);
    } else if (tab === 'viewAccountingOrder') {
        buildNavigation(accountingConf);
        viewAccountingOrder();
    }
    else if (tab === 'viewAllProduction' || tab === 'viewAllSurvey' || tab === 'viewNeedsSurvey' || tab === 'viewPreProduction' || tab === 'viewProduction' || tab === 'viewCompleted' || tab === 'viewDelivered' || tab === 'viewHold') {
        buildNavigation(productionConf);
        loadProduction(tab);
    } else if (tab === 'glass') {
        buildNavigation(cartConf);
        loadGlass();
    } else if (tab === 'extras') {
        buildNavigation(cartConf);
        loadExtras();
    } else if (tab === 'hardware') {
        buildNavigation(cartConf);
        loadHardware();
    } else if (tab === 'installation') {
        buildNavigation(cartConf);
        loadInstallation();
    } else if (tab === 'size') {
        if (cleanCartLoad()) {
            return;
        }
        buildNavigation(cartConf);
        loadSizeAndPanels();
    }
    else if (tab === 'fixed') {
        if (cleanCartLoad()) {
            return;
        }
        buildNavigation(cartConf);
        loadFixedPanels();
    } else if (tab === 'sash') {
        buildNavigation(cartConf);
        loadSizeAndSash();
    } else if (tab === 'openers') {
        buildNavigation(cartConf);
        loadOpeners();
    } else if (tab === 'door') {
        buildNavigation(cartConf);
        loadProducts();
    } else if (tab === 'newQuote') {
        salesStartOver();
    }
    else if (tab === 'cart') {
        buildNavigation(inCartConf);
        window.doorEditing = false;
        loadCart();
    } 
    else if (tab === 'customer') {
        buildNavigation(salesConf);
        loadCustomers();

    } else if (tab === 'convertToSale') {
        buildNavigation(orderConf);
        convertToSale();
    } else if (tab === 'customerView') {
        if (isProduction()) {
            buildNavigation(productionConf);
        } else {
            buildNavigation(salesConf);
        }
        loadViewCustomer();
    } else if (tab === 'customerEdit') {
        loadEditCustomer();
    }
    else if (tab === 'viewOrders') {
        buildNavigation(salesConf);
        loadOrders();
    } else if (tab === 'viewOrdersNeedsPayment') {
        buildNavigation(salesConf);
        loadOrders('needsPayment');
    }  else if (tab === 'viewOrdersHold') {
        buildNavigation(salesConf);
        loadOrders('holdOrder');
    } else if (tab === 'viewOrdersAccounting') {
        buildNavigation(salesConf);
        loadOrders('accounting');
    } else if (tab === 'viewOrdersPreProduction') {
        buildNavigation(salesConf);
        loadOrders('preProduction');
    } else if (tab === 'viewOrdersProduction') {
        buildNavigation(salesConf);
        loadOrders('production');
    } else if (tab === 'viewOrdersCompleted') {
        buildNavigation(salesConf);
        loadOrders('completed');
    } else if (tab === 'viewOrdersDelivered') {
        buildNavigation(salesConf);
        loadOrders('delivered');
    } else if (tab === 'viewLeads') {
        buildNavigation(leadsConf);
        loadLeads();
    } else if (tab == 'viewAssignedLeads') {
        buildNavigation(leadsConf);
        loadAssignedLeads();
    } else if (tab == 'viewLeadQuote') {
        buildNavigation(leadsConf);
        loadViewQuoteForLeads();
    }
    else if (tab === 'viewOrder') {

        var secondTab = getParameter('secondTab');
        if (empty(secondTab)) {
            buildNavigation(salesConf);
        }
        loadViewOrder();
    } else if (tab === 'viewCustomers') {
        buildNavigation(salesConf);
        loadViewCustomers();
    } else if (tab === 'paymentInfo') {
        if (isProduction()) {
            buildNavigation(productionConf);
        } else if (isAccounting()) {
            buildNavigation(accountingConf);
        } else {
            buildNavigation(salesConf);
        }
        loadPaymentInfo();
    } else if (tab === 'viewQuotes') {
        buildNavigation(salesConf);
        loadQuotes();
    } else if (tab === 'viewQuotesAll') {
        buildNavigation(salesConf);
        loadQuotes('All');
    }
    else if (tab === 'viewQuotesHot') {
        buildNavigation(salesConf);
        loadQuotes('Hot');
    } else if (tab === 'viewQuotesNew') {
        buildNavigation(salesConf);
        loadQuotes('New');
    }
    else if (tab === 'viewQuotesMedium') {
        buildNavigation(salesConf);
        loadQuotes('Warm');
    } else if (tab === 'viewQuotesCold') {
        buildNavigation(salesConf);
        loadQuotes('Cold');
    } else if (tab === 'viewQuotesHold') {
        buildNavigation(salesConf);
        loadQuotes('Hold');
    } else if (tab === 'viewQuotesArchived') {
        buildNavigation(salesConf);
        loadQuotes('Archived');
    }
    else if (tab === 'viewSale') {
        buildNavigation(salesConf);
        loadViewSale();
    } else if (tab === 'viewSavedQuoteConfirmation') {
        writeLocalStorageWithKeyAndData('quoteState', {'started':false});
        buildNavigation(salesConf);
        showSavedQuote();
    } else if (tab === 'home') {
        var module = getParameter('module');
        if (!empty(module)) {
            var idArray = module.split('-');
            renderModuleWithTitle(idArray[1], idArray[0]);
        } else {
            buildNavigation(cartConf);
            loadProducts();
        }
    } else if (tab === 'viewAccount') {
        buildNavigation(salesConf);
        loadAccount();
    } else if (tab === 'signIn') {
        buildNavigation(signInConf);
        loadSignIn();
    } else if (tab === 'viewCutSheet') {
        buildNavigation({});
        loadCutSheet();
    } else if (tab === 'viewGlassSheet') {
        buildNavigation({});
        loadGlassSheets();
    } else if (tab === 'viewGlassLabels') {
        buildNavigation({});
        loadGlassLabels();
    }
    else if (tab === 'viewModules') {
        buildNavigation(builderConf);
        loadViewModules();
    } else if (tab === 'communications') {
        buildNavigation(salesConf);
        loadCommunications();
    } else if (tab === 'viewModule') {
        buildNavigation(builderConf);
        loadViewModule();
    } else if (tab === 'viewHalfButtons') {
        buildNavigation(builderConf);
        loadHalfButtons();
    } else if (tab === 'viewHalfButton') {
        buildNavigation(builderConf);
        loadHalfButton();
    } else if (tab === 'users' || tab === 'frontEndAssets' || tab === 'frontEndInteractions' || tab === 'backEndInteractions' || tab === 'backEndAssets') {
        buildNavigation(builderConf);
        loadBuilder();
    } else if (tab === 'printInvoice') {
        //$('#mainContent').append('here');
        printInvoice();
    }  else if (tab === 'adminModuleSelect') {
        var idTag = getParameter('id');
        var idArray = idTag.split('-');
        renderModuleWithTitle(idArray[1], idArray[0]);
    }else if (tab === 'depositConfirmedMail'){
        loadDepositConfirmedMail();
    }
    else {
        buildNavigation(signInConf);
        if (allowGuests()){
            loadGuest();
        } else {
            loadSignIn();
        }
    }
}


function cleanCartLoad() {
    
    return false;
    if (quote.id) {
        startOver();
        return true;
    }

    return false ;
}


function determineCorrectTab() {

    var print = getParameter('print');
    if (print === 'yes') {
        return;
    }

    if (user.type == 'new' &&  tab != 'signIn') {
        user = {type: 'guest'};
        tab = "guest";
    }

    if (empty(user.token)) {
        if (user.type == 'salesPerson' || tab == 'signIn') {
            tab = "signIn";
        } else {
            tab = "guest";
        }
    }
}


function isAnOption(tab) {
    if (!empty(moduleInfo.nav)) {
        for (var a = 0; a < moduleInfo.nav.length; a++) {

            var optionCheck = moduleInfo.nav[a];

            if (optionCheck.idTag == tab) {
                return true;
            }

        }
    }
    return false;
}

function validatePageOptions(tab) {

    if ( tab === "hardware" ) {
        if (!isGlassComplete()) {
            missingSelections();
            return false;
        }
    }

    if (tab === "glass" || tab === "hardware" ) {
        if (!isExtraComplete()) {
            missingSelections();
            return false;
        }
        if(!checkExtraSelections()){
            return false;
        }
    }

    if (tab === "glass" || tab === "hardware" || tab === "extras") {
        if (!isColorComplete()) {
            missingSelections();
            return false;
        }
    }

    if ((tab === "size" && window.doorEditing) || tab === "color" || tab === "glass" || tab === "hardware" || tab === "extras") {
        if (!isSizeComplete()) {
            missingSelections();
            return false;
        }
    }

    if(tab === 'cart'){
        return false;
    }

    if (!validScreenUS()) {
        return false;
    }

    return true;
}

function validScreenUS() {

    if (empty(formStatus.panelOptions)) {
        return true;
    }

    if (moduleInfo.windowsOnly == 1) {
        return true;
    }

    if (parseInt(formStatus.height) > 96 ) {
        if (!empty(formStatus.panelOptions.screens)  ) {
            if (hasValue(formStatus.panelOptions.screens.selected, 'Panoramic')) {
                formStatus.errors['screens'] = "Please make a screen selection"
                missingSelections();
                return false;
            }
        }
    }

    if (!empty(formStatus.panelOptions.screens) && !isGuest() ) {
        if (empty(formStatus.panelOptions.screens.selected)) {
            formStatus.errors['screens'] = "Please make a screen selection"
            missingSelections();
            return false;
        }
    }

    return true;
}

function hasValue(variable, test) {

    var regex1 = RegExp(test);

    if (regex1.test(variable)  ) {
        return true;
    }

    return false;
}


function processClick(tab) {

    newTab = tab;
    if (!empty(formStatus.newWidth)) {
        runCheckingWidth(processClick);
        return;
    }

    if (!validatePageOptions(tab)) {
        return;
    }

    var quote = getParameter('quote');
    var order = getParameter('order');
    var cartItem = getParameter('cartItem');

    if (!empty(quote) && isNotMainNavigation(tab) ) {
        var secondTab = tab;
        tab = getParameter('tab');
        trackAndDoClick(tab, "&quote=" + quote + "&edit=item&cartItem=" + cartItem + "&secondTab=" + secondTab);
    }
    else if (!empty(order) && isNotMainNavigation(tab) ) {
        var secondTab = tab;
        tab = getParameter('tab');
        trackAndDoClick(tab, "&order=" + order + "&edit=item&cartItem=" + cartItem + "&secondTab=" + secondTab);
    }
    else {
        setTab(tab);
        trackAndDoClick(tab);
    }
}


function isNotMainNavigation(tab) {

    var mainNav = ['newQuote', 'viewQuotes', 'viewCustomers', 'communications','viewOrders',
    'viewLeads', 'viewAssignedLeads'] ;

    for (var a = 0; a < mainNav.length; a++) {
        if (tab == mainNav[a]) {
            return false;
        }
    }

    return true;
}

function tabClick() {

    var tab = $(this).attr('id');
    startSpinner();
    processClick(tab);
}


function navigationClick() {
    var tab = $(this).attr('id');
    startSpinner();
    setTab(tab);
    trackAndDoClick(tab);
}


