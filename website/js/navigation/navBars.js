'use strict'

function noNavBars() {

    //$('#preMainContent').hide();
    //$('#mainContent').css({"margin-top": "88px"});

}


function statusNavHTML() {

    var cartImage = cartSVG();
    var backImage = backSVG();

    var userMenu = userMenuHTML();


    var html = '' +
        '<div id="screen"></div>' +
        '<div id="menuOptionsBox">' +
        userMenu +
        '</div>' +
        '<div id="salesAdminMasterBox"></div>' +
        '<div id="headerNav">' +
        '   <div id="startOverButton" onclick="startOver()">' +
        '       <div id="headerStartOverImage" >' + backImage + '</div>' +
        '       <div id="headerStartOver" ' + cssFont + ' >Start Over</div>' +
        '   </div>' +
        '   <div id="workingOnTitle" ' + cssFont + ' ></div>' +
        '   <div id="headerRightMenu" ' + cssFont + ' onclick="showMenuOptions()" ></div>' +
        '   <div id="cartButton">' +
        '       <div id="headerCart" ' + cssFont + ' >Cart (<span id="cartTotal">0</span>)</div>' +
        '       <div id="headerCartImage" ' + cssFont + '>' + cartImage + '</div>' +
        '   </div>' +
        '</div>';

    return html;
}


function cartNavHTML() {

    var html = '<div id="cartNav">';

    var count = 0;
    for (var index in moduleInfo.nav) {

        var first = "";
        if (count == 0 ) {
            first = "topNavSectionFirst";
        }
        count++;

        var name = moduleInfo.nav[index].name;
        var idTag = moduleInfo.nav[index].idTag;

        html += '<div id="' + idTag +'" class="topNavSection ' + first + '  "  '+cssBorder +' >' + name + '</div>' ;
    }

    return html;
}


function salesNavHTML() {


    var columns = 3;

    if (siteDefaults.showCustomers == 1) {
        columns = columns + 1;
    }
    if (siteDefaults.showCommunication == 1) {
        columns = columns + 1;
    }

    var size = 953 / columns;
    var specialStyle = 'style="border-color:' + siteDefaults.cssBorder + ';width: ' + size +'px"';


    var customerHTML = '' +
        '<div id="viewCustomers" class="productNavSection" '+ specialStyle +'> View Customers </div>';
    var communicationHTML = '' +
        '<div id="communications" class="productNavSection" '+ specialStyle +'> Communications </div>' ;

    if (siteDefaults.showCustomers == 0) {
        customerHTML = '';
    }
    if (siteDefaults.showCommunication == 0) {
        communicationHTML = '';
    }



    var html = '' +
        '<div id="salesNav">' +
        '   <div id="newQuote"  class="productNavSection topNavSectionFirst" '+ specialStyle +'> New Quote </div>' +
        '   <div id="viewQuotes" class="productNavSection "  '+ specialStyle +'>View Quotes</div>' +
        '   <div id="viewOrders" class="productNavSection" '+ specialStyle +'> View Orders </div>' +
        customerHTML +
        communicationHTML +
        '</div>';

    return html;
}


function leadsNavHTML() {

    var html = '' +
        '     <div id="leadsNav" >' +
        '<div  id="viewLeads" class="productNavSection topNavSectionFirst leadsNavSectionWidth" ' + cssBorder + '> New </div>' +
        '<div  id="viewAssignedLeads" class="productNavSection leadsNavSectionWidth" ' + cssBorder + '> Assigned </div>' +
        '</div>';

    return html;
}


function accountingNavHTML() {

    var html = '' +
        '        <div id="accountingNav">' +
        '<div  id="confirmOrders" class="prductNavSection topNavSelected topNavSectionFirst" ' + cssBorder + '> Confirm Orders </div>' +
        '<div  id="readyForProduction" class="productNavSection " ' + cssBorder + '> In Pre-Production </div>' +
        '<div  class="productNavSection" ' + cssBorder + '> </div>' +
        '<div class="productNavSection" ' + cssBorder + '> </div>' +
        '<div class="productNavSection" ' + cssBorder + '> </div>' +
        '</div>';

    return html;
}


function productionNavHTML() {

    if(isSurvey()){
        var html = '' +
            '        <div id="productionNav">' +
            '<div id="viewAllProduction" class="productionActions survey orderStatusLeft ">View All</div>' +
            '<div id="needsSurvey" class="productionActions survey prodNeedsSurvey ">Needs Survey</div>' +
            '<div id="viewPreProduction" class="productionActions survey prodPreProduction ">Pre-Production</div>' +
            '<div class="productNavSection"> </div>' +
            '</div>';
    }else{
        var html = '' +
            '        <div id="productionNav">' +
            '<div  id="viewPreProduction" class="productNavSection topNavSelected topNavSectionFirst" ' + cssBorder + '> Pre-Production </div>' +
            '<div  id="viewProduction" class="productNavSection " ' + cssBorder + '> In Production </div>' +
            '<div  id="viewCompleted" class="productNavSection " ' + cssBorder + '> Completed </div>' +
            '<div  id="viewDelivered" class="productNavSection " ' + cssBorder + '> Delivered </div>' +
            '<div class="productNavSection"> </div>' +
            '</div>';
    }

    return html;
}


function builderNavHTML() {

    var html = '' +
        '<div id="builderNav">' +
        '<div  id="users" class="productNavSection topNavSelected topNavSectionFirst"> Users </div>' +
        '<div  id="frontEndAssets" class="productNavSection topNavSelected "> Front End Assets </div>' +
        '<div  id="frontEndInteractions" class="productNavSection "> Front End Interactions </div>' +
        '<div  id="backEndInteractions" class="productNavSection "> Back End Interactions </div>' +
        '<div  id="backEndAssets" class="productNavSection "> Back End Assets </div>' +
        '</div>';

    return html;
}
