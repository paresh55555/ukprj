'use strict'

function attachStartOverButtonWithId(id) {

    var html = startOverButtonHTML();
    attachHtmlWithId(html, id);
}


function startOverButtonHTML() {

    var html = '<div id="headerStartOverImage">' + backSVG() + '</div>' +
        '<div id="headerStartOver" ' + cssFont + ' onclick="startOver()">Start Over</div>';

    return html
}


function attachHtmlWithId(html, id) {

    if (!empty(id)) {
        var selector = buildIDSelector(id);
        $(selector).html(html);
    } else {
        console.log(html, "ID wasn't passed");
    }
}


function makeNextSectionButton() {

    var action = '';
    var title = '';

    var match = tab;
    var secondTab = getParameter('secondTab');
    if (!empty(secondTab)) {
        match = secondTab;
    }

    if (!empty(moduleInfo.nav)) {
        for (var a = 0; a < moduleInfo.nav.length; a++) {
            if (moduleInfo.nav[a].idTag == match) {
                action = moduleInfo.nav[a].action;
                title = moduleInfo.nav[a].nextTitle;
                break;
            }
        }
    }

    var nextIcon = nextSVG();
    var html = '' +
        '<div class="nextButton" onclick="' + action + '">' +
        '   <div class="nextTitle" ' + cssFont + '>' + title + '</div>' +
        '   <div class="nextImage">' +
        '   ' + nextIcon +
        '   </div>' +
        '</div>';

    $('#nextSection').html(html);
}