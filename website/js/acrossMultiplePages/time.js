'use strict'


function currentTimeInSeconds() {

    var milliseconds = (new Date).getTime();
    var seconds = milliseconds / 1000;

    return parseInt(seconds);
}

Number.prototype.padLeft = function(base,chr){
    var  len = (String(base || 10).length - String(this).length)+1;
    return len > 0? new Array(len).join(chr || '0')+this : this;
}


function add_business_days(passedDate, days) {
    var now = new Date(passedDate);
    var dayOfTheWeek = now.getDay();
    var calendarDays = days;
    var deliveryDay = dayOfTheWeek + days;
    if (deliveryDay >= 6) {
        //deduct this-week days
        days -= 6 - dayOfTheWeek;
        //count this coming weekend
        calendarDays += 2;
        //how many whole weeks?
        var deliveryWeeks = Math.floor(days / 5);
        //two days per weekend per week
        calendarDays += deliveryWeeks * 2;
    }
    now.setTime(now.getTime() + calendarDays * 24 * 60 * 60 * 1000);

    var newDate = (now.getMonth()+1).padLeft() + '/' + now.getDate().padLeft()+ '/' + now.getFullYear();
    return newDate;
}
