'use strict'

//use on SizeAndPanels
function buildChooseNumberOfPanels(numberOfPanels, maxSize) {

    var doorImages = buildDoorPanels(numberOfPanels, maxSize);

    var id = '#panelSection';
    var buttonID = 'panels-' + numberOfPanels + '" data-button="' + numberOfPanels;

    var label = numberOfPanels + ' Panel';
    attachButton(label, doorImages, id, buttonID);
}


function buildDoorPanels(numberOfPanels, buttonWidth) {

    var doorImageWidth = buttonWidth / numberOfPanels;
    var doorImages = '';
    var blankPanel = '<img src="images/slide-panel.gif" width="' + doorImageWidth + '" height="120" >';

    for (var a = 0; a < numberOfPanels; a++) {
        doorImages = doorImages + blankPanel;
    }

    return doorImages;
}


function buildPanelMovementButtons(numberOfPanels, allowBoth) {

    var selected =formStatus.panelOptions.swingDirection.selected;

    if (selected == 'left') {
        var options = buildLeftPanelMovement(numberOfPanels);
        return options;
    }

    if (selected == 'right') {
        var options = buildRightPanelMovement(numberOfPanels);
        return options;
    }

    if (selected == 'both') {
        var options = buildBothPanelMovement(numberOfPanels);
        return options;
    }

}

function buildLeftPanelMovement(numberOfPanels) {

    var selected = '';
    var imagesData = getPanelImages(numberOfPanels);
    var doorImages = '';

    if (formStatus.panelOptions.movement.selected == 'left') {
        selected = buildPanelsSelected(numberOfPanels)
    }

    var doorImages = imagesData.swingLeft;
    for (var a = 0; a < numberOfPanels - 1; a++) {
        doorImages = doorImages + imagesData.slideLeft;
        if (a == numberOfPanels - 2  ) {

            if (isUK()) {
                doorImages = doorImages + imagesData.divider;
            } else {
                doorImages = doorImages ;
            }
        }
    }

    var label ='Left'
    var buttonID = "movement-left-0";
    var button = buildButton(label, doorImages, buttonID, selected);

    return button;
}


function buildRightPanelMovement(numberOfPanels) {

    var selected = '';
    var imagesData = getPanelImages(numberOfPanels);
    var doorImages = '';

    if (formStatus.panelOptions.movement.selected == 'right') {
        selected = buildPanelsSelected(numberOfPanels)
    }

    var doorImages = ''
    for (var a = 0; a < numberOfPanels - 1; a++) {
        if (a == 0 ) {

            if (isUK()) {
                doorImages = doorImages + imagesData.divider;
            } else {
                doorImages = doorImages ;
            }
        }
        doorImages = doorImages + imagesData.slideRight;
    }
    doorImages = doorImages + imagesData.swingRight;

    var label ='Right'
    var slideLeft = parseInt(numberOfPanels) - 1;
    var buttonID = "movement-right-"+slideLeft;
    var button = buildButton(label, doorImages, buttonID, selected);

    return button;
}


function buildBothPanelMovement(numberOfPanels) {

    var special = getParameter('special');



    var startNumber = 1;
    var reduceSliderOptions = 2;

    if (special == 'movement' || siteDefaults.minGapForSwing == 0) {
        startNumber = 0;
        reduceSliderOptions = 1;
    }



    var buttons  = '';
    for (var a = startNumber; a < numberOfPanels - reduceSliderOptions; a++) {

        var button = buildSinglePanelMovement(numberOfPanels, a);
        buttons = buttons + button;
    }

    return buttons;
}

function buildSinglePanelMovement(numberOfPanels, leftNumber) {

    var selected = '';
    var imagesData = getPanelImages(numberOfPanels);
    var doorImages = '';

    if (formStatus.panelOptions.movement.numberLeft == leftNumber) {
        selected = buildPanelsSelected(numberOfPanels)
    }

    var doorImages = imagesData.swingLeft;
    for (var a = 0; a < leftNumber ; a++) {
        doorImages = doorImages + imagesData.slideLeft;
    }

    if (isUK()) {
        doorImages = doorImages + imagesData.divider;
    } else {
        doorImages = doorImages ;
    }


    for (var a = leftNumber; a < numberOfPanels - 2  ; a++) {
        doorImages = doorImages + imagesData.slideRight;
    }

    doorImages = doorImages + imagesData.swingRight;

    var slideRight = parseInt(numberOfPanels) - 2 - parseInt(leftNumber);

    var label ='Slide Left '+ leftNumber + ' / Slide Right '+  slideRight;

    var buttonID = "movement-both-"+leftNumber;
    var button = buildButton(label, doorImages, buttonID, selected);

    return button;
}


function buildPanelsSelected(numberOfPanels) {

    return  buildSwingSelected(numberOfPanels)

}
function getPanelImages(numberOfPanels) {

    var maxSize = panelMaxSize(numberOfPanels);
    var width = maxSize / numberOfPanels;

    var images = {
        "slideLeft": '<img src="images/slide-left-w-arrow.gif" width="' + width + '" height="120">',
        "swingLeft": '<img src="images/swing-left.gif" width="' + width + '" height="120">',
        "divider": '<img src="images/Divider-Bar.gif">',
        "slideRight": '<img src="images/slide-right-w-arrow.gif" width="' + width + '" height="120">',
        "swingRight": '<img src="images/swing-right.gif" width="' + width + '" height="120">'
    }

    return images;
}