'use strict'


function buildSwingDoorButtons(numberOfPanels) {


    var left = buildLeftSwingDirection(numberOfPanels);
    var right = buildRightSwingDirection(numberOfPanels);
    var both = buildSwingDoorBothDirections(numberOfPanels);

    return left + right + both;


}



function buildLeftSwingDirection(numberOfPanels) {

    var selected = '';
    var imagesData = getSwingImages(numberOfPanels);
    var doorImages = '';

    if (formStatus.panelOptions.swingDirection.selected == 'left') {
        selected = buildSwingSelected(numberOfPanels)
    }

    var doorImages = imagesData.swingLeft;
    for (var a = 0; a < numberOfPanels - 1; a++) {
        doorImages = doorImages + imagesData.panel;
    }

    var label ='Left'
    var buttonID = "swingDirection-left";
    var button = buildButton(label, doorImages, buttonID, selected);

    return button;
}


function buildRightSwingDirection(numberOfPanels) {

    var selected = '';
    var imagesData = getSwingImages(numberOfPanels);
    var doorImages = '';

    if (formStatus.panelOptions.swingDirection.selected == 'right') {
        selected = buildSwingSelected(numberOfPanels)
    }

    for (var a = 0; a < numberOfPanels - 1; a++) {
        doorImages = doorImages + imagesData.panel;
    }
    var sidePanels = numberOfPanels - 1;
    doorImages = doorImages + imagesData.swingRight;
    var label = 'Right';
    var buttonID = "swingDirection-right";

    var button = buildButton(label, doorImages, buttonID, selected);
    return button;
}


function buildSwingDoorBothDirections(numberOfPanels) {

    if (moduleInfo.doubleSwing != 1 || numberOfPanels < siteDefaults.minDoorPanelsDoubleSwing) {
        return '';
    }

    if (numberOfPanels < 2 ) {
        return '';
    }

    var selected = '';
    var imagesData = getSwingImages(numberOfPanels);
    var doorImages = '';

    if (formStatus.panelOptions.swingDirection.selected == 'both') {
        selected = buildSwingSelected(numberOfPanels)
    }

    var doorImages = imagesData.swingLeft;
    for (var a = 0; a < numberOfPanels - 2 ; a++) {
        doorImages = doorImages + imagesData.panel;
    }

    doorImages = doorImages + imagesData.swingRight;

    var label = 'Both';
    var buttonID = "swingDirection-both";

    var button = buildButton(label, doorImages, buttonID, selected);

    return button;
}


function buildSwingSelected (numberOfPanels) {

//    var offset = (numberOfPanels * 50 / 2 ) - 21;

    var selected = '' +
        '<div class="selectedDiv"> ' +
//        '   <img src="images/icon-selected.svg" style="margin-left: ' + offset + 'px; margin-top: -112px; z-index: 10; float: left;"> ' +
        '   <img src="images/icon-selected.svg"> ' +
        '</div>';

    return selected
}

function getSwingImages(numberOfPanels) {

    var maxSize = panelMaxSize(numberOfPanels);
    var width = maxSize / numberOfPanels;

    var imagesData = {
        "swingLeft": '<img src="images/swing-left.gif" width="' + width + '" height="120">',
        "panel": '<img src="images/slide-panel.gif" width="' + width + '" height="120">',
        "swingRight": '<img src="images/swing-right.gif" width="' + width + '" height="120">'
    }

    return imagesData;
}



function buildInSwingOrOutSwing() {

    var id = "#swingDirectionSection";
    var disabled = true;

    var swingDirectionImage = {
        left: {inside: "images/swing/no in left.svg", outside: "images/swing/Left Out.svg"},
        right: {inside: "images/swing/no in right.svg", outside: "images/swing/Right Out.svg"},
        both: {inside: "images/swing/no in both.svg", outside: "images/swing/Both Out.svg"}
    }

    var frame = formStatus.panelOptions.frame.selected;

    if (frame == 'Aluminum Block' || frame == 'Vinyl Block') {
        swingDirectionImage = {
            left: {inside: "images/swing/Left In.svg", outside: "images/swing/Left Out.svg"},
            right: {inside: "images/swing/Right In.svg", outside: "images/swing/Right Out.svg"},
            both: {inside: "images/swing/Both In.svg", outside: "images/swing/Both Out.svg"}
        }
        disabled = false;
    }
    
    if(formStatus.doorStyle){
        swingDirectionImage = {
            left: {inside: "images/swing/Inside Left.svg", outside: "images/swing/Outside Left.svg"},
            right: {inside: "images/swing/Inside Right.svg", outside: "images/swing/Outside Right.svg"},
            both: {inside: "images/swing/Both In.svg", outside: "images/swing/Both Out.svg"}
        };
        disabled = false;
    }

    var image = swingDirectionImage[formStatus.panelOptions.swingDirection.selected];

    if (image) {
        var button1 = {};
        button1.url = image.outside;
        button1.name = "Outswing";
        button1.description = "Panels swing and stack to outside of opening.";

        var button2 = {};
        button2.url = image.inside;
        button2.name = "Inswing";
        button2.description = "Panels swing and stack to inside of opening.";
        button2.disabled = disabled

        var buttons = [button1, button2];

        var buttonData = {};
        buttonData.buttons = buttons;
        buttonData.nameOfOption = 'swingInOrOut';
        buttonData.selected = formStatus.panelOptions.swingInOrOut.selected;

        var buttons = buildHalfButtons(buttonData);

        return buttons;
    }
}

function buildScreensOptions() {


    if (empty(formStatus.panelOptions.screens)) {
        return '';
    }

    var left =  {"url":"images/screens/Screen Left 121.png","direction":"left"};
    var right = {"url":"images/screens/Screen Right 121.png","direction":"right"};
    var split = {"url":"images/screens/Screen Split 121.png","direction":"split"};
    var noScreen = {"url":"images/screens/no_screens.png","direction":" "};


    var buttonTypes = [
        {"name":"Panoramic", "type":left},
        {"name":"Brio", "type":left},
        {"name":"Panoramic", "type":right},
        {"name":"Brio", "type":right},
        {"name":"Panoramic", "type":split},
        {"name":"Brio", "type":split},
        {"name":"No", "type":noScreen},
    ];


    var buttons = [];

    for(var index = 0; index < buttonTypes.length; index++) {

        var button = {} ;

        button.url = buttonTypes[index].type.url;
        button.description = "Collects to the " + buttonTypes[index].type.direction;
        if (buttonTypes[index].type.direction == "split") {
            button.description = "Collects to both sides";
        }
        if (buttonTypes[index].type.direction == " ") {
            button.description = '';
        }
            button.name = buttonTypes[index].name + ' Screen '+ capitaliseFirstLetter(buttonTypes[index].type.direction);

        if (parseInt(formStatus.height) <= 96 || buttonTypes[index].name == 'Brio' || buttonTypes[index].name == 'No'  ) {
            buttons.push(button);
        }

    }

    var buttonData = {};
    buttonData.buttons = buttons;
    buttonData.nameOfOption = 'screens';
    buttonData.selected = formStatus.panelOptions.screens.selected;

    var buttonsHTML = buildHalfButtons(buttonData);

    return buttonsHTML;

}
