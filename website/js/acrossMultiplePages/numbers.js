'use strict'

function numbersOnly (valueAsString) {

    if (empty(valueAsString)) {
        return '';
    }

    //valueAsString = valueAsString.replace(/\D/g, '');

    if(typeof valueAsString === 'string'){
        return valueAsString.replace(/^0+/, '');
    }else{
        return parseFloat(valueAsString);
    }
}


function validQuantity(quantity) {

    if (empty(quantity) ) {
        return '';
    }
    quantity = quantity.replace(/\D/g, '');

    if (quantity < 1) {
        quantity = 1;
    }
    if (quantity > 99) {
        quantity = 99;
    }
    return quantity;
}
