'use strict'

function attachButton(label, doorImages, id, buttonID) {

    var button = buildButton(label, doorImages, buttonID)
    $(id).append(button);
}


function buildButton(label, doorImages, buttonID, selected) {

    var customStyle = 'style="background-color:#44ac61;color:#FFFFFF"';
    if (empty(selected)) {
        customStyle = '';
    }

    var button =
        '<div id="' + buttonID + '" class="button" >' +
        '   <div class="buttonImage">' +
        '       ' + doorImages +
        '   </div>' +
        '   <div class="buttonLabel" ' + customStyle + '>' + label + '</div>' +
        '   ' + selected +
        '</div>';

    return button;
}

//replace
function buildExtendedHalfButton(button, option, disabled, defaultSelected) {

    disabled = typeof disabled !== 'undefined' ? disabled : '';
    defaultSelected = defaultSelected || false;

    var id = option + '-' + button.name;

    if (!button.highlight) {
        button.highlight = '';
    }

    if (button.name != "Inswing") {
        disabled = '';
    }

    var button =
        ['<div id="' + id + '" data-extra="' + button.extra + '" class="buttonRef extendedHalfButton' + disabled + '">',
            '<div class="extendedHalfButtonImage buttonImageRef">'+
                '<img src="' + button.url + '">'+
                (defaultSelected ? '<div class="extendedSelectedDiv selectedDivRef"><img src="images/icon-selected.svg"></div>' : '')+
            '</div>',
            '<div class="extendedHalfButtonTitle">' + button.name + '</div>',
            '<div class="extendedHalfButtonDescription">' + button.description + '</div>',
            '<div class="extendedHalfButtonHighlight">' + button.highlight + '</div>',
            '</div>'
        ].join('\n');


    return (button);
}



function displaySelected(selector) {

    
    var height = $(selector).height();
    var width = $(selector).width();

    if (width < 10) {
        width = 250;
    }
    if (height < 10) {
        height = 120;
    }

    var id = $(selector).attr('id');

//    var newWidth = width / 2 - 21;
//    var newHeight = -1 * (height / 2 + 35);

//    var marginLeft = newWidth + 'px';
//    var marginTop = newHeight + 'px';

    var checkImage = $('<img>', {
        src: 'images/icon-selected.svg'
    });

//    checkImage.css({"margin-left": marginLeft, "margin-top": marginTop, "z-index": 10, "float": "left"});

    var groupOfButtons = $(selector).parent();
    if ($(".selectedDiv", groupOfButtons).length > 0) {
        $(".selectedDiv", groupOfButtons).remove();
        $('.buttonLabel', groupOfButtons).css({"background-color": "#FFFFFF", "color": "#000000"});
    }

    $('.buttonLabel', selector).css({"background-color": "#44ac61", "color": "#FFFFFF"});

    jQuery('<div/>', {
        class: 'selectedDiv'
    }).appendTo($(selector));

    checkImage.appendTo($(".selectedDiv", selector));
}


function buildHalfButtons(optionData, group) {

    var buttonData = optionData.buttons;
    var buttons = '';
    for (var a = 0; a < buttonData.length; a++) {
        var buttonInfo = buttonData[a];
        buttonInfo.nameOfOption = optionData.nameOfOption;
        buttonInfo.selected = optionData.selected;

        var button = buildHalfButton(buttonInfo);
        if (buttonInfo.myGroup == group || group == 0 ) {
            buttons = buttons + button;
        }
    }

    return buttons;
}


function buildHalfButton(button) {

    var disableClickingButton = '';
    var id = button.nameOfOption + '-' + button.name;

    if (!button.highlight) {
        button.highlight = '';
    }

    var selected = '' +
        '<div class="extendedSelectedDiv">' +
        '   <img src="images/icon-selected.svg"> ' +
        '</div>';

    if (button.name != button.selected) {
        selected = '';
    }
    if (button.disabled) {
        disableClickingButton = "Disabled";
        selected = '';
    }

    var myButton = '' +
        '<div id="' + id + '" data-extra="' + button.extra + '" class="extendedHalfButton' + disableClickingButton + '">' +
        '   <div class="extendedHalfButtonImage">' +
        '       <img src="' + button.url + '"> ' +
        '' + selected +
        '   </div>' +
        '   <div class="extendedHalfButtonTitle">' + button.name + '</div>' +
        '   <div class="extendedHalfButtonDescription">' + button.description + '</div>' +
        '   <div class="extendedHalfButtonHighlight">' + button.highlight + '</div>' +
        '</div>';

    return myButton;
}


function getGroupForButtons(buttons, selected ) {

    var group = 0;
    for (var a = 0; a < buttons.length; a++) {
        var button= buttons[a];
        if (button.name == selected) {
            group = button.myGroup;
        }
    }
    return group;
}

function halfButtonClick() {

    var groupOfButtons = $(this).parent();
    var nextSection = $(this).parent().parent().next();
    var thisSection = $(this).parent().parent();



    var optionSelectedId = $(this).attr('id');
    var optionSelected = splitForSecondOption(optionSelectedId);
    var option = splitForFirstOption(optionSelectedId)


    formStatus.panelOptions[option].selected = optionSelected;
    nextSection.fadeIn(300);
    nextSection.removeClass('hidden');
    scrollToNextPositon(nextSection);

    if (option == 'frame') {
        if (!notActiveOption('swingInOrOut')) {
            formStatus.panelOptions.swingInOrOut.selected = '';
        }
        var html = swingInOrOutOptionsHMTL();
        $('#swingInOrOut').replaceWith(html);
        var group = getGroupForButtons(formStatus.panelOptions[option].buttons, optionSelected);
        var htmlTrack = trackOptionsHMTL(group);
        $('#track').replaceWith(htmlTrack);
    }

    $('.extendedHalfButton').unbind("click").bind("click", halfButtonClick);
    $('.button').unbind("click").bind("click", selectButton);

    var selectedDiv = $(".extendedSelectedDiv", thisSection);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
    }
    var selector = $('.extendedHalfButtonImage', this);
    displayExtendedSelected(selector);
    updateFormPrice();
}


function selectButton() {

    var groupOfButtons = $(this).parent();
    var thisSection = $(this).parent().parent();
    var optionSelectedId = $(this).attr('id');

    var optionSelected = splitForSecondOption(optionSelectedId);
    var option = splitForFirstOption(optionSelectedId)

    if(option == 'track' && formStatus.doorStyle){
        formStatus.panelOptions[option].selected = optionSelected;
        formStatus.numberOfPanels = 1;
        formStatus.panelOptions.panels.selected = 1;
        if (!notActiveOption('swingDirection')) {
            formStatus.panelOptions.swingDirection.selected = '';
        }
        if (!notActiveOption('movement')) {
            formStatus.panelOptions.movement.selected = '';
            formStatus.panelOptions.movement.numberLeft = '';
        }
        if (typeof formStatus.hardwareExtraChoices['extras-Door Restrictor'] !== 'undefined' && isHideDoorRestrictor()) {
            formStatus.hardwareExtraChoices['extras-Door Restrictor'] = 'no';
        }
    }
    if (option == 'panels') {

        if (formStatus.panelOptions.panels.selected != optionSelected) {
            formStatus.numberOfPanels = optionSelected;
            formStatus.panelOptions.panels.selected = optionSelected;

            if (!notActiveOption('swingDirection')) {
                formStatus.panelOptions.swingDirection.selected = '';
            }
            if (!notActiveOption('movement')) {
                formStatus.panelOptions.movement.selected = '';
                formStatus.panelOptions.movement.numberLeft = '';
            }
            if (typeof formStatus.hardwareExtraChoices['extras-Door Restrictor'] !== 'undefined' && isHideDoorRestrictor()) {
                formStatus.hardwareExtraChoices['extras-Door Restrictor'] = 'no';
            }

            var html = swingDirectionHMTL(formStatus.numberOfPanels);

            $('#swingDirection').replaceWith(html)
            $('#movement').hide();
            $('#swingInOrOut').hide();
        }

        //$('.selectedDiv','#swingSection').remove();
    }

    if (option == 'swingDirection') {
        var updateDoorStyle = false;
        if(formStatus.doorStyle && formStatus.panelOptions.swingDirection.selected !== optionSelected){
            updateDoorStyle = true;
        }
        formStatus.panelOptions.swingDirection.selected = optionSelected;
        if (notActiveOption('movement')) {

            if (optionSelected == 'left') {
                formStatus.panelOptions.movement.numberLeft = Number(formStatus.panelOptions.panels.selected) - 1
                formStatus.panelOptions.movement.selected = 'right';
            } else {
                formStatus.panelOptions.movement.numberLeft = 0;
                formStatus.panelOptions.movement.selected = 'left';
            }

        }
        var html = panelMovementOptionsHMTL();
        $('#movement').replaceWith(html)
        html = swingInOrOutOptionsHMTL()
        $('#swingInOrOut').replaceWith(html);
        
        if(updateDoorStyle){
            html = trackOptionsHMTL();
            $('#track').replaceWith(html);
        }
    }

    if (option == 'movement') {
        
        var numberLeft = splitForThirdOption(optionSelectedId);
        formStatus.panelOptions.movement.selected = optionSelected;
        formStatus.panelOptions.movement.numberLeft = numberLeft;
    }

    var nextSection = $(this).parent().parent().next();
    var nextOption = nextSection.attr('id');

    if (notActiveOption(nextOption) && nextOption != 'nextSection') {
        nextSection.hide();
    } else {
        nextSection.fadeIn(300);
        nextSection.removeClass('hidden');
        scrollToNextPositon(nextSection);
    }

    $('.extendedHalfButton').unbind("click").bind("click", halfButtonClick);
    $('.button').unbind("click").bind("click", selectButton);

    writeFormStatus();
    displaySelected(this);

    updateFormPrice();
}