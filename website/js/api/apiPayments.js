

function apiMakePayment(payment) {

    var json = JSON.stringify(payment);
    var url = "https://" + apiHostV1 + "/api/makePayment?"+authorize() + addSite() + addSuperToken();

    return requestUrlAndLogIfErrorPromiseForPost(url, json, 'Failed to Make Payment');
}


function apiDeletePayment(payment) {

    var json = JSON.stringify(payment);
    var url = "https://" + apiHostV1 + "/api/deletePayment?"+authorize() + addSite() + addSuperToken();

    requestUrlAndLogIfErrorPromiseForPut(url, json, "Delete Payment Failed");
}


function apiVerifiedPayment(payment) {

    var json = JSON.stringify(payment);
    var url = "https://" + apiHostV1 + "/api/verifiedPayment?"+authorize() + addSite() + addSuperToken();

    requestUrlAndLogIfErrorPromiseForPut(url, json, "Verified Payment Failed");
}

function apiNotVerifiedPayment(payment) {

    var json = JSON.stringify(payment);
    var url = "https://" + apiHostV1 + "/api/notVerifiedPayment?"+authorize() + addSite() + addSuperToken();

    requestUrlAndLogIfErrorPromiseForPut(url, json, "Not Verified Payment Failed");
}


