
function setPaidInFull(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/confirmFullPayment?" + authorizePlusAccounting() + addSite();

    return requestUrlAndLogIfErrorPromise(url,'Error Marking Paid In Full');
}

function postPaidInFullAndEmailInvoice(id, json) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/confirmFullPayment?" + authorizePlusAccounting() + addSite();

    return requestUrlAndLogIfErrorPromiseForPost(url,json,'Error Post Paid In Full');
}

function setRevertPaidInFull(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/revertPaidInFull?" + authorizePlusAccounting() + addSite();

    return requestUrlAndLogIfErrorPromise(url,'Error Marking Paid In Full');
}
