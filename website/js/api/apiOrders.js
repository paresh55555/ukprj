

function getOrderAPIWithNewUrlAndOrder(newUrl, orderID) {


    var url = "https://" + apiHostV1 + "/api/orders/" + orderID + "?" + authorizePlusSalesPerson() + addSite() + addSuperToken();
    if (!empty(newUrl)) {
        url = newUrl;
    }

        //var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/cutSheets?" + authorize() + '&site=' + site ;
    return requestUrlAndLogIfErrorPromise(url,'Error Getting Order');
}


function purchaseDoor(ccValues) {

    var json = JSON.stringify(ccValues);
    var url = "https://" + apiHostV1 + "/api/chargeCard?"+authorizePlusSalesPerson() + addSite() + addSuperToken();

    return requestUrlAndLogIfErrorPromiseForPost(url, json, 'Error Purchasing Door');
}