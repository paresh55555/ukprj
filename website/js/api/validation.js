'use strict'

function validate(data) {

    var valid = true;
    if (data) {
        if (data.status) {
            if (data.status === 'not authorized') {
                valid = false;
                bounceUser();
            }
        }
    }

    return valid;
}


function nonValidUser() {

    var token = getParameter('token');
    
    if (empty(user.token) && empty(token)) {
        bounceUser();
        return true;
    }

    return false;
}

function bounceUser() {
    resetFormAndCookies();
    if (isSalesPerson()) {
        trackAndDoClick('signIn');
    } else {
        trackAndDoClick(getGuestTab());
    }
}