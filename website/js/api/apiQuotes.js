function getViewQuote(url) {

    return requestUrlAndLogIfErrorPromise(url,'Error Getting Quote');
}


function apiUpdateSalesTerms(salesTerms) {

    var json = JSON.stringify(salesTerms);
    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/salesTerms?"+authorize() + addSite() + addSuperToken();
    if(isProduction()){
        var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/salesTerms?"+authorizePlusProduction() + addSite() + addSuperToken();
    }
    if(isAccounting()){
        var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/salesTerms?"+authorizePlusAccounting() + addSite() + addSuperToken();
    }

    return requestUrlAndLogIfErrorPromiseForPut(url, json, "SalesTerms Failed");
}

function updatePipelinerAPI(lead)  {

    return;
    
    var json = JSON.stringify(lead);
    var url = "https://" + apiHostV1 + "/api/V4/pipeliner/leads";
    
    return requestUrlAndLogIfErrorPromiseForPutV4(url, json,  'Error Updating Lead in Pipeliner ', 'yes', 1);
}

function deletePipelinerAPI(quoteID)  {

    return;
    
    var json = JSON.stringify(quoteID);
    var url = "https://" + apiHostV1 + "/api/V4/pipeliner/leads";

    return requestUrlAndLogIfErrorPromiseForDeleteV4(url, json,  'Error Deleting Lead in Pipeliner ', 'yes', 1);
}


// Used to send to Pipeliner and now removed.
function generateAndSendDocument(quoteID) {

    return;

    if(!quoteID || !user.pipelinerId) return false;

    var url = "https://" + apiHostV1 + "/api/V4/sendDocument?tab=viewQuote&id=" + quoteID + authorizePlusSalesPerson() + 'type=' + user.type + '&site=' + site + addSuperToken() + "&server=" + site;

    return requestUrlAndLogIfErrorPromiseForGetV4(url, 'Error saving document', 'yes', 1);
}


function apiRevertToQuote() {

    var json = {};

    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/revertToQuote?"+authorize() + addSite() + addSuperToken();

    return requestUrlAndLogIfErrorPromiseForPut(url, json, "Reverting Quote Failed");
}