function reLoginError() {

    alert("The site had an error, please login again");
    resetFormAndCookies();
    trackAndDoClick('signIn');
}

function processErrorStatusResponse(response){
    if(response && response.status && response.status === 'error'){
        if (this.retryLimit) {
            this.retryLimit--;
            $.ajax(this);
        }else{
            showErrorBox(response.message);
            trackJs.track(response.message);
        }
    }
}

function showErrorBox(errorMsg){

    //Skipping the debug message on production
    if (isProductionSite()) {
        return;
    }

    var dialogHtml = '<div title="Error">' +
            errorMsg +
            '</div>'
    var dialog = $(dialogHtml).dialog({
        "draggable": false,
        "resizable": false,
        "position": { my: "right-20px top+20px", at: "right top"}
    }).css({
        "color":"#fff"
    }).prev(".ui-dialog-titlebar").css({
        "background":"#db6a64",
        "border":0
    });
    dialog.parent().css({"background":"#db6a64", "border":0})
}

function processErrorResponse(response){
    if (this.retryLimit) {
        this.retryLimit--;
        $.ajax(this);
    }
    var errorMsg = response.responseJSON ? response.responseJSON.message : response;
    showErrorBox(errorMsg);
    trackJs.track(errorMsg);
}


function requestUrlAndLogIfErrorPromise(url, error) {

    if (empty(error)) {
        error = 'Error requesting url';
    }
    var config = {type:'GET', url: url, message: error, apiVersion: 'V1'};


    var promise = $.getJSON(url, function (apiJSON) {

        validate(apiJSON);
    }).error(function (data) {

        //Tracking the Error to TrackJS
        trackError(config);
        reLoginError();
    }).fail(function () {

        //Tracking the Error to TrackJS
        trackError(config);
        bounceUser();
    });

    return promise;
}
//
// function requestUrlReturnPromiseForPost(url, json) {
//
//     var type = 'POST';
//     var config = {type:type, json: json, url: url, message: error, apiVersion: 'V1'};
//
//
//     var promise = $.ajax({
//         url: url,
//         data: json,
//         type: type,
//         success: function (apiJSON) {
//
//         },
//         error: function (data) {
//             trackJs.track(data);
//         }
//     });
//
//     return promise;
// }

function requestUrlAndLogIfErrorPromiseForPost(url, json, error) {

    var type = "POST";
    var config = {type:type, json: json, url: url, message: error, apiVersion: 'V1'};

    var promise = $.ajax({
        url: url,
        data: json,
        type: "POST",
        success: function (apiJSON) {
            validate(apiJSON);
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);
            reLoginError(data);
        }
    });

    return promise;
}

function requestUrlAndLogIfErrorPromiseForDelete(url, error) {

    var type = 'DELETE';
    var config = {type:type, url: url, message: error, apiVersion: 'V1'};

    var promise = $.ajax({
        url: url,
        data: '',
        type: "DELETE",
        success: function (apiJSON) {
            validate(apiJSON);
        },
        fail: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);
            reLoginError(data);
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);
            reLoginError(data);
        }
    });

    return promise;
}

function requestUrlAndLogIfErrorPromiseForPostV3(url, json, error, skip, retries) {

    var header = 'Bearer '+user.v3Token;
    var type = 'POST';
    var config = {type:type, url: url, json: json, message: error, apiVersion: 'V3'};

    var promise = $.ajax({
        url: url,
        data: json,
        headers: {
            Authorization: header
        },
        type: type,
        retryLimit : retries || 0,
        success: function (apiJSON) {
            if (skip === 'yes') {
                processErrorStatusResponse.call(this, apiJSON);
            }
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            if (skip === 'yes') {
                processErrorResponse.call(this, data);
                return;
            }
            reLoginError();
        }
    });

    return promise;
}

function requestUrlAndLogIfErrorPromiseForPostV4(url, json, error, skip, retries) {

    var header = 'Bearer '+user.v4Token;
    var type = 'POST';
    var config = {type:type, url: url, json: json, message: error, apiVersion: 'V4'};

    var promise = $.ajax({
        url: url,
        data: json,
        headers: {
            Authorization: header
        },
        type: type,
        retryLimit : retries || 0,
        success: function (apiJSON) {
            if (skip === 'yes') {
                processErrorStatusResponse.call(this, apiJSON);
            }
        },
        error: function (data) {

            //Send Errors to TrackJS to be recorded
            trackError(config);

            if (skip === 'yes') {
                processErrorResponse.call(this, data);
                return;
            }
            reLoginError(data);
        }
    });

    return promise;
}

function requestUrlAndLogIfErrorPromiseForPostUploadV4(url, formData, error, skip) {

    var header = 'Bearer '+user.v4Token;
    var type = 'POST';
    var config = {type:type, url: url, json: formData, message: error, apiVersion: 'V4'};

    var promise = $.ajax({
        url: url,
        data: formData,
        processData: false,
        contentType: false,
        headers: {
            Authorization: header
        },
        type: type,
        success: function (response) {
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            if (skip =='yes') {
                return;
            }
            reLoginError();
        }
    });

    return promise;
}


function requestUrlAndLogIfErrorPromiseForPostUploadV3(url, formData, error, skip) {

    var header = 'Bearer '+user.v3Token;
    var type  = 'POST';
    var config = {type:type, url: url, json: formData, message: error, apiVersion: 'V3'};

    var promise = $.ajax({
        url: url,
        data: formData,
        processData: false,
        contentType: false,
        headers: {
            Authorization: header
        },
        type: type,
        success: function (response) {
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            if (skip =='yes') {
                return;
            }
            reLoginError();
        }
    });

    return promise;
}

function requestUrlAndLogIfErrorPromiseForPutV4(url, json, error, skip, retries) {

    var header = 'Bearer '+user.v4Token;
    var type = 'PUT';
    var config = {type:type, url: url, json: json, message: error, apiVersion: 'V4'};

    var promise = $.ajax({
        url: url,
        data: json,
        headers: {
            Authorization: header
        },
        type: type,
        retryLimit : retries || 0,
        success: function (apiJSON) {
            if (skip === 'yes') {
                processErrorStatusResponse.call(this, apiJSON);
            }
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            if (skip === 'yes') {
                processErrorResponse.call(this, data);
                return;
            }
            reLoginError();
        }
    });

    return promise;
}

function requestUrlAndLogIfErrorPromiseForDeleteV4(url, json, error, skip, retries) {

    var header = 'Bearer '+user.v4Token;
    var type = 'DELETE';
    var config = {type:type, url: url, json: json, message: error, apiVersion: 'V4'};

    var promise = $.ajax({
        url: url,
        data: json,
        headers: {
            Authorization: header
        },
        type: type,
        retryLimit : retries || 0,
        success: function (apiJSON) {
            if (skip === 'yes') {
                processErrorStatusResponse.call(this, apiJSON);
            }
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            if (skip === 'yes') {
                processErrorResponse.call(this, data);
                return;
            }
            reLoginError();
        }
    });

    return promise;
}


function requestUrlAndLogIfErrorPromiseForGetV4(url, error, skip, retries) {

    var header = 'Bearer '+user.v4Token;
    var type = 'GET';
    var config = {type:type, url: url,  message: error, apiVersion: 'V4'};

    var promise = $.ajax({
        url: url,
        cache: false,
        headers: {
            Authorization: header
        },
        type: type,
        retryLimit : retries || 0,
        success: function (apiJSON) {
            if (skip === 'yes') {
                processErrorStatusResponse.call(this, apiJSON);
            }
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            if (skip === 'yes') {
                processErrorResponse.call(this, data);
                return;
            }
            reLoginError();
        }
    });

    return promise;
}


function requestUrlAndLogIfErrorPromiseForPut(url, json, error) {

    var type = "PUT";
    var config = {type:type, url: url, json: json, message: error, apiVersion: 'V1'};

    var promise = $.ajax({
        url: url,
        data: json,
        type: type,
        success: function (apiJSON) {
            validate(apiJSON);
        },
        error: function (data) {

            //Tracking the Error to TrackJS
            trackError(config);

            reLoginError();
        }
    });

    return promise;
}

function getDoorFixedWidths(group) {

    var url = "https://" + apiHostV1 + "/api/doors/fixedWidth/" + group + "?" + authorize() + addSite();

    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Fixed Widths');
}


function getDoorFixedHeights(group) {

    var url = "https://" + apiHostV1 + "/api/doors/fixedHeight/" + group + "?" + authorize() + addSite();

    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Fixed Heights');
}


function getGlassOptionsAPI() {

    if (empty(formStatus.module)) {
        bounceUser();
        return '';
     
    }
    var url = "https://" + apiHostV1 + "/api/glassOptions/" + formStatus.module + "?" + authorize() + addSite();

    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Glass Options');
}



function getProducts() {

    var guest = '';
    if (isGuest()) {
        guest = '&guest=yes';
    }

    var url = "https://" + apiHostV1 + "/api/modules?" + addSite() + guest + authorize();

    return requestUrlAndLogIfErrorPromise(url, 'Error getting products');
}


function apiGetWindowPrice() {

    var myOptions = buildPriceEngineOptionsForWindows(windowsForm);

    var json = JSON.stringify(myOptions);
    var url = "https://" + apiHost + "/windowSystem/price?" + addSite();

    return requestUrlAndLogIfErrorPromiseForPostV4(url, json, 'Error Building Window CutSheet Parts');
}


function apiBuildWindowsCutParts() {

    var myOptions = buildPriceEngineOptionsForWindows(windowsForm);

    var json = JSON.stringify(myOptions);
    var url = "https://" + apiHost + "/windowSystem/cutSheet?" + addSite();

    return requestUrlAndLogIfErrorPromiseForPostV4(url, json, 'Error Building Window CutSheet Parts');
}


function apiPDF(url) {

    var config = {"url": url};
    var json = JSON.stringify(config);

    var url = "https://" + apiHost + "/pdf?" + addSite();

    return requestUrlAndLogIfErrorPromiseForPost(url, json, 'Error Build PDF');
}


function apiGetWindowsCutParts(id) {

    var url = "https://" + apiHost + "/windowSystem/cutSheet/" + id + "?" + addSite();

    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Window CutSheet Parts');
}


//Refactor//
function getWindowsPrice(myOptions) {

    var myPromise;
    var json = JSON.stringify(myOptions);
    var url = "https://" + apiHost + "/windowSystem?" + addSite();

    myPromise = $.post(url, json).done(function (data) {

        var pricing = jQuery.parseJSON(data);
        return "Great";

    }).error(function (data) {
        reLoginError(data);
    });

    return myPromise;
}


function getSiteDefaults() {

    var url = "https://" + apiHostV1 + "/api/defaults/site";

    return requestUrlAndLogIfErrorPromise(url, 'Get Site Defaults Failed');

}


function getTrickleColors() {

    var url = "https://" + apiHostV1 + "/api/colors/trickle?module=" + formStatus.module + authorize() + addSite();

    return requestUrlAndLogIfErrorPromise(url, 'getTrickleColors Failed');
}


function getModuleOptions() {

    var lookupModule = 1;

    if (!empty(formStatus.module)) {
        lookupModule = formStatus.module;
    }

    var url = "https://" + apiHostV1 + "/api/moduleInfo?module=" + lookupModule + addSite();

    return requestUrlAndLogIfErrorPromise(url, 'Error Get Module Info for ' + formStatus.module);
}

function getUISettings(){

    var url = "https://" + apiHostV1 + "/api/uiSettings";

    return requestUrlAndLogIfErrorPromise(url, 'Error Get UI Settings');
}

function getExtendedButtons(option) {

    var url = "https://" + apiHostV1 + "/api/extendedButtons?module=" + formStatus.module + "&option=" + option + authorize() + addSite();
    return requestUrlAndLogIfErrorPromise(url, 'Error Extended Buttons for ' + option);
}


function getPanelGroupsByWidth(width) {

    var url = "https://" + apiHostV1 + "/api/panels/" + width + "?module=" + formStatus.module + addSite();
    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Fixed Widths');
}


function getRevertFullPayment(id) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/unconfirmFullPayment?" + authorizePlusAccounting() + addSite();
    return requestUrlAndLogIfErrorPromise(url, 'Error Reverting Full Payment');
}


function getInvoice() {

    var order = getParameter('order');
    var token = getParameter('token');
    var site = getParameter('site');
    var url = "https://" + apiHostV1 + "/api/invoice/" + order + "?&token=" + token + "&site=" + site;

    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Invoice');
}


function getCreateCutSheets() {

    var sheets = buildSheets();
    var json = JSON.stringify(sheets);
    var url = "https://" + apiHostV1 + "/api/quotes/" + quote.id + "/cutSheetsSales?" + authorizePlusSalesPerson() + addSite() + addSuperToken();

    return requestUrlAndLogIfErrorPromiseForPut(url, json, 'Error Get CutSheets for Invoice');
}

function getCutSheetsWithIdAndSite(id, site) {

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/cutSheets?" + authorize() + '&site=' + site;
    return requestUrlAndLogIfErrorPromise(url, 'Error Getting CutSheet');
}

function getQuoteToPrint(id, site) {

    var debugURI = makeDebugURI();
    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printInvoice?" + authorize() + '&order=no' + '&site=' + site + debugURI;

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);

}

function getInvoiceToPrintForProduction(id, site) {

    var debugURI = makeDebugURI();
    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printInvoice?" + authorize() + '&site=' + site + debugURI + '&collection=yes';

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);
}


//f2
function getInvoiceToPrint(id, site) {

    var debugURI = makeDebugURI();
    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/printInvoice?" + authorize() + '&site=' + site + debugURI;

    var newWin = window.open(url);
    blockingPopupsAlert(newWin);
}


function makeDebugURI() {

    var debugURI = '';
    var debug = getParameter('debug');
    if (!empty(debug)) {
        debugURI = '&debug=' + debug;
    }

    return debugURI;
}


function getCustomer(customerID) {

    var url = "https://" + apiHostV1 + "/api/customers/" + customerID + "?" + authorize() + addSite() + addSuperToken();
    return requestUrlAndLogIfErrorPromise(url, 'Error Getting Customer');
}

function deleteCustomerAPI(customerID) {

    var url = "https://" + apiHostV1 + "/api/customers/" + customerID + "?"+authorize() + addSite() + addSuperToken();

    return requestUrlAndLogIfErrorPromiseForDelete(url, "Delete Customer Failed");
}


function putCustomerUpdate(update) {

    if (empty(quote.Customer)) {
        alert("Customer Errors");
    }

    var json = JSON.stringify(update);

    var url = "https://" + apiHostV1 + "/api/update/customer/" + quote.Customer.customerID + "?" + authorize() + addSite() + addSuperToken();

    return requestUrlAndLogIfErrorPromiseForPut(url, json, 'Error Updating Customer');
}


function putCutSheet(sheets, id) {

    var json = JSON.stringify(sheets);

    var url = "https://" + apiHostV1 + "/api/quotes/" + id + "/cutSheets?" + authorizePlusProduction() + addSite();

    return requestUrlAndLogIfErrorPromiseForPut(url, json, 'Error Updating CutSheet');
}

// function updateQuoteAddresses(quoteId, addresses) {
//
//     var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/addresses";
//
//
//     $.ajax({
//         url: url,
//         type: 'put',
//         headers: {
//             Authorization: header
//         },
//         success: function (data) {
//             user.v3Token = data.token;
//             writeUser(user);
//             window.location.href = "/?tab=" + myTab;
//         }
//     });
// }


function getCustomerAPI(customer_id) {

    var url = "https://" + apiHostV1 + "/api/V4/customers/"+customer_id+"?" + authorize() + addSite();

    return requestUrlAndLogIfErrorPromiseForGetV4(url, 'Error Getting Customer  ');
}

function getSalesTax(city, zip){

    var url = "https://" + apiHostV1 + "/api/V4/salesTax?city="+city+"&zip="+zip+"&country=us"

    return requestUrlAndLogIfErrorPromiseForGetV4(url, 'Error Getting SalesTax');
}

function updateAddressesAPI(quoteId, json) {
    
    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/addresses?"  + addSite();

    return requestUrlAndLogIfErrorPromiseForPutV4(url, json,  'Error Attaching Quote Addresses ', 'yes', 3);
}

function attachAddressesToQuoteAPI(quoteId, json, customerId) {
    
    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/addresses?addToQuote=yes&customer_id="+ customerId  + authorize() + addSite();
    
    return requestUrlAndLogIfErrorPromiseForPutV4(url, json,  'Error Attaching Quote Addresses ');
}

function attachEmailFiles(quoteId, formData){

    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/attachFiles";

    return requestUrlAndLogIfErrorPromiseForPostUploadV4(url, formData,  'Error file upload');
}

function deleteEmailAttachment(quoteId, data){

    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/deleteAttachment";

    return requestUrlAndLogIfErrorPromiseForPostV4(url, data,  'Error delete attachment');
}

function clearEmailAttachments(quoteId){

    if (siteDefaults.salesFlow == 2) {
        return;
    }
    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/clearAttachments";

    return requestUrlAndLogIfErrorPromiseForPostV4(url, {},  'Error clear attachments');
}

function uploadContractFile(quoteId, formData){

    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/contracts";

    return requestUrlAndLogIfErrorPromiseForPostUploadV4(url, formData,  'Error file upload');
}

function updateContractFile(quoteId, id, formData){

    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/contracts/"+id;

    return requestUrlAndLogIfErrorPromiseForPostUploadV4(url, formData,  'Error file upload');
}

function deleteContractFile(quoteId){

    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/contracts";

    return requestUrlAndLogIfErrorPromiseForDeleteV4(url, 'Error Deleting Contract');
}

function getContractID(quoteId){

    if(empty(quoteId)){
        return;
    }

    var url = "https://" + apiHostV1 + "/api/V4/quotes/"+quoteId+"/contracts";

    return requestUrlAndLogIfErrorPromiseForGetV4(url, 'Error Getting Contract');
}

function newCustomerAPI(customerJson) {

    var url = "https://" + apiHostV1 + "/api/customers?" + authorize() + addSite();

    return requestUrlAndLogIfErrorPromiseForPost(url, customerJson,  'Error Adding New Customer');

}


function generateProductionReportApi(date){

    var url = "https://" + apiHostV1 + "/api/V4/reports/openOrders?exportType=csv&end_date="+date;

    return requestUrlAndLogIfErrorPromiseForGetV4(url, 'Error Getting Contract');
}


// Call TrackJS with custom Error Message
function trackError(config) {

    var type = !empty(config.type) ? config.type :  '--';
    var url =  !empty(config.url)  ? config.url  :  '--';
    var json = !empty(config.json) ? config.json :  '--';
    var message = !empty(config.message) ? config.message :  '--';
    var apiVersion = !empty(config.apiVersion) ? config.apiVersion :  '--';

    // if (!empty(trackJs)) {
    //     var error = {"Message":message, "Type": type, "URL": url, "JSON":json, "ApiVersion": apiVersion };
    //     trackJs.track(error);
    // }
}
