'use strict'

function createScreenButtons(id) {
    $('.extendedButtonScreen', id).unbind("click").bind("click", extendedScreenSelected);
}

function buildExtrasScreens(screenData) {
    
    var buttons = ''
    
    for (var a = 0; a < screenData.length; a++) {
        var button = buildExtendedButtonScreen(screenData[a], 'screen');
        buttons = buttons + button;
    }

    return buttons;
}

function showScreenButtonDisplay(button){


    //Fracka Hack -- Returning all buttons for now.

    return true;

    if(button.maxSize == 0 && formStatus.width <= 5000){ // No screen
        return true;
    }
    if(button.name === 'One Screen' && formStatus.width >= 2000 && formStatus.width <= button.maxSize){ // One screen
        return true;
    }
    if(button.name === 'Two Screens' && formStatus.width > 5000 && formStatus.width <= button.maxSize){ // Two screens
        return true;
    }
    return false;
}

function buildExtendedButtonScreen(button, option) {
    
    if(showScreenButtonDisplay(button)){
        
        var id = option + '-' + button.highlight + '-' + button.name;

        var button =
                ['<div id="' + id + '" class="extendedButtonScreen">',
                    '<div class="extendedButtonImage"><img src="' + button.url + '"> </div>',
                    '<div class="extendedButtonTitleScreen">' + button.name + '</div>',
//            '<div class="extendedButtonDescriptionScreen">' + button.description + '</div>',
                    '</div>'
                ].join('\n');

        return (button);
    }else{
        return '';
    }
}

function screensForCart(row) {

    var html = '';
    if (!empty(row.screens) &&  splitForSecondOption(row.screens) != 'none' ) {
        var type = splitForThirdOption(row.screens);

        html = '' +
            '<div class="cartDescriptionRowMain">Screens</div>' +
            '<div class="cartDescriptionRowSecond">' + type + '</div>';
    }

    return html;
}

function extendedScreenSelected() {

    var thisSelector = $(this);

    var thisSection = thisSelector.parent();
    var optionSelected = thisSelector.attr('id');
    var section = $(thisSection).attr('id');
    var nextSection = thisSelector.parent().parent().next();

    formStatus[section] = optionSelected;
    writeFormStatus();

    var groupOfButtons = thisSelector.parent();
    var selectedDiv = $(".extendedSelectedDiv", groupOfButtons);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
        if($('.extendedButtonScreen').length == 1){
            formStatus[section] = '';
            writeFormStatus();
            return;
        }
    }

    var selector = $('.extendedButtonImage', this);
    displayExtendedSelected(selector);

    scrollToNextPositon(nextSection);

    updateFormPrice();
    lockDownTabs();
}