function bayPolesHtml(buttons){
    return '<div id="chooseBayPoles"> ' +
        '   <div class="title">Bay Polls</div> ' +
        '   <div id="bayPoleChoices" class="sectionBlock"> ' +
        '       ' + buttons +
        '   </div> ' +
        '</div> ';
}

function buildBayPoles(bayPolesData) {

    var buttons = '';
    for (var a = 0; a < bayPolesData.length; a++) {
        var button = buildExtendedHalfButton(bayPolesData[a], 'bayPole');
        buttons = buttons + button;
    }

    return buttons;
}

function buildBayPoleForPriceEngine(options) {

    var bayPoleOption = splitForSecondOption(options.bayPoleChoices);
    var bayPole = {"type":bayPoleOption};

    return bayPole;
}

function createBayPoleButtons(id) {
    $('.buttonRef', id).unbind("click").bind("click", extendedSelected);
}