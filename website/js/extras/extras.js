'use strict'

function loadExtras() {

    startSpinner();
    updateFormStatus();

    $.when(
        getTrickleColors(), getExtendedButtons("sill"), getExtendedButtons("extender"),
        getExtendedButtons("trickle"), getExtendedButtons("screens"), getExtendedButtons("baypole")
    ).done(function (colors, sill, extender, trickle, screens, baypoles) {

            extenderChoices = extender[0];
            var html = getExtrasHTML(sill[0], extender[0], trickle[0], colors[0], screens[0], baypoles[0]);
            buildAndAttachCorePage(html)
            $('#cartNav').show();
            selectTab('#extras');


            addFooter();
            lockDownTabs();

            createSillButtons('#sillChoices');
            createSillUpdater('#sillChoices');
            createExtenderButtons('#addOnChoices');
            createExtenderUpdater('#addOnChoices');

            createTrickleButtons('#trickleChoices');
            createBayPoleButtons('#bayPoleChoices');

            createScreenButtons('#screens');

            createTrickleColorButtons();
            updateExtraSelections();

            updateFormPrice();
            $('#dynamicContent').css("visibility", "visible");
            makeNextSectionButton(2);
            showContentAndSaveState();
            scrollTopOfPage();
        });
}


function validationExtenderDimensionByOption(selectedOption) {

    var type = "Width";
    if (empty(formStatus.addOnChoices[selectedOption].width)) {
        type = "Height";
    }

    var dimId = selectedOption + '-' + type;
    getDimensionAndValidate(dimId, splitForSecondOption(selectedOption));
}

function replaceSpaceWithDash(string){
    return string.split(' ').join('-')
}


function updateExtraSelections() {
    if (formStatus.sillOptions) {
        var sillSelector = '#' + (replaceSpaceWithDash(formStatus.sillOptions.selected));
        var selector = $('.extendedButtonImage', sillSelector);

        displayExtendedSelected(selector);
        updateSillDimensionsWithOptionSelected(formStatus.sillOptions.selected);
    }

    if (formStatus.addOnChoices) {
        for (var property in formStatus.addOnChoices) {
            if (formStatus.addOnChoices[property].selected == 'yes') {
                var selector = $('.extendedButtonImage', '#' + property);
                displayExtendedSelected(selector);
                validationExtenderDimensionByOption(property);
            }
        }
    }

    var id = "[id='" + formStatus.trickleChoices + "']";
    var selector = $('.buttonImageRef', id);
    displayExtendedSelected(selector);

    if (splitForSecondOption(formStatus.trickleChoices) == 'none' || empty(formStatus.trickleChoices)) {
        $('#chooseTrickleVentColor').hide();
    } else {
        $('#chooseTrickleVentColor').show();
    }

    var id = "[id='" + formStatus.screens + "']";
    var selector = $('.extendedButtonImage', id);
    displayExtendedSelected(selector);
    
    var id = "[id='" + formStatus.bayPoleChoices + "']";
    var selector = $('.buttonImageRef', id);
    displayExtendedSelected(selector);
}


function updateRadioButton() {

    var thisSelector = $(this);
    var section = thisSelector.parent().parent().parent().parent().attr('id');
    var deduct = radioValueFromOptionSelected(section);

    formStatus['addOnChoices'][section]['deduct'] = deduct;
    writeFormStatus();
    updateFormPrice();
}


function ifPressedSkip(e) {

    if ($(e.target).is('.dimKeyUp')) {
        return true;
    }
    if ($(e.target).is('.deductRadioButton')) {
        return true;
    }
    if ($(e.target).is('.deductRadioButtonLabel')) {
        return true;
    }
    if ($(e.target).is('.sillDimensionsDeductTitle')) {
        return true;
    }

    return false;

}


function checkExtraSelections() {

    var error = '';

    if (!validSillOptions()) {
        error = error + "Sill dimensions need correcting\n";
    }

    if (!validExtenderOptions()) {
        error = error + "Extender dimensions need correcting\n";
    }

    if (!validTickleOptions()) {

        error = error + "Your tickle vent needs a color selected\n";
    }

    if (!empty(error)) {
        stopSpinner();
        alert(error);
        return false;
    }

    return true;
}


function validDimension(value, type) {

    if (value < parseInt(moduleInfo[type + 'Min']) || value > parseInt(moduleInfo[type + 'Max'])) {

        return false;
    }

    return true;
}


function getDimensionAndValidate(id, type) {

    var selector = $(buildIDSelector(id));
    var value = selector.val();
    value = numbersOnly(value);


    if(type === "sillHeight" && empty(value)){
        selector.val(0);
    }else{
        selector.val(value);
    }

    if (validDimension(value, type)) {
        selector.removeClass('badDimension');
    } else {
        selector.addClass('badDimension');
    }

    return value;
}


