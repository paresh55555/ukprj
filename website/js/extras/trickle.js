'use strict'

function createTrickleButtons(id) {
    $('.extendedButtonTrickle', id).unbind("click").bind("click", extendedSelected);
}


function createTrickleColorButtons() {
    $('.finishOptionsItem').unbind("click").bind("click", chooseColor);
}


function buildTrickleColors(colorsData) {

    var buttons = '';
    for (var a = 0; a < colorsData.length; a++) {
        //var button = buildFinishGroupButton(colorsData[a]);
        colorsData[a].location = 'trickle';
        var button = buildFinishButton(colorsData[a]);
        buttons = buttons + button;

    }
   return buttons;
}


function buildTrickle(trickleData) {

    //var id = '#trickleChoices';
    //$(id).html('');
    var buttons = '';
    for (var a = 0; a < trickleData.length; a++) {
        var button = buildTrickleExtendedButton(trickleData[a], 'trickle');
        buttons = buttons + button;
        //$(id).append(button);
    }

    return buttons;
}


function buildTrickleForPriceEngine(options) {

    var trickleOption = splitForSecondOption(options.trickleChoices);
    var trickleColor = options.trickleColor;
    var trickle = {"type":trickleOption, "color": trickleColor};

    return trickle;
}

function buildScreenForPriceEngine(options) {

    var screenOption = splitForSecondOption(options.screens);

    var screen = {"type":screenOption};

    return screen;
}


function  trickleVentForCart(row) {

    var html = '';
    if (!empty(row.trickleChoices) &&  splitForSecondOption(row.trickleChoices) != 'none' ) {
        var type = splitForThirdOption(row.trickleChoices);

        html = '' +
            '<div class="cartDescriptionRowMain">Trickle Vent</div>' +
            '<div class="cartDescriptionRowSecond">' + type + ' ('+ row.trickleColor + ')</div>';
    }

    return html;
}


function buildTrickleExtendedButton(button, option) {

    var id = option + '-' + button.highlight + '-' + button.name;

    var button =
        ['<div id="' + id + '" class="extendedButtonTrickle">',
            '<div class="extendedButtonImage buttonImageRef"><img src="' + button.url + '"> </div>',
            '<div class="extendedButtonTitleTrickle">' + button.name + '</div>',
            '<div class="extendedButtonDescriptionTrickle">' + button.description + '</div>',
            '</div>'
        ].join('\n');

    return (button);
}


function validTickleOptions() {

    if (empty(formStatus.trickleChoices)) {
        return true;
    }

    var trickle = splitForSecondOption(formStatus.trickleChoices);
    if (trickle == 'none') {
        return true;
    }

    if (empty(formStatus.trickleColor) ) {
        return false;
    }

    return true;
}
