'use strict';


function getExtrasHTML(sills, extenders, trickles, colors, screens, bayPoles) {

    var sillButtons = buildSills(sills);
    var extenderButtons = buildExtenders(extenders);
    var trickleButtons = buildTrickle(trickles);
    var colorsButtons = buildTrickleColors(colors);
    var screenButtons = buildExtrasScreens(screens);
    var bayPoleButtons = buildBayPoles(bayPoles);

    var trickleColorHTML = '' +
        '<div id="chooseTrickleVentColor"> ' +
        '<div class="title">Choose Trickle Vent ' + colorText +'</div> ' +
        '   <div id="trickleColors" class="sectionBlock"> ' +
        '       ' + colorsButtons +
        '   </div> ' +
        '</div> ' ;

    if (empty(colorsButtons)) {
        trickleColorHTML = '';
        formStatus.trickleColor = 'None';
    }

    var screensHTML = '' +
        '<div id="chooseScreen"> ' +
        '   <div class="title">Screens</div> ' +
        '   <div id="screens" class="sectionBlock"> ' +
        '       ' + screenButtons +
        '   </div> ' +
        '</div> ';

    if (empty(screenButtons)) {
        screensHTML = '';
    }

    var extenderHTML = '' +
        '<div id="chooseAddons"> ' +
        '   <div class="title">Choose Add-ons</div> ' +
        '   <div id="addOnChoices" class="sectionBlock"> ' +
        '       ' + extenderButtons +
        '   </div> ' ;

    if (empty(extenderButtons)) {
        extenderHTML = '';
    }
    
    var bayPoleHtml = '';
    if(!empty(bayPoleButtons)){
        bayPoleHtml = bayPolesHtml(bayPoleButtons);
    }

    var html = '' +
        '<div id="dynamicContent"> ' +
        '<div id="myExtras"> <div> ' +
        '   <div class="title">Choose Sill Type</div> ' +
        '   <div id="sillChoices" class="sectionBlock"> ' +
        '       ' + sillButtons +
        '   </div> ' +
        '</div> ' +
        extenderHTML +
        '</div> ' +
        '<div id="chooseTricleVent"> ' +
        '   <div class="title">Choose Trickle Vent</div> ' +
        '   <div id="trickleChoices" class="sectionBlock"> ' +
        '       ' + trickleButtons +
        '   </div> ' +
        '</div> ' +
        trickleColorHTML +
        bayPoleHtml +
        screensHTML +
        '</div> ' +
        '</div> ' +
        '<' +
        'div id="nextSection"> ' +
        '</div> ' +
        '<div id="copyWriteFooter"></div> ' +
        '</div>';

    if(formStatus.doorStyle){
        extenderButtons = buildDoorStyleExtenders(extenders);
        html = '' +
        '<div id="dynamicContent"> ' +
            '<div id="chooseAddons"> ' +
                '<div class="title">Choose Add-ons</div> ' +
                '<div id="addOnChoices" class="sectionBlock"> ' +
                    extenderButtons +
                '</div> ' +
            '</div> ' +
            '<div id="nextSection"></div> ' +
            '<div id="copyWriteFooter"></div> ' +
        '</div>';
    }
    return html;

}