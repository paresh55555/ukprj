'use strict';

function createExtenderUpdater(id) {
    $('.dimKeyUp', id).unbind("keyup").bind("keyup", updateExtenderDimension);
    $('.deductRadioButton', id).unbind("change").bind("change", updateRadioButton);
}


function createExtenderButtons(id) {
    $('.extendedButton', id).unbind("click").bind("click", extendedOptionExtenderSelected);
}


function buildAddOnsForPriceEngine(options) {

    var addOns = {};
    for (var property in options.addOnChoices) {
        var option = splitForSecondOption(property);
        addOns[option] = options.addOnChoices[property];
    }

    return addOns;
}


function setHeightOrMinExtenderValueByOption(optionSelected) {

    var height = $('#' + optionSelected + '-Height').val();

    if (empty(height)) {
        var option = splitForSecondOption(optionSelected);
        var min = moduleInfo[option+'Min'];
        if (!empty(min)) {
            height = min;
            $('#extender-'+option+'-Height').val(height);
        }
    }

    return height;
}


function setWidthOrMinExtenderValueByOption(optionSelected) {

    var width = $('#' + optionSelected + '-Width').val();

    if (empty(width)) {
        var option = splitForSecondOption(optionSelected);
        var min = moduleInfo[option+'Min'];
        if (!empty(min)) {
            width = min;
            $('#extender-'+option+'-Width').val(width);
        }
    }

    return width;

}

function updateDimBoxValue(value, option, type){
    $('#'+option+'-'+type).val(value);
}

function extendedOptionExtenderSelected(e) {

    //Split if the textfield was entered
    if (ifPressedSkip(e)) {
        return;
    }

    var thisSelector = $(this);
    var groupOfButtons = thisSelector.parent();
    var multiple = groupOfButtons.data('multiple');
    var section = thisSelector.parent().parent().attr('id');
    var optionSelected = thisSelector.attr('id');
    var nextSection = thisSelector.parent().parent().parent().next();

    var existsValue = getExtendedDimensionValue(optionSelected);
    var width = '';
    var height = '';
    var readOnly = formStatus.addOnChoices[optionSelected] && formStatus.addOnChoices[optionSelected].readOnly || false;
    if(empty(existsValue)){
        var button = extenderChoices.find(function(ext){return ext.name === splitForSecondOption(optionSelected)});
        if(button){
            if(button.highlight === 'Width'){
                width = getDefaultsExtenderValues(button, 'width');
                readOnly = getDefaultsExtenderValues(button, 'readOnlyWidth');
                updateDimBoxValue(width, optionSelected, 'Width');
            }else{
                height = getDefaultsExtenderValues(button, 'height');
                readOnly = getDefaultsExtenderValues(button, 'readOnlyHeight');
                updateDimBoxValue(height, optionSelected, 'Height');
            }
        }
    }else{
        if(existsValue.type === 'width'){
            width = existsValue.value;
            updateDimBoxValue(width, optionSelected, 'Width');
        }else{
            height = existsValue.value;
            updateDimBoxValue(height, optionSelected, 'Height');
        }
    }
//    var height = setHeightOrMinExtenderValueByOption(optionSelected);
//    var width = setWidthOrMinExtenderValueByOption(optionSelected);

    var deduct = radioValueFromOptionSelected(optionSelected);

    var selections = {"height": height, "width": width, "deduct": deduct, "readOnly":readOnly};

    var options = {};
    if (!empty(formStatus[section])) {
        options = formStatus[section];
    }
    var selectedDiv = $(".selectedDivRef", groupOfButtons);

    if(multiple){
        var selectedDivMultiples = thisSelector.find(".selectedDivRef");
        if (selectedDivMultiples.length > 0) {
            selectedDivMultiples.remove();
            selections.selected = 'no';
            options[optionSelected] = selections;
        }
        else {
            var selector = $('.buttonImageRef', this);
            selections.selected = 'yes';
            options[optionSelected] = selections;
            displayExtendedSelected(selector);
        }
    }else{
        if (selectedDiv.length > 0) {
            var prevSelected = selectedDiv.parents('.buttonRef');
            options[prevSelected.attr('id')].selected = 'no';
            selectedDiv.remove();
        }
        var selector = $('.buttonImageRef', this);
        options[optionSelected] = selections;
        options[optionSelected].selected = 'yes';
        displayExtendedSelected(selector);
        // scrollToNextPositon(nextSection);
    }
    
    var extenderName = splitForSecondOption(optionSelected);
    // Deselect other extender options when "No Extender" selected
    if (extenderName.indexOf('noExtender') !== -1) {
        $(".buttonRef",groupOfButtons).each(function(){
                var section = $(this).parent().parent().attr('id');
                var optionSelected = $(this).attr('id');
                var extenderName = splitForSecondOption(optionSelected);
                if(extenderName.indexOf('noExtender') !== -1){
                    return true;
                }
                var options = {};
                if (formStatus[section]) {
                    options = formStatus[section];
                }

                var selectedDiv = $(this).find(".selectedDivRef");
                if (selectedDiv.length > 0) {
                    selectedDiv.remove();
                    options[optionSelected] = {};
                    options[optionSelected].selected = 'no';
                    formStatus[section] = options;
                }
        })
    }else{
        var noOption = $(".buttonRef[id*=noExtender]", groupOfButtons);
        if(formStatus.addOnChoices && noOption.length && formStatus.addOnChoices[noOption.attr('id')]){
            noOption.find('.selectedDivRef').remove();
            formStatus.addOnChoices[noOption.attr('id')].selected = 'no';
        }
    }

    formStatus[section] = options;

    writeFormStatus();
    updateFormPrice();
}


function updateExtenderDimension() {

    var thisSelector = $(this);
    var section = thisSelector.parent().parent().parent().attr('id');
    //var value = thisSelector.val();
    var id = thisSelector.attr('id');

    var type = splitForThirdOption(thisSelector.attr('id'));
    var extender = splitForSecondOption(thisSelector.attr('id'));

    type = type.toLowerCase();
    var value = getDimensionAndValidate(id, extender);

    formStatus['addOnChoices'][section][type] = value;
    writeFormStatus();
    updateFormPrice();
}


function buildRadioButtonDataYesNoExtender(id) {

    var answer = 'no';
    if (formStatus.addOnChoices[id]) {
        answer = formStatus.addOnChoices[id].deduct;
    }

    var data = {"no": 'checked="checked"', "yes": ""};
    if (answer == 'yes') {
        data = {"no": "", "yes": 'checked="checked"'};
    }

    return data;
}

function getDefaultsExtenderValues(button, option) {

    if (empty(button.defaults)) {
        return '';
    }

    var defaults = JSON.parse(button.defaults);

    if (defaults[option]) {
        return defaults[option];
    }

    return '';

}

function buildExtendedButtonExtender(button, option, defaultSelected) {

    var id = option + '-' + button.name;

    var existsValue = getExtendedDimensionValue(id);
    var readOnly = false;
    var value = '';

    if(empty(existsValue)){

        value = getDefaultsExtenderValues(button, 'height');
        readOnly = getDefaultsExtenderValues(button, 'readOnlyHeight');
        
        if(empty(value)){

            value = getDefaultsExtenderValues(button, 'width');
            readOnly = (readOnly === '') ? (getDefaultsExtenderValues(button, 'readOnlyWidth') || false) : readOnly;
        }
    }else{
        value = existsValue.value;
        readOnly = existsValue.readOnly;
    }
    
    var boxes = [
        {"text": button.highlight, "unit": "mm", "value": value, "id": id + '-' + button.highlight, "readOnly": readOnly}
    ];
//    var radioButtons = buildRadioButtonDataYesNoExtender(id);

    //var deduct = buildDeductRadioButtons(radioButtons, id);
    var deduct = '';
    var dimBox = buildSillBoxes(boxes);
    if(button.name.indexOf('noExtender') !== -1){
        dimBox = '';
    }
    
    var selectedHtml = defaultSelected ? '<div class="extendedSelectedDiv selectedDivRef"><img src="images/icon-selected.svg"></div>' : ''
    var buttonImage = '<div class="extendedButtonImage buttonImageRef"><img src="' + button.url + '"> '+selectedHtml+' </div>';
    if(formStatus.doorStyle){
        var selectedDS = getSelectedDoorStyle(formStatus.panelOptions);
        if(button.name === "leftExtender"){
            buttonImage = '<div class="extendedButtonImage buttonImageRef doorStyle left">'+
                            '<img class="extender" src="' + button.url + '">'+
                            '<img class="panel" src="'+getOrientedDoorStyleImageUrl(selectedDS[0].url,formStatus.panelOptions)+'">'+
                            selectedHtml +
                        '</div>';
        }
        if(button.name === "rightExtender"){
            buttonImage = '<div class="extendedButtonImage buttonImageRef doorStyle right">'+
                            '<img class="panel" src="'+getOrientedDoorStyleImageUrl(selectedDS[0].url,formStatus.panelOptions)+'">'+
                            '<img class="extender" src="' + button.url + '">'+
                            selectedHtml +
                        '</div>';
        }
    }

    var button = '' +
        '<div id="' + id + '" class="extendedButton buttonRef">' +
            buttonImage +
        '   <div class="sillDimensionsTitle">' + button.description + '</div>' +
        '   <div class="extenderBox">' +
        '           ' + dimBox +
        '       <div class="extenderBoxDeduct">' +
        '           <div class="deductRadioButtonBox">' + deduct + '</div>' +
        '       </div>' +
        '   </div>' +
        '   </div>';

    return (button);
}


function getExtendedDimensionValue(id) {

    if (empty(formStatus.addOnChoices)) {
        return '';
    }

    if (!empty((formStatus.addOnChoices[id]))) {
        if (formStatus.addOnChoices[id].selected == 'yes') {

            if (!empty(formStatus.addOnChoices[id].height)) {
                return {type: 'height', value:formStatus.addOnChoices[id].height, readOnly: formStatus.addOnChoices[id].readOnly};
            }

            if (!empty(formStatus.addOnChoices[id].width)) {
                return {type: 'width', value:formStatus.addOnChoices[id].width, readOnly: formStatus.addOnChoices[id].readOnly};
            }
        }
    }

    return '';
}


function buildExtenders(extenderData) {

    //var id = '#addOnChoices';
    var buttons = [];
    var c = 0;
    var extGroups = groupBy(extenderData, 'myGroup');

    for (var a in extGroups) {
        var extGroup = extGroups[a],
            groupButtons = [],
            isMultiple = false;
        if(c !== 0){
            // Skip delimiter for first group
            groupButtons.push('<hr class="group-delimiter"/>');
        }
        c++;
        for (var g = 0; g < extGroup.length; g++) {
            var extender = extGroup[g];
            var defaultSelected = false;
            if(extender.multiples){
                isMultiple = true;
            }
            // Select default option if it was not changed by user
            if(extender.default && empty(formStatus.addOnChoices['extender-' + extender.name])){
                var id = 'extender-' + extender.name;
                var height = getDefaultsExtenderValues(extender, 'height');
                var width = getDefaultsExtenderValues(extender, 'width');
                var readOnly = getDefaultsExtenderValues(extender, (extender.highlight === 'Width') ? 'readOnlyWidth' : 'readOnlyHeight');
                var deduct = radioValueFromOptionSelected(id);
                formStatus.addOnChoices[id] = {"height": height, "width": width, "deduct": deduct, "selected": 'yes', readOnly: readOnly};
                defaultSelected = true;
            }
            var button = buildExtendedButtonExtender(extender, 'extender', defaultSelected);
            groupButtons.push(button);
        }
        buttons.push('<div id="extenderGroup-'+a+'" data-multiple="'+isMultiple+'">'+groupButtons.join('\n')+'</div>');
    }

    return buttons.join('\n');
}

function buildDoorStyleExtenders(extenderData){
    var buttons = '';
    for (var a = 0; a < extenderData.length; a++) {
        var button = buildExtendedButtonExtender(extenderData[a], 'extender');
        buttons = buttons + button;
    }

    return buttons;
}

function topExtenderDeduct(topExtender) {

    if (empty(topExtender)) {
        return 0;
    }

    if (topExtender.deduct == 'yes' && topExtender.selected == 'yes') {
        return topExtender.height;
    } else {
        return 0;
    }
}


function leftRightExtendersDeduct(leftExtender, rightExtender) {

    var leftDeduct = deductExtenderWidth(leftExtender);
    var rightDeduct = deductExtenderWidth(rightExtender);

    return parseInt(leftDeduct) + parseInt(rightDeduct);
}


function deductExtenderWidth(extender) {

    if (empty(extender)) {
        return 0;
    }

    if (extender.deduct == 'yes' && extender.selected == 'yes') {
        return extender.width;
    } else {
        return 0;
    }
}


function addOnCartHTML(addOns) {

 if (empty(addOns)) {
        return '';
    }

    var html = '';

    if (!empty(addOns['extender-leftExtender20'])) {
        html = html + extenderHTML(addOns['extender-leftExtender20'], "Left Extender");
    }
    if (!empty(addOns['extender-leftExtender50'])) {
        html = html + extenderHTML(addOns['extender-leftExtender50'], "Left Extender");
    }

    if (!empty(addOns['extender-topExtender20'])) {
        html = html + extenderHTML(addOns['extender-topExtender20'], "Top Extender");
    }
    if (!empty(addOns['extender-topExtender50'])) {
        html = html + extenderHTML(addOns['extender-topExtender50'], "Top Extender");
    }

    if (!empty(addOns['extender-rightExtender20'])) {
        html = html + extenderHTML(addOns['extender-rightExtender20'], "Right Extender");
    }
    if (!empty(addOns['extender-rightExtender50'])) {
        html = html + extenderHTML(addOns['extender-rightExtender50'], "Right Extender");
    }

    return html;
}


function extenderHTML(extender, type) {

    if (extender.selected != 'yes') {
        return '';
    }
    var deduct = '';
    var tag = '';
    var amount = '';

    if (extender.deduct == 'yes') {
        deduct = '(deduct)';
    }
    if (empty(extender.height)) {
        tag = 'Width';
        amount = extender.width;
    } else {
        tag = 'Height';
        amount = extender.height;
    }
    var html = '' +
        '<div class="cartDescriptionRowMain">' + type + ': ' + deduct + '</div>' +
        '<div class="cartDescriptionRowSecond">' + amount + 'mm</div>' + cartDescriptionInitials() ;

    return html;
}



function addExtenderHeight(addOns) {

    if (empty(addOns)) {
        return 0;
    }

    var topExtender = addOns['extender-topExtender'];
    if (!empty(topExtender)) {
        if (topExtender.deduct != 'yes' && topExtender.selected == 'yes') {
            return parseInt( topExtender.height);
        }
    }

    return 0;
}


function addExtenderWidth(addOns) {

    if (empty(addOns)) {
        return 0;
    }

    var width = 0;

    var leftExtender = addOns['extender-leftExtender'];
    var rightExtender = addOns['extender-rightExtender'];

    if (!empty(leftExtender)) {
        if (leftExtender.deduct != 'yes' && leftExtender.selected == 'yes') {
            width = width + parseInt(leftExtender.width);
        }
    }

    if (!empty(addOns.rightExtender)) {
        if (rightExtender.deduct != 'yes' && rightExtender.selected == 'yes') {
            width = width + parseInt(rightExtender.width);
        }
    }

    return width;
}


function validExtenderOptions() {

    return true;
    var addOns = formStatus.addOnChoices;
    if (empty(addOns)) {
        return true;
    }

    var leftExtender = addOns['extender-leftExtender'];
    var rightExtender = addOns['extender-rightExtender'];
    var topExtender = addOns['extender-topExtender'];

    if (!validSelectedExtenderOption(leftExtender, 'width', 'leftExtender')) {
        return false;
    }
    if (!validSelectedExtenderOption(rightExtender, 'width', 'rightExtender')) {
        return false;
    }
    if (!validSelectedExtenderOption(topExtender, 'height', 'topExtender')) {
        return false;
    }

    return true;
}


function validSelectedExtenderOption(extender,checkField, type) {

    if (empty(extender)) {
        return true;
    }
    if (extender.selected == 'yes' ) {

        if (extender[checkField] >= moduleInfo[type+'Min'] && extender[checkField] <= moduleInfo[type+'Max']) {
            return true;
        }

        return false;
    }

    return true;
}
