'use strict';

function createSillUpdater(id) {
    $('.dimKeyUp', id).unbind("keyup").bind("keyup", updateSillDimensions);

}


function createSillButtons(id) {
    $('.extendedButton', id).unbind("click").bind("click", extendedSelectedSill);
}


function buildSillForPriceEngine(options) {

    var sill = {};
    if (empty(options.sillOptions)) {
        return sill;
    }

    var selected = options.sillOptions.selected
    selected = splitForSecondOption(selected);

    sill.selected = selected;
    sill.width = options.sillOptions.width;
    sill.height = options.sillOptions.height;
    sill.depth = options.sillOptions.depth;
    sill.deduct = options.sillOptions.deduct;

    if (sill.selected == 'nosill') {
        sill = {};
    }

    return sill;
}


function sillHeightDeduct(sill) {

    if (empty(sill)) {
        return 0;
    }

    if (sill.deduct == 'yes') {
        return isNaN(parseInt(sill.height)) ? 0 : sill.height
    } else {
        return 0;
    }
}


function addSillHeight(sill) {

    if (!empty(sill)) {
        var deduct = sillHeightDeduct(sill);

        if (empty(deduct)) {
            return isNaN(parseInt(sill.height)) ? 0 : sill.height   ;
        }
    }

    return 0;
}


function buildSills(sillData) {
    
    var buttons = ''
    for (var a = 0; a < sillData.length; a++) {
        var button = buildExtendedButtonSill(sillData[a], 'sill');
        buttons = buttons + button;
    }

    return buttons;
}


function getDefaultsSillValues(button, option) {

    if (empty(button.defaults)) {
        return '';
    }

    var defaults = JSON.parse(button.defaults);

    if (defaults[option]) {
        return defaults[option]
    }

    return '';

}


function buildExtendedButtonSill(buttonData, option) {

    var id = option + '-' + buttonData.name;
    id = replaceSpaceWithDash(id)

    var height = getSillDimensionValue(id, 'height');
    var width = getSillDimensionValue(id, 'width');
    var depth = getSillDimensionValue(id, 'depth');

    height = empty(height) ? getDefaultsSillValues(buttonData, 'height') : height;
    var readOnlyHeight = getDefaultsSillValues(buttonData, 'readOnlyHeight');

    depth = empty(depth) ? getDefaultsSillValues(buttonData, 'depth') : depth;
    var readOnlyDepth = getDefaultsSillValues(buttonData, 'readOnlyDepth');

    var boxes = [
        {"text": "Height (Bottom to Top)", "unit": "mm", "value": height, "id": "sillHeight-" + id, "readOnly": readOnlyHeight},
        {"text": "Width (Left to Right)", "unit": "mm", "value": width, "id": "sillWidth-" + id, class: "width"},
        {"text": "Depth (Outside to Inside)", "unit": "mm", "value": depth, "id": "sillDepth-" + id,  "readOnly": readOnlyDepth}
    ];

    var radioButtons = buildRadioButtonDataYesNo('no', id);
    if (!empty(formStatus.sillOptions)) {
        radioButtons = buildRadioButtonDataYesNo(formStatus.sillOptions.deduct, id);
    }

    //var deduct = buildDeductRadioButtons(radioButtons, id);
    var deduct='';


    var dimBox = buildSillBoxes(boxes);

    if (buttonData.name == 'nosill') {
        dimBox = buttonData.highlight;
        deduct = '';
    }
    id = replaceSpaceWithDash(id)

    var buttonHTML = '' +
        '<div id="' + id + '" class="extendedButton">' +
        '   <div class="extendedButtonImage"><img src="' + buttonData.url + '"> </div>' +
        '   <div class="sillDimensionsTitle">' + buttonData.description + '</div>' +
        '   <div class="sillDimensions">' +
        '       ' + dimBox +
        '   </div>' +
        deduct +
        '   </div>';

    return (buttonHTML);
}


function buildSillBoxes(sillBoxes) {

    var dimBoxes = '';
    for (var a = 0; a < sillBoxes.length; a++) {
        dimBoxes = dimBoxes + buildDimensionBox(sillBoxes[a]);
    }

    return dimBoxes;
}


function updateSillDimensionsWithOptionSelected(optionSelected) {
    setSillOptions(optionSelected);
    updateFormPrice();
}

function updateSillDimensions() {
    var thisSelector = $(this);
    var optionSelected = thisSelector.parent().parent().parent().attr('id');

    updateSillDimensionsWithOptionSelected(optionSelected);
}


function setWidthBasedOnDoorWidth (id) {

    var selector = $(buildIDSelector(id));
    var value = selector.val();

    if (empty(value) ){
        selector.val(formStatus.width);
    }

    var width = getDimensionAndValidate(id, 'sillWidth');

    return width;
}


function setSillOptions(optionSelected, first) {

    if (!empty(first)) {
        $('.sillDimensionBox input.width').val('')
    }

    var height = getDimensionAndValidate('sillHeight-' + optionSelected, 'sillHeight');
    var width = getDimensionAndValidate('sillWidth-' + optionSelected, 'sillWidth');
    var depth = getDimensionAndValidate('sillDepth-' + optionSelected, 'sillDepth');

    if (!empty(first)) {
        width = setWidthBasedOnDoorWidth('sillWidth-' + optionSelected);
    }
    //deduct requested to be hardCoded to yes
    var deduct = radioValueFromOptionSelected(optionSelected);


    formStatus['sillOptions'] = {
        "selected": optionSelected,
        "height": height,
        "width": width,
        "depth": depth,
        "deduct": deduct
    };

    writeFormStatus();
}


function extendedSelectedSill() {
    $(".extraDim").removeClass("badDimension");

    var thisSelector = $(this);
    var optionSelected = thisSelector.attr('id');

    setSillOptions(optionSelected, "yes");

    var groupOfButtons = thisSelector.parent();
    var selectedDiv = $(".extendedSelectedDiv", groupOfButtons);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
    }


    var selector = $('.extendedButtonImage', this);
    displayExtendedSelected(selector);
    //scrollToNextPositon(nextSection);
    updateFormPrice();
    lockDownTabs();
}


function radioValueFromOptionSelected(optionSelected) {

    var name = "'deductSill-" + optionSelected + "'";

    var deduct = "";
    var selected = $("input[type='radio'][name=" + name + "]:checked");
    if (selected.length > 0) {
        deduct = selected.val();
    }

    //requested to be hardCoded to Yes
    deduct = 'yes';
    return deduct;
}


function buildRadioButtonDataYesNo(answer, id) {

    var data = {"no": 'checked="checked"', "yes": ""};
    if (formStatus.sillOptions.selected == id) {
        if (!empty(answer)) {
            if (answer == 'yes') {
                data = {"no": "", "yes": 'checked="checked"'};
            }
        }
    }
    formStatusDefaults

    return data;
}


function getSillDimensionValue(id, type) {

    var value = '';
    if (empty(formStatus.sillOptions)) {
        return '';
    }

    if (formStatus.sillOptions.selected == id) {
        value = formStatus.sillOptions[type];
    }

    return value;
}


function buildDeductRadioButtons(selected, id) {

    var radios = '' +
        '<input type="radio" name="deductSill-' + id + '" id="deductSillNo" class="deductRadioButton" value="no" ' + selected.no + ' ">' +
        '<div class="deductRadioButtonLabel">No</div>' +
        '<input type="radio" name="deductSill-' + id + '" id="deductSillYes" class="deductRadioButton" value="yes" ' + selected.yes + ' ">' +
        '<div class="deductRadioButtonLabel">Yes</div>';

    var deduct = ' ' +
        '   <div class="sillDimensionsDeductTitle">Deduct from door Height?</div>' +
        '   <div class="deductRadioButtonBox">' + radios + '</div>';

    return deduct;
}


function buildDimensionBox(box) {


    var readOnly = '';
    var dimKeyUp = 'dimKeyUp';
    if(box.class){
        dimKeyUp = 'dimKeyUp '+box.class
    }
    if (!empty(box.readOnly)) {
        readOnly = 'readOnly';
        dimKeyUp = '';
    }

    box.id = replaceSpaceWithDash(box.id)

    var box = '' +
        '<div class="sillDimensionBox">' +
        '   <div class="sillNote">' + box.text + '</div>' +
        '   <input class="textBox extraDim '+ dimKeyUp + '" type="text" id="' + box.id + '" value="' + box.value + '" ' + readOnly + '/>' +
        '   <div class="sillUnit">' + box.unit + '</div>' +
        '</div>';

    return box
}


function sillCartHTML(sill) {

    if (empty(sill)) {
        return '';
    }
    if (empty(sill.selected) || sill.selected == 'sill-nosill') {
        return '';
    }

    var deduct = '';
    if (sill.deduct == 'yes') {
        deduct = '(deduct)';
    }


    var sillInfo = '' +
        '<div class="cartDescriptionRowMain">Sill: '+ splitForSecondOption(sill.selected)+' ' + deduct + '</div>' +
        '<div class="cartDescriptionRowSecond">'+ sill.height + 'mm x '  + sill.width + 'mm x ' + sill.depth + 'mm</div>' + cartDescriptionInitials() ;

    if (sill.height < 1) {
        sillInfo = '' +
            '<div class="cartDescriptionRowMain">Sill: '+ splitForSecondOption(sill.selected)+ '</div>' +
            '<div class="cartDescriptionRowSecond"></div>' + cartDescriptionInitials() ;
    }
    return sillInfo;
}


function validSillOptions() {

    var sill = formStatus.sillOptions;

    if (empty(sill)) {
        return true;
    }

    if (empty(sill.selected) || sill.selected == 'sill-nosill') {
        return true;
    }

    if (sill.width < moduleInfo.sillWidthMin || sill.width > moduleInfo.sillWidthMax ) {
        return false;
    }

    if (sill.height < moduleInfo.sillHeightMin || sill.height > moduleInfo.sillHeightMax ) {
        return false;
    }

    if (sill.depth < moduleInfo.sillDepthMin || sill.depth > moduleInfo.sillDepthMax ) {
        return false;
    }

    isUK();
    return true ;
}


function setSillMinMaxWidth(width) {

    moduleInfo.sillWidthMin = width;
    moduleInfo.sillWidthMax = parseFloat(width) + parseFloat(moduleInfo.sillRange);
    writeLocalStorageWithKeyAndData("moduleInfo",moduleInfo);

}