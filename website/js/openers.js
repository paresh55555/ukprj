
'use strict';

function loadOpeners() {

    var url = 'html/openers.html';
    if (isUK) {
        url = 'html/openersUK.html';
    }

    $.get(url, function (html) {

        $("#headerImage").attr("src", "images/header3.png");

        buildAndAttachCorePage(html)
        $('#cartNav').show();
        selectTab('#openers');

        addFooter();

        updateFormPrice();
        $('#dynamicContent').css("visibility", "visible");
        makeNextSectionButton(1);
        showContentAndSaveState();

    });
}


