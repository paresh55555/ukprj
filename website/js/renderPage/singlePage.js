'use strict'


function renderPage() {

    setIframe();
    renderDynamicContent();

    buildHistory();
    loadTab();


}



function completeHeader() {

    //userMenu();


}



function renderDynamicContent() {

    var html = statusNavHTML();

    html += salesNavHTML();
    html += leadsNavHTML();
    html += accountingNavHTML();
    html += productionNavHTML();
    html += builderNavHTML();
    html += mainContentHTML();
    html += footerHTML();

    attachHtmlWithId(html, 'startOfDynamicContent');
}