function loadCutSheet() {

    $('#companyHeader').hide();

    buildCutSheets();

}


function buildJobInfo(jobInfo, page, numberOfPages) {
    var letter = buildLetterForNumberOfDoors(page + 1);

    var orderDueDateHTML = ''+
        '   <div>Order Date: ' + cleanDate(jobInfo.orderDate) + '</div>' +
        '   <div>Due Date: ' + cleanDate(jobInfo.dueDate) + '</div>' ;

    if (siteDefaults.salesFlow == 2) {
        orderDueDateHTML = ''+
            '   <div>Origination Date: ' + cleanDate(jobInfo.orderDate) + '</div>' +
            '   <div>Will Be Completed: ' + cleanDate(jobInfo.estComplete) + '</div>' ;

    }

    var cityState = '';
    if (!empty(jobInfo.city) || !empty(jobInfo.state)) {
        cityState = '   <div>' + jobInfo.city + ', ' + jobInfo.state + '</div>';
    }

    var companyName = '';
    if ( !empty(jobInfo.companyName) ) {
        companyName = '   <div>' + jobInfo.companyName + '</div>';
    }


    var html = '' +
        '<div class="rightTopBar">' +
        '   <div>' + jobInfo.id + '-' + letter + '</div>' +
        '   <div>Item ' + (page + 1) + ' of ' + numberOfPages + '</div>' +
        '   <div>' + jobInfo.lastName + ', ' + jobInfo.firstName + '</div>' +
        '   ' + cityState +
        '   ' + companyName +
        '   ' + orderDueDateHTML +
        '</div>';

    return html;
}


function buildHeader(header) {
    $('#material').html(header.material);
    $('#text').html(header.text);
    $('#image').attr("src", header.image);

}


function buildDoorDetails(sheet, vinylColor) {

    var details = sheet.details;

    var operation = '' +
        '   <tr>' +
        '       <td>Operation</td>' +
        '       <td>' + details.Operation + '</td>' +
        '   </tr>';

    var handles = '' +
        '   <tr class="alt">' +
        '       <td>Handles</td>' +
        '       <td>' + details.Handles + '</td>' +
        '   </tr>';

    var spacer = '' +
        '   <tr class="alt">' +
        '       <td>&nbsp;</td>' +
        '       <td>&nbsp;</td>' +
        '   </tr>';


    if (empty(details.Operation)) {
        operation = '';
        spacer = '';
    }

    if (empty(details.Handles)) {
        handles = '';
    }

    var finish = '' +
        '   <tr class="alt">' +
        '       <td>Interior Finish</td>' +
        '       <td>' + details['Interior Finish'] + '</td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td>Exterior Finish</td>' +
        '       <td>' + details['Exterior Finish'] + '</td>' +
        '   </tr>';


    if (!empty(sheet.header.vinylColor)) {
        finish = '' +
            '   <tr class="alt">' +
            '       <td>Color</td>' +
            '       <td>' + sheet.header.vinylColor + ' Vinyl</td>' +
            '   </tr>';
    }

    var itemsList = sheet.items;
    var numberOfItems = itemsList.length;
    var tableRows = '';
    for (var a = 0; a < numberOfItems; a++) {
        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = '';
        }
        var item = itemsList[a];

        if (item.name == "Keyed Alike") {
            details['Keyed Alike'] = "Yes";
        }
        if (item.name == "Door Restrictor") {
            details['Door Restrictor'] = "Yes";
        }
    }

    var html = '' +
        '<table>' +
        '   <tr>' +
        '        <th colspan="2" >Door Details</th>' +
        '   </tr>' +
        operation +
        finish +
        '   <tr class="alt">' +
        '   <td>Frame</td>' +
        '   <td>' + details.Frame + '</td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td>Track</td>' +
        '       <td>' + details.Track + '</td>' +
        '   </tr>' +
        handles +
        '   <tr>' +
        '       <td>Keyed Alike</td>' +
        '       <td>' + details['Keyed Alike'] + '</td>' +
        '   </tr>' +
        '   <tr>' +
        '       <td>Door Restrictor</td>' +
        '       <td>' + details['Door Restrictor'] + '</td>' +
        '   </tr>' +
        '</table>';

    return html;

}



function buildGlass(glassList, glassChoices) {

    var pieceOfGlass = glassList.length;

    var tableRows = [];
    for (var a = 0; a < pieceOfGlass; a++) {

        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = ''
        }

        var glass = glassList[a];

        var glassSize = ' ';
        var glassTitle = '';

        if (isUK()) {

            glassTitle = '' +
                '       <th>Glass</th>' +
                '       <th>Width MM</th>' +
                '       <th>Height MM</th>';

            glassSize = '' +
                '<td>' + glass['Glass Width Inches'] + '</td>' +
                '<td>' + glass['Glass Height Inches'] + '</td>';

        } else {

            glassSize = '' +
                '<td>' + glass['Glass Width Inches'] + '</td>' +
                '<td>' + glass['Glass Height Inches'] + '</td>' +
                '<td>' + glass['Glass Width MM'] + '</td>' +
                '<td>' + glass['Glass Height MM'] + '</td>';

            glassTitle = '' +
                '       <th>Glass</th>' +
                '       <th>Width Inches</th>' +
                '       <th>Height Inches</th>' +
                '       <th>Width MM</th>' +
                '       <th>Height MM</th>';


        }


        var row =
            '<tr ' + alt + ' >' +
            '   <td>' + glass.name + '</td>' +
            '   ' + glassSize +
            '   <td>' + glass.Quantity + '</td>' +
            '</tr>';
        tableRows.push(row);
    }

    var glassChoices = buildGlassOptionsRow(glassChoices);

    var html = '' +
        '<table class="glassCutSheets">' +
        '   <tr>' +
        '       ' + glassTitle +
        '       <th>QTY</th>' +
        '   </tr>' +
        tableRows +
        glassChoices +
        '</table>';

    return html;


}


function buildGlassOptionsRow(glassChoices) {

    var tableRows = [];
    for (var a = 0; a < glassChoices.length; a++) {

        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = ''
        }

        var glassChoice = glassChoices[a];

        var columnSpan = ' colspan="6" ';
        if (isUK()) {
            columnSpan = ' colspan="4" ';

        }

        var row =
            '<tr ' + alt + ' >' +
            '   <td >' + glassChoice.name + '</td>' +
            '   <td ' + columnSpan + ' >&nbsp;</td>' +
            '</tr>';
        tableRows.push(row);
    }


    return tableRows;
}


function buildDesign(design) {

    if (design['Swing Door'] == "None") {
        design['sliding left'] = 0;
        design['sliding right'] = 0;
    }
    var panelsLeft = '' +
        '   <tr>' +
        '      <td>Sliding Panels Left</td>' +
        '      <td>' + design['sliding left'] + '</td>' +
        '   </tr>';

    var panelsRight = '' +
        '   <tr class="alt">' +
        '      <td>Sliding Panels Right</td>' +
        '      <td>' + design['sliding right'] + '</td>' +
        '   </tr>';


    var swingDoors = '' +
        '   <tr class="alt">' +
        '      <td>Swing Doors</td>' +
        '      <td>' + design['Swing Door'] + '</td>' +
        '   </tr>';

    if (design.panels == 1) {
        panelsLeft = '';
        panelsRight = '';
        swingDoors = '';
    }


    var html = '' +
        '<table class="design">' +
        '   <tr>' +
        '      <th colspan="2">Design</th>' +
        '   </tr>' +
        '   <tr>' +
        '       <td>Total Panels</td>' +
        '       <td >' + design.panels + '</td>' +
        '   </tr>' +
        swingDoors +
        panelsLeft +
        panelsRight +
        '</table>';

    return html;
}

function buildSize(size) {


    var netSizeWidth = '';
    var netSizeHeight = '';
    var netSizeTitle = '';


    var netSizeWidthMM = size['width MM'];
    var netSizeHeightMM = size['height MM'];

    if (isUK()) {
        netSizeWidthMM = size['width inches'];
        netSizeHeightMM = size['height inches'];

    } else {
        netSizeTitle = '<th >INCHES</th>';
        netSizeWidth = '<td>' + size['width inches'] + '</td>';
        netSizeHeight = '<td>' + size['height inches'] + '</td>';
    }

    var html = '' +
        '<table class="size">' +
        '    <tr>' +
        '        <th >NET SIZE</th>' +
        netSizeTitle +
        '        <th >MM</th>' +
        '    </tr>' +
        '    <tr>' +
        '        <td>Width</td>' +
        netSizeWidth +
        '        <td>' + netSizeWidthMM + '</td>' +
        '    </tr>' +
        '    <tr class="alt">' +
        '        <td>Height</td>' +
        netSizeHeight +
        '        <td>' + netSizeHeightMM + '</td>' +
        '    </tr>' +
        '</table>';


    return html;
}


function buildParts(partsList) {

    var numberOfParts = partsList.length;
    var tableRows = [];
    for (var a = 0; a < numberOfParts; a++) {

        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = '';
        }

        var part = partsList[a];
        if (!part.partNumber) {
            part.partNumber = '&nbsp;';
        }
        if (!part.material) {
            part.material = '&nbsp;';
        }

        var size = '';

        if (isUK()) {
            size = '<td>' + part['Cut Size Inches'] + '</td>';
        } else {
            size = '   <td>' + part['Cut Size Inches'] + '</td>' +
                '   <td>' + part['Cut Size MM'] + '</td>';
        }

        var row = '<tr ' + alt + ' >' +
            '   <td>' + part.partNumber + '</td>' +
            '   <td>' + part.name + '</td>' +
            '   <td>' + part.material + '</td>' +
            size +
            '   <td>' + part['Lengths'] + '</td>' +
            '   <td>&nbsp; </td>' +
            '</tr>';

        tableRows = tableRows + row;
    }

    var sizeTitle = '       <th>MM</th>';
    if (!isUK()) {
        sizeTitle = '       <th>Inches</th>' + sizeTitle;
    }

    var html = '' +
        '<table class="partsCutSheets" >' +
        '   <tr>' +
        '       <th>Part #</th>' +
        '       <th>Cut Part</th>' +
        '       <th>Material</th>' +
        '       ' + sizeTitle +
        '       <th>Lengths</th>' +
        '       <th>Initial</th>' +
        '   </tr>' +
        tableRows +
        '</table>';

    return html;
}


function buildItems(itemsList) {

    var numberOfItems = itemsList.length;
    var tableRows = '';
    for (var a = 0; a < numberOfItems; a++) {
        var alt = 'class="alt"';
        if (a % 2 == 0) {
            alt = '';
        }
        var item = itemsList[a];
        if (!item.partNumber) {
            item.partNumber = '';
        }
        var row =
            '<tr' + alt + ' >' +
            '   <td>' + item.partNumber + '</td>' +
            '   <td>' + item.name + '</td>' +
            '   <td>' + item.quantity + '</td>' +
            '</tr>';
        tableRows = tableRows + row;
    }

    var html = '' +
        '<table>' +
        '   <tr>' +
        '       <th>Part #</th>' +
        '       <th>Items Description</th>' +
        '       <th>Quantity</th>' +
        '   </tr>' +
        tableRows +
        '</table>';

    return html;
}


function buildPanel(panelInfo) {

    var numberOfPanels = panelInfo.panels;
    var maxSize = panelMaxSizeCutSheets(numberOfPanels);
    var width = maxSize / numberOfPanels;
    var height = 120;

    var images = {
        "leftSwing": '<img  class="cutSheetImages" src="/images/swing-left.gif" width="' + width + '" height="' + height + '" >',
        "left": '<img class="cutSheetImages" src="/images/slide-left-w-arrow.gif" width="' + width + '" height="' + height + '">',
        "divider": '<img class="cutSheetImages" src="/images/Divider-Bar.gif" height="' + height + '">',
        "rightSwing": '<img class="cutSheetImages" src="/images/swing-right.gif" width="' + width + '" height="' + height + '">',
        "right": '<img class="cutSheetImages" src="/images/slide-right-w-arrow.gif" width="' + width + '" height="' + height + '">',
        "window": '<img class="cutSheetImages" src="/images/panels/window.svg" width="' + width + '" height="' + height + '">'
    }

    var html = '';
    if (panelInfo['Swing Door'] == "both") {
        html = buildPanelBoth(panelInfo.panels, panelInfo['sliding left'], images);
    }
    else if (panelInfo['Swing Door'] == "left") {
        html = buildPanelLeft(panelInfo.panels, images);
    }
    else if (panelInfo['Swing Door'] == "right") {
        html = buildPanelRight(panelInfo.panels, images);
    } else {
        panelInfo['Swing Door'] = 'None';
        html = buildPanelWindow(panelInfo.panels, images);
    }

    return html;
}


function buildPanelBoth(numberOfPanels, numberLeft, images) {

    var html = images.leftSwing;

    for (var a = 0; a < numberLeft; a++) {
        html = html + images.left;
    }

    html = html + images.divider;

    for (var a = 0; a < numberOfPanels - numberLeft - 2; a++) {
        html = html + images.right;
    }

    html = html + images.rightSwing;

    return html;
}


function buildPanelLeft(numberOfPanels, images) {


    var html = images.leftSwing;

    for (var a = 0; a < numberOfPanels - 1; a++) {
        html = html + images.left;
    }

    if (numberOfPanels > 1) {
        html = html + images.divider;
    }

    return html;
}


function buildPanelWindow(numberOfPanels, images) {
    var html = '';

    for (var a = 0; a < numberOfPanels; a++) {
        html = html + images.window;
    }

    return html;
}


function buildPanelRight(numberOfPanels, images) {
    var html = images.divider;
    if (numberOfPanels == 1) {
        html = '';
    }

    for (var a = 0; a < numberOfPanels - 1; a++) {
        html = html + images.right;
    }
    html = html + images.rightSwing;

    return html;
}






function buildCutSheets() {
    var id = getParameter('id');
    var site = getParameter('site');

    getCutSheetsWithIdAndSite(id, site).done(function (sheets) {

        stopSpinner();
        for (var page = 0; page < sheets.length; page++) {
            buildSingleSheet(page, sheets[page], sheets.length)
        }
    });
}


function buildNotes(note) {

    if (empty(note)) {
        return '';
    }
    var noteTitle = '<div class="noteTitle"> Notes:</div>';
    note = note.replace(/(?:\r\n|\r|\n)/g, '<br />');

    var noteBox = '<div class="noteBox">' + note + '</div>';


    return noteTitle + noteBox;
}


function buildSingleSheet(page, sheet, totalSheets) {

    var panels = buildPanel(sheet.design);
    var vinyl = sheet.percentage;
    var doorType = sheet.jobInfo.doorType;

    if (doorType === 'Absolute Vinyl ' && empty(vinyl)) {
        vinyl = [{"name": "White Vinyl"}];
    }

    var jobInfo = buildJobInfo(sheet.jobInfo, page, totalSheets);
    var design = buildDesign(sheet.design);
    var size = buildSize(sheet.design);
    var doorDetails = buildDoorDetails(sheet, vinyl);
    var glass = buildGlass(sheet.glass, sheet.glassChoices);
    var parts = buildParts(sheet.parts);
    //var items = buildItems(sheet.items);
    var items = '' //Blank per Alan
    var note = buildNotes(sheet.productionNotes);

    var vinylColor = '';
    if (!empty(sheet.header.vinylColor)) {
        vinylColor = ' - '+sheet.header.vinylColor + ' Vinyl';
    }


    var html = '' +
        '<div class=pageBox>' +
        '   <div id="cutSheetCompany">' + siteDefaults.companyName +'</div>' +
        '   <div class="topBar">' +
        '       <div class="cutSheetPanel">' +
        panels +
        '       </div>' +
        jobInfo +
        '   </div>' +
        '    <div class="cutDoorTitle">' + doorType + vinylColor + '</div>' +
        '   <div class="doorInfo">' +
        '       <div class="leftBox">' +
        design +
        size +
        '       </div>' +
        doorDetails +
        '   </div>' +
        glass +
        parts +
        items +
        note +
        '</div>';

    $('body').append(html);
}


function buildSingleGlassSheet(page, sheet, totalSheets) {

    var glass = buildGlass(sheet.glass, sheet.glassChoices);

    var vinylColor = '';
    if (!empty(sheet.header.vinylColor)) {
        vinylColor = ' - '+sheet.header.vinylColor + ' Vinyl';
    }

    var letter = buildLetterForNumberOfDoors(page + 1);
    var door = sheet.jobInfo.id + '-' + letter ;

    var html = '' +
        '   <div class="glassSheetSectionBox-doorTitle">' + door + '</div>' +
        '   '+ glass ;

    return html;
}


function glassSheetCompanyInfo() {

    var companyAddress = getCompanyAddress();
    var dealerLogo = '<img id="printLogo" src="' + siteDefaults.printLogoURL + '">';

    var html = '' +
        '<div class="glassSheetLogoBox keepTogether ">' +
        '   <div class="cartRowHeader"></div>' +
        '   <div class="cartRowHeaderLeft">' + dealerLogo + '</div>' +
        '       <div class="companyInfoTitle">' + siteDefaults.companyName + '</div>' +
        companyAddress +
        '  </div>' +
        '</div>' ;

    return html;
}

function buildGlassSheetCustomer(sheet) {

    if (sheet.jobInfo.glass != 'Glazed') {
        sheet.jobInfo.glass = 'Unglazed';
    }
    var html = ''+
        '<div class="glassSheetSectionBox-customer">' +
        '   <table class="glassCutSheets">' +
        '      <tbody>'+
        '          <tr>'+
        '              <th>Name</th>'+
        '              <th>Company</th>'+
        '              <th>Ref</th>'+
        '              <th>Glazed / Unglazed</th>'+
        '          </tr>'+
        '          <tr>'+
        '              <td>'+ sheet.jobInfo.firstName +' ' + sheet.jobInfo.lastName   + '</td>'+
        '              <td>'+ returnBlankIfEmpty(sheet.jobInfo.companyName) +'</td>'+
        '              <td>'+ returnBlankIfEmpty(sheet.jobInfo.ref) +'</td>'+
        '              <td>' + sheet.jobInfo.glass + '</td>'+
        '          </tr>'+
        '      </tbody>'+
        '   </table>' +
        '</div>';

    return html;
}



function loadGlassSheets() {
    $('#companyHeader').hide();
    var id = getParameter('id');
    var site = getParameter('site');

    getCutSheetsWithIdAndSite(id, site).done(function (sheets) {
        stopSpinner();
        var glassHtml = '';
        for (var page = 0; page < sheets.length; page++) {
            glassHtml = glassHtml + buildSingleGlassSheet(page, sheets[page], sheets.length)
        }

        var customerDetails = buildGlassSheetCustomer(sheets[0]);
        // buildGlassSheetCustomer();

        var html = '' +
            glassSheetCompanyInfo() +
            '<div class=pageBox>' +
            '   <div class="glassSheetSectionBox">' +
            '       <div class="glassSheetSectionBox-header">' +
            '           <div class="glassSheetSectionBox-title">Customer Details</div>' +
            '       </div>' +
            '   ' + customerDetails +
            '   </div>' +
            '   <div class="glassSheetSectionBox">' +
            '       <div class="glassSheetSectionBox-header">' +
            '           <div class="glassSheetSectionBox-title">Glass Details</div>' +
            '       </div>' +
            '       <div class="glassSheetSectionBox-glass">' +
            '           ' + glassHtml +
            '       </div>' +
            '   </div>' +
            '</div>';


        $('body').append(html);
    });
}

function buildSingleGlassLabels(page, sheet, totalSheets) {

    var letter = buildLetterForNumberOfDoors(page);
    var header = '' +
        '<div class="labelSection">' +
            '<div class="companyName">' + siteDefaults.companyName + '</div>' +
            '<div>Job number: ' + sheet.jobInfo.id + '-' + letter + '</div>' +
            '{panelTmpl}' +
        '</div>' +
        '<hr>';
    var glass = buildGlassLabels(header, sheet.glass, sheet.glassChoices);

    var html = '' +
        ''+ glass ;

    return html;
}

function buildGlassLabels(header, glassList, glassChoices){
    var html = '';
    var pieceOfGlass = glassList.length;
    var glassName = '';

    for (var a = 0; a < pieceOfGlass; a++) {
        var glass = glassList[a],
            quantity = parseInt(glass.Quantity);
        if(glass.name === 'Low-E 2'){
            glassName = 'Solarban 60';
        }else if(glass.name === 'Low-E 3'){
            glassName = 'Solarban 70';
        }else{
            glassName = glass.name;
        }
        for(var i = 0; i < quantity; i++){
            var count = i + 1,
                panelTmpl = '<div>Qty: ' + count + ' of ' + quantity + ' {side}</div>';
            html+= '' +
                '<div class="glassLabelBoxLeft">' +
                    header.replace('{panelTmpl}',panelTmpl.replace('{side}', '(outside)')) +
                    '<div class="labelSection">' +
                        '<div class="labelBoldTitle">' + glassName + '</div>' +
                        '5/32" (4mm) Tempered Glass' +
                    '</div>' +
                    '<div class="labelSection">' +
                        '<div style="font-weight:bold">Size:</div>' +
                        '<div>' + glass['Glass Width Inches'] + 'in x' + glass['Glass Height Inches'] + 'in CL T</div>' +
                        '<div>' + glass['Glass Width MM'] + 'mm x' + glass['Glass Height MM'] + 'mm CL T</div>' +
                    '</div>' +
                '</div>' +
                '<div class="glassLabelBoxRight">' +
                    header.replace('{panelTmpl}',panelTmpl.replace('{side}', '(inside)')) +
                    '<div class="labelSection">' +
                        '<div class="labelBoldTitle">Clear Glass</div>' +
                        '5/32" (4mm) Tempered Glass' +
                    '</div>' +
                    '<div class="labelSection">' +
                        '<div style="font-weight:bold">Size:</div>' +
                        '<div>' + glass['Glass Width Inches']  + 'in x ' + glass['Glass Height Inches'] + 'in CL T</div>' +
                        '<div>' + glass['Glass Width MM']+ 'mm x ' + glass['Glass Height MM']  + 'mm CL T</div>' +
                    '</div>' +
                '</div>';
        }
    }
    return html;
}

function buildDeliveryLabels(page, sheet, orderInfo) {

    var letter = buildLetterForNumberOfDoors(page);

    var html = '';
    for (var panel = 0; panel < sheet.design.panels; panel++) {
        html = html + buildDeliveryLabel(panel, sheet, orderInfo, letter);
    }
    var additionalLabels = '';
    var extraItems = 4;
    if (!empty(sheet.design['Swing Door'])) {
        if (sheet.design['Swing Door'] == 'both') {
            extraItems = 5;
        }
    }

    for (var label = 1; label < extraItems; label++) {
        additionalLabels = additionalLabels + buildDeliveryLabel(parseInt(sheet.design.panels) + label, sheet, orderInfo, letter, label);
    }

    return html + additionalLabels;
}

function buildDeliveryLabel(panel, sheet, orderInfo, letter, additional){

    if (siteDefaults.salesFlow == 2) {
        orderInfo.shipping_contact = orderInfo.Customer.firstName  + ' ' + orderInfo.Customer.lastName;
        orderInfo.shipping_address1 = orderInfo.Customer.billingAddress1;
        orderInfo.shipping_address2 = orderInfo.Customer.billingAddress2;
        orderInfo.shipping_city = orderInfo.Customer.billingCity;
        orderInfo.shipping_state = orderInfo.Customer.billingState;
        orderInfo.shipping_zip = orderInfo.Customer.billingZip;
        orderInfo.shipping_email = orderInfo.Customer.email;

    }

    additional = additional || false;
    var html = '';

        var getAdditionalTitle = function(panelNum, sheet){
            switch (panelNum) {
                case 1:
                    return 'Top & Bottom Track';
                case 2:
                    return 'Jambs';
                case 3:
                    return 'Handle Set:' + sheet.details.Handles;
                case 4:
                    return 'Handle Set:' + sheet.details.Handles;
                default:
                    break;
            }
        };

    var keyedAlike = "";
    if (!empty(sheet.details['Keyed Alike'])) {
        if (sheet.details['Keyed Alike'] == 'yes') {
            keyedAlike = ' (Keyed Alike)';
        }
    }


    var extraItems = 3;
    if (!empty(sheet.design['Swing Door'])) {
        if (sheet.design['Swing Door'] == 'both') {
            extraItems = 4;
        }
    }


    html = html +
        '<div class="deliveryLabel">' +
            '<div class="deliveryBox left">' +
                '<div class="bold">Delivery address:</div>' +
                '<div>' + orderInfo.shipping_contact + '</div>' +
                '<div>' + orderInfo.shipping_address1 + orderInfo.shipping_address2 + '</div>' +
                '<div>' + orderInfo.shipping_city + ',' + orderInfo.shipping_state + ' ' + orderInfo.shipping_zip + '</div>' +
                '<div class="bottomBox">Delivery item ' + (additional ? panel : panel + 1) + ' of ' + (parseInt(sheet.design.panels) + extraItems) + '</div>' +
            '</div>' +
            '<div class="deliveryBox">' +
                '<div class="rightTitle bold">' + (additional ?
                    getAdditionalTitle(additional, sheet)
                :
                    'Panel ' + (panel + 1) + ' of ' + sheet.design.panels
                ) + '</div>' +
                '<div class="orderNum bold">' + sheet.jobInfo.id + '-' + letter + '</div>' +
                '<div class="bottomBox">' +
                    '<div class="bold">Door Details:</div>' +
                    '<div>Color: ' + sheet.header.vinylColor + '</div>' +
                    '<div>Hardware: ' + sheet.details.Handles + keyedAlike + '</div>' +
                    '<div>Glass: ' + (sheet.glass && sheet.glass.length && sheet.glass[0].name) + '</div>' +
                '</div>' +
            '</div>' +
        '</div>';
    return html;
}

function buildPanelLabels(page, sheet, orderInfo) {

    if (siteDefaults.salesFlow == 2) {
        orderInfo.shipping_contact = orderInfo.Customer.firstName  + ' ' + orderInfo.Customer.lastName;
        orderInfo.shipping_address1 = orderInfo.Customer.jobAddress1;
        orderInfo.shipping_address2 = orderInfo.Customer.jobAddress2;
        orderInfo.shipping_city = orderInfo.Customer.jobCity;
        orderInfo.shipping_state = orderInfo.Customer.jobState;
        orderInfo.shipping_zip = orderInfo.Customer.jobZip;
        orderInfo.shipping_email = orderInfo.Customer.email;
    }


    var letter = buildLetterForNumberOfDoors(page),
        html = '',
        getPanelImage = function(name, num, pos, width){
            pos = pos || '';
            var styleWidth = width ? 'width:' + width + 'px' : '';
            return '<div class="panelImage ' + pos + '"><img style="'+styleWidth+'" src="/images/printPanels/' + name + '" /><div class="number">' + num + '</div></div>';
        },
        getPanelImages = function(panel, sheet, total){
            var imagesHtml = '',
                width = false,
                direction = sheet.design['Swing Door'];
            if(total > 10)width = Math.floor((816-6)/total); // 816 - width of page; 6 - width of borders (first/last pages)
            if(direction === 'both'){
                var slidingLeft = sheet.design['sliding left'],
                    slidingRight = sheet.design['sliding right'];
                for (var i = 1; i <= total; i++) {
                    if (i === 1) {
                        imagesHtml = imagesHtml + getPanelImage('swing-left.png', i, 'first', width);
                        continue;
                    }
                    if (i === total) {
                        imagesHtml = imagesHtml + getPanelImage('swing-right.png', i, 'last', width);
                        continue;
                    }
                    if(slidingLeft > 0){
                        imagesHtml = imagesHtml + ((panel === i) ? getPanelImage('callout-left.png', i, '', width) : getPanelImage('slide-left.png', i, '', width));
                        slidingLeft--;
                    }else if(slidingRight > 0){
                        imagesHtml = imagesHtml + ((panel === i) ? getPanelImage('callout-right.png', i, '', width) : getPanelImage('slide-right.png', i, '', width));
                        slidingRight--;
                    }
                }
            }else{
                for (var i = 1; i <= total; i++) {
                    if (i === 1) {
                        imagesHtml = imagesHtml + (direction === 'left' ?
                            getPanelImage('swing-left.png', i, 'first', width) :
                            (panel === i) ? getPanelImage('callout-right.png', i, 'first', width) : getPanelImage('slide-right.png', i, 'first', width));
                        continue;
                    }
                    if (i === total) {
                        imagesHtml = imagesHtml + (direction === 'left' ?
                            (panel === i) ? getPanelImage('callout-left.png', i, 'last', width) : getPanelImage('slide-left.png', i, 'last', width) :
                            getPanelImage('swing-right.png', i, 'last', width));
                        continue;
                    }
                    imagesHtml = imagesHtml + (direction === 'left' ?
                        (panel === i) ? getPanelImage('callout-left.png', i, '', width) : getPanelImage('slide-left.png', i, '', width) :
                        (panel === i) ? getPanelImage('callout-right.png', i, '', width) : getPanelImage('slide-right.png', i, '', width));
                }
            }
            return imagesHtml;
        };
    for (var panel = 1; panel <= sheet.design.panels; panel++) {

        var shipping2HTML = '';

        if (!empty(orderInfo.shipping_address2)) {
            shipping2HTML = '<div>' + orderInfo.shipping_address2 + '</div>' ;
        }
        html = html +
            '<div class="panelLabel">' +
                '<div class="panelLabelHeader">' +
                    '<div><img class="panelLogo" src="' + siteDefaults.printLogoURL + '"></div>' +
                    '<div class="panelOrderNum">' + sheet.jobInfo.id + '-' + letter + '</div>' +
                    '<div class="panelCounter">Panel ' + (panel) + ' of ' + sheet.design.panels + '</div>' +
                '</div>' +
                '<div class="panelLabelImages">' +
                    '<div class="imagesWrapper">' +
                        getPanelImages(panel, sheet, parseInt(sheet.design.panels)) +
                    '</div>' +
                '</div>' +
                '<div class="panelLabelDetails">' +
                    '<div class="panelInfo">' +
                        '<div class="infoTitle">Customer Details:</div>' +
                        '<div class="infoBody">' +
                            '<div>' + orderInfo.shipping_contact + '</div>' +
                            '<div>' + orderInfo.shipping_address1 + '</div>' +
                            '' + shipping2HTML +
                            '<div>' + orderInfo.shipping_city + ', ' + orderInfo.shipping_state + ' ' + orderInfo.shipping_zip + '</div>' +
                            '<div>' + orderInfo.shipping_email + '</div>' +
                        '</div>' +
                    '</div>'+
                    '<div class="panelInfo">' +
                        '<div class="infoTitle">Product Details:</div>' +
                        '<div class="infoBody">' +
                            '<div><span>Series: </span>' + sheet.jobInfo.doorType + '</div>' +
                            '<div><span>Size: </span>' + sheet.design['width inches'] + ' x ' + sheet.design['height inches'] + ' - ' + sheet.design.panels + 'Panels</div>' +
                            '<div><span>Operation: </span>' + sheet.details.Operation + '</div>' +
                            '<div><span>Finish: </span>' + sheet.details['Exterior Finish'] + '/' + sheet.details['Interior Finish'] + '</div>' +
                            '<div><span>Color: </span>' + sheet.header.vinylColor + '</div>' +
                            '<div><span>Handle: </span>' + sheet.details.Handles + '</div>' +
                            '<div><span>Track: </span>' + sheet.details.Track + '</div>' +
                            '<div><span>Glass: </span>' + (sheet.glass && sheet.glass.length && sheet.glass[0].name) + '</div>' +
                        '</div>' +
                    '</div>'+
                '</div>' +
            '</div>' +
            (panel%2 === 0 ? '<div class="pb"></div>' : '');
    }

    return html;
}

function loadGlassLabels() {
    $('#companyHeader').hide();
    var id = getParameter('id');
    var site = getParameter('site');
    var token = getParameter('token');
    var production = getParameter('production');
    var deliveryLabelsHtml;
    var panelLabelsHtml;

    var url = "https://" + apiHostV1 + "/api/orders/" + id + "?&token=" + token + "&production=" + production + addSite();
    $.getJSON(url, function (orderInfo) {

        getCutSheetsWithIdAndSite(id, site).done(function (sheets) {
            stopSpinner();
            var labelsHtml = deliveryLabelsHtml = panelLabelsHtml = '';

            for (var page = 0; page < sheets.length; page++) {
                deliveryLabelsHtml = deliveryLabelsHtml + buildDeliveryLabels(page+1, sheets[page], orderInfo);
            }
            for (var page = 0; page < sheets.length; page++) {
                panelLabelsHtml = panelLabelsHtml + buildPanelLabels(page+1, sheets[page], orderInfo);
            }
            for (var page = 0; page < sheets.length; page++) {
                labelsHtml = labelsHtml + buildSingleGlassLabels(page+1, sheets[page], sheets.length);
            }

            var html = '' +
                '<div class="panelLabelsPage">' + panelLabelsHtml + '</div>' +
                '<div class="deliveryLabelsPage">' + deliveryLabelsHtml + '</div>' +
                '<div class="pageBoxGlassLabels">' +
                    '<div class="glassLabelsSectionBox">' +
                        labelsHtml +
                    '</div>' +
                '</div>';

            $('body').append(html);
        });
    }).error(function (data) {
        reLoginError(data);
    }).fail(function () {
        bounceUser();
    });
}