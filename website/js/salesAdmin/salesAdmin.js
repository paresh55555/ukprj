var code = {};

function launchSalesAdmin() {

    if (isLimitedAccount()) {
        limitedAccessAccount();
        return;
    }
    screenDim();
    discounts.Totals.preCost = getCostOfItemsInCart(quote.Cart);
    buildSalesAdmin('#salesAdminMasterBox');

}


function addSalesAdminWithLocation(location) {

    var total = numberOfItemInCart(quote.Cart);

    var html = '<img onclick="launchSalesAdmin()" id="saleAdminLauncher" src="/images/footer/gears.png">';

    var myCheck  = $.cookie('user');


    if (siteDefaults.hasSalesAdmin == 1 && total > 0 && (user.account == 'on' || user.account == 'limited') ) {
        $(location).html(html).show();
    }
    if (isAccounting()) {
        $(location).html(html).show();
    }
    if (!empty(user.superToken) && siteDefaults.superHasSalesAdmin == 1 && total > 0) {
        $(location).html(html).show();
    }

    if (isAudit()) {
        $(location).html(html).hide();
    }
}

function validateTotals(defaults) {

    var amounts = ['extra'];

    for (var a = 0; a < amounts.length; a++) {
        var valid = amounts[a];

        if (!defaults[valid]) {
            defaults[valid] = {"name": '', "amount": 0, "showOnQuote": false};

        }
    }

    return defaults;
}


function getJustSalesAdminDefaults() {

    var url = "https://" + apiHostV1 + "/api/salesperson/" + user.id + "/defaults?" + authorizePlusSalesPerson();
    $.getJSON(url, function (apiJSON) {
        validate(apiJSON);
        discounts = apiJSON;
        discounts.Totals.cost = {amount: formStatus.total, showOnQuote: true};
        writeDiscount(discounts);

    });

}

function leadQuoteReset() {

    var discounts2 = jQuery.extend({}, defaults);
    discounts2.Totals.code == 'SQ-00';
    discounts2.Totals.preCost = discounts.Totals.preCost;
    discounts = discounts2;


}

function hasCustomColorInCart(){
    var hasCustomColor = false;
    if(quote && quote.Cart){
        for (var property in quote.Cart) {
            if((quote.Cart[property] && quote.Cart[property].extColor && quote.Cart[property].extColor.kind === 'custom') ||
               (quote.Cart[property] && quote.Cart[property].intColor && quote.Cart[property].intColor.kind === 'custom')){
               hasCustomColor = true;
            }
        }
    }
    return hasCustomColor;
}

function buildSalesAdmin(selector) {


    var tab = getParameter('tab');

    var workingDiscounts = jQuery.extend({}, discounts);

    if (tab == 'viewAccount') {
        workingDiscounts = jQuery.extend({}, defaults);
    }

    if (empty(workingDiscounts)) {
        leadQuoteReset();
        workingDiscounts = jQuery.extend({}, discounts);
    }
    if (empty(workingDiscounts.Totals)) {
        leadQuoteReset();
        workingDiscounts = jQuery.extend({}, discounts);
    }
    if (empty(workingDiscounts.Totals.code)) {
        leadQuoteReset();
        workingDiscounts = jQuery.extend({}, discounts);
    }
    if (empty(workingDiscounts.Totals.installation)) {
        leadQuoteReset();
        workingDiscounts = jQuery.extend({}, discounts);
    }
    if((formStatus && formStatus.extColor && formStatus.extColor.kind === 'custom') || hasCustomColorInCart()){
        if (empty(workingDiscounts.Totals.additionalExtras)) {
            workingDiscounts.Totals.additionalExtras = [];
        }
        if(!workingDiscounts.Totals.additionalExtras.length ||
            !workingDiscounts.Totals.additionalExtras.find(function(extra){return extra.name === "Custom Color"})){
            var addExtra = {"name": "Custom Color", "amount": siteDefaults.custom_color_price, "showOnQuote": true};
            workingDiscounts.Totals.additionalExtras.push(addExtra);
        }
    }else{
        if(workingDiscounts.Totals.additionalExtras && workingDiscounts.Totals.additionalExtras.length){
            var idx = workingDiscounts.Totals.additionalExtras.findIndex(function(extra){return extra.name === "Custom Color"});
            if(idx !== -1){
                workingDiscounts.Totals.additionalExtras.splice(idx, 1);
            }
        }
    }


    var otherBoxTotals = buildOtherBoxTotals(workingDiscounts);
    workingDiscounts.Totals = validateTotals(workingDiscounts.Totals);


    var markUpText = '';
    workingDiscounts.Totals.cost.amount = calculateCostFromCodeSA(workingDiscounts.Totals.code);

    var discountBox = buildDiscountBox(workingDiscounts.Discounts);
    var discountBoxTotals = buildDiscountBoxTotals(workingDiscounts);

    var taxBox = buildTaxBox(workingDiscounts);

    var screensBox = buildScreens(workingDiscounts);
    var screensBoxTotals = buildScreensTotals(workingDiscounts);


    var saveButton = '<div class="SABottomButton"><div onclick="salesAdminApplyChanges()"  id="' + quoteID + '-SaveChanges" class="editDiscountButtons myGreen" > Save To Quote</div></div>';

    if (empty(quote.id)) {
        saveButton = '<div class="SABottomButton"><div onclick="clearSalesAdminCart()"  class="editDiscountButtons myGreen" > Done </div></div>';
    }
    if (tab == 'viewAccount') {
        saveButton = '<div class="SABottomButton"><div onclick="salesAdminDefaults()"  id="SaveChanges" class="editDiscountButtons myGreen" > Save Defaults </div></div>';
        markUpText = '<div class="markUpText">*After the dash is the default amount of mark up you are applying to the quote</div>';
    }

    var shipping = '<div class="salesAdminTitle">' + capitaliseFirstLetter(tax) + ', Shipping & Installation</div>';
    if (isUK()) {
        shipping = '<div class="salesAdminTitle">' + tax + ', Delivery & Installation</div>';
    }


    var preTax = '';
    if (siteDefaults.preTax == 1) {

        var code = calculateMarkUpWithCode(workingDiscounts.Totals.code);
        preTax = ''+
            '       <div class="salesAdminSplitLeft"> ' +
            '           <div class="specialTaxBox"> ' +
            '               <div class="specialTax">Tax </div>' +
            '           <div class="specialTaxBoxAmount">' +
            '       <input onkeyup="updateCodeSApreTax()" id="codeSApreTax" class="SACodePreTax" type="text" name="code"   value="' + code + '"/>' +
            '</div>' +
            '           </div>' +
            '       </div>' +
            '       <div class="salesAdminSplitRight"> ' +
            '           <div class="specialTaxBox"> ' +
            '           </div>' +
            '       </div>' ;

    }


    var saCode = '' +
        '       <div class="SACodeText">Code</div>' +
        '       <input onkeyup="updateCodeSA()" id="codeSA" class="SACode" type="text" name="code"  value="' + workingDiscounts.Totals.code + '"/>' ;

    //Robert Request this locked down 6/29/18
    saCode = '' +
        '       <div class="SACodeText">Code</div>' +
        '       <input readonly id="codeSA" class="SACode" type="text" name="code"  value="' + workingDiscounts.Totals.code + '"/>' ;

    if (siteDefaults.preTax == 1) {
        saCode = '' +
        '       <div class="SACodeText"  style="visibility:hidden">Code</div>' +
        '       <input onkeyup="updateCodeSA()" style="visibility:hidden" id="codeSA" class="SACode" type="text" name="code"   value="' + workingDiscounts.Totals.code + '"/>' ;

        //Robert Request this locked down 6/29/18
        saCode = '' +
            '       <div class="SACodeText"  style="visibility:hidden">Code</div>' +
            '       <input readonly style="visibility:hidden" id="codeSA" class="SACode" type="text" name="code"   value="' + workingDiscounts.Totals.code + '"/>' ;

    }



    var html = '' +
        '   <div id="salesAdminBox">' +
        '   '+ preTax +
        '       <div class="salesAdminSplitLeft"> ' +
        '           <div class="discountMainBox"> ' +
        '               <div class="salesAdminTitle">Discount</div>' +
        '               <div class="salesAdminText">Enter name and amount of discount </div>' +
        discountBox +
        '           </div>' +
        '       </div>' +
        '       <div class="salesAdminSplitRight"> ' +
        '           <div class="discountMainBoxTotals"> ' +
        '               <div class="salesAdminTitle">Discount Totals</div>' +
        '               <div class="salesAdminText">Totals based on info entered to the left.</div>' +
        discountBoxTotals +
        '           </div>' +
        '       </div>' +
        '       <div class="totalsFullLine"></div>' +
        '       <div class="salesAdminSplitLeft"> ' +
        '           <div class="discountTaxBox"> ' +
        screensBox +
        '           </div>' +
        '       </div>' +
        '       <div class="salesAdminSplitRight"> ' +
        '           <div class="discountOtherTotals"> ' +
        screensBoxTotals +
        '           </div>' +
        '       </div>' +

        '       <div class="totalsFullLine"></div>' +
        '       <div class="salesAdminSplitLeft"> ' +
        '           <div class="discountTaxBox"> ' +
        '               ' + shipping +
        '               <div class="salesAdminText">Enter in appropriate information.  </div>' +
        taxBox +
        '           </div>' +
        '       </div>' +
        '       <div class="salesAdminSplitRight"> ' +
        '           <div class="discountOtherTotals"> ' +
        '               <div class="salesAdminTitle">Other Totals</div>' +
        '               <div class="salesAdminText">Totals based on info entered to the left.</div>' +
        otherBoxTotals +
        '           </div>' +
        '       </div>' +
        '       <div class="totalsFullLine"></div>' +
        markUpText +
        saCode +
        saveButton +
        '   </div>';


    $(selector).html(html);
    if(isSalesAdminLimited()){
         $(selector).find('input').attr('disabled', true);
         $("#extraTextInput, #extraAmounInput, input[id^='additionalExtraAmounInput-'], input[id^='additionalExtraTextInput-']").attr('disabled', false)
    }
    $(selector).show();
    addNewFieldStatus(workingDiscounts);


}
function clearSalesAdminBox() {

    $('#salesAdminMasterBox').hide();
    $('#salesAdminBox').remove();
    screenClear();


}
function clearSalesAdminCart() {
    clearSalesAdminBox();
    trackAndDoClick('cart');
}


function clearSalesAdmin() {

    clearSalesAdminBox();
    //if (!empty(quote)){
    //    if (!empty(quote.id)){
    //        trackAndDoClick('viewQuote', '&quote=' + quote.id + '&');
    //    }
    //}

}
function salesAdminApplyChanges() {

    if (quote.id) {
        clearSalesAdminBox()

        saveQuoteToDB();

    } else {
        clearSalesAdmin();
    }

}

function updateExtraSA(row) {
    var val = $('#extraTextInput').val();
    val = sanitizeString(val);
    val = handleBlankText(val);
    $('#extraName').html(val);

    discounts.Totals.extra.name = val;
    writeDiscount(discounts);

}

function buildScreens(data) {

    var totals = data.Totals;

    var html =
        ' <div class="saDiscountBox">' +
        '   <div class="discountRowTaxBox"><input tabindex="390" onkeyup="updateTaxBox()" id="screensBox" class="fixedAmount" type="text"   value="' + totals.screens.amount + '"/></div>' +
        '   <div class="discountRowTaxBox">Enter Costs for All Screens</div>' +
        ' </div>';

    return html;

}


function buildTaxBox(data) {

    var totals = data.Totals;

    if (empty(totals.installation.type)) {
        totals.installation.type = '';
    }

    var html = usTaxBox(totals);
    if (isUK()) {
        html = ukTaxBox(totals);
    }

    return html;
}


function ukTaxBox(totals) {

    var html = ' ' +
        '<div class="saDiscountBox">' +
        '   <div class="discountRowTaxBoxDelete"></div>' +
        '   <div class="discountRowTaxBox"><input tabindex="395" onkeyup="updateTaxBox()" id="shippingBox" class="fixedAmount" type="text"   value="' + totals.shipping.amount + '"/></div>' +
        '   <div class="discountRowTaxBox">Delivery</div>' +
        '</div>' +
        '<div class="saDiscountBox">' +
        '   <div class="discountRowTaxBoxDelete"></div>' +
        '   <div class="discountRowTaxBox"><input tabindex="402" onkeyup="updateTaxBox()" id="installationBox" class="fixedAmount" type="text"   value="' + totals.installation.amount + '"/></div>' +
        '   <div class="discountRowTaxBox">Installation</div>' +
        '   <div class="discountRowTaxBox"><input tabindex="400" onkeyup="updateTaxBox()" id="installationType" class="fixedAmount" type="text"   value="' + totals.installation.type + '"/></div>' +
        '</div>' +
        extrasNamesHTML(totals) +
        additionalExtrasNamesHTML(totals) +
        '<div class="saDiscountBox">' +
        '   <div class="discountRowTaxBoxDelete"></div>' +
        '   <div class="discountRowTaxBox"><input tabindex="490" onkeyup="updateTaxBox()" id="tax" class="percentAmount" type="text"   value="' + totals.tax.amount + '"/></div>' +
        '   <div class="discountRowTaxBox">' + tax + ' %</div>' +
        '</div>' +
        '<div class="discountButtonBox">' +
        '   <div id="addExtrasButton" class="addExtrasButton" onclick="addSalesAdminExtra()"> Add Extra </div>' +
        '</div>';


    return html;
}

function additionalExtrasNamesHTML(totals) {

    if (empty(totals.additionalExtras)) {
        return '';
    }

    var html = '';

    for (var a = 0; a < totals.additionalExtras.length; a++) {

        var rowHTML = additionalExtrasNamesRowHTML(totals.additionalExtras[a], a);
        html = html + rowHTML;
    }

    return html;
}

function additionalExtrasNamesRowHTML(additionalExtra, a) {

    var tab1 = 404 + 2*a;
    var tab2 = tab1+1;


    var html = '' +
        '<div class="saDiscountBox">' +
        '   <div class="discountRowTaxBoxDelete">' +
        '       <div class="additionalExtraRowBoxDelete" onclick="additionalExtraDelete('+a+')">Delete</div>' +
        '   </div>' +
        '   <div class="discountRowTaxBox">' +
        '       <input tabindex="'+tab2+'" onkeyup="updateAdditionalExtraAmount('+a+')" id="additionalExtraAmounInput-'+a+'" class="fixedAmount" type="text"   value="' + additionalExtra.amount + '"/>' +
        '   </div>' +
        '   <div class="discountRowTaxBox">' +
        '       <input tabindex="'+tab1+'" onkeyup="updateAdditionalExtra('+a+')" id="additionalExtraTextInput-'+a+'" class="discountName" type="text"  maxlength="16" value="' + additionalExtra.name + '"/>' +
        '   </div>' +
        '</div>' ;

    return html;
}

function extrasNamesHTML(totals) {

    var html = '' +
    '<div class="saDiscountBox">' +
        '   <div class="discountRowTaxBoxDelete"></div>' +
        '   <div class="discountRowTaxBox"><input tabindex="404" onkeyup="updateTaxBox()" id="extraAmounInput" class="fixedAmount" type="text"   value="' + totals.extra.amount + '"/></div>' +
    '   <div class="discountRowTaxBox"><input tabindex="403" onkeyup="updateExtraSA()" id="extraTextInput" class="discountName" type="text"  maxlength="16" value="' + totals.extra.name + '"/></div>' +
    '</div>' ;

    return (html);
}

function usTaxBox(totals) {

    var resaleNumber = returnBlankIfUndefined(totals.tax.resale)

    var usTax = ''+
        '<div class="saDiscountBox">' +
        '           <div class="discountRowTaxBoxDelete"></div>' +
        '           <div class="discountRowTaxBox"><input tabindex="390" onkeyup="updateTaxBox()" id="tax" class="percentAmount" type="text"   value="' + totals.tax.amount + '"/></div>' +
        '           <div class="discountRowTaxBox">' + tax + ' %</div>' +
        '           <div class="discountRowTaxBox"><input tabindex="390" onkeyup="updateTaxBox()" id="resale" class="resell resellSmall" type="text"   value="' + resaleNumber + '"/></div>' +
        '           <div class="discountRowTaxBox resellSmall">Resale #</div>' +
        '       </div>' ;

    if ((siteDefaults.preTax == '1' || siteDefaults.preTax == '2' ) && totals.tax.amount < 1) {
        usTax = '';
    }

    var html = ' ' +
        usTax +
        ' <div class="saDiscountBox">' +
        '           <div class="discountRowTaxBoxDelete"></div>' +
        '           <div class="discountRowTaxBox"><input tabindex="395" onkeyup="updateTaxBox()" id="shippingBox" class="fixedAmount" type="text"   value="' + totals.shipping.amount + '"/></div>' +
        '           <div class="discountRowTaxBox">Shipping</div>' +
        '       </div>' +
        ' <div class="saDiscountBox">' +
        '           <div class="discountRowTaxBoxDelete"></div>' +
        '           <div class="discountRowTaxBox"><input tabindex="402" onkeyup="updateTaxBox()" id="installationBox" class="fixedAmount" type="text"   value="' + totals.installation.amount + '"/></div>' +
        '           <div class="discountRowTaxBox">Installation</div>' +
        '           <div class="discountRowTaxBox"><input tabindex="400" onkeyup="updateTaxBox()" id="installationType" class="fixedAmount" type="text"   value="' + totals.installation.type + '"/></div>' +
        '       </div>' +
        ' <div class="saDiscountBox">' +
        '           <div class="discountRowTaxBoxDelete"></div>' +
        '           <div class="discountRowTaxBox"><input tabindex="404" onkeyup="updateTaxBox()" id="extraAmounInput" class="fixedAmount" type="text"   value="' + totals.extra.amount + '"/></div>' +
        '           <div class="discountRowTaxBox"><input tabindex="403" onkeyup="updateExtraSA()" id="extraTextInput" class="discountName" type="text"  maxlength="16" value="' + totals.extra.name + '"/></div>' +
        '       </div>';

    if(siteDefaults.salesFlow !== 2){
        html += ' ' +
        additionalExtrasNamesHTML(totals) +
        '<div class="discountButtonBox">' +
        '   <div id="addExtrasButton" class="addExtrasButton" onclick="addSalesAdminExtra()"> Add Extra </div>' +
        '</div>';
    }

    return html;
}


function buildDiscountBox(data) {

    var html = buildDiscountBoxRowHeader();

    for (var a = 0; a < data.length; a++) {
        data[a].id = a;

        var row = buildDiscountBoxRow(data[a]);
        html = html + row;
    }

    html = html + buildDiscountButton();
    return html;

}
function buildDiscountButton() {

    var row = ' <div class="discountButtonBox">' +
        '      <div id="addNewFieldButton" class="discountButton" onclick="addNewField()"> Add New Field </div>' +
        '   </div>';

    return row;

}

function addNewFieldStatus(data) {

    var count = data.Discounts.length;

    if (count > 2) {
        $('#addNewFieldButton').hide();
    } else {
        $('#addNewFieldButton').show();
    }

}

function deleteDiscount(id) {

    if(isSalesAdminLimited()) return;

    discounts.Discounts.splice(id, 1);
    //delete data.Discounts[id]
    writeDiscount(discounts);
    launchSalesAdmin();
}


function additionalExtraDelete(id) {

    discounts.Totals.additionalExtras.splice(id, 1);
    //delete data.Discounts[id]
    writeDiscount(discounts);
    launchSalesAdmin();
}



function blankDiscount(id) {

    id = typeof id !== 'undefined' ? id : 1;
    var blankRow = {name: "", amount: 0, type: "percent", id: id, showOnQuote: true};
    return blankRow;
}

function addNewField() {

    if(isSalesAdminLimited()) return;

    var count = discounts.Discounts.length;
    var blankRow = blankDiscount(count + 1);
    if (count < 3) {
        discounts.Discounts.push(blankRow);

        writeDiscount(discounts);
        launchSalesAdmin();
    }
    //addNewFieldStatus(data);

}

function addSalesAdminExtra() {

    if (empty(discounts.Totals.additionalExtras)) {
        discounts.Totals.additionalExtras = [];
    }
    var addExtra = {"name": "", "amount": "", "showOnQuote": false}
    discounts.Totals.additionalExtras.push(addExtra);

    launchSalesAdmin();

    // var count = discounts.Discounts.length;
    // var blankRow = blankDiscount(count + 1);
    // if (count < 3) {
    //     discounts.Discounts.push(blankRow);
    //
    //     writeDiscount(discounts);
    //     chooseWhichToLauncher();
    // }
    // //addNewFieldStatus(data);

}

function buildDiscountBoxRowHeader(rowData) {

    var row = '  <div class="leftHeaderSpacer"></div>' +
        '        <div class="DiscountBoxHeader">' +
        '           <div class="discountRowBoxName">Discount Name</div>' +
        '           <div class="discountRowBoxAmount">Amount</div>' +
        '           <div class="discountRowBoxType">Select Type</div>' +
        '       </div>';

    return row;
}

function sanitizeString(str) {

    if (empty(str)) {
        return '';
    }
    str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim, "");
    return str.trim();
}

function handleBlankText(val) {
    if (!empty(val)) {
        if (val.length < 1) {
            val = '&nbsp;';
        }
    } else {
        val = '&nbsp;';
    }

    return val;
}

function returnBlankIfUndefined(val) {
    if (empty(val)) {
        val = '';
    }
    return val;
}

function updateNameSA(row) {
    var val = $('#nameDiscount-' + row).val();
    val = sanitizeString(val);
    val = handleBlankText(val);

    $('#discountNameText-' + row).html(val);

    discounts.Discounts[row].name = val;
    writeDiscount(discounts);
}

function updateAdditionalExtra(row) {
    var val = $('#additionalExtraTextInput-' + row).val();
    val = sanitizeString(val);
    val = handleBlankText(val);

    $('#additionalExtraName-' + row).html(val);

    if (empty(discounts.Totals.additionalExtras)) {
        discounts.Totals.additionalExtras=[];
    }

    discounts.Totals.additionalExtras[row].name = val;
    writeDiscount(discounts);
}

function updateAdditionalExtraAmount(row) {
    var amount = $('#additionalExtraAmounInput-' + row).val();

    amount = validNumber(amount, '#additionalExtraAmounInput-' + row);

    if (empty(discounts.Totals.additionalExtras)) {
        discounts.Totals.additionalExtras=[];
    }

    discounts.Totals.additionalExtras[row].amount = amount;

    updateTaxBox();

    writeDiscount(discounts);
}


function updateAmountOrRadioSA(row) {
    var val = $('#discountAmount-' + row).val();
    var type = $("input[type='radio'][name='type-Discount" + row + "']:checked").val();

    var cost = empty(discounts.Totals.cost.amount) ? 0 : discounts.Totals.cost.amount;

    val = validNumber(val, '#discountAmount-' + row);
    type = sanitizeString(type);
    cost = parseFloat(cost);

    var amount = calculateDiscountAmountStraight(val, type, cost);
    $('#discountAmountText-' + row).html(currency + formatMoney(amount));

    discounts.Discounts[row].type = type;
    discounts.Discounts[row].amount = val;

    updateFormPrices();

    writeDiscount(discounts);
}


function updateFormPrices() {


    var cost = discounts.Totals.cost.amount;
    // /+ (discounts.Totals.cost.amount * parseFloat(discounts.Totals.taxSpecial)/ 100);
    // discounts.Totals.cost.amount = cost;

    var subTotal = getSubTotalForSalesAdmin(discounts);

    var discountTotal = getDiscountTotals(discounts);
    var taxAmount = getTaxAmount(discounts);
    var totalCost = getTotalForSalesAdmin(discounts);


    cost = parseFloat(cost);
    subTotal = parseFloat(subTotal);
    discountTotal = parseFloat(discountTotal);
    taxAmount = parseFloat(taxAmount);
    totalCost = parseFloat(totalCost);

    $('#cost').html(currency + formatMoney(cost));
    $('#subTotal').html(currency + formatMoney(subTotal));
    $('#discountTotal').html(currency + formatMoney(discountTotal));
    $('#salesTax').html(currency + formatMoney(taxAmount));
    $('#totalCost').html(currency + formatMoney(totalCost));
}

function updateAdditionalTotals() {

}

function updateTaxBox() {

    // var taxSpecial = $('#taxSpecial').val();
    var tax = $('#tax').val();
    var resale = $('#resale').val();
    var shipping = $('#shippingBox').val();
    var installation = $('#installationBox').val();
    var extra = $('#extraAmounInput').val();
    var screens = $('#screensBox').val();
    var installType = $('#installationType').val();

    if (!empty(resale)) {
        resale = JSON.stringify(resale).replace(/\W/g, '');
    }

    if (!empty(resale)) {
        tax = 0;
    }
    tax = validNumber(tax, '#tax');

    shipping = validNumber(shipping, '#shippingBox');
    installation = validNumber(installation, '#installationBox');
    extra = validNumber(extra, '#extraAmounInput');
    screens = validNumber(screens, '#screensBox');

    // discounts.Totals.taxSpecial = taxSpecial;
    discounts.Totals.tax.amount = tax;
    discounts.Totals.tax.resale = resale;
    discounts.Totals.shipping.amount = shipping;
    discounts.Totals.installation.amount = installation;
    discounts.Totals.extra.amount = extra;
    discounts.Totals.screens.amount = screens;
    discounts.Totals.installation.type = installType;


    $('#installationTypeText').html(installType + ' Install');
    $('#installationText').html(currency + formatMoney(installation));
    $('#shippingText').html(currency + formatMoney(shipping));
    $('#extraAmount').html(currency + formatMoney(extra));

    $('#screensAmount').html(currency + formatMoney(screens));
    updateAdditionalExtrasBox();

    updateFormPrices();
    writeDiscount(discounts);
}

function updateAdditionalExtrasBox() {

    if (empty(discounts.Totals.additionalExtras)) {
        return;
    }

    for (var a = 0; a < discounts.Totals.additionalExtras.length; a++) {
        $('#additionalExtraAmount-'+a).html(currency + formatMoney(discounts.Totals.additionalExtras[a].amount));
    }

}

function updateCodeSApreTax() {

    var code = $('#codeSApreTax').val();
    var cost = parseFloat((discounts.Totals.preCost * Number(code) / 100)) + parseFloat(discounts.Totals.preCost);

    // var cost = calculateCostFromCodeSA(code);
    discounts.Totals.cost.amount = cost;
    discounts.Totals.code = 'YZ-'+code;

    updateFormPrices();

    writeDiscount(discounts);
}


function updateCodeSA() {

    //Robert request this removed 6/27/2018
    return;

    var code = $('#codeSA').val();

    var cost = calculateCostFromCodeSA(code);
    discounts.Totals.cost.amount = cost;
    discounts.Totals.code = code;

    updateFormPrices();

    writeDiscount(discounts);
}
function calculateCostFromDiscount() {

    if (empty(discounts.Totals)) {
        return '';
    }

    var cost = calculateCostFromCodeSA(discounts.Totals.code);

    return cost;

}
function calculateMarkUp() {

    code = sanitizeString(discounts.Totals.code);
    return calculateMarkUpWithCode(code);

}

function calculateMarkUpWithCode(code) {

    if (empty(code)) {
        return 0;
    }
    var rates = code.split("-");
    var markup = 0;
    if (!empty(rates[1])) {
        markup = validNumber(rates[1]);
        $('#codeSA').val(rates[0] + '-' + markup);
    }
    return markup;

}
function calculateCostFromCodeSA(code) {

    var markup = calculateMarkUpWithCode(code);

    var cost = parseFloat((discounts.Totals.preCost * Number(markup) / 100)) + parseFloat(discounts.Totals.preCost);

    return cost;

}

function buildDiscountBoxRow(rowData) {


    var percentage = 'checked';
    var flat = '';
    if (rowData.type == 'flat') {
        percentage = '';
        flat = 'checked';
    }

    var row = ' ' +
        '<div class="saDiscountBox">' +
        '   <div class="discountRowBox">' +
        '       <input tabindex="' + rowData.id + '1" onkeyup="updateNameSA(' + rowData.id + ')" id="nameDiscount-' + rowData.id + '" class="discountName" type="text"  maxlength="16" value="' + rowData.name + '"  /></div>' +
        '           <div class="discountRowBox"><input tabindex="' + rowData.id + '2" onkeyup="updateAmountOrRadioSA(' + rowData.id + ')" id="discountAmount-' + rowData.id + '" class="discountAmount" type="text"   value="' + rowData.amount + '"/></div>' +
        '           <div class="discountRowBox">' +
        '               <input class="salesAdminRadio" tabindex="' + rowData.id + '3" onchange="updateAmountOrRadioSA(' + rowData.id + ')"  name="type-Discount' + rowData.id + '" type="radio" value="percentage" ' + percentage + ' >' +
        '               <div class="discountTypeText">%</div>' +
        '           </div>' +
        '           <div class="discountRowBox">' +
        '               <input class="salesAdminRadio" tabindex="' + rowData.id + '4" onchange="updateAmountOrRadioSA(' + rowData.id + ')" name="type-Discount' + rowData.id + '" type="radio" value="flat" ' + flat + ' >' +
        '               <div class="discountTypeText">' + currency + '</div>' +
        '           </div>' +
        '           <div class="discountRowBoxDelete" onclick="deleteDiscount(' + rowData.id + ')">Delete</div>' +
        '       </div>';


    return row;
}


//function salesAdminCheckBox() {
//
//    var html = '';
//    var checkBoxes = new Array("Base Price", "Discount %", "Discount Amount", "Discounted Price", "Total Price", "Tax", "Shipping", "Installation");
//
//    for (var a = 0; a < checkBoxes.length; a++) {
//        var option  = checkBoxes[a];
//        html = html + '<div class="salesAdminCheckBox"><input class="SACB" type="checkbox" name="'+ option +'" value="yes" >' + option + '</div>' ;
//    }
//    return html;
//
//}


function calculateDiscountAmount(data, totals) {

    var amount = calculateDiscountAmountStraight(data.amount, data.type, totals.cost.amount);
    return amount;

}

function calculateDiscountAmountStraight(amount, type, cost) {


    var newAmount = empty(amount) ? 0 : amount;

    newAmount = parseFloat(newAmount);
    if (type == "percentage") {
        newAmount = parseFloat(cost) * newAmount / 100;
    }
    return newAmount;

}

function buildDiscountBoxTotals(data) {

    var html = buildDiscountBoxRowHeaderTotal(data.Totals);

    var discountTotal = 0;

    for (var a = 0; a < data.Discounts.length; a++) {
        //data.Discounts[a].id = a;
        var row = buildDiscountBoxRowTotal(data.Discounts[a], data.Totals);
        discountTotal = parseFloat(discountTotal) + calculateDiscountAmount(data.Discounts[a], data.Totals);

        html = html + row;
    }

    html = html + buildDiscountBoxRowAmountTotals(data);
    html = html + buildDiscountBoxRowSubTotals(data);

    return html;
}

function buildDiscountBoxRowHeaderTotal(totals) {


    var yesOnQuote = isCheckedYes(totals.cost);
    var noOnQuote = isCheckedNo(totals.cost);

    var cost = totals.cost.preCost;


    var row = ' ' +
        '<div class="DiscountBoxHeader">' +
        '   <div class="discountRowBoxNameRight">Item Total:</div>' +
        '   <div id="cost" class="discountRowBoxAmountRight">' + currency + formatMoney(totals.cost.amount) + '</div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="200"   onclick="updateRadioButtonTotal(\'cost\',true)"  name="type-Cost" type="radio" value="Yes" ' + yesOnQuote + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '   </div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="200"   onclick="updateRadioButtonTotal(\'cost\',false)" name="type-Cost" type="radio" value="No" ' + noOnQuote + ' >' +
        '       <div class="discountTypeText">No</div>' +
        '   </div>' +
        '   <div class="totalsLine"></div>' +
        '</div>';

    return row;
}

function isCheckedYes(value) {

    if (typeof value == 'undefined') {
        return '';
    }
    if (value.showOnQuote == true) {
        return 'checked';
    } else {
        return '';
    }

}
function isCheckedNo(value) {

    if (typeof value == 'undefined') {
        return '';
    }
    if (value.showOnQuote == false) {
        return 'checked';
    } else {
        return '';
    }

}
function buildDiscountBoxRowTotal(rowData, totals) {

    var amount = calculateDiscountAmount(rowData, totals);
    var yesOnQuote = isCheckedYes(rowData);
    var noOnQuote = isCheckedNo(rowData);
    var name = handleBlankText(rowData.name);

    var row = ' ' +
        '<div class="discountBoxTotals">' +
        '   <div id="discountNameText-' + rowData.id + '" class="discountRowBoxNameRight">' + name + '</div>' +
        '   <div id="discountAmountText-' + rowData.id + '" class="discountRowBoxAmountRightRow">' + currency + formatMoney(amount) + '</div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="20' + rowData.id + '" onclick="updateRadioButtonWithRow(' + rowData.id + ',true)" name="type-' + rowData.id + '" type="radio" value="Yes"  ' + yesOnQuote + ' >' +
        '       <div class="discountTypeText">Yes</div>' +
        '   </div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="20' + rowData.id + '" onclick="updateRadioButtonWithRow(' + rowData.id + ',false)" name="type-' + rowData.id + '" type="radio" value="No" ' + noOnQuote + '>' +
        '       <div class="discountTypeText">No</div>' +
        '   </div>' +
        '</div>';

    return row;
}

function buildDiscountBoxRowAmountTotals(data) {

    var totals = data.Totals;
    var discountTotal = getDiscountTotals(data);
    var yesOnQuote = isCheckedYes(totals.discountTotal);
    var noOnQuote = isCheckedNo(totals.discountTotal);

    var row = ' ' +
        '<div class="discountBoxTotals">' +
        '   <div class="discountRowBoxNameRightRed">Discount Total:</div>' +
        '   <div id="discountTotal" class="discountRowBoxAmountRightRow">' + currency + formatMoney(discountTotal) + '</div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="210" onclick="updateRadioButtonTotal(\'discountTotal\',true)" name="type-DiscountTotal" type="radio" value="Yes" ' + yesOnQuote + ' >' +
        '       <div class="discountTypeText">Yes</div>' +
        '   </div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="210"  onclick="updateRadioButtonTotal(\'discountTotal\',false)" name="type-DiscountTotal" type="radio" value="No" ' + noOnQuote + ' >' +
        '       <div class="discountTypeText">No</div>' +
        '   </div>' +
        '</div>';

    return row;
}

function getDiscountTotals(data) {

    if (empty(data.Discounts)) {
        return 0;
    }

    var discountTotal = 0;
    for (var a = 0; a < data.Discounts.length; a++) {
        discountTotal = discountTotal + calculateDiscountAmount(data.Discounts[a], data.Totals);
    }
    return discountTotal;

}


function buildDiscountBoxRowSubTotals(data) {

    var totals = data.Totals

    var yesOnQuote = isCheckedYes(totals.subTotal)
    var noOnQuote = isCheckedNo(totals.subTotal)

    var subTotal = getSubTotalForSalesAdmin(data);

    var row = ' ' +
        '<div class="discountBoxTotals">' +
        '   <div class="discountRowBoxNameRight">Sub-Total:</div>' +
        '   <div id="subTotal" class="discountRowBoxAmountRight">' + currency + formatMoney(subTotal) + '</div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="211" onclick="updateRadioButtonTotal(\'subTotal\',true)" name="type-SubTotals" type="radio" value="Yes" ' + yesOnQuote + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '   </div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="211" onclick="updateRadioButtonTotal(\'subTotal\',false)" name="type-SubTotals" type="radio" value="No" ' + noOnQuote + '>' +
        '       <div class="discountTypeText">No</div>' +
        '   </div>' +
        '</div>';

    return row;
}

function getSubTotalForSalesAdmin(data) {

    if (isEmptyObject(data.Totals)) {
        return 0;
    }
    if (isEmptyObject(data.Totals.cost)) {
        return 0;
    }
    if (data.Totals.cost.amount == 'undefined') {
        return 0;
    }

    data.Totals.cost.amount = calculateCostFromCodeSA(data.Totals.code);

    var discountTotal = getDiscountTotals(data);

    var subTotal = parseFloat(data.Totals.cost.amount) - discountTotal;

    return subTotal;

}

function validTotalsAmount(data) {

    if (empty(data)) {
        return 0;
    }

    if (empty(data.amount)) {
        return 0;
    }

    return data.amount;

}

function getTotalForSalesAdmin(data) {

    var subTotal = getSubTotalForSalesAdmin(data);

    if (isGuest()) {
        subTotal = data.Totals.preCost;
    }
    var tax = getTaxAmount(data);
    var shipping = validTotalsAmount(data.Totals.shipping);
    var installation = validTotalsAmount(data.Totals.installation);
    var extra = validTotalsAmount(data.Totals.extra);
    var additionExtras = getAdditionalExtras(data.Totals);
    var screens = validTotalsAmount(data.Totals.screens);
    var total = parseFloat(additionExtras) + parseFloat(subTotal) + parseFloat(screens) + parseFloat(tax) + parseFloat(shipping) + parseFloat(installation) + parseFloat(extra);

    return total;

}
function getAdditionalExtras(totals) {

    if(empty(totals.additionalExtras)) {
        return 0;
    }

    var total = 0;
    for (var a = 0; a < totals.additionalExtras.length; a++) {

        var cost = 0;
        if (!empty(totals.additionalExtras[a].amount) ) {
            cost = totals.additionalExtras[a].amount;
        }

        total = total + parseFloat(cost);
    }

    return total;
}

function getTaxAmount(data) {

    if (empty(data.Totals.tax)) {
        return 0;
    }

    if (empty(data.Totals.screens)) {
        data.Totals.screens = {amount: 0};
    }
    if (empty(data.Totals.shipping)) {
        data.Totals.shipping = {amount: 0};
    }
    if (empty(data.Totals.extra)) {
        data.Totals.extra = {amount: 0};
    }
    if (empty(data.Totals.installation)) {
        data.Totals.installation = {amount: 0};
    }



    var percentage = empty(data.Totals.tax.amount) ? 0 : data.Totals.tax.amount;
    var subTotal = getSubTotalForSalesAdmin(data);
    var screens = empty(data.Totals.screens.amount) ? 0 : data.Totals.screens.amount;

    var shipping = empty(data.Totals.shipping.amount) ? 0 : data.Totals.shipping.amount;
    var extra = empty(data.Totals.extra.amount) ? 0 : data.Totals.extra.amount;


    var additionalExtras = getAdditionalExtras(data.Totals);
    var installation = empty(data.Totals.installation.amount) ? 0 : data.Totals.installation.amount;

    var amount = ((parseFloat(subTotal) + parseFloat(screens) ) * percentage) / 100;
    if (isUK()) {
        amount = ((parseFloat(subTotal) + parseFloat(screens) + parseFloat(shipping) + parseFloat(extra) +  parseFloat(additionalExtras)+ parseFloat(installation)) * percentage) / 100;
    }

    if (amount < 0) {
        amount = 0;
    }
    return amount;
}


function updateRadioButtonWithRowForExtras(row, status) {

    discounts.Totals.additionalExtras[row].showOnQuote = status;
    writeDiscount(discounts);
}


function updateRadioButtonWithRow(row, status) {

    discounts.Discounts[row].showOnQuote = status;
    writeDiscount(discounts);
}


function updateRadioButtonTotal(key, status) {

    discounts.Totals[key].showOnQuote = status;

}


function buildOtherBoxTotals(data) {

    var totals = data.Totals;

    var yesTax = isCheckedYes(totals.tax);
    var noTax = isCheckedNo(totals.tax);

    var yesShipping = isCheckedYes(totals.shipping);
    var noShipping = isCheckedNo(totals.shipping);

    var yesInstallation = isCheckedYes(totals.installation);
    var noInstallation = isCheckedNo(totals.installation);

    var total = getTotalForSalesAdmin(data);
    var yesTotal = isCheckedYes(totals.finalTotal);
    var noTotal = isCheckedNo(totals.finalTotal);

    var taxAmount = getTaxAmount(data);


    var installType = ''
    if (!empty(totals.installation)) {
        installType = returnBlankIfUndefined(totals.installation.type);
    }

    var shipping = '<div class="discountRowBoxNameRight">Shipping:</div>';
    var taxes = "Sales Tax:";
    if (isUK()) {
        shipping = '<div class="discountRowBoxNameRight">Delivery:</div>';
        taxes = "VAT";
    }

    var shippingAmount = '';
    if (!empty(totals.shipping)) {
        shippingAmount = totals.shipping.amount;
    }

    var taxHTML = '' +
        '<div class="discountBoxTotals">' +
        '    <div  class="discountRowBoxNameRight">' + taxes + '</div>' +
        '    <div id="salesTax" class="discountRowBoxAmountRight">' + currency + formatMoney(taxAmount) + '</div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="500" onclick="updateRadioButtonTotal(\'tax\',true)" name="type-DiscountTotals" type="radio" value="percentage" ' + yesTax + ' >' +
        '       <div class="discountTypeText">Yes</div>' +
        '    </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="501" onclick="updateRadioButtonTotal(\'tax\',false)" name="type-DiscountTotals" type="radio" value="flat" ' + noTax + ' >' +
        '       <div class="discountTypeText">No</div>' +
        '    </div>' +
        '</div>' ;


    var ukTaxHTML = '';
    var usTaxHTML = taxHTML;


    //PreTax Thing
    if ((siteDefaults.preTax == 1 || siteDefaults.preTax == 2) && taxAmount < 1) {
        usTaxHTML = '' +
            '<div class="discountBoxTotals hidden">' +
            '    <div  class="discountRowBoxNameRight">' + taxes + '</div>' +
            '    <div id="salesTax" class="discountRowBoxAmountRight">' + currency + formatMoney(taxAmount) + '</div>' +
            '    <div class="discountRowBox">' +
            '       <input class="salesAdminRadio" tabindex="500" onclick="updateRadioButtonTotal(\'tax\',false)" name="type-DiscountTotals" type="radio" value="percentage" ' + yesTax + ' >' +
            '       <div class="discountTypeText">Yes</div>' +
            '    </div>' +
            '    <div class="discountRowBox">' +
            '       <input class="salesAdminRadio" tabindex="501" onclick="updateRadioButtonTotal(\'tax\',true)" name="type-DiscountTotals" type="radio" value="flat" checked >' +
            '       <div class="discountTypeText">No</div>' +
            '    </div>' +
            '</div>' ;
    }




    if (isUK()) {
        ukTaxHTML = taxHTML;
        usTaxHTML = '';
    }




    var row = ' ' +
        usTaxHTML +
        '<div class="discountBoxTotals">' +
        '    ' + shipping +
        '    <div id="shippingText" class="discountRowBoxAmountRight">' + currency + formatMoney(shippingAmount) + '</div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="502" onclick="updateRadioButtonTotal(\'shipping\',true)" name="type-Shipping" type="radio" value="percentage" ' + yesShipping + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '    </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="503" onclick="updateRadioButtonTotal(\'shipping\',false)" name="type-Shipping" type="radio" value="flat" ' + noShipping + ' >' +
        '       <div class="discountTypeText">No</div>' +
        '    </div>' +
        '</div>' +
        '<div class="discountBoxTotals">' +
        '    <div id="installationTypeText" class="discountRowBoxNameRight">' + installType + ' Install:</div>' +
        '    <div id="installationText" class="discountRowBoxAmountRight">' + currency + formatMoney(totals.installation.amount) + ' </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="504" onclick="updateRadioButtonTotal(\'installation\',true)" name="type-Installation" type="radio" value="percentage" ' + yesInstallation + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '    </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="505" onclick="updateRadioButtonTotal(\'installation\',false)" name="type-Installation" type="radio" value="flat" ' + noInstallation + '>' +
        '       <div class="discountTypeText">No</div>' +
        '    </div>' +
        '</div>' +
        extrasHTML(totals)+
        additionalExtrasHTML(totals) +
        ukTaxHTML +
        '<div class="discountBoxTotals">' +
        '    <div class="discountRowBoxNameRight">Total Cost:</div>' +
        '    <div id="totalCost" class="discountRowBoxAmountRight">' + currency + formatMoney(total) + ' </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="508" onclick="updateRadioButtonTotal(\'finalTotal\',true)" name="type-finalTotal" type="radio" value="percentage" ' + yesTotal + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '    </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="509" onclick="updateRadioButtonTotal(\'finalTotal\',false)" name="type-finalTotal" type="radio" value="flat" ' + noTotal + '>' +
        '       <div class="discountTypeText">No</div>' +
        '    </div>' +
        '</div>';


    quote.total = total;

    return row;
}


function additionalExtrasHTML(totals) {

    if (empty(totals.additionalExtras)) {
        return '';
    }

    var html = '';

    for (var a = 0; a < totals.additionalExtras.length; a++) {

        var rowHTML = additionalExtrasRowHTML(totals.additionalExtras[a],a);
        html = html + rowHTML;
    }

    return html;
}

function additionalExtrasRowHTML(additionalExtra, count) {

    var yesExtra = isCheckedYes(additionalExtra);
    var noExtra = isCheckedNo(additionalExtra);

    var extraName = '';
    if (!empty(additionalExtra)) {
        extraName = handleBlankText(additionalExtra.name);
    }

    var html = ''+
        '<div class="discountBoxTotals">' +
        '    <div id="additionalExtraName-'+count+'" class="discountRowBoxNameRight">' + extraName + '</div>' +
        '    <div id="additionalExtraAmount-'+count+'" class="discountRowBoxAmountRight">' + currency + formatMoney(additionalExtra.amount) + ' </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminExtrasRadio" tabindex="506"  onclick="updateRadioButtonWithRowForExtras('+count+',true)" name="type-extra-'+count+'" type="radio" value="percentage" ' + yesExtra + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '    </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminExtrasRadio" tabindex="507"  onclick="updateRadioButtonWithRowForExtras('+count+',false)" name="type-extra-'+count+'" type="radio" value="flat" ' + noExtra + '>' +
        '       <div class="discountTypeText">No</div>' +
        '    </div>' +
        '</div>' ;

    return html;

}


function extrasHTML(totals) {

    var yesExtra = isCheckedYes(totals.extra);
    var noExtra = isCheckedNo(totals.extra);

    var extraName = '';
    if (!empty(totals.extra)) {
        extraName = handleBlankText(totals.extra.name);
    }

    var html = ''+
        '<div class="discountBoxTotals">' +
        '    <div id="extraName" class="discountRowBoxNameRight">' + extraName + '</div>' +
        '    <div id="extraAmount" class="discountRowBoxAmountRight">' + currency + formatMoney(totals.extra.amount) + ' </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="506"  onclick="updateRadioButtonTotal(\'extra\',true)" name="type-extra" type="radio" value="percentage" ' + yesExtra + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '    </div>' +
        '    <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="507"  onclick="updateRadioButtonTotal(\'extra\',false)" name="type-extra" type="radio" value="flat" ' + noExtra + '>' +
        '       <div class="discountTypeText">No</div>' +
        '    </div>' +
        '</div>' ;

    return html;
}


function otherTotalsUS(data) {


}


function otherTotalsUK(data) {


}


function buildScreensTotals(data) {

    var totals = data.Totals;

    var yesScreens = isCheckedYes(totals.screens);
    var noScreens = isCheckedNo(totals.screens);

    var row = '' +
        '<div class="discountBoxTotals">' +
        '   <div class="discountRowBoxNameRight">Screens:</div>' +
        '   <div id="screensAmount" class="discountRowBoxAmountRight">' + currency + formatMoney(totals.screens.amount) + '</div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="392" onclick="updateRadioButtonTotal(\'screens\',true)" name="type-Screens" type="radio" value="percentage" ' + yesScreens + '>' +
        '       <div class="discountTypeText">Yes</div>' +
        '   </div>' +
        '   <div class="discountRowBox">' +
        '       <input class="salesAdminRadio" tabindex="393" onclick="updateRadioButtonTotal(\'screens\',false)" name="type-Screens" type="radio" value="flat" ' + noScreens + ' >' +
        '       <div class="discountTypeText">No</div>' +
        '   </div>' +
        '</div>';

    return row;
}
