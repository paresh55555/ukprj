/*jshint strict:false */
/*jslint browser: true*/
/*global $, jQuery, alert*/
'use strict';

function loadSizeAndPanels() {

    startSpinner();
    updateFormStatus();
    validFormStatus('sizeAndPanels');

    $('#footerPrice').show();
    attachStartOverButtonWithId('startOverButton');
    $("#headerImage").attr("src", "images/header.png");

    $.when(
        getExtendedButtons("frame"), getExtendedButtons("track")
    ).done(function (frameOptions, trackOptions) {

        if (empty(formStatus.panelOptions)) {
            trackAndDoClick('home');
            return;
        }

        if (!empty(formStatus.panelOptions['frame'])) {
            formStatus.panelOptions['frame'].buttons = frameOptions[0];
        }

        // Check for door style and set the flag
        if(trackOptions && trackOptions.length && hasDoorStyles(trackOptions[0])){
            formStatus['doorStyle'] = hasDoorStyles(trackOptions[0]);
            formStatus.numberOfPanels = 1;
        }
        if (!empty(formStatus.panelOptions['track'])) {
            formStatus.panelOptions['track'].buttons = trackOptions[0];
        }
        var html = customSizeHTML();
        buildAndAttachCorePage(html);

        selectTab('#size');
        $('#cartNav').show();

        $('.extendedHalfButton').unbind("click").bind("click", halfButtonClick);
        $('.button').unbind("click").bind("click", selectButton);

        var widthSelector = $("#width");
        var heightSelector = $("#height");

        widthSelector.change(getPanels);
        widthSelector.change(checkForSill);

        widthSelector.keyup(newWidth);
        heightSelector.keyup(saveHeightToCookie);
        heightSelector.change(validateHeight);
        heightSelector.change(checkForTopExtender);

        validateWidth(false);
        validateHeight(false);

        makeNextSectionButton(0);
        showContentAndSaveState();

        scrollTopOfPage();
    });
}


function newWidth() {
    formStatus.newWidth = 'yes';
}
function customSizeHTML() {

    var dimensions = customDimensionsHTML();
    var customPanels = customPanelOptionsHMTL();

    var html = '' +
        '<div id="dynamicContent"> ' +
        '   ' + dimensions +
        '   <div id="options">' +
        '       ' + customPanels +
        '   </div>' +
        '   <div id="copyWriteFooter"></div> ' +
        '</div>'

    return html;
}


function markSelectedChoices() {

    placeSelectedImageExtendedHalf(formStatus.chooseFrameSection);
    placeSelectedImageExtendedHalf(formStatus.chooseTrackSection);
    placeSelectedImage(formStatus.choosePanels);
    placeSelectedImage(formStatus.chooseSwings);
    placeSelectedImage(formStatus.panelMovement);
    placeSelectedImageExtendedHalf(formStatus.swingDirectionSection);
}


function placeSelectedImage(id) {

    if (id) {
        var selector = $(buildIDSelector(id));
        displaySelected(selector);
    }
}


function decideVisibiltyOfForm() {

    hideAllSizePanelOptions();

    if (isSalesPerson()) {
        for (var property in moduleInfo.options) {

            var checkName = moduleInfo.options[property].formStatusName;

            if (!empty(formStatus[checkName]) || empty(checkName)) {
                $("#" + moduleInfo.options[property].nameOfOption).show();

                var nextOption = findOptions(checkName);
                if (!empty(nextOption)) {
                    $("#" + nextOption).show();
                }
            }
        }
    } else {
        if (formStatus.chooseSwings) {

            $('#nextColorFinish').show();
        }
    }

    validateHeight();
}


function hideAllSizePanelOptions() {

    $("#chooseFrame").hide();
    $("#chooseTrack").hide();
    $("#choosePanels").hide();
    $("#chooseSwings").hide();
    $("#panelMovement").hide();
    $("#swingDirection").hide();
    $("#nextColorFinish").hide();
}


function shutDownForm() {

    //$('.topNavSection').unbind("click");
    $('#sizePanelsOptions').hide();
    //hideAllSizePanelOptions();

}
function reStoreForm() {

    //$('.topNavSection').unbind("click").bind("click", tabClick);
    $('#sizePanelsOptions').show();

    //loadChoicesAlreadyMade();
}


function showChoice(id) {

    if (id) {
        var selector = $(buildIDSelector(id));
        selector.parent().parent().show();
        if (isSalesPerson()) {
            selector.parent().parent().next().show();
        }
    }
}


function placeSelectedImageExtendedHalf(id) {

    if (id) {
        var idSelector = buildIDSelector(id);
        var selector = $('.extendedHalfButtonImage', idSelector);
        displayExtendedSelected(selector);
    }
}


function showChoosePanels() {

    if (isWidthValid(true) && isHeightValid(true)) {
        $("#choosePanels").show();
    }


}

function validWidth2(width) {

//    var width = $("#width").val();

    var error = true;
    if (width > 100 || width < 60) {

        return false;

    } else {
        return true;
    }


}
function isBlank(value) {

    if (value == '') {
        return true;
    }
    return false;
}

function isValidSizes() {

    if (isWidthValid(true) && isHeightValid(true)) {
        return true;
    }

    return false
}

function isWidthValid(strict) {
    
    var width = Number(formStatus.width);

    if (!formStatus.module) {
        return false;
    }

    if (strict) {
        if (width > moduleInfo.maxWidth || width < moduleInfo.minWidth || isNaN(width)) {
            return false;
        }
    } else {
        if (width > moduleInfo.maxWidth || width < moduleInfo.minWidth || isNaN(width)) {
            if (formStatus.width != '') {
                return false;
            }
        }
    }
    return true;
}


function displaySizeFormIfValid() {

    if (isValidSizes()) {
        $('#options').fadeIn(300);
    } else {
        $('#options').fadeOut(300);
    }

}

function validateWidth(width) {

    formStatus.width = numbersOnly(formStatus.width);
    $('#width').val(formStatus.width);

    if (isWidthValid()) {
        setSillMinMaxWidth(width);
        var css = {"border-color": siteDefaults.cssBorder};
        $('#widthError').html('');
        $('#widthInch').css(css);
        $('#width').css(css);

        deleteErrorValue('width');
    } else {
        var css = {"border-color": "darkred"};

        if (typeof formStatus.module != 'undefined') {
            $('#widthError').html(moduleInfo.messageWidth);
        }
        formStatus.errors.width = 'You have an invalid width';
        $('#widthInch').css(css);
        $('#width').css(css);
    }

    displaySizeFormIfValid();
    showNextOptionAfterSize();
}


function getFirstOptionToShow() {

    for (var property in formStatus.panelOptions) {

        var show = formStatus.panelOptions[property].firstToShow;
        var showGuest = formStatus.panelOptions[property].firstToShowForGuest;
        var nameOfOption = formStatus.panelOptions[property].nameOfOption;

        if (isGuest()) {
            if (showGuest == 1) {
                return nameOfOption;
            }

        } else {
            if (show == 1) {
                return nameOfOption;
            }
        }
    }
}

function deleteErrorValue(value) {

    if (!empty(formStatus)) {
        if (!empty(formStatus.errors)) {
            if (!empty(formStatus.errors[value])) {
                delete formStatus.errors[value];
            }
        }
    }
}

function isHeightValid(strict) {

    var height = Number(formStatus.height);

    deleteErrorValue('height');

    if (!formStatus.module) {
        return false;
    }

    if (strict) {
        if (height > moduleInfo.maxHeight || height < moduleInfo.minHeight || isNaN(height)) {
            if (empty(formStatus.errors)) {
                formStatus.errors = {};
            }
            formStatus.errors.height = "You have an invalid height";
            return false;
        }
    } else {
        if ((height > moduleInfo.maxHeight || height < moduleInfo.minHeight || isNaN(height) ) && formStatus.height != '') {
            if (empty(formStatus.errors)) {
                formStatus.errors = {};
            }
            formStatus.errors.height = "You have an invalid height";
            return false;
        }
    }

    return true;
}


function showNextOptionAfterSize() {

    if (isWidthValid(true) && isHeightValid(true)) {

        var optionToShow = getFirstOptionToShow();

        $('#' + optionToShow).fadeIn(200);
        writeFormStatus();
    }
}


function validateHeight(strict) {

    if (formStatus.firstTime) {
        strict = false;
    }

    var isValid = isHeightValid(strict);
    formStatus.validSize = isValid;
    writeFormStatus();

    if (isValid) {
        var css = {"border-color": siteDefaults.cssFont};
        $('#heightError').html('');
        $('#heightInch').css(css);
        $('#height').css(css);
    } else {
        var css = {"border-color": "darkred"};
        $('#heightError').html(moduleInfo.messageHeight);
        $('#heightInch').css(css);
        $('#height').css(css);

        resetFormPrice();
    }

    displaySizeFormIfValid();
    showNextOptionAfterSize();

    return isValid;
}


function buildSwing(numberOfPanels) {

    var leftDoorImage = '<img src="images/swing-left.gif">';
    var right = '';
    var both = '<img src="images/swing-left.gif">';
    var blankPanel = '<img src="images/slide-panel.gif">';

    var a = '';
    for (a = 0; a < numberOfPanels - 1; a++) {
        leftDoorImage = leftDoorImage + blankPanel;
        right = right + blankPanel;
        if (a != numberOfPanels - 2) {
            both = both + blankPanel;
        }
    }

    right = right + '<img src="images/swing-right.gif">';
    both = both + '<img src="images/swing-right.gif">';

    var buttonsImage = [leftDoorImage, right, both];
    var buttonsLabel = ['left', 'right', 'both'];
    var buttonsImage = [leftDoorImage, right, both];
    var buttonsLabel = ['left', 'right', 'both'];

    if (numberOfPanels == 1 || numberOfPanels == 2 || moduleInfo.doubleSwing == 0) {
        buttonsImage = [leftDoorImage, right];
        buttonsLabel = ['left', 'right'];
    }

    var id = '#swingSection';
    $(id).html('');

    for (a = 0; a < buttonsImage.length; a++) {

        var buttonID = buttonsLabel[a] + '" data-button="' + numberOfPanels;
        var label = buttonsLabel[a];
        var doorImages = buttonsImage[a];

        attachButton(label, doorImages, id, buttonID);
    }

    $('.button').unbind("click").bind("click", selectButton);
}


function buildTrack() {

    var myPromise = '';
    if (formStatus.chooseFrameSection === 'frame-Aluminum Block') {
        myPromise = addExtendedHalfButtons('#chooseTrackSection', "track2")
    } else {
        myPromise = addExtendedHalfButtons('#chooseTrackSection', "track")
    }
    $('.extendedHalfButton').unbind("click").bind("click", extendedHalfSelected);

    return myPromise;
}


function buildSwingInsideOutside() {

    var id = "#swingDirectionSection";
    var disabled = 'Disabled';

    var swingDirectionImage = {
        left: {inside: "images/swing/no in left.svg", outside: "images/swing/Left Out.svg"},
        right: {inside: "images/swing/no in right.svg", outside: "images/swing/Right Out.svg"},
        both: {inside: "images/swing/no in both.svg", outside: "images/swing/Both Out.svg"}
    }

    var frame = splitForSecondOption(formStatus.chooseFrameSection);
    if (frame == 'Aluminum Block' || frame == 'Vinyl Block') {
        swingDirectionImage = {
            left: {inside: "images/swing/Left In.svg", outside: "images/swing/Left Out.svg"},
            right: {inside: "images/swing/Right In.svg", outside: "images/swing/Right Out.svg"},
            both: {inside: "images/swing/Both In.svg", outside: "images/swing/Both Out.svg"}
        }
        disabled = '';
    }
    
    if(formStatus.doorStyle){
        swingDirectionImage = {
            left: {inside: "images/swing/Inside Left.svg", outside: "images/swing/Outside Left.svg"},
            right: {inside: "images/swing/Inside Right.svg", outside: "images/swing/Outside Right.svg"},
            both: {inside: "images/swing/Both In.svg", outside: "images/swing/Both Out.svg"}
        };
        disabled = '';
    }

    var image = swingDirectionImage[formStatus.chooseSwings];

    if (image) {
        var button1 = {};
        button1.url = image.outside;
        button1.name = "Outswing";
        button1.description = "Panels swing and stack to outside of opening.";

        var button2 = {};
        button2.url = image.inside;
        button2.name = "Inswing";
        button2.description = "Panels swing and stack to inside of opening.";

        var buttonData = [button1, button2];

        var buttons = buildExtendedHalfButtons(buttonData, "swing", disabled);

        $(id).html('');
        $(id).append(buttons);
        //$(id).append(inside);
        $('.extendedHalfButton', id).unbind("click").bind("click", extendedHalfSelected);
    }
}


function displaySizeExtendedSelected(selector) {

    jQuery('<div/>', {
        class: 'extendedSelectedDiv'
    }).appendTo($(selector));

    jQuery('<img/>', {
        src: 'images/icon-selected.svg'
    }).appendTo($(".extendedSelectedDiv", selector));
}

