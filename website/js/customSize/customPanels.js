'use strict'


function customPanelOptionsHMTL() {


    var html = frameOptionsHMTL();
    html += trackOptionsHMTL();
    html += numberOfPanelsHMTL();
    html += swingDirectionHMTL();
    html += panelMovementOptionsHMTL();
    html += swingInOrOutOptionsHMTL();
    html += screensHTML();
    html += nextSectionOptionsHMTL();


    return html;
}


function notActiveOption(option) {

    if (empty(formStatus.panelOptions[option])) {
        return true;
    }

    if (option == 'nextSection') {
        return false;
    }

    var panelOption = formStatus.panelOptions[option];
    var active = false;

    if (panelOption.show == 0) {
        active = true;
    }
    if (isGuest() && panelOption.forGuest == 0) {
        active = true;
    }

    if (!empty(panelOption.default)) {
        if (empty(panelOption.selected)) {
            panelOption.selected = panelOption.default;
        }
    }

    formStatus.panelOptions[option] = panelOption;
    writeFormStatus();

    return active;
}


function panelOptionHidden(option) {

    if (!empty(formStatus.panelOptions[option].selected)) {
        return '';
    }
    var previousOption = formStatus.panelOptions[option].previousOption;

    if (!empty(previousOption)) {
        if (!empty(formStatus.panelOptions[previousOption].selected)) {
            return '';
        }
    }

    if (formStatus.panelOptions[option].initiallyHidden != 1) {
        return '';
    }

    return ' class="hidden" ';
}


function frameOptionsHMTL() {

    if (notActiveOption('frame')) {
        return '';
    }

    var isHidden = ' class="hidden" ';
    if (isWidthValid(true) && isHeightValid(true)) {
        isHidden = '';
    }

    var frameButtons = buildHalfButtons(formStatus.panelOptions.frame, 0);

    var html = '' +
        '<div id="frame"  ' + isHidden + '> ' +
        '   <div class="title"><P>Choose Frame Type:</P></div> ' +
        '   <div class="sizeNote"><b>New construction or Retro-fit?</b> Select the frame that you believe best fits your needs.' +
        '   If you are not sure what track is right for your job, select the one you think is best. You will be able to change' +
        '   the track type before your order is processed. All frames are the same cost.</div> ' +
        '   <div id="chooseFrameSection" class="sectionBlock">' +
        '       ' + frameButtons +
        '   </div> ' +
        '</div>';

    return html;
}


function trackOptionsHMTL(group) {

    if (notActiveOption('track')) {
        return '';
    }

    if (empty(group)) {
        group = getGroupForButtons(formStatus.panelOptions.track.buttons, formStatus.panelOptions.track.selected);
    }

    var isHidden = panelOptionHidden('frame');
    
    if(formStatus.doorStyle){
        var doorStyleButtons = buildDoorStyleButtons(formStatus.panelOptions.track, group);

        var html = ' ' +
            '<div id="track" ' + isHidden + '> ' +
            '   <div class="title"><P>Choose Door Style:</P></div> ' +
            '   <div id="chooseTrackSection" class="sectionBlock"> ' +
            '       ' + doorStyleButtons +
            '   </div> ' +
            '</div>';

        return html;
    }else{
        var trackButtons = buildHalfButtons(formStatus.panelOptions.track, group);

        var html = ' ' +
            '<div id="track" ' + isHidden + '> ' +
            '   <div class="title"><P>Choose Track Type:</P></div> ' +
            '   <div class="sizeNote"><b> There are two track types: </b> With weather-barrier upstand, and without weather-barrier upstep.' +
            '   As the name suggest, the track with the upstep will give optimal protection against wind and water intrusion.</div> ' +
            '   <div id="chooseTrackSection" class="sectionBlock"> ' +
            '       ' + trackButtons +
            '   </div> ' +
            '</div>';

        return html;
    }
}

function buildDoorStyleButtons(optionData, group){
    
    var buttonData = optionData.buttons;
    var buttons = '';
    for (var a = 0; a < buttonData.length; a++) {
        var buttonInfo = buttonData[a];
        buttonInfo.nameOfOption = optionData.nameOfOption;
        buttonInfo.selected = optionData.selected;

        var updatedUrl = getOrientedDoorStyleImageUrl(buttonInfo.url, formStatus.panelOptions);

        var button = buildDoorStyleButton(buttonInfo.name, updatedUrl, buttonInfo.name, buttonInfo.selected);
        if (buttonInfo.myGroup == group || group == 0 ) {
            buttons = buttons + button;
        }
    }

    return buttons;
}

function buildDoorStyleButton(label, url, id, selected){
    var selectedIcon = '';
    if(selected == id){
        selectedIcon = '<div class="selectedDiv">' +
        '   <img src="images/icon-selected.svg"> ' +
        '</div>';
    }
    var customStyle = 'style="background-color:#44ac61;color:#FFFFFF"';
    if (empty(selectedIcon)) {
        customStyle = '';
    }
    return '<div id="track-'+id+'" data-button="1" class="button doorStyle">' + 
                '<div class="buttonImage">' +
                    '<img src="'+url+'" width="150" height="150">' + 
                '</div>' +
                '<div class="buttonLabel" ' + customStyle + '>'+label+'</div>' + 
                selectedIcon +
            '</div>';
}

function numberOfPanelsHMTL() {

    if (notActiveOption('panels') || formStatus.doorStyle) {
        return '';
    }

    var isHidden = panelOptionHidden('panels');
    var arrayOfPanels = formStatus.panelOptions.panels.arrayOfPanels;

    if (empty(arrayOfPanels)) {
        isHidden = ' class="hidden" ';
    }

    var panelButtons = buildPanelButtons(arrayOfPanels);

    var html = '' +
        '   <div id="panels" ' + isHidden + '> ' +
        '       <div class="title">Choose Number of Panels:</div> ' +
        '       <div id="panelButtonsBox" class="sectionBlock"> ' +
        '           ' + panelButtons +
        '       </div> ' +
        '   </div>';

    return html;
}


function swingDirectionHMTL() {

    if (notActiveOption('swingDirection')) {
        return '';
    }

    var isHidden = panelOptionHidden('swingDirection');

    var swingButtons = buildSwingDoorButtons(formStatus.numberOfPanels);

    var html = '' +
        '<div id="swingDirection" ' + isHidden + '> ' +
        '   <div class="title">Choose Swing Door Option & Position:</div> ' +
        '   <div class="sizeNoteMulti">Select 1 or 2 swing doors and their placement: Your door system can have ' +
        '   either one and possibly two sing doors. Select either swing door on left, right, or if available both. </div> ' +
        '   <div class="sizeNoteRed">CAUTION!: Images are of door as viewed from outside looking in.</div> ' +
        '   <div id="swingSection" class="sectionBlock" data-section="Swing Doors">' +
        '        ' + swingButtons +
        '   </div> ' +
        '</div>';

    if (!empty(moduleInfo.windowsOnly)) {
        html = '';
    }
    return html;
}


function panelMovementOptionsHMTL() {

    if (notActiveOption('movement') || formStatus.doorStyle) {
        return '';
    }
    var isHidden = panelOptionHidden('swingDirection');
    var movementButtons = buildPanelMovementButtons(formStatus.numberOfPanels);

    var html = '' +
        '<div id="movement" ' + isHidden + '> ' +
        '   <div class="title"><P>Panel Movement:</P></div> ' +
        '   <div class="sizeNoteMulti"><b>Panel slide-n-pivot options:</b> The sliding panels of your system will slide toward and pivot out beside your swing door. If you have selected 2 swing doors, you can choose side you would like the panels to stack.</div> ' +
        '   <div id="panelMovementSection" class="sectionBlock"> ' +
        '       ' + movementButtons +
        '   </div> ' +
        '</div>';

    if (!empty(moduleInfo.windowsOnly)) {
        html = '';
    }

    return html;
}


function screensHTML() {


    if (empty(formStatus.panelOptions.screens)) {
        return '';
    }

    if (isUK()) {
        return '';
    }

    if (isGuest()) {
        return '';
    }


    var isHidden = panelOptionHidden('swingInOrOut');
    var screenButtons = buildScreensOptions();

    var html = '' +
        '<div id="screensUS" ' + isHidden + '> ' +
        '   <div class="title"><P>Choose Screens:</P></div> ' +
        // '   <div class="sizeNoteMulti"></div> ' +
        '   <div id="screenSection" class="sectionBlock"> ' +
        '       ' + screenButtons +
        '   </div> ' +
        '</div>';

    if (!empty(moduleInfo.windowsOnly)) {
        html = '';
    }

    return html;
}



function swingInOrOutOptionsHMTL() {

    if (notActiveOption('swingInOrOut')) {
        return '';
    }
    var isHidden = panelOptionHidden('movement');
    var swingInOrOutButtons = buildInSwingOrOutSwing();

    var html = '' +
        '<div id="swingInOrOut" ' + isHidden + '> ' +
        '   <div class="title"><P>Choose Swing Direction:</P></div> ' +
        '   <div class="sizeNoteMulti"><b>Out-swing or In-swing:</b> Select wether you want your doors to swing and stack on the outside, or on the inside. </div> ' +
        '   <div id="swingInOrOutSection" class="sectionBlock"> ' +
        '       ' + swingInOrOutButtons +
        '   </div> ' +
        '</div>';

    if (!empty(moduleInfo.windowsOnly)) {
        html = '';
    }

    return html;
}


function nextSectionOptionsHMTL() {

    var html = '' +
        '<div id="nextSection"> </div>';

    return html;
}


function getPanels() {

    var width = $("#width").val();
    formStatus.width = width;
    validateWidth(width);

    writeFormStatus();

    if (!isWidthValid(true)) {
        return $.Deferred().resolve().promise();
    }

    if (width > 0) {

        $.when(
            getPanelGroupsByWidth(width)
        ).done(function (arrayOfPanels) {
            delete formStatus.newWidth
            updatePanelInformation(arrayOfPanels);
        });
    }
}

function checkForSill() {

    if (!isWidthValid(true)) {
        return ;
    }

    if (!empty(formStatus.sillOptions)) {
        if (!empty(formStatus.sillOptions.width)) {
            formStatus['sillOptions'].width = $('#width').val()
            alert("You have a Sill with a width of "+formStatus.sillOptions.width);
        }
    }
}


function checkForTopExtender() {

    var isValid = isHeightValid(true);
    if (!isValid) {
        return;
    }
    if (!empty(formStatus.addOnChoices)) {
        if (!empty(formStatus.addOnChoices['extender-topExtender'])) {
            if (!empty(formStatus.addOnChoices['extender-topExtender'].height)) {
                if (!empty(formStatus.addOnChoices['extender-topExtender'].height > 0 && formStatus.addOnChoices['extender-topExtender'].selected == 'yes' )) {
                    alert("You have a Top Extender with a height of " + formStatus.addOnChoices['extender-topExtender'].height);
                }
            }

        }
    }
}


function updatePanelInformation(arrayOfPanels) {

    if (JSON.stringify(formStatus.panelOptions.panels.arrayOfPanels) === JSON.stringify(arrayOfPanels.panels)) {
        formStatus.panelOptions.panels.arrayOfPanels = arrayOfPanels.panels;
        updateFormPrice();
        return;
    }

    formStatus.numberOfPanels = '';
    formStatus.panelOptions.panels.arrayOfPanels = arrayOfPanels.panels;
    writeFormStatus();

    formStatus.panelOptions.panels.selected = '';

    if (!notActiveOption('swingInOrOut')) {
        formStatus.panelOptions.swingInOrOut.selected = '';
    }

    if (!notActiveOption('swingDirection')) {
        formStatus.panelOptions.swingDirection.selected = '';
    }

    if (!notActiveOption('screens')) {
        formStatus.panelOptions.screens.selected = '';
    }

    var html = numberOfPanelsHMTL();
    $('#panels').replaceWith(html);

    if(!arrayOfPanels.panels.length){
        $('#panelButtonsBox').html('There are no panels for this width');
    }
    $('#swingDirection').hide();
    $('#movement').hide();
    $('#swingInOrOut').hide();
    $('#screensUS').hide();

    updateFormPrice();
    showNextOptionAfterSize();
    $('.button').unbind("click").bind("click", selectButton);
    $('.extendedHalfButton').unbind("click").bind("click", halfButtonClick);
}


function panelMaxSize(numberOfPanels) {

    var maxSize = numberOfPanels * 50;
    if (maxSize > 890) {
        maxSize = 890;
    }

    return maxSize;
}


function panelMaxSizeCutSheets(numberOfPanels) {

    var maxSize = numberOfPanels * 50;
    if (maxSize > 550) {
        maxSize = 550;
    }

    return maxSize;
}



function buildPanelButtons(arrayOfPanels) {

    if (empty(arrayOfPanels)) {
        return '';
    }

    var maxSize = 0;
    var buttons = '';
    for (var a = 0; a < arrayOfPanels.length; a++) {

        var numberOfPanels = arrayOfPanels[a];
        maxSize = panelMaxSize(numberOfPanels)

//        var offset = (maxSize / 2) - 21;

        var selected = '' +
            '<div class="selectedDiv"> ' +
//            '   <img src="images/icon-selected.svg" style="margin-left: ' + offset + 'px; margin-top: -112px; z-index: 10; float: left;"> ' +
            '   <img src="images/icon-selected.svg"> ' +
            '</div>';

        if (numberOfPanels != formStatus.numberOfPanels) {
            selected = '';
        }


        var doorImages = buildDoorPanels(numberOfPanels, maxSize);
        var buttonID = 'panels-' + numberOfPanels + '" data-button="' + numberOfPanels;
        var label = numberOfPanels + ' Panel';
        var button = buildButton(label, doorImages, buttonID, selected);

        buttons = buttons + button;
    }

    return buttons;
}
