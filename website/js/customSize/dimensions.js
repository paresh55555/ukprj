'use strict'


function customDimensionsHTML() {

    var rounding = '(round to nearest .25")';
    var sizing = '<B>Proper sizing.</B> We recommend that your door system be <B>1 inch narrower</B> and <b>1 inch shorter</b> than your opening to allow for needed adjustments when installing. ';

    if (isUK()) {
        rounding = '(round to nearest mm)';
        sizing = '<B>Proper sizing.</B> We recommend that your door system be <B>20mm narrower</B> and <b>20mm shorter</b> than your opening to allow for needed adjustments when installing. ';

    }

    var width = formStatus.width;
    var height = formStatus.height;
    if (height == 0 ) {
        height = '';
    }
    if (width == 0) {
        width = '';
    }

    var html = '' +
        '<div id="chooseDimensions"> ' +
        '   <div class="title">Choose Dimensions:</div> ' +
        '   <div class="sectionBlock"> ' +
        '       <div class="dimensionNote">' + sizing +
        '   </div> ' +
        '   <div id="dimensionsInput"> ' +
        '       <div id="widthInput" > ' +
        '           <div id="widthNote">Width ' + rounding + '</div> ' +
        '           <input class="textBox" type="text" id="width" value="'+ width +'"  '+ cssBorder+' /> ' +
        '               <div id="widthInch" '+ cssBorder +'>'+uomText +'</div> ' +
        '               <div id="widthError"></div> ' +
        '           </div> ' +
        '           <div id="heightInput"> ' +
        '               <div id="heightNote">Height ' + rounding + '</div> ' +
        '               <input class="textBox" type="text" id="height" value="'+ height +'" '+cssBorder+'/> ' +
        '               <div id="heightInch" ' + cssBorder + '>'+uomText +'</div> ' +
        '               <div id="heightError"></div> ' +
        '           </div> ' +
        '       </div> ' +
        '   </div> ' +
        '</div>';

    return html;
}