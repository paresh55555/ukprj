'use strict';

var installation = {};


function loadInstallation() {

    updateFormStatus();

    $.get('html/installation.html', function (html) {

        buildAndAttachCorePage(html);
        selectTab('#installation')

        $('#nextHardware').hide();

        getInstallationOptions();

        scrollTopOfPage();
        addFooter();
        updateFormPrice();

    });

}


function checkInstallationChoices() {

    if (formStatus.installationOptions) {
        var id = "[id='" + formStatus.installationOptions + "']";
        var idSelector =  $('.extendedButtonImage', id);
        displayExtendedSelected(idSelector);
    }


}

function getInstallationOptions(type) {

    var url = "https://" + apiHostV1 + "/api/installation?" + addSite();
    $.getJSON(url, function (apiJSON) {
        installation = apiJSON;
        var id = "#installationOptions";

        addInstallationOptions(id);
        checkInstallationChoices();
        createInstallationButtons(id);

    });
}

function createInstallationButtons(id) {
    $('.extendedButton', id).unbind("click").bind("click", extendedSelected);
}


function addInstallationOptions(id) {

    var buttons = buildInstallationButtons(installation);
    $(id).html('');
    $(id).append(buttons);
}



function buildInstallationButtons(buttonData) {
    var buttons = [];
    for ( var a=0; a < buttonData.length ; a++){
        var install = buttonData[a];
        var button = buildExtendedButton(install);
        buttons.push(button);
    }
    return (buttons);
}

