'use strict'

function loadAccount () {

    var url = "https://" + apiHostV1 + "/api/salesperson/" + user.id + "/defaults?"+authorizePlusSalesPerson() + addSite();
    $.getJSON(url, function (apiJSON) {

        validate(apiJSON);
        defaults =  apiJSON;

        var html = '<div class="accountTitle"> Default Quote Settings</div>' +
            '       <div id="salesAdminDefaults"></div>';
        $('#mainContent').html(html);

        buildSalesAdmin('#salesAdminDefaults');

        $('.SACodeText').html('Markup');
        showContentAndSaveState();


    });

}