'use strict';

function renderWindowsCutSheet(cutSheetData) {

    stopSpinner();
    var cutSheetHTML = buildWindowCutSheet(cutSheetData);

    var html = '' +
        '<div class=pageBox>' +
        '   ' + cutSheetHTML +
        '</div>' ;

    $('#companyHeader').hide();
    $('body').append(html);
}


function buildWindowCutSheet(cutSheetData) {

    var windowSystemRowHTML =  windowSystemRow(cutSheetData.system);
    var rowTitles = buildPartRowTitles();
    var rows = buildPartsRow(cutSheetData.parts);
    var locations = buildLocations(cutSheetData.locations);

    return windowSystemRowHTML + rowTitles + rows + locations;
}


function windowSystemRow(system) {
    
    var html = '' +
        '<div class="wctSystemBox"> ' +
        '   <div class="wctImageBox"> ' +
        '       <img class="windowsCutSheet" src="/images/windows/cutSheets/'+ system.type +'.svg" >' +
        '   </div>' +
        '   <div class="wctSystemRow"> ' +
        '      Window System ' +
        '   </div>' +
        '   <div class="wctSystem"> ' +
        '       <div class="wctCol3 wctSystemPadding">Width</div> ' +
        '       <div class="wctCol3 wctSystemPadding">' + system.width + ' mm</div> ' +
        '   </div>'+
        '   <div class="wctSystem"> ' +
        '       <div class="wctCol3 wctSystemPadding">Height</div> ' +
        '       <div class="wctCol3 wctSystemPadding">' + system.height + ' mm</div> ' +
        '   </div>'+
        '   <div class="wctSystem"> ' +
        '       <div class="wctCol3 wctSystemPadding">Locations</div> ' +
        '       <div class="wctCol3 wctSystemPadding">' + system.locations + '</div> ' +
        '   </div>'+
        '</div>';

    return html;


}


function buildLocations(locations) {

    var locationsHTML = '';

    var position = 1;
    for (var key in locations) {
        locations[key].position = position;
        locationsHTML = locationsHTML + buildLocation(locations[key], key);
        position++;
    }

    return locationsHTML;
}


function buildLocation(location, key) {

    var locationTitles = buildLocationTitles();
    var locationRow = buildLocationRow(location);
    var locationTypesTitles = buildLocationTypesTitles();
    var locationSashRow = buildLocationTypesSash(location);
    var partTitle = buildPartRowTitles();
    var parts = buildPartsRow(location.parts);

    var glassTitle = buildGlassRowTitles();
    var glassRow = buildGlassRow(location.glass);
    var html = '' +
        ' <div class="wctBox">' +
        '   ' +locationTitles +
        '   ' +locationRow +
        '   ' + locationSashRow +
        '</div>' +

        partTitle +
        parts +
        glassTitle +
        glassRow;

    return html;
}


function buildLocationTitles() {

    var html = '' +
        '<div class="wctLocationBox"> ' +
        '   <div class="wctCol1"> Location </div> ' +
        '   <div class="wctCol2"> Row </div> ' +
        '   <div class="wctCol3"> Column </div> ' +
        '   <div class="wctCol4"> Width </div> ' +
        '   <div class="wctCol5"> Height </div> ' +
        '</div>';

    return html;
}


function buildLocationTypesTitles() {

    var html = '' +
        '<div class="wctTitleBox wctSpaceTop"> ' +
        '   <div class="wctCol1"> &nbsp; </div> ' +
        '   <div class="wctCol4"> Width </div> ' +
        '   <div class="wctCol5"> Height </div> ' +
        '   <div class="wctCol3"> &nbsp; </div> ' +
        '</div>';

    return html;
}


function buildLocationTypesRows(location) {

    var html = '' +
        '<div class="wctRowBox"> ' +
        '   <div class="wctCol1"> Opening </div> ' +
        '   <div class="wctCol4"> ' + location.width + '  </div> ' +
        '   <div class="wctCol5"> ' + location.height + '  </div> ' +
        '   <div class="wctCol3"> &nbsp; </div> ' +
        '</div>';

    return html;
}


function buildLocationTypesSash(location) {

    var html = '' +
        '<div class="wctRowBox wctSpaceBottom"> &nbsp;' +
        '</div>';

    if (!empty(location.sash)) {
        html = '' +
            '<div class="wctTitleBox wctSpaceTop"> ' +
            '   <div class="wctCol1"> &nbsp; </div> ' +
            '   <div class="wctCol4"> Width </div> ' +
            '   <div class="wctCol5"> Height </div> ' +
            '   <div class="wctCol3"> Opener </div> ' +
            '</div>' +
            '<div class="wctRowBox wctSpaceBottom"> ' +
            '   <div class="wctCol1"> Sash </div> ' +
            '   <div class="wctCol4"> ' + location.sash.width + '  </div> ' +
            '   <div class="wctCol5"> ' + location.sash.height + '  </div> ' +
            '   <div class="wctCol3"> ' + location.sash.opener + ' </div> ' +
            '</div>';
    }

    return html;
}


function buildLocationRow(location) {

    var html = '' +
        '<div class="wctRowBox"> ' +
        '   <div class="wctCol1"> ' + location.position + ' </div> ' +
        '   <div class="wctCol2"> ' + location.row + ' </div> ' +
        '   <div class="wctCol3"> ' + location.column + ' </div> ' +
        '   <div class="wctCol4"> ' + location.width + '  </div> ' +
        '   <div class="wctCol5"> ' + location.height + '  </div> ' +
        '</div>';

    return html;
}

function buildPartRowTitles() {

    var html = '' +
        '<div class="wctTitleBox"> ' +
        '   <div class="wctCol1"> Part # </div> ' +
        '   <div class="wctCol2"> Cut Part </div> ' +
        '   <div class="wctCol3"> Material </div> ' +
        '   <div class="wctCol4"> MM </div> ' +
        '   <div class="wctCol5"> Lengths </div> ' +
        '   <div class="wctCol5"> Initial </div> ' +
        '</div>';

    return html;
}


function buildPartsRow(parts) {

    var rowHTML = '';

    for (var key in parts) {
        rowHTML = rowHTML + windowPartRow(parts[key]);
    }


    return rowHTML;
}



function windowPartRow(part) {

    var row = '' +
        '<div class="wctRowBox"> ' +
        '   <div class="wctCol1"> T.B.D. </div> ' +
        '   <div class="wctCol2"> ' + part.name + ' </div> ' +
        '   <div class="wctCol3"> Aluminum </div> ' +
        '   <div class="wctCol4"> ' + part.length +' </div> ' +
        '   <div class="wctCol5"> ' + part.number + ' </div> ' +
        '   <div class="wctCol5"> &nbsp; </div> ' +
        '</div>';


    return row;

}

function buildGlassRowTitles() {

    var html = '' +
        '<div class="wctTitleBox"> ' +
        '   <div class="wctCol1"> Glass # </div> ' +
        '   <div class="wctCol2"> Glass Type </div> ' +
        '   <div class="wctCol3"> &nbsp;  </div> ' +
        '   <div class="wctCol4"> Width </div> ' +
        '   <div class="wctCol5"> Height </div> ' +
        '   <div class="wctCol5"> Initial </div> ' +
        '</div>';

    return html;
}


function buildGlassRow(glass) {

    var row = '' +
        '<div class="wctRowBox"> ' +
        '   <div class="wctCol1"> T.B.D. </div> ' +
        '   <div class="wctCol2"> ' + glass.name + ' </div> ' +
        '   <div class="wctCol3"> &nbsp; </div> ' +
        '   <div class="wctCol4"> ' + glass.width + ' </div> ' +
        '   <div class="wctCol5"> ' + glass.height + ' </div> ' +
        '   <div class="wctCol5"> &nbsp; </div> ' +
        '</div>';

    return row;
}