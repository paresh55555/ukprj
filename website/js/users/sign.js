'use strict';

var size = "3px";

function loadSignIn() {

    resetFormAndCookies();
    noNavBars();

    var html = signInHTML();

    $('#preMainContent').html('');
    $('#mainContent').html(html);

    $('#signSwitcher').corner("3px");
    $('#footer').hide();

    setupRememberME();
    setupSignInButton();

    $('#password').on("click", function () {
        $(this).select();
    });

    var forgotPassword = $('#forgotPassword');
    forgotPassword.corner(size);
    forgotPassword.unbind("click").click(doForgotPassword);

    var signIn = $('#signInSwitcher');
    var signUp = $('#signUpSwitcher');

    signIn.css({"background-color": siteDefaults.cssReverseBackground, "color": siteDefaults.cssReverseFont});
    signUp.css({"background-color": siteDefaults.cssBackground, "color": siteDefaults.cssFont});

    signIn.unbind("click");
    signUp.unbind("click").bind("click", loadSignUp);

    $('#proceedAsGuest').unbind("click").bind("click", proceedAsGuest);

    var error = getParameter('error');
    if (error) {
        $('#errorMessageLogin').html('Username or Password was incorrect');
        window.history.pushState("", "", '/?tab=signIn');
        $('#password').val('');
    }

    addFooter();
    showContentAndSaveState();


}

function signInAndSignUpsHTML() {

    var html = '' +
        '<div id="signInOnly" style="background-color:' + siteDefaults.cssReverseBackground + '" >Sign In</div> ';

    if (siteDefaults.allowSignUps == 1) {
        html = '' +
            '<div id="signInSwitcher" >Sign In</div> ' +
            '<div id="signUpSwitcher">Sign Up</div> ';
    }

    return html;
}


function signInHTML() {

    var proceedAsGuest = '';
    if (allowGuests()) {
        proceedAsGuest = '<div id="proceedAsGuest" ' + cssBorder + '>Proceed As a Guest</div> ';
    }


    if (siteDefaults.allowSignUps == 1) {

    }
    var signInAndSignUps = signInAndSignUpsHTML();

    var html = ' ' +
        '<div id="signMainContent"> ' +
        '   <div id="signSwitcher" ' + cssBorder + '> ' +
        '   ' + signInAndSignUps +
        '   </div> ' +
        '   <div id="signSwitchContent"> ' +
        '       <div id="emailLabel">Email Address</div> ' +
        '       <input class="emailSignIn" type="text" id="email" value=""/> ' +
        '       <div id="passwordLabel">Password</div> ' +
        '       <input class="passwordSignIn" type="password" id="password" value=""/> ' +
        '       <div id="rememberMeCheckBox">	' +
        '           <input id="rememberMe" type="checkbox" name="rememberMe" value="yes">Remember Me</div> ' +
        '       <div id="errorMessageLogin"></div> ' +
        '       <div id="signInButton" ' + cssBorder + '>Sign In</div> ' +
        //'       <div id="forgotPassword" ' + cssBorder + '>Forgot Password</div> ' +
        '       ' + proceedAsGuest +
        '   </div> ' +
        '   <div id="copyWriteFooter"></div> ' +
        '   <div class="spacer"></div> ' +
        '</div>';

    return html;
}


function setupSignInButton() {

    var signInButton = $('#signInButton');
    signInButton.corner(size);
    signInButton.unbind("click").click(doSignIn);

    $(document).keypress(function(e) {
        if(e.which == 13) {
            doSignIn();
        }
    });
}


function setupRememberME() {

    var rememberME = $('#rememberMe');
    if (uiState.rememberMe == 'yes') {
        rememberME.prop('checked', true);
        $('#email').val(uiState.email);
        $('#password').val('1234567891234567');
    }
    rememberME.change(setRememberME);
}


function setRememberME() {

    if ($('#rememberMe').prop('checked')) {
        uiState.rememberMe = 'yes';
    } else {
        uiState.rememberMe = '';
    }
    writeStateUI();
}


function proceedAsGuest() {

    setUser({type: "guest"});
    trackAndDoClick(getGuestTab());
}


function loadSignUp() {

    var html = signupHTML();
    $('#signSwitchContent').html(html);

    var signInButton = $('#signUpButton');
    signInButton.corner(size);
    signInButton.click(doSignUp);

    var signIn = $('#signInSwitcher');
    var signUp = $('#signUpSwitcher');

    signUp.css({"background-color": siteDefaults.cssBackground, "color": siteDefaults.cssFont});
    signIn.css({"background-color": siteDefaults.cssReverseBackground, "color": siteDefaults.cssReverseFont});

    signIn.unbind("click").bind("click", loadSignIn);
    signUp.unbind("click");
}


function signupHTML() {

    var html = '' +
        '<div id="firstNameLabel" class="signUpLabel">First Name</div> ' +
        '<input class="signUpField" type="text" id="firstName" value=""/> ' +
        '<div id="lastNameLabel" class="signUpLabel">Last Name</div> ' +
        '<input class="signUpField" type="text" id="lastName" value=""/> ' +
        '<div id="emailSignUpLabel" class="signUpLabel">Email Address</div> ' +
        '<input class="signUpField" type="text" id="emailAddress" value=""/> ' +
        '<div id="passwordSignUpLabel" class="signUpLabel">Password</div> ' +
        '<input class="signUpField" type="password" id="password" value=""/> ' +
        '<div id="signUpButton" ' + cssBorder + '>Sign Up</div>';

    return html;
}


function setRememberMeEmallPassword(data) {

    uiState.email = data.email;
    uiState.token = data.token;

    writeStateUI();
}


function rememberMeAddUser(checkUser) {

    if (uiState.rememberMe == 'yes') {
        return uiState.token;
    }

    return '';
}

function V3Token(email, password, myTab) {

    var pass = email + ':' + password;
    var newPass = btoa(pass);
    var header = 'Basic ' + newPass;
    var url = "https://" + apiHostV1 + "/api/V4/token";


    $.ajax({
        url: url,
        type: 'post',
        headers: {
            Authorization: header
        },
        success: function (data) {

            if (!empty(data.token)) {
                user.v3Token = data.token;
                uiState.v3Token = data.token;
                writeUser(user);
                writeStateUI();
            }
            // window.location.href = "/?tab=" + myTab;
        }
    });
}



function doSignIn() {

    hideContent();
    startSpinner();
    var email = $('#email').val();
    var password = $('#password').val();

    var token = rememberMeAddUser();
    var checkUser = {"email": email, "password": password, "token": token};

    var json = JSON.stringify(checkUser);


    var url = "https://" + apiHostV1 + "/api/user?" + addSite();
    $.post(url, json, function (data) {

        var status = validatedSalesPersonLogin(data);
        if (!status) {
            return;
        }


        setRememberMeEmallPassword(data);
        writeLocalStorageWithKeyAndData('dealer', data.dealer);
        setIframe();

        writeDefaults({});
        user = {
            name: data.name, type: data.type, id: data.id,
            token: data.token, extended: data.extended,
            email: data.email, prefix: data.prefix,
            account: data.salesAdmin, anytimeEdits: data.anytimeQuoteEdits,
            v3Token: data.v3Token, v4Token: data.v4Token, pipelinerId: data.pipelinerId
        };

        if (!empty(data.superToken)) {
            user.superToken = data.superToken;
        }

        defaults = data.defaults;
        discounts = defaults;
        quote.SalesDiscount = defaults;
        formStatus = formStatusDefaults();

        writeUser(user);
        writeFormStatus();
        writeDefaults(defaults);
        writeQuote(quote);

        var myTab = 'guest';
        if (user.type == 'leads') {
            myTab = 'viewLeads';
        } else if (user.type == 'accounting' || user.type == 'accountingLimited' || user.type == 'audit') {
            myTab = 'verifyDeposit';
        } else if (user.type == 'production' || user.type == 'productionLimited' ) {
            myTab = 'viewAllProduction';
        }  else if (user.type == 'survey' || user.type == 'surveyLimited') {
            myTab = 'viewAllSurvey';
        }
        else if (user.type == 'builder') {
            myTab = 'frontEndAssets';
        } else {
            var status = isUK();
            if (status == true) {
                writeLocalStorageWithKeyAndData('quoteState', {'started':true});
                myTab = 'home';
            } else {
                myTab = 'viewQuotes&r='+currentTimeInSeconds();
            }
        }

        window.location.href = "/?tab=" + myTab;
    });
}


function updateHeader() {

    //if (!empty(dealer.logoSite)) {
    //    var html = '<img src="' + dealer.logoSite + '">';
    //
    //    $('#companyLogo').html(html);
    //    $('.companyLinks').html('');
    //
    //}


}


function doSignUp() {

}


function doForgotPassword() {


}