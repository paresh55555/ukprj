"use strict";

function loadGuest() {

    user = {'type': 'guest'};
    writeUser(user);
    prepForNewGuest();
    noNavBars();

    $('#preMainContent').hide();
    var html = guestHTML();

    $('#mainContent').html(html);
    $('#footer').hide();

    var quoteButton = $('#proceedToQuote');
    quoteButton.corner(size);
    quoteButton.unbind("click").bind("click", proceedToQuote);

    $('.signUpField').bind("keyup", validateValue);

    scrollTopOfPage();
    showContentAndSaveState();

}

function leadTypeHTML() {

    var html = '' +
        '<div id="leadTypeLabel" class="leadType">* What best describes you?</div> ' +
        '<div  id="leadTypeError" class="errorMessage">Please select what best describes you</div> ' +
        '<div class="leadTypeRowBox">' +
        '   <input class="leadTypeRadio" type="radio"  value="Home Owner" name="leadType"  tabindex="13">' +
        '   <div class="leadTypeLabel">Home Owner</div>' +
        '</div>' +
        '<div class="leadTypeRowBox">' +
        '   <input class="leadTypeRadio" type="radio"  value="Contractor" name="leadType"  tabindex="14">' +
        '   <div class="leadTypeLabel">Contractor</div>' +
        '</div>' +
        '<div class="leadTypeRowBox">' +
        '   <input class="leadTypeRadio" type="radio"  value="Designer" name="leadType"  tabindex="15">' +
        '   <div class="leadTypeLabel">Architect/Designer</div>' +
        '</div>' +
        '<div class="leadTypeRowBox">' +
        '   <input class="leadTypeRadio" type="radio"  value="Builder" name="leadType"  tabindex="16">' +
        '   <div class="leadTypeLabel">Builder</div>' +
        '</div>';

    return html;
}

function guestHTML() {

    var leadType = leadTypeHTML();
    var html = '' +
        '<div  class="guestWelcomeTitle">Use Our Online Quoter to</div> ' +
        '<div  class="guestWelcomeTitle">Get Your Price in Less Than 1 Minute!</div> ' +
        '<div id="guestRegistration"> ' +
        '<div  class="guestNote">Please fill out the form below so that we can know where to send your quote. Thank you!</div> ' +
        '<div id="nameLabel" class="signUpLabel">* Name</div> ' +
        '<div id="nameError" class="errorMessage">Please enter your name</div> ' +
        '<input class="signUpField" type="text" id="name" value=""/> ' +
        '<div id="emailLabel" class="signUpLabel">* Email</div> ' +
        '<div id="emailError" class="errorMessage">Please enter your email</div> ' +
        '<input class="signUpField" type="text" id="email" value=""/> ' +
        '<div id="zipLabel" class="signUpLabel">* ' + zipText + '</div> ' +
        '<div id="zipError" class="errorMessage">Please enter your ' + zipShort + '</div> ' +
        '<input class="signUpField" type="text" id="zip" value=""/> ' +
        '<div id="phoneLabel" class="signUpLabel">* Phone</div> ' +
        '<div  id="phoneError" class="errorMessage">Please enter your phone</div> ' +
        '<input class="signUpField" type="text" id="phone" value=""/> ' +
        leadType +
        '<div id="proceedToQuote" ' + cssBorder + '>Proceed To Quote</div> ' +
        '<div  class="guestNoteLogin">Account <a href="?tab=signIn" ' + cssFont + '>Login</a></div> ' +
        '</div> ' +
        '<div id="copyWriteFooter"></div> ' +
        '<div class="spacer"></div>' +
        (tab === 'guestForm' ? '<script>fbq("track", "Lead");</script>' : '');

    return html;
}

function validateValue() {

    var text = $(this).val();
    if (text) {
        $(this).prev().hide();
    }
}


function validateGuest(name, email, zip, phone, leadType) {


    var status = true
    if (!name) {
        $('#nameError').show();
        status = false;
    }
    if (!email) {
        $('#emailError').show();
        status = false;
    }
    if (!zip) {
        $('#zipError').show();
        status = false;
    }

    if (!phone) {
        $('#phoneError').show();
        status = false;
    }

    if (!leadType) {
        $('#leadTypeError').show();
        status = false;
    }

    return status;
}


function proceedToQuote() {


    var quoteButton = $('#proceedToQuote');
    quoteButton.unbind("click").bind("click", proceedToQuote);

    var name = $('#name').val();
    var email = $('#email').val();
    var zip = $('#zip').val();
    var phone = $('#phone').val();
    var leadType = $("input[type='radio'][name=leadType]:checked").val();


    var newGuest = {"name": name, "email": email, "zip": zip, "phone": phone, "leadType": leadType};

    if (!validateGuest(name, email, zip, phone, leadType)) {
        quoteButton.bind("click", proceedToQuote);
        return;
    }
    startSpinner();

    if(tab === 'guestForm'){
        newGuest['guestForm'] = true;
    }

    var json = JSON.stringify(newGuest);

    var url = "https://" + apiHostV1 + "/api/guest?" + addSite();
    $.post(url, json, function (data) {
        if (!empty(data.guestRedirect)) {
          var random = '&' + Math.floor(Math.random() * (100000 - 1)) + 1;
          var url = "https://" + data.guestRedirect + "/?tab=guestSpecial&json="+encodeURIComponent(json)+random;
            window.location.href = url;
        } else {
            loginGuest(data, email);
        }
    });
}


function loginGuest(data, email) {

    user = { name: "Guest", id: data.id, token: data.token, type: 'guest',
        email: email, "v3Token":data.v3token, "v4Token": data.v4token,
        'guestForm': data.guestForm, 'assignBarry': data.assignBarry };

    uiState.v3Token = data.v3token;
    uiState.v4Token = data.v4token;
    writeUser(user);

    if (empty(user.assignBarry) ) {
        writeLocalStorageWithKeyAndData("gForm", 'no');
    }

    quote = resetQuote();
    // window.location.href = "/?tab=home";
    renderDynamicContent();
    // V3Token('guest','guest','home');
    trackAndDoClick('home');
}