'use strict';
var rows;
var columns;
var windowTypes;
var minWidth = 0;
var maxWidth;
var minHeight;
var maxHeight;
var currentHeight;
var currentWidth;

function loadSizeAndSash() {

    var html = getSashHTML();

    $("#headerImage").attr("src", "images/header3.png");
    buildAndAttachCorePage(html)
    $.when(getWindowTypes(), getModuleInfoFromAPI()).done(function () {
        windowsForm.module = formStatus.module;
        restoreFormState();
        $('#windowCutSheet').unbind("click").bind("click", buildCutSheetParts);
        if (windowsForm.windowType) {
            updateWindowPrice();
        }

    });

    $('#cartNav').show();
    selectTab('#sash');
    addFooter();


    $('#dynamicContent').css("visibility", "visible");
    makeNextSectionButton(0);
    showContentAndSaveState();

    $('#windowOptionsSection').hide();
    $('#nextSection').hide();
    setUpWindowHeightAndWidth();

    $('#headerStartOver').show();
    $('#headerStartOverImage').show();

}

function getSashHTML() {


     var sizeNote =  ''+
         '           <div class="dimensionNote">' +
        '               <B>Proper sizing.</B> We recommend that your door system be <B>20mm narrower</B> and <b>20mm shorter</b> than your opening to allow for needed adjustments when ' +
         '          </div>' ;

     if (isUKGroup()) {
         sizeNote = '';
     }

    var html = '' +
        '<div id="dynamicContent">' +
        '   <div id="chooseDimensions">' +
        '       <div class="title">Choose Dimensions:</div>' +
        '       <div class="sectionBlock">' +
        '       ' + sizeNote +
        '       <div id="dimensionsInput">' +
        '           <div id="widthInput">' +
        '               <div id="widthNote">Width (round to nearest mm)</div>' +
        '               <input class="textBox" type="text" id="width" value=""  '+ cssBorder + '/>' +
        '               <div id="widthInch" '+ cssBorder + '>mm</div>' +
        '               <div id="widthError"></div>' +
        '           </div>' +
        '           <div id="heightInput">' +
        '               <div id="heightNote">Height (round to nearest mm)</div>' +
        '               <input class="textBox" type="text" id="height" value="" '+ cssBorder + ' />' +
        '               <div id="heightInch" '+ cssBorder + '>mm</div>' +
        '               <div id="heightError"></div>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '<div id="windowOptionsSection">' +
        '   <div class="title">Choose Configuration of Windows:</div>' +
        '   <div class="sashNote">Select a window configuration.</div>' +
        '   <div id="windowOptions">' +
        '   </div>' +
        '</div>' +
        '<div id="windowBuilderSection">' +
        '   <div class="title">Choose Size of Windows and Openers:</div>' +
        '   <div class="sashNote">To add an opener, first select a square from your chosen window configuration and the click on an opening direction below. To remove an opener, first select the square and then click on the non-opening option.</div>' +
        '   <div id="windowBuilder">' +
        '   </div>' +
        '   <div id="operations">' +
        '       <div id="none" class="operation hingeBorder">' +
        '           <img width="100%" height="100%" src="/images/openers/white/noneWhite.svg">' +
        '       </div>' +
        '       <div id="hingeTop" class="operation hingeBorder">' +
        '           <img width="100%" height="100%" src="/images/openers/white/hingeTopWhite.svg">' +
        '       </div>' +
        '       <div id="hingeBottom" class="operation hingeBorder">' +
        '           <img width="100%" height="100%" src="/images/openers/white/hingeBottomWhite.svg">' +
        '       </div>' +
        '       <div id="hingeLeft" class="operation hingeBorder">' +
        '           <img width="100%" height="100%" src="/images/openers/white/hingeLeftWhite.svg">' +
        '       </div>' +
        '       <div id="hingeRight" class="operation hingeBorder ">' +
        '           <img width="100%" height="100%" src="/images/openers/white/hingeRightWhite.svg">' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '<div id="nextSection">' +
        '</div>' +
        '<div id="copyWriteFooter"></div>' +
        '<div class="spacer"></div>' +
        '</div>';

    return html;
}

function buildCutSheetParts() {

    apiBuildWindowsCutParts().done(function (result) {

        var debug = getParameter('debug');
        debug='yes';
        var options = 'server=' + apiHostV1 + '&tab=printWindowCutSheet&id=' + result.cutSheetId + '&debug=' + debug + authorizePlusSalesPerson() + addSite();

        var url = 'https://' + apiHost + '/pdf?' + options;

        var newWin = window.open(url);
        if (!newWin || newWin.closed || typeof newWin.closed == 'undefined') {
            alert("Popup window was blocked, turn off your popup blocker to allow for printing.");
        }

    });
}


function updateWindowPrice() {

    //Fracka TEMP by-pass

    updateFormPrice();

    return;
    if (empty(windowsForm.rows)) {
        return ;
    }
    apiGetWindowPrice().done(function (result) {

        if (empty(result)) {
            return;
        }
        $('#footerPrice').html(currency + formatMoney(result.price)).show();
    });
}


function getCutSheetParts(id) {


    var myPromise = apiGetWindowsCutParts(id);


    // $.when(setSiteDefaults()).done(function () {
    //
    //     loadLocalData();
    // });
    //
    myPromise.done(function (cutSheetData) {

        var uri = window.location.href;


        renderWindowsCutSheet(cutSheetData)

    });


}


function restoreFormState() {

    restoreDimensions();
    checkWidth('yes');
    checkHeight('yes');
    restoreWindowType();
    restoreOpeners();
}


function restoreDimensions() {

    columns = windowsForm.columns;
    rows = windowsForm.rows;
}


function restoreOpeners(openers) {
    if (openers) {
        windowsForm.openers = openers
    }
    var keys = Object.keys(windowsForm.openers);
    

    for (var a = 0; a < keys.length; a++) {
        var key = keys[a];
        var value = windowsForm.openers[key];

        selectedWindow = key;
        buildAddOpener("#" + value);
    }
}


function restoreWindowType() {

    if (!empty(windowsForm.windowType)) {

        // var selector = '#' + windowsForm.windowType;
        renderWindowPassedType(windowsForm.windowType);
        // renderWindowType(selector);
    }
}


function setUpWindowHeightAndWidth() {

    var widthSelector = $('#width');
    widthSelector.unbind("keyup").bind("keyup", checkWidth);
    widthSelector.val(windowsForm.width);

    var heightSelector = $('#height');
    heightSelector.unbind("keyup").bind("keyup", checkHeight);
    heightSelector.val(windowsForm.height);
}


function windowMinsAndMaxes() {

    var numberOfWindowChooses = windowTypes.length;

    for (var a = 0; a < numberOfWindowChooses; a++) {

        if (minWidth == 0) {
            minWidth = windowTypes[a].minWidthSize;
            maxWidth = windowTypes[a].maxWidthSize;
            minHeight = windowTypes[a].minHeightSize;
            maxHeight = windowTypes[a].maxHeightSize;
        }

        if (windowTypes[a].minWidthSize < minWidth) {
            minWidth = windowTypes[a].minWidthSize;
        }
        if (windowTypes[a].maxWidthSize > maxWidth) {
            maxWidth = windowTypes[a].maxWidthSize;
        }
        if (windowTypes[a].minHeightSize < minHeight) {
            minHeight = windowTypes[a].minHeightSize;
        }
        if (windowTypes[a].maxHeightSize > maxHeight) {
            maxHeight = windowTypes[a].maxHeightSize;
        }

    }
}


function isBlankValueAndFirstTime(value, firstTime) {

    if (empty(firstTime)) {

        return false;
    }

    if (empty(value)) {
        onlyWidthAndHeightForSash();

        return true;
    }

    return false;
}


function checkHeight(firstTime) {
    var type = 'height';
    delay(function () {
        checkWidthOrHeightAndFirstTime(type, firstTime);
    }, 200);
}


function checkWidth(firstTime) {
    var type = 'width';
    delay(function () {
        checkWidthOrHeightAndFirstTime(type, firstTime);
    }, 200);
    if (firstTime != 'yes' && formStatus.sillOptions.width) {
        formStatus.sillOptions.width = $('#width').val()
    }
}


function checkWidthOrHeightAndFirstTime(type, firstTime) {


    var selector = $('#' + type);
    var value = selector.val();

    

    if (isBlankValueAndFirstTime(value, firstTime)) {
        return;
    }

    if (!empty(windowsForm.windowType) && firstTime != 'yes') {
        windowsForm.windowType = ''
        restoreOpeners()
        onlyWidthAndHeightForSash()
        setFrameAndMoveToNextSection()
        $('#footerPrice').html('T.B.D')
    }

    if (isValidTypeWithValue(type, value)) {
        selector.removeClass('backgroundPink');
        if (windowsForm.windowType) {
            updateWindowPrice();
        } else {
            $('#footerPrice').html('T.B.D')
        }
        
        setFrameAndMoveToNextSection();
        if (type == 'width') {
            $('#widthError').html('')
        } else if (type == 'height') {
            $('#heightError').html('')
        }
    } else {
        selector.addClass('backgroundPink');
        if (type == 'width') {
            $('#widthError').html("*Windows have a maximum width of " +maxWidth+" mm and minimum width of "+minWidth+" mm")
        } else if (type == 'height') {
            $('#heightError').html("*Windows have a maximum width of " +maxHeight+" mm and minimum width of "+minHeight+" mm")
        }
        onlyWidthAndHeightForSash();
    }
}


function isValidTypeWithValue(type, value) {

    if (type == 'width') {
        if (validWidth(value)) {
            return true;
        }
    } else {
        if (validHeight(value)) {
            return true;
        }
    }

    return false;
}

function isValidWidthSash(){
    var selector = $('#width');
    var value = Number(selector.val());
    if (!value) {
        return true
    }
    if (validWidth(value)) {
        return true
    }
    return false
}


function validWidth(width) {

    if (width >= minWidth && width <= maxWidth) {
        return true;
    }
    return false;
}

function isValidHeightSash(){
    var selector = $('#height');
    var value = Number(selector.val());
    if (!value) {
        return true
    }
    if (validHeight(value)) {
        return true
    }
    return false
}


function validHeight(height) {

    if (height >= minHeight && height <= maxHeight) {
        return true;
    }
    return false;
}


function onlyWidthAndHeightForSash() {

    $('#windowOptionsSection').hide();
    $('#windowBuilderSection').hide();
    $('#nextSection').hide();
}


function setFrameAndMoveToNextSection() {
    currentHeight = $('#height').val();
    currentWidth = $('#width').val();
    windowsForm.width = currentWidth;
    windowsForm.height = currentHeight;
    formStatus.width = currentWidth;
    formStatus.height = currentHeight;
    //windowsForm.openers = {};
    writeWindowsForm();

    if (!validWidth(currentWidth) || !validHeight(currentHeight)) {
        return;
    }

    var html = buildSectionsAndButtonsForWindows(windowTypes);

    $('#windowOptions').html(html);
    $('.windowButton').unbind("click").bind("click", selectWindowType);
    
    setFrame();
    $('#windowOptionsSection').show();

    if(windowsForm.windowType){
        displaySelected('#'+windowsForm.windowType);
        renderWindowPassedType(windowsForm.windowType);
        restoreOpeners();
        $('#nextSection').show();
    }
}


function setFrame() {

    var height = $('#height').val();
    var width = $('#width').val();

    var aspectRatio = width / height;
    var UIAspectRatio = maxUIWidth / maxUIHeight;

    if (aspectRatio > UIAspectRatio) {
        chooseWidthMultiplier(width, height);
    } else {
        chooseHeightMultiplier(width, height);
    }
}


function doesWindowFit(window) {

    if (window.maxHeightSize < currentHeight || window.minHeightSize > currentHeight) {
        return false;
    }
    if (window.maxWidthSize < currentWidth || window.minWidthSize > currentWidth) {
        return false;
    }

    return true;
}


function buildSectionsAndButtonsForWindows(windowOptions) {

    var section = null;
    var windows = [];
    var html = '';

    for (var a = 0; a < windowOptions.length; a++) {

        var window = windowOptions[a];

        if (!doesWindowFit(window)) {
            continue;
        }


        if (empty(section)) {
            section = window.windowSection;
        }
        if (window.windowSection != section) {

            var newSection = buildWindowSection(windows, section);
            var sectionLine = '<div class="windowSeparator"></div>';

            section = window.windowSection;

            html = html + newSection + sectionLine;
            windows = new Array();
        }

//        if (splitForFirstOption(window.Layout) == 'V') {
            windows.push(window);
//        }
    }

    var sectionEnd = '<div class="sectionEnd"></div>';
    html = html + buildWindowSection(windows, section) + sectionEnd;

    return html;
}


function buildWindowSection(windows, section) {
    if (section) {
        $('.sashNote').show()
        var section = '<div class="windowSection"> <img src="/images/windows/sections/Section' + section + '.png"> </div>';
        var buttons = buildWindowOptions(windows);

        return section + buttons;
    } else{
        $('.sashNote').hide()
        return '<div>No window to select</div>'
    } 
}


function getWindowTypes() {

    var url = "https://" + apiHostV1 + "/api/windows?module=" + formStatus.module + authorize() + addSite();

    var json = {"width": 1000, "height": 1000};
    json = JSON.stringify(json);

    var myPromise = $.post(url, json, function (data) {
        windowTypes = jQuery.parseJSON(data);
        windowMinsAndMaxes(windowTypes);
    });

    return myPromise;
}


function addWidthsToRow(row) {

    for (var a = 1; a < row.numberOfWindows; a++) {
        row['width' + a] = Math.round(frame.width * xMult / row.numberOfWindows)
    }

    return row;
}


function selectWindowType() {

    columns = '';
    rows = '';
    windowsForm.openers = {};
    windowsForm.rows = {};

    renderWindowType(this);
    updateWindowPrice();
    $('#nextSection').show();
}

function renderWindowPassedType(type) {
    var res = type.split("-");
    var direction = res.shift();

    windowsForm['windowType'] = type;
    writeWindowsForm();
    displaySelected('#'+type);

    if (direction == 'V') {

        if (isEmptyObject(rows)) {
            windowsForm.columns = '';
            rows = buildVerticalData(res);
            windowsForm.rows = rows;
            writeWindowsForm();
        }
        buildVerticalWindows(rows);

    } else {
        if (isEmptyObject(columns)) {
            windowsForm.rows = '';
            columns = buildHorizontalData(res);
            windowsForm.columns = columns ;
            writeWindowsForm();
        }
        buildHorizontalWindows(columns);
    }

    if(formStatus.panelOptions && formStatus.panelOptions.swingDirection){
        formStatus.panelOptions.swingDirection.selected = 1;
    }

    formStatus.numberOfPanels = windowsToNumOfPanels();
    if(formStatus.panelOptions && formStatus.panelOptions.track){
        formStatus.panelOptions.track.selected = type;
        formStatus.panelOptions.panels.selected = windowsToNumOfPanels();
    }
    var section = $('#windowBuilderSection');
    section.show();

    scrollToPositionAndOffset(section, -200);
    updateWindowsPrice();
}

function windowsToNumOfPanels(){
    var panels = 0;
    for(var row in windowsForm.rows){
        if(windowsForm.rows.hasOwnProperty(row) && windowsForm.rows[row].numberOfWindows){
            panels += +windowsForm.rows[row].numberOfWindows;
        }
    }
    for(var column in windowsForm.columns){
        if(windowsForm.columns.hasOwnProperty(column) && windowsForm.columns[column].numberOfWindows){
            panels += +windowsForm.columns[column].numberOfWindows;
        }
    }
    if(panels === 0) panels = 1;
    return panels;
}

function renderWindowType(selector) {

    var type = $(selector).attr('id');
    renderWindowPassedType(type);
}


function buildWindowOptions(windowOptions) {

    var html = '';

    for (var a = 0; a < windowOptions.length; a++) {
        var window = windowOptions[a];
        var button = buildWindowTypeButton(window);

        // if (window.stacking == 'vertical') {
        html = html + button;
        // }
    }

    return (html);
}


function buildWindowTypeButton(window) {

    var id = window.id;

    var button = '' +
        '<div id="' + window.Layout + '" class="windowButton">' +
        '<div class="windowButtonImage"><img src="' + window.img + '"></div>' +
        '</div>';

    return button;
}
