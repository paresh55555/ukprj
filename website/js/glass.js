'use strict'

var glassChoices = {};
var glassOptions = {};


function loadGlass() {

    startSpinner();
    updateFormStatus();

    $.get('html/glass.html', function (html) {

        $("#headerImage").attr("src", "images/header3.png");

        buildAndAttachCorePage(html)
        $('#cartNav').show();
        selectTab('#glass');

        getGlassChoices();
        getGlassOptions();

        addFooter();
        lockDownTabs();
        updateFormPrice();
        $('#dynamicContent').css("visibility", "visible");
        makeNextSectionButton(2);
        showContentAndSaveState();
        if (empty(glassOptions)) {
            $('#chooseGlassOptions').hide();
        }
    });
}


function createGlassButtons(id) {
    $('.buttonRef', id).unbind("click").bind("click", selectGlass);
}


function createGlassButtonsOptions(id) {
    $('.buttonRef', id).unbind("click").bind("click", selectGlass);
}


function extendedOptionSelected() {
    var section = $(this).parent().attr('id');
    var optionSelected = $(this).attr('id');

    var options = {};
    if (formStatus[section]) {
        options = formStatus[section];
    }
    if(checkForYaleLock(optionSelected)){
        return;
    }

    var selectedDiv = $(".extendedSelectedDiv", this);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
        options[optionSelected] = 'no';
        formStatus[section] = options;
    }
    else {
        var selector = $('.extendedButtonImage', this);
        options[optionSelected] = 'yes';
        formStatus[section] = options;
        displayExtendedSelected(selector);
    }

    writeFormStatus();
    updateFormPrice();
}

function extendedOptionSelectedSingle() {
    var section = $(this).parent().attr('id');
    var optionSelected = $(this).attr('id');

    var options = {};
    if (formStatus[section]) {
        options = formStatus[section];
    }

    if(checkForYaleLock(optionSelected)){
        return;
    }

    // Clear possible selections
    for (var property in options) {
        if (options.hasOwnProperty(property)) {
            var selectedDiv = $(".extendedSelectedDiv", $(this).parent());
            selectedDiv.remove();
            options[property] = 'no';
        }
    }
    formStatus[section] = options;
    // Select option
    var selector = $('.extendedButtonImage', this);
    options[optionSelected] = 'yes';
    formStatus[section] = options;
    displayExtendedSelected(selector);

    writeFormStatus();
    updateFormPrice();
}

function checkForYaleLock(optionSelected){
    var isYale = false;
    if(optionSelected == 'extras-Ultion 3'){
        if (typeof formStatus.hardwareChoices != 'undefined' && formStatus.hardwareChoices == 'hardware-Yale Electronic') {
            alert('Ultion 3 can not be added to the a Yale Electronic Lock');
            formStatus.hardwareExtraChoices['extras-Ultion 3'] = 'no';
            isYale = true;
        }
    }
    return isYale;
}

function extendedSelected() {

    var thisSelector = $(this);

    var thisSection = thisSelector.parent();
    var optionSelected = thisSelector.attr('id');
    var section = $(thisSection).attr('id');
    var nextSection = thisSelector.parent().parent().next();

    if (section === "chooseTrackSection2") {
        section = "chooseTrackSection";
    }
    formStatus[section] = optionSelected;
    writeFormStatus();

    var groupOfButtons = thisSelector.parent();
    var selectedDiv = $(".selectedDivRef", groupOfButtons);
    if (selectedDiv.length > 0) {
        selectedDiv.remove();
    }
    if (nextSection.attr('id') === 'chooseTrack') {
        if (formStatus.chooseFrameSection === 'frame-Aluminum Block') {
            $("#chooseTrack").hide();
            nextSection = $("#chooseTrack2");
//            $("#chooseTrack2").show();
        } else {
            $("#chooseTrack2").hide();
            $("#chooseTrack").show();
        }
    } else if (nextSection.attr('id') === 'chooseTrack2') {
        nextSection = $('#nextColorFinish');
//        $('#nextColorFinish').show();
    }

    var selectedOptionValue = splitForSecondOption(optionSelected);

    if (selectedOptionValue == 'Unglazed') {
        $('#chooseGlassOptions').hide();
        $("#chooseGlassOptions .extendedButton").each(function(){
                var section = $(this).parent().attr('id');
                var optionSelected = $(this).attr('id');

                var options = {};
                if (formStatus[section]) {
                    options = formStatus[section];
                }

                var selectedDiv = $(".selectedDivRef", this);
                if (selectedDiv.length > 0) {
                    selectedDiv.remove();
                    options[optionSelected] = 'no';
                    formStatus[section] = options;
                }
        })
        nextSection = $('#nextHardware');
    } else {
        $('#chooseGlassOptions').show();
    }

    if (selectedOptionValue == 'none' || empty(selectedOptionValue)) {
        $('#chooseTrickleVentColor').hide();
        nextSection = $('#nextSection');
    } else {
        $('#chooseTrickleVentColor').show();
    }

    if (empty(glassOptions)) {
        $('#chooseGlassOptions').hide();
    }

    var selector = $('.buttonImageRef', this);
    displayExtendedSelected(selector);

    // flog(nextSection);
    // debugAlert(nextSection);
    //
    // scrollToNextPositon(nextSection);

    updateFormPrice();
    lockDownTabs();

    if(section == 'trickleChoices'){
        // Select 50mm Top extender when trickle vent is selected
        if(!empty(selectedOptionValue) && selectedOptionValue !== 'No Trickle Vent'){
            var topExt50 = $("#extender-topExtender50");
            if(topExt50.length){
                topExt50.click();
            }
        }
    }
}


function displayExtendedSelected(selector) {

    $('.selectedDivRef', selector).remove();

    jQuery('<div/>', {
        class: 'extendedSelectedDiv selectedDivRef'
    }).appendTo($(selector));

    jQuery('<img/>', {
        src: 'images/icon-selected.svg'
    }).appendTo($(".selectedDivRef", selector));
}


function checkGlassChoices() {

    var options = formStatus.glassChoices;

    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            if (options[key] == "yes") {
                var id = "[id='" + key + "']";
                var idSelector = $('.buttonImageRef', id);
                displayExtendedSelected(idSelector);
            }
        }
    }
    
    if (formStatus.glassChoices['glass-Unglazed'] && formStatus.glassChoices['glass-Unglazed'] === 'yes') {
        $('#chooseGlassOptions').hide();
    } else {
        $('#chooseGlassOptions').show();
    }
}


function checkGlassOptions() {

    var options = formStatus.glassOptions;

    for (var key in options) {
        if (options.hasOwnProperty(key)) {
            if (options[key] == "yes") {
                var id = "[id='" + key + "']";
                var idSelector = $('.buttonImageRef', id);
                displayExtendedSelected(idSelector);
            }
        }
    }
}


function getGlassOptions() {

    var myPromise = getGlassOptionsAPI();
    if (empty(myPromise)) {
        return;
    }
    
    myPromise.done(function(apiJSON) {
        validate(apiJSON);
        glassOptions = apiJSON;
        var id = "#glassOptions";
        addGlassOptions(id);
        checkGlassOptions();
        createGlassButtonsOptions(id);
        if (empty(glassOptions)) {
            $('#chooseGlassOptions').hide();
        }


    })

}


function getGlassChoices(type) {

    if (empty(formStatus.module)) {
        return '';
    }

    var url = "https://" + apiHostV1 + "/api/glassChoices/" + formStatus.module + "?" + authorize() + addSite();


    $.getJSON(url, function (apiJSON) {
        validate(apiJSON);

        glassChoices = apiJSON;
        if(isGuest()){
            glassChoices = glassChoices.filter(function(glass){
                return glass.name !== 'Unglazed';
            })
        }
        var id = "#glassChoices";
        addGlassChoices(id);
        checkGlassChoices();
        createGlassButtons(id);
        scrollTopOfPage();

    });
}

function addGlassOptions(id) {

    var buttons = buildGlassButtons(glassOptions, 'option');
    $(id).html('');
    $(id).append(buttons);

}


function addGlassChoices(id) {

    var buttons = buildGlassButtons(glassChoices, 'glass');
    $(id).html('');
    $(id).append(buttons);
}


function buildGlassButtons(buttonData, type) {

    var buttons = [];
    var sectionName = type === 'glass' ? 'glassChoices' : 'glassOptions';
    var glassGroups = groupBy(buttonData, 'group');
    var UIView = uiSettings.find(function(setting){
        if(type === 'glass'){
            return setting.section === 'glass';
        }
        if(type === 'option'){
            return setting.section === 'glassOptions';
        }
        return null;
    });
    
    for (var a in glassGroups) {
        var glassGroup = glassGroups[a],
            groupButtons = [],
            isMultiple = false;
        if(a != 1 && a != 0){
            // Skip delimiter for first group
            groupButtons.push('<hr class="group-delimiter"/>');
        }
        for (var g = 0; g < glassGroup.length; g++) {
            var glass = glassGroup[g];
            var defaultSelected = false;
            if(glass.multiples){
                isMultiple = true;
            }
            // Select default option if it was not changed by user
            if(glass.default && formStatus[sectionName]['glass-' + glass.name] !== 'no'){
                formStatus[sectionName]['glass-' + glass.name] = 'yes';
                defaultSelected = true;
            }
            var button = '';
            if(UIView){
                switch (UIView.display){
                    case 'half':
                        button = buildExtendedHalfButton(glass, 'glass', false, defaultSelected);
                        break;
                    case 'max':
                        button = buildExtendedMaxButton(glass, 'glass', defaultSelected);
                        break;
                    default:
                        button = buildExtendedButton(glass, 'glass', defaultSelected);
                }
            }else{
                button = buildExtendedButton(glass, 'glass', defaultSelected);
            }
            groupButtons.push(button);
        }
        buttons.push('<div id="glassGroup-'+a+'" data-multiple="'+isMultiple+'" style="overflow: auto">'+groupButtons.join('\n')+'</div>');
    }
    return (buttons);
}

function selectGlass(){

    var thisSelector = $(this);
    var groupOfButtons = thisSelector.parent();
    var multiple = groupOfButtons.data('multiple');
    var thisSection = groupOfButtons.parent();
    var parentId = groupOfButtons.attr('id')

    var optionSelected = thisSelector.attr('id');
    var section = $(thisSection).attr('id');

    var nextSibling = document.getElementById(parentId).nextElementSibling;


    var glass = {};
    if (formStatus[section]) {
        glass = formStatus[section];
    }
    var selectedDiv = $(".selectedDivRef", groupOfButtons);
    
    if(multiple){
        var selectedDivMultiples = thisSelector.find(".selectedDivRef");
        if (selectedDivMultiples.length > 0) {
            selectedDivMultiples.remove();
            glass[optionSelected] = 'no';
            formStatus[section] = glass;
        }
        else {
            var selector = $('.buttonImageRef', this);
            glass[optionSelected] = 'yes';
            formStatus[section] = glass;
            displayExtendedSelected(selector);
        }
    }else{
        if (selectedDiv.length > 0) {
            var prevSelected = selectedDiv.parents('.buttonRef');
            glass[prevSelected.attr('id')] = 'no';
            selectedDiv.remove();
            formStatus[section] = glass;
        }
        var selector = $('.buttonImageRef', this);
        glass[optionSelected] = 'yes';
        formStatus[section] = glass;
        displayExtendedSelected(selector);
        if (section == 'glassChoices') {
            scrollToNextPositon($('#chooseGlassOptions'));
        } else if(nextSibling){
            scrollToNextPositon($('#'+nextSibling.getAttribute('id')));
        } else {
            //Fracka -- Might turn into DB option to scroll or not scroll
            // scrollToNextPositon($('#nextSection'))
        }
        
    }
    
    // Deselect and hide glass options when unglazed is selected
    if (formStatus.glassChoices && formStatus.glassChoices['glass-Unglazed'] == 'yes') {
        $('#chooseGlassOptions').hide();
        $("#chooseGlassOptions .buttonRef").each(function(){
                var section = $(this).parent().parent().attr('id');
                var optionSelected = $(this).attr('id');

                var options = {};
                if (formStatus[section]) {
                    options = formStatus[section];
                }

                var selectedDiv = $(this).find(".selectedDivRef");
                if (selectedDiv.length > 0) {
                    selectedDiv.remove();
                    options[optionSelected] = 'no';
                    formStatus[section] = options;
                }
        })
        scrollToNextPositon($('#nextSection'));
    } else {
        $('#chooseGlassOptions').show();
    }

    if (empty(glassOptions)) {
        $('#chooseGlassOptions').hide();
    }

    writeFormStatus();
    updateFormPrice();
    lockDownTabs();
}