QUnit.test("Write Multiple Cookies", function (assert) {

    clearCart();
    cart = {"Cart100":{"width":"80","height":"80","choosePanels":"panels-2","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-Bronze","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"intColor":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"module":"2","moduleTitle":"Vinyl Door","numberOfPanels":2,"total":"676.12","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirection":"directionOutside","chooseTrackSection":"track-Flush","chooseFrameSection":"frame-1 inch Nail Fin","finishTypevinyl":"vinyl-0-exterior","paintedFinish":"vinyl-0-exterior","glassOptions":{"undefined-Argon":"yes"},"unique":"Cart100"}};
    var property = "Cart100";
    var result = duplicateRowFromCart(property);

    writeCart(result);
    readCart();

    var count = 0;
    for (var k in cart) {
        if (cart.hasOwnProperty(k)) {
            ++count;
        }
    }

    assert.equal(count, 2, "Should return blank text");




});



QUnit.test("Test Duplicate Cart Row", function (assert) {

    cart = {"Cart100":{"width":"80","height":"80","choosePanels":"panels-2","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-Bronze","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"intColor":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"module":"2","moduleTitle":"Vinyl Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*Vinyl Doors have a maximum width of 240 inches and a minimum width of 48 inches","messageHeight":"*Vinyl Doors have a maximum height of 96 inches and minimum height of 36 inches"},"numberOfPanels":2,"total":"676.12","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirection":"directionOutside","chooseTrackSection":"track-Flush","chooseFrameSection":"frame-1 inch Nail Fin","finishTypevinyl":"vinyl-0-exterior","paintedFinish":"vinyl-0-exterior","glassOptions":{"undefined-Argon":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart100"}};


    var property = "Cart100";
    var result = duplicateRowFromCart(property);

    assert.equal( Object.keys(cart).length, 2, "Should go from 1 to 2 ");


});


QUnit.test("Test Unqiue ID", function (assert) {

    formStatus = {"width":"120","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08","edit":"C500"}

    var result = uniqueID();
    sleep(1);
    var result2 = uniqueID();
    assert.notEqual(result, result2, "These should be different (" + result + ")-(" + result2 + ")");


});

QUnit.test("Add To Cart Test", function (assert) {


    formStatus = {"width":"100","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08"}
    var result = updateGlobalCartWithFormStatus(formStatus);

    formStatus = {"unique":"C100", "width":"120","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08"}
    var result2 = updateGlobalCartWithFormStatus(formStatus);
    assert.equal(cart.C100.width, 120 , "Width Should be Equal" );


    formStatus = {"unique":"C100", "width":"100","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08"}
    updateGlobalCartWithFormStatus(formStatus);
    assert.equal(cart.C100.width, 100 , "Width Should be Equal to Updated Value" );

});

QUnit.test("Cart Button Test", function (assert) {

    formStatus = {"width":"100","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08"}
    var result = cartButtonText(formStatus);

    var shouldResult = "Add to Cart";
    assert.equal(result, shouldResult, result + " should be: "+shouldResult );

    formStatus = {"unique":"C100", "width":"100","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08"}
    var result = cartButtonText(formStatus);
    var shouldResult = "Save Changes";
    assert.equal(result, shouldResult, result + " should be: "+shouldResult );


});

QUnit.test("Cart Has All the Valid Items Selected", function (assert) {

//    var formStatus = {"width":"100","height":"80","choosePanels":"panels-3","glassChoices":"","hardwareChoices":"","installationOptions":"","vinylType":"","extColor":"","intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"2894.08"}
    var formStatus = {"width":"230","height":"80","choosePanels":"panels-7","glassChoices":"undefined-Glass Solabran 60","hardwareChoices":"hardware-Bronze","installationOptions":"undefined-Basic","vinylType":"","extColor":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":7,"total":"7570.87","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirection":"directionInside","chooseTrackSection":"track-5/8 Step Up ","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/White-thm.jpg"},{"name":"Bronze","info":"","url":"images/hardware/Standard-Bronze-thm.jpg"},{"name":"Satin","info":"","url":"images/hardware/Standard-Satin-thm.jpg"},{"name":"Dallas Satin","info":"","url":"images/hardware/Dallas-Satin-thm.jpg"},{"name":"Curved Bronze","info":"","url":"images/hardware/Curved-Bronze-thm.jpg"},{"name":"Curved Satin","info":"","url":"images/hardware/Curved-Satin-thm.jpg"},{"name":"Dale Stainless","info":"","url":"images/hardware/Dale-Stainless-Steel-thm.jpg"}],"unique":"Cart1415710548745","chooseFrameSection":"frame-Alumimum Block"};
    var testFormStatus = {};

    jQuery.extend(testFormStatus,formStatus);
    var result = formHasAllTheValidItemsSelected(testFormStatus);
    assert.equal(result, true, "Should be a valid form");

    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.width;
    var result = formHasAllTheValidItemsSelected(testFormStatus);
    assert.equal(result, false, "Missing Width not be a valid form");


    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.height;
    var result = formHasAllTheValidItemsSelected(testFormStatus);
    assert.equal(result, false, "Should not be a valid form");


    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.choosePanels;
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, '', "Should return blank text");


    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.chooseTrackSection;
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, '', "Should return blank text");


    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.chooseFrameSection;
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, '', "Should return blank text");

    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.glassChoices;
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, '', "Should return blank text");

    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.installationOptions;
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, '', "Should return blank text");

});

QUnit.test("Build Correct Cart Button", function (assert) {

    var formStatus = {"width":"230","height":"80","choosePanels":"panels-7","glassChoices":"undefined-Glass Solabran 60","hardwareChoices":"hardware-Bronze","installationOptions":"undefined-Basic","vinylType":"","extColor":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"intColor":"","module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":7,"total":"7570.87","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirection":"directionInside","chooseTrackSection":"track-5/8 Step Up ","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/White-thm.jpg"},{"name":"Bronze","info":"","url":"images/hardware/Standard-Bronze-thm.jpg"},{"name":"Satin","info":"","url":"images/hardware/Standard-Satin-thm.jpg"},{"name":"Dallas Satin","info":"","url":"images/hardware/Dallas-Satin-thm.jpg"},{"name":"Curved Bronze","info":"","url":"images/hardware/Curved-Bronze-thm.jpg"},{"name":"Curved Satin","info":"","url":"images/hardware/Curved-Satin-thm.jpg"},{"name":"Dale Stainless","info":"","url":"images/hardware/Dale-Stainless-Steel-thm.jpg"}],"chooseFrameSection":"frame-Alumimum Block"};
    var testFormStatus = {};

    jQuery.extend(testFormStatus,formStatus);
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, "Add to Cart", "Valid Cart");


    testFormStatus = {};
    testFormStatus.unique = "C110";
    jQuery.extend(testFormStatus,formStatus);
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, "Save Changes", "this is an cart being edited");

    testFormStatus = {};
    jQuery.extend(testFormStatus,formStatus);
    delete testFormStatus.width;
    var result = cartButtonAddToCartOrSaveChanges(testFormStatus);
    assert.equal(result, '', "Should return blank text");

});




QUnit.test("Build Row Header", function (assert) {

    var row = {};
    row.index = 2;
    row.totalItems = 4;

    var results = buildRowHeader(row);
    //$('#preview').html(results);

    assert.equal(true, true, "Should return blank text");


});

QUnit.test("Build Row Titles", function (assert) {

    var row = {};
    row.total = 10000.00;

    var results = buildRowTitles(row);
    //$('#preview').html(results);

    assert.equal(true, true, "Should return blank text");


});



QUnit.test("Test Total in Cart", function (assert) {

    var cart = {};
    cart.index = 2;
    cart.index2 = 4;
    cart.index3 = 5;

    var results = getTotalItemsInCart(cart);
    assert.equal(3, results, "Should match number of properties blank text");

    cart.index4 = 5;
    results = getTotalItemsInCart(cart);
    assert.equal(4, results, "Should match number of properties blank text");

});

QUnit.test("Test Build Side Left Panels", function (assert) {

    var results = cartBuildPanelLeft(7);
    //$('#preview').html(results);
    assert.equal(true, true, "Should return blank text");

});


QUnit.test("Test Build Side Right Panels", function (assert) {

    var results = cartBuildPanelRight(7);
    //$('#preview').html(results);
    assert.equal(true, true, "Should return blank text");

});

QUnit.test("Test Build Side Both Panels", function (assert) {

    var results = cartBuildPanelBoth(9,4);
    //$('#preview').html(results);
    assert.equal(true, true, "Should return blank text");

});


QUnit.test("Build Row Panel", function (assert) {

    var row = {};
    row.total = 10000.00;
    row.totalItems = 4;
    row.choosePanels = "panels-7";
    row.chooseSwings = "right";
    //row.panelMovement = "both3-2";

    var results = buildRowPanels(row);
    //$('#preview').html(results);
    assert.equal(true, true, "Should return blank text");

});


//
//
