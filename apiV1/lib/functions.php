<?php

function buildAccountingConfig($app)
{
    $search = $app->request()->params('search');
    $order = validQuoteColumn($app->request()->params('order') );
    $direction = validDirection($app->request()->params('direction'));
    $pageLimit = getPageAndLimitWithRequest($app->request);
    $export = $app->request()->params('export');

    $config = array( "order" => $order, "direction" => $direction, "search" => $search, "pageLimit" => $pageLimit, "export" => $export);
    return $config;
}



function writeQuoteOrderWithConfig($config)
{
    $config['writeOnly'] = true;
    printQuoteOrderWithConfig($config);
}


function printQuoteOrderWithConfig($config)
{
    writePhantomjsPDF($config);
    if (empty($config['writeOnly'])) {
        displayPDF($config);
    }
}

function writePhantomjsPDF($config)
{
    $url = $config['url'];
    $fileNamePath = $config['fileNamePath'];
    execPhantomjs($url, $fileNamePath);
}

function writePhantomJsPDFWithObjectConfig($config)
{
    $url = $config->url;
    $fileNamePath = $config->fileNamePath;
    execPhantomjs($url, $fileNamePath);
}

function execPhantomJs($url, $fileNamePath)
{
    $command = "/usr/bin/phantomjs --ignore-ssl-errors=true /usr/bin/rasterize.js '" . $url . "' $fileNamePath 'Letter' ";
    exec($command);
}


function displayPDF($config)
{
    $app = $config['app'];
    $fileNamePath = $config['fileNamePath'];
    $fileName = $config['fileName'];

    $app->response->headers->set('Content-Type', 'application/pdf');
    $app->response->headers->set('Pragma', "public");
    $app->response->headers->set('Content-disposition:', 'attachment; filename=' . $fileName);
    $app->response->headers->set('Content-Transfer-Encoding', 'binary');
    $app->response->headers->set('Content-Length', filesize($fileNamePath));
    $openFile = fopen($fileNamePath, "r") or die("Unable to open file!");

    echo fread($openFile, filesize($fileNamePath));
    fclose($openFile);
}


function printQuoteOrderWithConfigUsingPhantomjs($config)
{
    $app = $config['app'];
    $url = $config['url'];
    $fileNamePath = $config['fileNamePath'];
    $fileName = $config['fileName'];

    exec("/usr/bin/phantomjs --ignore-ssl-errors=true /usr/bin/rasterize.js '" . $url . "' $fileNamePath 'Letter' ");

    $app->response->headers->set('Content-Type', 'application/pdf');
    $app->response->headers->set('Pragma', "public");
    $app->response->headers->set('Content-disposition:', 'attachment; filename=' . $fileName);
    $app->response->headers->set('Content-Transfer-Encoding', 'binary');
    $app->response->headers->set('Content-Length', filesize($fileNamePath));
    $openFile = fopen($fileNamePath, "r") or die("Unable to open file!");
    if (empty($config['writeOnly'])) {
        echo fread($openFile, filesize($fileNamePath));
    }
    fclose($openFile);
}


function removeAllHTMLTags($value)
{
    $length = strlen($value);
    $newLength = 0;

    while ($length != $newLength ) {
        $length = strlen($value);
        $value = strip_tags($value);
        $newLength = strlen($value);
    }

    return $value;
}


function sqlInsertBuilderStraight($db, $option, $table)
{
    foreach ($option as $column => $value) {
        $cols[] = trim($column);

        if (isset($value) && !is_array($value)) {
            $vals[] = mysqli_real_escape_string($db, $value);
        } else {
            $vals[] = '';
        }
    }

    $colnames = "`" . implode("`, `", $cols) . "`";
    $colvals = "'" . implode("', '", $vals) . "'";
    $sql = "INSERT INTO $table ($colnames) VALUES ($colvals)";

    return $sql;
}



    function sqlInsertBuilder($db, $option, $table)
{
    foreach ($option as $column => $value) {
        $cols[] = trim($column);

        if (isset($value) && !is_array($value)) {
            $value = removeAllHTMLTags($value);
            $vals[] = mysqli_real_escape_string($db, $value);
        } else {
            $vals[] = '';
        }
    }

    $colnames = "`" . implode("`, `", $cols) . "`";
    $colvals = "'" . implode("', '", $vals) . "'";
    $sql = "INSERT INTO $table ($colnames) VALUES ($colvals)";

    return $sql;
}


function sqlUpdateBuilder($db, $option, $table, $id)
{
    $id = mysqli_real_escape_string($db, $id);
    foreach ($option as $column => $value) {
        if ($column != 'id') {
            $value = removeAllHTMLTags($value);
            $value = mysqli_real_escape_string($db, $value); // this is dedicated to @Jon
            $value = "'$value'";
            $updates[] = "$column = $value";
        }
    }
    $implodeArray = implode(', ', $updates);
    $sql = ("UPDATE $table SET $implodeArray  WHERE jobNumber='$id' ");

    return $sql;
}


function sqlUpdateBuilderWithConfigAndData($config, $data)
{
    $idValue = $config['idValue'];
    $idName = $config['idName'];
    $db = $config['db'];
    $table = $config['table'];


    $idValue = mysqli_real_escape_string($db, $idValue);
    foreach ($data as $column => $value) {
        if ($column != $idName) {
            $value = removeAllHTMLTags($value);
            $value = mysqli_real_escape_string($db, $value); // this is dedicated to @Jon
            $value = "'$value'";
            $updates[] = "$column = $value";
        }
    }
    $implodeArray = implode(', ', $updates);
    $sql = ("UPDATE $table SET $implodeArray  WHERE $idName='$idValue' ");

    return $sql;
}


function logSQLWithConfigReturnVersion($config, $sameVersion = false)
{
    $quoteID = $config['quoteID'];
    $table = $config['table'];
    $db = $config['db'];
    $userID = $config['userID'];

    $sql = mysqli_real_escape_string($db, $config['sql']);
    $newVersion = empty($config['newVersion']) ? 'false' : 'true';

    $getVersionSQL = "SELECT `version` FROM $table WHERE quoteID = $quoteID ORDER BY logID DESC limit 1";
    $stmt = $db->prepare($getVersionSQL);
    $stmt->execute();
    /* Fetch result to array */
    $results = $stmt->get_result();
    $data = $results->fetch_assoc();

    $version = intval($data['version']);

    if (!$sameVersion) {
        $version = $version + 1;
    }

    $logSQL = "INSERT INTO $table SET quoteID='$quoteID', `sql`='$sql', `userID`='$userID' ,  `version`=$version ";

    if ($db->query($logSQL) === false) {
        throw new Exception('Database failed to LOG ' . $logSQL);
    };

    return $version;
}


function sqlUpdateBuilderID($db, $option, $table, $id)
{

    $getVersionSQL = "Show columns from `".$table."`";
    $stmt = $db->prepare($getVersionSQL);
    $stmt->execute();
    /* Fetch result to array */
    $results = $stmt->get_result();
    $validFields = [];
    while( $allFields = $results->fetch_assoc() ) {
        $validFields[] = $allFields['Field'];
    }

    $id = mysqli_real_escape_string($db, $id);
    foreach ($option as $column => $value) {
        if ($column != 'id' && !is_object($value)) {
            $value = mysqli_real_escape_string($db, $value); // this is dedicated to @Jon
            $value = "'$value'";
            
            if ( in_array($column, $validFields) ) {

                $updates[] = "$column = $value";
            }
            
        }
    }
    $implodeArray = implode(', ', $updates);


    $sql = ("UPDATE $table SET $implodeArray  WHERE id='$id' ");

    return $sql;
}


function insertOption($db, $option, $table)
{
    if (is_null($table)) {
        $table = "options";
    }
    $sql = sqlInsertBuilder($db, $option, $table);
    if ($db->query($sql) === false) {
        trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
    } else {
        $last_inserted_id = $db->insert_id;
        $affected_rows = $db->affected_rows;

        return $last_inserted_id;
    }
}


function getDatabase()
{
    //phpunit -> export SITE_NAME=panoramicdoors; phpunit tests/Custom/<your test>

    if ($_SERVER['SITE_NAME'] == 'panoramicdoors') {
        return 'panoramic';
    } else if ($_SERVER['SITE_NAME'] == 'panoramicdoorsUK') {
        return 'panoramicUK';
    }  else if ($_SERVER['SITE_NAME'] == 'panoramicdoorsUKtrade') {
        return 'panoramicUKtrade';
    } else if ($_SERVER['SITE_NAME'] == 'panoramicGroup') {
        return 'panoramicGroup';
    } else if ($_SERVER['SITE_NAME'] == 'magnalineSystems') {
        return 'magnaline';
    } else if ($_SERVER['SITE_NAME'] == 'productionCopy') {
        return 'productioncopy';
    } else if ($_SERVER['SITE_NAME'] == 'smartdoors') {
        return 'smartdoors';
    } else if ($_SERVER['SITE_NAME'] == 'pd') {
        return 'pd';
    } else if ($_SERVER['SITE_NAME'] == 'ces') {
        return 'ces';
    } else if ($_SERVER['SITE_NAME'] == 'audit') {
        return 'audit';
    } else if ($_SERVER['SITE_NAME'] == 'demo1') {
        return 'demo1';
    }

    throw new Exception('Failed to Select a Database');
}

function invoiceURL($config) {

    $config['printType'] = 'printInvoice';
    $url =  printURL($config);

    return $url;
}

function orderURL($config) {

    $config['printType'] = 'printOrder';
    $url =  printURL($config);

    return $url;
}

function quoteURL($config) {

    $config['printType'] = 'printQuote';
    $url =  printURL($config);

    return $url;
}


function printURL($config) {

    if (empty($config['collection'])) {
        $config['collection'] = '';
    }

    $url = 'https://'. $_SERVER['SERVER_NAME'] .'/?tab='.$config['printType'] .'&order='.$config['id'].'&token='. $config['token'] . '&site='.$config['site']. '&location='.$config['location'].'&collection='.$config['collection'] ;
     return $url;
}


function printGuestURL($config) {

    if (empty($config['collection'])) {
        $config['collection'] = '';
    }

    $url = 'https://'. $_SERVER['SERVER_NAME'] .'/?tab=cart&print=yes&guest='.$config['guestID'].'&prefix='.$config['prefix'].'&quote='.$config['id'].'&token='. $config['token'] . '&site='.$config['site']. '&location='.$config['location'].'&collection='.$config['collection'] ;

//    https://pd.rioft.com/?quote=42&tab=cart&prefix=SQ&location=US&print=yes&guest=22488&token=56feb57e156fd&site=pd.rioft.com
    return $url;
}

function getLocation()
{
    if (empty($_SERVER['SERVER_NAME'])) {
        return 'US';
    }

    if ($_SERVER['SITE_NAME'] == 'panoramicdoorsUK' or $_SERVER['SITE_NAME'] == 'panoramicdoorsUKTrade' )  {
        return 'UK';
    }


    if ($_SERVER['SERVER_NAME'] == 'pduk.salesquoter.co.uk' or $_SERVER['SERVER_NAME'] == 'mobileuk' or $_SERVER['SERVER_NAME'] == 'panoramicdoorsuk.ifracka.com' or $_SERVER['SERVER_NAME'] == 'panoramicdoors.salesquoter.co.uk' or $_SERVER['SERVER_NAME'] == 'salesquoter.co.uk' or $_SERVER['SERVER_NAME'] == 'uk.iteamtogether.com') {
        return 'UK';
    } else {
        return 'US';
    }
}


function isMagnaline()
{
    if ($_SERVER['SITE_NAME'] == 'magnalineSystems'  )  {
        return true;
    }

    return false;
}



function updateOptionID($db, $option, $table, $id)
{
    if (is_null($table)) {
        $table = "options";
    }
    $sql = sqlUpdateBuilderID($db, $option, $table, $id);

    if ($db->query($sql) === false) {
        trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
    }
}


function updateOption($db, $option, $table, $id)
{
    if (is_null($table)) {
        $table = "options";
    }
    $sql = sqlUpdateBuilder($db, $option, $table, $id);

    if ($db->query($sql) === false) {
        trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
    } else {
        $last_inserted_id = $db->insert_id;
        $affected_rows = $db->affected_rows;
    }

    if ($affected_rows < 1) {
        insertOption($db, $option, $table);
    }
}

function splitForSecondOption($option)
{
    if (empty($option)) {
        return '';
    }
    $options = preg_split("/-/", $option, 2);
    if (empty($options)) {
        return '';
    }
    if (!empty($options[1])) {
        return ($options[1]);
    }
    return '';
}

function splitForFourthOption($option)
{
    if (empty($option)) {
        return '';
    }
    $options = preg_split("/-/", $option);
    if (empty($options)) {
        return '';
    }
    if (!empty($options[3])) {
        return ($options[3]);
    }
    return '';
}


function humanDateToMysql($dateTime)
{
    $dateTime = preg_split("/\s+/", $dateTime);

    if (empty($dateTime[0])) {
        return '';
    }

    $date = $dateTime[0];

    if (empty($date) or $date == '/' or $date == '11/30/-0001' or $date == '--') {
        return '';
    }

    $dt = DateTime::createFromFormat('m/d/Y', $date);
    if (!empty($dt)) {
        $date = $dt->format('Y-m-d');
    }

    return $date;
}


function buildOffSetLimitWithPageLimit($page, $limit)
{
//    $page = $config['page'];
//    $limit = $config['limit'];

    if ($limit == 'All') {
        return '';
    }

    if ($page < 1 or $limit < 1) {
        $page = 1;
        $limit = 10;
    }

    $offset = ($page - 1) * $limit;

    $pageLimit = "LIMIT $limit OFFSET $offset";

    return $pageLimit;
}


function sendEmailViaSendGrid($emailInfo)
{
    if (empty($emailInfo->from)) {
        throw new Exception('SendEmail Missing From');
    };
    if (empty($emailInfo->subject)) {
        throw new Exception('SendEmail Missing Subject');
    };
    if (empty($emailInfo->message)) {
        throw new Exception('SendEmail Missing Message');
    };
    if (empty($emailInfo->address)) {
        throw new Exception('SendEmail Missing Address');
    };

    if (!filter_var($emailInfo->from, FILTER_VALIDATE_EMAIL)) {
        $emailInfo->from = 'sales@salesquoter.com';
    }

    $sendGridKey = "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";
    $sendGrid = new SendGrid($sendGridKey);

    $from = new SendGrid\Email(null, $emailInfo->from);
    $subject = $emailInfo->subject;
    $to = new SendGrid\Email(null, preg_replace('/\s+/', '', $emailInfo->address));
    $content = new SendGrid\Content("text/html", $emailInfo->message);

    $mail = new SendGrid\Mail($from, $subject, $to, $content);

    if (!empty($emailInfo->file)) {

        if(!file_exists($emailInfo->file)){
            return array("message" => 'File not exists');
        }
        $fileName = basename($emailInfo->file);
        if (!empty($emailInfo->fileName)) {
            $fileName = $emailInfo->fileName;
        }
        $attachment = new SendGrid\Attachment();

        $fileBase64 = base64_encode(file_get_contents($emailInfo->file));
        $attachment->setContent($fileBase64);
        $attachment->setType("application/pdf");
        $attachment->setFilename($fileName);
        $attachment->setDisposition("attachment");
        $attachment->setContentId(uniqid());
        $mail->addAttachment($attachment);

    }

    if (!empty($emailInfo->attachments)) {

        foreach ($emailInfo->attachments as $file) {
            $fileName = basename($file);
            $attachment = new SendGrid\Attachment();

            $fileBase64 = base64_encode(file_get_contents($file));
            $attachment->setContent($fileBase64);
            $attachment->setType("application/pdf");
            $attachment->setFilename($fileName);
            $attachment->setDisposition("attachment");
            $attachment->setContentId(uniqid());
            $mail->addAttachment($attachment);
        }

    }

    $response = $sendGrid->client->mail()->send()->post($mail);
    $message = array("message" => "failed to send email");

    if ($response->statusCode() == '202') {
        $message = array ("message" => "success");
    } else {
//        print_r($response);
//        print_r($emailInfo);
    }
    return $message;
}

function isProduction(){
    $prodDomains = array(
        'panoramicdoors-pd.salesquoter.com',
        'panoramicdoos-ces.salesquoter.com',
        'panoramicdoors.salesquoter.co.uk',
        'panoramicdoors-trade.salesquoter.co.uk'
    );

    if(in_array($_SERVER['SERVER_NAME'], $prodDomains)){
        return true;
    }
    return false;
}

function getCorrectCustomerEmail($customerData){
    $testEmails = array('fracka@fracka.com','happytester123456@gmail.com', 'alexandr.reshodko@gmail.com');
    $email = isProduction() ?
            $customerData['email'] :
            in_array($customerData['email'], $testEmails) ? $customerData['email'] : 'qa@salesquoter.com';
    return $email;
}

function sendFullPaymentConfirmationEmail($email, $orderNumber, $salesEmail){
    if(!isUK()){
        $message = file_get_contents(getcwd().'/mailTemplates/fullPaymentConfirmed.html');

        $fullPaymentEmail = (object) array(
            "address" => $email,
            "from" => $salesEmail,
            "subject" => "Your order ".$orderNumber." is completed and on its way to you",
            "message" => $message
        );
        sendEmailViaSendGrid($fullPaymentEmail);
    }
}
?>