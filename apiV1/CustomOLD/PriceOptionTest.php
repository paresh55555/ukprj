<?php


require_once "./customClasses/PriceOption.php";

/**
 * Class PriceOptionTest
 *
 * @group priceOption
 */
class PriceOptionTest extends PHPUnit_Framework_TestCase
{


    public $cost_inch;
    public $paint_foot;
    public $waste_percent;
    public $total_cost_inch;
    public $number_of_lengths;
    public $cut_size;
    protected $number_of_lengths_formula;
    protected $cut_size_formula;

    public function buildData() {

        $data['nameOfOption'] = '';
        $data['cost_meter'] = '';
        $data['cost_paint_meter'] = '';
        $data['waste_percent'] = '';
        $data['number_of_lengths_formula'] = '';
        $data['cut_size_formula'] = '';
        $data['burnOff'] = '';
        $data['quantityFormula'] = '';
        $data['costItem'] = '';

        return ($data);

    }


    public function testSetObjectFromJSON()
    {

        $priceOption = new PriceOption();

        $json = $priceOption->buildDefault();
        $defaults = json_decode($json);

        $result = $priceOption->setOptionFromJSON($json);

        $total_cost_inch = $defaults->cost_inch + $defaults->paint_foot + (($defaults->waste_percent / 100) * $defaults->cost_inch);


        $this->assertEquals($defaults->cost_inch, $priceOption->cost_inch);
        $this->assertEquals($defaults->paint_foot, $priceOption->{'cost_paint_meter'});
        $this->assertEquals($defaults->waste_percent, $priceOption->{'waste_percent'});
        $this->assertEquals($total_cost_inch, $priceOption->totalCostInch);
        $this->assertEquals($defaults->burnOff, $priceOption->burnOff);
        $this->assertEquals($defaults->quantityFormula, $priceOption->quantityFormula);
        $this->assertEquals($defaults->costItem, $priceOption->costItem);
        $this->assertTrue($result);


    }

    public function testSetNumberOfPanels()
    {
        $priceOption = new SizeOption();
        $priceOption->{'number_of_lengths_formula'} = 2;

        $priceOption->setNumber_of_lengths();

        $this->assertEquals(2, $priceOption->{'number_of_lengths'});

    }


    public function testGetTotalCostPerInch()
    {
        $priceOption = new PriceOption();
        $priceOption->cost_inch = .1250;
        $priceOption->paint_foot = 0;
        $priceOption->waste_percent = 20;

        $priceOption->getTotalCostPerInch();
        $this->assertEquals(.15, $priceOption->totalCostInch);

    }

    public function testSetCutSize()
    {
        $priceOption = new PriceOption();
        $priceOption->{'width'} = 180;
        $priceOption->{'cut_size_formula'} = 'width - 1';

        $priceOption->setCutSize();

        $this->assertEquals(179, $priceOption->{'cut_size'});

    }


    public function testSetQuantity()
    {
        $priceOption = new PriceOption();
        $priceOption->{'panels'} = 5;
        $priceOption->{'quantityFormula'} = 'panels - 1';

        $priceOption->setQuantity();

        $this->assertEquals(4, $priceOption->quantity);

    }

    public function testTotalPricePerItem()
    {
        $priceOption = new PriceOption();

        $quantityFormula = 'panels -1 ';
        $costItem  = 50;

        $data = $this->buildData();
        $data['quantityFormula'] = $quantityFormula;
        $data['costItem'] = $costItem;
        $data['nameOfOption'] = $costItem;
        $data['cost_meter']= '' ;

        $json = json_encode($data);

        $width = 180;
        $height = 96;
        $panels = 5;

        $priceOption->setOptionFromJSON($json);
        $priceOption->setTotalPrice($width, $height, $panels);

        $this->assertEquals(200, $priceOption->totalPrice);


    }


    public function testTotalPricePerInch()
    {
        $priceOption = new PriceOption();

        $cost_inch = .1250;
        $paint_foot = .00;
        $waste_percent = 20;
        $number_of_lengths_formula = '2';
        $cut_size_formula = 'width - 1';
        $burnOff = .2;

        $data = $this->buildData();
        $data['cost_meter'] = $cost_inch;
        $data['cost_paint_meter'] = $paint_foot;
        $data['waste_percent'] = $waste_percent;
        $data['number_of_lengths_formula'] = $number_of_lengths_formula;
        $data['cut_size_formula'] = $cut_size_formula;
        $data['burnOff'] = $burnOff;


        $json = json_encode($data);

        $width = 180;
        $height = 96;
        $panels = 5;

        $priceOption->setOptionFromJSON($json);
        $priceOption->setTotalPrice($width, $height, $panels);

        $this->assertEquals(53.70, $priceOption->totalPrice);


    }


  public function testBuildBreakDown()
  {

    $priceOption = new PriceOption();
    $json = $priceOption->buildDefault();
    $priceOption->setOptionFromJSON($json);

    $width = 180;
    $height = 96;
    $panels = 5;
    $priceOption->setTotalPrice($width, $height, $panels);

    $priceOption->createBreakDown();
//    $priceOption->displayBreakDown();
    $this->assertTrue(true);


  }
}

?>