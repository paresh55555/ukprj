<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';
//require 'vendor/autoload.php';
\Slim\Slim::registerAutoloader();

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
require_once 'vendor/autoload.php';

require_once 'customClasses/PriceEngine.php';
require_once 'customClasses/Site.php';
require_once 'customClasses/Customer.php';
require_once 'customClasses/Quote.php';
require_once 'customClasses/User.php';
require_once 'customClasses/Guest.php';
require_once 'customClasses/Auth.php';
require_once 'customClasses/Lead.php';
require_once 'customClasses/CutSheet.php';
require_once 'customClasses/Builder.php';
require_once 'customClasses/Search.php';
require_once 'customClasses/UIOptions.php';
require_once 'customClasses/UINav.php';
require_once 'customClasses/Glass.php';
require_once 'customClasses/CreditCard.php';
require_once 'customClasses/Payment.php';
require_once 'customClasses/Email.php';
require_once 'customClasses/PaymentLog.php';
require_once 'customClasses/UISettings.php';

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
    'debug' => true
));
require_once 'routes/cc.php';
require_once 'routes/modules.php';
require_once 'routes/quotes.php';
require_once 'routes/orders.php';
require_once 'routes/salesPersons.php';
require_once 'routes/settings.php';
require_once 'routes/options.php';
require_once 'routes/builder.php';
require_once 'routes/windows.php';
require_once 'routes/doors.php';
require_once 'routes/updates.php';
require_once 'routes/payments.php';


const SEND_GRID_KEY = '2LxeogtrRy2rx-VKQRZ9xA';


/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */
class BindParam
{
    private $values = array(), $types = '';

    public function add($type, &$value)
    {
        $this->values[] = $value;
        $this->types .= $type;
    }

    public function get()
    {


        return array_merge(array($this->types), $this->values);
    }
}

class SetSite extends \Slim\Middleware
{
    public function call()
    {
        // Get reference to application
        $app = $this->app;

        $site = $app->request->params('site');
        $_SERVER['site'] = $site;

        // Run inner middleware and application
        $this->next->call();

    }

}

$app->add(new \SetSite());


$app->get('/defaults/site', function () use ($app) {


    $site = new Site();
    $defaults = $site->getSiteForLocation($_SERVER['SITE_NAME']);

    if (!empty($defaults)) {
        header("Content-Type: application/json");
        echo json_encode($defaults);
    } else {
        notAuthorized();
    }

    exit();


});


$app->post('/guest', function () use ($app) {

    $data = $app->request->getBody();
    $guestData = json_decode($data);

    $guest = new Guest();
    $response = $guest->newGuest($guestData);

    if ($response) {
        header("Content-Type: application/json");
        echo json_encode($response);
    } else {
        notAuthorized();
    }

    exit();


});


$app->post('/images/upload/:id', function ($id) use ($app) {

//    $data = $app->request->getBody();
//    $guestData = json_decode($data);

    $ds = DIRECTORY_SEPARATOR;  //1

    $storeFolder = 'uploads';   //2

    $fileName = uniqid();

    $images = '/var/www/html/site-web/images/uploads/';


    if (!empty($_FILES)) {

        $tempFile = $_FILES['file']['tmp_name'];

        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);


        $targetFile = $images . $fileName . '.' . $ext;

        move_uploaded_file($tempFile, $targetFile); //6

        $response = array("file" => $fileName . '.' . $ext);

        renderJSON($response);

    }


});


$app->get('/users', function () use ($app) {


    authUserWithTypeAndRequest('builder', $app->request);

    $user = new User();

    $response = $user->getUsers();

    renderJSON($response);


});


$app->post('/user', function () use ($app) {


    $data = $app->request->getBody();
    $userData = json_decode($data);

    $user = new User();

    if (empty($userData)) {
        $userData->token = '';
    }
    if (empty($userData->token)) {
        $userData->token = '';
    }
    if (empty($userData->email)) {
        $userData->email = '';
    }
    if (empty($userData->password)) {
        $userData->password = '';
    }

    $response = $user->login($userData->email, $userData->password, $userData->token);

    if ($response) {
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {
        notAuthorized();
    }

    exit();


});


function renderJSON($data)
{
    header("Content-Type: application/json");
    echo json_encode($data);

}

function makeValuesReferenced($arr)
{
    $refs = array();
    foreach ($arr as $key => $value)
        $refs[$key] = &$arr[$key];
    return $refs;

}


function isUK()
{

    $location = getLocation();
    if ($location == 'UK') {
        return true;
    }
    return false;


}


function notAuthorized()
{
    $data = Array("status" => "not authorized");
    header("Content-Type: application/json");

    echo json_encode($data);
    exit();
}


function authBasic($request)
{
    $user = authCheck($request);
    if ($user) {
        return true;
    }
}

function authSuper($request)
{
    $token = $request->params('superToken');

    $auth = new Auth();
    $user = $auth->validateSuperToken($token);

    if (!empty($user)) {
        return true;
    }

    return false;
}

function authCheck($request)
{
    $token = $request->params('token');

    if (empty($token)) {
        notAuthorized();
    }

    $auth = new Auth();
    $user = $auth->validateToken($token);

    if (!empty($user)) {
        return $user;
    } else {
        notAuthorized();
    }
}

function authLeads($request)
{
    $user = authCheck($request);

    if ($user['type'] == 'leads') {
        return $user['id'];
    } else {
        notAuthorized();
    }

}

function authSales($request)
{
    $user = authCheck($request);

    if ($user['type'] == 'salesPerson' || $user['type'] == 'salesPersonSurvey') {
        return $user['id'];
    } else {
        notAuthorized();
    }

}

function authAccounting($request)
{
    $user = authCheck($request);

    if ($user['type'] == 'accounting' || $user['type'] == 'accountingLimited' || $user['type'] == 'audit') {
        return $user['id'];
    }
    else {
        notAuthorized();
    }

}

function authAccountingWithUser($request)
{
    $user = authCheck($request);

    if ($user['type'] == 'accounting' || $user['type'] == 'accountingLimited' || $user['type'] == 'audit') {
        return $user;
    }
    else {
        notAuthorized();
    }

}

function authProduction($request)
{
    $user = authCheck($request);

    if ($user['type'] == 'production' || $user['type'] == 'productionLimited' || $user['type'] == 'survey' || $user['type'] == 'surveyLimited') {
        return $user['id'];
    } else {
        notAuthorized();
    }

}


function authUserWithTypeAndRequest($type, $request)
{
    $user = authCheck($request);

    if ($user['type'] == $type) {
        return $user['id'];
    } else {
        notAuthorized();
    }

}


function auth($request)
{

    $salesPerson = $request->params('salesPerson');
    $token = $request->params('token');

    if (!$salesPerson) {
        notAuthorized();
    }
    if (!$token) {
        notAuthorized();
    }

    $db = connect_db();
    $sql = "SELECT id from authorized where salesPerson=? and token=? and `timestamp` > now() ";
//    $stmt = $db->stmt_init();
    $stmt = $db->prepare($sql);
    $stmt->bind_param("ss", $salesPerson, $token);
    $stmt->execute();
    /* Fetch result to array */
    $result = $stmt->get_result();
    $data = $result->fetch_assoc();

    if (!$data['id'] > 0) {
        notAuthorized();
    }

    return true;
}


function getPageAndLimitWithRequest($request)
{
    $page = intval($request->params('page'));
    $limit = intval($request->params('limit'));


    if ($page < 1 or $limit < 1 or $limit > 100) {
        $pageLimit = array('page' => 1, 'limit' => 10);
    } else {
        $pageLimit = array('page' => $page, 'limit' => $limit);

    }


    if ($request->params('limit') == 'All') {
        $pageLimit['limit'] = 90000000;
    }

    return $pageLimit;
}


$aBitOfInfo = function ($app) {

    $access = $app->request()->params('access');
//    echo "Current route is " . $access;
//    echo "Done";
    return;

};


$app->get('/customers', function () use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();

    $sql = "SELECT * from customer";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($options = $results->fetch_assoc()) {
        $data[] = $options;
    }
    echo json_encode($data);

});


$app->get('/customers/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $id = intval($id);

    $db = connect_db();

    $sql = "SELECT * from customer where id = ?";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $results = $stmt->get_result();
    $customer = $results->fetch_assoc();
    echo json_encode($customer);

});


$app->post('/emailGuest', function () use ($app) {

    $user = authCheck($app->request);

    $data = $app->request->getBody();
    $emailInfo = json_decode($data);

    $debug = $app->request()->params('debug');

    if (!isset($emailInfo)) {
        throw new RuntimeException("bad post data");
    }

    if (!isset($emailInfo->address)) {
        throw new RuntimeException("no emails passed");
    }


    $emailInfo->from = 'sales@salesquoter.com';
    $emailInfo->subject = "Your Door Quote";

    if (empty($emailInfo->message)) {
        $emailInfo->message = "Your Door Quote";
    }

    $fileName = "SQ" . $emailInfo->quote . ".pdf";

    $location = getLocation();

    $url = "https://" . $_SERVER['SERVER_NAME'] . "/?tab=cart&quote=" . $emailInfo->quote .
        "&prefix=SQ&location='.$location.'&print=yes&guest=" . $emailInfo->guest . "&token=" . $emailInfo->token . "&site=" . $_SERVER['site'];

    if ($debug == 'yes') {
        print_r($url);
        exit();
    }

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = '/tmp/' . $fileName;
    $config['fileName'] = $fileName;

    writePhantomjsPDF($config);

    $emailInfo->file = $config['fileNamePath'];

    $message = sendEmailViaSendGrid($emailInfo);

    header("Content-Type: application/json");
    echo json_encode($message);
});


$app->post('/email', function () use ($app) {

    $debug = $app->request()->params('debug');
    $order = $app->request()->params('order');
    $superToken = $app->request()->params('superToken');

    $realID = authSales($app->request);
    $user = new User();
    $userData = $user->getUser($realID);

    $data = $app->request->getBody();
    $emailInfo = json_decode($data);

    $emailInfo->from = $userData['email'];
    if (!filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
        $emailInfo->from = "sales@panoramicdoors.com";
    }

    if (!empty($emailInfo->quote)) {
        $fileName = 'Quote-' . $emailInfo->quote . '.pdf';
        if ($order == 'yes') {
            $fileName = 'Order-' . $emailInfo->quote . '.pdf';
        }

        $fileNamePath = "/tmp/" . $fileName;

        $location = getLocation();

        $url = "https://" . $_SERVER['SERVER_NAME'] . "/?tab=viewQuote&quote=" . $emailInfo->quote .
            "&prefix=" . $userData['prefix'] . "&location=" . $location . "&print=yes&salesPersonName=" .
            $userData['name'] . "&salesPerson=" . $emailInfo->salesPerson . "&token=" . $emailInfo->token .
            "&superToken=".$superToken."&site=" . $_SERVER['site'] . "&order=" . $order;

        if ($debug == 'yes') {
            header('Location: ' . $url);
            exit();
        }

        $config['app'] = $app;
        $config['url'] = $url;
        $config['fileNamePath'] = $fileNamePath;
        $config['fileName'] = $fileName;

        writeQuoteOrderWithConfig($config);
    }

    $message = '';

    $emailInfo->addresses[] = 'follow-up@panoramicdoors.com';
    foreach ($emailInfo->addresses as $address) {
        $emailInfo->address =  preg_replace('/\s+/', '', $address);
        $emailInfo->message = preg_replace('/\n/', '<br>', $emailInfo->message);
        if (!empty($emailInfo->quote)) {
            $emailInfo->file = $fileNamePath;
            $emailInfo->fileName = $fileName;

            $emailInfo->attachments = [];
            $filesDir = "/tmp/emailFiles-quote-" . $emailInfo->quote;
            if(is_dir($filesDir)){
                $files = array_diff(scandir($filesDir), array('..', '.'));
                foreach ($files as $file) {
                    array_push($emailInfo->attachments, $filesDir.'/'.$file);
                }
            }
        }
        if (filter_var($emailInfo->address, FILTER_VALIDATE_EMAIL)) {
            $message = sendEmailViaSendGrid($emailInfo);
        }else{
            $message = array("message" => 'Wrong email address');
        }
    }

    header("Content-Type: application/json");
    echo json_encode($message);
});


$app->post('/customers', function () use ($app) {

    $user = authSales($app->request);

    header("Content-Type: application/json");

    $data = $app->request->getBody();
    $newCustomer = json_decode($data);


    if (empty($newCustomer)) {
        echo "Error with Customer";
        return;
        // do stuff
    }

    $customer = new Customer();
    $id = $customer->add($newCustomer);

    $response = Array();
    if ($id) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
        $response['data'] = $newCustomer;
    }
    header("Contenft-Type: application/json");
    echo json_encode($response);

});


$app->delete('/customers/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $customer = array("active" => 0);

    if (empty($customer)) {
        echo "Error with Customer";
        return;
        // do stuff
    }
    $db = connect_db();
    updateOptionID($db, $customer, "customer", $id);

    if ($id) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    }
//    } else {
//        $response['status'] = 'Error';
//        $response['data'] = $newCustomer;
//    }
    header("Content-Type: application/json");
    echo json_encode($response);

});


$app->put('/customers/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $data = $app->request->getBody();

    $customer = json_decode($data);
    if (empty($customer)) {
        echo "Error with Customer";
        return;
        // do stuff
    }
    $db = connect_db();

    updateOptionID($db, $customer, "customer", $id);

    if ($id) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    }
//    } else {
//        $response['status'] = 'Error';
//        $response['data'] = $newCustomer;
//    }
    header("Content-Type: application/json");
    echo json_encode($response);

});


$app->get('/leads', function () use ($app) {


    $leadsManager = $app->request()->params('leads');
    if (Is_int($leadsManager) || $leadsManager < 1) {
        return;
    }

    authLeads($app->request());

    $pageLimit = getPageAndLimitWithRequest($app->request);
    $leadsObj = new Lead($pageLimit);

    $search = $app->request()->params('search');

    $results = $leadsObj->getLeads($search);

    header("Total: " . $results['total']);
    echo json_encode($results['data']);

});

$app->put('/leads/:id/revoke', function ($id) use ($app) {


    $leadsManager = $app->request()->params('leads');
    if (Is_int($leadsManager) || $leadsManager < 1) {
        return;
    }
    authLeads($app->request());
    $leadsObj = new Lead();

    $status = $leadsObj->revokeLead($id);
    $response = array("status" => $status);

    header("Content-Type: application/json");
    echo json_encode($response);

});


$app->get('/leads/transferred', function () use ($app) {


    $leadsManager = $app->request()->params('leads');
    if (Is_int($leadsManager) || $leadsManager < 1) {
        return;
    }

    authLeads($app->request());

    $pageLimit = getPageAndLimitWithRequest($app->request);
    $leadsObj = new Lead($pageLimit);

    $search = $app->request()->params('search');
    $results = $leadsObj->getAssignedLeads($search);

    header("Total: " . $results['total']);
    echo json_encode($results['data']);

});

$app->get('/leads/:id/transfer/:salesPerson', function ($id, $salesPerson) use ($app) {

    $id = intval($id);
    $salesPerson = intval($salesPerson);

    if (!is_int($id) || !is_int($salesPerson)) {
        return;
    }

    authLeads($app->request());
    $quote = new Quote();

    $quoteID = $quote->transferQuote($id, $salesPerson);

//    $quote->updateQuoteToSalesPerson($id, $salesPerson);

    $response = array("status" => "success", "id" => $quoteID);

    header("Content-Type: application/json");
    echo json_encode($response);

});


$app->delete('/leads/:id', function ($id) use ($app) {

    authLeads($app->request());

    $id = intval($id);

    $db = connect_db();

    $sql = "update quotesSales set status='deleted' where id = ?";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $id);

    if (!$stmt->execute()) {
        trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
    }

    $response = Array();
    $response['success'] = "true";

    echo json_encode($response);

});


$app->get('/guestQuotePrint/:id', function ($id) use ($app) {

    authCheck($app->request);

    $fileName = "SQ" . $id . ".pdf";

    $guest = $app->request()->params('guest');
    $prefix = $app->request()->params('prefix');
    $token = $app->request()->params('token');
    $debug = $app->request()->params('debug');
    $location = getLocation();

    $url = "https://" . $_SERVER['SERVER_NAME'] . "/?quote=" . $id . "&tab=cart&prefix=$prefix&location=" . $location . "&print=yes&guest=" .
        $guest . "&token=" . $token . "&site=" . $_SERVER['site'];;

    if ($debug == 'yes') {
        header('Location: ' . $url);
        exit();
    }

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = '/tmp/' . $fileName;
    $config['fileName'] = $fileName;

    writePhantomjsPDF($config);
    displayPDF($config);
});


/**
 * @param $type
 */
function displayProductionOrders($app, $type)
{
    $pageLimit = getPageAndLimitWithRequest($app->request);

    authProduction($app->request);

    $order = $app->request()->params('order');
    $direction = $app->request()->params('direction');
    $search = $app->request()->params('search');


    $quote = new Quote($pageLimit);

    $results = $quote->getProductionOrders($type, $order, $direction, $search);

    header("Content-Type: application/json");
    header("Total: " . $results['total']);
    echo json_encode($results['data']);
//    echo json_encode($results['sql']);  // If you want to know what was search, will break it for UI
//    echo json_encode($results['search']);
}


$app->get('/cutSheet/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();
    $id = intval($id);

    $sql = "SELECT json from cutSheet where jobNumber = ?";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $results = $stmt->get_result();
    $json = $results->fetch_assoc();
    echo $json['json'];

});


$app->post('/printCutSheet', function () use ($app) {

    header("Content-Type: application/pdf");

    $content = $app->request->getBody();

    require_once('lib/dompdf/dompdf_config.inc.php');

    $html =
        '<html><body>' .
        '<p>Put your html here, or generate it with your favourite ' .
        'templating system.</p>' .
        '</body></html>';

    $dompdf = new DOMPDF();
    $dompdf->load_html($content);
    $dompdf->render();
    $dompdf->stream("sample.pdf");


//    $html2pdf = new HTML2PDF('P','A4','fr');
//    $html2pdf->WriteHTML($content);
//
//    $file  = uniqid().".pdf";
//
//    $html2pdf->Output('/var/www/html/site-web/pdf/'.$file , 'F');
//    echo '{"file":"pdf/'.$file.'}';

});

$app->post('/cutSheet', function () use ($app) {

//  header("Content-Type: application/json");

    $json = $app->request->getBody();

    if (empty($json)) {
        echo "Error with cutSheet";
        return;
        // do stuff
    }

    $myData = json_decode($json);


//    var_dump($myData);
    $myJSON['json'] = $json;

    if (isset($myData->jobInfo->productionDate)) {

        $date = date('Y-m-d', strtotime($myData->jobInfo->productionDate));
        $myJSON['productionDate'] = $date;
    } else {
        $myJSON['productionDate'] = '';
    }

    if (isset($myData->jobInfo->dueDate)) {
        $date = date('Y-m-d', strtotime($myData->jobInfo->dueDate));
        $myJSON['dueDate'] = $date;
    } else {
        $myJSON['dueDate'] = '';
    }

    if (isset($myData->jobInfo->jobNumber)) {
        $myJSON['jobNumber'] = $myData->jobInfo->jobNumber;
    } else {
        $myJSON['jobNumber'] = '';
    }


    $db = connect_db();

    updateOption($db, $myJSON, "cutSheet", $myJSON['jobNumber']);

    $lastID = insertOption($db, $myJSON, "cutSheet");


    $response['id'] = $lastID;

    header("Content-Type: application/json");
    echo $myJSON['jobNumber'];

});


$app->put('/cutSheet/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $json = $app->request->getBody();


    if (empty($json)) {
        echo "Error with Customer";
        return;
        // do stuff
    }
    $myJSON['json'] = $json;

    $db = connect_db();
    updateOption($db, $myJSON, "cutSheet", $id);

    header("Content-Type: application/json");
    echo $json;

});

$app->get('/costSheet', function () use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();

    $jobNumber = $app->request()->params('jobNumber');

    $sql = "SELECT * from costSheet where jobNumber = ? ";

    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $jobNumber);

    $stmt->execute();
    /* Fetch result to array */
    $result = $stmt->get_result();
    $data = $result->fetch_assoc();

    echo $data['json'];

});


$app->post('/costSheet', function () use ($app) {

    header("Content-Type: application/json");

    $json = $app->request->getBody();

    if (empty($json)) {
        echo "Error with costSheet";
        return;
        // do stuff
    }
    $myJSON['json'] = $json;

    $myData = json_decode($json);

    if (isset($myData->jobInfo->productionDate)) {

        $date = date('Y-m-d', strtotime($myData->jobInfo->productionDate));
        $myJSON['productionDate'] = $date;
    } else {
        $myJSON['productionDate'] = '';
    }

    if (isset($myData->jobInfo->dueDate)) {
        $date = date('Y-m-d', strtotime($myData->jobInfo->dueDate));
        $myJSON['dueDate'] = $date;
    } else {
        $myJSON['dueDate'] = '';
    }

    if (isset($myData->jobInfo->jobNumber)) {
        $myJSON['jobNumber'] = $myData->jobInfo->jobNumber;
    } else {
        $myJSON['jobNumber'] = '';
    }


    $db = connect_db();
    updateOption($db, $myJSON, "costSheet", $myJSON['jobNumber']);

    $lastID = insertOption($db, $myJSON, "costSheet");

    header("Content-Type: application/json");
//  echo $json;
    $response['id'] = $lastID;

    echo $lastID;
});

$app->put('/costSheet/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $json = $app->request->getBody();


    if (empty($json)) {
        echo "Error with Customer";
        return;
        // do stuff
    }
    $myJSON['json'] = $json;

    $db = connect_db();
    updateOption($db, $myJSON, "costSheet", $id);

    header("Content-Type: application/json");
    echo $json;

});


$app->get('/swing', function () use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();
    // query the database
    $result = $db->query('SELECT name from swing');
    $data = array();

    while ($bandArr = $result->fetch_assoc(MYSQLI_NUM)) {
        $data[] = $bandArr[0];

    }

    echo json_encode($data);

//    $sql = "select * FROM wine ORDER BY name";
////    echo "$sql";
//    echo "welcome to windows";
//    $width = $app->request()->params('width');
//    $height = $app->request()->params('height');
//    echo "Width: $width & Height: $height";

});

$app->post('/swing', function () use ($app) {
//    $data = $app->request()->post();
    $data = $app->request->getBody();

    $swings = json_decode($data);
    $db = connect_db();
    header("Content-Type: application/json");
//    echo json_encode($swings);
    foreach ($swings as &$value) {
        $v1 = "'" . $db->real_escape_string($value) . "'";
//        $sql="INSERT INTO tbl (col1_varchar, col2_number) VALUES ($v1,10)";

        $sql = "insert into swing (name) VALUES ($v1) ";
        if ($db->query($sql) === false) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
        } else {
            $last_inserted_id = $db->insert_id;
            $affected_rows = $db->affected_rows;
        }
    }
    echo json_encode($swings);

});


$app->get('/buildLeads', function () use ($app) {

    $db = connect_db();

    $yesterday = date('Y-m-d', time() - 60 * 60 * 24);
    $file = '/var/www/panoramicdoors/site-web/leadReports/' . $yesterday . '.csv';

    $sql = "(select 'Sales Person','Guest Name','Guest Email','Quote Number')
UNION (select users.name as 'Sales Person', guest.name as 'Guest Name', guest.email as
            'Guest Email',quotesGuest.salesQuoteID as quoteNumber from quotesGuest, guest, 
            users where transferred >= DATE(NOW()) - INTERVAL 1 DAY and  guest.id = quotesGuest.guest  
            and quotesGuest.salesPerson = users.id INTO OUTFILE '" . $file . "' FIELDS TERMINATED BY ','
            ENCLOSED BY '\"'
            LINES TERMINATED BY '\n') ";


    if ($db->query($sql) === false) {
        throw new Exception('Failed to create update ' . $sql);
    }

    $response = array("Done");
    echo json_encode($response);

});

$app->get('/options', function () use ($app) {


    $type = $app->request()->params('type');
    $base = $app->request()->params('base');
    $db = connect_db();


//    $sql =  "SELECT * from options";
//    if ($type){
    $sql = "SELECT * from options where type=? and base=?";
//    }

    $stmt = $db->stmt_init();
    $stmt = $db->prepare($sql);


    /* bind parameters for markers */
    $stmt->bind_param("ss", $type, $base);
    $stmt->execute();
    /* Fetch result to array */
    $results = $stmt->get_result();

//    $result = $db->query( $sql );
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[] = $options;
    }

    header("Content-Type: application/json");
    echo json_encode($data);

});

$app->post('/options', function () use ($app) {
//    $data = $app->request()->post();
    $data = $app->request->getBody();

    $option = json_decode($data);
    if (empty($option)) {
        echo "Error with Parameters";
        return;
        // do stuff
    }
    $db = connect_db();
//    insertOption($db,$option);

    header("Content-Type: application/json");
    echo json_encode($option);

});


$app->post('/buildCut', function () use ($app) {

    $json = $app->request->getBody();

    $priceEngine = new PriceEngine();
    $priceEngine->buildDoorWithOptions($json);
    $priceEngine->getTotalPrice();

    $result = array("cutSheet" => $priceEngine->buildCostSheet($json));

    header("Content-Type: application/json");

    echo json_encode($result);

});


$app->post('/search/customer', function () use ($app) {

    $salesPersonID = authCheck($app->request());

    $content = $app->request->getBody();
    $searchTerm = json_decode($content);

    $search = new Search();
    $results = $search->getCustomersOfSalesPerson($searchTerm, $salesPersonID['id']);

    header("Content-Type: application/json");
    echo json_encode($results);


});


// GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>

        </head>
        <body>
         Yes
        </body>
    </html>
EOT;
        echo $template;
    }
);


$app->get('/books/:id', function ($id) {
    //Show book identified by $id
    echo "This is an $id";
});


// POST route
$app->post(
    '/post',
    function () {
        echo 'This is a POST route';
    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
