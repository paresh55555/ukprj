<?php

$app->get('/doorPrice', function () use ($app) {

    header("Content-Type: application/json");

    $width = $app->request()->params('width');
    $height = $app->request()->params('height');
    $panels = $app->request()->params('panels');

    $priceEngine = new PriceEngine();
    $priceEngine->width = $width;
    $priceEngine->height = $height;
    $priceEngine->panels = $panels;


    $priceEngine->getTotalPrice();

//    $this->assertEquals(3,count($priceEngine->panels));

    $result = array("cost" => $priceEngine->totalPrice);

//    $result = array ("doors" => array ($sections, $sections + 1 , $sections + 2 ) );
    echo json_encode($result);

});



$app->post('/doors', function () use ($app) {

    $json = $app->request->getBody();

    $ob = json_decode($json);
    if($ob === null) {
        notAuthorized();
    }
//    $windowOptions = json_decode($data);

    $priceEngine = new PriceEngine();

    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);


    $panelWidth = $priceEngine->panelWidth;

    if ( isUK() ) {
        $panelWidth = number_format((float) ($panelWidth), 2, '.', '');

    }
    else {
        $panelWidth = number_format((float) ($panelWidth / 25.4), 2, '.', '');
    }


    $result = array("cost" => $priceEngine->totalPrice, "panelWidth" => $panelWidth, "debug" => $priceEngine->getDebugOutput());
    header("Content-Type: application/json");
    echo json_encode($result);

});


$app->get('/doors/fixedWidth/:group', function ($group) use ($app) {

    $doors = new Door();
    $fixedWidths  = $doors->getFixedWithByGroup($group);

    header("Content-Type: application/json");
    echo json_encode($fixedWidths);
});


$app->get('/doors/fixedHeight/:group', function ($group) use ($app) {

    $doors = new Door();
    $fixedHeight  = $doors->getFixedHeightByGroup($group);

    header("Content-Type: application/json");
    echo json_encode($fixedHeight);
});

