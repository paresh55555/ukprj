<?php

require_once 'customClasses/Module.php';
require_once 'customClasses/Color.php';

$app->get('/halfButtons', function () use ($app) {


//    authBasic($app->request());

    header("Content-Type: application/json");

    $option = $app->request()->params('option');
    $db = connect_db();

    if ($option != 'track' && $option != 'frame' && $option != 'track2') {
        return;
    }

    $sql = " SELECT * from SQ_halfButtons where `type`=?  ";

    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $option);

    $stmt->execute();
    $results = $stmt->get_result();
    $halfButtons = Array();

    while ($halfButton = $results->fetch_assoc()) {
        $halfButtons[] = $halfButton;
    }
    echo json_encode($halfButtons);

});


$app->get('/halfButtons/:id', function ($id) use ($app) {


//    authBasic($app->request());

    header("Content-Type: application/json");

    $db = connect_db();


    $sql = " SELECT * from SQ_halfButtons where `halfButtonID`=?  ";

    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $id);

    $stmt->execute();
    $results = $stmt->get_result();
    $halfButtons = Array();

    $halfButton = $results->fetch_assoc();

    renderJSON($halfButton);



});

$app->get('/extendedButtons', function () use ($app) {

    authBasic($app->request());

    $module = $app->request()->params('module');
    $option = $app->request()->params('option');
    $module = intval($module);
    $db = connect_db();

    $sql = "SELECT * from optionsForExtendedButton  where module_id=? and `option`=? and active=1 order by myOrder";

    $stmt = $db->prepare($sql);

    $stmt->bind_param("is", $module, $option);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($installation = $results->fetch_assoc()) {
        $data[] = $installation;
    }
    renderJSON($data);

});


$app->get('/colors/trickle', function () use ($app) {


    authBasic($app->request());

    header("Content-Type: application/json");
    $module = $app->request()->params('module');
    $module = intval($module);

    if (!is_int($module)) {
        notAuthorized();
    }

    $db = connect_db();
    $sql = "SELECT nameOfOption  from optionsFinishTypes where module_id = ? and kind = 'trickle' order by myOrder";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $module);
    $stmt->execute();
    $results = $stmt->get_result();
    $color = $results->fetch_assoc();

    $data = array();
    if (!empty($color['nameOfOption'])) {
        $kind =$color['nameOfOption'];

        $sql = "SELECT id,nameOfOption,superSection,url,hex from optionsFinish f where f.kind= ? and active = 1 ORDER BY myOrder ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $kind);
        $stmt->execute();
        $results = $stmt->get_result();

        while ($finish = $results->fetch_assoc()) {

            $name = $finish['nameOfOption'];
            $url = $finish['url'];
            $hex = $finish['hex'];
            $id = $finish['id'];

            $row = ["id" => $id,  "nameOfOption" => $name, "url" => $url, "hex" => $hex, "type" => $kind];
            array_push($data, $row);
        }
    }

    echo json_encode($data);
});


$app->get('/colors', function () use ($app) {

    authBasic($app->request());

    header("Content-Type: application/json");
    $module = $app->request()->params('module');
    $module = intval($module);

    if (is_int($module)) {

        $color = new Color();
        $colorOptionsForModule = $color->getColorsForModule($module);
        echo json_encode($colorOptionsForModule);
    }
});

$app->get('/panels/:width', function ($width) use ($app) {

    $moduleID = $app->request()->params('module');

    $moduleObj = new Module();
    $moduleInfo =$moduleObj->getModuleByID($moduleID);

    $panels = array();

    $db = connect_db();
    $sql = "SELECT panels from panelRanges  where panelGroup = ? and low <= ? and ? <= high order by panels ";


    $stmt = $db->prepare($sql);
    $stmt->bind_param("iii", $moduleInfo['panelGroup'], $width, $width);
    $stmt->execute();

    $results = $stmt->get_result();

    while ($options = $results->fetch_assoc()) {
        $panels[] = $options['panels'];
    }

    $results = array ("panels" => $panels);

    header("Content-Type: application/json");
    echo json_encode($results);


});

$app->get('/doors', function () use ($app) {

    header("Content-Type: application/json");

    $width = $app->request()->params('width');
    $height = $app->request()->params('height');
    $parameters = $app->request()->params('parameters');
    $db = connect_db();

    if ($parameters) {
        $sql = "SELECT * from confs c  where c.type='doors' ";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $results = $stmt->get_result();
        $doorsConfs = $results->fetch_assoc();

        echo $doorsConfs['settings'];
        exit();
    }

//    echo "$width";
    $priceEngine = new PriceEngine();
    $priceEngine->width = $width;
    $priceEngine->getNumberOfPanels();
//    $this->assertEquals(3,count($priceEngine->panels));

    $result = array("doors" => $priceEngine->panels);

//    $result = array ("doors" => array ($sections, $sections + 1 , $sections + 2 ) );
    echo json_encode($result);

});


$app->get('/buttons', function () use ($app) {
    header("Content-Type: application/json");
    $db = connect_db();
    $buttonType = $app->request()->params('type');
    $typeOfButtons = explode(",", $buttonType);

    $query = "SELECT * from photos p,buttons b  where active = 1 and b.photo_id = p.id and (";
//
//    $query = 'SELECT * FROM users WHERE ';
    $bindParam = new BindParam();
    $qArray = array();

    foreach ($typeOfButtons as &$button) {
        $qArray[] = 'b.type = ?';
        $bindParam->add('s', $button);
    }

    $query .= implode(' OR ', $qArray);

    $query .= ") order by myOrder";

//   echo $query . '<br/>';

    $stmt = $db->stmt_init();
    $stmt = $db->prepare($query);
    $t1 = "test";
    $t2 = "test2";
    call_user_func_array(array($stmt, 'bind_param'), makeValuesReferenced($bindParam->get()));
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[] = $options;
    }
//    header("Content-Type: application/json");
    echo json_encode($data);
    exit();


    exit();
//call_user_func_array( array($stm, 'bind_param'), $bindParam->get());
//
//
//    /exit();

    if ($sqlType) {
        $sql = "SELECT * from buttons b,photos p  where active = 1 and " . $sqlType . " and b.photo_id = p.id order by myOrder";
//
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);
        $t1 = "test";
        $t2 = "test2";
        $stmt->bind_param("ss", $typeOfButtons);
    } else {
        $sql = "SELECT * from buttons  where active = 1 order by myOrder";
        $stmt = $db->prepare($sql);
    }
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[] = $options;
    }
//    header("Content-Type: application/json");
    echo json_encode($data);
    exit();

});


$app->get('/sections', function () use ($app) {

    $db = connect_db();

    $sql = "SELECT * from sections  where active = 1 order by myOrder ";
//    $stmt= $db->prepare($sql) ;

    $result = mysqli_query($db, $sql);
    $data = array();
    while ($options = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $sqlSub = "SELECT * from sub_sections where section_id = " . $options["id"] . " and active = 1 order by myOrder";
        $resultSub = mysqli_query($db, $sqlSub);

        $subSectionResults = array();
        while ($sub_section = mysqli_fetch_array($resultSub, MYSQLI_ASSOC)) {
            $subSectionResults[] = $sub_section;
        }

        $options["sub_sections"] = $subSectionResults;
        $data[] = $options;
    }

    header("Content-Type: application/json");
    echo json_encode($data);
    exit();

});

$app->get('/finishes', function () use ($app) {

    authBasic($app->request());

    header("Content-Type: application/json");
    $kind = $app->request()->params('kind');
    $db = connect_db();

//  $finishes = ["great" => "here"];
// echo $kind;
    $data = Array();
    if ($kind == "ral") {
        $sql = "SELECT nameOfOption,superSection,hex,ralColor from optionsFinishRal f where f.kind= ? and active = 1 ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $kind);
        $stmt->execute();
        $results = $stmt->get_result();

        while ($finish = $results->fetch_assoc()) {

//        var_dump ($finish);

            $superSection = $finish['superSection'];
            $name = $finish['nameOfOption'];
            $hex = $finish['hex'];
            $ralColor = $finish['ralColor'];

            if (!isset($data[$superSection])) {
                $data[$superSection] = [];
            }

//        $row = ["name" => $name, "hex" => $hex , "type" => $kind ];
//        $row = ["name" => $name] ;
            $row = ["colorGroupKind" => $kind, "superSection" => $superSection, "name" => $name, "hex" => $hex, "type" => $kind, "ralColor" => $ralColor];
            array_push($data[$superSection], $row);
        }
//      var_dump ($data);
//      echo json_encode($data);
    } else if ($kind == 'basicUK'  or $kind == 'trickle' or $kind == 'woodPremium' or $kind == 'woodStandard' or $kind == 'basic' or $kind == 'arch' or $kind == 'custom' or $kind == 'foilStandard' or $kind == 'foilSpecial') {
        $sql = "SELECT id,nameOfOption,superSection,url,hex from optionsFinish f where f.kind= ? and active = 1 ORDER BY myOrder ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $kind);
        $stmt->execute();
        $results = $stmt->get_result();

        while ($finish = $results->fetch_assoc()) {
            $superSection = $finish['superSection'];
            $name = $finish['nameOfOption'];
            $url = $finish['url'];
            $hex = $finish['hex'];
            $id = $finish['id'];

            if (!isset($data[$superSection])) {
                $data[$superSection] = [];
            }

            $row = ["colorGroupKind" => $kind, "superSection" => $superSection, "name" => $name, "url" => $url, "hex" => $hex, "type" => $kind];
            array_push($data[$superSection], $row);
        }
    } else if ($kind == 'vinyl') {
        $sql = "SELECT nameOfOption,superSection,hex from optionsFinish f where f.kind= ? and active = 1 ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $kind);
        $stmt->execute();
        $results = $stmt->get_result();

        while ($finish = $results->fetch_assoc()) {
            $superSection = $finish['superSection'];
            $name = $finish['nameOfOption'];
            $hex = $finish['hex'];

            if (!isset($data[$superSection])) {
                $data[$superSection] = [];
            }

            $row = ["name" => $name, "hex" => $hex, "type" => $kind];
            array_push($data[$superSection], $row);
        }
    } else if ($kind == 'superSectionRAL') {
        $sql = "SELECT name,url from ralSections  ";
//     echo $sql;
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $results = $stmt->get_result();

        while ($finish = $results->fetch_assoc()) {
//        $finish['name'] = $finish['superSection'];
//        unset($finish['superSection']);
            $data[] = $finish;

        }
    } else {
        return '';
    }

//  var_dump ($data);

//  var_dump (json_encode($data));

    echo json_encode($data);

});


$app->get('/hardware', function () use ($app) {

    authBasic($app->request());


    header("Content-Type: application/json");

    $name = $app->request()->params('name');
    $module = $app->request()->params('module');
    $module = intval($module);

    $db = connect_db();

    $sql = "SELECT  id,name,info,url from optionsForHardware  where module_id= ? and active=1 and `type` ='handle' order by myOrder ";

    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $module);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($hardwareChoice = $results->fetch_assoc()) {
        if ($name) {
            if ($name == $hardwareChoice['name']) {
                $data = $hardwareChoice;
                break;
            }
        } else {
            $data[] = $hardwareChoice;
        }
    }

    echo json_encode($data);

});

$app->get('/hardware/extra', function () use ($app) {

    authBasic($app->request());


    header("Content-Type: application/json");

    $name = $app->request()->params('name');
    $module = $app->request()->params('module');
    $module = intval($module);

    $db = connect_db();

    $sql = "SELECT  name,info,url,description from optionsForHardware  where module_id= ? and active=1 and `type` ='extra' order by myOrder ";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $module);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($hardwareChoice = $results->fetch_assoc()) {
        if ($name) {
            if ($name == $hardwareChoice['name']) {
                $data = $hardwareChoice;
                break;
            }
        } else {
            $data[] = $hardwareChoice;
        }
    }

    echo json_encode($data);

});

$app->get('/glassChoices/:id', function ($id) use ($app) {
    authBasic($app->request());

    header("Content-Type: application/json");

    $db = connect_db();

    $sql = "SELECT nameOfOption name,description,url,multiples,`default`,`group` from optionsGlass  where module_id=? and active=1 and glassOrOption='glass' order by myOrder ";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($glassChoice = $results->fetch_assoc()) {
        $data[] = $glassChoice;
    }
    echo json_encode($data);

});


$app->get('/glassOptions/:id', function ($id) use ($app) {

    authBasic($app->request());
    header("Content-Type: application/json");

    $db = connect_db();

    $sql = "SELECT nameOfOption name,description,url,multiples,`default`,`group` from optionsGlass  where module_id=? and active=1 and glassOrOption='option' order by myOrder";

    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $id);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($glassChoice = $results->fetch_assoc()) {
        $data[] = $glassChoice;
    }
    echo json_encode($data);

});

$app->get('/installation', function () use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();

    $sql = "SELECT name,description,url from optionsForInstallation  where module_id=1 and active=1 order by myOrder";
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($installation = $results->fetch_assoc()) {
        $data[] = $installation;
    }
    echo json_encode($data);

});


$app->get('/uiOptions/:group', function ($group) use ($app) {


    $options = new UIOptions();

    $results = $options->getOptionsByGroup($group);

    header("Content-Type: application/json");
    echo json_encode($results);

});
