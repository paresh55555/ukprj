<?php

$app->get('/uiSettings', function () use ($app) {

    header("Content-Type: application/json");
    $uiSettings = new UISettings();
    $results = $uiSettings->getSettings();

    renderJSON($results);

});
