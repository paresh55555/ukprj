<?php

use PipelinerSales\ApiClient\PipelinerClient;


$app->get('/salesperson', function () use ($app) {

    authLeads($app->request());
    $db = connect_db();

    $sql = "SELECT name,discount,id from users where `type` = 'salesPerson' and active = 1 and myOrder != 0 ORDER BY myOrder ";
    $stmt = $db->prepare($sql);

    $stmt->execute();
    $results = $stmt->get_result();
    $data = Array();
    while ($salesPerson = $results->fetch_assoc()) {
        $data[] = $salesPerson;
    }

    header("Content-Type: application/json");
    echo json_encode($data);

});


$app->get('/salesperson/:id', function ($id) use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();

    $sql = "SELECT * from salesPerson where id = ?";
    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $results = $stmt->get_result();
    $salesPerson = $results->fetch_assoc();
    echo json_encode($salesPerson);

});


$app->post('/salesperson', function () use ($app) {

    header("Content-Type: application/json");

    $data = $app->request->getBody();

    $salesPerson = json_decode($data);
    if (empty($salesPerson)) {
        echo "Error with salesperson";
        return;
        // do stuff
    }
    $db = connect_db();
    insertOption($db, $salesPerson, "salesPerson");

    header("Content-Type: application/json");
    echo json_encode($salesPerson);

});


$app->put('/salesperson/:id/defaults', function ($id) use ($app) {

    $realID = authSales($app->request);

    $data = $app->request->getBody();

    $salesPerson = array ('defaults' => $data);

    $defaults = json_decode($data);
    if (!isset ($defaults->Discounts) || !isset ($defaults->Totals)) {
        notAuthorized();
    }

    if (empty($salesPerson)) {
        echo "Error with Sales Perons";
        return;
        // do stuff
    }
    $db = connect_db();
    updateOptionID($db, $salesPerson, "users", $realID);

    $response = Array();
    if ($id) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
        $response['data'] = $defaults;
    }

    header("Content-Type: application/json");


    echo json_encode($response);

});


$app->get('/salesperson/:id/defaults', function ($id) use ($app) {

    $realID = authSales($app->request);

    $user = new User();

    $response = $user->getDefaults($realID);


//    $db = connect_db();
//    $sql = "SELECT defaults  from users where id=? ";
//    $stmt = $db->prepare($sql);
//
//    $stmt->bind_param("i", $realID);
//    $stmt->execute();
//    /* Fetch result to array */
//    $result = $stmt->get_result();
//    $data = $result->fetch_assoc();
//
////    $json = '{"Discounts":[{"amount":0,"id":0,"name":"Discount","showOnQuote":true,"type":"flat"}],
////    "Totals":{"code":"YZ-00","tax":{"amount":0,"showOnQuote":false},
////    "shipping":{"amount":0,"showOnQuote":false},"installation":{"amount":0,"showOnQuote":false},
////    "cost":{"amount":0,"showOnQuote":true},"preCost":10000, "finalTotal":{"showOnQuote":true},
////    "subTotal":{"showOnQuote":true},"extra":{"name":"", "amount":0, "showOnQuote":false},
////     "discountTotal":{"showOnQuote":true}}}';
////    $default = json_decode ($json);
//
//    $discounts = array ("amount" => 0, "id" => 0, "name" => "Discount", "showOnQuote" => true, "type" => "flat");
//
//    $yesOnQuote = array ("amount" => 0, "showOnQuote" => true) ;
//    $noOnQuote = array ("amount" => 0, "showOnQuote" => false) ;
//
//    $totals = array ("code" => "YZ-00", "tax" => $noOnQuote,
//        "shipping" => $noOnQuote, "installation" => $noOnQuote,
//        "cost" => $yesOnQuote, "preCost" => 10000,
//        "finalTotal" => $yesOnQuote, "subTotal" => $yesOnQuote,
//        "extra" => array("name" => "", "amount" => 0, "showOnQuote" => false),
//        "discountTotal" => $yesOnQuote );
//
//    $default = array ("Discounts" => array($discounts), "Totals" => $totals);
//
//
//    if (empty($data['defaults'])) {
//        $response = $default;
//    } else {
//        $response = $data['defaults'] ;
//    }


    header("Content-Type: application/json");
    echo json_encode($response);






});



$app->put('/salesperson/:id', function ($id) use ($app) {


    $data = $app->request->getBody();

    $salesPerson = json_decode($data);

    if (empty($salesPerson)) {
        echo "Error with Sales Perons";
        return;
        // do stuff
    }
    $db = connect_db();
    updateOption($db, $salesPerson, "salesperson", $id);

    header("Content-Type: application/json");
    echo json_encode($salesPerson);

});


