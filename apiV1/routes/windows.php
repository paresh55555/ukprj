<?php

require_once 'customClasses/Windows.php';


use customClasses\WindowSystemConfig;
use customClasses\WindowSystem;


$app->post('/windows', function () use ($app) {

    $data = $app->request->getBody();
    $size = json_decode($data);

    $window = new Windows();
    $results = $window->getTypesOfWindowsBasedOnSize($size);

    header("Content-Type: application/json");
    echo json_encode($results);


});


$app->post('/windows/price', function () use ($app) {

    $json = $app->request->getBody();

    $wsc = new WindowSystemConfig();
    $options = array();

    $config = $wsc->buildWindowSystemConfig($json);
    $windowSystem = new WindowSystem($config);

    $result = array("cost" => $windowSystem->getTotal());
    header("Content-Type: application/json");
    echo json_encode($result);
});

