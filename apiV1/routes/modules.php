<?php

$app->get('/moduleInfo', function () use ($app) {

    header("Content-Type: application/json");
    $id = $app->request()->params('module');
    $id = intval($id);


    $module = new Module();
    if($id === 0){
        $results['status'] = 'Error';
        $results['message'] = 'Empty module ID';
    }else{
        $results = $module->getModuleInfo($id);
    }


    renderJSON($results);
});


$app->get('/modules', function () use ($app) {

    authCheck($app->request());

    $db = connect_db();

    $isGuest = $app->request()->params('guest');

    if ($isGuest == 'yes') {
        $sql = "SELECT * from SQ_modules where forGuests='1' and `active` = '1' and `site`=? ORDER BY myOrder";
    } else {
        $sql = "SELECT * from SQ_modules where `active` = '1' and `site` =?  ORDER BY myOrder";
    }

    $stmt = $db->prepare($sql);
    $stmt->bind_param("s", $_SERVER['SITE_NAME']);
    $stmt->execute();

    $results = $stmt->get_result();
    $data = Array();
    while ($moduleInfo = $results->fetch_assoc()) {
        $data[] = $moduleInfo;
    }

    renderJSON($data);
});


$app->get('/modules/:id', function ($moduleID) use ($app) {

    $moduleID = intval($moduleID);
    $db = connect_db();

    $sql = "SELECT * from SQ_modules where moduleID = ?";

    $stmt = $db->prepare($sql);
    $stmt->bind_param("i", $moduleID);
    $stmt->execute();
    $results = $stmt->get_result();
    $moduleInfo = $results->fetch_assoc();

    renderJSON($moduleInfo);
});


$app->put('/modules/:id', function ($moduleID) use ($app) {

    $moduleID = intval($moduleID);

    $data = $app->request->getBody();
    $updateModule = json_decode($data);

    $db = connect_db();

    $config['idValue'] = $moduleID;
    $config['idName'] = 'moduleID';
    $config['db'] = $db;
    $config['table'] = 'SQ_modules';

    if (!empty($updateModule->tempUrl)) {
        $updateModule->url = $updateModule->tempUrl;
        unset ($updateModule->tempUrl);
    } else {
        unset ($updateModule->url);
    }
    if (isset($updateModule->undefined)) {
        unset ($updateModule->undefined);
    }

    $sql = sqlUpdateBuilderWithConfigAndData($config, $updateModule);

    if ($db->query($sql) === false) {
        throw new Exception('Database failed to Update ' . $sql);
    } else {

        $response = array("status" => 'Success');
    }

    renderJSON($response);
});



