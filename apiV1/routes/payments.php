<?php

$app->post('/makePayment', function () use ($app) {

    $dataJSON = $app->request->getBody();
    $paymentData = json_decode($dataJSON);

    $payment = new Payment();

    $status = $payment->makePayment($paymentData);

    if ($status) {
        $response['status'] = 'Success';
        $response['insertedID'] = $status;
    } else {
        $response['status'] = 'Error';

    }

    echo  renderJSON($response);
});


$app->put('/deletePayment', function () use ($app) {

    $dataJSON = $app->request->getBody();
    $paymentData = json_decode($dataJSON);
    $payment = new Payment();

    $status = $payment->delete($paymentData->id);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo  renderJSON($response);
});


$app->put('/verifiedPayment', function () use ($app) {

    $dataJSON = $app->request->getBody();
    $paymentData = json_decode($dataJSON);
    $payment = new Payment();

    $status = $payment->verified($paymentData->id);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo  renderJSON($response);
});


$app->put('/notVerifiedPayment', function () use ($app) {

    $dataJSON = $app->request->getBody();
    $paymentData = json_decode($dataJSON);
    $payment = new Payment();

    $status = $payment->notVerified($paymentData->id);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo  renderJSON($response);
});