<?php


$app->get('/orders/confirmOrders', function () use ($app) {
    header("Content-Type: application/json");
    authAccounting($app->request);


    $config = buildAccountingConfig($app);
    $config['type'] = 'Confirming Payment';

    renderProduction($config);
});


$app->get('/orders/verifyDeposit', function () use ($app) {
    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'Confirming Payment';
    $config['userType'] = $user['type'];

    renderProduction($config);
});


$app->get('/orders/readyForProduction', function () use ($app) {


    header("Content-Type: application/json");
    authAccounting($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'inProduction';

    renderProduction($config);
});


$app->get('/orders/accountingPreProduction', function () use ($app) {


    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);


    $config = buildAccountingConfig($app);
    $config['type'] = 'Ready For Production';
    $config['userType'] = $user['type'];
    renderProduction($config);

});


$app->get('/orders/accountingAll', function () use ($app) {


    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'All';
    $config['userType'] = $user['type'];
    $config['export'] = $user['type'];

    renderProduction($config);
});

$app->get('/orders/accountingNeedsSurvey', function () use ($app) {


    header("Content-Type: application/json");
    authAccounting($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'needsSurvey';

    renderProduction($config);
});

$app->get('/orders/accountingProduction', function () use ($app) {


    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'inProduction';
    $config['userType'] = $user['type'];

    renderProduction($config);
});


$app->get('/orders/accountingCompleted', function () use ($app) {

    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'Completed';
    $config['userType'] = $user['type'];

    renderProduction($config);
});


$app->get('/orders/accountingDelivered', function () use ($app) {

    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);


    $config = buildAccountingConfig($app);
    $config['type'] = 'Delivered';
    $config['userType'] = $user['type'];

    renderProduction($config);
});


$app->get('/orders/accountingHold', function () use ($app) {

    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);

    $config = buildAccountingConfig($app);
    $config['type'] = 'Hold';
    $config['userType'] = $user['type'];

    renderProduction($config);
});


//else if (tab === 'accounting' || tab === 'confirmOrders' || tab === 'accountingProduction'
//    || tab === 'balanceDue' || tab === 'paymentCompleted' || tab === 'accountingPreProduction'
//    || tab === 'accountingCompleted' || tab === 'accountingDelivered'
//    || tab === 'accountingHold' || tab == 'verifyDeposit') {

//$app->get('/orders/inProduction', function () use ($app) {
//
//
//    header("Content-Type: application/json");
//    authAccounting($app->request);
//
//    renderProduction('inProduction');
//
//});


$app->get('/orders/balanceDue', function () use ($app) {


    header("Content-Type: application/json");
    $user = authAccountingWithUser($app->request);


    $config = buildAccountingConfig($app);
    $config['type'] = 'balanceDue';
    $config['userType'] = $user['type'];
    renderProduction($config);

});

$app->get('/orders/paymentCompleted', function () use ($app) {


    header("Content-Type: application/json");
    authAccounting($app->request);
    $config = buildAccountingConfig($app);
    $config['type'] = 'paymentCompleted';
    renderProduction($config);

});


$app->get('/orders/accounting', function () use ($app) {


    header("Content-Type: application/json");
    authAccounting($app->request);

    renderProduction(null);

});


$app->get('/orders/preProduction', function () use ($app) {

    displayProductionOrders($app, 'Ready For Production');

});

$app->get('/orders/needsSurvey', function () use ($app) {

    displayProductionOrders($app, 'Needs Survey');

});

$app->get('/orders/viewAllSurvey', function () use ($app) {

    displayProductionOrders($app, 'viewAllSurvey');

});

$app->get('/orders/inProduction', function () use ($app) {

    displayProductionOrders($app, 'In Production');

});

$app->get('/orders/completed', function () use ($app) {

    displayProductionOrders($app, 'Completed');

});

$app->get('/orders/hold', function () use ($app) {

    displayProductionOrders($app, 'Hold');

});

$app->get('/orders/delivered', function () use ($app) {

    displayProductionOrders($app, 'Delivered');

});

$app->get('/orders/viewAll', function () use ($app) {

    displayProductionOrders($app, 'viewAll');

});

$app->get('/orders', function () use ($app) {

    $pageLimit = getPageAndLimitWithRequest($app->request);
    $superToken = validSuperToken($app->request()->params('superToken'));

    $salesPerson = $app->request()->params('salesPerson');
    if (Is_int($salesPerson) || $salesPerson < 1) {
        return;
    }

    $fullStatus = $app->request()->params('fullStatus');
    $status = $app->request()->params('status');
    if (empty($status)) {
        $status = null;
    }

    $order = validQuoteColumn($app->request()->params('order'));
    $direction = validDirection($app->request()->params('direction'));

    $search = $app->request()->params('search');

    $readID = authSales($app->request());


    $quote = new Quote($pageLimit);

    $config = array("salesPersonID" => $readID, "status" => $status,
        "order" => $order, "direction" => $direction, "search" => $search, "fullStatus" => $fullStatus,
        "superToken" => $superToken);

    $results = $quote->getAllOrdersWithConfig($config);

    header("Content-Type: application/json");
    header("Total: ".$results['total']);
    echo json_encode($results['data']);
});


$app->get('/invoice/:id', function ($id) use ($app) {

    $user = authCheck($app->request());

    $quote = new Quote();
    $data = $quote->getQuoteAndCustomer($id);

    $glass = new Glass();

    $data['Cart'] = $glass->addGlassSizeToCart($data['Cart']);

    echo json_encode($data);
});


$app->get('/orders/:id', function ($id) use ($app) {

    header("Content-Type: application/json");
    $salesPerson = $app->request()->params('salesPerson');
    $production = $app->request()->params('production');
    $accounting = $app->request()->params('accounting');
    $superToken = validSuperToken($app->request()->params('superToken'));
    $getQuote = array('quote' => $id);

    if (!empty($production)) {
        authProduction($app->request);
        $getQuote['production'] = 'yes';

    } elseif (!empty($accounting)) {
        authAccounting($app->request);
        $getQuote['accounting'] = 'yes';
    } else {
        authSales($app->request());
        $getQuote['salesPerson'] = $salesPerson;
    }
    $getQuote['superToken'] = $superToken;

    $quote = new Quote();
    $user = new User();
    $data = $quote->getSalesQuote($getQuote);
    if (isset($data['SalesPerson'])) {
        $userData = $user->getActiveUser($data['SalesPerson']);
        $data['SalesPersonInfo'] = array(
            'name' => $userData['name'],
            'email' => $userData['email'],
            'phone' => $userData['phone'],
        );
    }

    echo json_encode($data);


});


$app->get('/orders/:id/production', function ($id) use ($app) {


    header("Content-Type: application/json");

    authProduction($app->request());

    $getQuote = array('quote' => $id);

    $quote = new Quote();
    $data = $quote->getProductionOrder($getQuote);

    echo json_encode($data);


});


$app->put('/orders/:id/woodOrdered', function ($id) use ($app) {


    header("Content-Type: application/json");
    authProduction($app->request());

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
        // do stuff
    }

    $data = json_decode($json);

    $quote = new Quote();
    $status = $quote->woodOrdered($data);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/orders/:id/glassOrdered', function ($id) use ($app) {


    header("Content-Type: application/json");
    authProduction($app->request());

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
        // do stuff
    }

    $data = json_decode($json);

    $quote = new Quote();
    $status = $quote->glassOrdered($data);
    $status = $quote->glassOrdered($data);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/orders/:id/productionNotes', function ($id) use ($app) {


    header("Content-Type: application/json");
    authProduction($app->request());

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
        // do stuff
    }

    $data = json_decode($json);


    $quote = new Quote();
    $status = $quote->productionNotes($data);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});

function renderProductionCashDue($config)
{


}

function renderProduction($config)
{

    $offsetLimit = '';
    if (is_array($config)) {
        $direction = $config['direction'];
        $order = $config['order'];
        $search = $config['search'];
        $type = $config['type'];
        $pageLimit = $config['pageLimit'];
        $offsetLimit = buildOffSetLimitWithPageLimit($pageLimit['page'], $pageLimit['limit']);
    } else {
        $type = $config;
    }

    $db = connect_db();


    $select = 'SELECT orderName, delivered, paidInFull, version, quotesSales.id, companyName, SalesDiscount, prefix,
            postfix,  quotesSales.status, lastName, firstName, phone, email,  quantity, total, orderDate, dueDate,
            SQ_addresses.billing_city as city1,
            SQ_addresses.shipping_city as city2, 
            customer.billingCity as city3, 
            SQ_addresses.billing_state as state1,
            SQ_addresses.shipping_state as state2, 
            customer.billingState as state3, 
            `type`,  depositDue, balanceDue from quotesSales
            LEFT JOIN customer ON Customer=customer.id
            LEFT JOIN SQ_payments ON quotesSales.id=SQ_payments.quoteID
            LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id ';

    $whereCore = " where quotesSales.status !='deleted'  ";

    if ($type == 'paymentCompleted') {
        $wherePlus = " and `paidInFull`='yes'  ";

    } else if ($type == 'balanceDue') {
        $wherePlus = " and `paidInFull`!='yes' and (`type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

    } else if ($type == 'inProduction') {
        $wherePlus = " and (`type`='In Production') ";

    } else if ($type == 'needsSurvey') {
        $wherePlus = " and (`type`='Needs Survey') ";
    } else if ($type == 'verifyDeposit') {
        $wherePlus = " and ( `type`='Ready For Production') ";
    } else if ($type == 'Hold') {
        $wherePlus = " and quotesSales.status='Hold' and  ( `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";
    } else if ($type == 'All') {
        $wherePlus = " and  ( `type` = 'Needs Survey' or  `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

    } else {
        $wherePlus = " and `type`='$type' ";
    }

    $sqlSearch = " and ( (SQ_payments.kind like ? && SQ_payments.status = 1)  OR SQ_payments.amount like ? OR lastName like ? OR firstName like ? OR phone like ? OR `total` like ? OR companyName like ? OR quotesSales.orderName like ? OR SQ_addresses.billing_email like ? OR  SQ_addresses.shipping_email like ? OR  customer.email like ? OR quotesSales.orderName like ? ) ";


    $sql = $select.$whereCore.$wherePlus.$sqlSearch." GROUP BY (quotesSales.id) ORDER BY quotesSales.id DESC ";
    $stmt = $db->prepare($sql);

    if (empty($search)) {
        $sqlSearch = "%%";
    } else {
        $sqlSearch = "%$search%";
    }
    $stmt->bind_param("ssssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);

    $stmt->execute();
    $stmt->store_result();
    $count = $stmt->num_rows;

    $sql = $sql.$offsetLimit;

    $stmt = $db->prepare($sql);
    $stmt->bind_param("ssssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
    $stmt->execute();

    $results = $stmt->get_result();
    $data = Array();
    setlocale(LC_MONETARY, 'en_US.utf8');

    $payment = new Payment();

    while ($options = $results->fetch_assoc()) {

        $options['number'] = $options['id'];
        $options['SalesDiscount'] = json_decode($options['SalesDiscount']);

        $payments = $payment->getPayments($options['id']);
        $options['SalesDiscount']->Payments = $payments;

        $orderDate = strtotime($options['orderDate']);
        $dueDate = strtotime($options['dueDate']);

        $options['orderDate'] = date('m/d/Y', $orderDate);
        $options['dueDate'] = date('m/d/Y', $dueDate);

        $delivered = strtotime($options['delivered']);
        $options['delivered'] = date('m/d/Y', $delivered);

        $data[] = $options;
    }

    $results = array("total" => $count, "data" => $data);

    if (!empty($config['userType'])) {

        if ($config['userType'] == 'audit' && !isMagnaline()  ) {

            $config['totalFromPD'] = $count;
            $config['returnedFromPD'] = count($data);

            $config['db'] = 'ces';
            $resultsFromCES = renderProductionDouble($config);
            $results['total'] = $results['total'] + $resultsFromCES['total'];
            $results['data'] = array_merge($results['data'], $resultsFromCES['data']);

        }
    }

    echo json_encode($results);
}


function specialOffSetLimitWithPageLimit($config )
{
    $pageLimit = $config['pageLimit'];
    $page = $pageLimit['page'];
    $limit = $pageLimit['limit'];

    $needFromCES = $limit - $config['returnedFromPD'];

    if ($limit == 'All') {
        return '';
    }

    if ($needFromCES == 0) {
        $pageLimit = "LIMIT 0";
    }
    else if ($needFromCES < $limit ) {
        $pageLimit = "LIMIT $needFromCES OFFSET 0";
    }
    else {
        $pdPage = ceil ( $config['totalFromPD']/ $limit) ;
        $cesPage = $page - $pdPage;
        $bonus = $pdPage * $limit - $config['totalFromPD'];
        $offset = ($cesPage - 1) * $limit + $bonus;
        $pageLimit = "LIMIT $limit OFFSET $offset";
    }

    return $pageLimit;
}
function renderProductionDouble($config)
{

    $offsetLimit = '';
    if (is_array($config)) {
        $direction = $config['direction'];
        $order = $config['order'];
        $search = $config['search'];
        $type = $config['type'];

        $offsetLimit = specialOffSetLimitWithPageLimit($config);
    } else {
        $type = $config;
    }

    $db = connect_db($config['db']);


    $select = 'SELECT "ces", delivered, paidInFull, version, quotesSales.id, companyName, SalesDiscount, prefix,
            postfix,  quotesSales.status, lastName, firstName, phone, email,  quantity, total, orderDate, dueDate,
            SQ_addresses.billing_city as city1,
            SQ_addresses.shipping_city as city2, 
            customer.billingCity as city3, 
            SQ_addresses.billing_state as state1,
            SQ_addresses.shipping_state as state2, 
            customer.billingState as state3, 
            `type`,  depositDue, balanceDue from quotesSales
            LEFT JOIN customer ON Customer=customer.id
            LEFT JOIN SQ_payments ON quotesSales.id=SQ_payments.quoteID
            LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id ';

    $whereCore = " where quotesSales.status !='deleted'  ";

    if ($type == 'paymentCompleted') {
        $wherePlus = " and `paidInFull`='yes'  ";

    } else if ($type == 'balanceDue') {
        $wherePlus = " and `paidInFull`!='yes' and (`type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

    } else if ($type == 'inProduction') {
        $wherePlus = " and (`type`='In Production') ";

    } else if ($type == 'needsSurvey') {
        $wherePlus = " and (`type`='Needs Survey') ";
    } else if ($type == 'verifyDeposit') {
        $wherePlus = " and ( `type`='Ready For Production') ";
    } else if ($type == 'Hold') {
        $wherePlus = " and quotesSales.status='Hold' and  ( `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";
    } else if ($type == 'All') {
        $wherePlus = " and  ( `type` = 'Needs Survey' or  `type` = 'Confirming Payment' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered')  ";

    } else {
        $wherePlus = " and `type`='$type' ";
    }

    $sqlSearch = " and ( (SQ_payments.kind like ? && SQ_payments.status = 1)  OR SQ_payments.amount like ? OR lastName like ? OR firstName like ? OR phone like ? OR `total` like ? OR companyName like ? OR quotesSales.id like ? OR SQ_addresses.billing_email like ? OR  SQ_addresses.shipping_email like ? OR  customer.email like ? OR concat_ws('',quotesSales.prefix,quotesSales.id) like ? ) ";


    $sql = $select.$whereCore.$wherePlus.$sqlSearch." GROUP BY (quotesSales.id) ORDER BY quotesSales.id DESC ";
    $stmt = $db->prepare($sql);

    if (empty($search)) {
        $sqlSearch = "%%";
    } else {
        $sqlSearch = "%$search%";
    }
    $stmt->bind_param("ssssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);

    $stmt->execute();
    $stmt->store_result();
    $count = $stmt->num_rows;

    $sql = $sql.$offsetLimit;

    $stmt = $db->prepare($sql);
    $stmt->bind_param("ssssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
    $stmt->execute();

    $results = $stmt->get_result();
    $data = Array();
    setlocale(LC_MONETARY, 'en_US.utf8');

    $payment = new Payment();

    while ($options = $results->fetch_assoc()) {

        $options['number'] = $options['id'];
        $options['SalesDiscount'] = json_decode($options['SalesDiscount']);

        $payments = $payment->getPaymentsSpecial($options['id'],$config['db']);
        $options['SalesDiscount']->Payments = $payments;

        $orderDate = strtotime($options['orderDate']);
        $dueDate = strtotime($options['dueDate']);
        $options['orderDate'] = date('m/d/Y', $orderDate);
        $options['dueDate'] = date('m/d/Y', $dueDate);

        $delivered = strtotime($options['delivered']);
        $options['delivered'] = date('m/d/Y', $delivered);
        $data[] = $options;
    }


    $results = array("total" => $count, "data" => $data);

    return $results;
}
