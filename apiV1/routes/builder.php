<?php

$app->get('/builder/users', function () use ($app) {

    authUserWithTypeAndRequest('builder',$app->request);

    $results = array();
    echo renderJSON($results);

});


$app->get('/builder/frontEndAssets', function () use ($app) {

    $builder = new Builder();

    $results = $builder->getFrontEndAssets();

    echo renderJSON($results);

});


$app->get('/builder/frontEndInteractions', function () use ($app) {

    $builder = new Builder();

    $results = $builder->getFrontEndInteractions();

    echo renderJSON($results);

});

$app->get('/builder/backEndAssets', function () use ($app) {

    $builder = new Builder();

    $results = $builder->getBackEndAssets();

    echo renderJSON($results);

});

$app->get('/builder/backEndInteractions', function () use ($app) {

    $builder = new Builder();

    $results = $builder->getBackEndInteractions();

    echo renderJSON($results);

});


