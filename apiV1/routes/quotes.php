<?php

$app->delete('/quotes/:id', function ($id) use ($app) {

    $id = intval($id);

    $salesPerson = intval($app->request()->params('salesPerson'));
    $realSalesPerson = authSales($app->request);
    $superToken = validSuperToken($app->request()->params('superToken'));

    if ($realSalesPerson != $salesPerson && empty($superToken)) {
        notAuthorized();
    }
    if ($salesPerson < 1) {
        notAuthorized();
    }

    $db = connect_db();

    if (empty($superToken)) {
        $sql = "UPDATE quotesSales set status='deleted' where SalesPerson= ? and id= ? ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $realSalesPerson, $id);
    } else {
        $sql = "UPDATE quotesSales set status='deleted' where id=? ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);
    }

    if (!$stmt->execute()) {
        trigger_error('Wrong SQL: '.$sql.' Error: '.$db->error, E_USER_ERROR);
    }

    $response = Array();
    $response['success'] = "true";

    echo json_encode($response);

});


$app->delete('/quotes/:id/production', function ($id) use ($app) {

    authProduction($app->request);

    $quote = new Quote();
    $status = $quote->deleteQuote($id);

    $response = array("status" => $status);

    echo json_encode($response);

});


function validQuoteColumn($column)
{

    $validColumns = array('id', 'lastName', 'firstName', 'companyName', 'phone',
        'quantity', 'total', 'followUp', 'secondFollowUp', 'leadQuality', 'status',
        'balanceDue', 'dueDate', 'estComplete', 'depositDue', 'billingZip');

    if (in_array($column, $validColumns)) {
        return $column;
    } else {
        return 'id';
    }
}


function validSuperToken($superToken)
{
    $auth = new Auth();
    $user = $auth->validateSuperToken($superToken);

    if (!empty($user)) {
        return "$superToken";
    } else {
        return null;
    }
}


function validDirection($direction)
{
    if ($direction == 'desc') {
        return 'DESC';
    } else {
        return 'ASC';
    }

}


$app->get('/quotes', function () use ($app) {

    $production = $app->request()->params('production');
    $salesPerson = $app->request()->params('salesPerson');
    $search = $app->request()->params('search');
    $order = validQuoteColumn($app->request()->params('order'));
    $direction = validDirection($app->request()->params('direction'));
    $superToken = validSuperToken($app->request()->params('superToken'));

    $pageLimit = getPageAndLimitWithRequest($app->request);

    if (Is_int($salesPerson) || $salesPerson < 1) {
        return;
    }

    $leadQuality = $app->request()->params('lead');
    if (empty($leadQuality)) {
        $leadQuality = null;
    }
    if ($production == 'yes') {
        authProduction($app->request());
        $realID = $salesPerson;
    } else {
        $realID = authSales($app->request());
    }

    $quote = new Quote($pageLimit);

    $config = array("salesPersonID" => $realID, "leadQuality" => $leadQuality,
        "order" => $order, "direction" => $direction, "search" => $search, "superToken" => $superToken);

    $results = $quote->getAllSaleQuotesWithConfig($config);


    header("Content-Type: application/json");
    header("Total: ".$results['total']);
    echo json_encode($results['data']);
});


$app->put('/quotes/:id/lead', function ($id) use ($app) {


    $leadsManager = $app->request()->params('leads');
    if (Is_int($leadsManager) || $leadsManager < 1) {
        return;
    }
    authLeads($app->request());
    $leadsObj = new Lead();

    $status = $leadsObj->revokeLead($id);
    $response = array("status" => $status);

    header("Content-Type: application/json");
    echo json_encode($response);

});


function downLoadedFileNameFromIdAndOrder($id, $order)
{

    $fileName = 'Order-'.$id.'.pdf';
    if ($order == 'Quote') {
        $fileName = 'Quote-'.$id.'.pdf';
    }
    if ($order == 'invoice') {
        $fileName = 'Order-'.$id.'.pdf';
    }

    return $fileName;

}

$app->get('/quotePrint/:id', function ($id) use ($app) {


    $site = $app->request()->params('site');
    $debug = $app->request()->params('debug');
    $manifest = $app->request()->params('manifest');
    $purchaseCert = $app->request()->params('purchaseCert');
    $superToken = validSuperToken($app->request()->params('superToken'));

    $_SERVER['site'] = $site;

    $production = $app->request()->params('production');

    if ($production == "yes") {
        authProduction($app->request);
        $quote = new Quote();
        $quoteData = $quote->getQuote($id);
        $salesPerson = $quoteData['SalesPerson'];

    } else {
        $salesPerson = authSales($app->request);
    }

    $superTokenURI = '';
    if (!empty($superToken)) {
        $superTokenURI = "&superToken=$superToken";
    }

    $token = $app->request()->params('token');
    $order = $app->request()->params('order');

    $user = new User();
    $userInfo = $user->getUser($salesPerson);

    $location = getLocation();

    $url = "https://".$_SERVER['SERVER_NAME']."/?quote=".$id."&tab=viewQuote&print=yes&prefix=".$userInfo['prefix']."&salesPersonName=".
        $userInfo['name']."&salesPerson=".$salesPerson."&token=".$token."&order=".$order.
        "&location=".$location."&production=".$production."&site=".$_SERVER['site']."&manifest=".$manifest.$superTokenURI."&purchaseCert=".$purchaseCert;

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $fileName = 'Quote-'.$id.'.pdf';
    if ($order == 'yes') {
        $fileName = 'Order-'.$id.'.pdf';
    }

//    exec("/usr/local/bin/wkhtmltopdf.sh  --page-size 'letter' '" . $url . "' $myFile");
//    $app->response->headers->set('Content-Type', 'application/pdf');
//    $app->response->headers->set('Pragma', "public");
//    $app->response->headers->set('Content-disposition:', 'attachment; filename='.$fileName);
//    $app->response->headers->set('Content-Transfer-Encoding', 'binary');
//    $app->response->headers->set('Content-Length', filesize($fileName));
//
//    $openFile = fopen($fileName, "r") or die("Unable to open file!");
//    echo fread($openFile, filesize($fileName));
//    fclose($openFile);

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = '/tmp/'.$fileName;
    $config['fileName'] = $fileName;
    if ($purchaseCert == 'yes') {
        $config['fileName'] = 'Certificate_of_Purchase_'.$fileName ;
    }

    writePhantomjsPDF($config);
    displayPDF($config);
});

$app->get('/quotes/:id', function ($id) use ($app) {

    $salesPerson = $app->request()->params('salesPerson');
    $production = $app->request()->params('production');
    $accounting = $app->request()->params('accounting');
    $print = $app->request()->params('print');
    $superToken = validSuperToken($app->request()->params('superToken'));

    if ($production == 'yes') {
        authProduction($app->request());
    } elseif ($accounting == 'yes') {
        authAccounting($app->request());
    } else {
        authSales($app->request());
    }

    $getQuote = array('quote' => $id);
    $getQuote['salesPerson'] = $salesPerson;
    $getQuote['superToken'] = $superToken;
    $quote = new Quote();
    $data = $quote->getSalesQuote($getQuote);


    if ($print == 'yes') {
        $user = new User();
        $dealer = $user->getUsersDealers($salesPerson);
        $data['dealer'] = $dealer;
    }
    header("Content-Type: application/json");


    echo json_encode($data);

});


$app->get('/quotes/:id/convertToOrder', function ($id) use ($app) {


    $salesPerson = $app->request()->params('salesPerson');
    $realID = authSales($app->request());

    $getQuote = array('quote' => $id);
    $getQuote['salesPerson'] = $salesPerson;

    $quote = new Quote();
    $status = $quote->convertToOrderForSalesPerson($id, $realID);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});

$app->get('/quotes/:id/sendToConfirmPayment', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {
        authCheck($app->request());

        $quote = new Quote();
        $status = $quote->sendToConfirmPayment($id);

        if ($status) {
            $response['id'] = $id;
            $response['status'] = 'Success';
        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }

});

$app->get('/quotes/:id/sendToNeedsSurvey', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {
        authAccounting($app->request());

        $quote = new Quote();
        $module = new Module();
        $user = new User();

        $customer = new Customer();
        $quoteData = $quote->getQuote($id);

        $cart = json_decode($quoteData['Cart'], true);

        $moduleData = array('needs_survey' => 0);

        if (!empty($cart[0]) and !empty($cart[0]['module'])) {
            $moduleData = $module->getModuleByID($cart[0]['module']);
        }

        $customerData = $customer->getCustomer($quoteData['Customer']);

        $userData = $user->getActiveUser($quoteData['SalesPerson']);

        $site = new Site();
        $siteData = $site->getSiteForLocation($_SERVER['SITE_NAME']);

        if (!empty($moduleData['needs_survey'])) {
            $status = $quote->sendToNeedsSurvey($id);
        }

        if (empty($moduleData['needs_survey'])) {
            $status = $quote->sendToProduction($id);
        }


        if ($status) {

            $response['id'] = $id;
            $response['status'] = 'Success';

            if ($siteData['salesFlow'] == 2) {

//                $quote->inProduction($id);
                $quote->setOrderDateNow($id);
                $quote->setProductionCompleteDate($id, $quoteData);
                $quote->addInvoiceNumber($id);
                $response['email'] = $customerData[0]['email'];
            }

            // Email to the Customer
            $message = file_get_contents(getcwd().'/mailTemplates/depositConfirmed.tpl');
            $imagesUrl = "https://".$_SERVER['SERVER_NAME'].'/images';
            $replace = array(
                '{customerName}',
                '{jobId}',
                '{imagesUrl}',
                '{compDate}',
                '{salesName}',
                '{salesEmail}',
                '{salesPhone}',
            );

            $jobId = $quoteData['prefix'].$quoteData['id'];
            $customerName = $customerData[0]['firstName'].' '.$customerData[0]['lastName'];

            $with = array(
                $customerName,
                $jobId,
                $imagesUrl,
                $quote->mysqlToHumanDate($quoteData['dueDate']),
                $userData['name'],
                $userData['email'],
                $userData['phone'],
            );

            $message = str_replace($replace, $with, $message);
            $emailInfo = (object) array(
                "address" => getCorrectCustomerEmail($customerData[0]),
                "from" => 'no-reply@panoramicdoors.com',
                "subject" => "THANK YOU FOR YOUR PURCHASE",
                "message" => $message,
            );

            //This check is EXTREMELY important and must not be removed. Fracka 1/10/18
            if (!isUK()) {
                sendEmailViaSendGrid($emailInfo);
            }
        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }
});

$app->put('/quotes/:id/revertToQuote', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);
        return;

    }

    authAccounting($app->request());

    $quote = new Quote();
    $status = $quote->sendToQuote($id);
    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }
    header("Content-Type: application/json");
    echo json_encode($response);

});

$app->get('/quotes/:id/sendToProduction', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {
        authAccounting($app->request());

        $quote = new Quote();
        $customer = new Customer();
        $quoteData = $quote->getQuote($id);

        $customerData = $customer->getCustomer($quoteData['Customer']);

        $site = new Site();
        $siteData = $site->getSiteForLocation($_SERVER['SITE_NAME']);
        $status = $quote->sendToProduction($id);

        if ($status) {

            $response['id'] = $id;
            $response['status'] = 'Success';

            if ($siteData['salesFlow'] == 2) {

//                $quote->inProduction($id);
                $quote->setOrderDateNow($id);
                $quote->setProductionCompleteDate($id, $quoteData);
                $quote->addInvoiceNumber($id);
                $response['email'] = $customerData[0]['email'];
            }
        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }
});


$app->get('/quotes/:id/unconfirmFullPayment', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {
        authAccounting($app->request());

        $quote = new Quote();
        $status = $quote->unconfirmFullPayment($id);
        if ($status) {
            $response['id'] = $id;
            $response['status'] = 'Success';

        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }

});

$app->get('/quotes/:id/confirmFullPayment', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {
        authAccounting($app->request());

        $quote = new Quote();
        $customer = new Customer();
        $user = new User();
        $status = $quote->confirmFullPayment($id);

        $quoteData = $quote->getQuote($id);
        $userData = $user->getActiveUser($quoteData['SalesPerson']);
        $customerData = $customer->getCustomer($quoteData['Customer']);

        if ($status) {
            $response['id'] = $id;
            $response['status'] = 'Success';

            $orderNumber = $quoteData ? $quoteData['prefix'].$quoteData['quote_id'] : '';
            $salesEmail = $userData ? $userData['email'] : 'no-reply@salesquoter.com';
            sendFullPaymentConfirmationEmail(
                getCorrectCustomerEmail($customerData[0]),
                $orderNumber,
                $salesEmail
            );
            if (!isProduction()) {
                // Email to the Customer
                $doorReadyFile = getcwd().'/mailTemplates/doorReady.pdf';
                $installTipsFile = getcwd().'/mailTemplates/installTips.pdf';

                $doorReadyInfo = (object) array(
                    "address" => getCorrectCustomerEmail($customerData[0]),
                    "from" => 'no-reply@salesquoter.com',
                    "subject" => "CONGRATULATIONS YOUR DOOR IS READY",
                    "message" => "Please See Attached PDF",
                    "file" => $doorReadyFile
                );
                $installTipsInfo = (object) array(
                    "address" => getCorrectCustomerEmail($customerData[0]),
                    "from" => 'no-reply@salesquoter.com',
                    "subject" => "Installation Tips",
                    "message" => "Please See Attached PDF",
                    "file" => $installTipsFile
                );

                //This check is EXTREMELY important and must not be removed. Fracka 1/10/18
                if (isUK()) {
                    //  These are to remain commented out -- Fracka 1/10/18
                    //    sendEmailViaSendGrid($doorReadyInfo);
                    //  sendEmailViaSendGrid($installTipsInfo);
                }

            }
        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }

});

$app->post('/quotes/:id/confirmFullPayment', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {

        authAccounting($app->request());

        $dataJSON = $app->request->getBody();
        $postData = json_decode($dataJSON);

        $config['id'] = $postData->id;
        $config['token'] = $postData->token;
        $config['site'] = $postData->site;
        $config['location'] = getLocation();

        $url = invoiceURL($config);
        $fileNamePath = "/tmp/Invoice-".$postData->id.'.pdf';

        $configPDF['url'] = $url;
        $configPDF['fileNamePath'] = $fileNamePath;

        $site = new Site();
        $siteData = $site->getSiteForLocation($_SERVER['SITE_NAME']);

        $configEmail = (object) array(
            "address" => $postData->email,
            "from" => $siteData['salesEmail'],
            "subject" => "Paid in Full",
            "message" => " ",
            "file" => $configPDF['fileNamePath']);

        $quote = new Quote();
        $user = new User();
        $quoteData = $quote->getQuote($id);
        $userData = $user->getActiveUser($quoteData['SalesPerson']);

        $status = $quote->confirmFullPayment($id);

        $orderNumber = $quoteData ? $quoteData['prefix'].$quoteData['quote_id'] : '';
        $salesEmail = $userData ? $userData['email'] : 'no-reply@salesquoter.com';
        sendFullPaymentConfirmationEmail(
            getCorrectCustomerEmail(array('email'=>$postData->email)),
            $orderNumber,
            $salesEmail
        );

        $email = new Email();
        $email->emailInvoice($configPDF, $configEmail);

        if ($status) {
            $response['id'] = $id;
            $response['status'] = 'Success';
        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }

});


$app->get('/quotes/:id/revertPaidInFull', function ($id) use ($app) {

    if (empty($id) || $id == 'undefined') {
        $response['status'] = 'Error';
        $response['message'] = "Id not passed";
        header("Content-Type: application/json");
        echo json_encode($response);

    } else {

        authAccounting($app->request());

        $quote = new Quote();
        $status = $quote->revertPaidInFull($id);

        if ($status) {
            $response['id'] = $id;
            $response['status'] = 'Success';
        } else {
            $response['status'] = 'Error';

        }
        header("Content-Type: application/json");
        echo json_encode($response);
    }

});


$app->get('/quotes/:id/convertToProduction', function ($id) use ($app) {

    $salesPerson = $app->request()->params('salesPerson');
    $realID = authSales($app->request());

    $getQuote = array('quote' => $id);
    $getQuote['salesPerson'] = $salesPerson;

    $quote = new Quote();
    $status = $quote->convertToProduction($id, $realID);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);
});


$app->get('/quotes/:id/convertToPendingProduction', function ($id) use ($app) {


    $salesPerson = $app->request()->params('salesPerson');
    $realID = authSales($app->request());

    $quoteObj = new Quote();
    $quote = $quoteObj->getQuote($id);

    $superToken = validSuperToken($app->request()->params('superToken'));
    if ($superToken) {
        $realID = $quote['SalesPerson'];
    } else {
        if ($salesPerson != $quote['SalesPerson']) {
            notAuthorized();
        }
    }


    $quote = new Quote();
    $status = $quote->convertToPendingProduction($id, $realID);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});


$app->get('/quotes/:id/inProduction', function ($id) use ($app) {

    authProduction($app->request());

    $quote = new Quote();
    $status = $quote->inProduction($id);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});

$app->get('/quotes/:id/preProduction', function ($id) use ($app) {


    authProduction($app->request());

    $quote = new Quote();
    $status = $quote->readyForProduction($id);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});

$app->get('/quotes/:id/needsSurvey', function ($id) use ($app) {


    authProduction($app->request());

    $quote = new Quote();
    $status = $quote->needsSurvey($id);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});

$app->get('/quotes/:id/completed', function ($id) use ($app) {


    authProduction($app->request());

    $quote = new Quote();
    $status = $quote->completed($id);

    $quoteData = $quote->getQuoteAndCustomer($id);

    $doors = array();
    $complete_date = date("Y-m-d");
    $warranty_end_date =  date('m/d/Y', strtotime('+10 years'));

//    print_r ($warranty_end_date);
//    exit();
//    print_r($quoteData['Cart']);

    foreach ($quoteData['Cart'] as $orgDoor) {

        $door = array(
            "warranty_end_date" => $warranty_end_date,
            "door_type" => $orgDoor->moduleTitle,
        );

        $door['traits'] =  $quote->buildTraits($orgDoor);

        $doors[] = $door;
    }


    $first_name ='';
    $last_name = '';

    $contact  = preg_split("/\s+/", $quoteData['address']['billing_contact']);
    if (!empty($contact[0])) {
        $first_name = $contact[0];
    }
    if (!empty($contact[1])) {
        $last_name = $contact[1];
    }


    $data = array(
        "order_number" => $quoteData['orderName'],
        "site" => "Panoramic",
        "complete_date" => $complete_date,
        "first_name" => $first_name,
        "last_name" => $last_name,
        "email" =>  $quoteData['address']['billing_email'],
        "phone" => $quoteData['address']['billing_phone'],
        "address_1" => $quoteData['address']['billing_address1'],
        "address_2" => $quoteData['address']['billing_address2'],
        "city" => $quoteData['address']['billing_city'],
        "state" => $quoteData['address']['billing_state'],
        "zipcode" => $quoteData['address']['billing_zip'],
        "doors" => $doors
    );

//    $dataJSON  = '{"order_number": "FFF1234", "site":"Panoramic", "complete_date":"12-10-2018",
//     "first_name": "niloy", "last_name": "B", "email": "niloy.cste@gmail.com", "phone": "+8801964405239",
//     "address_1":"test", "address_2":"test", "city":"test", "state":"test", "zipcode": "10222",
//
//
//     "doors": [{ "warranty_end_date": "12-07-2018", "door_type": "test", "traits": [{ "id": "0", "name": "color", "trait_value": "black" }]}]}';
//


    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
        $response['data'] = $data;
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});

$app->get('/quotes/:id/delivered', function ($id) use ($app) {


    authProduction($app->request());

    $quote = new Quote();
    $status = $quote->delivered($id);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});


$app->get('/quotes/:id/onHold', function ($id) use ($app) {


    authProduction($app->request());

    $quote = new Quote();
    $status = $quote->onHold($id);

    if ($status) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';

    }
    header("Content-Type: application/json");
    echo json_encode($response);


});


$app->put('/quotes/:id/updateHoldStatus', function ($id) use ($app) {

    header("Content-Type: application/json");

    $user = authCheck($app->request());

    $realID = '0';
    if ($user['type'] == 'production' || $user['type'] == 'productionLimited') {
        $superToken = "Yes";

    } else {
        $realID = authSales($app->request());
        $superToken = validSuperToken($app->request()->params('superToken'));
    }


    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
    }

    $data = json_decode($json);

    $quote = new Quote();
    $status = $quote->updateHoldStatus($data, $realID, $id, $superToken);
    if ($data == 'hold') {
        $data = 'holdOrder';
        $quote->sendHoldEmail($id);
    }
    $status = $quote->leadQualityUpdateBySalesPersonAndId($data, $realID, $id, $superToken);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->get('/quotes/:id/guest', function ($id) use ($app) {

    $id = intval($id);
    header("Content-Type: application/json");
    $salesPerson = $app->request()->params('salesPerson');

    authCheck($app->request());

    $quote = new Quote();
    $data = $quote->getGuestQuote($id);
    $user = new User();
    $userData = $user->getActiveUser($data['SalesPerson']);
    if($userData){
        $data['phone'] = $userData['phone'];
    }

    echo json_encode($data);


});


$app->post('/quotes', function () use ($app) {

    header("Content-Type: application/json");
    $salesPersonID = authSales($app->request());
    $admin = $app->request->params('admin');
    $data = $app->request->getBody();

    $newQuote = json_decode($data);

    $newQuote->SalesPerson = $salesPersonID;

    if (empty($newQuote)) {
        echo "Error with Quote Data";
        return;
    }

    if (empty($newQuote->Cart)) {
        echo "Error: No cart in Quote";
        return;
    }

    $quote = new Quote();
    if (isset($newQuote->id)) {
        $id = $quote->update($newQuote->id, $newQuote);
    } else {
        $id = $quote->add($newQuote);
    }


    $response = Array();
    if (is_numeric($id)) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
        $response['data'] = $newQuote;

    }
    header("Content-Type: application/json");
    echo json_encode($response);
});


$app->post('/quotes/guest', function () use ($app) {

    header("Content-Type: application/json");
    authCheck($app->request());

    $data = $app->request->getBody();
    $newQuote = json_decode($data);

    if (empty($newQuote)) {
        echo "Error with Quote Data";

        return;
    }

    if (empty($newQuote->Cart)) {
        echo "Error: No cart in Quote";

        return;
    }

    $config['token'] = $newQuote->token;
    $config['site'] = $newQuote->site;
    $config['location'] = getLocation();

    unset($newQuote->token);
    unset($newQuote->site);

    $quote = new Quote();
    $response = $quote->addGuestQuote($newQuote);


    $config['id'] = $response['id'];
    $salesPersonData = $response['salesPerson'];

    if (!empty($salesPersonData) && !empty($salesPersonData['id'])) {

        if ($salesPersonData['sendMessage'] == 1) {

            $config['guestID'] = $salesPersonData['guestID'];
            $config['prefix'] = $salesPersonData['prefix'];
            $url = printGuestURL($config);

            $fileNamePath = "/tmp/Quote-".$config['prefix'].$response['id'].'.pdf';

            $configPDF['url'] = $url;
            $configPDF['fileNamePath'] = $fileNamePath;

            $salesPersonData['messageSubject'] = preg_replace('/{quote}/', $config['prefix'].$response['id'], $salesPersonData['messageSubject']);

            $salesPersonData['message'] = preg_replace('/{salesPerson}/', $salesPersonData['name'], $salesPersonData['message']);
            $salesPersonData['message'] = preg_replace('/{salesPhone}/', $salesPersonData['phone'], $salesPersonData['message']);
            $salesPersonData['message'] = preg_replace('/{salesEmail}/', $salesPersonData['email'], $salesPersonData['message']);

            $configEmail = (object) array(
                "address" => $salesPersonData['guestEmail'],
                "from" => $salesPersonData['email'],
                "subject" => $salesPersonData['messageSubject'],

                "message" => $salesPersonData['message'],
                "file" => $configPDF['fileNamePath']);

            $email = new Email();

//            print_r($configPDF);
//            print_r($configEmail);
//            exit();

            try {
                $email->emailInvoice($configPDF, $configEmail);
            } catch (Exception $err) {
            }

        }
    }

    if (!empty($response['salesPerson'])) {
        unset($response['salesPerson']['message']);
    }

    if (is_numeric($response['id'])) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
        $response['data'] = $newQuote;

    }
    header("Content-Type: application/json");
    echo json_encode($response);

});

$app->put('/quotes/:id/payment', function ($id) use ($app) {

    header("Content-Type: application/json");
    $salesPersonID = authCheck($app->request());

    $data = $app->request->getBody();
    $saveQuote = json_decode($data);


    if (empty($saveQuote)) {
        echo "Error with Quote Data";
        return;
    }

    if (empty($saveQuote->Cart)) {
        echo "Error: No cart in Quote";
        return;
    }

    $quote = new Quote();


    $id = $quote->updateSameVersion($id, $saveQuote);

    $response = Array();
    if (is_numeric($id)) {
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
        $response['data'] = $saveQuote;

    }


    renderJSON($response);

});


$app->put('/quotes/:id/pickupDelivery', function ($id) use ($app) {

    header("Content-Type: application/json");
    $salesPerson = authCheck($app->request());


    $data = $app->request->getBody();
    $pickupDelivery = json_decode($data);

    $pickupDelivery->id = $id;
    $pickupDelivery->SalesPerson = $salesPerson['id'];

    $quote = new Quote();

    $id = $quote->updatePickupDelivery($pickupDelivery);

    $response['id'] = $id;
    $response['status'] = 'Success';

    renderJSON($response);

});

$app->put('/quotes/:id', function ($id) use ($app) {

    header("Content-Type: application/json");
    $updatingPerson = authCheck($app->request());

    $data = $app->request->getBody();


    $saveQuote = json_decode($data);
    $saveQuote->userID = $updatingPerson['id'];

    if (isset($saveQuote->followUp)) {
        $saveQuote->followUp = humanDateToMysql($saveQuote->followUp);
    }

    if (isset($saveQuote->secondFollowUp)) {
        $saveQuote->secondFollowUp = humanDateToMysql($saveQuote->secondFollowUp);
    }

    if (empty($saveQuote)) {
        echo "Error with Quote Data";
        return;
    }

    if (empty($saveQuote->Cart)) {
        echo "Error: No cart in Quote";
        return;
    }

    $userObject = new User();
    $user = $userObject->getUser($updatingPerson['id']);

    $quote = new Quote();

    $quoteResults = $quote->getQuote($id);

    //Robert requested ability for anythingQuoteEdit to happen for everything 11/2/17

    if (($quoteResults['type'] == 'Completed' or $quoteResults['type'] == 'Delivered') && $user['anytimeQuoteEdits'] != '1') {
        $response['status'] = 'Error';
        $response['message'] = 'Production Already Started';
    } else if ($quoteResults['type'] == 'In Production' && $user['anytimeQuoteEdits'] != '1') {
        $response['status'] = 'Error';
        $response['message'] = 'Production Already Started';
    } else {

        unset($saveQuote->token);
        unset($saveQuote->site);

        $id = $quote->update($id, $saveQuote);

        $response = Array();
        if (is_numeric($id)) {
            $response['id'] = $id;
            $response['status'] = 'Success';
        } else {
            $response['status'] = 'Error';
            $response['data'] = $saveQuote;

        }
    }

    renderJSON($response);

});

$app->get('/quotes/:id/cutSheets', function ($id) use ($app) {

    header("Content-Type: application/json");
    $site = $app->request()->params('site');
    $_SERVER['site'] = $site;

//    authCheck($app->request());

    $quote = new quote();
    $results = $quote->getCutSheets($id);

    header("Content-Type: application/json");
    if (!empty($results['cutSheets'])) {
        echo $results['cutSheets'];
    } else {
        $response = Array();
        $response['status'] = 'Error';
        echo json_encode($response);
    }

});

$app->put('/quotes/:id/cutSheets', function ($id) use ($app) {


    authProduction($app->request);

    $data = $app->request->getBody();
    $sheets = json_decode($data);


    if (empty($sheets)) {
        echo "Error with Steets Data";
        return;
    }

    $cutSheet = new CutSheet();
    $result = $cutSheet->buildAndSaveCutSheets($id, $sheets);

    $response = Array();
    if ($result) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }


    header("Content-Type: application/json");
    echo json_encode($response);
});


$app->put('/quotes/:id/cutSheetsSales', function ($id) use ($app) {

    $realID = authSales($app->request);
    $superToken = validSuperToken($app->request()->params('superToken'));

    $data = $app->request->getBody();
    $sheets = json_decode($data);

    if (empty($sheets)) {
        echo "Error with Steets Data";
        return;
    }

    $cutSheet = new CutSheet();
    $result = $cutSheet->buildAndSaveCutSheetsSales($id, $sheets, $realID, $superToken);

    $response = Array();
    if ($result) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    header("Content-Type: application/json");
    echo json_encode($response);
});


$app->get('/quotes/:id/printInvoice', function ($id) use ($app) {

    authCheck($app->request);

    $config['id'] = $id;
    $config['token'] = $app->request()->params('token');
    $config['site'] = $app->request()->params('site');
    $config['collection'] = $app->request()->params('collection');
    $order = $app->request()->params('order');
    $config['location'] = getLocation();

    $debug = $app->request()->params('debug');

    if ($order == 'no') {
        $order = 'Quote';
    } else {
        $order = 'invoice';
    }

    $url = invoiceURL($config);

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $fileName = downLoadedFileNameFromIdAndOrder($id, $order);
    $fileNamePath = "/tmp/".$fileName;

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = $fileNamePath;
    $config['fileName'] = $fileName;

    writePhantomjsPDF($config);
    displayPDF($config);
});


$app->post('/quotes/:id/emailInvoice', function ($id) use ($app) {

    authCheck($app->request);

    $config['id'] = $id;
    $config['token'] = $app->request()->params('token');
    $config['site'] = $app->request()->params('site');
    $config['location'] = getLocation();

    $debug = $app->request()->params('debug');
    $order = 'invoice';
    $order = $app->request()->params('order');
    $url = invoiceURL($config);

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $fileName = 'Quote-'.$id.'.pdf';
    if ($order == 'yes') {
        $fileName = 'Order-'.$id.'.pdf';
    }

    $fileNamePath = "/tmp/".$fileName;

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = $fileNamePath;
    $config['fileName'] = $fileName;


//    writePhantomjsPDF($config);

    $data = $app->request->getBody();
    $emailInfo = json_decode($data);

    $site = new Site();
    $siteData = $site->getSiteForLocation($_SERVER['SITE_NAME']);

    $emailInfo->from = $siteData['salesEmail'];

    if (!empty ($emailInfo->address)) {
        $emailInfo->addresses = $emailInfo->address;
    }

    $email = new Email();

    foreach ($emailInfo->addresses as $emailInfo->address) {

        $configPDF['url'] = $url;
        $configPDF['fileNamePath'] = $fileNamePath;

        $message = $email->emailInvoice($configPDF, $emailInfo);

    }

    header("Content-Type: application/json");
    echo json_encode($message);
});


$app->get('/quotes/:id/printCutSheetsSales', function ($id) use ($app) {

    authSales($app->request);

    $token = $app->request()->params('token');
    $debug = $app->request()->params('debug');
    $order = 'yes';

    $location = getLocation();

    $url = 'https://'.$_SERVER['SERVER_NAME'].'/?tab=viewCutSheet&id='.$id.'&print=yes&token='.$token.'&location='.$location.'&site='.$_SERVER['site'];

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $fileName = 'CutSheet-'.$id.'.pdf';
    $fileNamePath = "/tmp/".$fileName;

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = $fileNamePath;
    $config['fileName'] = $fileName;

    printQuoteOrderWithConfig($config);
});


$app->get('/quotes/:id/printGlassSheets', function ($id) use ($app) {

    authProduction($app->request);

    $debug = $app->request()->params('debug');
    $token = $app->request()->params('token');

    $url = 'https://'.$_SERVER['SERVER_NAME'].'/?tab=viewGlassSheet&id='.$id.'&print=yes&token='.$token."&site=".$_SERVER['site'];;

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $order = 'yes';
    $fileName = downLoadedFileNameFromIdAndOrder($id, $order);
    $fileNamePath = "/tmp/".$fileName;

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = $fileNamePath;
    $config['fileName'] = $fileName;

    printQuoteOrderWithConfig($config);
});

$app->get('/quotes/:id/printGlassLabels', function ($id) use ($app) {

    authProduction($app->request);

    $debug = $app->request()->params('debug');
    $token = $app->request()->params('token');
    $production = $app->request()->params('production');

    $url = 'https://'.$_SERVER['SERVER_NAME'].'/?tab=viewGlassLabels&id='.$id.'&print=yes&token='.$token."&production=".$production."&site=".$_SERVER['site'];;

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $order = 'yes';
    $fileName = downLoadedFileNameFromIdAndOrder($id, $order);
    $fileNamePath = "/tmp/".$fileName;

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = $fileNamePath;
    $config['fileName'] = $fileName;

    printQuoteOrderWithConfig($config);
});

$app->get('/quotes/:id/printCutSheets', function ($id) use ($app) {

    authProduction($app->request);

    $debug = $app->request()->params('debug');
//    $myFile = "/var/www/panoramic/tmp/" . $id . ".pdf";
    $token = $app->request()->params('token');

    $url = 'https://'.$_SERVER['SERVER_NAME'].'/?tab=viewCutSheet&id='.$id.'&print=yes&token='.$token."&site=".$_SERVER['site'];;

    if ($debug == 'yes') {
        header('Location: '.$url);
        exit();
    }

    $order = 'yes';
    $fileName = downLoadedFileNameFromIdAndOrder($id, $order);
    $fileNamePath = "/tmp/".$fileName;

    $config['app'] = $app;
    $config['url'] = $url;
    $config['fileNamePath'] = $fileNamePath;
    $config['fileName'] = $fileName;

    printQuoteOrderWithConfig($config);
});


$app->put('/quotes/:id/guest', function ($id) use ($app) {

    header("Content-Type: application/json");
    $salesPersonID = authCheck($app->request());

    $data = $app->request->getBody();
    $saveQuote = json_decode($data);


    unset($saveQuote->token);
    unset($saveQuote->site);
    unset($saveQuote->guestEmail);

    if (empty($saveQuote)) {
        echo "Error with Quote Data";
        return;
    }

    if (empty($saveQuote->Cart)) {
        echo "Error: No cart in Quote";
        return;
    }

    $quote = new Quote();
    $id = $quote->updateGuest($id, $saveQuote);

    $response = Array();
    if (is_numeric($id)) {
        $response['id'] = $id;
        $response['id'] = $id;
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
        $response['data'] = $saveQuote;

    }
    header("Content-Type: application/json");
    echo json_encode($response);
});


$app->put('/quotes/:id/leadQuality', function ($id) use ($app) {

    header("Content-Type: application/json");

    $user = authCheck($app->request());

    $realID = '0';
    if ($user['type'] == 'production') {
        $superToken = "Yes";

    } else {
        $realID = authSales($app->request());
        $superToken = validSuperToken($app->request()->params('superToken'));
    }


    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
        // do stuff
    }

    $data = json_decode($json);

    $quote = new Quote();
    $status = $quote->leadQualityUpdateBySalesPersonAndId($data, $realID, $id, $superToken);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/quotes/:id/po', function ($id) use ($app) {


    header("Content-Type: application/json");

    $realID = '0';
    $user = authCheck($app->request());
    $superToken = '';

    if ($user['type'] == 'production') {
        $superToken = "Yes";

    } else {
        $realID = authSales($app->request());
        $superToken = validSuperToken($app->request()->params('superToken'));
    }

//    $realID = authSales($app->request());

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
    }

    $data = json_decode($json);

    $quote = new Quote();
    $status = $quote->poBySalesPersonAndId($data->po, $realID, $id, $superToken);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/quotes/:id/salesTerms', function ($id) use ($app) {

    header("Content-Type: application/json");

    $accounting = $app->request->params('accounting');
    $production = $app->request->params('production');

    $realID = '';
    if(!empty($accounting)) {
        $realID = authAccounting($app->request());
        $superToken = 1;
    }else if(!empty($production)){
        $realID = authProduction($app->request());
        $superToken = 1;
    }else{
        $realID = authSales($app->request());
        $superToken = validSuperToken($app->request()->params('superToken'));
    }

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Status Not Passed";
        return;
        // do stuff
    }

    $data = json_decode($json);

    $quote = new Quote();
    $status = $quote->updateSaleTerms($data->salesTerms, $realID, $id, $superToken);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);
});


$app->put('/quotes/:id/estComplete', function ($id) use ($app) {

    header("Content-Type: application/json");
    authProduction($app->request());

    $json = $app->request->getBody();


    $estComplete = json_decode($json);
    $estComplete->id = $id;


    $quote = new Quote();
    $status = $quote->quoteEstCompleteDate($estComplete);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});

$app->put('/quotes/:id/startedProd', function ($id) use ($app) {

    header("Content-Type: application/json");
    authProduction($app->request());

    $json = $app->request->getBody();


    $startedProd = json_decode($json);
    $startedProd->id = $id;


    $quote = new Quote();
    $status = $quote->quotestartedProdDate($startedProd);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/quotes/:id/dueDate', function ($id) use ($app) {

    header("Content-Type: application/json");

    $production = $app->request->params('production');

    $realID = '';
    if (empty($production)) {
        $realID = authSales($app->request());
    } else {
        $realID = authProduction($app->request());
    }

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Notes Not Passed";
        return;
        // do stuff
    }

    $superToken = validSuperToken($app->request()->params('superToken'));
    if (!empty($production)) {
        $superToken = 1;
    }
    $dueDate = json_decode($json);
    $dueDate->id = $id;
    $dueDate->SalesPerson = $realID;
    $dueDate->superToken = $superToken;

    $quote = new Quote();
    $status = $quote->quoteDueDate($dueDate);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/quotes/:id/updateFollowUps', function ($id) use ($app) {

    header("Content-Type: application/json");
    $realID = authSales($app->request());
    $superToken = validSuperToken($app->request()->params('superToken'));

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Notes Not Passed";
        return;
        // do stuff
    }

    $followUps = json_decode($json);
    $followUps->id = $id;
    $followUps->SalesPerson = $realID;
    $followUps->superToken = $superToken;

    $quote = new Quote();
    $status = $quote->quoteFollowUps($followUps);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


$app->put('/quotes/:id/updateNotes', function ($id) use ($app) {

    header("Content-Type: application/json");

    $production = $app->request->params('production');
    $accounting = $app->request->params('accounting');

    $realID = '';
    if (!empty($production)) {
        $realID = authProduction($app->request());
    } else if(!empty($accounting)) {
        $realID = authAccounting($app->request());
    }else{
        $realID = authSales($app->request());
    }

    $json = $app->request->getBody();
    if (empty($json)) {
        echo "Notes Not Passed";
        return;
        // do stuff
    }

    $notes = json_decode($json);
    $notes->id = $id;
    $notes->SalesPerson = $realID;

    $quote = new Quote();
    $status = $quote->quoteNotes($notes);

    if ($status) {
        $response['status'] = 'Success';
    } else {
        $response['status'] = 'Error';
    }

    echo json_encode($response);


});


