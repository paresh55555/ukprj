<?php

$app->post('/chargeCard', function () use ($app) {

    $dataJSON = $app->request->getBody();
    
    
    
    $postData = json_decode($dataJSON);
    
    $paymentLog = new PaymentLog();
    $paymentLog->startPayment(clone $postData);


    $config['id'] = $postData->id;
    $config['token'] = $postData->token;
    $config['site'] = $postData->site;
    $config['location'] = getLocation();

    $url = invoiceURL($config);
    $fileNamePath= "/tmp/Invoice-" . $postData->id.'.pdf' ;

    $configPDF['url'] = $url;
    $configPDF['fileNamePath'] = $fileNamePath;

    $site = new Site();
    $siteData = $site->getSiteForLocation($_SERVER['SITE_NAME']);

    $configEmail = (object) array (
        "address" => $postData->email,
        "from" => $siteData['salesEmail'],
        "subject" => "Purchase confirmation" ,
        "message" => " ",
        "file" => $configPDF['fileNamePath']);

    $creditCard = new CreditCard();
    $success  = $creditCard->chargeCard($postData, $paymentLog);

    if ($success) {
        $result['status'] = 'Success';
        $email = new Email();
        $email->emailInvoice($configPDF, $configEmail);
    } else {
        $result['status'] = 'Error';
    }

    echo  renderJSON($result);
});
