<?php


require_once "./customClasses/PriceEngine.php";


/**
 * Class PriceEngineTest
 *
 * @group priceEngine
 */
class PriceEngineTest extends PHPUnit_Framework_TestCase
{

    public function buildData()
    {

        $data['name'] = '';
        $data['width'] = '';
        $data['height'] = '';
        $data['formula'] = '';

        return ($data);

    }

    public function newPriceEngine()
    {

        $priceEngine = new PriceEngine();
        $priceEngine->width = 180;
        $priceEngine->height = 96;
        $priceEngine->panels = 5;
        $priceEngine->module = 1;
        $priceEngine->buildOptions();
        return ($priceEngine);

    }


    public function testSetName()
    {
        // make an instance of the user
        $user = new PriceEngine();

        $name = "Fracka";
        $user->setName($name);
        $this->assertEquals($name, $user->getName());
    }

    // Test Handles Correct JSON
    public function testSetObjectFromJSON_Success()
    {

        $priceEngine = new PriceEngine();
        $width = 100;
        $height = 102;

        $data = $this->buildData();
        $data['width'] = $width;
        $data['height'] = $height;
        $data['formula'] = '$this->width - 1';

        $json = json_encode($data);

        $result = $priceEngine->setObjectFromJSON($json);
        $this->assertTrue($result);

    }


    // Test to handle poor JSON
    public function testSetObjectFromJSON_Failure()
    {
        // make an instance of the user
        $priceEngine = new PriceEngine();
        $json = '{"name":"Fracka","b":2,"c":3,"d":4,"e:5}';
        $result = $priceEngine->setObjectFromJSON($json);
        $this->assertFalse($result);

    }

    public function testSetObjectFromJSON_width()
    {
        // make an instance of the user
        $priceEngine = new PriceEngine();
        $width = 100;
        $height = 102;
        $data = $this->buildData();
        $data['width'] = $width;
        $data['height'] = $height;
        $data['formula'] = '$this->width - 1';

        $json = json_encode($data);

        $result = $priceEngine->setObjectFromJSON($json);
        $this->assertTrue($result);
        $this->assertEquals($width, $priceEngine->{'width'});
        $this->assertEquals($height, $priceEngine->{'height'});

    }

    public function testGetOptions()
    {
        $priceEngine = new PriceEngine();
        $options = $priceEngine->getOptions();
        $this->assertGreaterThan(1, count($options[0]));
    }


//    public function testBuildOptions() {
//
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildOptions();
//
//        $totalOption = count($priceEngine->coreItemsSized);
//        $this->assertGreaterThan(0,$totalOption);
//
//        $priceEngine->buildOptions();
//        $totalOption2ndTry = count($priceEngine->coreItemsSized);
//        $this->assertEquals($totalOption,$totalOption2ndTry);
//
//    }
//
//
//  public function testGetNumberOfPanels() {
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->width = 240;
//    $priceEngine->getNumberOfPanels();
//    $this->assertEquals(3,count($priceEngine->panels));
//
//    $priceEngine->width = 61;
//    $priceEngine->getNumberOfPanels();
//    $this->assertEquals(1,count($priceEngine->panels));
//
//    $priceEngine->width = 400;
//    $priceEngine->getNumberOfPanels();
//    $this->assertEquals(1,count($priceEngine->panels));
//  }
//
//
//
//  public function testDiscount()
//  {
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Vinyl Inside");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("4089.52", $priceEngine->totalPrice);
//
//  }
//  public function testGetTotalPrice()
//  {
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Vinyl Inside");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("4089.52", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Vinyl Inside");
//    $priceEngine->groupedItems[5]->selectOption("Inside Custom Color");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("4426.13", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Vinyl Inside");
//    $priceEngine->groupedItems[5]->selectOption("Inside RAL");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("4257.83", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Wood Standard Inside");
//    $priceEngine->groupedItems[5]->selectOption("");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("5112.19", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Wood Premium Inside");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->groupedItems[5]->selectOption("");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("5437.52", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Wood Premium Inside");
//    $priceEngine->groupedItems[6]->selectOption("FLUSH BOTTOM TRACK");
//    $priceEngine->groupedItems[7]->selectOption("Glass Solabran 60");
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("5605.83", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Wood Premium Inside");
//    $priceEngine->groupedItems[7]->selectOption('Glass Solabran 70');
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("5451.26", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Wood Premium Inside");
//    $priceEngine->groupedItems[3]->selectOption("Extra Swing Door");
//    $priceEngine->groupedItems[7]->selectOption('Glass Solabran 60');
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("6037.52", $priceEngine->totalPrice);
//
//
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->groupedItems[0]->selectOption("Wood Premium Inside");
//    $priceEngine->groupedItems[2]->selectOption("10 Kick Plate");
//    $priceEngine->groupedItems[3]->selectOption("Extra Swing Door");
//    $priceEngine->groupedItems[7]->selectOption('Glass Solabran 60');
//    $priceEngine->getTotalPrice();
//    $this->assertEquals("6687.52", $priceEngine->totalPrice);
//  }
//
//
//
//  public function testSetDoorOptions()
//  {
//    $priceEngine = $this->newPriceEngine();
//    $priceEngine->setDoorOptions("both","2");
//    $this->assertEquals("both", $priceEngine->doorOptions['Swing Door']);
//    $this->assertEquals(2, $priceEngine->doorOptions['sliding left']);
//    $this->assertEquals(1, $priceEngine->doorOptions['sliding right']);
//
//    $priceEngine->setDoorOptions("left",0);
//    $this->assertEquals("left", $priceEngine->doorOptions['Swing Door']);
//    $this->assertEquals(4, $priceEngine->doorOptions['sliding left']);
//    $this->assertEquals(0, $priceEngine->doorOptions['sliding right']);
//
//    $priceEngine->setDoorOptions("right",0);
//    $this->assertEquals("right", $priceEngine->doorOptions['Swing Door']);
//    $this->assertEquals(0, $priceEngine->doorOptions['sliding left']);
//    $this->assertEquals(4, $priceEngine->doorOptions['sliding right']);
//
//    $result = $priceEngine->setDoorOptions("both",6);
//    $this->assertFalse($result);
//
//  }
//
//  public function testSetDoorDetails()
//  {
//    $priceEngine = $this->newPriceEngine();
//    $result = $priceEngine->setDoorDetails("Slide","RAL #BEBD7F","RAL #AF2B1E","Block","1 in Flush","Standard White");
//
//    $this->assertEquals("Slide", $priceEngine->doorDetails['Operation']);
//    $this->assertEquals("RAL #BEBD7F", $priceEngine->doorDetails['Interior Finish']);
//    $this->assertEquals("RAL #AF2B1E", $priceEngine->doorDetails['Exterior Finish']);
//    $this->assertEquals("Block", $priceEngine->doorDetails['Frame']);
//    $this->assertEquals("1 in Flush", $priceEngine->doorDetails['Track']);
//    $this->assertEquals("Standard White", $priceEngine->doorDetails['Handles']);
//
//    $this->assertTrue($result);
//
//  }
//
//
//  public function testReturnSortedItemsByMyOrder()
//  {
//
//    $myTestArray[0] = [
//        'name' => "Items3",
//        'myOrder' => 3
//    ];
//    $myTestArray[1] = [
//      'name' => "Items2",
//      'myOrder' => 2
//    ];
//    $myTestArray[2] = [
//      'name' => "Items1",
//      'myOrder' => 1
//    ];
//
//    $priceEngine = $this->newPriceEngine();
//    $mySortedArray= $priceEngine->returnSortedItemsByMyOrder($myTestArray);
//
//
//    $this->assertEquals($mySortedArray[0]['name'], 'Items1');
//    $this->assertEquals($mySortedArray[1]['name'], 'Items2');
//    $this->assertEquals($mySortedArray[2]['name'], 'Items3');
//
//  }
//
//  public function testIfItemsAndReturnSortedByMyOrder()
//  {
//
//    $myTestArray[0] = [
//      'name' => "Items3",
//      'myOrder' => 3
//    ];
//    $myTestArray[1] = [
//      'name' => "Items2",
//      'myOrder' => 2
//    ];
//    $myTestArray[2] = [
//      'name' => "Items1",
//      'myOrder' => 1
//    ];
//
//    $myCostSheet['parts'] = $myTestArray;
//
//    $priceEngine = $this->newPriceEngine();
//    $mySortedCostSheet= $priceEngine->ifItemsAndReturnSortedByMyOrder($myCostSheet);
//    $mySortedArray = $mySortedCostSheet['parts'];
//
//    $this->assertEquals($mySortedArray[0]['name'], 'Items1');
//    $this->assertEquals($mySortedArray[1]['name'], 'Items2');
//    $this->assertEquals($mySortedArray[2]['name'], 'Items3');
//
//  }
//
//
    public function testUpdateNumberOfSwingsForFixedCoreItems()
    {

        $swingsNumber = 2;
        $priceEngine = $this->newPriceEngine();
//    $priceEngine->setDoorOptionsAndUpdateSwingsForFixedCoreItems("both",$swingsNumber);
        $priceEngine->updateNumberOfSwingsForFixedCoreItems($swingsNumber);

        $item1 = $priceEngine->coreItemsFixed->options[0];
        $this->assertEquals($item1->swings, $swingsNumber);

        //Should not update if only 1
        $swingsNumber = 1;
        $priceEngine->updateNumberOfSwingsForFixedCoreItems($swingsNumber);
        $item1 = $priceEngine->coreItemsFixed->options[0];
        $this->assertEquals($item1->swings, 2);


    }

    public function testUpdateNumberOfSwingsForSizedCoreItems()
    {

        $swingsNumber = 2;
        $priceEngine = $this->newPriceEngine();
//    $priceEngine->setDoorOptionsAndUpdateSwingsForFixedCoreItems("both",$swingsNumber);
        $priceEngine->updateNumberOfSwingsForSizedCoreItems($swingsNumber);

        $item1 = $priceEngine->coreItemsSized->options[0];

        $this->assertEquals($item1->swings, $swingsNumber);

        //Should not update if only 1
        $swingsNumber = 1;
        $priceEngine->updateNumberOfSwingsForSizedCoreItems($swingsNumber);
        $item1 = $priceEngine->coreItemsSized->options[0];
        $this->assertEquals($item1->swings, 2);


    }

    public function testUpdateNumberOfSwingsForGroupedOptions()
    {

        $swingsNumber = 2;
        $priceEngine = $this->newPriceEngine();
        $priceEngine->updateNumberOfSwingsForGroupedOptions($swingsNumber);


        $item1 = $priceEngine->groupedItems['finishMaterialExt']->options[0]->options[0];


        $this->assertEquals($item1->swings, $swingsNumber);

        $item2 = $priceEngine->groupedItems['Hardware']->options[0]->options[0];
        $this->assertEquals($item2->swings, $swingsNumber);

        $item3 = $priceEngine->groupedItems['glassChoice']->options[0]->options[0];
        $this->assertEquals($item3->swings, $swingsNumber);
    }



//  public function testBuildCutSheet()
//  {
//    $priceEngine = $this->newPriceEngine();
//
//    $priceEngine->width= 180;
//    $priceEngine->panels = 5;
//    $priceEngine->height = 96;
//
////    $priceEngine->getNumberOfPanels();
////    var_dump($priceEngine->panels);
////    exit();
//
//    $priceEngine->groupedItems[0]->selectOption("Vinyl Inside");
////    $priceEngine->groupedItems[5]->selectOption("Inside RAL");
//
////    $priceEngine->groupedItems[2]->selectOption("10 Kick Plate");
//    $priceEngine->groupedItems[3]->selectOption("Extra Swing Door");
//    $priceEngine->groupedItems[7]->selectOption('Glass Solabran 60');
//
//
//    $priceEngine->setDoorOptions("both",1);
//
//
//
//    $priceEngine->setDoorDetails("Inswing","Vinyl","Vinyl","Block","1 5/8 Up Stand","Standard White");
//    $priceEngine->getTotalPrice();
//
//    $priceEngine->buildCostSheet();
//
//
//  }


//  public function testBuildCutSheetForVinyl() {
//    $priceEngine = $this->newPriceEngine();
//
//    $priceEngine->width= 180;
//    $priceEngine->panels = 5;
//    $priceEngine->height = 96;
//
//    $priceEngine->groupedItems[0]->selectOption("Vinyl");
//    $priceEngine->groupedItems[7]->selectOption('Glass Solabran 60');
//    $priceEngine->setDoorOptions("left",1);
//
//    $priceEngine->groupedItems[3]->selectOption("Extra Swing Door");
//    $priceEngine->setDoorOptions("both",1);
//
//    $priceEngine->setDoorDetails("Inswing","Vinyl","Vinyl","Block","1 5/8 Up Stand","Standard White");
//    $priceEngine->getTotalPrice();
//
//    $priceEngine->buildCostSheet();
//
//  }
//
//  public function testBuildCutSheetForRAL() {
//    $priceEngine = $this->newPriceEngine();
//
////    $priceEngine->width= 180;
////    $priceEngine->panels = 5;
////    $priceEngine->height = 96;
////
////    $priceEngine->groupedItems[0]->selectOption("Vinyl");
////    $priceEngine->groupedItems[7]->selectOption('Glass Solabran 60');
////    $priceEngine->setDoorOptions("left",1);
////
////    $priceEngine->groupedItems[3]->selectOption("Extra Swing Door");
////    $priceEngine->setDoorOptions("both",1);
////
////    $priceEngine->setDoorDetails("Inswing","Vinyl","Vinyl","Block","1 5/8 Up Stand","Standard White");
////    $priceEngine->getTotalPrice();
////
//////    $priceEngine->buildCostSheet();
//
//    $json = '{"width":"200","height":"120","panels":5,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":["Laminated","Breather Tubes","Argon"],"hardware":"Satin"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//
//    $priceEngine->buildCostSheet();
//
//    $this->assertEquals("4432.16", $priceEngine->totalPrice);
//
//  }


//  public function testGetModuleInfoID()
//  {
//    $priceEngine = new PriceEngine();
//
//    $moduleInfo = $priceEngine->getModuleInfoID(1);
//
//  }
//
//  public function testHardWareSelected() {
//
//    $json = '{"module":"9999","width":"200","height":"120","panels":5,"hardware":"Satin"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("50.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"9999","width":"200","height":"120","panels":5,"hardware":"White"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("25.00", $priceEngine->totalPrice);
//
//  }
//
//  public function testGlassType() {
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"glassChoice":"Glass Solabran 60"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("100.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"glassChoice":"Glass Solabran 70"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("200.00", $priceEngine->totalPrice);
//
//  }
//
//  public function testGlassOptionsLaminated() {
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"glassOptions":["Laminated"]}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("50.00", $priceEngine->totalPrice);
//
//  }
//
//  public function testGlassOptionsArgon() {
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"glassOptions":["Argon"]}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("50.00", $priceEngine->totalPrice);
//
//  }
//
//  public function testGlassOptionsArgonLaminated() {
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"glassOptions":["Argon","Laminated"]}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("100.00", $priceEngine->totalPrice);
//
//  }
//
//  public function testSwingDoor() {
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"swingDoor":"both"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("500.00", $priceEngine->totalPrice);
//
//  }
//
//
//  public function testSwingDoorWithSwingUpdates() {
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"swingDoor":"both","hardware":"Satin"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("600.00", $priceEngine->totalPrice);
//
//  }
//


//    public function testDoorDetails()
//    {
//
//        $json = '{"swingDirection":"In Swing", "module":"1","width":"200","height":"80","panels":7,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Laminated"],"finishes":{"ext":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg","location":"ext"},"int":{"type":"woodPremium","name1":"Mahogany","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Mohagony121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"Basic","track":"5/8 Step Up ","frame":"1 3/8 Nail Fin"}';
//
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $doorDetails = $priceEngine->buildDoorDetails();
//
//        $this->assertEquals("In Swing", $doorDetails['Operation']);
//        $this->assertEquals("Mahogany", $doorDetails['Interior Finish']);
//        $this->assertEquals("Walnut", $doorDetails['Exterior Finish']);
//        $this->assertEquals("1 3/8 Nail Fin", $doorDetails['Frame']);
//        $this->assertEquals("5/8 Step Up ", $doorDetails['Track']);
//        $this->assertEquals("Bronze", $doorDetails['Handles']);
//
//    }
//
//    public function testDoorDesign()
//    {
//       $json = '{"module":"1","width":"200","height":"80","panels":7,"swingDoor":"both","glassChoice":"Glass Clear","glassOptions":["Laminated"],"finishes":{"ext":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg","location":"ext"},"int":{"type":"woodPremium","name1":"Mahogany","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Mohagony121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"Basic","track":"Flush","frame":"Alumimum Block","swingDirection":"Out Swing","panelMovementRight":"4"}';
//
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $doorDesign = $priceEngine->buildDoorDesign();
//
//        $this->assertEquals("200", $doorDesign['width inches']);
//        $this->assertEquals("80", $doorDesign['height inches']);
//        $this->assertEquals("5080", $doorDesign['width MM']);
//        $this->assertEquals("2032", $doorDesign['height MM']);
//        $this->assertEquals("7", $doorDesign['panels']);
//        $this->assertEquals("both", $doorDesign['Swing Door']);
//        $this->assertEquals("1", $doorDesign['sliding left']);
//        $this->assertEquals("4", $doorDesign['sliding right']);
//
//    }

    public function testSwingDoorWithInstallOptions()
    {


//    $json = '{"module":"9999","width":"10","height":"10","panels":5,"installOptions":"Basic"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("800.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":1,"installOptions":"Full"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("650.00", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":5,"installOptions":"Full"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("950.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":5,"installOptions":"Assisted"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("400.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"9999","width":"10","height":"10","panels":5,"installOptions":"None"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $this->assertEquals("0.00", $priceEngine->totalPrice);

    }

    public function testBuildCostSheet()
    {

//    $json = '{"module":"1","width":"200","height":"120","glassChoice":"Glass Solabran 70",
//      "panels":5,"installOptions":"Basic", "swingDoor":"both",
//      "panelsRight":"3", "hardware":"Satin", "track":"Flush", "swingDirection":"InSwing",
//      "color":{"extColor":{"name":"RAL 1000"}, "intColor":{"name":"RAL 2000"} }}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//
//    $id = $priceEngine->buildCostSheet();
//    echo "ID: " . $id;
////      $this->assertEquals("1200.00", $priceEngine->totalPrice);
//    $material = $priceEngine->costSheet['header']['material'];
//    $panels = $priceEngine->costSheet['design']['panels'];
//    $sidleRight = $priceEngine->costSheet['design']['sliding right'];
//    $track = $priceEngine->costSheet['details']['Track'];
//    $handle = $priceEngine->costSheet['details']['Handles'];
//
//    $extColor = $priceEngine->costSheet['details']['Exterior Finish'];
//    $intColor = $priceEngine->costSheet['details']['Interior Finish'];
//
//    $this->assertEquals("CLAD", $material);
//    $this->assertEquals("5", $panels);
//    $this->assertEquals("3", $sidleRight);
//    $this->assertEquals("Satin", $handle);
//    $this->assertEquals("Flush", $track);
//    $this->assertEquals("RAL 1000", $extColor);
//    $this->assertEquals("RAL 2000", $intColor);
//
//    $json = '{"module":"1","width":"200","height":"120","panels":6,"swingDoor":"both","glassChoice":"Glass Clear","glassOptions":["Argon","Laminated"],"hardware":"Bronze","installOptions":"No Install"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $priceEngine->buildCostSheet();
//    $material = $priceEngine->costSheet['header']['material'];
//    $this->assertEquals("CLAD", $material);
//
//
//    $json = '{"module":"2","width":"200","height":"120","panels":6}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptions($json);
//    $priceEngine->buildCostSheet();
//    $material = $priceEngine->costSheet['header']['material'];
//    $this->assertEquals("Vinyl", $material);
//
//    $id = $priceEngine->buildCostSheet();

    }

//  Should be Working from Here down

//  public function testColorCLADPricing() {
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"yes","numberOfSides":"bothSides"},
//    "ext":{"type":"arch","name1":"Aged Copper","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"arch","name1":"Aged Copper","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("440.00", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"yes","numberOfSides":"oneSide"},
//    "ext":{"type":"arch","name1":"Aged Copper","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"arch","name1":"Aged Copper2","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("472.00", $priceEngine->totalPrice);
//
//
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"no","numberOfSides":"oneSide"},
//    "ext":{"type":"ral","name1":"Aged Copper","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"basic","name1":"White","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("440.00", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"no","numberOfSides":"oneSide"},
//    "ext":{"type":"ral","name1":"Aged Copper","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"custom","name1":"White","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("520.00", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"no","numberOfSides":"oneSide"},
//    "ext":{"type":"basic","name1":"Aged Copper","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"custom","name1":"White","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("480.00", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"yes","numberOfSides":"bothSides"},
//    "ext":{"type":"custom","name1":"yogi","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"custom","name1":"yogi","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("480.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"9998","width":"200","height":"120","panels":5,"swingDoor":"left",
//    "glassChoice":"Glass Clear",
//    "glassOptions":["Argon"],
//    "finishes":{"finishOptions":{"sameType":"yes","numberOfSides":"oneSide"},
//    "ext":{"type":"custom","name1":"yogi","name2":"undefined","hex":"undefined","buttonID":"arch","subSectionID":"all"},
//    "int":{"type":"custom","name1":"yogi","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//    "paintedColor":"NA"},
//    "hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("520.00", $priceEngine->totalPrice);
//
//    $json = '{"module":"1","width":"200","height":"120","panels":5,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Argon"],
//    "finishes":{"ext":{"type":"woodStandard","name1":"Alder","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//                "int":{"type":"basic","name1":"Alder","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//                "paintedColor":"NA",
//                "finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Satin"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("10855.72", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"1","width":"200","height":"120","panels":5,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Argon"],
//    "finishes":{"ext":{"type":"woodStandard","name1":"Alder","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//                "int":{"type":"woodStandard","name1":"Alder","name2":"undefined","hex":"undefined","buttonID":"wood","subSectionID":"all"},
//                "paintedColor":"NA",
//                "finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Satin"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("15820.21", $priceEngine->totalPrice);
//
//
//
//  }
//
//
//  public function testSBuildDefaultAmount() {
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->module = 9999;
//    $priceEngine->width = 200;
//    $priceEngine->height = 100;
//    $priceEngine->panels = 5;
//
//    $priceEngine->buildDefaultTotal();
//    $priceEngine->displayBreakDown();
//
//    $this->assertEquals("20500.00", $priceEngine->defaultTotal);
//
//  }

    public function testColorVinylPricing()
    {

//        $json = '{"module":"9999","width":"80","height":"80","panels":2,"glassChoice":"","glassOptions":[],"finishes":{"ext":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"int":"","vinylColor":"White Vinyl","paintedColor":"none","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"","installOptions":"","track":"","frame":"","panelMovementRight":""}';
//        $json = '{"module":"2","width":"80","height":"80","panels":2,"glassChoice":"","glassOptions":[],"finishes":{"ext":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined","location":"ext"},"int":"","vinylColor":"White Vinyl","paintedColor":"none","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"","installOptions":"","track":"","frame":"","panelMovementRight":""}';
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $result = $priceEngine->buildCostSheet();
//        $this->assertEquals("1753.34", $priceEngine->totalPrice);
//
//        $json = '{"module":"9999","width":"80","height":"80","panels":2,"glassChoice":"","glassOptions":[],"finishes":{"ext":{"type":"vinyl","name1":"undefined","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"int":"","vinylColor":"Beige Vinyl","paintedColor":"none","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"","installOptions":"","track":"","frame":"","panelMovementRight":""}';
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $this->assertEquals("894.39", $priceEngine->totalPrice);
//
//
//        $json = '{"swingDoor":"left","glassChoice":"Glass Solabran 60","hardware":"White","installOptions":"No Install","module":"9999","width":"200","height":"100","panels":"5"}';
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $this->assertEquals("20500.00", $priceEngine->totalPrice);
//
//
//        $json = '{"swingDoor":"left","glassChoice":"Glass Solabran 60","finishes":{"vinylColor":"White Vinyl","paintedColor":"One Color"},"hardware":"White","installOptions":"No Install","module":"9999","width":"200","height":"100","panels":"5"}';
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $this->assertEquals("24190.00", $priceEngine->totalPrice);
//
//        $json = '{"swingDoor":"left","glassChoice":"Glass Solabran 60","finishes":{"vinylColor":"White Vinyl","paintedColor":"Two Color"},"hardware":"White","installOptions":"No Install","module":"9999","width":"200","height":"100","panels":"5"}';
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $this->assertEquals("25625.00", $priceEngine->totalPrice);


    }


    public function testMaterialOption()
    {
        $priceEngine = new PriceEngine();
        $option = "basicOneSide";
        $newOption = $priceEngine->createMaterialOption($option);
        $this->assertEquals("aluminumOneSide", $newOption);

        $option = "woodStandardOneSide";
        $newOption = $priceEngine->createMaterialOption($option);
        $this->assertEquals("woodStandardOneSide", $newOption);

        $option = "woodPremiumOneSide";
        $newOption = $priceEngine->createMaterialOption($option);
        $this->assertEquals("woodPremiumOneSide", $newOption);

    }


    public function testFinishMaterialSelection()
    {


        $json = '{"module":"1","width":"120","height":"80","panels":3,"swingDoor":"left","glassChoice":"Glass Solabran 60",
    "glassOptions":["Laminated"],
    "finishes":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},
     "int":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},
     "paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"Bronze","installOptions":"No Install"}';

        $priceEngine = new PriceEngine();
        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $priceEngine->totalPrice;
        $this->assertEquals(0, $priceEngine->groupedItems["finishMaterialExt"]->selected);
        $this->assertEquals(0, $priceEngine->groupedItems["finishMaterialInt"]->selected);

        $json = '{"module":"1","width":"120","height":"80","panels":3,"swingDoor":"left","glassChoice":"Glass Solabran 60",
    "glassOptions":["Laminated"],
    "finishes":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},
     "int":{"type":"basic","name1":"Black","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},
     "paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"No Install"}';
        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $this->assertEquals(0, $priceEngine->groupedItems["finishMaterialExt"]->selected);
        $this->assertEquals(0, $priceEngine->groupedItems["finishMaterialInt"]->selected);


        $json = '{"module":"1","width":"130","height":"96","panels":4,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Laminated"],"finishes":{"ext":{"type":"woodStandard","name1":"Alder","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"int":{"type":"woodStandard","name1":"Alder","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"No Install"}';
        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $this->assertEquals(1, $priceEngine->groupedItems["finishMaterialExt"]->selected);
        $this->assertEquals(1, $priceEngine->groupedItems["finishMaterialInt"]->selected);


        $json = '{"module":"1","width":"130","height":"96","panels":4,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Laminated"],"finishes":{"ext":{"type":"woodPremium","name1":"Mahogany","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Mohagony121.jpg"},"int":{"type":"woodPremium","name1":"Mahogany","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Mohagony121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"No Install"}';
        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $this->assertEquals(2, $priceEngine->groupedItems["finishMaterialExt"]->selected);
        $this->assertEquals(2, $priceEngine->groupedItems["finishMaterialInt"]->selected);

        $json = '{"module":"1","width":"130","height":"96","panels":4,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Laminated"],"finishes":{"ext":{"type":"woodPremium","name1":"Mahogany","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Mohagony121.jpg"},"int":{"type":"woodStandard","name1":"Alder","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"No Install"}';
        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $this->assertEquals(2, $priceEngine->groupedItems["finishMaterialExt"]->selected);
        $this->assertEquals(1, $priceEngine->groupedItems["finishMaterialInt"]->selected);

        $json = '{"module":"1","width":"130","height":"96","panels":4,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Laminated"],"finishes":{"ext":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"int":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"No Install"}';
        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $this->assertEquals(1, $priceEngine->groupedItems["finishMaterialExt"]->selected);
        $this->assertEquals(2, $priceEngine->groupedItems["finishMaterialInt"]->selected);


    }

    public function testBuildCorePercentage()
    {
        $priceEngine = new PriceEngine();
        $json = '{"module":"9999","width":"180","height":"96","panels":5,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":[],"finishes":{"ext":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg"},"int":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"White","installOptions":"No Install","track":"5/8 Step Up ","frame":"1 3/8 Nail Fin","swingDirection":"Out Swing","panelMovementRight":""}';
        $priceEngine->module = "9999";
        $priceEngine->buildCorePercentage();
        $this->assertEquals($priceEngine->corePercentage[0]->nameOfOption, "Profit Margin");

        $priceEngine->module = "999955";
        $priceEngine->buildCorePercentage();
        $this->assertEquals(isset($priceEngine->corePercentage[0]), false);

    }

    public function testBugTesting()
    {


//    $json = '{"module":"1","width":"200","height":"120","panels":6,"swingDoor":"both","glassChoice":"Glass Clear","glassOptions":["Argon","Laminated"],"hardware":"Bronze","installOptions":"No Install"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("6974.33", $priceEngine->totalPrice);
//
//    $json='{"module":"2","width":"200","height":"120","panels":6,"swingDoor":"both","glassChoice":"Glass Solabran 60","glassOptions":["Argon"],"hardware":"Curved Bronze","vinylType":"White Vinyl Painted"}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("2511.78", $priceEngine->totalPrice);
//
//
//    $json = '{"module":"1","width":"200","height":"120","panels":6,"swingDoor":"both","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes1":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"undefined","buttonID":"basic","subSectionID":"all","location":"ext"},"int":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"undefined","buttonID":"basic","subSectionID":"all","location":"int"},"paintedColor":"NA"},"hardware":"Satin"}';
//    $json = '{"module":"1","width":"200","height":"120","panels":5,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"undefined","buttonID":"basic","subSectionID":"all"},"int":{"type":"ral","name1":"Beige","name2":"RAL 1001","hex":"#C2B078","buttonID":"ral","subSectionID":"Yellow"},"paintedColor":"NA","finisheOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Satin"}';

//    $json = '{"module":"1","width":"200","height":"120","panels":5,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Argon"],
//    "finishes":{"ext":{"type":"custom","name1":"Apollow White","name2":"undefined","hex":"undefined","buttonID":"basic","subSectionID":"all","location":"ext"},
//    "int":{"type":"ral","name1":"Green beige","name2":"RAL 1000","hex":"#BEBD7F","buttonID":"ral","subSectionID":"Yellow"},
//    "paintedColor":"NA",
//    "finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Satin"}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("7443.45", $priceEngine->totalPrice);


//    $json = '{"module":"1","width":"120","height":"100","panels":3,"glassOptions":[],"finishes":{"ext":"","int":"","paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}}}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("7443.45", $priceEngine->totalPrice);

//    $json = '{"module":"1","width":"120","height":"100","panels":4,"swingDoor":"right","glassOptions":[],"finishes":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"undefined","buttonID":"basic","subSectionID":"all"},"int":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}}}';
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $this->assertEquals("7443.45", $priceEngine->totalPrice);

//    $json = '{"module":"1","width":"120","height":"80","panels":3,"swingDoor":"left","glassChoice":"Glass Solabran 60",
//    "glassOptions":["Laminated"],
//    "finishes":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},
//     "int":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},
//
//
//     "paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"Bronze","installOptions":"No Install"}';
////
////    $json = '{"module":"1","width":"120","height":"80","panels":3,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":["Laminated"],"finishes":{"ext":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"int":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"Bronze","installOptions":"No Install"}';
////    $json = '{"module":"2","width":"180","height":"96","panels":5,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":["Laminated"],"finishes":{"ext":{"type":"vinyl","name1":"Backwood","name2":"undefined","hex":"#2e432b","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"int":{"type":"vinyl","name1":"Burgandy","name2":"undefined","hex":"#61002b","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"paintedColor":"One Color","finishOptions":{"sameType":"yes","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"Basic"}';
////    $json = '{"module":"2","width":"180","height":"96","panels":5,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":["Laminated"],"finishes":{"ext":{"type":"vinyl","name1":"Amazon Green","name2":"undefined","hex":"#14261a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"int":{"type":"vinyl","name1":"Burgandy","name2":"undefined","hex":"#61002b","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"paintedColor":"One Color","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Bronze","installOptions":"Basic"}';
////    $json = '{"module":"2","width":"120","height":"96","panels":4,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Laminated","Argon"],"finishes":{"vinylColor":"","paintedColor":"NA"},"hardware":"Satin","installOptions":""}';
////
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $priceEngine->totalPrice;
//    $json = $priceEngine->buildCostSheet();
//
//
//
//    $this->assertEquals("2513.91", $priceEngine->totalPrice);
//
//    $json = '{"module":"2","width":"80","height":"80","panels":2,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":["Laminated"],"finishes":{"ext":{"type":"vinyl","name1":"Backwood","name2":"undefined","hex":"#2e432b","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"int":{"type":"vinyl","name1":"Backwood","name2":"undefined","hex":"#2e432b","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"vinylColor":"White Vinyl","paintedColor":"One Color","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"","installOptions":"","track":"","frame":"","swingDirection":"Out Swing","panelMovementRight":""}';
//
//    $priceEngine = new PriceEngine();
//    $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//    $priceEngine->totalPrice;
//    $json = $priceEngine->buildCostSheet();


//    $json='{"module":"2","width":"180","height":"96","panels":5,"swingDoor":"left","glassChoice":"Glass Solabran 60","glassOptions":[],"finishes":{"vinylColor":"White Vinyl","paintedColor":"NA"},"hardware":"White","installOptions":"No Install","track":"Flush","frame":"Alumimum Block","swingDirection":"Out Swing","panelMovementRight":""}';
//    $json ='{"module":"1","width":"180","height":"96","panels":5,"swingDoor":"left","glassChoice":"","glassOptions":[],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"int":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"","installOptions":"","track":"5/8 Step Up ","frame":"1 3/8 Nail Fin","swingDirection":"Out Swing","panelMovementRight":""}';

//    $json = '{"module":"3","width":"310","height":"96","panels":10,"swingDoor":"left","glassChoice":"Glass Solabran 70","glassOptions":["Argon","Laminated"],"finishes":{"ext":"","int":"","vinylColor":"","paintedColor":"NA"},"hardware":"Bronze","installOptions":"","track":"5/8 Step Up ","frame":"1 3/8 Nail Fin","swingDirection":"Out Swing","panelMovementRight":""}';
//    $json = '{"module":"4","width":"100","height":"80","panels":3,"swingDoor":"left","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Satin","installOptions":"No Install","track":"5/8 Step Up ","frame":"Alumimum Block","swingDirection":"Out Swing","panelMovementRight":"0"}';
//    $json = '{"module":"1","width":"80","height":"80","panels":2,"swingDoor":"right","glassChoice":"Glass Solabran 60","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"int":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"White","installOptions":"No Install","track":"Flush","frame":"1 inch Nail Fin","swingDirection":"Out Swing","panelMovementRight":""}';
//    $json ='{"module":"1","width":"80","height":"80","panels":3,"swingDoor":"both","glassChoice":"Glass Solabran 60","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"int":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"White","installOptions":"No Install","track":"5/8 Step Up","frame":"1 inch Nail Fin","swingDirection":"Out Swing","panelMovementRight":"1"}';

//        $json='{"module":"1","width":"80","height":"80","panels":3,"swingDoor":"both","glassChoice":"Glass Solabran 60","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"int":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"White","installOptions":"No Install","track":"Flush","frame":"Aluminum Block","swingDirection":"Out Swing","panelMovementRight":"1","jobInfo":{"jobNumber":"1","productionDate":"2","dueDate":"3"}}';
//        $priceEngine = new PriceEngine();
//        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
//        $priceEngine->totalPrice;
//        $cut = $priceEngine->buildCostSheet($json);
//
//
//        $this->assertEquals("4602.45", $priceEngine->totalPrice);

    }

    public function testTranslateModuleBaseOnFrameAndTrack()
    {
        $priceEngine = new PriceEngine();
        $priceEngine->coreModule = '1';
        $priceEngine->track = 'Flush';
        $priceEngine->frame = 'Aluminum Block';

        $module = $priceEngine->translateModuleBaseOnFrameAndTrack();
        $this->assertEquals("5", $module);

        $priceEngine->subModule = null;
        $priceEngine->frame = '1 3/8 Nail Fin';
        $module = $priceEngine->translateModuleBaseOnFrameAndTrack();
        $this->assertEquals("4", $module);


        $priceEngine->subModule = null;
        $priceEngine->frame = '1 3/8 Nail Fin';
        $priceEngine->track = '5/8 Step Up';
        $module = $priceEngine->translateModuleBaseOnFrameAndTrack();
        $this->assertEquals("1", $module);


    }

    public function testGetSubmodule()
    {

        $priceEngine = new PriceEngine();
        $priceEngine->coreModule = '1';
        $module = $priceEngine->getSubmodule("1");
        $this->assertEquals("1", $module);

        $priceEngine->coreModule = '1';
        $module = $priceEngine->getSubmodule("2");
        $this->assertEquals("4", $module);

        $priceEngine->coreModule = '1';
        $module = $priceEngine->getSubmodule("3");
        $this->assertEquals("5", $module);

    }


    public function testGetSubModuleBaseOnFrameAndTrack()
    {
        $priceEngine = new PriceEngine();

        $track = 'Flush';
        $frame = 'Aluminum Block';
        $subModule = $priceEngine->getSubModuleBaseOnFrameAndTrack($frame, $track);
        $this->assertEquals("3", $subModule);


        $track = 'Flush';
        $frame = '1 3/8 Nail Fin';
        $subModule = $priceEngine->getSubModuleBaseOnFrameAndTrack($frame, $track);
        $this->assertEquals("2", $subModule);

        $track = '5/8 Step Up';
        $frame = '1 3/8 Nail Fin';
        $subModule = $priceEngine->getSubModuleBaseOnFrameAndTrack($frame, $track);
        $this->assertEquals("1", $subModule);

    }

    public function testKeepOnlyForCutSheetAndRemoveTotalCost()
    {

        $part = array(
            'name' => 'Option 5',
            'partNumber' => 'A400',
            'Cut Size Inches' => '80',
            'Cut Size MM' => '2032',
            'Lengths' => '3',
            'Quantity of Inches' => '240',
            'Quantity of Feet' => '20',
            'myOrder' => '305',
            'forCutSheet' => '1');

        $parts[] = $part;

        $priceEngine = new PriceEngine();
        $results = $priceEngine->keepOnlyForCutSheetAndRemoveTotalCost($parts);
        $this->assertEquals("1", count($results));

        $part['forCutSheet'] = 0;
        $parts = '';
        $parts[] = $part;
        $priceEngine = new PriceEngine();
        $results = $priceEngine->keepOnlyForCutSheetAndRemoveTotalCost($parts);

        $this->assertEquals("0", count($results));

        unset ($part['forCutSheet']);
        $parts = '';
        $parts[] = $part;
        $priceEngine = new PriceEngine();
        $results = $priceEngine->keepOnlyForCutSheetAndRemoveTotalCost($parts);
        $this->assertEquals("1", count($results));


    }


    public function testDecodeFinishedTypeSpecial()
    {


        $priceEngine = new PriceEngine();

        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "woodStandard";
        $finishes['int'] = array();
        $finishes['int']['type'] = "woodStandard";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("SW_SW", $results);


        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "woodPremium";
        $finishes['int'] = array();
        $finishes['int']['type'] = "woodPremium";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("PW_PW", $results);


        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "woodStandard";
        $finishes['int'] = array();
        $finishes['int']['type'] = "woodPremium";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("SW_PW", $results);


        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "woodPremium";
        $finishes['int'] = array();
        $finishes['int']['type'] = "woodStandard";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("SW_PW", $results);


        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "something";
        $finishes['int'] = array();
        $finishes['int']['type'] = "woodStandard";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("SW_Aluminum", $results);


        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "something";
        $finishes['int'] = array();
        $finishes['int']['type'] = "woodPremium";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("PW_Aluminum", $results);


        $finishes = Array();
        $finishes['ext'] = array();
        $finishes['ext']['type'] = "something";
        $finishes['int'] = array();
        $finishes['int']['type'] = "something";
        $newF = json_encode($finishes);
        $finishes = json_decode($newF);
        $priceEngine->finishes = $finishes;
        $results = $priceEngine->decodeFinishedTypeSpecial();
        $this->assertEquals("AluminumBothSides", $results);

    }

    /**
     * @group f1
     */
    public function testGetPanelWidth()
    {

        $priceEngine = new PriceEngine();

        $json = '{"module":"2","width":"100","height":"90","panels":"4","glassChoice":"","glassOptions":[],"finishes"
:{"ext":"","int":"","vinylColor":"","paintedColor":"NA"},"hardware":"","hardwareExtras":"","installOptions"
:"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"","panelMovementRight":"","subModules"
:1,"note":""}';

        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);

        $priceEngine->getPanelWidth();


        $this->assertEquals($priceEngine->panelWidth, 595.25);


    }
}
?>