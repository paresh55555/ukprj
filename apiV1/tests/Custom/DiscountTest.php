<?php


require_once "./customClasses/Discount.php";


class DiscountTest extends PHPUnit_Framework_TestCase
{

    private $newDiscount;

    public function setup()
    {
        $this->newDiscount = array('name' => "Discount", "amount" => 199, "type" => "percentage", "salesPerson" => '8', "setID" => '3', "shouldDisplay" => true);

    }

    public function testSetOptionFromJSON()
    {

        $this->assertTrue(true);
//
//
//        $discount = new Discount();
//        $json = $discount->buildDefault();
//        $defaults = json_decode($json);
//        $discount->setOptionFromJSON($json);
//
//        $this->assertEquals($discount->amount, $defaults->amount);
//        $this->assertEquals($discount->basePrice, $defaults->basePrice);
//        $this->assertEquals($discount->name, $defaults->name);
//        $this->assertEquals($discount->type, $defaults->type);
//        $this->assertEquals($discount->shouldDisplay, $defaults->shouldDisplay);
    }
//
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter name in Discount JSON
//     */
//    public function testSetOptionFromJSONMissingBasePrice()
//    {
//        $discount = new Discount();
//        $json = $discount->buildDefault();
//        $defaults = json_decode($json);
//        unset($defaults->name);
//        $newDefaults = json_encode($defaults);
//
//        $discount->setOptionFromJSON($newDefaults);
//
//        $this->assertEquals($discount->amount, $defaults->amount);
//
//    }
//
//    public function testGetDiscountAmount()
//    {
//
//        $discount = new Discount();
//        $json = $discount->buildDefault();
//
//        $discount->setOptionFromJSON($json);
//        $discount->getDiscountAmount();
//        $this->assertEquals($discount->totalDiscount, 1000);
//
//        $discount->type = "flat";
//        $discount->amount = 3000;
//        $discount->getDiscountAmount();
//        $this->assertEquals(3000, $discount->totalDiscount);
//    }
//
//
//    public function testCreateGetDeleteDiscount()
//    {
//
//        $discount = new Discount();
//
//        $id = $discount->createDiscount($this->newDiscount);
//        $this->assertTrue(is_numeric($id));
//
//        $results = $discount->getDiscount($id);
//        $this->assertTrue(is_array($results));
//
//        $results = $discount->deleteDiscount($results['id'],$results['salesPerson']);
//        $this->assertTrue($results);
//
//        $results = $discount->getDiscount($id);
//        $this->assertFalse(is_array($results));
//
//    }
//
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter name in Discount
//     */
//    public function testCreateDiscountMissingName()
//    {
//        $discount = new Discount();
//        unset ($this->newDiscount['name']);
//        $id = $discount->createDiscount($this->newDiscount);
//
//    }
//
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter amount in Discount
//     */
//    public function testCreateDiscountMissingAmount()
//    {
//        $discount = new Discount();
//        unset ($this->newDiscount['amount']);
//        $id = $discount->createDiscount($this->newDiscount);
//
//    }
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter type in Discount
//     */
//    public function testCreateDiscountMissingType()
//    {
//        $discount = new Discount();
//        unset ($this->newDiscount['type']);
//        $id = $discount->createDiscount($this->newDiscount);
//
//
//    }
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter salesPerson in Discount
//     */
//    public function testCreateDiscountMissingSalesPerson()
//    {
//        $discount = new Discount();
//        unset ($this->newDiscount['salesPerson']);
//        $id = $discount->createDiscount($this->newDiscount);
//
//    }
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter setID in Discount
//     */
//    public function testCreateDiscountMissingSetID()
//    {
//        $discount = new Discount();
//        unset ($this->newDiscount['setID']);
//        $id = $discount->createDiscount($this->newDiscount);
//
//    }
//
//    /**
//     *  ok
//     *
//     * @expectedException Exception
//     * @expectedExceptionMessage missing parameter shouldDisplay in Discount
//     */
//    public function testCreateDiscountMissingShouldDisplay()
//    {
//        $discount = new Discount();
//        unset ($this->newDiscount['shouldDisplay']);
//        $id = $discount->createDiscount($this->newDiscount);
//
//    }


}
