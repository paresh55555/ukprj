<?php


require_once "./customClasses/SizeOption.php";


/**
 * Class SizeOptionTest
 *
 * @group sizeOption
 */
class SizeOptionTest extends PHPUnit_Framework_TestCase
{


    public $cost_meter;
    public $cost_paint_meter;
    public $waste_percent;
    public $total_cost_meter;
    public $number_of_lengths;
    public $cut_size;
    protected $number_of_lengths_formula;
    protected $cut_size_formula;


    public function testSetObjectFromJSON()
    {

        $priceOption = new SizeOption();

        $json = $priceOption->buildDefault();
        $defaults = json_decode($json);

        $result = $priceOption->setOptionFromJSON($json);
        $total_cost_meter = $defaults->cost_meter + $defaults->cost_paint_meter + (($defaults->waste_percent / 100) * $defaults->cost_meter);

        $this->assertEquals($defaults->cost_meter, $priceOption->cost_meter);
        $this->assertEquals($defaults->cost_paint_meter, $priceOption->{'cost_paint_meter'});
        $this->assertEquals($defaults->waste_percent, $priceOption->{'waste_percent'});
        $this->assertEquals($total_cost_meter, $priceOption->totalCostMeter);
        $this->assertEquals($defaults->burnOff, $priceOption->burnOff);
        $this->assertEquals($defaults->myOrder, $priceOption->myOrder);
        $this->assertEquals($defaults->forCutSheet, $priceOption->forCutSheet);
        $this->assertEquals($defaults->partNumber, $priceOption->partNumber);
        $this->assertEquals($defaults->material, $priceOption->material);
        $this->assertEquals(1, $priceOption->swings);
        $this->assertTrue($result);

    }

    public function testSetNumberOfPanels()
    {
        $priceOption = new SizeOption();
        $priceOption->{'number_of_lengths_formula'} = 2;

        $priceOption->setNumber_of_lengths();

        $this->assertEquals(2, $priceOption->{'number_of_lengths'});

    }


    public function testGetTotalCostPerMeter()
    {
        $priceOption = new SizeOption();
        $json = $priceOption->buildDefault();
        $priceOption->setOptionFromJSON($json);

        $priceOption->cost_meter = 5.0;
        $priceOption->cost_paint_meter = 2.5;
        $priceOption->waste_percent = 20;

        $priceOption->getTotalCostPerMeter();
        $this->assertEquals(8.5, $priceOption->totalCostMeter);

    }

    public function testSetCutSize()
    {
        $priceOption = new SizeOption();
        $priceOption->{'width'} = 180;
        $priceOption->{'cut_size_formula'} = 'width - 1';

        $priceOption->setCutSize();

        $this->assertEquals(179, $priceOption->{'cut_size'});

    }


    public function testSetQuantity()
    {
        $priceOption = new SizeOption();
        $priceOption->{'panels'} = 5;
        $priceOption->{'quantityFormula'} = 'panels - 1';

        $priceOption->setQuantity();

        $this->assertEquals(4, $priceOption->quantity);

    }


    public function testTotalPricePerMeter()
    {
        $priceOption = new SizeOption();
        $json = $priceOption->buildDefault();
        $priceOption->setOptionFromJSON($json);

        $width = 180 * 25.4;
        $height = 96 * 25.4;
        $panels = 5;

        $priceOption->setOptionFromJSON($json);
        $priceOption->setTotalPrice($width, $height, $panels);

        $this->assertEquals(62.90, $priceOption->totalPrice);


    }


    public function testBuildBreakDown()
    {

        $priceOption = new SizeOption();
        $json = $priceOption->buildDefault();
        $priceOption->setOptionFromJSON($json);

        $width = 180;
        $height = 96;
        $panels = 5;
        $priceOption->setTotalPrice($width, $height, $panels);

        $breakDown = $priceOption->breakDown;

        $this->assertEquals($breakDown['name'], "Cool Option");
        $this->assertEquals($breakDown['partNumber'], "A4506");
        $this->assertEquals($breakDown['material'], "vinyl");
        $this->assertEquals($breakDown['Cut Size Inches'], "179");
        $this->assertEquals($breakDown['Cut Size MM'], "4547");

    }
}

?>