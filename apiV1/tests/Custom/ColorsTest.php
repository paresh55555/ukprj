<?php


require_once "./customClasses/Color.php";


/**
 * Class ColorTest
 *
 */
class ColorTest extends PHPUnit_Framework_TestCase
{

    /**
     * @group colors
     */
    public function testGetColorsForModule()
    {

        $colors = new Color();

        $results = $colors->getColorsForModule(2);
        print_r ($results);
        $this->assertTrue(true);
    }


    /**
     * @group colors2
     */
    public function testGetColorsByGroup()
    {

        $colors = new Color();

        $results = $colors->getColorsForGroupID(8);
        print_r($results);
        $this->assertTrue(true);
    }

}
