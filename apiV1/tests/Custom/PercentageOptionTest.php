<?php


require_once "./customClasses/PercentageOption.php";


/**
 * Class PercentageOptionTest
 *
 * @group percentageOption
 */
class PercentageOptionTest extends PHPUnit_Framework_TestCase
{

  public function testSetObjectFromJSON()
  {

    $percent = new PercentageOption();
    $json = $percent->buildDefault();
    $result = $percent->setOptionFromJSON($json);

    $defaults = json_decode($json);
    $this->assertEquals($defaults->nameOfOption, $percent->nameOfOption);
    $this->assertEquals($defaults->percentage, $percent->percentage);
    $this->assertTrue($result);


  }

  public function testGetTotal
  ()
  {

    $percent = new PercentageOption();
    $json = $percent->buildDefault();
    $result = $percent->setOptionFromJSON($json);
    $total = $percent->getTotal(1000);
    $percent->color = "#443434";

    $this->assertTrue($result);
    $this->assertEquals(100, $percent->totalPrice);
    $this->assertEquals($total, $percent->totalPrice);



  }

  public function testBreakDown()
  {

    $percent = new PercentageOption();
    $json = $percent->buildDefault();
    $result = $percent->setOptionFromJSON($json);
    $percent->color = "#443434";
    $total = $percent->getTotal(1000);

    $this->assertEquals($percent->breakDown['percentage'][0]['name'],"Super RAL Color");
    $this->assertEquals($percent->breakDown['percentage'][0]['color'],"#443434");
    $this->assertEquals($percent->breakDown['percentage'][0]['cost'],"USD 100.00");

//    $percent->displayBreakDown();
  }
}

?>