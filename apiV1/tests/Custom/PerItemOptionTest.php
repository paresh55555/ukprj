<?php


require_once "./customClasses/PerItemOption.php";
require_once "./customClasses/PhpStringParser.php";

/**
 * Class PerItemOptionTest
 *
 * @group perItemOption
 */
class PerItemOptionTest extends PHPUnit_Framework_TestCase
{

    public function buildData()
    {

        $data['nameOfOption'] = '';
        $data['quantityFormula'] = '';
        $data['costItem'] = '';

        return ($data);

    }

    public function testSetObjectFromJSON()
    {

        $perItemOption = new PerItemOption();
        $json = $perItemOption->buildDefault();
        $defaults = json_decode($json);

        $result = $perItemOption->setOptionFromJSON($json);

        $data['nameOfOption'] = "Cool Option";
        $data['quantityFormula'] = 'panels-1';
        $data['costItem'] = 50;
        $data['forCutSheet'] = 1;


        $total_cost = $defaults->quantityFormula * $defaults->costItem;


        $this->assertEquals($defaults->quantityFormula, $perItemOption->quantityFormula);
        $this->assertEquals($defaults->costItem, $perItemOption->costItem);
        $this->assertEquals($defaults->myOrder, $perItemOption->myOrder);
        $this->assertEquals($defaults->partNumber, $perItemOption->partNumber);
        $this->assertTrue($result);


    }

    public function testSetNumberOfPanels()
    {
        $perItemOption = new PerItemOption();
        $perItemOption->{'number_of_lengths_formula'} = 2;

        $perItemOption->setNumber_of_lengths();

        $this->assertEquals(2, $perItemOption->{'number_of_lengths'});

    }


    public function testSetQuantity()
    {
        $perItemOption = new PerItemOption();
        $perItemOption->{'panels'} = 5;
        $perItemOption->{'quantityFormula'} = 'panels - 1';

        $perItemOption->setQuantity();

        $this->assertEquals(4, $perItemOption->quantity);

    }

    public function testSetQuantityComplexFunction()
    {
        $perItemOption = new PerItemOption();
        $perItemOption->{'panels'} = 5;
        $perItemOption->{'quantityFormula'} = '<?php panels ?>';

        $perItemOption->{'quantityFormula'} = '<?php prePanels > 3 ? "panels * 150" : "500"; ?>';

        $perItemOption->setQuantity();

        $this->assertEquals(750, $perItemOption->quantity);

    }


    public function testTotalPricePerItem()
    {
        $perItemOption = new PerItemOption();
        $json = $perItemOption->buildDefault();
        $perItemOption->setOptionFromJSON($json);

        $width = 180;
        $height = 96;
        $panels = 5;

        $perItemOption->setTotalPrice($width, $height, $panels);
        $this->assertEquals(200, $perItemOption->totalPrice);


    }

    public function testBuildBreakDown()
    {

        $perItemOption = new PerItemOption();
        $json = $perItemOption->buildDefault();
        $perItemOption->setOptionFromJSON($json);

        $width = 180;
        $height = 96;
        $panels = 5;
        $perItemOption->setTotalPrice($width, $height, $panels);

        $breakDown = $perItemOption->breakDown;

        $this->assertEquals($breakDown['name'], "PerItem Cool Option");
        $this->assertEquals($breakDown['cost per part'], 50);
        $this->assertEquals($breakDown['cost'], "USD 200.00");
        $this->assertEquals($breakDown['quantity'], $perItemOption->quantity);
        $this->assertEquals($breakDown['forCutSheet'], $perItemOption->forCutSheet);
        $this->assertEquals($breakDown['myOrder'], $perItemOption->myOrder);
        $this->assertEquals($breakDown['partNumber'], $perItemOption->partNumber);
    }


}

?>