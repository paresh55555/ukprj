<?php


require_once "./customClasses/Payment.php";


class PaymentTest extends PHPUnit_Framework_TestCase
{

    private $newDiscount;

//    public function setup()
//    {
//        $this->newDiscount = array('name' => "Discount", "amount" => 10, "type" => "percentage", "salesPerson" => '8', "setID" => '3', "shouldDisplay" => true);
//
//    }
//
    public function testMakePayment()
    {

        $payment = new Payment();

        $config['amount'] = '1.24';
        $config['date'] = '01/01/2016';
        $config['lastFour'] = '1234';
        $config['quoteID'] = '1';
        $config['kind'] = 'Visa';

        $config = json_decode(json_encode($config));
        $result = $payment->makePayment($config);


        $this->assertTrue($result);
    }

    public function testGetPayment()
    {
        $id = 1;
        $payment = new Payment();
        $result = $payment->getPayments($id);


        $this->assertTrue(count($result) > 0);
    }

    public function testVerifyPayment()
    {

        $payment = new Payment();

        $id = 1;

        $result = $payment->verified($id);

        $this->assertTrue($result);
    }


    public function testNOtVerifyPayment()
    {

        $payment = new Payment();

        $id = 1;

        $result = $payment->notVerified($id);

        $this->assertTrue($result);
    }


    public function testDeletePayment()
    {

        $payment = new Payment();

        $id = 1;

        $result = $payment->delete($id);

        $this->assertTrue($result);
    }
}
