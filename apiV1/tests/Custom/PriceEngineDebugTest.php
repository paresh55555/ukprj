<?php


require_once "./customClasses/PriceEngine.php";


/**
 * Class PriceEngineDebugTest
 *
 * @group debug
 */
class PriceEngineDebugTest extends PHPUnit_Framework_TestCase
{

    public function buildData()
    {
        $data['name'] = '';
        $data['width'] = '';
        $data['height'] = '';
        $data['formula'] = '';

        return ($data);
    }

    public function newPriceEngine()
    {
        $priceEngine = new PriceEngine();
        $priceEngine->width = 180;
        $priceEngine->height = 96;
        $priceEngine->panels = 5;
        $priceEngine->module = 1;
        $priceEngine->buildOptions();

        return ($priceEngine);
    }


    /**
     *
     */
    public function testBugTesting()
    {

        $json = '{"module":"1","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-basic-all-Anthracite Grey-null-#1e262d-","kind":"basic","name1"
:"Anthracite Grey","name2":"null","hex":"#1e262d","buttonID":"extColor-basic","subSectionID":"all","url"
:""},"int":{"id":"intColor-woodStandard-all-Fir-null-null-images/colors/wood/Birch121.jpg","kind":"woodStandard"
,"name1":"Fir","name2":"null","hex":"null","buttonID":"intColor-woodStandard","subSectionID":"all","url"
:"images/colors/wood/Birch121.jpg"},"paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides"
:"oneSide"}},"hardware":"","hardwareExtras":"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum
 Block","swingDirection":"Outswing","panelMovementRight":0,"subModules":1,"note":"","sill":{},"addOns"
:{},"trickle":{"type":""}}';

        $json = '{"module":"3","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-basic-all-White-null-#fcffff-","kind":"basic","name1":"White","name2"
:"null","hex":"#fcffff","buttonID":"extColor-basic","subSectionID":"all","url":""},"int":{"id":"extColor-basic-all-White-null-
#fcffff-","kind":"basic","name1":"White","name2":"null","hex":"#fcffff","buttonID":"extColor-basic","subSectionID"
:"all","url":""},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}}
,"hardware":"","hardwareExtras":"None","installOptions":"","track":"Upstand","frame":"Aluminum Block"
,"swingDirection":"Outswing","panelMovementRight":0,"subModules":1,"note":"","sill":{},"addOns":{},"trickle"
:{"type":""}}';


        $json ='{"module":"3","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-woodStandard-all-Alder-null-null-images/colors/wood/Alder121.jpg"
,"kind":"woodStandard","name1":"Alder","name2":"null","hex":"null","buttonID":"extColor-woodStandard"
,"subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"int":{"id":"extColor-woodStandard-all-Alder-null-null-images
/colors/wood/Alder121.jpg","kind":"woodStandard","name1":"Alder","name2":"null","hex":"null","buttonID"
:"extColor-woodStandard","subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"paintedColor"
:"NA","finishOptions":{"sameType":"yes","numberOfSides":"twoSides"}},"hardware":"","hardwareExtras":"None"
,"installOptions":"","track":"Upstand","frame":"Aluminum Block","swingDirection":"Outswing","panelMovementRight"
:0,"subModules":1,"note":"","sill":{},"addOns":{},"trickle":{"type":""}}';


        $json ='{"module":"3","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-woodStandard-all-Alder-null-null-images/colors/wood/Alder121.jpg"
,"kind":"SW_SW","name1":"Alder","name2":"null","hex":"null","buttonID":"extColor-woodStandard"
,"subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"int":{"id":"intColor-woodStandard-all-White
 Oak-null-null-images/colors/wood/WhiteOak121.jpg","kind":"SW_SW","name1":"White Oak","name2"
:"null","hex":"null","buttonID":"intColor-woodStandard","subSectionID":"all","url":"images/colors/wood
/WhiteOak121.jpg"},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"oneSide"}}
,"hardware":"","hardwareExtras":"None","installOptions":"","track":"Upstand","frame":"Aluminum Block"
,"swingDirection":"Outswing","panelMovementRight":0,"subModules":1,"note":"","sill":{},"addOns":{},"trickle"
:{"type":""}}';

//        $json = $json2;

        $json = '{"module":"14","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":"","int":"","paintedColor":"NA","vinylColor":"Beige"},"hardware":"","hardwareExtras"
:"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing"
,"panelMovementRight":0,"subModules":1,"note":"","sill":{},"addOns":{},"trickle":{"type":""}}';

        $json = '{"module":"14","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":"","int":"","paintedColor":"NA","vinylColor":"Beige"},"hardware":"","hardwareExtras"
:"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing"
,"panelMovementRight":0,"subModules":1,"note":"","sill":{},"addOns":{},"trickle":{"type":""}}';

        $json = '{"module":"1","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-arch-all-Clear Anodized-null-null-images/colors/architecturalColors
/anodized-121.png","kind":"arch","name1":"Clear Anodized","name2":"null","hex":"null","buttonID":"extColor-arch"
,"subSectionID":"all","url":"images/colors/architecturalColors/anodized"},"int":{"id":"extColor-arch-all-Clear
 Anodized-null-null-images/colors/architecturalColors/anodized-121.png","kind":"arch","name1":"Clear
 Anodized","name2":"null","hex":"null","buttonID":"extColor-arch","subSectionID":"all","url":"images
/colors/architecturalColors/anodized"},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides"
:"bothSides"}},"hardware":"","hardwareExtras":"None","installOptions":"","track":"Flush","frame":"Aluminum
 Block","swingDirection":"Outswing","panelMovementRight":0,"subModules":1,"note":"","sill":{},"addOns"
:{},"trickle":{"type":""}}';


        $json = '{"module":"3","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-woodPremium-all-Mahogany-null-null-images/colors/wood/Mohagony121
.jpg","kind":"woodPremium","name1":"Mahogany","name2":"null","hex":"null","buttonID":"extColor-woodPremium"
,"subSectionID":"all","url":"images/colors/wood/Mohagony121.jpg"},"int":{"id":"intColor-woodStandard-all-Alder-null-null-images
/colors/wood/Alder121.jpg","kind":"woodStandard","name1":"Alder","name2":"null","hex":"null","buttonID"
:"intColor-woodStandard","subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"paintedColor"
:"NA","finishOptions":{"sameType":"no","numberOfSides":"swpw"}},"hardware":"","hardwareExtras":"None"
,"installOptions":"","track":"Upstand","frame":"Aluminum Block","swingDirection":"Outswing","panelMovementRight"
:0,"subModules":1,"note":"","sill":{},"addOns":{},"trickle":{"type":""}}';


        $json  = '{"module":"21","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"Unglazed","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-ral-Yellow-Beige-RAL 1001-#C2B078-undefined","kind":"ral","name1"
:"Beige","name2":"RAL 1001","hex":"#C2B078","buttonID":"extColor-ral","subSectionID":"Yellow","url":"undefined"
},"int":{"id":"intColor-woodPremium-all-Mahogany-null-null-images/colors/wood/Mohagony121.jpg","kind"
:"woodPremium","name1":"Mahogany","name2":"null","hex":"null","buttonID":"intColor-woodPremium","subSectionID"
:"all","url":"images/colors/wood/Mohagony121.jpg"},"paintedColor":"NA","finishOptions":{"sameType":"no"
,"numberOfSides":"oneSide"}},"hardware":"Straight Satin","hardwareExtras":"None","installOptions":""
,"track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing","panelMovementRight":0,"subModules"
:1,"productionNotes":"","note":"","sill":{},"addOns":{},"trickle":{"type":""}}';


        $json = '{"module":"21","width":80,"height":80,"panels":"2","swingDoor":"left","glassChoice":"Unglazed","glassOptions"
:[],"finishes":{"ext":{"buttonID":"custom","type":"custom","customName":"","kind":"custom"},"int":{"buttonID"
:"custom","type":"custom","customName":"","kind":"custom"},"paintedColor":"NA","finishOptions":{"sameType"
:"yes","numberOfSides":"oneSide"}},"hardware":"Straight Satin","hardwareExtras":"None","installOptions"
:"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing","panelMovementRight":0
,"subModules":1,"productionNotes":"","note":"","sill":{},"addOns":{},"trickle":{"type":""},"jobInfo"
:{"firstName":"Alan","lastName":"Rees","orderDate":"--","dueDate":"--","id":"FF18091","doorType":"eeCore
 Aluminum","city":"Oceanside","state":"CA"}}';

        $json = '{"module":"25","width":1501,"height":1200,"panels":"2","swingDoor":"left","glassChoice":"Low-E 2","glassOptions"
:["Argon","Laminated"],"finishes":{"ext":{"id":"extColor-basicUK-all-White-null-#fcffff-null","kind"
:"basicUK","name1":"White","name2":"null","hex":"#fcffff","buttonID":"extColor-basicUK","subSectionID"
:"all","url":"null"},"int":{"id":"extColor-basicUK-all-White-null-#fcffff-null","kind":"basicUK","name1"
:"White","name2":"null","hex":"#fcffff","buttonID":"extColor-basicUK","subSectionID":"all","url":"null"
},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"},"vinylColor":"White"
},"hardware":"Black","hardwareExtras":"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum
 Block","swingDirection":"Outswing","panelMovementRight":0,"subModules":1,"productionNotes":"","note"
:"","sill":{"selected":"sill1","width":"1500","height":"100","depth":"100","deduct":"no"},"addOns":{"topExtender"
:{"height":"100","width":10,"deduct":"no","selected":"yes"}},"trickle":{"type":"trickleVent4000","color"
:"Brown"}}';


        $json ='{"module":"1","width":1500,"height":1200,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":"","int":"","paintedColor":"NA"},"hardware":"","hardwareExtras":"None","installOptions"
:"","track":"Flush","frame":"Aluminum Block","swingDirection":"Outswing","panelMovementRight":0,"subModules"
:1,"note":"","sill":{},"addOns":{},"trickle":{"type":""}}';


        $json = '{"module":"1","width":1500,"height":1200,"panels":"2","swingDoor":"left","glassChoice":"","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-ral-Orange-Yellow orange-RAL 2000-#ED760E-undefined","kind":"ral"
,"name1":"Yellow orange","name2":"RAL 2000","hex":"#ED760E","buttonID":"extColor-ral","subSectionID"
:"Orange","url":"undefined"},"int":{"id":"extColor-ral-Orange-Yellow orange-RAL 2000-#ED760E-undefined"
,"kind":"ral","name1":"Yellow orange","name2":"RAL 2000","hex":"#ED760E","buttonID":"extColor-ral","subSectionID"
:"Orange","url":"undefined"},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"
}},"hardware":"","hardwareExtras":"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block"
,"swingDirection":"Outswing","panelMovementRight":0,"subModules":1,"note":"","sill":{"selected":"sill1"
,"width":"1500","height":"","depth":"","deduct":"no"},"addOns":{},"trickle":{"type":""}}';


        $json = '{"module":"1","width":1500,"height":1200,"panels":"2","swingDoor":"left","glassChoice":"Glass Clear"
,"glassOptions":[],"finishes":{"ext":{"id":"extColor-woodStandard-all-Alder-null-null-images/colors/wood
/Alder121.jpg","kind":"woodStandard","name1":"Alder","name2":"null","hex":"null","buttonID":"extColor-woodStandard"
,"subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"int":{"id":"intColor-woodPremium-all-Cherry-null-null-images
/colors/wood/Cherry121.jpg","kind":"woodPremium","name1":"Cherry","name2":"null","hex":"null","buttonID"
:"intColor-woodPremium","subSectionID":"all","url":"images/colors/wood/Cherry121.jpg"},"paintedColor"
:"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"White","hardwareExtras"
:"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing"
,"panelMovementRight":0,"subModules":1,"productionNotes":"","note":"","sill":{},"addOns":{},"trickle"
:{"type":""}}';

//
//        $json = '{"module":"1","width":1500,"height":1200,"panels":"2","swingDoor":"left","glassChoice":"Glass Clear"
//,"glassOptions":[],"finishes":{"ext":{"id":"extColor-woodStandard-all-Alder-null-null-images/colors/wood
///Alder121.jpg","kind":"woodStandard","name1":"Alder","name2":"null","hex":"null","buttonID":"extColor-woodStandard"
//,"subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"int":{"id":"intColor-woodStandard-all-Alder-null-null-images
///colors/wood/Alder121.jpg","kind":"woodStandard","name1":"Alder","name2":"null","hex":"null","buttonID"
//:"intColor-woodStandard","subSectionID":"all","url":"images/colors/wood/Alder121.jpg"},"paintedColor"
//:"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"White","hardwareExtras"
//:"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing"
//,"panelMovementRight":0,"subModules":1,"productionNotes":"","note":"","sill":{},"addOns":{},"trickle"
//:{"type":""}}';

        $json = '{"module":"14","width":2000,"height":1000,"panels":"3","swingDoor":"left","glassChoice":"Unglazed","glassOptions"
:[],"finishes":{"ext":{"id":"extColor-foilStandard-all-Golden Oak-null-null-images/colors/foilsStandard
/GoldenOakFoil.jpg","kind":"foilStandard","name1":"Golden Oak","name2":"null","hex":"null","buttonID"
:"extColor-foilStandard","subSectionID":"all","url":"images/colors/foilsStandard/GoldenOakFoil.jpg"}
,"int":{"id":"extColor-foilStandard-all-Golden Oak-null-null-images/colors/foilsStandard/GoldenOakFoil
.jpg","kind":"foilStandard","name1":"Golden Oak","name2":"null","hex":"null","buttonID":"extColor-foilStandard"
,"subSectionID":"all","url":"images/colors/foilsStandard/GoldenOakFoil.jpg"},"paintedColor":"NA","finishOptions"
:{"sameType":"yes","numberOfSides":"bothSides"},"vinylColor":"White"},"hardware":"White","hardwareExtras"
:"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing"
,"panelMovementRight":0,"subModules":1,"productionNotes":"","note":"","sill":{},"addOns":{},"trickle"
:{"type":""}}';


        $json = '{"module":"14","width":1000,"height":1500,"panels":"1","swingDoor":"right","glassChoice":"Low-E 3 with
 Argon","glassOptions":[],"finishes":{"ext":{"id":"extColor-foilStandard-all-White-null-#fcffff-null"
,"kind":"foilStandard","name1":"White","name2":"null","hex":"#fcffff","buttonID":"extColor-foilStandard"
,"subSectionID":"all","url":"null"},"int":{"id":"extColor-foilStandard-all-White-null-#fcffff-null","kind"
:"foilStandard","name1":"White","name2":"null","hex":"#fcffff","buttonID":"extColor-foilStandard","subSectionID"
:"all","url":"null"},"paintedColor":"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"
},"vinylColor":"White"},"hardware":"Graphite","hardwareExtras":"None","installOptions":"","track":"5
/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing","panelMovementRight":0,"subModules":1
,"productionNotes":"","note":"","sill":{},"addOns":{},"trickle":{"type":""}}';


        $priceEngine = new PriceEngine();

        $priceEngine->debug = true;

        $priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
        $priceEngine->totalPrice;



//        $priceEngine->buildCutSheet($json);

//        $cut = $priceEngine->buildCutSheet($json);
//
        echo "Cut: ******** \n";
//
//        print_r($cut);


//        $cut = json_encode($cut);
//        echo "$cut \n";


//        $this->assertEquals("0.00", $priceEngine->totalPrice);

    }


}

?>