<?php


require_once "./customClasses/GroupOption.php";
require_once "./customClasses/BuilderGroup.php";


/**
 * Class GroupOptionTest
 *
 * @group groupOption
 */
class GroupOptionTest extends PHPUnit_Framework_TestCase
{

//  public function testGetOptionTypes()
//  {
//
//    $group  = new BuilderGroup();
//    $moduleInfo['id'] = 1;
//    $moduleInfo['unitType'] = 'inch';
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,1);
//
//    $groupOptions = new GroupOption();
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//
//    $this->assertEquals(3,count($groupOptions->options));
//
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,2);
//
//    $groupOptions = new GroupOption();
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//
//    $this->assertEquals(1,count($groupOptions->options));
//
//  }
//
//
//
//  public function testSelectOption() {
//
//    $group  = new BuilderGroup();
//    $moduleInfo['id'] = 1;
//    $moduleInfo['unitType'] = 'inch';
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,1);
//
//    $groupOptions = new GroupOption();
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//
//    $groupOptions->selectOption($groupOptions->options[2]->name);
//    $this->assertEquals(2,$groupOptions->selected);
//
//
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,2);
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//    $groupOptions->selectOption($groupOptions->options[0]->name);
//    $this->assertEquals(0,$groupOptions->selected);
//
//  }

//  public function testGetBreakDown() {
//
//    $group  = new BuilderGroup();
//    $moduleInfo['id'] = 1;
//    $moduleInfo['unitType'] = 'inch';
//
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,1);
//
//    $groupOptions = new GroupOption();
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//
//    $groupOptions->selectOption($groupOptions->options[2]->name);
//    $this->assertEquals(2,$groupOptions->selected);
//
//    $width = 120;
//    $height = 80;
//    $panels = 5;
//    $basePrice = 1000;
//    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
//
//    $firstOption  = $groupOptions->breakDown['parts'][0];
//    $this->assertEquals($firstOption['name'],"SASH COVERS/SIDES PRE. WOOD");
//
//
//  }
//
//  public function testGetTotal() {
//
//    $group  = new BuilderGroup();
//    $moduleInfo['id'] = 1;
//    $moduleInfo['unitType'] = 'inch';
//
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,1);
//
//    $groupOptions = new GroupOption();
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//
//    $groupOptions->selectOption($groupOptions->options[2]->name);
//    $this->assertEquals(2,$groupOptions->selected);
//
//    $width = 120;
//    $height = 80;
//    $panels = 5;
//    $basePrice = 1000;
//    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
////    $this->assertEquals("1320.62",$total);
//
//    $groupOptions->selectOption($groupOptions->options[0]->name);
//    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
//    $this->assertEquals("45.31",$total);
//
//    $data = $group->getSizeGroupDataByModuleAndGroup(1,2);
//
//    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
//    $groupOptions->selectOption($groupOptions->options[0]->name);
//
//    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
//    $this->assertEquals("650.00",$total);
//
//
//
//  }


  public function testGetTotalWithHardware() {

  }

  public function testGetTotalWithColor() {

  }



  public function testGetTotalWithGlass() {

    $group  = new BuilderGroup();
    $moduleInfo['id'] = 9999;
    $moduleInfo['unitType'] = 'mm';

    $data = $group->getSizeGroupDataByModuleAndGroup(9999,2);


    $groupOptions = new GroupOption();
    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);

    //Needs to be in MM
    $width = 200*25.4;
    $height = 100*25.4;
    $panels = 5;
    $basePrice = 1000;

    $groupOptions->selectOption($groupOptions->options[0]->name);


    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
    $this->assertEquals("19999.96",$total);

    $groupOptions->selectOption($groupOptions->options[1]->name);

    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
    $this->assertEquals("23999.95",$total);

  }


  public function testBreakDown() {

    $group  = new BuilderGroup();
    $moduleInfo['id'] = 9999;
    $moduleInfo['unitType'] = 'mm';

    $data = $group->getSizeGroupDataByModuleAndGroup(9999,2);


    $groupOptions = new GroupOption();
    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);
    $width = 200*25.4;
    $height = 100*25.4;
    $panels = 5;
    $basePrice = 1000;
    $groupOptions->selectOption($groupOptions->options[0]->name);

    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
    $this->assertEquals("19999.96",$total);

    $option = $groupOptions->breakDown['glassChoices'];
    $this->assertEquals($option[0]['name'],"Low-E 2");

  }

  public function testSetSWings() {

    $module = 9999;
    $moduleInfo['id'] = 1;
    $moduleInfo['unitType'] = 'inch';

    $group  = new BuilderGroup();
    $data = $group->getSizeGroupDataByModuleAndGroup($module,1);

    $groupOptions = new GroupOption();
    $groupOptions->buildGroupWithDataAndModuleInfo($data,$moduleInfo);

    $groupOptions->setSwings(2);

    $this->assertEquals("2",$groupOptions->options[0]->options[0]->swings);

    $groupOptions->selectOption($groupOptions->options[0]->name);

    $width = 120;
    $height = 80;
    $panels = 5;
    $basePrice = 1000;
    $total = $groupOptions->getTotal($width,$height,$panels,$basePrice);
    $this->assertEquals("0.00",$total);

  }


}

?>