<?php


require_once "./customClasses/GlassOptionsSet.php";
require_once "./customClasses/GlassOption.php";

class GlassOptionSetTest extends PHPUnit_Framework_TestCase
{

  public function testBuildSetFromJSON () {

    $set = new GlassOptionsSet();
    $json = "";
    $error = $set->buildSetFromArrayOfJSON($json);
    $this->assertEquals($error, "Not an array");

  }


  public function testSetTotalPrivate () {

    $set = new GlassOptionsSet();

    $reflector = new ReflectionClass(get_class($set));
    $prop = $reflector->getProperty('total');
    $this->assertTrue($prop->isPrivate());

  }

  public function testGetTotal () {

    $set = new GlassOptionsSet();
    $glassOption = new glassOption();
    $singleJsonOption = $glassOption->buildDefault();

    $glassOption->setOptionFromJSON($singleJsonOption);

    $width = 180;
    $height = 96;
    $panels = 5;
    $glassOption->getTotalPrice($width,$height,$panels);


    $jsonObj = json_decode($singleJsonOption);
    $options = Array();

//    var_dump($jsonObj);

    $jsonObj->nameOfOption = "Name 1";
    array_push($options,$jsonObj,$jsonObj,$jsonObj);
    $error = $set->buildSetFromArrayOfJSON($options);
    $total = $set->getTotal($width,$height,$panels);

//    var_dump($total);

    $this->assertNull($error);
    $this->assertEquals($glassOption->totalPrice*count($options), $total);

//    $glassChoice->displayBreakDown();

  }

  public function testBuildOption () {

    $error = null;
    $set = new GlassOptionsSet();
    $glassOption = new GlassOption();
    $singleJsonOption = $glassOption->buildDefault();
    $jsonObj = json_decode($singleJsonOption);

    $options = Array();
    $jsonObj->nameOfOption = "Name 1";
    array_push($options,$jsonObj);
    $jsonObj->nameOfOption = "Name 2";
    array_push($options,$jsonObj);
    $jsonObj->nameOfOption = "Name 3";
    array_push($options,$jsonObj);
    $length_before = count ($options);

    $error = $set->buildSetFromArrayOfJSON($options);
    $length_after = count($set->options);

    $this->assertEquals($length_before,$length_after);
    $this->assertNull($error);

  }

  public function testBreakDown () {
    $set = new GlassOptionsSet();
    $glassOption = new GlassOption();
    $singleJsonOption = $glassOption->buildDefault();
    $glassOption->setOptionFromJSON($singleJsonOption);

    $width = 180;
    $height = 96;
    $panels = 5;
    $glassOption->getTotalPrice($width,$height,$panels);

    $jsonObj = json_decode($singleJsonOption);
    $options = Array();

    $jsonObj->nameOfOption = "Name 1";
    array_push($options,$jsonObj,$jsonObj,$jsonObj);
    $error = $set->buildSetFromArrayOfJSON($options);
    $total = $set->getTotal($width,$height,$panels);

    $glassOptions = $set->breakDown['glassChoices'];
    $option = $glassOptions[0];

    $this->assertEquals($option['name'],"Name 1");

  }
}
