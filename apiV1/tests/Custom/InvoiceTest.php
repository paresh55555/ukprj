<?php

require_once "./customClasses/Invoice.php";

/**
 * Class InvoiceTest
 *
 * @group glass
 */
class InvoiceTest extends PHPUnit_Framework_TestCase
{

    public function testGetNextInvoiceNumberWithQuoteIdAndCartNumber () {

        $invoice = new Invoice();


        $ID = $invoice->getNextInvoiceNumberWithQuoteIdAndCartNumber(11,'Cart1234');
        $ID2 = $invoice->getNextInvoiceNumberWithQuoteIdAndCartNumber(11,'Cart1234');

        $this->assertFalse($ID == $ID2);

    }
}
