<?php


require_once "./customClasses/BuilderGroup.php";

class BuilderGroupTest extends PHPUnit_Framework_TestCase
{

  public function testGetSizeGroupData()
  {

    $group  = new BuilderGroup();
    $data = $group->getSizeGroupDataByModuleAndGroup(9999,1);
    $this->assertEquals(2,count($data));

  }

  public function testGetGroupIDSForModule()
  {

    $group  = new BuilderGroup();
    $module_id = 9999;
    $data = $group->getGroupIDSForModule($module_id);

    $this->assertEquals(8,count($data));

  }


}

?>