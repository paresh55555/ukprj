<?php


require_once "./customClasses/Customer.php";


class CustomerTest extends PHPUnit_Framework_TestCase
{


    public function testGetCustomers()
    {

        $data = Array();
        $customer = new Customer();
        $data = $customer->getCustomers();

        $user = $data[0];

        $this->assertEquals($user['firstName'], "Fracka10");

    }

    public function testGetCustomer()
    {

        $customer = new Customer();
        $id = 1;
        $data = $customer->getCustomer($id);
        $json = json_encode($data[0]);
        $obj = json_decode($json);
        $this->assertNotEquals($obj->firstName, "Fracka");

    }

    public function testAddCustomer()
    {

        $customerData = Array();
        $customerData['firstName'] = "Fracka";
        $customerData['lastName'] = "Future";

        $customerData['companyName'] = "Future";
        $customerData['email'] = 'fracka@fracka.com';
        $customerData['phone'] = "515-421-5373";

        $customerData['billingAddress1'] = "4506 Velasco Pl";
        $customerData['billingAddress2'] = "Suite 100";
        $customerData['billingCity'] = "Austin";
        $customerData['billingState'] = "TX";
        $customerData['billingZip'] = "78749";

        $customerData['jobAddress1'] = "4506 Velasco Pl";
        $customerData['jobAddress2'] = "Suite 100";
        $customerData['jobCity'] = "Austin";
        $customerData['jobState'] = "TX";
        $customerData['jobZip'] = "78749";

        $customer = new Customer();
        $id  = $customer->add($customerData);


        $data = $customer->getCustomer($id);
        $json = json_encode($data[0]);
        $obj = json_decode($json);
        $this->assertEquals($obj->firstName, $customerData['firstName']);
        $this->assertEquals($obj->lastName, $customerData['lastName']);
        $this->assertEquals($obj->companyName, $customerData['companyName']);
        $this->assertEquals($obj->email, $customerData['email']);
        $this->assertEquals($obj->phone, $customerData['phone']);
        $this->assertEquals($obj->billingAddress1, $customerData['billingAddress1']);
        $this->assertEquals($obj->billingAddress2, $customerData['billingAddress2']);
        $this->assertEquals($obj->billingCity, $customerData['billingCity']);
        $this->assertEquals($obj->billingState, $customerData['billingState']);
        $this->assertEquals($obj->billingZip, $customerData['billingZip']);

        $this->assertEquals($obj->jobAddress1, $customerData['jobAddress1']);
        $this->assertEquals($obj->jobAddress2, $customerData['jobAddress2']);
        $this->assertEquals($obj->jobCity, $customerData['jobCity']);
        $this->assertEquals($obj->jobState, $customerData['jobState']);
        $this->assertEquals($obj->jobZip, $customerData['jobZip']);


    }

}
