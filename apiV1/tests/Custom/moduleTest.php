<?php


require_once "./customClasses/Module.php";


class ModuleTest extends PHPUnit_Framework_TestCase
{

//    public function testDeleteModuleFromTable()
//    {
//
//        $module = new Module();
//        $freeModuleNumber = $module->getFreeModuleNumber();
//        $module->copyModuleFromTable("module", 1, $freeModuleNumber);
//        $module->deleteModuleFromTable("module", $freeModuleNumber);
//        $results = $module->getTableData("module", $freeModuleNumber, $freeModuleNumber);
//        $this->assertEquals(count($results), 0);
//
//    }
//
    /**
     * @group module
     */
    public function testAddModule()
    {

        $id = 2;
        $module = new Module();
        $groups = $module->getColorGroupsByID($id);
        print_r($groups);

    }


    public function getColorGroupsByID($id)
    {

        if (!is_numeric($id)) {
            throw new Exception("getColorGroupsByID wasn't passed an ID!!!");
        }

        $db = connect_db();
        $sql = "SELECT * from SQ_modules_colors where moduleID = '" . $id . "' ORDER BY location,myOrder";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();

        $data = array();
        while ($options = $results->fetch_assoc()) {;
            $data[] = array('id' => $options['colorGroup'],'location' => $options['location']) ;

        }
        return $data;
    }


//
//    public function testBuildInsert()
//    {
//
//        $module = new Module();
//        $freeModuleNumber = $module->getFreeModuleNumber();
//        $results = $module->getTableData("module", 1, $freeModuleNumber);
//
//        $sql = $module->buildInsert($results[0], "module");
//        $sqlMatch = "INSERT INTO module (`name`,`type`,`unitType`,`defaultOptions`,`maxWidth`,`minWidth`,`maxHeight`,`minHeight`,`messageWidth`,`messageHeight`,`module_id`) VALUES ('CLAD','CLAD','mm','{}','240','48','96','36','*CLAD Doors have a maximum width of \$maxWidth inches and minimum width of \$minWidth inches','*CLAD Doors have a maximum height of \$maxHeight inches and a minimum height of \$minHeight inches','$freeModuleNumber')";
//        $this->assertEquals($sql, $sqlMatch);
//        $module->deleteModuleFromTable("module", $freeModuleNumber);
//    }
//
//    public function testSqlInsert()
//    {
//
//        $module = new Module();
//        $freeModuleNumber = $module->getFreeModuleNumber();
//        $results = $module->getTableData("module", 1, $freeModuleNumber);
//
//        $sql = $module->buildInsert($results[0], "module");
//        $success = $module->insertRow($sql);
//
//        $this->assertEquals(true, $success);
//        $module->deleteModuleFromTable("module", $freeModuleNumber);
//
//
//    }
//
//    public function testGetFreeModuleNumber()
//    {
//
//        $module = new Module();
//        $freeModuleNumber = $module->getFreeModuleNumber();
//        $results = $module->getTableData("module", 1, $freeModuleNumber);
//        $sql = $module->buildInsert($results[0], "module");
//        $success = $module->insertRow($sql);
//        $freeModuleNumberNew = $module->getFreeModuleNumber();
//        $this->assertEquals($freeModuleNumber + 1, $freeModuleNumberNew);
//
//        $module->deleteModuleFromTable("module", $freeModuleNumber);
//
//
//    }
//
//    public function testCopyTableModule()
//    {
//
//        $module = new Module();
//        $freeModuleNumber = $module->getFreeModuleNumber();
//        $module->copyModuleFromTable("module", 1, $freeModuleNumber);
//
//        $results = $module->getTableData("module", 1, $freeModuleNumber);
//        $resultsNew = $module->getTableData("module", $freeModuleNumber, $freeModuleNumber);
//        $this->assertEquals($results, $resultsNew);
//
//        $module->deleteModuleFromTable("module", $freeModuleNumber);
//
//    }
//
//    public function testUpdateModuleToName()
//    {
//
//        $module = new Module();
//        $success = $module->updateModuleToName(9999, "Tester2");
//        $this->assertEquals($success, true);
//        $success = $module->updateModuleToName(9999, "Tester");
//        $this->assertEquals($success, true);
//
//    }
//
//
//    public function testCopyModuleToNewNameAndDeleteModule()
//    {
//        $module = new Module();
//        $name = "Great";
//        $newModuleID = $module->copyModuleToNewName(1,$name);
//        $results = $module->getTableData("module", $newModuleID, $newModuleID);
//        $this->assertEquals($results[0]['name'], $name);
//
//
//        $module->deleteModule($newModuleID);
//        $results = $module->getTableData("module", $newModuleID, $newModuleID);
//        $this->assertEquals(count($results), 0);
//
//    }


    /**
     * @group newModule1
     */
    public function testSetupProductionData()
    {

        $module = new Module();
        $name = "Vinyl with Aluminum Block Frame Window";

        $newModuleID = $module->copyModuleToNewName(14,$name);
        echo $newModuleID."\n";

//        $module = new Module();
//        $name = "Vinyl Nail Fin";
//        $newModuleID = $module->copyModuleToNewName(2,$name);
//        echo $newModuleID."\n";

//        $module = new Module();
//        $name = "Signature Window 1";
//        $newModuleID = $module->copyModuleToNewName(3,$name);
//        echo $newModuleID."\n";

    }
    /**
     * @group newModule
     */
    public function testDeleteProductionData()
    {
//        $module = new Module();
//        $module->deleteModule("12");

    }


}
