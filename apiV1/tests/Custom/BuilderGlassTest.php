<?php


require_once "./customClasses/BuilderGlass.php";
require_once "./customClasses/GlassOption.php";
//require_once "./customClasses/SizeOptionSet.php";

/**
 * Class BuilderGlassTest
 *
 * @group builderGlass
 */
class BuilderGlassTest extends PHPUnit_Framework_TestCase
{

  public function testGetGlassData () {

    $builder = new BuilderGlass();
    $data = $builder->getGlassByModuleAndOption(1,7);
    $this->assertEquals($data[0]['nameOfOption'], "Low-E 2");
  }

  public function testGetGlassDataWithName () {

    $nameOfOption = 'Unglazed';
    $builder = new BuilderGlass();
    $dataArray = $builder->getGlassByModuleAndNameOfOption(3, $nameOfOption);


    $data = $dataArray[0];

    $data['width'] = 200 * 25.4;
    $data['height'] = 60 * 25.4;
    $data['panels'] = 5;
    $data['swings'] = 2;


    $json = json_encode($data);

    $glassOption = new GlassOption();
    $glassOption->setOptionFromJSON($json);

    $glassOption->setGlassHeight();
    $glassOption->setGlassWidth();

    print_r( $glassOption->glassHeight);
    print_r($glassOption->glassWidth );


//    $this->assertEquals($data[0]['nameOfOption'], $nameOfOption);
  }

}
