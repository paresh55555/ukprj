<?php


require_once "./customClasses/PerItemOptionSet.php";
require_once "./customClasses/PerItemOption.php";


/**
 * Class OptionSetTest
 *
 * @group optionSet
 */
class OptionSetTest extends PHPUnit_Framework_TestCase
{

//  public function testBuildSetFromJSON () {
//
//    $set = new SizeOptionsSet();
//    $json = "";
//    $error = $set->buildSetFromArrayOfJSON($json);
//    $this->assertEquals($error, "Not an array");
//
//  }
//
//
//  public function testSetTotalPrivate () {
//
//    $set = new SizeOptionsSet();
//
//    $reflector = new ReflectionClass(get_class($set));
//    $prop = $reflector->getProperty('total');
//    $this->assertTrue($prop->isPrivate());
//
//  }
//
//  public function testGetTotal () {
//
//    $set = new SizeOptionsSet();
//    $priceOption = new SizeOption();
//    $singleJsonOption = $priceOption->buildDefault();
//
//    $priceOption->setOptionFromJSON($singleJsonOption);
//
//    $width = 180;
//    $height = 96;
//    $panels = 5;
//    $priceOption->setTotalPrice($width,$height,$panels);
//
//    $jsonObj = json_decode($singleJsonOption);
//    $options = Array();
//
//    $jsonObj->nameOfOption = "Name 1";
//    array_push($options,$jsonObj,$jsonObj,$jsonObj);
//    $error = $set->buildSetFromArrayOfJSON($options);
//    $total = $set->getTotal($width,$height,$panels);
//
//    $this->assertNull($error);
//    $this->assertEquals($priceOption->totalPrice*count($options), $total);
//
//  }
//
//  public function testBuildOption () {
//
//    $error = null;
//    $set = new SizeOptionsSet();
//    $priceOption = new SizeOption();
//    $singleJsonOption = $priceOption->buildDefault();
//    $jsonObj = json_decode($singleJsonOption);
//
//    $options = Array();
//    $jsonObj->nameOfOption = "Name 1";
//    array_push($options,$jsonObj);
//    $jsonObj->nameOfOption = "Name 2";
//    array_push($options,$jsonObj);
//    $jsonObj->nameOfOption = "Name 3";
//    array_push($options,$jsonObj);
//    $length_before = count ($options);
//
//    $error = $set->buildSetFromArrayOfJSON($options);
//    $length_after = count($set->options);
//
//    $this->assertEquals($length_before,$length_after);
//    $this->assertNull($error);
//
//  }

  public function testPrintSet () {
    $set = new PerItemOptionSet();
    $perItemOption = new PerItemOption();
    $singleJsonOption = $perItemOption->buildDefault();

    $perItemOption->setOptionFromJSON($singleJsonOption);

    $width = 180;
    $height = 96;
    $panels = 5;
    $perItemOption->setTotalPrice($width,$height,$panels);

    $jsonObj = json_decode($singleJsonOption);
    $options = Array();

    $jsonObj->nameOfOption = "Name 1";
    array_push($options,$jsonObj,$jsonObj,$jsonObj);
    $error = $set->buildSetFromArrayOfJSON($options);
    $total = $set->getTotal($width,$height,$panels);

    $itemOptions = $set->breakDown['items'];
    $option = $itemOptions[0];

    $this->assertEquals($option['name'],"Name 1");

  }

  public function testSetSwings () {
    $set = new PerItemOptionSet();
    $perItemOption = new PerItemOption();
    $singleJsonOption = $perItemOption->buildDefault();


    $perItemOption->setOptionFromJSON($singleJsonOption);
    $perItemOption->quantityFormula = "swings";

    $width = 180;
    $height = 96;
    $panels = 5;
    $perItemOption->setTotalPrice($width,$height,$panels);

    $jsonObj = json_decode($singleJsonOption);
    $options = Array();

    $jsonObj->quantityFormula = "swings";
    array_push($options,$jsonObj,$jsonObj);
    $error = $set->buildSetFromArrayOfJSON($options);

    $set->setSwings(2);
    $total = $set->getTotal($width,$height,$panels);



    $this->assertEquals($total, '200.00');



  }


}
