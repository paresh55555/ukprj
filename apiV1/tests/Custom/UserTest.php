<?php

require_once "./customClasses/User.php";
require_once "./customClasses/AccessPrivateProtectedMethodProperties.php";

/**
 * Class UserCustomTest
 *
 * @group user
 */
class UserCustomTest extends AccessPrivateProtectedMethodProperties
{

    public function setUp()
    {
        $userData = Array("email" => "fracka@fracka.com", "password" => "Great");
        $user = new User();

        $this->testClass = $user;
        parent::setup();
    }

//
//    public function testLogin()
//    {
//        $user = new User();
//        $result = $user->login();
//        $this->assertFalse($result);
//
//        $result = $user->login("fracka@fracka.com");
//        $this->assertFalse($result);
//
//        $result = $user->login("f","f");
//        $this->assertEquals($result['id'],9);
//
//        $user->setPassword("Great2");
//        $result = $user->login();
//        $this->assertFalse($result);
//    }


    public function testGetSalesPersonForArea()
    {
        $user = new User();

        $quote = new stdClass();
        $quote->zip = 78749;
        $quote->guest = 100;

        $data = $user->getSalesPersonForArea($quote);
        print_r($data);
    }
}
