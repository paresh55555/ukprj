<?php

require_once "./customClasses/WindowSystem.php";



class WindowSystemTest extends AccessPrivateProtectedMethodProperties {

    private $config;
    public $windowFrameMock;

    public function setUp()
    {

        $transom =  array("transom" => array("width" => 500, "height" => 500));
        $transom2 =  array("transom" => array("width" => 1000, "height" => 1000));
        $mullion =  array("mullion" => array("width" => 500, "height" => 1000));
        $frame =    array("frame" => array("width" => 1000, "height" => 1000));
        $openers = array("opener" => array("width" => 1000, "height" => 1000));


        $options = array($frame, $mullion, $transom, $transom2, $openers);

        $this->config = array("module" => 16, "options" => $options);


        $this->windowFrameMock = $this->getMockBuilder('customClasses\WindowFrame')
            ->disableOriginalConstructor()
            ->setMethods(array('validOptions'))
            ->getMock();

        $frame = new WindowSystem($this->config);
        $this->testClass = $frame;
        parent::setup();

    }


    /**
     * @group frame
     */
    public function testValidOptions()
    {
//        $this->options = array();
        $frame = $this->getMethod('validOptions');
        $frame->invoke($this->testClass, $this->options);

    }


    /**
     * @group frame
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp /^Invalid Option Passed.+/
     */
    public function testValidOptionsInvalid()
    {
        $this->options = array();
        $frame = $this->getMethod('validOptions');
        $frame->invoke($this->testClass, $this->options);
    }
    /**
     * @group frame1
     */
    public function testGetTotalIntegrationTest()
    {
        $this->testClass = new WindowSystem($this->config);
        parent::setup();

//        $frame = $this->getMethod('buildWithOptions');
//        $frame->invoke($this->testClass, $this->options);

        $total = $this->testClass->getTotal();

        echo "Total: $total\n";
    }

        /**
     * @group frame2
     */
    public function testLoadBuilder()
    {
        $this->windowFrameMock->expects($this->once())
            ->method("validOptions")
            ->with($this->options);

        $this->testClass = $this->windowFrameMock;
        parent::setup();

        $frame = $this->getMethod('buildWithOptions');
        $frame->invoke($this->testClass, $this->options);


//        $total = $this->testClass->getTotal();

//        echo "Total: $total\n";



    }




}
