<?php


require_once "./customClasses/Quote.php";

/**
 * @group quotes
 */
class QuoteTest extends PHPUnit_Framework_TestCase
{

    public function testIsCustomer()
    {

        $newQuote = (object)array('Customer' => '1');
        $quote = new Quote();
        $status = $quote->isCustomer($newQuote);
        $this->assertTrue($status);

        $newQuote = (object)array('Customer' => 'A');
        $quote = new Quote();
        $status = $quote->isCustomer($newQuote);
        $this->assertFalse($status);

    }


    public function testIsSalesPerson()
    {
        $newQuote = (object)array('SalesPerson' => '1');
        $quote = new Quote();
        $status = $quote->isSalesPerson($newQuote);
        $this->assertTrue($status);

        $newQuote = (object)array('SalesPerson' => 'B');
        $quote = new Quote();
        $status = $quote->isSalesPerson($newQuote);
        $this->assertFalse($status);

    }


    public function testIsTotal()
    {
        $newQuote = (object)array('total' => '1000.00');
        $quote = new Quote();
        $status = $quote->isTotal($newQuote);
        $this->assertTrue($status);

        $newQuote = (object)array('total' => '1200G');
        $quote = new Quote();
        $status = $quote->isTotal($newQuote);
        $this->assertFalse($status);

    }

    public function testValidDiscount()
    {

        $newQuote = (object)array('discount' => '20');
        $quote = new Quote();
        $status = $quote->validDiscount($newQuote);
        $this->assertTrue($status);

        $newQuote = (object)array('discount' => 'AA');
        $quote = new Quote();
        $status = $quote->validDiscount($newQuote);
        $this->assertFalse($status);

        $newQuote = (object)array();
        $quote = new Quote();
        $status = $quote->validDiscount($newQuote);
        $this->assertTrue($status);

    }


    /**
     * @throws Exception
     */
    public function testGetGuestQuote()
    {

        $quoteID = 126;
        $guest = 92;
        $quote = new Quote();
        $data = $quote->getGuestQuote($quoteID);

        $this->assertEquals($data['id'], $quoteID);
        $this->assertEquals($data['guest'], $guest);

        $quoteID = '10';
        $data = $quote->getGuestQuote($quoteID);
        $this->assertFalse($data['id'] == $quoteID);

    }


    /**
     * ok
     *
     * @group quote
     */
    public function testGetSalesQuote()
    {

        $getQuote = array('salesPerson' => '9', 'quote' => '5049');
        $quote = new Quote();
        $data = $quote->getSalesQuote($getQuote);


        foreach ($quote->customerInfo as $check) {
            $this->assertFalse(isset($data[$check]));
        }


//        $this->assertTrue(is_array($data['Cart']));
//        $this->assertTrue(is_array($data['Customer']));
//
//
//        $this->assertEquals($data['id'], $getQuote['quote']);
//        $this->assertEquals($data['SalesPerson'], $getQuote['salesPerson']);
//        $this->assertEquals($data['Customer']['customerID'], '220');
//
//        $getQuote['quote'] = '10';
//
//        $data = $quote->getSalesQuote($getQuote);
//

//
//        $this->assertFalse($data['id'] == $getQuote['quote']);

    }


    public function testTransferQuote()
    {

        $guestQuoteId = 127;
        $salesPerson = 2;

        $quote = new Quote();
        $data = $quote->transferQuote($guestQuoteId, $salesPerson);

        $this->assertTrue(is_numeric($data));

    }


    public function testAddGuestQuote()
    {

        $cart = json_decode('{"Cart100232":{"name":"Great"}}');

        $newQuote = (object)array(
            'total' => 100.59,
            'Cart' => $cart
        );

        $quote = new Quote();
        $id = $quote->addGuestQuote($newQuote);
        $this->assertTrue(is_numeric($id));


    }


    public function testAddAndDeleteQuote()
    {
        $cart = json_decode('{"Cart100232":{"name":"Great"}}');

        $salesPerson = 1;
        $newQuote = (object)array('SalesPerson' => $salesPerson,
            'Customer' => 1,
            'total' => 100.59,
            'Cart' => $cart,
            'discount' => '20');

        $quote = new Quote();
        $addedQuote = $quote->add($newQuote);
        $this->assertTrue(is_numeric($addedQuote));

        $getQuote = array('salesPerson' => $salesPerson, 'quote' => $addedQuote);
        $data1 = $quote->getSalesQuote($getQuote);

        $this->assertTrue(is_array($data1));

        $fullQuote = $quote->getQuote($addedQuote);
        $this->assertFalse($data1 == $fullQuote);


        $quote = new Quote();
        $quote->deleteQuoteWithQuoteIDandSalesPerson($addedQuote, $salesPerson);

        $getQuote = array('salesPerson' => $salesPerson, 'quote' => $addedQuote);
        $quote = new Quote();
        $data2 = $quote->getSalesQuote($getQuote);

        $this->assertFalse($data1 == $data2);


    }


    public function testConvertToOrderForSalesPerson()
    {

        $id = '77';
        $salesPerson = '8';

        $quote = new Quote();
        $quote->resetOrderToQuote($id, $salesPerson);
        $quoteData = $quote->getQuote($id);

        $this->assertEquals($quoteData['type'], 'quote');

        $result = $quote->convertToOrderForSalesPerson($id, $salesPerson);
        $this->assertTrue($result);

        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'order');

    }


    public function testConvertToPendingProduction()
    {

        $id = '77';
        $salesPerson = '8';

        $quote = new Quote();
        $quote->resetOrderToQuote($id, $salesPerson);
        $quoteData = $quote->getQuote($id);

        $this->assertEquals($quoteData['type'], 'quote');


        $result = $quote->convertToOrderForSalesPerson($id, $salesPerson);
        $this->assertTrue($result);

        $quote->convertToPendingProduction($id, $salesPerson);

        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'Confirming Payment');

    }

    public function testSendToProduction()
    {

        $id = '77';
        $salesPerson = '8';

        $quote = new Quote();
        $quote->resetOrderToQuote($id, $salesPerson);
        $quoteData = $quote->getQuote($id);

        $this->assertEquals($quoteData['type'], 'quote');


        $result = $quote->convertToOrderForSalesPerson($id, $salesPerson);
        $this->assertTrue($result);

        $quote->convertToPendingProduction($id, $salesPerson);

        $quote->sendToProduction($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'Ready For Production');


    }

    /**
     * @throws Exception
     */
    public function testInProduction()
    {

        $id = '77';
        $salesPerson = '8';

        $quote = new Quote();
        $quote->resetOrderToQuote($id, $salesPerson);
        $quoteData = $quote->getQuote($id);

        $this->assertEquals($quoteData['type'], 'quote');

        $result = $quote->convertToOrderForSalesPerson($id, $salesPerson);
        $this->assertTrue($result);

        $quote->convertToPendingProduction($id, $salesPerson);
        $quote->sendToProduction($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'Ready For Production');

        $quote->inProduction($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'In Production');


        $quote->delivered($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'Delivered');

        $quote->completed($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'Completed');

        $quote->onHold($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['status'], 'hold');
        $this->assertEquals($quoteData['type'], 'Completed');

        $quote->deleteQuote($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['status'], 'deleted');

        $quote->readyForProduction($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'Ready For Production');


    }


    /**
     * @throws Exception
     *
     * @group f10
     */
    public function testInProductionDateMove()
    {


        $id = '77';
        $quote = new Quote();
        $quote->inProduction($id);
        $quoteData = $quote->getQuote($id);
        $this->assertEquals($quoteData['type'], 'In Production');


    }

    /**
     *
     */
    public function testWoodStatus()
    {

        $wood = array('wood' => 'Cherry', 'id' => '77');
        $quote = new Quote();
        $wood = json_encode($wood);
        $wood = json_decode($wood);

        $quote->woodOrdered($wood);

        $quoteData = $quote->getQuote($wood->id);
        $this->assertEquals($quoteData['woodOrdered'], $wood->wood);

        $wood = array('wood' => 'Fir', 'id' => '77');
        $wood = json_encode($wood);
        $wood = json_decode($wood);
        $quote->woodOrdered($wood);

        $quoteData = $quote->getQuote($wood->id);
        $this->assertEquals($quoteData['woodOrdered'], $wood->wood);


    }


    /**
     * @throws Exception
     */
    public function testWoodStatusFalse()
    {

        $wood = array('wood' => 'Bla', 'id' => '77');
        $quote = new Quote();
        $status = $quote->woodOrdered($wood);

        $this->assertFalse($status);

//        $quoteData = $quote->getQuote($id);
    }

    /**
     * @throws Exception
     */
    public function testValidWood()
    {

        $quote = new Quote();
        $goodWood = array('Fir', 'Alder', 'White Oak', 'Cherry', 'Mahogany', 'Walnut', 'Mixed');
        foreach ($goodWood as $testWood) {
            $array = array('id' => '77', 'wood' => $testWood);
            $array = json_encode($array);
            $array = json_decode($array);

            $status = $quote->validWood($array);
            $this->assertTrue($status);

        }

        $status = $quote->validWood(null);
        $this->assertFalse($status);

        $array = array('id' => '77');
        $array = json_encode($array);
        $array = json_decode($array);
        $status = $quote->validWood($array);
        $this->assertFalse($status);


        $array = array('wood' => 'Fir');
        $array = json_encode($array);
        $array = json_decode($array);
        $status = $quote->validWood($array);
        $this->assertFalse($status);

        $array = array('id' => '77', 'wood' => 'Bla');
        $array = json_encode($array);
        $array = json_decode($array);
        $status = $quote->validWood($array);
        $this->assertFalse($status);


    }

    /**
     *
     */
    public function testGlassOrdered()
    {

        $glass = array('glass' => 'No', 'id' => '77');
        $quote = new Quote();
        $glass = json_encode($glass);
        $glass = json_decode($glass);

        $quote->glassOrdered($glass);

        $quoteData = $quote->getQuote($glass->id);
        $this->assertEquals($quoteData['glassOrdered'], $glass->glass);

        $glass = array('glass' => 'Yes', 'id' => '77');
        $quote = new Quote();
        $glass = json_encode($glass);
        $glass = json_decode($glass);

        $quote->glassOrdered($glass);

        $quoteData = $quote->getQuote($glass->id);
        $this->assertEquals($quoteData['glassOrdered'], $glass->glass);


    }


    public function testDebug()
    {
        $json = '{"Customer":109,"SalesPerson":8,"Cart":{"Cart1415839666888":{"width":"100","height":"80","choosePanels":"panels-3","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-Satin","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"woodPremium","name1":"Walnut","name2":"undefined","hex":"null","buttonID":"woodPremium","subSectionID":"all","url":"images/colors/wood/Walnut121.jpg"},"intColor":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"numberOfPanels":3,"total":"6305.53","chooseSwings":"both","panelMovement":"both1-0","swingDirection":"Out Swing","chooseTrackSection":"track-5/8 Step Up ","chooseFrameSection":"frame-Alumimum Block","finishTypevinyl":"vinyl-0-noExt","finishTypeext":"ext-woodPremium","finishTypeint":"int-woodStandard","intNotSameExt":true,"glassOptions":{"undefined-Argon":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1415839666888","jobInfo":{"jobNumber":"A500","productionDate":"","dueDate":""},"index":1,"totalItems":3},"Cart1415890115964":{"module":"2","moduleTitle":"Vinyl Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*Vinyl Doors have a maximum width of 240 inches and a minimum width of 48 inches","messageHeight":"*Vinyl Doors have a maximum height of 96 inches and minimum height of 36 inches"},"width":"80","height":"80","choosePanels":"panels-2","numberOfPanels":2,"total":"2204.22","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirection":"Out Swing","chooseTrackSection":"track-5/8 Step Up ","chooseFrameSection":"frame-Alumimum Block","finishTypevinyl":"vinyl-0-exterior","paintedFinish":"vinyl-0-exterior","extColor":{"type":"vinyl","name1":"Buff","name2":"undefined","hex":"#d69d3a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"intColor":{"type":"vinyl","name1":"Buff","name2":"undefined","hex":"#d69d3a","buttonID":"vinyl","subSectionID":"undefined","url":"undefined"},"vinylType":"vinyl-Beige Vinyl","glassChoices":"undefined-Glass Solabran 60","glassOptions":{"undefined-Argon":"yes","undefined-Laminated":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","unique":"Cart1415890115964","jobInfo":{"jobNumber":"A500","productionDate":"","dueDate":""},"index":2,"totalItems":3,"validSize":true},"Cart1416351487879":{"module":"1","moduleTitle":"Clad Door","moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"width":"111","height":"80","choosePanels":"panels-2","numberOfPanels":2,"chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirection":"Out Swing","chooseFrameSection":"frame-Alumimum Block","chooseTrackSection":"track-Flush","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","extColor":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"glassChoices":"undefined-Glass Clear","glassOptions":{"undefined-Laminated":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","unique":"Cart1416351487879","index":3,"totalItems":3,"validSize":true,"total":"3555.75"}},"total":12065.5}';
        $json = '{"Customer":146,"SalesPerson":8,"Cart":{"Cart1417905457638":{"width":"80","height":"80","choosePanels":"panels-3","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"module":"1","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Clad Door","numberOfPanels":"3","chooseFrameSection":"frame-1 inch Nail Fin","chooseTrackSection":"track-Upstep","total":"4594.11","chooseSwings":"both","panelMovement":"both1-0","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","glassOptions":{"undefined-Laminated":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1417905457638","index":1,"totalItems":1,"type":"quote"},"Cart1417964673499":{"width":"80","height":"80","choosePanels":"panels-2","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"module":"1","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Clad Door","numberOfPanels":"2","chooseFrameSection":"frame-1 3/8 Nail Fin","chooseTrackSection":"track-Upstep","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1417964673499","type":"quote"}},"total":1000}';
        $json = '{"Cart":{"Cart1417905457638":{"width":"80","height":"80","choosePanels":"panels-3","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"module":"1","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Clad Door","numberOfPanels":"3","chooseFrameSection":"frame-1 inch Nail Fin","chooseTrackSection":"track-Upstep","total":"4594.11","chooseSwings":"both","panelMovement":"both1-0","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","glassOptions":{"undefined-Laminated":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1417905457638","index":1,"totalItems":2,"type":"quote"},"Cart1417973785423":{"width":"80","height":"80","choosePanels":"panels-3","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"module":"1","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Clad Door","numberOfPanels":"3","chooseFrameSection":"frame-1 inch Nail Fin","chooseTrackSection":"track-Upstep","total":"4594.11","chooseSwings":"both","panelMovement":"both1-0","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","glassOptions":{"undefined-Laminated":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1417973785423","index":2,"totalItems":2,"type":"quote"}},"total":9188.22,"quantity":2}';
        $json = '{"Cart":{"Cart1417905457638":{"width":"80","height":"80","choosePanels":"panels-3","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"module":"1","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Clad Door","numberOfPanels":"3","chooseFrameSection":"frame-1 inch Nail Fin","chooseTrackSection":"track-Upstep","total":"4594.11","chooseSwings":"both","panelMovement":"both1-0","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","glassOptions":{"undefined-Laminated":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1417905457638","index":1,"totalItems":1,"type":"quote"},"Cart1417964673499":{"width":"80","height":"80","choosePanels":"panels-2","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-White","installationOptions":"undefined-No Install","vinylType":"","extColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"intColor":{"type":"basic","name1":"Gray","name2":"undefined","hex":"#97968E","buttonID":"basic","subSectionID":"all","url":""},"module":"1","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*CLAD Doors have a maximum width of 240 inches and minimum width of 48 inches","messageHeight":"*CLAD Doors have a maximum height of 96 inches and a minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Clad Door","numberOfPanels":"2","chooseFrameSection":"frame-1 3/8 Nail Fin","chooseTrackSection":"track-Upstep","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-yesExt","finishTypeext":"ext-basic","finishTypeint":"int-basic","hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1417964673499","type":"quote"}},"total":"10000","quantity":2}';
        $json = '{"Cart":{"Cart1421007369064":{"width":"125","height":"90","choosePanels":"panels-3","glassChoices":"undefined-Glass Clear","hardwareChoices":"hardware-Straight Bronze","installationOptions":"","vinylType":"vinyl-White Vinyl","extColor":"","intColor":"","module":"2","validSize":true,"moduleSize":{"maxWidth":240,"minWidth":48,"maxHeight":96,"minHeight":36,"messageWidth":"*Vinyl Doors have a maximum width of 240 inches and a minimum width of 48 inches","messageHeight":"*Vinyl Doors have a maximum height of 96 inches and minimum height of 36 inches"},"firstTime":false,"moduleTitle":"Vinyl Door","note":"","quantity":10,"numberOfPanels":"3","chooseFrameSection":"frame-1 3/8 Nail Fin","chooseTrackSection":"track-Upstep","total":"2601.56","chooseSwings":"left","panelMovement":"leftSwingDirection","swingDirectionSection":"swing-Outswing","finishTypevinyl":"vinyl-0-no","glassOptions":{"undefined-Argon":"yes"},"hardwareToChooseFrom":[{"name":"White","info":"","url":"images/hardware/wht-120.gif"},{"name":"Straight Bronze","info":"","url":"images/hardware/stnd-bronze-120.gif"},{"name":"Straight Satin","info":"","url":"images/hardware/stnd-satin-120.gif"},{"name":"Dallas Satin","info":"","url":"images/hardware/dalla-120.gif"},{"name":"Curved Bronze","info":"","url":"images/hardware/crv-bronze.gif"},{"name":"Curved Satin","info":"","url":"images/hardware/crv-satin-120.gif"},{"name":"Dale Stainless","info":"","url":"images/hardware/dale-120.gif"}],"unique":"Cart1421007369064","index":1,"totalItems":10,"type":"quote"}},"total":26015.6,"quantity":10,"guest":59,"type":"guestQuote"}';

        $quoteData = json_decode($json);
//
        $quote = new Quote();
//        $id = $quote->add($quoteData);
//
//        var_dump ($id);
//

        $quoteID = 137;

        $id = $quote->update($quoteID, $quoteData);
//        $this->assertTrue(is_numeric($id));

    }


    public
    function testGetCutSheets()
    {

        $quote = new Quote();
        $results = $quote->getCutSheets(573);

        $this->assertTrue(empty($results['cutSheets']));


    }

    /**
     * @group f9
     */


    public function testCartTotal()
    {


        $cart = array("quantity" => 5, "total" => 100);

        $data = array("Cart" => array($cart, $cart));
        $data = json_encode($data);
        $data = json_decode($data);


        $quote = new Quote();
        $results = $quote->cartTotal($data);

        $this->assertEquals($results, 1000);


    }

//
//$cart = $quote->Cart;
//$total = 0;
//$count = 0;
//
//foreach ($cart as $door) {
//
//while ($count < $door->quantity) {
//$count++;
//if (isset($door->total)) {
//$total += floatval($door->total);
//echo "Door: " . $door->total . "\n";
//
//echo " 'Total: $total' \n";
//}
//}
//}
//
//echo " 'Final: $total' \n";
//return $total;


    /**
     * @group f1
     */
    public
    function testDaysFromNow()
    {


//
//        $date = '2015-03-25';
//
//        $quote = new Quote();
//        $results = $quote->daysFromNow($date);
//
//        print_r($results);

    }


    public function testAddInvoiceNumber()
    {
        $id = '1';
        $quote = new Quote();
        $quote->addInvoiceNumber($id);
    }


    public function testGetProductionCompleteDateFromQuote()
    {
        $quote = new Quote();

        $quoteData =  $quote->getQuote('1');
        $total = $quote->getProductionCompleteDateFromQuote($quoteData);

        $this->assertEquals('30', $total);
    }


    public function setProductionCompleteDate($quoteID, $quoteData)
    {
        if (empty($quoteID)) {
            throw new Exception('ID for setProductionCompleteDate Required');
        }

        $db = connect_db();

        $sql = "update quotesSales set  estComplete = DATE_ADD(NOW(), INTERVAL 21 DAY) where id=?  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set Complete Data ' . $sql);
        };

        return true;
    }
}