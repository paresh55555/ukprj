<?php


require_once "./customClasses/GlassOption.php";

class GlassOptionTest extends PHPUnit_Framework_TestCase
{

    public function testSetObjectFromJSON()
    {

        $glassOption = new GlassOption();
        $json = $glassOption->buildDefault();
        $defaults = json_decode($json);
        $result = $glassOption->setOptionFromJSON($json);

        $this->assertEquals($defaults->costSquareMeter, $glassOption->costSquareMeter);
        $this->assertEquals($defaults->nameOfOption, $glassOption->{'nameOfOption'});
        $this->assertEquals($defaults->height_cut_size_formula, $glassOption->{'height_cut_size_formula'});
        $this->assertEquals($defaults->width_cut_size_formula, $glassOption->{'width_cut_size_formula'});
        $this->assertEquals($defaults->glassOrOption, $glassOption->{'glassOrOption'});
        $this->assertTrue($result);

    }

    public function testProcessFormula()
    {
        $glassOption = new GlassOption();
        $glassOption->height = 10;
        $glassOption->width = 10;
        $glassOption->swings = 10;
        $glassOption->panels = 10;

        $formula = "panels + swings + width + height";
        $result = $glassOption->processFormula($formula);
        $this->assertEquals("40", $result);
    }


    public function testSetGlassWidth()
    {
        $glassOption = new GlassOption();
        $json = $glassOption->buildDefault();
        $result = $glassOption->setOptionFromJSON($json);

        $glassOption->setGlassWidth();
        $this->assertEquals("866.9", $glassOption->glassWidth);
    }

    public function testSetGlassHeight()
    {
        $glassChoice = new GlassOption();
        $json = $glassChoice->buildDefault();
        $result = $glassChoice->setOptionFromJSON($json);

        $glassChoice->setGlassHeight();
        $this->assertEquals("1313.17", $glassChoice->glassHeight);
        $this->assertTrue($result);
    }

    public function testGetTotalPrice()
    {
        $glassChoice = new GlassOption();
        $json = $glassChoice->buildDefault();
        $result = $glassChoice->setOptionFromJSON($json);


        $totalPrice = $glassChoice->getTotalPrice("4572", "2438.4", '5');
        $this->assertEquals($totalPrice, $glassChoice->totalPrice);
        $this->assertEquals($totalPrice, "343.51");
    }

    public function testCreateBreakDown()
    {
        $glassChoice = new GlassOption();
        $json = $glassChoice->buildDefault();
        $result = $glassChoice->setOptionFromJSON($json);

        $totalPrice = $glassChoice->getTotalPrice("180", "96", '5');

        $breakDown = $glassChoice->breakDown;

        $this->assertEquals($breakDown['name'], $glassChoice->nameOfOption);
        $this->assertEquals($breakDown['Glass Height Inches'], $glassChoice->glassHeight);
        $this->assertEquals($breakDown['Glass Width Inches'], $glassChoice->glassWidth);
        $this->assertEquals($breakDown['Quantity'], $glassChoice->quantity);
        $this->assertEquals($breakDown['Total Cost'], $glassChoice->totalPrice);

    }
}

?>