<?php


require_once "./customClasses/BuilderSet.php";
require_once "./customClasses/SizeOptionSet.php";


/**
 * Class BuilderSetTest
 *
 * @group builderSet
 */
class BuilderSetTest extends PHPUnit_Framework_TestCase
{

  public function testGetSetData () {

    $builder = new BuilderSet();
    $moduleInfo['id'] = 9999;
    $moduleInfo['unitType'] = 'mm';

    $data = $builder->getSizeSetDataByModuleAndOption($moduleInfo,0);
    $this->assertEquals(1, count($data));

    $set = new SizeOptionsSet();

    $error = $set->buildSetFromArrayOfJSON($data);
    $this->assertNull($error);

    $width = 180;
    $height = 96;
    $panels = 5;
    $total = $set->getTotal($width,$height,$panels);

  }

  public function testGetPerItemSetData () {

    $builder = new BuilderSet();
    $moduleInfo['id'] = 1;
    $moduleInfo['unitType'] = 'inch';
    $data = $builder->getPerItemSetDataByModuleAndOption(1,0);
    $this->assertEquals(10, count($data));


  }

  public function testGetPerItemSetDataByModuleAndOption () {

    $builder = new BuilderSet();
    $data = $builder->getPerItemSetDataByModuleAndOption(1,0);
    $this->assertEquals(10, count($data));

  }

  public function testGetSetDataByModuleAndOption () {

    $builder = new BuilderSet();
    $moduleInfo['id'] = 9999;
    $moduleInfo['unitType'] = 'inch';


    $data = $builder->getSizeSetDataByModuleAndOption($moduleInfo,0);
    $this->assertEquals(1, count($data));

  }


}
