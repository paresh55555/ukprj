<?php


require_once('./customClasses/CutSheet.php');
//require_once('./customClasses/AccessPrivateProtectedMethodProperties.php');

/**
 * Class CutSheetTest
 *
 * @group cutSheet
 */
class CutSheetTest extends PHPUnit_Framework_TestCase
{

    public function setUp()
    {

        $cutSheet = new cutSheet();
        $this->testClass = $cutSheet;
        parent::setup();

    }

    public function testConstruct()
    {
//        $email = $this->getProperty("email");
//        $password = $this->getProperty("password");
//
//        $this->assertEquals("fracka@fracka.com", $email);
//        $this->assertEquals("Great", $password);

    }


//    public function testBuildCutSheets()
//    {
//
//        $cutSheet = new cutSheet();
//
//        $json ='{"module":"3","width":"204","height":"120","panels":"7","swingDoor":"right","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":"","location":"ext"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Straight Satin","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Inswing","panelMovementRight":6,"jobInfo":{"jobNumber":"A400","productionDate":"","dueDate":""}}';
//        $json2 ='{"module":"3","width":"196","height":"120","panels":"7","swingDoor":"right","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":"","location":"ext"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Straight Satin","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Inswing","panelMovementRight":6,"jobInfo":{"jobNumber":"A400","productionDate":"","dueDate":""}}';
//
//        $quote = json_decode($json);
//        $quote2 = json_decode($json2);
//
//        $cut=  $cutSheet->buildCutSheets(array($quote, $quote2));
//
//        $this->assertEquals(2,count($cut));
//
//    }
//
//    public function testBuildAndSaveCutSheets()
//    {
//
//        $cutSheet = new cutSheet();
//
//        $json ='{"module":"3","width":"204","height":"120","panels":"7","swingDoor":"right","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":"","location":"ext"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Straight Satin","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Inswing","panelMovementRight":6,"jobInfo":{"jobNumber":"A400","productionDate":"","dueDate":""}}';
//        $json2 ='{"module":"3","width":"196","height":"120","panels":"7","swingDoor":"right","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":"","location":"ext"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Straight Satin","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Inswing","panelMovementRight":6,"jobInfo":{"jobNumber":"A400","productionDate":"","dueDate":""}}';
//
//        $quote = json_decode($json);
//        $quote2 = json_decode($json2);
//
//        $sheets = array($quote,$quote2);
//
//        $result =   $cutSheet->buildAndSaveCutSheets(77, $sheets);
//
//        $this->assertTrue($result);
//    }
//

    /**
     * @group fracka1
     */
    public function testBuildAndSaveCutSheetsTest()
    {

        $cutSheet = new cutSheet();

        $json = '{"module":"3","width":"204","height":"120","panels":"7","swingDoor":"right","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":"","location":"ext"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Straight Satin","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Inswing","panelMovementRight":6,"jobInfo":{"jobNumber":"A400","productionDate":"","dueDate":""}}';
        $json2 = '{"module":"3","width":"196","height":"120","panels":"7","swingDoor":"right","glassChoice":"Glass Clear","glassOptions":["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":"","location":"ext"},"int":{"type":"woodStandard","name1":"Fir","name2":"undefined","hex":"null","buttonID":"woodStandard","subSectionID":"all","url":"images/colors/wood/Birch121.jpg"},"vinylColor":"","paintedColor":"NA","finishOptions":{"sameType":"no","numberOfSides":"oneSide"}},"hardware":"Straight Satin","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Inswing","panelMovementRight":6,"jobInfo":{"jobNumber":"A400","productionDate":"","dueDate":""}}';

        $json = '[{"module":"1","width":"80","height":"80","panels":"2","swingDoor":"left","glassChoice":"Low","glassOptions"
:["Argon"],"finishes":{"ext":{"type":"basic","name1":"Apollow White","name2":"undefined","hex":"#F8FCFE"
,"buttonID":"basic","subSectionID":"all","url":""},"int":{"type":"basic","name1":"Apollow White","name2"
:"undefined","hex":"#F8FCFE","buttonID":"basic","subSectionID":"all","url":""},"vinylColor":"","paintedColor"
:"NA","finishOptions":{"sameType":"yes","numberOfSides":"bothSides"}},"hardware":"Straight Satin","hardwareExtras"
:"None","installOptions":"","track":"5/8 Upstep","frame":"Aluminum Block","swingDirection":"Outswing"
,"panelMovementRight":0,"subModules":1,"note":"","jobInfo":{"firstName":"f","lastName":"f","orderDate"
:"--","dueDate":"01/01/1970","id":"S16963","doorType":"SMART Door","city":"","state":""}}]';

        $id = 16963;
        $realID = 9;
        $sheets = json_decode($json);

        $cutSheet = new CutSheet();
        $result = $cutSheet->buildAndSaveCutSheetsSales($id, $sheets, $realID);


        $this->assertTrue($result);
    }

}
