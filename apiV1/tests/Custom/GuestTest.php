<?php

require_once "./customClasses/WindowSystem.php";
require_once "./customClasses/AccessPrivateProtectedMethodProperties.php";

class GuestCustomTest extends AccessPrivateProtectedMethodProperties
{

    private $guestData;

    public function setUp()
    {

//        $userData = Array("email" => "fracka@fracka.com", "password" => "Great");
        $guest = new Guest();
        $guestData = array ("name" => "Fracka Future", "email" => "fracka@fracka.com", "zip" => "78749" , "phone" => "512-632-4897");

        $json = json_encode($guestData);
        $this->guestData = json_decode($json);

        $this->testClass = $guest;
        parent::setup();

    }

//    public function testConstruct()
//    {
//        $email = $this->getProperty("email");
//        $password = $this->getProperty("password");
//
//        $this->assertEquals("fracka@fracka.com", $email);
//        $this->assertEquals("Great", $password);
//
//    }

    public function testNewGuest()
    {


        $guest = new Guest();

        $result = $guest->newGuest($this->guestData);
        $this->assertTrue(isset($result['id']));
        $this->assertTrue(isset($result['token']));
        $this->assertEquals($result['type'],"guest");


    }


    public function testGetGuest()
    {

        $guestID = 1;

        $guest = new Guest();
        $result = $guest->getGuest($guestID);
        $this->assertEquals($result["name"], "Fracka Future");
        $this->assertEquals($result["email"], "fracka@fracka.com");



    }


    /**
     *  ok
     *
     * @expectedException Exception
     * @expectedExceptionMessage missing parameter name in newGuest
     */
    public function testNewGuestExceptionName()
    {

        $guest = new Guest();
        unset ($this->guestData->name);
        $result = $guest->newGuest($this->guestData);

    }

    /**
     *  ok
     *
     * @expectedException Exception
     * @expectedExceptionMessage missing parameter email in newGuest
     */
    public function testNewGuestExceptionEmail()
    {

        $guest = new Guest();
        unset ($this->guestData->email);
        $result = $guest->newGuest($this->guestData);

    }

    /**
     *  ok
     *
     * @expectedException Exception
     * @expectedExceptionMessage missing parameter zip in newGuest
     */
    public function testNewGuestExceptionZip()
    {

        $guest = new Guest();
        unset ($this->guestData->zip);
        $result = $guest->newGuest($this->guestData);

    }
}
