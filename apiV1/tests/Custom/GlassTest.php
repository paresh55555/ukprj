<?php

require_once "./customClasses/BuilderGlass.php";
require_once "./customClasses/GlassOption.php";

/**
 * Class GlassTest
 *
 * @group glass
 */
class GlassTest extends PHPUnit_Framework_TestCase
{

    public function testGetGlassDataWithName () {

        $nameOfOption = 'Unglazed';
        $builder = new BuilderGlass();
        $dataArray = $builder->getGlassByModuleAndNameOfOption(3, $nameOfOption);
        $data = $dataArray[0];
        $data['width'] = 200 * 25.4;
        $data['height'] = 60 * 25.4;
        $data['panels'] = 5;
        $data['swings'] = 2;

        $json = json_encode($data);

        $glassOption = new GlassOption();
        $glassOption->setOptionFromJSON($json);

        $glassOption->setGlassHeight();
        $glassOption->setGlassWidth();

        $this->assertEquals($glassOption->glassHeight, 1310);
        $this->assertEquals($glassOption->glassWidth, 867);
    }
}
