<?php


require_once "./customClasses/Lead.php";
require_once "./customClasses/Quote.php";
require_once "./customClasses/Guest.php";

/**
 * Class LeadTest
 *
 * @group leads
 */
class LeadTest extends PHPUnit_Framework_TestCase
{

    public function testGetLeads()
    {

        $leads = new Lead();
        $results = $leads->getLeads();
        $this->assertTrue(is_array($results));

    }


    public function testGetAssignedLeads()
    {

        $leads = new Lead();
        $results = $leads->getAssignedLeads();
        $this->assertTrue(is_array($results));

    }

    /**
     * @group f8
     */
    public function testTransferAllLeadsToSalesPerson()
    {

        $leads = new Lead();
        $results = $leads->transferAllLeadsToSalesPerson('18');
        print_r($results);


    }




    public function testRevokeLeads()
    {

        $cart = json_decode ('{"Cart100232":{"name":"Great"}}');

        $newQuote = (object) array(
            'total' => 100.59,
            'Cart' => $cart,
            'guest' => 3,
            'type' => "quote"
        );

        $quote = new Quote();
        $id  = $quote->addGuestQuote($newQuote);
        $this->assertTrue(is_numeric($id));

        $salesPerson = 9;
        $quoteID = $quote->transferQuote($id, $salesPerson);

        $leads = new Lead();
        $results = $leads->revokeLead($id);

        $this->assertEquals($results, "Success");

        $quote->deleteGuestQuote($id);

    }

}
