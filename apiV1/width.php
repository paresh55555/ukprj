<?php
/**
 * Step 1: Require the Slim Framework
 *
 * If you are not using Composer, you need to require the
 * Slim Framework and register its PSR-0 autoloader.
 *
 * If you are using Composer, you can skip this step.
 */
require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

require_once 'customClasses/PriceEngine.php';

/**
 * Step 2: Instantiate a Slim application
 *
 * This example instantiates a Slim application using
 * its default settings. However, you will usually configure
 * your Slim application now by passing an associative array
 * of setting names and values into the application constructor.
 */
$app = new \Slim\Slim(array(
    'debug' => true
));


/**
 * Step 3: Define the Slim application routes
 *
 * Here we define several Slim application routes that respond
 * to appropriate HTTP request methods. In this example, the second
 * argument for `Slim::get`, `Slim::post`, `Slim::put`, `Slim::patch`, and `Slim::delete`
 * is an anonymous function.
 */

class BindParam{
    private $values = array(), $types = '';

    public function add( $type, &$value ){
        $this->values[] = $value;
        $this->types .= $type;
    }

    public function get(){
        return array_merge(array($this->types), $this->values);
    }
}


$app->post('/windows', function () use ($app) {

    $data = $app->request()->post();
    $data = $app->request->getBody();

    $size = json_decode($data);
    $width = $size->{'width'};
    $height =  $size->{'height'};
//    echo $height;
    $priceEngine = new PriceEngine();
    $priceEngine->width = $width;
    $priceEngine->height = $height;
    $priceEngine->panels = 5;
    $priceEngine->getTotalPrices();


    $total = array('total' => $priceEngine->totalPrice);
    header("Content-Type: application/json");
    echo json_encode($total);

//    $total->{'total'} = $height * $height;
//    echo json_encode($total);

});

function makeValuesReferenced($arr){
    $refs = array();
    foreach($arr as $key => $value)
        $refs[$key] = &$arr[$key];
    return $refs;

}

$app->get('/buttons', function () use ($app) {
    header("Content-Type: application/json");
    $db = connect_db();
    $buttonType = $app->request()->params('type');
    $typeOfButtons = explode(",", $buttonType);

    $query =  "SELECT * from photos p,buttons b  where active = 1 and b.photo_id = p.id and (";
//
//    $query = 'SELECT * FROM users WHERE ';
    $bindParam = new BindParam();
    $qArray = array();

    foreach ($typeOfButtons as &$button) {
        $qArray[] = 'b.type = ?';
        $bindParam->add('s',$button);
    }
//    var_dump($bindParam->get());
    $query .= implode(' OR ', $qArray);

    $query .= ") order by myOrder";

//   echo $query . '<br/>';

    $stmt = $db->stmt_init();
    $stmt= $db->prepare($query) ;
    $t1 = "test";
    $t2 = "test2";
    call_user_func_array( array($stmt, 'bind_param'), makeValuesReferenced($bindParam->get()));
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[]= $options;
    }
//    header("Content-Type: application/json");
    echo json_encode($data);
    exit();


exit();
//call_user_func_array( array($stm, 'bind_param'), $bindParam->get());
//
//    print_r ($sqlType);
//    /exit();

    if ($sqlType) {
        $sql =  "SELECT * from buttons b,photos p  where active = 1 and ".$sqlType." and b.photo_id = p.id order by myOrder";
//        print_r ($sql);
        $stmt = $db->stmt_init();
        $stmt= $db->prepare($sql) ;
        $t1 = "test";
        $t2 = "test2";
        $stmt->bind_param("ss", $typeOfButtons);
    }
    else {
        $sql =  "SELECT * from buttons  where active = 1 order by myOrder";
        $stmt= $db->prepare($sql) ;
    }
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[]= $options;
    }
//    header("Content-Type: application/json");
    echo json_encode($data);
    exit();

});


$app->get('/sections', function () use ($app) {

    $db = connect_db();

    $sql =  "SELECT * from sections  where active = 1 order by myOrder ";
//    $stmt= $db->prepare($sql) ;

    $result = mysqli_query($db,$sql);
    $data = array();
    while($options = mysqli_fetch_array($result,MYSQLI_ASSOC)) {

        $sqlSub =  "SELECT * from sub_sections where section_id = ".$options["id"]." and active = 1 order by myOrder";
        $resultSub = mysqli_query($db,$sqlSub);

        $subSectionResults = array();
        while ($sub_section = mysqli_fetch_array($resultSub, MYSQLI_ASSOC)  ) {
            $subSectionResults[]= $sub_section;
        }

        $options["sub_sections"] = $subSectionResults;
        $data[]= $options;
    }

    header("Content-Type: application/json");
    echo json_encode($data);
    exit();

});


$app->get('/doors', function () use ($app) {

    header("Content-Type: application/json");

    $width = $app->request()->params('width');
    $height = $app->request()->params('height');
    $parameters  = $app->request()->params('parameters');
    $db = connect_db();

    if ($parameters) {
        $sql =  "SELECT * from confs c  where c.type='doors' ";
        $stmt= $db->prepare($sql) ;
        $stmt->execute();
        $results = $stmt->get_result();
        $doorsConfs = $results->fetch_assoc();

        echo  $doorsConfs['settings'];
        exit();
    }

//    echo "$width";
    $priceEngine = new PriceEngine();
    $priceEngine->width = $width;
    $priceEngine->getNumberOfPanels();
//    $this->assertEquals(3,count($priceEngine->panels));

    $result = array ("doors" => $priceEngine->panels );

//    $result = array ("doors" => array ($sections, $sections + 1 , $sections + 2 ) );
    echo json_encode($result);

});


$app->get('/swing', function () use ($app) {

    header("Content-Type: application/json");

    $db = connect_db();
    // query the database
    $result = $db->query( 'SELECT name from swing' );
    $data = array();

    while ($bandArr = $result->fetch_assoc(MYSQLI_NUM)) {
        $data[]= $bandArr[0];

    }

    echo json_encode($data);

//    $sql = "select * FROM wine ORDER BY name";
////    echo "$sql";
//    echo "welcome to windows";
//    $width = $app->request()->params('width');
//    $height = $app->request()->params('height');
//    echo "Width: $width & Height: $height";

});

$app->post('/swing', function () use ($app) {
//    $data = $app->request()->post();
    $data = $app->request->getBody();
//  print_r ($data);
    $swings = json_decode($data);
    $db = connect_db();
    header("Content-Type: application/json");
//    echo json_encode($swings);
    foreach ($swings as &$value) {
        $v1="'" . $db->real_escape_string($value) . "'";
//        $sql="INSERT INTO tbl (col1_varchar, col2_number) VALUES ($v1,10)";

        $sql = "insert into swing (name) VALUES ($v1) ";
        if($db->query($sql) === false) {
            trigger_error('Wrong SQL: ' . $sql . ' Error: ' . $db->error, E_USER_ERROR);
        } else {
            $last_inserted_id = $db->insert_id;
            $affected_rows = $db->affected_rows;
        }
    }
    echo json_encode($swings);

});

$app->get('/options', function () use ($app) {


    $type = $app->request()->params('type');
    $base = $app->request()->params('base');
    $db = connect_db();


//    $sql =  "SELECT * from options";
//    if ($type){
        $sql =  "SELECT * from options where type=? and base=?";
//    }

    $stmt = $db->stmt_init();
    $stmt= $db->prepare($sql) ;


    /* bind parameters for markers */
    $stmt->bind_param("ss", $type, $base);
    $stmt->execute();
    /* Fetch result to array */
    $results = $stmt->get_result();

//    $result = $db->query( $sql );
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[]= $options;
    }

    header("Content-Type: application/json");
    echo json_encode($data);

});

$app->post('/options', function () use ($app) {
//    $data = $app->request()->post();
    $data = $app->request->getBody();

    $option = json_decode($data);
    if (empty($option)) {
        echo "Error with Parameters";
        return;
        // do stuff
    }
    $db = connect_db();
    insertOption($db,$option);

    header("Content-Type: application/json");
    echo json_encode($option);

});




// GET route
$app->get(
    '/',
    function () {
        $template = <<<EOT
<!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8"/>
            <title>Slim Frakca Framework for PHP 5</title>
            <style>
                html,body,div,span,object,iframe,
                h1,h2,h3,h4,h5,h6,p,blockquote,pre,
                abbr,address,cite,code,
                del,dfn,em,img,ins,kbd,q,samp,
                small,strong,sub,sup,var,
                b,i,
                dl,dt,dd,ol,ul,li,
                fieldset,form,label,legend,
                table,caption,tbody,tfoot,thead,tr,th,td,
                article,aside,canvas,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section,summary,
                time,mark,audio,video{margin:0;padding:0;border:0;outline:0;font-size:100%;vertical-align:baseline;background:transparent;}
                body{line-height:1;}
                article,aside,details,figcaption,figure,
                footer,header,hgroup,menu,nav,section{display:block;}
                nav ul{list-style:none;}
                blockquote,q{quotes:none;}
                blockquote:before,blockquote:after,
                q:before,q:after{content:'';content:none;}
                a{margin:0;padding:0;font-size:100%;vertical-align:baseline;background:transparent;}
                ins{background-color:#ff9;color:#000;text-decoration:none;}
                mark{background-color:#ff9;color:#000;font-style:italic;font-weight:bold;}
                del{text-decoration:line-through;}
                abbr[title],dfn[title]{border-bottom:1px dotted;cursor:help;}
                table{border-collapse:collapse;border-spacing:0;}
                hr{display:block;height:1px;border:0;border-top:1px solid #cccccc;margin:1em 0;padding:0;}
                input,select{vertical-align:middle;}
                html{ background: #EDEDED; height: 100%; }
                body{background:#FFF;margin:0 auto;min-height:100%;padding:0 30px;width:440px;color:#666;font:14px/23px Arial,Verdana,sans-serif;}
                h1,h2,h3,p,ul,ol,form,section{margin:0 0 20px 0;}
                h1{color:#333;font-size:20px;}
                h2,h3{color:#333;font-size:14px;}
                h3{margin:0;font-size:12px;font-weight:bold;}
                ul,ol{list-style-position:inside;color:#999;}
                ul{list-style-type:square;}
                code,kbd{background:#EEE;border:1px solid #DDD;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:0 4px;color:#666;font-size:12px;}
                pre{background:#EEE;border:1px solid #DDD;border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px;padding:5px 10px;color:#666;font-size:12px;}
                pre code{background:transparent;border:none;padding:0;}
                a{color:#70a23e;}
                header{padding: 30px 0;text-align:center;}
            </style>
        </head>
        <body>
            <header>
                <a href="http://www.slimframework.com"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHIAAAA6CAYAAABs1g18AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABRhJREFUeNrsXY+VsjAMR98twAo6Ao4gI+gIOIKOgCPICDoCjCAjXFdgha+5C3dcv/QfFB5i8h5PD21Bfk3yS9L2VpGnlGW5kS9wJMTHNRxpmjYRy6SycgRvL18OeMQOTYQ8HvIoJKiiz43hgHkq1zvK/h6e/TyJQXeV/VyWBOSHA4C5RvtMAiCc4ZB9FPjgRI8+YuKcrySO515a1hoAY3nc4G2AH52BZsn+MjaAEwIJICKAIR889HljMCcyrR0QE4v/q/BVBQva7Q1tAczG18+x+PvIswHEAslLbfGrMZKiXEOMAMy6LwlisQCJLPFMfKdBtli5dIihRyH7A627Iaiq5sJ1ThP9xoIgSdWSNVIHYmrTQgOgRyRNqm/M5PnrFFopr3F6B41cd8whRUSufUBU5EL4U93AYRnIWimCIiSI1wAaAZpJ9bPnxx8eyI3Gt4QybwWa6T/BvbQECUMQFkhd3jSkPFgrxwcynuBaNT/u6eJIlbGOBWSNIUDFEIwPZFAtBfYrfeIOSRSXuUYCsprCXwUIZWYnmEhJFMIocMDWjn206c2EsGLCJd42aWSyBNMnHxLEq7niMrY2qyDbQUbqrrTbwUPtxN1ZZCitQV4ZSd6DyoxhmRD6OFjuRUS/KdLGRHYowJZaqYgjt9Lchmi3QYA/cXBsHK6VfWNR5jgA1DLhwfFe4HqfODBpINEECCLO47LT/+HSvSd/OCOgQ8qE0DbHQUBqpC4BkKMPYPkFY4iAJXhGAYr1qmaqQDbECCg5A2NMchzR567aA4xcRKclI405Bmt46vYD7/Gcjqfk6GP/kh1wovIDSHDfiAs/8bOCQ4cf4qMt7eH5Cucr3S0aWGFfjdLHD8EhCFvXQlSqRrY5UV2O9cfZtk77jUFMXeqzCEZqSK4ICkSin2tE12/3rbVcE41OBjBjBPSdJ1N5lfYQpIuhr8axnyIy5KvXmkYnw8VbcwtTNj7fDNCmT2kPQXA+bxpEXkB21HlnSQq0gD67jnfh5KavVJa/XQYEFSaagWwbgjNA+ywstLpEWTKgc5gwVpsyO1bTII+tA6B7BPS+0PiznuM9gPKsPVXbFdADMtwbJxSmkXWfRh6AZhyyzBjIHoDmnCGaMZAKjd5hyNJYCBGDOVcg28AXQ5atAVDO3c4dSALQnYblfa3M4kc/cyA7gMIUBQCTyl4kugIpy8yA7ACqK8Uwk30lIFGOEV3rPDAELwQkr/9YjkaCPDQhCcsrAYlF1v8W8jAEYeQDY7qn6tNGWudfq+YUEr6uq6FZzBpJMUfWFDatLHMCciw2mRC+k81qCCA1DzK4aUVfrJpxnloZWCPVnOgYy8L3GvKjE96HpweQoy7iwVQclVutLOEKJxA8gaRCjSzgNI2zhh3bQhzBCQQPIHGaHaUd96GJbZz3Smmjy16u6j3FuKyNxcBarxqWWfYFE0tVVO1Rl3t1Mb05V00MQCJ71YHpNaMcsjWAfkQvPPkaNC7LqTG7JAhGXTKYf+VDeXAX9IvURoAwtTFHvyYIxtnd5tPkywrPafcwbeSuGVwFau3b76NO7SHQrvqhfFE8kM0Wvpv8gVYiYBlxL+fW/34bgP6bIC7JR7YPDubcHCPzIp4+cum7U6NlhZgK7lua3KGLeFwE2m+HblDYWSHG2SAfINuwBBfxbJEIuWZbBH4fAExD7cvaGVyXyH0dhiAYc92z3ZDfUVv+jgb8HrHy7WVO/8BFcy9vuTz+nwADAGnOR39Yg/QkAAAAAElFTkSuQmCC" alt="Slim"/></a>
            </header>
            <h1>Welcome to Slim!</h1>
            <p>
                Congratulations! Frakca Your Slim application is running. If this is
                your first time using Slim, start with this <a href="http://www.slimframework.com/learn" target="_blank">"Hello World" Tutorial</a>.
            </p>
            <section>
                <h2>Get Started</h2>
                <ol>
                    <li>The application code is in <code>index.php</code></li>
                    <li>Read the <a href="http://docs.slimframework.com/" target="_blank">online documentation</a></li>
                    <li>Follow <a href="http://www.twitter.com/slimphp" target="_blank">@slimphp</a> on Twitter</li>
                </ol>
            </section>
            <section>
                <h2>Slim Framework Community</h2>

                <h3>Support Forum and Knowledge Base</h3>
                <p>
                    Visit the <a href="http://help.slimframework.com" target="_blank">Slim support forum and knowledge base</a>
                    to read announcements, chat with fellow Slim users, ask questions, help others, or show off your cool
                    Slim Framework apps.
                </p>

                <h3>Twitter</h3>
                <p>
                    Follow <a href="http://www.twitter.com/slimphp" target="_blank">@slimphp</a> on Twitter to receive the very latest news
                    and updates about the framework.
                </p>
            </section>
            <section style="padding-bottom: 20px">
                <h2>Slim Framework Extras</h2>
                <p>
                    Custom View classes for Smarty, Twig, Mustache, and other template
                    frameworks are available online in a separate repository.
                </p>
                <p><a href="https://github.com/codeguy/Slim-Extras" target="_blank">Browse the Extras Repository</a></p>
            </section>
        </body>
    </html>
EOT;
        echo $template;
    }
);


$app->get('/books/:id', function ($id) {
    //Show book identified by $id
    echo "This is an $id";
});


// POST route
$app->post(
    '/post',
    function () {
        echo 'This is a POST route';
    }
);

// PUT route
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);

// PATCH route
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});

// DELETE route
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);

/**
 * Step 4: Run the Slim application
 *
 * This method should be called last. This executes the Slim application
 * and returns the HTTP response to the HTTP client.
 */
$app->run();
