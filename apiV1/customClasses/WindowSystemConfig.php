<?php

class WindowSystemConfig
{

    public function __construct()
    {

    }


    public function buildWindowSystemConfig($json)
    {
        $passedOptions = json_decode($json);

        $transom =  array("transom" => array("width" => $passedOptions->frameWidth, "height" => $passedOptions->frameHeight));
        $transom2 =  array("transom" => array("width" => $passedOptions->frameWidth, "height" => $passedOptions->frameHeight));
        $mullion =  array("mullion" => array("width" => $passedOptions->frameWidth, "height" => $passedOptions->frameHeight));
        $frame =    array("frame" => array("width" => $passedOptions->frameWidth, "height" => $passedOptions->frameHeight));

        $options = array($frame, $mullion, $transom, $transom2);
        $config = array("module" => $passedOptions->module, "options" => $options);

        return $config;
    }
}


