<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class DiscountSet
{

    public $discounts = array();
    private $basePrice;
    private $trueBasePrice;
    private $maxDiscount;
    private $configMinimums;
    private $setID;
    private $totalDiscount;

    public function __construct()
    {

        $this->configMinimums = array ('setID', 'basePrice', 'trueBasePrice', 'maxDiscount');
    }


    public function validateOptionsWithMinimums($options, $validateArray)
    {

        foreach ($validateArray as $option) {
            if (!isset($options[$option])) {
                throw new Exception('missing config ' . $option . ' in DiscountSet');
            }
        }
        return true;

    }

    public function initWithConfig($config)
    {
        $this->validateOptionsWithMinimums($config,$this->configMinimums);

        $this->basePrice = $config['basePrice'];
        $this->trueBasePrice = $config['trueBasePrice'];
        $this->maxDiscount = $config['maxDiscount'];
        $this->setID = $config['setID'];


    }

    public function initWithConfigAndBuildDiscounts($config)
    {
        $this->initWithConfig($config);

        $discounts = $this->getDiscounts();
        $this->buildDiscounts($discounts);


    }


    public function getTotalDiscount()
    {
        return $this->totalDiscount;
    }
    public function getDiscounts()
    {

        if (!isset($this->setID)) {
            throw new Exception('init not run missing setID ');
        }

        $db = connect_db();
        $sql = "SELECT *  from discounts where setID=? ";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $this->setID);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();

        while ($discount = $results->fetch_assoc()) {
            $discounts[] = $discount;
        }

        return $discounts;
    }

    public function buildDiscounts($discountsData)
    {

        foreach ($discountsData as $discount) {

            $discount['basePrice'] = $this->basePrice;
            $json = json_encode($discount);
            $discountObj = new Discount();
            $discountObj->setOptionFromJSON($json);
            $this->totalDiscount +=  $discountObj->totalDiscount;
            $this->discounts[] = $discountObj;
        }


    }


    public function buildDiscountSet($setID)
    {
        $db = connect_db();

        $sql = " SELECT quotesGuest.id, name, zip, phone, quantity, total, quoted, `type`, transferred
                   from quotesGuest,guest where
                   guest=guest.id and status ='new'  ";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];
            $options['zip'] = preg_replace('/\D/', '', $options['zip']);

            $options['phone'] = preg_replace('/\D/', '', $options['phone']);
            $options['phone'] = "(" . substr($options['phone'], 0, 3) . ") " . substr($options['phone'], 3, 3) . "-" . substr($options['phone'], 6);

            $quoteDate = strtotime($options['quoted']);
            $options['quoted'] = date('m/d/Y', $quoteDate);
            $options['id'] = "SQ" . $options['id'];
            $options['total'] = money_format('%n', $options['total']);

            $data[] = $options;
        }

        return $data;

    }


}


?>
