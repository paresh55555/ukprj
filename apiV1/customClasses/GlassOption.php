<?php

class glassOption
{
    public $width;
    public $height;
    public $panels;


    public $swings = 1;
    public $nameOfOption;
    public $costSquareMeter;
    public $costPerPanel;

    public $glassHeight;
    public $glassWidth;

    public $height_cut_size_formula;
    public $height_cut_size;

    public $width_cut_size_formula;
    public $width_cut_size;

    public $quantity;
    public $breakDown = Array();

    public $glassOrOption;

    public $totalPrice;

    public function buildDefault()
    {

        $data['width'] = 200 * 25.4;
        $data['height'] = 60 * 25.4;
        $data['panels'] = 5;
        $data['nameOfOption'] = "GLASS SOLABAN Awesome";
        $data['costSquareMeter'] = 40.3;
        $data['height_cut_size_formula'] = 'height-210.83';
        $data['width_cut_size_formula'] = '(width-78.74)/panels -133.35';
        $data['glassOrOption'] = 'glass';
        $data['module'] = 1;
        $data['swings'] = 2;
        $json = json_encode($data);
        return ($json);

    }


    public function setOptionFromJSON($json)
    {

        $optionValues = json_decode($json);
        if (json_last_error()) {
            $this->logit("#####################################################ERROR", "");
            return false;
        } else {
            if (isset($optionValues->{"width"}) and isset($optionValues->{"height"}) and isset($optionValues->{"panels"})) {
                $this->width = $optionValues->{"width"};
                $this->height = $optionValues->{"height"};
                $this->panels = $optionValues->{"panels"};
            }


            $this->nameOfOption = $optionValues->{"nameOfOption"};
            $this->glassOrOption = $optionValues->{"glassOrOption"};
            $this->costSquareMeter = $optionValues->{"costSquareMeter"};
            $this->costPerPanel = $optionValues->{"costPerPanel"};
//      $this->costSquareMeter = $optionValues->{"costSquareMM"};
            $this->height_cut_size_formula = $optionValues->{"height_cut_size_formula"};
            $this->width_cut_size_formula = $optionValues->{'width_cut_size_formula'};
            return true;
        }

    }

    public function processFormula($formula)
    {

        if (!$formula) {
            $this->logit("ERROR", "");

            return 0;
        }

        $formula = strtolower($formula);

        $formula = str_replace("width", '$this->width', $formula);
        $formula = str_replace("height", '$this->height', $formula);
        $formula = str_replace("panels", '$this->panels', $formula);
        $formula = str_replace("swings", '$this->swings', $formula);

        eval('$formula = ' . $formula . ';');

        return ($formula);

    }

    public function setGlassWidth()
    {

        $glassWidth = $this->processFormula($this->width_cut_size_formula);
        $this->glassWidth = round($glassWidth, 2);
    }

    public function setGlassHeight()
    {
        $glassHeight = $this->processFormula($this->height_cut_size_formula);
        $this->glassHeight = round($glassHeight, 2);
    }

    public function getTotalPrice($width, $height, $panels)
    {
        $this->width = $width;
        $this->height = $height;
        $this->panels = $panels;

        $this->setGlassWidth();
        $this->setGlassHeight();

        if (empty($this->costSquareMeter)) {
            $totalPrice = $this->costPerPanel * $this->panels;
        }
        else {
            $totalPrice = ($this->glassHeight * $this->glassWidth) / 1000000 * $this->costSquareMeter * $this->panels;

        }

        $this->totalPrice = round($totalPrice, 2);
        $this->quantity = $this->panels;
        $this->createBreakDown();
        return ($this->totalPrice);


    }

    public function createBreakDown()
    {

        setlocale(LC_MONETARY, 'en_US');
        $breakdown['name'] = $this->nameOfOption;
        $breakdown['Glass Height Inches'] = $this->glassHeight;
        $breakdown['Glass Height MM'] = round($this->glassHeight * 25.4, 2);
        $breakdown['Glass Width Inches'] = $this->glassWidth;
        $breakdown['Glass Width MM'] = round($this->glassWidth * 25.4, 2);
        $breakdown['Quantity'] = $this->quantity;
        $breakdown['GlassOrOption'] = $this->glassOrOption;
        $breakdown['Total Cost'] = money_format('%i', $this->totalPrice);
        $this->breakDown = $breakdown;


    }

    public function displayBreakDown()
    {
        $this->createBreakDown();
        echo "\n";
        foreach ($this->breakDown as $key => $value) {
            echo "$key: $value \n";
        }
    }


}

?>