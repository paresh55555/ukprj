<?php

require_once "./customClasses/PerItemOption.php";

class PerItemOptionSet    {

  private $total;
  public $options = Array();
  public $breakDown;

  public function buildSetFromArrayOfJSON($arrayOfJSON) {

    $error = null;
    $this->options = [];

    if (is_array($arrayOfJSON)) {
      foreach ($arrayOfJSON as &$myOption ) {
        $json = json_encode($myOption);
        $sizeOption = new PerItemOption();
        $sizeOption->setOptionFromJSON($json);
        $this->options[] = $sizeOption ;
      }
    }
    else {
      $error = "Not an array";
    }
    return ($error);

  }


  public function getTotal($width,$height,$panels) {

    $this->setTotal($width,$height,$panels);
    return ($this->total);

  }

  public function setTotal($width,$height,$panels) {

    $this->total = 0;
    foreach ($this->options as &$myOption ) {
      $myOption->setTotalPrice($width,$height,$panels);
      $this->total = $this->total + $myOption->totalPrice;
    }
    $errors = null;
    $this->displayBreakDown();
    return ($errors);


  }

  public function displayBreakDown() {

    $options = Array ();

    foreach ($this->options as &$myOption ) {
      $options[] = $myOption->breakDown;
    }
    $this->breakDown['items'] = $options;

  }

  public function setSwings($numberOfSwings) {
    foreach ($this->options as &$myOption ) {
      $myOption->swings = $numberOfSwings;
    }


  }

}





?>
