<?php

class Door
{
    protected $options = array ("width", "height", "panels", "colorOption", "glassChoice", "kickPlate", "swingDoor",
        "flushBottom", "glassOptions", "hardware", "hardwareExtras", "installOptions", "vinylType", "module",
         "panelsRight", "track", "swingDirection", "frame", "finishes", "panelMovementRight", "sill", "addOns", "trickle", "bayPole", "screen", "screenUS");

    public function getOptions()
    {
        return $this->options;
    }

    public function getFixedWithByGroup($group)
    {

        $db = connect_db();
        $sql = "SELECT width, panels from  SQ_fixedPanels where `group`=? ";
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $group);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $doors = array();

        while ($door = $results->fetch_assoc()) {
            $doors[]= $door;
        }

        return $doors;
    }


    public function getFixedHeightByGroup($group)
    {

        $db = connect_db();
        $sql = "SELECT * from  SQ_fixedPanelHeight where `group`=? ";
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $group);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $doors = array();

        while ($door = $results->fetch_assoc()) {
            $doors[]= $door;
        }

        return $doors;
    }

}

