<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

class UISettings {
    
    public function getSettings()
    {
        $db = connect_db();
        $sql = "SELECT * from ui_settings";

        $stmt = $db->prepare($sql);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        while ($settings = $results->fetch_assoc()) {
            unset($settings['id']);
            $data[] = $settings;
        }
        return $data;

    }
    
}
