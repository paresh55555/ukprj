<?php

use GuzzleHttp\Client;

require_once 'lib/mysql.php';
require_once 'Auth.php';
require_once 'Guest.php';

class User
{
    protected $name;
    private $email;
    private $password;
    private $validation;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }


    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function getValidation()
    {
        return $this->validation;
    }


    public function talk()
    {
        return "Hello world!";
    }


    public function __construct()
    {

    }


    public function setValidation($id)
    {

        $this->validation = Array("salesPerson" => $id, "token" => unique());


    }

    public function getUsers()
    {


        $db = connect_db();
        $sql = "SELECT *  from users  ";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->execute();
        $results = $stmt->get_result();

        while ($user = $results->fetch_assoc()) {
            $users[] = $user;
        }

        return ($users);

    }


    public function getUsersDealers($salesPerson)
    {

        $db = connect_db();

        $sql = "SELECT users.id, `type`, `email`, `name`,extended,defaults,prefix, firstName, lastName, companyName, address1, address2, city, state, zip, logoQuote,
                        logoSite, link1, link1text, link2, link2text, SQ_dealers.phone, backgroundColor  from users, SQ_dealers  where users.id=? and users.dealer = SQ_dealers.id ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $salesPerson);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $data = $this->buildDealer($data);

        return ($data['dealer']);

    }

    public function getManager($id)
    {
        $results = $this->getUser($id);
        return $results['manager'];
    }

    public function getUser($id)
    {
        $db = connect_db();
        $sql = "SELECT *  from users where id=?  ";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        if (isset($data)) {
            if ($data['id'] > 0) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }

    public function getActiveUser($id)
    {
        $db = connect_db();
        $sql = "SELECT * from users where id=? and active = 1";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        if (isset($data)) {
            if ($data['id'] > 0) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function makeBlankDefault()
    {

        $discounts = array("amount" => 0, "id" => 0, "name" => "Discount", "showOnQuote" => true, "type" => "flat");

        $yesOnQuote = array("amount" => 0, "showOnQuote" => true);
        $noOnQuote = array("amount" => 0, "showOnQuote" => false);

        $totals = array("code" => "YZ-00", "tax" => $noOnQuote,
            "shipping" => $noOnQuote, "installation" => $noOnQuote,
            "cost" => $yesOnQuote, "preCost" => 10000,
            "finalTotal" => $yesOnQuote, "subTotal" => $yesOnQuote,
            "extra" => array("name" => "", "amount" => 0, "showOnQuote" => false),
            "screens" => array("name" => "", "amount" => 0, "showOnQuote" => false),
            "discountTotal" => $yesOnQuote);

        $default = array("Discounts" => array($discounts), "Totals" => $totals);

        return $default;


    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getDefaults($id)
    {

        $db = connect_db();
        $sql = "SELECT defaults  from users where id=? ";


        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if (empty($data['defaults'])) {
            $response = $this->makeBlankDefault();
            updateOptionID($db, $response, "users", $id);
            exit();
        } else {
            $response = json_decode($data['defaults']);
        }

        return $response;
    }

    public function getV3Token($email = '', $password = '')
    {
        return $this->getToken($email, $password, 'apiV3');
    }

    public function getV4Token($email = '', $password = '')
    {
        return $this->getToken($email, $password, 'api/V4');
    }


    public function getToken($email = '', $password = '', $site)
    {
        $url = "https://".$_SERVER['SERVER_NAME']."/$site/token";

        $client = new \GuzzleHttp\Client([
            'defaults' => [
                'auth' => [$email, $password],
            ]
        ]);


        try {
            $response = $client->post($url, [
                'auth' => [
                    $email,
                    $password
                ]]);


            $results = json_decode($response->getBody());

        } catch (\Exception $e) {

            $results = null;
        }




        if (!empty($results->token)) {
            return $results->token;
        }
    }



    /**
     * @param string $email
     * @param string $password
     *
     * @return array|bool
     */
    public function login($email = '', $password = '', $token = null)
    {

        if (!$email) {
            return false;
        }
        if (!$password) {
            return false;
        }

        $db = connect_db();
        $sql = "SELECT salesAdmin, anytimeQuoteEdits, superSales, users.id, `type`, `email`, `name`, pipelinerId, extended, defaults, prefix, password, firstName, lastName, companyName, address1, address2, city, state,
                zip, logoQuote, logoSite, link1, link1text, link2, link2text, SQ_dealers.phone, backgroundColor  from users, SQ_dealers
                where email = ?  and users.dealer = SQ_dealers.id and active = 1";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("s", $email);
        $stmt->execute();


        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if (isset($data)) {

            if ($data['password'] != $password) {

                if ($password == '1234567891234567') {

                    $auth = new Auth();
                    $validToken = $auth->validateToken($token);

                    if (empty($validToken['id'])) {

                        return false;

                    } else {
                        if ($validToken['id'] != $data['id']) {
                            return false;
                        }
                    }

                } else {
                    return false;
                }
            }

            if ($data['id'] > 0) {
                $config = Array("type" => $data['type'], "id" => $data['id']);
                $auth = new Auth();
                $data['token'] = $auth->login($config);
                if ($data['superSales'] == 1) {
                    $data['superToken'] = $auth->createSuperToken($config);
                }
                $data = $this->buildDealer($data);

                if (empty ($data['defaults'])) {
                    $data['defaults'] = $this->makeBlankDefault();
                    $updateDefaults = array('defaults' => json_encode($data['defaults']));
                    updateOptionID($db, $updateDefaults, "users", $data['id']);


                } else {
                    $data['defaults'] = json_decode($data['defaults']);
                }

                $tokenV4 = $this->getV4Token($email, $data['password']);
                $data['v4Token'] = $tokenV4;
//                $token = $this->getV3Token($email, $data['password']);
//                $data['v3Token'] = $token;
//

                return $data;
            } else {
                return false;
            }
        } else {

            return false;
        }
    }


    public function buildDealer($data)
    {
        $dealer = array();

        $fields = array('firstName', 'lastName', 'companyName', 'address1', 'address2', 'city', 'state', 'zip', 'logoQuote', 'logoSite', 'link1', 'link1text', 'link2', 'link2text', 'phone', 'backgroundColor');

        foreach ($fields as $field) {
            $dealer[$field] = $data[$field];
            unset($data[$field]);
        }

        $data['dealer'] = $dealer;

        return $data;
    }


    public function getSalesPersonByPass($quote)
    {

        $guest = new Guest();
        $newGuest = $guest->getGuest($quote->guest);

        $zip = intval($newGuest['zip']);
        $db = connect_db();

        $sql = "SELECT *  from users, SQ_sales_terriority t where t.zip=? and t.salesPersonId = users.id and users.active = 2";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $newGuest['zip']);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if (!empty($data) ) {

            $data['guestEmail'] = $newGuest['email'];
            $data['guestName'] = $newGuest['name'];
            $data['guestPhone'] = $newGuest['phone'];
            $data['guestZip'] = $newGuest['zip'];
            $data['guestID'] = $newGuest['id'];
            return $data;
        }

        return false;
    }

    public function getSalesPersonForArea($quote)
    {
        $guest = new Guest();
        $newGuest = $guest->getGuest($quote->guest);

        $zip = intval($newGuest['zip']);
        $db = connect_db();

        $sql = "SELECT *  from users, SQ_sales_terriority t where t.zip=? and t.salesPersonId = users.id and users.active = 1";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $newGuest['zip']);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        //Check for BiPass Account Zip
        if (empty($data) && $zip != 0) {

            $sql = "SELECT *  from  SQ_sales_terriority t where t.zip=? and t.salesPersonId = 0 limit 1";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("s", $newGuest['zip']);

            $stmt->execute();
            /* Fetch result to array */
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();

            if (!empty($data)) {

                unset($data['id']);
                $data['guestEmail'] = $newGuest['email'];
                $data['guestName'] = $newGuest['name'];
                $data['guestPhone'] = $newGuest['phone'];
                $data['guestZip'] = $newGuest['zip'];
                $data['guestID'] = $newGuest['id'];

                return $data;
            }
        }


        if (empty($data) && $zip != 0) {

            $sql = "SELECT *  from users, SQ_sales_terriority t where t.zipLow<=? and t.zipHigh>=? and t.salesPersonId = users.id and users.active = 1";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ss", $zip, $zip);
            $stmt->execute();
            /* Fetch result to array */
            $result = $stmt->get_result();
            $data = $result->fetch_assoc();
        }

        unset($data['password']);
        unset($data['defaults']);

        $data['guestEmail'] = isset($newGuest['email']) ? $newGuest['email'] : '';
        $data['guestName'] = $newGuest['name'];
        $data['guestPhone'] = $newGuest['phone'];
        $data['guestZip'] = $newGuest['zip'];
        $data['guestID'] = $newGuest['id'];

        if (!empty($data['message'])) {
            $data['message'] = preg_replace('/@name/', $data['guestName'], $data['message']);
        }

        return $data;
    }

    public function getSalesPersonForId($quote, $id)
    {
        $guest = new Guest();
        $newGuest = $guest->getGuest($quote->guest);

        $data = $this->getActiveUser($id);

        unset($data['password']);
        unset($data['defaults']);

        $data['guestEmail'] = $newGuest['email'];
        $data['guestName'] = $newGuest['name'];
        $data['guestPhone'] = $newGuest['phone'];
        $data['guestZip'] = $newGuest['zip'];
        $data['guestID'] = $newGuest['id'];

        if (!empty($data['message'])) {
            $data['message'] = preg_replace('/@name/', $data['guestName'], $data['message']);
        }
        return $data;
    }

}

