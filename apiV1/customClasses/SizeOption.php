<?php

class sizeOption
{
    public $width;
    public $height;
    public $depth;
    public $panels;

    public $swings = 1;
    public $nameOfOption;
    public $partNumber;
    public $cost_meter;

    public $cost_paint_meter;
    public $waste_percent;
    public $totalCostMeter;
    public $burnOff;

    public $number_of_lengths_formula;
    public $number_of_lengths;

    public $cut_size_formula;
    public $cut_size;

    public $quantityFormula;
    public $quantity;
    public $breakDown = Array();
    public $jsonBreakDown;
    public $myOrder;
    public $optionValuesPassedIn;

    public $totalPrice;
    public $material;
    public $forCutSheet;


    public function buildDefault()
    {

        $data['nameOfOption'] = "Cool Option";
        $data['partNumber'] = 'A4506';
        $data['cost_meter'] = 4.9;
        $data['cost_paint_meter'] = 1.0;
        $data['waste_percent'] = 20;
        $data['number_of_lengths_formula'] = 2;
        $data['cut_size_formula'] = 'width-1';
        $data['burnOff'] = .2;
        $data['quantityFormula'] = 'panels-1';
        $data['myOrder'] = 1;
        $data['forCutSheet'] = 1;
        $data['material'] = 'vinyl';
        $json = json_encode($data);

        return ($json);

    }

    public function getTotalCostPerMeter()
    {
        $totalCost = $this->cost_meter + $this->cost_paint_meter + (($this->waste_percent / 100) * $this->cost_meter);

        round($totalCost, 3, PHP_ROUND_HALF_UP);


        $this->totalCostMeter = $totalCost;

        return $this->totalCostMeter;
    }


    public function setOptionFromJSON($json)
    {

        $optionValues = json_decode($json);
        $this->optionValuesPassedIn = $optionValues;

        if (json_last_error()) {
            $this->logit("#####################################################ERROR", "");

            return false;
        } else {
            $this->nameOfOption = $optionValues->{"nameOfOption"};
            $this->material = $optionValues->{"material"};
            $this->cost_meter = $optionValues->{"cost_meter"};
            $this->cost_paint_meter = $optionValues->{"cost_paint_meter"};
            $this->partNumber = $optionValues->{'partNumber'};
            $this->burnOff = $optionValues->{'burnOff'};
            $this->waste_percent = $optionValues->{"waste_percent"};
            $this->getTotalCostPerMeter();
            $this->number_of_lengths_formula = $optionValues->{"number_of_lengths_formula"};
            $this->myOrder = $optionValues->{"myOrder"};
            $this->cut_size_formula = $optionValues->{"cut_size_formula"};
            $this->forCutSheet = $optionValues->{"forCutSheet"};
            return true;
        }
    }


    public function process_formula($formula)
    {
        if (!$formula) {

            $this->logit("Name: " . $this->nameOfOption . " Formula was not set in SizeOption-Process_formula", "");
            return 0;
        }

        $formula = strtolower($formula);
        $formula = str_replace("depth", '$this->depth', $formula);
        $formula = str_replace("width", '$this->width', $formula);
        $formula = str_replace("height", '$this->height', $formula);
        $formula = str_replace("bo", '$this->burnOff', $formula);
        $formula = str_replace("panels", '$this->panels', $formula);
        $formula = str_replace("swings", '$this->swings', $formula);

        $this->logit("Forumla: " . $formula , "");
        eval('$formula = ' . $formula . ';');

        return ($formula);
    }


    public function setNumber_of_lengths()
    {
        $this->number_of_lengths = $this->process_formula($this->number_of_lengths_formula);
    }


    public function setCutSize()
    {
        $this->cut_size = $this->process_formula($this->cut_size_formula);
    }


    public function setQuantity()
    {
        $this->quantity = $this->process_formula($this->quantityFormula);
    }


    public function getTotalPricePerMeter()
    {
        $this->setNumber_of_lengths();
        $this->setCutSize();
        $totalPrice = $this->number_of_lengths * $this->cut_size * ($this->totalCostMeter / 1000);

        $this->totalPrice = round($totalPrice, 2, PHP_ROUND_HALF_UP);

        return $this->totalPrice;
    }


    public function setTotalPrice($width, $height, $panels, $depth = 0)
    {
        $this->depth = $depth;
        $this->width = $width;
        $this->height = $height;
        $this->panels = $panels;
        $this->getTotalPricePerMeter();

        $this->createBreakDown();
    }


    public function displayBreakDown()
    {
        echo "\n";
        $breakdown = json_decode($this->jsonBreakDown);
        if ($breakdown) {
            foreach ($breakdown as $key => $value) {
                echo "$key: $value \n";
            }
        }
    }


    public function createBreakDown()
    {
        setlocale(LC_MONETARY, 'en_US');
        $breakdown['name'] = $this->nameOfOption;
        $breakdown['forCutSheet'] = $this->forCutSheet;
        $breakdown['partNumber'] = $this->partNumber;
        $breakdown['material'] = $this->material;
        $breakdown['Cut Size Inches'] = round($this->cut_size, 2);
        $breakdown['Cut Size MM'] = round($this->cut_size * 25.4);
        $breakdown['Lengths'] = $this->number_of_lengths;
        $breakdown['Quantity of Inches'] = $this->cut_size * $this->number_of_lengths;
        $breakdown['Quantity of Feet'] = round($this->number_of_lengths * $this->cut_size / 12, 2);
        $breakdown['Total Cost'] = money_format('%i', $this->totalPrice);
        $breakdown['myOrder'] = $this->myOrder;

        $this->breakDown = $breakdown;
    }


    public function logit($label, $data)
    {
        $message =  "--------" . $label . ": " . $data ;
    }
}

?>