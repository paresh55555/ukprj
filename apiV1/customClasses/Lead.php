<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
require_once 'Quote.php';




class Lead
{

    public $options = Array();
    public $selected;
    private $page;
    private $limit;

    public function __construct($pageLimit = null) {

        if (!empty($pageLimit)) {
            $this->page = $pageLimit['page'];
            $this->limit = $pageLimit['limit'];
        }

    }


    public function transferAllLeadsToSalesPerson($salesPerson)
    {
        $data = $this->getLeads();
        $quote = new Quote();

        foreach ($data['data'] as $lead) {
            $quote->transferQuote($lead['number'], $salesPerson);
        }

    }

    public function getLeads($search='')
    {
        $db = connect_db();



        $offsetLimit = buildOffSetLimitWithPageLimit($this->page,$this->limit);

        $extraWhere = " and (name like ? OR guest.zip like ? OR phone like ? OR quotesSales.id like ? OR concat_ws('',quotesSales.prefix,quotesSales.id) like ? ) ORDER BY quotesSales.id DESC ";
//        $extraWhere = " and (name like ? OR zip like ? OR phone like ?) ";


        $sql = " SELECT orderName, quotesSales.id, name, guest.zip, phone, email, quantity, total, quoted, `type`, transferred, county, state
                   from quotesSales,guest
                   LEFT JOIN zip_lookup on zip_lookup.zip = guest.zip
                   where guest=guest.id and quotesSales.transferred = '0000-00-00 00:00:00' and status !='deleted' ";

        if (empty($search) or $search == 'undefined') {
            $sqlSearch = "%%";
        } else {
            preg_match_all('!\d+!', $search, $matches);
            $sqlIdSearch = implode(' ', $matches[0]);
            $sqlIdSearch = $sqlIdSearch ? "%$sqlIdSearch%" : "%$search%";
            $sqlSearch = "%$search%";
        }


        $sql = $sql . $extraWhere;

        $stmt = $db->prepare($sql);
        $stmt->bind_param("sssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlIdSearch, $sqlSearch);
//        $stmt->bind_param("ssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);

        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;

        $sql = $sql . $offsetLimit;
        $stmt = $db->prepare($sql );
        $stmt->bind_param("sssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlIdSearch, $sqlSearch);

//        $stmt->bind_param("ssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);

        $stmt->execute();
        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];
            $options['zip'] = preg_replace("/[^a-zA-Z0-9] +/", "", $options['zip']);
            $options['phone']   = preg_replace('/\D/', '', $options['phone'] );
            $options['phone'] = "(" . substr($options['phone'], 0, 3) . ") " . substr($options['phone'], 3, 3) . "-" . substr($options['phone'], 6);

            $quoteDate = strtotime($options['quoted']);
            $options['quoted'] = date('m/d/Y', $quoteDate);
            $options['id'] = "SQ".$options['id'];
            $options['total'] = money_format('%!n', $options['total']);
            $options['county'] = $options['county'] ? $options['county'] : '';
            $options['state'] = $options['state'] ? $options['state'] : '';

            $data[] = $options;
        }

        $results = array("total" => $count, "data" => $data);

        return ($results);


    }

    public function getAssignedLeads($search=null)
    {
        $db = connect_db();

        $offsetLimit = buildOffSetLimitWithPageLimit($this->page,$this->limit);


//        $extraWhere = " and (name like ? OR zip like ? OR phone like ? OR quotesGuest.id like ? ) ";
//        $extraWhere = " and (name like ? OR zip like ? OR phone like ?) ";

//
//        $sql = " SELECT quotesGuest.id, name, zip, phone, quantity, total, quoted, `type`, transferred
//                   from quotesGuest,guest where
//                   guest=guest.id and status ='new'  ";
//
//        if (empty($search) or $search == 'undefined') {
//            $sqlSearch = "%%";
//        } else {
//            $sqlSearch = "%$search%";
//        }
//
//
//        $sql = $sql . $extraWhere;
//
//        $stmt = $db->prepare($sql);
//        $stmt->bind_param("ssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
////        $stmt->bind_param("ssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);


        if (empty($search) or $search == 'undefined') {
            $sqlSearch = "%%";
        } else {
            $sqlSearch = "%$search%";
        }

        $extraWhere = " and (guest.name like ? OR zip like ? OR guest.phone like ? OR users.name like ? )" ;

        $sql = " SELECT orderName, users.name as salesPersonName, quotesSales.id, guest.name, zip, guest.phone, quantity, total, quoted, quotesSales.`type`, transferred
                   from quotesSales,guest,users where
                   guest=guest.id and salesPerson=users.id and quotesSales.status != 'deleted' and transferred !='0000-00-00 00:00:00' ". $extraWhere . " order by quotesSales.id DESC ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;

        $sql = $sql . $offsetLimit;
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ssss",  $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        $stmt->execute();

        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];
            $options['zip'] = preg_replace('/\D/', '', $options['zip']);

            $options['phone']   = preg_replace('/\D/', '', $options['phone'] );
            $options['phone'] = "(" . substr($options['phone'], 0, 3) . ") " . substr($options['phone'], 3, 3) . "-" . substr($options['phone'], 6);

            $quoteDate = strtotime($options['quoted']);
            $options['quoted'] = date('m/d/Y', $quoteDate);
            $options['id'] = "SQ".$options['id'];
            $options['total'] = money_format('%!n', $options['total']);

            $data[] = $options;
        }

        $results = array("total" => $count, "data" => $data);

        return ($results);


    }


    public function revokeLead($guestQuoteID) {

        $db = connect_db();
        $sql = " UPDATE  quotesSales SET `status`='new',salesPerson='',orderName=?, transferred = '0000-00-00 00:00:00' WHERE id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ii", $guestQuoteID, $guestQuoteID);

        if (!$stmt->execute()) {

            throw new Exception('Database failed to Revoke SalesQuote ' . $sql);
        };


        return ("Success");

    }

}


?>
