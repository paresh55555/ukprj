<?php

require_once 'lib/mysql.php';
require_once 'BuilderGroup.php';
require_once 'SizeOptionSet.php';
require_once 'BuilderSet.php';
require_once 'BuilderPercentage.php';
require_once 'PercentageOption.php';
require_once 'PerItemOptionSet.php';
require_once 'GlassOptionsSet.php';

class GroupOption
{
    public $name;
    public $option_types = Array();
    public $options = Array();
    public $translatedFormOptions = Array();
    public $selected;
    public $total;
    public $totalExempt;
    public $breakDown;
    private $debug;

    public $customHeight;
    public $customWidth;
    public $customDepth;


    public function __construct($debug = false)
    {
        $this->debug = $debug;
    }


    public function buildGroupWithDataAndModuleInfo($data, $moduleInfo)
    {
        $this->options = '';

        foreach ($data as $option) {

            if ($option['option_kind'] == "sized") {
                $buildSet = new BuilderSet();
                $data = $buildSet->getSizeSetDataByModuleAndOption($moduleInfo, $option['option_type']);

                $set = new SizeOptionsSet();
                $set->buildSetFromArrayOfJSON($data);
                $set->name = $option['name'];
                $this->options[] = $set;
            }

            if ($option['option_kind'] == "percentage") {
                $builder = new BuilderPercentage();
                $data = $builder->getPercentageByModuleAndOption($moduleInfo['id'], $option['option_type']);

                if (isset($data[0])) {
                    $json = json_encode($data[0]);
                    $percentageOption = new PercentageOption();
                    $percentageOption->setOptionFromJSON($json);
                    $this->options[] = $percentageOption;
                }
            }

            if ($option['option_kind'] == "fixed") {
                $buildSet = new BuilderSet();
                $data = $buildSet->getPerItemSetDataByModuleAndOption($moduleInfo['id'], $option['option_type']);

                $set = new PerItemOptionSet();
                $set->buildSetFromArrayOfJSON($data);
                $set->name = $option['name'];

                if ($this->debug) {
                    echo "Name of Fixed: $set->name \n";
                }
                $this->options[] = $set;

            }

            if ($option['option_kind'] == "glass") {
                $buildSet = new BuilderGlass();
                $data = $buildSet->getGlassByModuleAndOption($moduleInfo['id'], $option['option_type']);
                $set = new GlassOptionsSet();
                $set->buildSetFromArrayOfJSON($data);
                $set->name = $option['name'];
                $this->options[] = $set;
            }
        }
    }


    public function setSwings($numberOfSwings)
    {
        if ($this->options) {
            foreach ($this->options as $option) {
                $option->setSwings($numberOfSwings);
            }
        }
    }


    public function selectOption($name)
    {
        if ($this->debug) {
            echo "Select Name: $name \n";
        }

        $this->selected = null;
        $count = 0;
        if (!empty($this->options)) {
            foreach ($this->options as $option) {

                if ($option->name == $name) {
                    $this->selected = $count;
                }
                $count++;
            }
        }
    }


    public function getTotal($width, $height, $panels, $basePrice)
    {


        if (!is_null($this->selected)) {
            if (isset($this->options[$this->selected]->percentage)) {
                $this->total = $this->options[$this->selected]->getTotal($basePrice);
            } else {
                if ($this->debug) {
                    if ($this->customHeight) {
                        echo "Custom Height: ".$this->customHeight."\n";
                        echo "Custom Width: ".$this->customWidth."\n";
                        echo "Custom Depth: ".$this->customDepth."\n";
                    }
                }

                if (!empty($this->customHeight) or !empty($this->customWidth)) {
                    $this->total = $this->options[$this->selected]->getTotal($this->customWidth, $this->customHeight, $panels, $this->customDepth);
                } else {
                    $this->total = $this->options[$this->selected]->getTotal($width, $height, $panels);
                }

                if (isset($this->options[$this->selected]->options[0]->totalExempt)) {
                    if ($this->options[$this->selected]->options[0]->totalExempt == '1') {
                        $this->totalExempt = $this->total;
                    }
                }
            }
            $this->displayBreakDown();

            return ($this->total);
        }

        return ("Option not selected");
    }


    public function buildGroupedOption($id)
    {
        $this->getOptionTypes($id);
    }


    public function getOptions()
    {
        foreach ($this->option_types as $option) {
            $buildSet = new BuilderSet();
            $data = $buildSet->getSizeSetData($option['option_type']);
            $set = new OptionsSet();
            $set->buildSetFromArrayOfJSON($data);
            $this->options[] = $set;
        }
    }


    public function getOptionTypes($id)
    {
        $group = new BuilderGroup();
        $data = $group->getSizeGroupData(1);
        $this->option_types = $data;
    }


    public function buildOptionsFromDatabase($data)
    {
        $error = NULL;
        foreach ($data as $row) {

            $set = $row['option_type'];
            $options = Array();

            if (isset($this->options[$set])) {
                $options = $this->options[$set];
            }
            array_push($options, json_encode($row));
            $this->options[$set] = $options;

        }

        return ($error);
    }


    public function displayBreakDown()
    {
        if (!is_null($this->selected)) {
            $this->breakDown = $this->options[$this->selected]->breakDown;
        }
    }
}

?>
