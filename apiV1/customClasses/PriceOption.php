<?php

class PriceOption    {
    public $width;
    public $height;
    public $panels;

    public $nameOfOption;

    public $cost_inch;
    public $paint_foot;
    public $waste_percent;
    public $totalCostInch;
    public $burnOff;

    public $number_of_lengths_formula;
    public $number_of_lengths;

    public $cut_size_formula;
    public $cut_size;

    public $costItem;
    public $quantityFormula;
    public $quantity;
    public $breakDown = Array();
    public $swings = 1;

    public $totalPrice;

    public function buildDefault() {

    $data['nameOfOption'] = "Cool Option";
    $data['quantityFormula'] = 'panels-1';
    $data['costItem'] = 50;
    $json = json_encode($data);

    return ($json);

    }
    public function getTotalCostPerInch () {
       $totalCost = $this->cost_inch + $this->paint_foot + (($this->waste_percent/100)*$this->cost_inch) ;
       round($totalCost,3,PHP_ROUND_HALF_UP);
       $this->totalCostInch = $totalCost;
       return $this->totalCostInch;
    }

    public function setOptionFromJSON($json) {


//        $this->logit("SetOptions",$json);

        $optionValues = json_decode($json);
        if (json_last_error()) {
          $this->logit("#####################################################ERROR","");

            return false;
        }
        else {
            $this->nameOfOption = $optionValues->{"nameOfOption"};
            $this->cost_inch = $optionValues->{"cost_meter"};
            $this->paint_foot = $optionValues->{"cost_paint_meter"};
            $this->burnOff = $optionValues->{'burnOff'};
            $this->waste_percent = $optionValues->{"waste_percent"};
            $this->getTotalCostPerInch();
            $this->quantityFormula = $optionValues->{'quantityFormula'};
            $this->costItem = $optionValues->{'costItem'};

            $this->number_of_lengths_formula = $optionValues->{"number_of_lengths_formula"};

            $this->cut_size_formula = $optionValues->{"cut_size_formula"};
            return true;
        }

    }
    public function process_formula ($formula) {

      if (!$formula){
        $this->logit("ERROR","");

        return 0;
      }

        $formula = str_replace("width",'$this->width',$formula);
        $formula = str_replace("height",'$this->height',$formula);
        $formula = str_replace("BO",'$this->burnOff',$formula);
        $formula = str_replace("panels",'$this->panels',$formula);
        $formula = str_replace("swings", '$this->swings', $formula);


//      var_dump($formula);
        eval('$formula = '.$formula.';');

        return ($formula);

    }

    public function setNumber_of_lengths() {
        $this->number_of_lengths =  $this->process_formula($this->number_of_lengths_formula);
//      $this->logit("#Lengths",$this->number_of_lengths);
    }

    public function setCutSize() {
//        echo $this->cut_size_formula;
//        echo "CutSize \n";
        $this->cut_size =  $this->process_formula($this->cut_size_formula);
    }

    public function setQuantity() {
//        echo $this->cut_size_formula;
//        echo "CutSize \n";
        $this->quantity =  $this->process_formula($this->quantityFormula);
    }

    public function getTotalPricePerInch($width,$height,$panels) {

//        $this->logit("width:in",$width);
        $this->setNumber_of_lengths();
//        $this->logit("num", $this->number_of_lengths);
        $this->setCutSize();

//        $this->logit("numLength", $this->number_of_lengths);
//        $this->logit("Cut_Size", $this->cut_size);
//        $this->logit("TotalCost", $this->totalCostMeter);

        $totalPrice = $this->number_of_lengths*$this->cut_size*$this->totalCostInch;

        $this->totalPrice = round($totalPrice, 2, PHP_ROUND_HALF_UP);
        return $this->totalPrice ;
    }

    public function getTotalPricePerItem() {

        $this->setQuantity();
        $totalPrice = $this->quantity * $this->costItem;
        $this->totalPrice = round($totalPrice, 2,  PHP_ROUND_HALF_UP);
        return $this->totalPrice ;

    }

    public function setTotalPrice($width,$height,$panels) {

        $this->width = $width;
        $this->height = $height;
        $this->panels = $panels;

        if ($this->costItem) {
            $this->getTotalPricePerItem();
        }
        else {
            $this->getTotalPricePerInch($width,$height,$panels);
        }

    }

  public function createBreakDown() {

    setlocale(LC_MONETARY, 'en_US');
    $this->breakDown[] = "Name: ".$this->nameOfOption;
    $this->breakDown[] = "Cut Size Inches: ".$this->cut_size;
    $this->breakDown[] = "Cut Size MM: ".$this->cut_size * 25.4;
    $this->breakDown[] = "Lengths: ".$this->number_of_lengths;
    $this->breakDown[] = "Quantity of Inches: ".$this->cut_size*$this->number_of_lengths;
    $this->breakDown[] = "Quantity of Feet: ".round($this->number_of_lengths*$this->cut_size/12,2);
    $this->breakDown[] = "Total Cost: ".money_format('%i', $this->totalPrice);

  }
  public function displayBreakDown() {
    echo "\n";
    foreach ($this->breakDown as &$lineItem ) {
      echo $lineItem."\n";
    }
  }

    public function logit($label,$data) {

        echo "\n--------".$label.": ".$data."\n";

    }

}

?>