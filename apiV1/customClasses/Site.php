<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

class Site
{
    public function getSiteForLocation($site)
    {
        $db = connect_db();
        $sql = "SELECT *  from SQ_site_defaults site where siteName=?      ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $site);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();


        $sql = "select Distinct(translatedModule) from SQ_modules, submodule where active = 1 and SQ_modules.moduleID = module_id";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();

        while ($module = $results->fetch_assoc()) {
            $data['modules'][] = $module['translatedModule'];
        }

        if (isset($data)) {
            unset($data['id']);
            return $data;
        } else {
            return false;
        }
    }
}



