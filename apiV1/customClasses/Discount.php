<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class Discount
{

    public $basePrice;
    public $name;
    public $type;
    public $shouldDisplay;
    public $amount;
    private $createMinimums;
    private $jsonMinimums;

    public $totalDiscount;


    public function __construct()
    {

        $this->createMinimums = array('name', "amount", "type", "salesPerson", "setID", "shouldDisplay");
        $this->jsonMinimums = array('basePrice', "name", "type", "amount", "shouldDisplay");
    }

    public function buildDefault()
    {

        $data['basePrice'] = 10000;
        $data['name'] = 'Discount';
        $data['type'] = 'percentage';
        $data['amount'] = 10;
        $data['shouldDisplay'] = true;
        $json = json_encode($data);

        return ($json);

    }


    public function validate($options, $validateArray)
    {

        foreach ($validateArray as $option) {
            if (!isset($options[$option])) {
                throw new Exception('missing parameter ' . $option . ' in Discount');
            }
        }
        return true;

    }

    public function validateCreateDiscount($options)
    {

        foreach ($this->createMinimums as $option) {
            if (!isset($options[$option])) {
                throw new Exception('missing parameter ' . $option . ' in Discount');
            }
        }
        return true;

    }

    public function validateJSONDiscount($options)
    {

        foreach ($this->jsonMinimums as $option) {
            if (!isset($options->$option)) {
                throw new Exception('missing parameter ' . $option . ' in Discount JSON');
            }
        }
        return true;


    }

    public function setOptionFromJSON($json)
    {

        $optionValues = json_decode($json);
        if (json_last_error()) {
            $this->logit("#####################################################ERROR", "");
            return false;
        }

        $this->validateJSONDiscount($optionValues);

        $this->basePrice = $optionValues->basePrice;
        $this->name = $optionValues->name;
        $this->type = $optionValues->type;
        $this->amount = $optionValues->amount;
        $this->shouldDisplay = $optionValues->shouldDisplay;

        $this->getDiscountAmount();
        return true;


    }

    public function getDiscountAmount()
    {

        if ($this->type == 'percentage') {
            $this->totalDiscount = ($this->basePrice * $this->amount / 100);
        } else {
            $this->totalDiscount = $this->amount;
        }

    }

    public function getDiscount($id)
    {

        if (!isset($id)) {
            throw new Exception('no id passed');
        }

        $db = connect_db();
        $sql = "SELECT *  from discounts where id=? ";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;
    }


    public function createDiscount($new)
    {

        $this->validateCreateDiscount($new);

        $db = connect_db();
        $sql = "INSERT INTO  discounts SET `type`=?, `setID`=?, `amount`=?, `name`=?, `shouldDisplay`=?, `salesPerson`=?  ";

        $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("sidsii", $new['type'], $new['setID'], $new['amount'], $new['name'], $new['shouldDisplay'], $new['salesPerson']);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to Insert New Discount ' . $sql);
        };
        $last_inserted_id = $db->insert_id;

        return $last_inserted_id;
    }


    public function deleteDiscount($id, $salesPerson)
    {

        if (!isset($id)) {
            throw new Exception('no id passed');
        }

        $db = connect_db();
        $sql = "delete from discounts where id=? and salesPerson=? ";
        $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("ii", $id, $salesPerson);
        if (!$stmt->execute()) {
            throw new Exception('Database failed to Delete Discount ' . $sql);
        };

        return true;
    }
}


?>
