<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

class Customer
{

    private $total;
    private $fields = array('id','firstName', 'lastName', 'phone', 'companyName', 'email',
        'sales_person_id','billingAddress1','billingAddress2','billingCity','billingState',
        'billingZip','jobAddress1','jobAddress2','jobCity','jobState','jobZip','customerNotes',
        'shippingName','shippingEmail','billingName','billingEmail','leadSource','leadType',
        'sameAsShipping','customerPO','customerREF','contact','jobPhone','active','billingPhone',
        'shippingPhone');
    public $options = Array();
    public $selected;


    public function getCustomers()
    {

        $db = connect_db();
        $sql = "SELECT * from customer";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[] = $options;
        }
        return $data;

    }

    public function add($customer)
    {

        $db = connect_db();
        unset($customer->search);
        if(isset($customer->empty)){
            unset($customer->empty);
        }

        if (isset ($customer) && is_array($customer)) {
            unset($customer['id']);
        }
        
        foreach ($customer as $key=>$value) {
            if (!in_array($key, $this->fields)) {
                notAuthorized();
            }
        }

        $sql = sqlInsertBuilder($db, $customer, 'customer');

        if ($db->query($sql) === false) {
            trigger_error('Wrong SQL: '.$sql.' Error: '.$db->error, E_USER_ERROR);
            return false;
        } else {
            $last_inserted_id = $db->insert_id;
            $affected_rows = $db->affected_rows;
            return $last_inserted_id;
        }

    }

    public function getCustomer($id)
    {

        $db = connect_db();
        $id = intval($id);
//    $sql =  "SELECT * from customer";
//    $stmt= $db->prepare($sql) ;
//    $stmt->execute();
//    $results = $stmt->get_result();
//    $customers = $results->fetch_assoc();


        $sql = "SELECT * from customer where id = ? and active = 1";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[] = $options;
        }
        return $data;

    }


    public function getSizeGroupData($group_id)
    {

        $db = connect_db();
        $group_id = intval($group_id);
        $sql = "SELECT * from optionsGrouped where group_id = ? order by myOrder";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $group_id);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[] = $options;
        }
        return $data;

    }

}


?>
