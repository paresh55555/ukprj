<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class Invoice
{

    public function getNextInvoiceNumberWithQuoteIdAndCartNumber($quoteID, $cartNumber)
    {
        $db = connect_db();

        $sql = " INSERT INTO  SQ_invoiceNumber SET `quoteID`=?, `cartNumber`=?  ";
        $stmt = $db->prepare($sql);

        $stmt->bind_param("ss", $quoteID, $cartNumber);
        if (!$stmt->execute()) {
            throw new Exception('Database failed to Insert New Invoice ' . $sql);
        };
        $lastId = $db->insert_id;

        $paddedID = str_pad($lastId, 4, '0', STR_PAD_LEFT);

        return $paddedID;
    }
}

