<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
//require_once 'Guest.php';
//require_once 'Customer.php';
//require_once 'User.php';


class OrderList
{

    public function getListIds($dbname)
    {

        $db = connect_db($dbname);

        $getIdsSQL = "SELECT id from quotesSales order by id ";

        $stmt = $db->prepare($getIdsSQL);

        $stmt->execute();
        $results = $stmt->get_result();

        $data = array();
        while ($id = $results->fetch_assoc()) {

            $data[] = $id['id'];
        }

        return $data;
    }

    public function getPatchIds($dbname)
    {


        $db = connect_db($dbname);

        $getIdsSQL = "SELECT * from migrate order by id ";

        $stmt = $db->prepare($getIdsSQL);

        $stmt->execute();
        $results = $stmt->get_result();

        $data = array();
        while ($id = $results->fetch_assoc()) {

            $data[] = $id;
        }

        return $data;
    }


    public function getListIdsNoSite($dbname)
    {

        $db = connect_db($dbname);

        $getIdsSQL = "SELECT id, prefix, `site` from quotesSales  where `site` is null order by id";

        $stmt = $db->prepare($getIdsSQL);

        $stmt->execute();
        $results = $stmt->get_result();

        $data = array();
        while ($id = $results->fetch_assoc()) {

            $data[] = $id;
        }

        return $data;
    }
}