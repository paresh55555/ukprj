<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class MasterGetInsert
{

    private $dbname ;
    private $table;

    public function __construct($dbName,$table,$column = 'id')
    {

        $this->dbname = $dbName;
        $this->table = $table;
        $this->column = $column;

    }


    public function get($column)
    {

        $db = connect_db($this->dbname);


        $get = "SELECT * from ".$this->table." where ".$this->column." = '$column'";

        $stmt = $db->prepare($get);

        $stmt->execute();
        $results = $stmt->get_result();


        $order =  $results->fetch_assoc();

        return $order;
    }


    public function getAllRows()
    {

        $db = connect_db($this->dbname);


        $get = "SELECT * from ".$this->table;

        $stmt = $db->prepare($get);

        $stmt->execute();
        $results = $stmt->get_result();

        $data = array();

        while ($item = $results->fetch_assoc()) {
            $data[] = $item;
        }

        return $data;
    }



    public function getAll($column)
    {

        $db = connect_db($this->dbname);


        $get = "SELECT * from ".$this->table." where ".$this->column." = $column";

        $stmt = $db->prepare($get);

        $stmt->execute();
        $results = $stmt->get_result();

        $data = array();

        while ($item = $results->fetch_assoc()) {
            $data[] = $item;
        }

        return $data;
    }


    public function insert($order)
    {

        if (empty($order)) {
            return ;
        }
        $db = connect_db($this->dbname);
        $insertSQL = sqlInsertBuilder($db, $order, $this->table);

        if ($db->query($insertSQL) === false) {

            echo 'Database failed to insert '.$insertSQL."\n";
//            throw new Exception('Database failed to insert '.$insertSQL);
            return false;

        } else {
            $last_inserted_id = $db->insert_id;

            return $last_inserted_id;
        }

    }


    public function insertStraight($order)
    {

        if (empty($order)) {
            return ;
        }
        $db = connect_db($this->dbname);
        $insertSQL = sqlInsertBuilderStraight($db, $order, $this->table);

        if ($db->query($insertSQL) === false) {

            echo 'Database failed to insert '.$insertSQL."\n";
//            throw new Exception('Database failed to insert '.$insertSQL);
            return false;

        } else {
            $last_inserted_id = $db->insert_id;

            return $last_inserted_id;
        }

    }


    public function updateContact($id,$contracts)
    {

        $db = connect_db($this->dbname);

        $contracts = mysqli_real_escape_string($db, $contracts);

        $sql = "Update SQ_signed_contracts set contracts='$contracts' where quote_id = $id ";
        if ($db->query($sql) === false) {

            echo 'Database failed to Update '.$sql."\n";
//            throw new Exception('Database failed to insert '.$insertSQL);
            return false;

        } else {
            $last_inserted_id = $db->insert_id;

            return $last_inserted_id;
        }

    }

    public function updateContactOff($id)
    {

        $db = connect_db($this->dbname);

        $sql = "Update SQ_signed_contracts set active='0' where quote_id = $id ";
        if ($db->query($sql) === false) {

            echo 'Database failed to Update '.$sql."\n";
//            throw new Exception('Database failed to insert '.$insertSQL);
            return false;

        } else {
            $last_inserted_id = $db->insert_id;

            return $last_inserted_id;
        }

    }
}
