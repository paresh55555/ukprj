<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class Orders
{

    private $dbname ;

    public function __construct($dbName)
    {

        $this->dbname = $dbName;

    }


    public function get($id)
    {

        $db = connect_db($this->dbname);


        $get = "SELECT * from quotesSales where id = $id";

        $stmt = $db->prepare($get);

        $stmt->execute();
        $results = $stmt->get_result();


       $order =  $results->fetch_assoc();

        return $order;
    }


    public function insert($order)
    {

        $db = connect_db($this->dbname);
        $insertSQL = sqlInsertBuilderStraight($order, 'quotesSales');

        if ($db->query($insertSQL) === false) {
            throw new Exception('Database failed to insert '.$insertSQL);

        } else {
            $last_inserted_id = $db->insert_id;

            return $last_inserted_id;
        }

    }


    public function update($id,$orderName)
    {

        $db = connect_db($this->dbname);
        $insertSQL = "Update quotesSales set orderName='$orderName' where id= $id ";

        if ($db->query($insertSQL) === false) {
            throw new Exception('Database failed to insert '.$insertSQL);

        } else {
            $last_inserted_id = $db->insert_id;

            return $last_inserted_id;
        }

    }


}
