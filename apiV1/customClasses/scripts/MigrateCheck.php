<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
//require_once 'Guest.php';
//require_once 'Customer.php';
//require_once 'User.php';


class MigrateCheck
{
    private $id;

    public function init($id)
    {
        $this->id = $id;
    }

    public function processID()
    {

        $db = connect_db('panoramic');
        $getCheck = "SELECT * from migrate where old_order_id=?";

        $stmt = $db->prepare($getCheck);
        $stmt->bind_param("i",$this->id);

        $stmt->execute();
        $results = $stmt->get_result();

        $status = $results->fetch_assoc();

        if (!empty($status)) {
            return false;
        }

        $insertCheck = "INSERT into migrate set old_order_id=? ";
        $stmt = $db->prepare($insertCheck);

        $stmt->bind_param("i",$this->id);

        if (!$stmt->execute()) {
            throw new Exception('Failed to insert $id into migrate check');
        };

        return true;

    }


    public function updateColumn($column, $value)
    {

        $db = connect_db('panoramic');

        $updateSQL = "UPDATE migrate set ".$column."=? where old_order_id=? ";
        $stmt = $db->prepare($updateSQL);
        $stmt->bind_param("ss",$value,$this->id);

        if (!$stmt->execute()) {
            throw new Exception('Failed to Update $column with $value into migrate check');
        };

    }
}