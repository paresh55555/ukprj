<?php

require_once 'lib/mysql.php';
require_once 'PercentageOption.php';

class BuilderPercentage    {


  public function getPercentageByModuleAndOption($module, $option_type) {

    $db = connect_db();
    $sql =  "SELECT * from optionsPercentage where module_id = ? and option_type = ? and active = 1 limit 1";

    $stmt= $db->prepare($sql);
    $stmt->bind_param("is", $module, $option_type);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
      $data[]= $options;
    }
    return $data;

  }

    public function getPercentageObjectByModuleAndOption($module, $option_type) {

        $db = connect_db();
        $sql =  "SELECT * from optionsPercentage where module_id = ? and option_type = ? and active = 1 limit 1";

        $stmt= $db->prepare($sql);
        $stmt->bind_param("is", $module, $option_type);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($option = $results->fetch_assoc()) {
            $percentageOption = new PercentageOption();
            $json = json_encode($option);
            $percentageOption->setOptionFromJSON($json);
            $data[]= $percentageOption;
        }

        return $data;

    }


}



?>
