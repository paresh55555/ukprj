<?php

require_once "./customClasses/GlassOption.php";
require_once "./customClasses/BuilderGlass.php";

class GlassOptionsSet    {

  private $total;
  public $name;
  public $options = Array();
  public $breakDown;

  public function buildSetFromArrayOfJSON($arrayOfJSON) {

    $error = null;
    $this->options = [];

    if (is_array($arrayOfJSON)) {
      foreach ($arrayOfJSON as &$myOption ) {
        $json = json_encode($myOption);
        $glassOption = new GlassOption();
        $glassOption->setOptionFromJSON($json);
        $this->options[] = $glassOption ;
      }
    }
    else {
      $error = "Not an array";
    }
    return ($error);

  }

  public function setSwings($numberOfSwings) {

      if (is_array($this->options)) {
          foreach ($this->options as &$myOption) {
              $myOption->swings = $numberOfSwings;
          }
      }

  }
  public function getTotal($width,$height,$panels) {

    $this->setTotal($width,$height,$panels);
    return ($this->total);

  }

  public function setTotal($width,$height,$panels) {

    $this->total = 0;
    if ($this->options) {
      foreach ($this->options as &$myOption ) {
        $myOption->getTotalPrice($width,$height,$panels);
        $this->total = $this->total + $myOption->totalPrice;
      }
      $errors = null;
      $this->createBreakDown();
      return ($errors);
    }

  }

  public function displayBreakDown() {

    foreach ($this->options as &$myOption ) {
      $myOption->createBreakDown();
      $myOption->displayBreakDown();
    }

  }

  public function createBreakDown() {

    $options = Array ();

    foreach ($this->options as &$myOption ) {
      $options[] = $myOption->breakDown;
    }
    $this->breakDown['glassChoices'] = $options;

  }

}




?>
