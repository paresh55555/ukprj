<?php


class PercentageOption    {
  public $totalPrice;
  public $nameOfOption;
  public $name;
  public $percentage;
  public $baseCost;
  public $color;
  public $breakDown;
  public $jsonBreakDown;



  public function buildDefault() {
    $data['nameOfOption'] = "Super RAL Color";
    $data['percentage'] = '10';
    $json = json_encode($data);
    return ($json);
  }

  public function setOptionFromJSON($json) {

    $optionValues = json_decode($json);
    if (json_last_error()) {
      $this->logit("#####################################################ERROR","");
      return false;
    }
    else {
      $this->nameOfOption = $optionValues->{"nameOfOption"};
      $this->name = $this->nameOfOption;
      $this->percentage = $optionValues->{'percentage'};


      return true;
    }

  }
  public function setSwings() {
    //Does nothing for now
  }

  public function getTotal($baseCost) {

    $this->totalPrice = $baseCost * $this->percentage / 100 ;
    $this->baseCost = $baseCost;
    $this->createBreakDown();
    return($this->totalPrice);


  }


  public function createBreakDown() {

    $options = Array();

    setlocale(LC_MONETARY, 'en_US');
    $option['name'] = $this->nameOfOption;
    $option['color'] = $this->color;
    $option['percentage'] = $this->percentage;
    $option['cost'] = money_format('%i', $this->totalPrice);

    $options[]= $option;
    $this->breakDown['percentage'] = $options;

  }
  public function displayBreakDown() {
    echo "\n";
    foreach ($this->breakdown as $key => $value ) {
      echo "$key: $value \n";
    }
  }


  public function logit($label,$data) {

    echo "\n--------".$label.": ".$data."\n";

  }

}

?>