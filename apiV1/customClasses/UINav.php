<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

class UINav
{

    public function getNavByGroup($group)
    {

        $db = connect_db();
        $sql = "SELECT `navType`, `name`, `idTag`, `action`, `nextTitle`  from SQ_nav where navGroup=? order by myOrder  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $group);
        $stmt->execute();

        $results = $stmt->get_result();
        $options = array();
        while ($optionName = $results->fetch_assoc()) {
            $options[] = $optionName;
        }

        return $options;

    }



}
