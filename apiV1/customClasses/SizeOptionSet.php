<?php

require_once "./customClasses/SizeOption.php";
require_once "./customClasses/BuilderSet.php";

class SizeOptionsSet
{

    private $total;
    public $name;
    public $options = Array();
    public $breakDown;

    public function buildSetFromArrayOfJSON($arrayOfJSON)
    {
        $error = null;
        $this->options = [];

        if (is_array($arrayOfJSON)) {
            foreach ($arrayOfJSON as &$myOption) {
                $json = json_encode($myOption);
                $sizeOption = new SizeOption();
                $sizeOption->setOptionFromJSON($json);
                $this->options[] = $sizeOption;
            }
        } else {
            $error = "Not an array";
        }
        return ($error);
    }


    public function setSwings($numberOfSwings)
    {
        foreach ($this->options as &$myOption) {
            $myOption->swings = $numberOfSwings;
        }
    }


    public function getTotal($width, $height, $panels, $depth = 0)
    {
        $this->setTotal($width, $height, $panels, $depth);

        return ($this->total);
    }


    public function setTotal($width, $height, $panels, $depth = 0)
    {
        foreach ($this->options as &$myOption) {
            $myOption->setTotalPrice($width, $height, $panels, $depth);
            $this->total = $this->total + $myOption->totalPrice;
        }
        $errors = null;
        $this->createBreakDown();

        return ($errors);
    }


    public function displayBreakDown()
    {
        foreach ($this->options as &$myOption) {
            $myOption->createBreakDown();
            $myOption->displayBreakDown();
        }
    }


    public function createBreakDown()
    {
        $options = Array();

        foreach ($this->options as &$myOption) {
            $options[] = $myOption->breakDown;
        }
        $this->breakDown['parts'] = $options;
    }
}


?>
