<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
require_once 'BuilderGlass.php';
require_once 'GlassOption.php';

class Glass
{

    public function addGlassSizeToCart($cart)
    {
        foreach ($cart as $key =>$door) {

            if (!empty($door->glassChoices)) {
                $nameOfOption = splitForSecondOption($door->glassChoices);
            } else {
                $nameOfOption = 'Unglazed';
            }

            $builder = new BuilderGlass();
            $dataArray = $builder->getGlassByModuleAndNameOfOption($door->module, $nameOfOption);
            
            if (empty($dataArray)) {
                return $cart;
            }
            $data = $dataArray[0];

            $data['width'] =$door->width * 25.4;
            $data['height'] = $door->height * 25.4;
            $data['panels'] = $door->numberOfPanels;
            $data['swings'] = 1;

            if (!empty($door->panelOptions)) {
                if (!empty($door->panelOptions->swingDirection)) {
                    if (!empty($door->panelOptions->swingDirection->selected == 'both')) {
                        $data['swings'] = 2;
                    }
                }
            }

            $json = json_encode($data);

//            print_r($json);

            $glassOption = new GlassOption();
//            $glassOption->width = $door->width;
//            $glassOption->height = $door->height;
//            $glassOption->width re $door->width;

            $glassOption->setOptionFromJSON($json);
            $glassOption->setGlassHeight();
            $glassOption->setGlassWidth();


            $cart[$key]->glassHeight = $glassOption->glassHeight;
            $cart[$key]->glassWidth = $glassOption->glassWidth;

            $cart[$key]->glassHeightIn = round($glassOption->glassHeight / 25.4, 2);
            $cart[$key]->glassWidthIn = round($glassOption->glassWidth / 25.4, 2);
        }

        return $cart;
    }
}

