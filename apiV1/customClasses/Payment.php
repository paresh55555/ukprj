<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class Payment
{

    public $discounts = array();
    private $basePrice;
    private $trueBasePrice;
    private $maxDiscount;
    private $configMinimums;
    private $setID;
    private $totalDiscount;

    protected $requiredPaymentOptions = array("amount", "date", "lastFour", "quoteID", "kind");


//    public function __construct()
//    {
//
//        $this->configMinimums = array ('setID', 'basePrice', 'trueBasePrice', 'maxDiscount');
//    }


    public function verified($id = null)
    {
        if (!is_numeric($id)) {
            throw new Exception("Verified payment missing an ID");
        }

        $db = connect_db();

        $sql = "UPDATE SQ_payments SET `verified`=1 where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);

        if (!$stmt->execute()) {
            throw new Exception('Failed to mark payment verified');
        };

        return true;
    }


    public function notVerified($id)
    {
        if (!is_numeric($id)) {
            throw new Exception("unVerified payment missing an ID");
        }

        $db = connect_db();

        $sql = "UPDATE SQ_payments SET `verified`=0 where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);

        if (!$stmt->execute()) {
            throw new Exception('Failed to unmark payment verified');
        };

        return true;
    }


    public function delete($id)
    {
        if (!is_numeric($id)) {
            throw new Exception("delete payment missing an ID");
        }

        $db = connect_db();

        $sql = "UPDATE SQ_payments SET `status`=0 where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);

        if (!$stmt->execute()) {
            throw new Exception('Failed to unmark payment verified');
        };

        $sql = " SELECT quoteID FROM SQ_payments  WHERE  id=?  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $data = $results->fetch_assoc();

        if (!empty($data['quoteID'])) {
            $this->updateBalanceDue($data['quoteID']);
        }

        return true;
    }


    public function makePayment($payment)
    {

        if (!$this->checkPayment($payment)) {
            return false;
        };

        $db = connect_db();

        $sql = "INSERT INTO SQ_payments SET `amount`=?, `date`=?, `lastFour`=?, `quoteID`=?, `kind`=?";
        $stmt = $db->prepare($sql);

        $paymentDate = humanDateToMysql($payment->date);

        $stmt->bind_param("dssss",
            $payment->amount,
            $paymentDate,
            $payment->lastFour,
            $payment->quoteID,
            $payment->kind);

        if (!$stmt->execute()) {
            throw new Exception('Failed to add Payment');
        };
        $last_inserted_id = $db->insert_id;

        $this->updateBalanceDue($payment->quoteID);

        return $last_inserted_id;
    }

    public function updateBalanceDue($quoteID)
    {
        $db = connect_db();

        $totalPayments = $this->getTotalPayments($quoteID);
        $sql = "UPDATE quotesSales SET `balanceDue`=(total-$totalPayments) where id=?";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $quoteID);

        if (!$stmt->execute()) {
            throw new Exception('Failed to update BalanceDue');
        };

    }

    public function getPaymentsSpecial($quoteID,$site)
    {

        $db = connect_db($site);

        $sql = " SELECT id, amount, `date`, lastFour, kind, verified FROM SQ_payments  WHERE  quoteID=? and status = '1' ";

        $stmt = $db->prepare($sql);

        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $data = array();
        while ($payment = $results->fetch_assoc()) {

            $payment['date'] = date('m/d/Y', strtotime($payment['date']));
            $data[] = $payment;
        }

        return $data;
    }


    public function getPayments($quoteID)
    {

        $db = connect_db();

        $sql = " SELECT id, amount, `date`, lastFour, kind, verified FROM SQ_payments  WHERE  quoteID=? and status = '1' ";

        $stmt = $db->prepare($sql);

        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $data = array();
        while ($payment = $results->fetch_assoc()) {

            $payment['date'] = date('m/d/Y', strtotime($payment['date']));
            $data[] = $payment;
        }

        return $data;
    }

    public function getTotalPayments($quoteID)
    {
        $db = connect_db();

        $sql = " select sum(amount) as Payment from SQ_payments WHERE  quoteID=? and status = '1' ";

        $stmt = $db->prepare($sql);

        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $payment = $results->fetch_assoc();

        if (!empty($payment['Payment'])) {
            return $payment['Payment'];
        }

        return 0;

    }


    protected function checkPayment($payment)
    {
        foreach ($this->requiredPaymentOptions as $property) {
            if (empty($payment->{$property})) {
                throw new Exception("Missing $property for making a payment:");
            }
        }
        return true;
    }


}


?>
