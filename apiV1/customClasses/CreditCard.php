<?php

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

require_once ('Quote.php');
require_once ('Payment.php');
require_once ('PaymentLog.php');

define("AUTHORIZENET_LOG_FILE", "/tmp/phplog");

class CreditCard
{
    public function __construct()
    {
    }

    protected function completePayment($config)
    {

        $quote = new Quote();
        $authCode = $config['authCode'];
        $ccNumber = $config['ccNumber'];
        $ccAmount = $config['ccAmount'];
        $quoteData = $config['quoteData'];
        $transID = $config['transID'];
        $quoteID = $config['quoteID'];
        $salesPerson = $config['salesPerson'];

        $today = date("m/d/y");
        $lastFour = substr($ccNumber, -4);
        $type = $this->cardType($ccNumber);
        $payment = array("amount" => $ccAmount, "date" => $today, "number" => $lastFour,
            "type" => $type, "authCode" => $authCode, "lastFour" => $lastFour, "quoteID" => $quoteID,
            "transID" => $transID, "kind" => $type);

        $payments = new Payment();
        $objPayment = json_decode(json_encode($payment));
        $payments->makePayment($objPayment);

        $discounts = json_decode($quoteData['SalesDiscount']);

        if (!empty($discounts)) {
            if (!empty($discounts->Payments)) {
                array_push($discounts->Payments, $payment);
            } else {
                $discounts->Payments = array($payment);
            }
        } else {
            $payments = array($payment);
            $discounts->Payments = $payments;
        }
        $quote->confirmFullPayment($quoteID);
        $quote->setOrderDateNow($quoteID);
        $quote->setProductionCompleteDate($quoteID, $quoteData);

        $quote->addInvoiceNumber($quoteID);

        //Adds payment
        $quote->updateSalesDiscount($discounts, $quoteID);

        $quote->convertToPreProduction($quoteID, $salesPerson);
    }

    public function chargeCard($postOptions, $paymentLog)
    {
        
        $ccNumber = $postOptions->ccNumber;
        $ccMonth = $postOptions->ccMonth;
        $ccYear = $postOptions->ccYear;
        $ccCarCode = $postOptions->cardCode;
        $ccAmount = $postOptions->ccAmount;
        $quoteID = $postOptions->id;
        $salesPerson = $postOptions->salesPerson;
        $ccZip = $postOptions->ccZip;

        

        $quote = new Quote();
        $quoteData = $quote->getQuote($quoteID);

        if ($ccNumber == '4111222211112222') {
            $config['ccNumber'] = $ccNumber;
            $config['ccAmount'] = $ccAmount;
            $config['quoteData'] = $quoteData;
            $config['quoteID'] = $quoteID;
            $config['authCode'] = "Testing ByPass";
            $config['transID'] = "Testing ByPass";
            $config['salesPerson'] = $salesPerson;

            $this->completePayment($config);
            $result['status'] = 'Success';
            return true;
        }


        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName("3JWr4mf6Q");
        $merchantAuthentication->setTransactionKey("768kZgKy5A68Vc28");

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($ccNumber);
        $creditCard->setExpirationDate($ccYear."-".$ccMonth);
        $creditCard->setCardCode($ccCarCode);
        $paymentOne = new AnetAPI\PaymentType();
        $paymentOne->setCreditCard($creditCard);

        $billto = new AnetAPI\CustomerAddressType();
        $billto->setZip($ccZip);
        $billto->setAddress($postOptions->billingAddress1." ".$postOptions->billingAddress2);
        $billto->setCity($postOptions->billingCity);
        $billto->setState($postOptions->billingState);
        $billto->setPhoneNumber($postOptions->phone);
        $billto->setEmail($postOptions->email);
        $billto->setFirstName($postOptions->ccName);
        $billto->setCompany($postOptions->company);

        // Create a transaction
        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($ccAmount);
        $transactionRequestType->setBillTo($billto);
        $transactionRequestType->setPayment($paymentOne);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::PRODUCTION);

        $paymentLog->updateResponse(print_r($response, true));

        $success = false;
        if ($response != null) {
            $tresponse = $response->getTransactionResponse();
            $paymentLog->updateTResponse(print_r($tresponse, true));
            
            if (($tresponse != null) && ($tresponse->getResponseCode() == "1")) {

                $config['ccNumber'] = $ccNumber;
                $config['ccAmount'] = $ccAmount;
                $config['quoteData'] = $quoteData;
                $config['quoteID'] = $quoteID;
                $config['quote'] = $quoteID;
                $config['authCode'] = $tresponse->getAuthCode();
                $config['transID'] = $tresponse->getTransId();
                $config['salesPerson'] = $salesPerson;

                $this->completePayment($config);
                $success = true;

            } else {
//                echo "Charge Credit Card ERROR :  Invalid response\n";
                $success = false;
            }
        } else {
//            echo "Charge Credit card Null response returned";
            $success = false;
        }

        return $success;
    }

    protected function cardType($number)
    {
        $number = preg_replace('/[^\d]/', '', $number);
        if (preg_match('/^3[47][0-9]{13}$/', $number)) {
            return 'American Express';
        } elseif (preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/', $number)) {
            return 'Diners Club';
        } elseif (preg_match('/^6(?:011|5[0-9][0-9])[0-9]{12}$/', $number)) {
            return 'Discover';
        } elseif (preg_match('/^(?:2131|1800|35\d{3})\d{11}$/', $number)) {
            return 'JCB';
        } elseif (preg_match('/^5[1-5][0-9]{14}$/', $number)) {
            return 'MasterCard';
        } elseif (preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/', $number)) {
            return 'Visa';
        } else {
            return '';
        }
    }

}

