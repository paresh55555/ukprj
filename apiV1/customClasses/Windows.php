<?php


require_once 'lib/mysql.php';
require_once 'Auth.php';

class Windows
{


    public function __construct()
    {

    }


    public function getTypesOfWindowsBasedOnSize($size)
    {

        $db = connect_db();
        $sql = "SELECT * from SQ_windows ORDER BY windowSection ";

        $stmt = $db->prepare($sql);

        $stmt->execute();

        $results = $stmt->get_result();
        $options = array();

        while ($option = $results->fetch_assoc()) {
            $options[] = $option;
        }

        return $options;

    }

}

