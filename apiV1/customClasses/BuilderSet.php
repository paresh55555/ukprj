<?php

require_once 'lib/mysql.php';

class BuilderSet    {

  private $total;
  public $options = Array();


  public function getSizeSetDataByModuleAndOption($moduleInfo, $option_type) {


    if (!$moduleInfo['id'] ) {
      throw new Exception('Module Number was not passed');
    }

    $db = connect_db();
    $sql =  "SELECT * from optionsSized where module_id = ? and active = 1 and option_type = ? order by myOrder";

    $stmt= $db->prepare($sql);
    $stmt->bind_param("is", $moduleInfo['id'], $option_type);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
        $data[]= $options;
    }
    return $data;
  }


//  public function getModuleInfoIDFromBuilder($id)
//  {
//
//    $db = connect_db();
//    $sql = "SELECT * from module where id = ".$id." limit 1";
//
//    $stmt = $db->prepare($sql);
//    $stmt->execute();
//    /* Fetch result to array */
//    $results = $stmt->get_result();
//    $options = $results->fetch_assoc();
//
//    return $options;
//  }


//  public function getSizeSetData($option_type) {
//
//    $data = $this->getSizeSetDataByModuleAndOption(1,$option_type);
//    return $data;
//
//  }



  public function getPerItemSetDataByModuleAndOption($module, $option_type) {

    $db = connect_db();
    $sql =  "SELECT * from optionsFixed where module_id = ? and active = 1 and option_type = ? order by myOrder";
    $stmt= $db->prepare($sql);
    $stmt->bind_param("is", $module, $option_type);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
      $data[]= $options;
    }
    return $data;

  }



//  public function getPerItemSetData($option_type) {
//
//    $data = $this->getPerItemSetDataByModuleAndOption(1,$option_type);
//    return $data;
//
//  }

}



?>
