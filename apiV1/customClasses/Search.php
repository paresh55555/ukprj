<?php


require_once 'lib/mysql.php';
require_once 'Auth.php';

class Search
{



    public function __construct()
    {

    }

    public function getCustomersOfSalesPerson($search,$salesPersonID)
    {
        $db = connect_db();
        $sql = "SELECT DISTINCT(customer.id) , firstName, lastName, billingAddress1 as address1, billingAddress2 as address2,
                billingCity as city, billingState as state, billingZip as zip, phone, email   from customer
                WHERE sales_person_id=? and (firstName like ? || lastName like ?) and customer.active = 1";
        $stmt = $db->prepare($sql);

        $search = "%$search%";

        $stmt->bind_param("iss", $salesPersonID,$search,$search);
        $stmt->execute();
        $results = $stmt->get_result();

        $customers = array();
        while ($customer = $results->fetch_assoc()) {
            $customers[] = $customer;
        }

        return ($customers);
    }


    public function getUser($id)
    {
        $db = connect_db();
        $sql = "SELECT `id`, `email`, `type`, `name`,extended,prefix  from users where id=?  ";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        if (isset($data)) {
            if ($data['id'] > 0) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    public function makeBlankDefault()
    {

        $discounts = array("amount" => 0, "id" => 0, "name" => "Discount", "showOnQuote" => true, "type" => "flat");

        $yesOnQuote = array("amount" => 0, "showOnQuote" => true);
        $noOnQuote = array("amount" => 0, "showOnQuote" => false);

        $totals = array("code" => "YZ-00", "tax" => $noOnQuote,
            "shipping" => $noOnQuote, "installation" => $noOnQuote,
            "cost" => $yesOnQuote, "preCost" => 10000,
            "finalTotal" => $yesOnQuote, "subTotal" => $yesOnQuote,
            "extra" => array("name" => "", "amount" => 0, "showOnQuote" => false),
            "screens" => array("name" => "", "amount" => 0, "showOnQuote" => false),
            "discountTotal" => $yesOnQuote);

        $default = array("Discounts" => array($discounts), "Totals" => $totals);

        return $default;


    }

    /**
     * @param $id
     *
     * @return array
     */
    public function getDefaults($id)
    {

        $db = connect_db();
        $sql = "SELECT defaults  from users where id=? ";
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();


        if (empty($data['defaults'])) {
            $response = $this->makeBlankDefault();
            updateOptionID($db, $response, "users", $id);
            exit();
        } else {
            $response = json_decode($data['defaults']);
        }

        return $response;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return array|bool
     */
    public function login($email = '', $password = '')
    {

        if (!$email) {
            return false;
        }
        if (!$password) {
            return false;
        }

        $db = connect_db();
        $sql = "SELECT `id`, `type`, `email`,  `name`,extended,defaults,prefix  from users where email=? and password=? ";
        $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();
        if (isset($data)) {
            if ($data['id'] > 0) {
                $config = Array("type" => $data['type'], "id" => $data['id']);
                $auth = new Auth();
                $data['token'] = $auth->login($config);
                if (empty ($data['defaults'])) {
                    $data['defaults'] = $this->makeBlankDefault();
                    $updateDefaults = array ('defaults' => json_encode($data['defaults']));
                    updateOptionID($db, $updateDefaults, "users", $data['id']);


                } else {
                    $data['defaults'] = json_decode($data['defaults']);
                }

                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }


    }
}

