<?php

require_once 'PhpStringParser.php';


class PerItemOption
{
    public $width;
    public $height;
    public $panels;

    public $nameOfOption;
    public $partNumber;
    public $costItem;
    public $quantityFormula;
    public $quantity;
    public $breakDown = Array();
    public $forCutSheet;
    public $myOrder;
    public $swings = 1;
    public $totalExempt;

    public $totalPrice;

    public function buildDefault()
    {

        $data['nameOfOption'] = "PerItem Cool Option";
        $data['quantityFormula'] = 'panels-1';
        $data['costItem'] = 50;
        $data['forCutSheet'] = 1;
        $data['myOrder'] = 1;
        $data['partNumber'] = 'A100';
        $data['totalExempt'] = 0;

        $json = json_encode($data);

        return ($json);

    }

    public function getTotalCostPerInch()
    {
        $totalCost = $this->cost_inch + $this->paint_foot + (($this->waste_percent / 100) * $this->cost_inch);
        round($totalCost, 3, PHP_ROUND_HALF_UP);
        $this->totalCostInch = $totalCost;
        return $this->totalCostInch;
    }

    public function setOptionFromJSON($json)
    {

        $optionValues = json_decode($json);
        if (json_last_error()) {
            $this->logit("#####################################################ERROR", "");

            return false;
        } else {
            $this->nameOfOption = $optionValues->{"nameOfOption"};
            $this->quantityFormula = $optionValues->{'quantityFormula'};
            $this->costItem = $optionValues->{'costItem'};
            $this->forCutSheet = $optionValues->{'forCutSheet'};
            $this->myOrder = $optionValues->{'myOrder'};
            $this->totalExempt = $optionValues->{'totalExempt'};
            $this->partNumber = $optionValues->{'partNumber'};
            return true;
        }

    }

    public function process_formula($formula)
    {

        if (!$formula) {
            $this->logit("ERROR", "");

            return 0;
        }

        $formula = strtolower($formula);
        $formula = str_replace("prepanels", $this->panels, $formula);

        $p = new PhpStringParser();
        $formula = $p->parse($formula);
        $formula = str_replace("width", '$this->width', $formula);
        $formula = str_replace("height", '$this->height', $formula);
        $formula = str_replace("bo", '$this->burnOff', $formula);
        $formula = str_replace("panels", '$this->panels', $formula);
        $formula = str_replace("swings", '$this->swings', $formula);

        eval('$formula = ' . $formula . ';');

        return ($formula);

    }

    public function setNumber_of_lengths()
    {
        $this->number_of_lengths = $this->process_formula($this->number_of_lengths_formula);
//      $this->logit("#Lengths",$this->number_of_lengths);
    }

    public function setCutSize()
    {
//        echo $this->cut_size_formula;
//        echo "CutSize \n";
        $this->cut_size = $this->process_formula($this->cut_size_formula);
    }

    public function setQuantity()
    {
//        echo $this->cut_size_formula;
//        echo "CutSize \n";

        $this->quantity = $this->process_formula($this->quantityFormula);


    }

    public function getTotalPricePerItem()
    {

        $this->setQuantity();
        $totalPrice = $this->quantity * $this->costItem;
        $this->totalPrice = round($totalPrice, 2, PHP_ROUND_HALF_UP);
        return $this->totalPrice;


    }

    public function setTotalPrice($width, $height, $panels)
    {

        $this->width = $width;
        $this->height = $height;
        $this->panels = $panels;
        $this->getTotalPricePerItem();
        $this->createBreakDown();

    }

    public function createBreakDown()
    {

        setlocale(LC_MONETARY, 'en_US');
        $this->breakDown['name'] = $this->nameOfOption;
        $this->breakDown['cost per part'] = $this->costItem;
        $this->breakDown['quantity'] = $this->quantity;
        $this->breakDown['cost'] = money_format('%i', $this->totalPrice);
        $this->breakDown['forCutSheet'] = $this->forCutSheet;
        $this->breakDown['myOrder'] = $this->myOrder;
        $this->breakDown['partNumber'] = $this->partNumber;

    }


    public function displayBreakDown()
    {
        echo "\n";
        foreach ($this->breakDown as $key => $value) {
            echo "$key: $value \n";
        }
    }

    public function logit($label, $data)
    {

        echo "\n--------" . $label . ": " . $data . "\n";

    }

}

?>