<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

class Email
{

    public function emailInvoice($configPDF, $configEmail)
    {
        //$configPDF['url']
        //$configPDF['fileNamePath'];

//        $configEmail->address;
//        $configEmail->from;
//        $configEmail->subject;
//        $configEmail->message;
        if (!empty($configEmail->quote)) {
            $configEmail->file =  $configPDF['fileNamePath'];
        }
        
        writePhantomjsPDF($configPDF);
        $message = sendEmailViaSendGrid($configEmail);

        return $message;
    }
}

