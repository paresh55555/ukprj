<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';

class UIOptions
{
    public function getOptionsByGroup($group)
    {

        $db = connect_db();
        $sql = "SELECT *  from SQ_options where optionGroup=? order by myOrder  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $group);
        $stmt->execute();

        $results = $stmt->get_result();
        $options = array();
        while ($option = $results->fetch_assoc()) {
            $options[$option['nameOfOption']] = $option;
        }

        return $options;

    }



}
