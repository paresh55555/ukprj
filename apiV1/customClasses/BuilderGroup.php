<?php

require_once 'lib/mysql.php';

class BuilderGroup    {

  private $total;
  public $options = Array();
  public $selected;

//  public function getSizeGroupData($group_id) {
//
//    $db = connect_db();
//    $sql =  "SELECT * from optionsSized where module_id = 1 and active = 1 and group_id = ".$group_id."  order by option_type, myOrder";
//    $stmt= $db->prepare($sql) ;
//    $stmt->execute();
//    $results = $stmt->get_result();
//    $data = array();
//
//    while ($options = $results->fetch_assoc()) {
//      $data[]= $options;
//    }
//    return $data;
//
//  }

  public function getGroupIDSForModule($module_id) {

    $db = connect_db();
//    $sql =  "SELECT Distinct(group_id) from optionsGrouped where module_id = ".$module_id."  order by myOrder";
    $sql =  "SELECT group_id from groupNames where module_id = ?";

    $stmt= $db->prepare($sql);
    $stmt->bind_param("i", $module_id);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
      $data[]= $options;
    }
    return $data;

  }


  public function getSizeGroupDataByModuleAndGroup($module_id, $group_id) {

    $db = connect_db();
    $sql =  "SELECT * from optionsGrouped where group_id = ? and module_id = ? order by myOrder";
    $stmt= $db->prepare($sql);
    $stmt->bind_param("ii", $group_id, $module_id);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();

    while ($options = $results->fetch_assoc()) {
      $data[]= $options;
    }
    return $data;
  }



//  public function getSizeGroupData($group_id) {
//
//    $db = connect_db();
//    $sql =  "SELECT * from optionsGrouped where group_id = ".$group_id."  order by myOrder";
//    $stmt= $db->prepare($sql) ;
//    $stmt->execute();
//    $results = $stmt->get_result();
//    $data = array();
//
//    while ($options = $results->fetch_assoc()) {
//      $data[]= $options;
//    }
//    return $data;
//
//  }

}



?>
