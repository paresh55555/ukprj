<?php

require_once 'lib/mysql.php';
require_once "BuilderGroup.php";
require_once "BuilderSet.php";
require_once "SizeOptionSet.php";

class WindowSystem
{
    protected $config;
    protected $options;

    protected $module;

    protected $frame;
    protected $transom;
    protected $mullion;
    protected $opener;

    protected $components = array();

    protected $groupBuilder;

    protected $requiredOptions = ["width", "height", "module"];


    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->module = $config['module'];
        $this->options = $config['options'];

        $this->groupBuilder  = new BuilderGroup();
        $this->buildComponents();
    }


    /**
     * @param array $options
     *
     * @throws RuntimeException
     */
    protected function validOptions($options)
    {
        foreach ($this->requiredOptions as $option) {
            if (empty($options[$option])) {
                throw new \RuntimeException('Invalid Option Passed '+$option);
            }
        }
    }


    protected function buildComponents()
    {
        $this->frame = $this->buildGroup(1);
        $this->transom = $this->buildGroup(2);
        $this->mullion = $this->buildGroup(3);
        $this->opener = $this->buildGroup(4);
    }


    protected function buildGroup($group)
    {
        $moduleInfo['id'] = $this->module;
        $data = $this->groupBuilder->getSizeGroupDataByModuleAndGroup($this->module, $group);
        $group = new GroupOption();
        $group->buildGroupWithDataAndModuleInfo($data,$moduleInfo);

        foreach ($group->options as $option) {
            echo "Selected Options: ".$option->name." \n";
            $group->selectOption($option->name);
        }

        return $group;
    }


    protected function getTotalForComponent($component)
    {
        $type = key($component);
        $size = $component[$type];

        echo "Type: $type -->";
        $componentTotal =  $this->{$type}->getTotal($size['width'],$size['height'],1,0);
        echo " $componentTotal \n";

        return $componentTotal;
    }


    /**
     * @return int
     */
    public function getTotal()
    {
        $total = 0;
        foreach ($this->options as $option) {
            $total = $total + $this->getTotalForComponent($option);
        }

        return $total;
    }



}


