<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class FollowupNotesLog
{

    public function insertNotesLog($quote_id, $note)
    {
        $db = connect_db();

        $sql = "INSERT followup_notes_log SET quote_id=?, note=? ";
        $stmt = $db->prepare($sql);

        $stmt->bind_param("is",$quote_id, $note);

        if (!$stmt->execute()) {
            throw new Exception('Failed to insert followup notes log');
        };

    }

}


?>