<?php


require_once 'lib/mysql.php';
require_once 'PriceEngine.php';


class CutSheet
{

    private $priceEngine;

    public function __construct()
    {
        $this->priceEngine = new PriceEngine();
    }

    public function buildCutSheets($sheets)
    {
        $cutSheets = array();
        foreach ($sheets as $sheet) {

            $json = json_encode($sheet);

            $this->priceEngine = new PriceEngine();
            $this->priceEngine->buildDoorWithOptionsGetDefaultBuildTotal($json);
            $this->priceEngine->totalPrice;

            $cutSheet= $this->priceEngine->buildCutSheet($json);
            if (!empty($sheet->note)){
                $cutSheet['note']= $sheet->note;
            }
            if (!empty($sheet->productionNotes)){
                $cutSheet['productionNotes']= $sheet->productionNotes;
            }
            $cutSheets[] = $cutSheet;
        }

        return $cutSheets;
    }


    public function buildAndSaveCutSheets($id, $sheets)
    {
        $cutSheets = $this->buildCutSheets($sheets);
        $jsonSheets = json_encode($cutSheets);

        $db = connect_db();
        $sql = "update quotesSales set cutSheets=? where id=?  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $jsonSheets, $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to add CutSheet' . $sql);
        };

        return true;
    }


    public function buildAndSaveCutSheetsSales($id, $sheets, $SalesPerson, $superToken = null)
    {
        $cutSheets = $this->buildCutSheets($sheets);

        $jsonSheets = json_encode($cutSheets);

        $db = connect_db();

        if (empty($superToken)) {
            $sql = "update quotesSales set cutSheets=? where id=? and SalesPerson = ?  ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("sss", $jsonSheets, $id, $SalesPerson);
        } else {
            $sql = "update quotesSales set cutSheets=? where id=?  ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ss", $jsonSheets, $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed to add CutSheet' . $sql);
        };

        return true;
    }
}

