<?php


require_once 'lib/mysql.php';

class Auth
{

    private $id;
    private $type;
    private $token;
    private $validUserTypes = array(
        'builder',
        'production',
        'productionLimited',
        'surveyLimited',
        'survey',
        'accounting',
        'accountingLimited',
        'audit',
        'salesPerson',
        'salesPersonSurvey',
        'admin',
        'guest',
        'leads'
    );


    public function validateConfigParameters($config)
    {

        if (!$config['id']) {
            throw new Exception("Id not passed for createToken in Auth");
        }

        if (!$config['type']) {
            throw new Exception("Type not passed for createToken in Auth");
        }

        foreach ($this->validUserTypes as $type) {

            if ($config['type'] == $type ) {
                return true;
            }
        }

        throw new Exception("Invalid Type passed for createToken in Auth");
    }

    public function createToken($config)
    {

        $this->validateConfigParameters($config);
        $this->id = $config['id'];
        $this->type = $config['type'];
        $this->token = uniqid();
        return $this->token;


    }

    public function addSuperToken()
    {
        $this->addToken("yes");

    }

    public function addToken($super = null)
    {

        $db = connect_db();

        if (empty($super)) {
            $sql = "INSERT INTO  authorized SET `type`=?, `typeID`=?, `token`=?, `super` = 0, `expires`=now() + INTERVAL 5 DAY";
        } else {
            $sql = "INSERT INTO  authorized SET `type`=?, `typeID`=?, `token`=?, `super` = 1, `expires`=now() + INTERVAL 5 DAY";

        }
        $stmt = $db->prepare($sql);

        $stmt->bind_param("sss", $this->type, $this->id, $this->token);
        $stmt->execute();


    }

    public function validateSuperToken($token)
    {
        return $this->validateToken($token, "yes");
    }


    public function validateToken($token, $super = null)
    {

        if (!is_string($token)) {
            return false;
        }

        $db = connect_db();
        if (empty($super)) {
            $sql = "SELECT id as rowID, `typeID` as id, `type`, super from authorized where token=? and expires > now() ";
            $stmt = $db->prepare($sql);
        } else {
            $sql = "SELECT id as rowID, `typeID` as id, `type`, super from authorized where token=? and super = 1 and expires > now() ";
            $stmt = $db->prepare($sql);
        }

        $stmt->bind_param("s", $token);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if ($data) {
            $this->extendToken($db, $data['rowID']);
            unset($data['rowID']);
            return $data;
        } else {
            return false;
        }
    }

    public function extendToken($db, $id)
    {

        $sql = "UPDATE authorized set expires= NOW() + INTERVAL 1 DAY where id = ? ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();


    }

    public function login($config)
    {
        $token = $this->createToken($config);
        $this->addToken();
        return ($token);

    }


    public function createSuperToken($config)
    {
        $token = $this->createToken($config);
        $this->addSuperToken();
        return ($token);

    }

    public function setValidation($id)
    {

        $this->validation = Array("salesPerson" => $id, "token" => unique());

    }


}

