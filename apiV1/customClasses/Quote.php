<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
require_once 'Guest.php';
require_once 'Customer.php';
require_once 'User.php';
require_once 'Module.php';
require_once 'Invoice.php';
require_once 'Payment.php';
require_once 'FollowupNotesLog.php';

use GuzzleHttp\Client;


class Quote
{

    private $limit;
    private $page;
    private $total;
    public $options = Array();
    public $selected;
    public $validQuality = array("holdorder","new", "hot", "warm", "cold", "hold", "archived", null, '');
    public $customerInfo = array('jobPhone', 'contact', 'customerREF', 'customerPO', 'leadSource', 'lastName', 'firstName', 'phone', 'companyName', 'email', 'billingAddress1', 'billingAddress2',
        'billingCity', 'billingState', 'billingZip', 'jobAddress1', 'jobAddress2', 'jobCity', 'jobState', 'jobZip',
        'customerNotes', 'billingEmail', 'billingName', 'shippingName', 'shippingEmail', 'sameAsShipping',
        'billing_address1', 'billing_address2','billing_city','billing_state', 'billing_zip', 'billing_phone', 'billing_contact',
        'billing_email', 'shipping_address1', 'shipping_address2', 'shipping_city', 'shipping_state', 'shipping_zip',
        'shipping_phone', 'shipping_contact', 'shipping_email');



    public function __construct($pageLimit = null)
    {
        if (!empty($pageLimit)) {
            $this->page = $pageLimit['page'];
            $this->limit = $pageLimit['limit'];
            if (!is_numeric($this->limit) ) {
                $this->limit = 10;
            }

        } else {
            $this->page = 1;
            $this->limit = 10;
        }


    }


    public function isSalesPerson($newQuote)
    {

        if (isset($newQuote->SalesPerson)) {
            if (!is_numeric($newQuote->SalesPerson)) {
                return false;
            }
            return true;
        }
        return false;

    }

    public function isCustomer($newQuote)
    {
        if (isset($newQuote->Customer)) {
            if (!is_numeric($newQuote->Customer)) {
                return false;
            }
            return true;
        }
        return false;

    }


    public function isTotal($newQuote)
    {

        if (isset($newQuote->total)) {
            if (!is_numeric($newQuote->total)) {
                return false;
            }
            return true;
        }
        return false;

    }


    public function validDiscount($newQuote)
    {

        if (isset($newQuote->discount)) {
            if (!is_numeric($newQuote->discount)) {
                return false;
            }
            return true;
        }
        return true;

    }

    public function prepIncomingQuoteDataWithNoCustomer($quoteData, $noCustomer = true)
    {

        if ($noCustomer) {
            unset ($quoteData->Customer);
        }

        $removeArray = $this->customerInfo;
        foreach ($removeArray as $remove) {
//            if (isset ($quoteData->{$remove})) {
            unset($quoteData->{$remove});
//            }
        }

        if (isset($quoteData->dueDate)) {
            $quoteData->dueDate = $this->humanDateToMysql($quoteData->dueDate);
        }
        if (isset($quoteData->orderDate)) {
            $quoteData->orderDate = $this->humanDateToMysql($quoteData->orderDate);
        }
        if (isset($quoteData->startedProd)) {
            $quoteData->startedProd = $this->humanDateToMysql($quoteData->startedProd);
        }

        $quoteData->Cart = $this->cleanCart($quoteData->Cart);
        $quoteData->total = $this->quoteTotal($quoteData);

        $costShowOnQuote = true;

        if (isUK()) {
            if (!empty($quoteData->SalesDiscount)) {
                if (!empty($quoteData->SalesDiscount->Totals)) {
                    if (!empty($quoteData->SalesDiscount->Totals->cost)) {
                        if ($quoteData->SalesDiscount->Totals->cost->showOnQuote != '1') {
                            $costShowOnQuote = 0;
                        }
                    }
                }
            }

            $cost = array('amount' => $quoteData->total, 'showOnQuote' => $costShowOnQuote);

        } else {
            $cost = array('amount' => $quoteData->total, 'showOnQuote' => true);
        }


        $quoteData = $this->setSalesDiscountAmount($quoteData, 'preCost', $this->cartTotal($quoteData));
        $quoteData = $this->setSalesDiscountAmount($quoteData, 'cost', $cost);


        $quoteData->depositDue = $this->depositDue($quoteData);
        $quoteData->balanceDue = $this->balanceDue($quoteData);


        $quoteData->Cart = json_encode($quoteData->Cart);
        if (json_last_error() != 0) {
            throw new Exception('Quote has bad Cart data, can\'t convert to JSON');
        }

        $quoteData->quantity = $this->numberOfDoorsQuoteObject($quoteData);


        if (isset($quoteData->SalesDiscount)) {
            $quoteData->SalesDiscount = json_encode($quoteData->SalesDiscount);
        }
        if (json_last_error() != 0) {
            throw new Exception('Quote has bad Sales Discount data, can\'t convert to JSON');
        }

        $quoteData->total = number_format($quoteData->total, 2, '.', '');

        if (!empty($quoteData->quoted))  {
            $quoteData->quoted = date("Y-m-d H:i:s");
        }

        if (empty($quoteData->type)) {
            $quoteData->type = 'quote';
        }

        $quoteData->status = 'new';

        if (empty($quoteData->leadQuality)) {
            $quoteData->leadQuality = 'New';
        }

        return $quoteData;
    }

    public function updateSameVersion($quoteID, $quoteData)
    {
        $quoteID = $this->update($quoteID, $quoteData, true);
        return $quoteID;

    }

    public function updatePickupDelivery($quote)
    {
        if (empty($quote->id)) {
            throw new Exception('No id passed for quote pickupDelivery');
        }

        $validArray = array("", "Pickup", "Delivery");


        if (!in_array($quote->pickupDelivery, $validArray)) {
            throw new Exception('Invalid pickup / delivery sent');
        }

        $db = connect_db();

        $sql = " update quotesSales set pickupDelivery=? where  id=?  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("si", $quote->pickupDelivery, $quote->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to Update pickupDelivery');
        };

        return true;
    }

    public function updateSalesDiscount($salesDiscount, $quoteID)
    {
        $db = connect_db();

        $salesDiscountJSON = json_encode($salesDiscount);
        $sql = "update quotesSales set SalesDiscount=? where  id=? ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("si", $salesDiscountJSON, $quoteID);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set SalesDiscount');
        };

    }

    public function update($quoteID, $quoteData, $sameVersion = false)
    {

        $userID = '';

        if (isset($quoteData->userID)) {
            $userID = $quoteData->userID;
            unset($quoteData->userID);
        }

        if (isset($quoteData->SalesPersonNameOnQuote)) {
            unset($quoteData->SalesPersonNameOnQuote);
        }

        if(isset($quoteData->SalesPersonInfo)){
            unset($quoteData->SalesPersonInfo);
        }

        unset ($quoteData->versions);

        unset($quoteData->quoted);
        $quoteData = $this->prepIncomingQuoteDataWithNoCustomer($quoteData);


        $db = connect_db();
        $sql = sqlUpdateBuilderID($db, $quoteData, 'quotesSales', $quoteID);


        if ($db->query($sql) === false) {
            throw new Exception('Database failed to Update '.$sql);

        } else {
            $config['quoteID'] = $quoteID;
            $config['table'] = 'SQ_order_logs';
            $config['sql'] = $sql;
            $config['db'] = $db;
            $config['userID'] = $userID;

            $version = logSQLWithConfigReturnVersion($config, $sameVersion);

            $sql = sqlUpdateBuilderID($db, array('version' => $version), 'quotesSales', $quoteID);
            if ($db->query($sql) === false) {
                throw new Exception('Database failed to Update '.$sql);

            }

            return $quoteID;
        }

    }


    public function add($newQuote)
    {

        $newQuote = $this->prepIncomingQuoteDataWithNoCustomer($newQuote, false);
        $newQuote->quoted  = date("Y-m-d H:i:s");

        $db = connect_db();
//        if (empty($newQuote->id)) {
//            $newQuote->id = $this->getSalesCount();
//        }


        $sql = sqlInsertBuilder($db, $newQuote, 'quotesSales');

        if ($db->query($sql) === false) {
            throw new Exception('Database failed to insert '.$sql);

        } else {
            $last_inserted_id = $db->insert_id;

            $sql = "update quotesSales set orderName=? where id=".$last_inserted_id;
            $stmt = $db->prepare($sql);

            $prefix = '';
            if (!empty($newQuote->prefix)) {
                $prefix = $newQuote->prefix;
            }
            $orderName = $prefix.$last_inserted_id;

            $stmt->bind_param("s", $orderName);
            $stmt->execute();

            $affected_rows = $db->affected_rows;
            return $last_inserted_id;
        }

    }

    public function getSalesCount()
    {
        $db = connect_db();

        $toInsert['date'] = date("Y-m-d H:i:s");

        $sql = sqlInsertBuilder($db, $toInsert, 'salesCount');

        if ($db->query($sql) === false) {
            throw new Exception('Database failed to get a new count'.$sql);

        } else {
            $last_inserted_id = $db->insert_id;
            return $last_inserted_id;
        }
    }


    public function transferToSalesPersonIfPossibleNewQuoteAndInsertId($newQuote, $last_inserted_id, $guestForm = false)
    {

        $assigned = false;
        if (isUK()) {
            $this->transferQuote($last_inserted_id, 18);

            return '';
        } else {
            $user = new User();
            if(property_exists($newQuote, 'existSalesPersonId')){
                $salePerson = $user->getSalesPersonForId($newQuote, $newQuote->existSalesPersonId);

                if (empty($salePerson['id'])) {
                    return $salePerson;
                }

                $this->transferQuote($last_inserted_id, $salePerson['id']);
                $assigned = true;
            }
            if (property_exists($newQuote,'assignLead') && !$assigned) {
                $salePerson = $user->getSalesPersonForId($newQuote, $newQuote->assignLead);

                if (empty($salePerson['id'])) {
                    return $salePerson;
                }

                $this->transferQuote($last_inserted_id, $salePerson['id']);
                $assigned = true;

            }
            if(!$assigned && !$guestForm){

                $salePerson = $user->getSalesPersonForArea($newQuote);

                if (empty($salePerson['id'])) {
                    return $salePerson;
                }

                $this->transferQuote($last_inserted_id, $salePerson['salesPersonId']);
            }

            if($guestForm){
                $salePerson = $user->getSalesPersonForId($newQuote, 83); // 83 - Barry BLG account id

                if (empty($salePerson['id'])) {
                    return $salePerson;
                }
                $this->transferQuote($last_inserted_id, $salePerson['id']);
            }


            $message = 'https://'.$_SERVER['SERVER_NAME'].'/?tab=viewQuote&quote='.$last_inserted_id;

            if ($salePerson['sendMessage'] == 1) {
                $message = $message."\n Welcome email was sent to ".$salePerson['guestName']." (".$salePerson['guestEmail'].") \n";
            }

            $emailInfo = new stdClass();
            $emailInfo->from = "leads@salesquoter.com";
            $emailInfo->subject = "You have a new lead: ".$salePerson['prefix'].$last_inserted_id;
            $emailInfo->message = $message;
            $emailInfo->address = $salePerson['email'];

            if (filter_var($emailInfo->address, FILTER_VALIDATE_EMAIL)) {
                sendEmailViaSendGrid($emailInfo);
            }
            return $salePerson;
        }
    }

    public function findCustomerByGuestEmail($email){
        $db = connect_db();

        $getCustomerSQL = "SELECT SalesPerson from quotesSales JOIN customer ON quotesSales.Customer = customer.id JOIN users ON quotesSales.SalesPerson = users.id 
                          WHERE customer.email = ? and users.active = 1 ORDER BY quotesSales.id";

        $stmt = $db->prepare($getCustomerSQL);
        $stmt->bind_param("s", $email);

        $stmt->execute();
        $results = $stmt->get_result();

        $data = $results->fetch_assoc();

        if(!empty($data['SalesPerson'])){
            return $data['SalesPerson'];
        }
        return false;
    }


    public function addGuestQuote($newQuote)
    {
        $newQuote->Cart = $this->cleanCart($newQuote->Cart);
        $newQuote->total = $this->quoteTotal($newQuote);
        if(isset($newQuote->SalesDiscount)){
            unset($newQuote->SalesDiscount);
        }
        $newQuote->quantity = $this->guestNumberOfDoorsWithCart($newQuote->Cart);

        $newQuote->Cart = json_encode($newQuote->Cart);
        if (json_last_error() != 0) {
            throw new Exception('Add Quote has bad data, can\'t convert to JSON');
        }

        $newQuote->type = 'quote';
        $newQuote->quoted = date("Y-m-d H:i:s");
        $newQuote->status = 'new';
        $newQuote->leadQuality = 'New';
        $newQuote->total = number_format($newQuote->total, 2, '.', '');
//        $newQuote->id = $this->getSalesCount();

        $assignLead = null;
        $guestEmail = $newQuote->guestEmail;

        $guestForm = false;
        if(isset($newQuote->guestForm)){
            $guestForm = $newQuote->guestForm;
            unset($newQuote->guestForm);
        }

        unset($newQuote->guestEmail);
        if(property_exists($newQuote,'assignLead')){
            $assignLead = $newQuote->assignLead;
            unset($newQuote->assignLead);
        }
        $db = connect_db();
        $sql = sqlInsertBuilder($db, $newQuote, 'quotesSales');


        if ($db->query($sql) === false) {
            throw new Exception('Database failed to insert '.$sql);

        } else {
            $last_inserted_id = $db->insert_id;
            $this->updateOrderNameWithId($last_inserted_id, $last_inserted_id);

            $affected_rows = $db->affected_rows;
            if($assignLead){
                $newQuote->assignLead = $assignLead;
            }

            $existSalesPersonId = $this->findCustomerByGuestEmail($guestEmail);
            if(!empty($existSalesPersonId)){
                $user = new User();
                if($user->getActiveUser($existSalesPersonId)){
                    $newQuote->existSalesPersonId = $existSalesPersonId;
                }
            }
//
            $salesPerson = $this->transferToSalesPersonIfPossibleNewQuoteAndInsertId($newQuote, $last_inserted_id, $guestForm);

            $response['salesPerson'] = $salesPerson;
            $response['id'] = $last_inserted_id;
            $response['amount'] = $newQuote->total;
            $response['status'] = 'Success';

            $newAddress = new stdClass();
            $newAddress->quote_id = $last_inserted_id;

            if (!empty($salesPerson['salesPersonId'])) {
                $newAddress->sales_person_id = intval($salesPerson['salesPersonId']);
            }
            $newAddress->billing_contact = $salesPerson['guestName'];
            $newAddress->billing_phone = $salesPerson['guestPhone'];
            $newAddress->billing_email = $salesPerson['guestEmail'];
            $newAddress->billing_zip = $salesPerson['guestZip'];

            $sql = sqlInsertBuilder($db, $newAddress, 'SQ_addresses');
            if ($db->query($sql) === false) {
                throw new Exception('Database failed to insert '.$sql);
            }

            return $response;
        }
    }


    public function cleanCart($cart)
    {
        $cleanCart = array();
        if (empty($cart)) {
            return '';
        }
        foreach ($cart as $door) {
            if (isset($door->total)) {
                array_push($cleanCart, $door);
            }
        }

        return $cleanCart;
    }


    public function depositDue($quote)
    {
        $total = $this->quoteTotal($quote);
        $payments = $this->cartTotalPayments($quote);
        $depositDue = ($total / 2) - $payments;

        //UK now has a different deposit amount
        if (isUK()) {
            $depositDue = ($total * .5 ) - $payments;
        }
        if ($depositDue < 0) {
            $depositDue = 0;
        }
        return $depositDue;
    }


    public function balanceDue($quote)
    {
        $total = $this->quoteTotal($quote);
        $payments = $this->cartTotalPayments($quote);

        $balanceDue = $total - $payments;

        return $balanceDue;
    }


    public function cartTotal($quote)
    {

        $cart = $quote->Cart;
        $total = 0;
        $count = 0;

        if (empty($cart)) {
            return 0;
        }
        foreach ($cart as $door) {
            while ($count < $door->quantity) {
                $count++;
                if (isset($door->total)) {
                    $total += floatval($door->total);
                }
            }
            $count = 0;
        }

        return $total;
    }


    public function quoteTotal($quote)
    {
        $total = $this->cartTotal($quote);

        $markUp = $this->markUpAmount($quote);
        $discounts = $this->getDiscounts($quote);

        $screens = $this->getFlatAmount($quote, 'screens');
        $tax = $this->getFlatAmount($quote, 'tax');
        $extra = $this->getFlatAmount($quote, 'extra');
        $additionalExtras = $this->getAdditionalExtras($quote);

        $shipping = $this->getFlatAmount($quote, 'shipping');
        $installation = $this->getFlatAmount($quote, 'installation');

        $total = floatval($total) + (floatval($markUp) * floatval($total)) + $screens - $discounts;
        if (isUK()) {
            $total = $total + $extra + $shipping + $installation + $additionalExtras;
            $total = $total + (($total * $tax) / 100);

        } else {
            $total = $total + (($total * $tax) / 100);
            $total = $total + $extra + $shipping + $installation;
        }

        return $total;
    }

    public function guestNumberOfDoorsWithCart($cart)
    {
        $total = 0;
        foreach ($cart as $door) {
            if (isset($door->quantity)) {
                $total = $total + $door->quantity;
            }
        }

        return $total;
    }


    public function numberOfDoors($quote)
    {
        $cart = json_decode($quote['Cart']);
        $total = 0;

        if (empty($cart)) {
            return 0;
        }
        foreach ($cart as $door) {
            if (isset($door->quantity)) {
                $total = $total + $door->quantity;
            }
        }

        return $total;
    }


    public function numberOfDoorsQuoteObject($quote)
    {
        $newQuote = array();
        $newQuote['Cart'] = $quote->Cart;

        return $this->numberOfDoors($newQuote);
    }


    public function numberOfPanels($quote)
    {
        $cart = json_decode($quote['Cart']);

        $total = 0;
        foreach ($cart as $door) {
            if (isset($door->numberOfPanels)) {
                $total = $total + $door->numberOfPanels;
            }
        }

        return $total;
    }


    function daysFromNow($date)
    {
        $previousDate = strtotime($date);
        $today = date("Y-m-d");
        $now = strtotime($today);

        $secondsLeft = $now - $previousDate;

        $days = floor($secondsLeft / (60 * 60 * 24));
        return $days;
    }


    /**
     * @param $type
     */
    function getProductionOrders($type, $order = 'id', $direction = 'asc', $search = '')
    {
        $validOrders = array('companyName', 'lastName', 'quantity', 'total', 'startedProd', 'dueDate', 'estComplete');
        if (!in_array($order, $validOrders)) {
            $order = 'id';
        }

        $db = connect_db();

        $sql = " SELECT orderName, po, paidInFull, version, quotesSales.id, SalesDiscount, Cart, prefix, postfix, woodOrdered, glassOrdered,
                    productionNotes, status, companyName, lastName, firstName, phone, email, quantity, total,
                    orderDate, dueDate, estComplete,  startedProd,   `type`,  depositDue, balanceDue from quotesSales 
                    LEFT JOIN customer ON quotesSales.Customer=customer.id
                    LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id ";

        $where = "where status !='deleted' and `type`='$type'  ";

        if ($type == 'Hold') {
            $where = "where status !='deleted' and `status`='Hold'  ";

        } else if ($type == "viewAll") {
            $where = "where status !='deleted' and
            ( `type`='Needs Survey' or `type`='Ready For Production' or `type`='In Production' or `type`='Completed' or `type`='Delivered') ";
        } else if ($type == "viewAllSurvey"){
            $where = "where status !='deleted' and
            (`type`='Ready For Production' or type='Needs Survey') ";
        }


        if (empty($search)) {
            $sqlSearch = "%%";
        } else {
            $sqlSearch = "%$search%";
        }
        $extraWhere = " and (SQ_addresses.billing_email like ? OR  SQ_addresses.shipping_email like ? OR  customer.email like ? OR lastName like ? OR companyName like ? OR quotesSales.orderName like ? OR  quotesSales.orderName like ?) ";


        $order = " ORDER BY $order $direction ";
        $sql = $sql.$where.$extraWhere;

        $stmt = $db->prepare($sql);
        $stmt->bind_param("sssssss", $sqlSearch, $sqlSearch,$sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);


        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;

        $offsetLimit = buildOffSetLimitWithPageLimit($this->page, $this->limit);

        $sql = $sql.$order.$offsetLimit;

        $stmt = $db->prepare($sql);
        $stmt->bind_param("sssssss", $sqlSearch, $sqlSearch,$sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);

        $stmt->execute();
        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['numberOfItems'] = $this->numberOfDoors($options);
            $options['numberOfPanels'] = $this->numberOfPanels($options);
            $options['number'] = $options['id'];
            $options['SalesDiscount'] = json_decode($options['SalesDiscount']);
            $options['Cart'] = json_decode($options['Cart']);
            $orderDate = strtotime($options['orderDate']);
            $dueDate = strtotime($options['dueDate']);
            $estComplete = strtotime($options['estComplete']);
            $startedProd = strtotime($options['startedProd']);

            $options['daysInProduction'] = $this->daysFromNow($options['startedProd']);

            $options['po'] = $this->nonNullReturn($options['po']);
            $options['orderDate'] = $this->validDate(date('m/d/y', $orderDate));
            $options['dueDate'] = $this->validDate(date('m/d/y', $dueDate));
            $options['estComplete'] = $this->validDate(date('m/d/y', $estComplete));
            $options['startedProd'] = $this->validDate(date('m/d/y', $startedProd));
//        $options['id'] = "Q"+$options['id'];
            $options['total'] = money_format('%!n', $options['total']);
            $options['depositDue'] = money_format('%!n', $options['depositDue']);
            $options['balanceDue'] = money_format('%!n', $options['balanceDue']);

            $data[] = $options;
        }

        $results = array('total' => $count, 'data' => $data, 'sql' => $sql, 'search' => $sqlSearch);

        return $results;
    }

    public function validDate($date)
    {

        $checkDate = preg_split("/ /", $date);


        if ($date == '01/01/70' or $date == '11/30/-1' or $date == '11/30/-0001' or $checkDate[0] == '12/31/1969') {
            $date = '--';
        }

        return $date;

    }


    public function markUpAmount($quote)
    {
        if (isset($quote->SalesDiscount)) {
            if (isset($quote->SalesDiscount->Totals)) {
                if (isset($quote->SalesDiscount->Totals->code)) {
                    $code = preg_split("/-/", $quote->SalesDiscount->Totals->code);
                    if (isset($code[1])) {
                        return $code[1] / 100;
                    } else {
                        return 0;
                    }


                }
            }
        }

    }


    public function setSalesDiscountAmount($quote, $property, $amount)
    {


        if (empty($quote->SalesDiscount)) {
            $values = array('Totals' => array($property => $amount), 'Discounts' => array(), 'Payments' => array());
            $values = json_encode($values);
            $values = json_decode($values);

            $quote->SalesDiscount = $values;
        }

        $quote->SalesDiscount->Totals->{$property} = $amount;

        return $quote;

    }

    public function getDiscounts($quote)
    {
        if (empty($quote->SalesDiscount)) {
            return 0;
        }
        if (empty($quote->SalesDiscount->Totals)) {
            return 0;
        }
        if (empty($quote->SalesDiscount->Discounts)) {
            return 0;
        }

        $total = $this->cartTotal($quote);
        $markUp = $this->markUpAmount($quote);
        $preTotal = $total + ($total * $markUp);


        $totalDiscount = 0;

        foreach ($quote->SalesDiscount->Discounts as $discount) {
            if ($discount->type == 'flat') {
                $totalDiscount = $totalDiscount + floatval($discount->amount);
            } else {
                $totalDiscount = $totalDiscount + floatval((($discount->amount * $preTotal) / 100));
            }
        }

        return $totalDiscount;


    }


    public function getAdditionalExtras($quote)
    {
        if (empty($quote->SalesDiscount)) {
            return 0;
        }
        if (empty($quote->SalesDiscount->Totals)) {
            return 0;
        }

        if (empty($quote->SalesDiscount->Totals->additionalExtras)) {
            return 0;
        }

        $additionalExtras = $quote->SalesDiscount->Totals->additionalExtras;

        $total = 0;
        foreach ($additionalExtras as $extra) {
            if (!empty($extra->amount)) {
               $total = $total + $extra->amount;
            }
        }

        return ($total);
    }


    public function getFlatAmount($quote, $item)
    {
        if (empty($quote->SalesDiscount)) {
            return 0;
        }
        if (empty($quote->SalesDiscount->Totals)) {
            return 0;
        }

        if (empty($quote->SalesDiscount->Totals->{$item})) {
            return 0;
        }

        if (empty($quote->SalesDiscount->Totals->{$item}->amount)) {
            return 0;
        }

        $amount = $quote->SalesDiscount->Totals->{$item}->amount;


        return ($amount);

    }

    public function cartTotalPayments($quote)
    {
        $payments = new Payment();

        $total = 0;
        if (!empty($quote->id)) {
            $total = $payments->getTotalPayments($quote->id);
        }

        return $total;
    }

    public function updateGuest($quoteID, $quoteData)
    {

        $quoteData->total = $this->quoteTotal($quoteData);
        $quoteData->quantity = $this->guestNumberOfDoorsWithCart($quoteData->Cart);


        $quoteData->Cart = json_encode($quoteData->Cart);
        if(isset($quoteData->SalesDiscount)){
            unset($quoteData->SalesDiscount);
        }
        if (json_last_error() != 0) {
            throw new Exception('Add Quote has bad data, can\'t convert to JSON');
        }

        $quoteData->quoted = date("Y-m-d H:i:s");

        $db = connect_db();
        $sql = sqlUpdateBuilderID($db, $quoteData, 'quotesSales', $quoteID);

        if ($db->query($sql) === false) {
            throw new Exception('Database failed to Update '.$sql);

        } else {

            return $quoteID;
        }

    }

    private function updateQuoteToTransferred($guestQuoteId, $salesPerson, $salesQuoteID)
    {

        if (empty($guestQuoteId)) {
            throw new Exception('Guest ID not Passed for updateQuoteToTransferred');
        }
        if (empty($salesPerson)) {
            throw new Exception('SalesPerson not Passed for updateQuoteToTransferred');
        }
        if (empty($salesQuoteID)) {
            throw new Exception('SalesQuoteID not Passed for updateQuoteToTransferred');
        }
        $db = connect_db();

        $log = "Transferred to SalesPerson: $salesPerson for Quote: $salesQuoteID";

        $sql = " UPDATE  quotesGuest set `status`='transferred',salesPerson=?, salesQuoteID=?, transferred = now(), `log` = ? WHERE id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("iisi", $salesPerson, $salesQuoteID, $log, $guestQuoteId);


        if (!$stmt->execute()) {
            throw new Exception('Database failed to Update '.$sql);
        };

    }


    public function transferQuote($guestQuoteId, $salesPerson)
    {
        $quote = $this->getQuoteForGuest($guestQuoteId);

        $guest = new Guest();
        $newGuest = $guest->getGuest($quote['guest']);

        $nameArray = explode(' ', $newGuest['name'], 2);

        $newGuest['firstName'] = $nameArray[0];
        if (isset($nameArray[1])) {
            $newGuest['lastName'] = $nameArray[1];
        }

        unset($newGuest['name']);
        $newGuest['billingZip'] = $newGuest['zip'];
        $newGuest['jobZip'] = $newGuest['zip'];

        unset($newGuest['zip']);

        $customer = new Customer();
        $customerID = $customer->add($newGuest);

        unset($quote['guest']);
        unset($quote['transferred']);

        $quote['SalesPerson'] = $salesPerson;
        $quote['Customer'] = $customerID;
        $quote = json_encode($quote);
        $quote = json_decode($quote);

        $user = new User();
        $salesUser = $user->getUser($salesPerson);


        $quote->prefix = $salesUser['prefix'];
        $quote->postfix = 'Q';
        $quote->orderName = $quote->prefix.$quote->id;

        if (!empty($salesUser['leadQuality'])) {
            $quote->leadQuality = $salesUser['leadQuality'];
        }

        $this->updateQuoteToSalesPerson($quote);

//        $newQuoteID = $this->add($quote);
//        $this->updateQuoteToTransferred($guestQuoteId, $salesPerson, $newQuoteID);

        return $quote->id;


    }

    public function updateOrderNameWithId($orderName, $id)
    {
        $db = connect_db();

        $sql = "update quotesSales set orderName=? where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("si", $orderName, $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to update to OrderName');
        };

    }

    public function updateQuoteToSalesPerson($quote)
    {
        $db = connect_db();

        $sql = "update quotesSales set SalesPerson=?, prefix=?, postfix=?, orderName=?, Customer=?, transferred=now() where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("isssii", $quote->SalesPerson, $quote->prefix, $quote->postfix, $quote->orderName, $quote->Customer, $quote->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to update to SalesPerson');
        };
    }


    public function getGuestQuote($quoteID, $all = null)
    {


        if (empty($quoteID)) {
            throw new Exception('Quote Parameters was not passed for getGuestQuote Lookup');
        }


        $db = connect_db();


        $sql = " SELECT orderName, quotesSales.id, Cart, `quantity`, guest, SalesPerson, total, quoted, transferred from quotesSales  WHERE quotesSales.id=?  ";

        if ($all == 'yes') {
            $sql = " SELECT * from quotesSales WHERE quotesSales.id=?  ";
        }

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if (isset($data['Cart'])) {
            $data['Cart'] = json_decode($data['Cart']);
        }

        return $data;

    }

    public function resetOrderToQuote($id, $salesPerson)
    {

        $db = connect_db();

        $sql = "update quotesSales set type='quote' where SalesPerson=? and id=?  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $salesPerson, $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Reset Orer to  Quote'.$sql);
        };

        return true;

    }


    public function convertToOrderForSalesPerson($id, $salesPerson)
    {

        $db = connect_db();
        $userObject = new User();
        $user = $userObject->getUser($salesPerson);

        if($user && $user['superSales']){
            $sql = "update quotesSales set type='order',version=1, orderDate=now() where id=? and type='quote' ";

            $stmt = $db->prepare($sql);
            $stmt->bind_param("s", $id);
        }else{
            $sql = "update quotesSales set type='order',version=1, orderDate=now() where SalesPerson=? and id=? and type='quote' ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ss", $salesPerson, $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Quote to Order'.$sql);
        };

        $sql = mysqli_real_escape_string($db, $sql);

        $logSQL = "INSERT INTO SQ_order_logs SET quoteID=?, `sql`='$sql', `userID`=? ,  `version`=1 ";
        $stmt = $db->prepare($logSQL);
        $stmt->bind_param("ss", $id, $salesPerson);
        if (!$stmt->execute()) {
            throw new Exception('Database failed to LOG for Order '.$logSQL);
        };

        return true;

    }

    public function convertToPendingProduction($id, $salesPerson)
    {


        $db = connect_db();
        $sql = "update quotesSales set type='Confirming Payment', postfix='A' where SalesPerson=? and id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $salesPerson, $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to confirmingPayment'.$sql);
        };


        $this->checkForScreen($id);


        return true;

    }

    public function sendEmailAboutScreensUS($quote) {

        $orderNumber = $quote['prefix'].$quote['id'];
        $subject = "Screen needed for {$orderNumber}";
        $message = "Screen needed for <a href='https://{$_SERVER['SERVER_NAME']}/?tab=viewOrder&order={$quote['id']}'>{$orderNumber}</a>";

//        https://local-flow1.rioft.com/?tab=viewOrder&order=116049&349041

        $email = "mallory@panoramicdoors.com";
        if ( $_SERVER['SERVER_NAME'] != 'panoramicdoors.salesquoter.com') {
            $email = 'fracka@fracka.com';
        }


        $emailInfo = (object) array(
            'from' => 'no-reply@salesquoter.com',
            'address' => $email,
            'subject' => $subject,
            'message' => $message
        );

        sendEmailViaSendGrid($emailInfo);

    }


    public function checkForScreen($id)
    {
        $quote = $this->getQuoteClean($id);

        if (preg_match("/Screen Left/", $quote['Cart']) or
            preg_match("/Screen Right/", $quote['Cart']) or
            preg_match("/Screen Split/", $quote['Cart']) ) {

                $orderNumber = $quote['prefix'].$quote['id'];
                $this->sendEmailAboutScreensUS($quote);
        }

    }

    public function convertToPreProduction($id, $salesPerson)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToProduction Can not be blank');
        }

        $db = connect_db();
        $sql = "update quotesSales set type='Ready For Production', startedProd = now() , postfix='WP' where id=? and SalesPerson=? ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $id, $salesPerson);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;
    }

    public function unconfirmFullPayment($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToProduction Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set type='Confirming Payment', paidInFull='' where id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;

    }

    public function confirmFullPayment($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToProduction Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set paidInFull='yes' where id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;
    }


    public function revertPaidInFull($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToProduction Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set paidInFull='no' where id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;
    }

    public function sendToNeedsSurvey($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToProduction Can not be blank');
        }


        $db = connect_db();
        $sql = "update quotesSales set type='Needs Survey', startedProd = now() where id=? and type='Confirming Payment' ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;
    }

    public function sendToProduction($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToProduction Can not be blank');
        }

        $db = connect_db();
        $sql = "update quotesSales set type='Ready For Production', startedProd = now() , postfix='WP' where id=? and type='Confirming Payment' ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;
    }


    public function sendToQuote($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToQuote Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set type='quote', postfix='Q' where id=?  ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Quote'.$sql);
        };

        return true;
    }


    public function sendToConfirmPayment($id)
    {
        if (empty($id)) {
            throw new Exception('ID for sendToConfirmPayment Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set type='Confirming Payment',postfix='A',paidInFull='no' where id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Move Order to Set To Production'.$sql);
        };

        return true;
    }

    public function needsSurvey($id)
    {
        if (empty($id)) {
            throw new Exception('ID for Needs Survey  Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set type='Needs Survey',status='new' where id=? and (type='In Production' or type='Completed' or type='Delivered' or type='Ready For Production' ) ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to needsSurvey '.$sql);
        };

        return true;

    }

    public function readyForProduction($id)
    {
        if (empty($id)) {
            throw new Exception('ID for Ready for Production  Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set leadQuality='new', type='Ready For Production',postfix='WP',status='new' where id=? and (type='In Production' or type='Completed' or type='Delivered' or type='Ready For Production' or type='Needs Survey' ) ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to inProduction '.$sql);
        };

        return true;

    }

    public function moveEstCompleteAhead()
    {

        $date = date('Y-m-d');
        $newDate = date('Y-m-d', strtotime($date.' + 14 days'));

        return $newDate;

    }

    public function setOrderDateNow($id)
    {

        if (empty($id)) {
            throw new Exception('ID for setOrderDateNow Required');
        }

        $db = connect_db();
        $sql = "update quotesSales set orderDate=now()  where id=?   ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to inProduction '.$sql);
        };

        return true;
    }

    public function getProductionCompleteDateFromQuote($quoteData)
    {

        $numberOfDaysAhead = $this->getProductionCompleteNumberOfDaysAhead($quoteData);
        $futureDate = $this->getWorkingDateForNumberOfDays($numberOfDaysAhead);

        return $futureDate;
    }

    public function getProductionCompleteNumberOfDaysAhead($quoteData)
    {
        $cart = json_decode($quoteData['Cart']);

        $largestTotal = 0;

        foreach ($cart as $door) {

            $addDays = 10;
            $bonusDays = 0;

            if ($door->module == "21") {
                $addDays = $addDays + 10;
            }

            if (!empty($door->extColor)) {
                if ($door->extColor->kind == 'custom' or $door->intColor->kind == 'custom') {
                    $bonusDays = 20;

                } else if ($door->extColor->kind == 'ral' or $door->extColor->kind == 'arch' or $door->extColor->kind == 'woodPremium' or $door->extColor->kind == 'woodStandard') {
                    $bonusDays = 10;
                } else if ($door->intColor->kind == 'ral' or $door->intColor->kind == 'arch' or $door->intColor->kind == 'woodPremium' or $door->intColor->kind == 'woodStandard') {
                    $bonusDays = 10;
                } else if ($door->extColor->name1 != $door->intColor->name1) {
                    $bonusDays = 10;
                }
            }

            $tempTotal = $addDays + $bonusDays;

            if ($tempTotal > $largestTotal) {
                $largestTotal = $tempTotal;
            }
        }
        return $largestTotal;
    }


    public function getWorkingDateForNumberOfDays($numberOfDays)
    {

        $futureDate = date('Y-m-j', strtotime($numberOfDays.' weekdays'));

        return $futureDate;
    }


    public function setProductionCompleteDate($quoteID, $quoteData)
    {
        if (empty($quoteID)) {
            throw new Exception('ID for setProductionCompleteDate Required');
        }

        $db = connect_db();

        $futureDate = $this->getProductionCompleteDateFromQuote($quoteData);

        $sql = "update quotesSales set  estComplete ='$futureDate' where id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quoteID);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set Complete Data '.$sql);
        };

        return true;
    }


    public function inProduction($id)
    {
        if (empty($id)) {
            throw new Exception('ID for inProduction Can not be blank');
        }

        $db = connect_db();

        $quote = $this->getQuote($id);

        $estComplete = $quote['estComplete'];
        if ($quote['estComplete'] == '0000-00-00') {
            $estComplete = $this->moveEstCompleteAhead();
        }

        $sql = "update quotesSales set leadQuality='new', type='In Production',postfix='P',status='new', estComplete=? where id=? and (type='Ready For Production' or type='Completed' or type='Delivered' or type='In Production' ) ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $estComplete, $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to inProduction '.$sql);
        };

        return true;

    }

    public function completed($id)
    {
        if (empty($id)) {
            throw new Exception('ID for completed Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set leadQuality='new', type='Completed',postfix='C',status='new', `completedDate`=now() where id=? and (type='In Production' or type='Ready For Production' or type='Delivered' or type='Completed') ";
     
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to completed to Set To Production'.$sql);
        };

        return true;

    }

    public function delivered($id)
    {
        if (empty($id)) {
            throw new Exception('ID for delivered Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set leadQuality='new', type='Delivered',postfix='D',`status`='new', `delivered`=now() where id=? and (type='In Production' or type='Ready For Production' or type='Completed' or type='Delivered' ) ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Delivered to Set To Production'.$sql);
        };

        return true;

    }


    public function onHold($id)
    {
        if (empty($id)) {
            throw new Exception('ID for on hold Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set leadQuality='holdOrder', status='hold', postfix=concat(postfix, 'H')  where id=? and (type='Delivered' or type='In Production' or type='Ready For Production' or type='Completed' ) ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to onHold to Set To Production'.$sql);
        };

        $this->sendHoldEmail($id);

        return true;

    }

    public function sendHoldEmail($quoteID){
        $db = connect_db();

        $sql = "SELECT quotesSales.orderName, quotesSales.prefix, users.name, users.email FROM `quotesSales`"
                . " LEFT JOIN users on quotesSales.SalesPerson = users.id where quotesSales.id=?";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

//        $orderNumber = $data['prefix'].$quoteID;

        $currentDateTime = date('Y-m-d H:i:s');
        $newDateTime = date('h:i A', strtotime($currentDateTime));

        $message = "Dear {$data['name']},<br/><br/>

        Order <a href='https://{$_SERVER['SERVER_NAME']}/?tab=viewQuote&quote={$quoteID}'>{$data['orderName']}</a>, was put on hold.<br/>
        Please verify with the customer it is ready to be produced and then remove the hold on the order. <br/><br/>

        Thank You,<br/>
        Sales Quoter<br/>
        <br/>
        <br/>
        P.S. Please don't reply to this email as it is not monitored.";

        $emailInfo = new stdClass();
        $emailInfo->from = "no-reply@salesquoter.com";
        $emailInfo->subject = "Order " . $data['orderName'] . " has been placed on Hold.";
        $emailInfo->message = $message;
        $emailInfo->address = $data['email'];

        if (filter_var($emailInfo->address, FILTER_VALIDATE_EMAIL)) {
            //This check is EXTREMELY important and must not be removed. Fracka 1/10/18
            if (!isUK()) {
                sendEmailViaSendGrid($emailInfo);
            }
        }
    }

    public function deleteQuote($id)
    {
        if (empty($id)) {
            throw new Exception('ID for Delete Can not be blank');
        }

        $db = connect_db();

        $sql = "update quotesSales set status='deleted' where id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Delete Quote'.$sql);
        };

        return true;

    }

    public function getAllOrdersWithConfig($config)
    {
        $salesPersonID = $config['salesPersonID'];
        $status = $config['status'];
        $direction = $config['direction'];
        $order = $config['order'];
        $search = $config['search'];
        $fullStatus = $config['fullStatus'];
        $superToken = $config['superToken'];

        if ($order == 'id') {
            $order = 'quotesSales.'.$order;
        }

        $typeOfStatus = array('needsPayment' => 'order', 'accounting' => 'Confirming Payment',  'holdOrder' => 'holdOrder',
            'preProduction' => 'Ready For Production', 'production' => 'In Production',
            'completed' => 'Completed', 'delivered' => 'Delivered');

        $db = connect_db();

        $extraWhere = " and (lastName like ? OR firstName like ? OR companyName like ? OR quotesSales.orderName like ? OR SQ_addresses.billing_email like ? OR SQ_addresses.shipping_email like ? OR customer.email like ? OR  quotesSales.orderName like ?  )";


        $salesPersonSQL = ' SalesPerson=? and';
        if (!empty($superToken)) {
            $salesPersonSQL = $this->addSuperUser($salesPersonID);
        }

        $select = "SELECT  orderName, leadQuality, SalesDiscount, quoted, version, quotesSales.id, companyName, prefix, postfix, status, lastName, firstName, phone, email, `quantity`,
                    total, orderDate, estComplete, dueDate, `type`,  depositDue, balanceDue, completedDate from quotesSales
                    LEFT JOIN customer ON quotesSales.Customer=customer.id
                    LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id ";

        $whereCore = " where ".$salesPersonSQL." Customer=customer.id and status !='deleted' ";


        if (!empty($typeOfStatus[$status])) {



            if ($status == 'accounting') {
                $wherePlus = " and  (`type` ='Needs Survey' or `type` ='Confirming Payment')  ".$extraWhere." and `leadQuality` != 'holdOrder' ORDER BY $order $direction";
            } else {
                $wherePlus = " and `type` ='".$typeOfStatus[$status]."' ".$extraWhere." and `leadQuality` != 'holdOrder' ORDER BY $order $direction";
            }


            if ($fullStatus == 0) {
                if ($typeOfStatus[$status] == 'In Production') {
                    $wherePlus = " and (`type` ='In Production'  or `type` = 'Ready For Production')  ".$extraWhere." ORDER BY $order $direction";
                }
            }

            if ($status == 'holdOrder') {
                $wherePlus = " and `leadQuality` ='".$typeOfStatus[$status]."' ".$extraWhere." ORDER BY $order $direction";
            }

        } else {
            $wherePlus = " and (`type`='order' or `type`='Confirming Payment' or `type`='Needs Survey' or `type` = 'Ready For Production' or
                    `type` = 'In Production' or  `type` = 'Completed' or  `type` = 'Delivered') ".$extraWhere." ORDER BY $order $direction  ";

        }

        $offsetLimit = buildOffSetLimitWithPageLimit($this->page, $this->limit);

        if (empty($search)) {
            $sqlSearch = "%%";
        } else {
            $sqlSearch = "%$search%";
        }

        $sql = $select.$whereCore.$wherePlus;

        $stmt = $db->prepare($sql);

        if (empty($superToken)) {
            $stmt->bind_param("sssssssss", $salesPersonID, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        } else {
            $stmt->bind_param("ssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        }

        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;

        $sql = $sql." ".$offsetLimit;


        $stmt = $db->prepare($sql);
        if (empty($superToken)) {
            $stmt->bind_param("sssssssss", $salesPersonID, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        } else {
            $stmt->bind_param("ssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        }

        $stmt->execute();

        $results = $stmt->get_result();

        if(!$results){
            return array("total" => 0, "data" => array());
        }

        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];

            $orderDate = strtotime($options['orderDate']);

            $options['quoted'] = $this->mysqlToHumanDate($options['quoted']);
            $options['estComplete'] = $this->mysqlToHumanDate($options['estComplete']);
            $options['completedDate'] = $this->mysqlToHumanDate($options['completedDate']);
            $options['dueDate'] = $this->mysqlToHumanDate($options['dueDate']);
            $options['orderDate'] = $this->mysqlToHumanDate($options['orderDate']);
//            $options['total'] = money_format('%!n', $options['total']);


            $options['SalesDiscount'] = json_decode($options['SalesDiscount']);
            if ($options['SalesDiscount']) {
                $options['SalesDiscount'] = (array) $options['SalesDiscount'];
            }

            if (empty($options['SalesDiscount'])) {
                $user = new User();
                $defaults = $user->getDefaults($data['SalesPerson']);
                $options['SalesDiscount'] = $defaults;
                $saveData = json_encode($data);
                $saveData = json_decode($saveData);

                $this->update($data['id'], $saveData);
            }

            if ($fullStatus == 0) {
                if ($options['type'] == 'order') {
                    $options['type'] = 'Confirming Payment';
                }
                if ($options['type'] == 'Ready For Production') {
                    $options['type'] = 'In Production';
                }
            }


            $payment = new Payment();
            $payments = $payment->getPayments($options['id']);
            $options['SalesDiscount']['Payments'] = $payments;

            $data[] = $options;
        }

        $results = array("total" => $count, "data" => $data);

        return $results;
    }

    public function getAllSaleQuotesWithConfigBindSQL($stmt, $superToken, $salesPersonID, $sqlSearch)
    {
        if (empty($superToken)) {
            $stmt->bind_param("sssssssssss", $salesPersonID, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        } else {
            $stmt->bind_param("ssssssssss", $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch, $sqlSearch);
        }
    }

    public function addSuperUser($salesPersonID)
    {
        $user = new User();
        $managerString = $user->getManager($salesPersonID);

        if (!empty($managerString)) {
            $salesPersonSQL = "SalesPerson IN (".$managerString.") and ";
        } else {
            $salesPersonSQL = '';
        }

        return $salesPersonSQL;
    }

    public function getAllSaleQuotesWithConfig($config)
    {
        $salesPersonID = $config['salesPersonID'];
        $leadQuality = $config['leadQuality'];
        $direction = $config['direction'];
        $order = $config['order'];
        $search = $config['search'];
        $superToken = $config['superToken'];

        if ($order == 'id') {
            $order = 'quotesSales.'.$order;
        }

        if ($leadQuality == "viewAll") {
            $leadQuality = '';
        }

        if (!in_array(strtolower($leadQuality), $this->validQuality)) {
            throw new Exception('Invalid Lead Quality Sent for get Quotes');
        }

        $leadWhere = '';
        if (!empty($leadQuality)) {
            $leadWhere = "and leadQuality='$leadQuality'";
        }

        $db = connect_db();
        $offsetLimit = buildOffSetLimitWithPageLimit($this->page, $this->limit);

        $salesPersonSQL = ' SalesPerson=? and';
        if (!empty($superToken)) {
            $salesPersonSQL = $this->addSuperUser($salesPersonID);
        }

        $sql = " SELECT orderName, quotesSales.id, followUp, secondFollowUp, billingZip as zip,  companyName, leadQuality, leadType,  prefix, postfix,  lastName, firstName, phone, `quantity`, total, quoted, `type`,
                  contacted, formComplete from quotesSales 
                  LEFT JOIN customer ON quotesSales.Customer=customer.id
                  LEFT JOIN SQ_addresses ON  SQ_addresses.quote_id = quotesSales.id
                  where ".$salesPersonSQL." Customer=customer.id and status !='deleted' and type='quote' $leadWhere and (lastName like ? OR firstName like ? OR phone like ? OR billingZip like ? OR companyName like ? OR quotesSales.orderName like ? OR SQ_addresses.billing_email like ? OR  SQ_addresses.shipping_email like ? OR customer.email like ? OR quotesSales.orderName like ? ) ORDER BY $order $direction  ";

        $stmt = $db->prepare($sql);

        if (empty($search)) {
            $sqlSearch = "%%";
        } else {
            $sqlSearch = "%$search%";
        }

        $this->getAllSaleQuotesWithConfigBindSQL($stmt, $superToken, $salesPersonID, $sqlSearch);

        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;

        $sql = $sql.$offsetLimit;
        

        $stmt = $db->prepare($sql);

        $this->getAllSaleQuotesWithConfigBindSQL($stmt, $superToken, $salesPersonID, $sqlSearch);

        $stmt->execute();

        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];

            if (isset($options['zip'])) {
                $options['zip'] = preg_replace('/\D/', '', $options['zip']);
            }

            if (!empty($options['phone'])) {
                $options['phone'] = preg_replace('/\D/', '', $options['phone']);
                $options['phone'] = "(".substr($options['phone'], 0, 3).") ".substr($options['phone'], 3, 3)."-".substr($options['phone'], 6);
            }
            $quoteDate = strtotime($options['quoted']);

            $options['quoted'] = date('m/d/Y', $quoteDate);
//        $options['id'] = "Q"+$options['id'];
            $options['total'] = money_format('%!n', $options['total']);

            $options['contacted'] = date("m/d/Y", strtotime($options['contacted']));
            $options['followUp'] = $this->mysqlToHumanDate($options['followUp']);
            $options['secondFollowUp'] = $this->mysqlToHumanDate($options['secondFollowUp']);

            $options['followUp'] = $this->nonNullReturn($options['followUp']);
            $options['secondFollowUp'] = $this->nonNullReturn($options['secondFollowUp']);

            $options['zip'] = $this->nonNullReturn($options['zip']);

            if ($options['contacted'] == '11/30/-0001' or $options['contacted'] == '01/01/1970') {
                $options['contacted'] = '--';
            }
            $data[] = $options;
        }

        $results = array("total" => $count, "data" => $data);

        return ($results);
    }


    public function getAllSaleQuotes($salesPersonID, $leadQuality = null)
    {

        if (!in_array(strtolower($leadQuality), $this->validQuality)) {
            throw new Exception('Invalid Lead Quality Sent for get Quotes');

        }

        $leadWhere = '';
        if (!empty($leadQuality)) {
            $leadWhere = "and leadQuality='$leadQuality'";
        }
        $db = connect_db();


        $offsetLimit = buildOffSetLimitWithPageLimit($this->page, $this->limit);


        $sql = " SELECT quotesSales.id, followUp, secondFollowUp,  companyName, leadQuality,  prefix, postfix,  lastName, firstName, phone, `quantity`, total, quoted, `type`,
                  contacted from quotesSales,customer where SalesPerson=?
                   and Customer=customer.id and status !='deleted' and type='quote' $leadWhere ORDER BY quotesSales.id DESC  ";


        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $salesPersonID);
        $stmt->execute();
        $stmt->store_result();
        $count = $stmt->num_rows;


        $sql = $sql.$offsetLimit;
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $salesPersonID);
        $stmt->execute();

        $results = $stmt->get_result();
        $data = Array();
        setlocale(LC_MONETARY, 'en_US.utf8');

        while ($options = $results->fetch_assoc()) {

            $options['number'] = $options['id'];

            if (isset($options['zip'])) {
                $options['zip'] = preg_replace('/\D/', '', $options['zip']);
            }

            if (!empty($options['phone'])) {
                $options['phone'] = preg_replace('/\D/', '', $options['phone']);
                $options['phone'] = "(".substr($options['phone'], 0, 3).") ".substr($options['phone'], 3, 3)."-".substr($options['phone'], 6);
            }
            $quoteDate = strtotime($options['quoted']);


            $options['quoted'] = date('m/d/Y', $quoteDate);
//        $options['id'] = "Q"+$options['id'];
            $options['total'] = money_format('%!n', $options['total']);

            $options['contacted'] = date("m/d/Y", strtotime($options['contacted']));
            $options['followUp'] = $this->mysqlToHumanDate($options['followUp']);
            $options['secondFollowUp'] = $this->mysqlToHumanDate($options['secondFollowUp']);

            $options['followUp'] = $this->nonNullReturn($options['followUp']);
            $options['secondFollowUp'] = $this->nonNullReturn($options['secondFollowUp']);


            if ($options['contacted'] == '11/30/-0001' or $options['contacted'] == '01/01/1970') {
                $options['contacted'] = '--';
            }
            $data[] = $options;
        }

        $results = array("total" => $count, "data" => $data);

        return ($results);

    }

    public function nonNullReturn($value)
    {
        if (empty($value)) {
            $value = '';
        }

        if ($value == '01/01/1970 (12:00 am)' or $value == '11/30/-0001 (12:00 am)') {
            $value = '';
        }

        if ($value == '01/01/1970' or $value == '11/30/-0001') {
            $value = '--';
        }

        return $value;
    }


    protected function addPanelItems($item)
    {

        if (empty($item->module)) {
            return '';
        }
        $id = $item->module;
        $module = new Module();
        $moduleInfo = $module->getModuleInfo($id);

        $panelOptions = json_decode(json_encode($moduleInfo['panelOptions']));

        if (!empty($item->chooseFrameSection)) {
            $panelOptions->frame->selected = splitForSecondOption($item->chooseFrameSection);
        }
        if (!empty($item->chooseTrackSection)) {
            $panelOptions->track->selected = splitForSecondOption($item->chooseTrackSection);
        }
        if (!empty($item->choosePanels)) {
            $panelOptions->panels->selected = splitForSecondOption($item->choosePanels);
        }
        if (!empty($item->swingDirectionSection)) {
            $panelOptions->swingInOrOut->selected = splitForSecondOption($item->swingDirectionSection);
        }
        if (!empty($item->chooseSwings)) {
            $panelOptions->swingDirection->selected = $item->chooseSwings;
        }
        if (!empty($item->panelMovement)) {
            $panelOptions->movement->slideLeft = splitForSecondOption($item->panelMovement);
            $panelOptions->movement->selected = $item->chooseSwings;
        }
        $item->panelOptions = $panelOptions;

        return $item;
    }


    protected function convertCart($cart)
    {
        $cart = json_decode($cart);

        $newCart = array();
        if (empty($cart)) {
            return '';
        }

        foreach ($cart as $item) {
            if (empty($item->panelOptions)) {
                $item = $this->addPanelItems($item);
                if (empty($item)) {
                    continue;
                }
            }
            $newCart[$item->unique] = $item;
        }

        return $newCart;
    }


    public function salesPersonAccessToQuote($quoteId, $salesPersonId)
    {
        $db = connect_db();
        $sql = " SELECT id FROM quotesSales  WHERE SalesPerson=? AND quotesSales.id=? AND status !='deleted' ";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $salesPersonId, $quoteId);
        $stmt->execute();
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if (empty($data)) {
            return false;
        }

        return true;
    }


    public function getQuoteAndCustomer($id)
    {
        if (empty($id)) {
            throw new Exception('No Quote ID passed for getQuoteAndCustomer');
        }
        $db = connect_db();

        $sql = " SELECT  orderName, completedDate, jobPhone, paidInFull, contact, customerREF, customerPO, leadSource, version, followUp, dueDate, estComplete, po, secondFollowUp, quotesSales.id, leadQuality, quoteNotes, productionNotes, prefix, orderDate, postfix, Cart, Customer, SalesDiscount, lastName,firstName, `type`, phone, `quantity`,
            companyName, startedProd, email, billingAddress1, billingAddress2, billingCity, billingState, billingZip,
            jobAddress1, jobAddress2, jobCity, jobState, jobZip, total, quoted, SalesPerson , contacted, customerNotes, formComplete, sameAsShipping,
            shippingName, shippingEmail, billingName, billingEmail, pickupDelivery
            FROM quotesSales,customer  WHERE  quotesSales.id=? AND Customer=customer.id ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $reformedCustomerQuoteData = $this->buildQuoteAndCustomerResponse($data);
        $address = $this->getNewBillingAddress($id);
        $reformedCustomerQuoteData['address'] = $address;

        return $reformedCustomerQuoteData;
    }


    public function getNewBillingAddress($quote_id)
    {
        $db = connect_db();
        $sql = " SELECT * FROM SQ_addresses  WHERE  quote_id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quote_id);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        unset($data['id']);
        unset($data['quote_id']);
        unset($data['sales_person_id']);

        return $data;
    }

    public function buildQuoteAndCustomerResponse($data)
    {
        if (empty($data)) {
            return $data;
        }

//        $addArray = $this->customerInfo;

        $data['dueDate'] = $this->mysqlToHumanDate($data['dueDate']);
        $data['completedDate'] = $this->mysqlToHumanDate($data['completedDate']);
        $data['estComplete'] = $this->mysqlToHumanDate($data['estComplete']);
        $data['startedProd'] = $this->mysqlToHumanDate($data['startedProd']);
        $data['quoted'] = $this->mysqlToHumanDate($data['quoted']);
        $data['orderDate'] = $this->mysqlToHumanDate($data['orderDate']);
        $data['followUp'] = $this->mysqlToHumanDate($data['followUp']);
        $data['secondFollowUp'] = $this->mysqlToHumanDate($data['secondFollowUp']);

//        $newCustomer = array();
//        $newCustomer['customerID'] = $data['Customer'];

//        foreach ($addArray as $add) {
//            if (isset($data[$add])) {
//                $newCustomer[$add] = $data[$add];
//                unset($data[$add]);
//            } else {
//                $newCustomer[$add] = '';
//                unset($data[$add]);
//            }
//        }

        $getCustomer = new Customer();

        $customer = $getCustomer->getCustomer($data['Customer']);


        if(empty($customer)){
            $customerData = array();
            $customerFields = array('firstName','lastName','companyName','email',
                'phone','billingAddress1','billingAddress2','billingCity','billingState','billingZip',
                'jobAddress1','jobAddress2','jobCity','jobState','jobZip');

            foreach ($customerFields as $field){
                $customerData[$field] = "";
            }

            $customer = new Customer();
            $id = $customer->add($customerData);
            $data['Customer'] = $customerData;
            $data['Customer']['id'] = $id;
            $data['Customer']['customerID'] = $id;
            
            $db = connect_db();

            $sql = "update quotesSales set Customer=? where id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $id, $data['id']);
            $stmt->execute();
        }else{
            $data['Customer'] = $customer[0];
            $data['Customer']['customerID'] = $data['Customer']['id'];
        }


        $data['po'] = $this->nonNullReturn($data['po']);
        $data['startedProd'] = $this->nonNullReturn($data['startedProd']);
        $data['orderDate'] = $this->nonNullReturn($data['orderDate']);
        $data['quoteNotes'] = $this->nonNullReturn($data['quoteNotes']);
        $data['productionNotes'] = $this->nonNullReturn($data['productionNotes']);
        $data['followUp'] = $this->nonNullReturn($data['followUp']);
        $data['secondFollowUp'] = $this->nonNullReturn($data['secondFollowUp']);
        $data['Cart'] = $this->convertCart($data['Cart']);
        $data['SalesDiscount'] = json_decode($data['SalesDiscount']);
        if ($data['SalesDiscount']) {
            $data['SalesDiscount'] = (array) $data['SalesDiscount'];
        }

        if (empty($data['SalesDiscount'])) {
            $user = new User();
            $defaults = $user->getDefaults($data['SalesPerson']);

            $data['SalesDiscount'] = $defaults;
            $saveData = json_encode($data);
            $saveData = json_decode($saveData);
            $this->update($data['id'], $saveData);
        } else {
            $payment = new Payment();
            $payments = $payment->getPayments($data['id']);

            $data['SalesDiscount']['Payments'] = $payments;
        }
        $data['versions'] = $this->buildVersionsFromQuoteID($data['id']);

        return $data;
    }

    public function getSalesQuote($getQuote)
    {

        if (empty($getQuote['quote'])) {
            throw new Exception('Quote Parameters was not passed for getSalesQuote Lookup');
        }

        $db = connect_db();

//        $sqlSelect = " SELECT `status`, salesTerms, jobPhone, paidInFull, contact, customerREF, customerPO, leadSource, version, followUp, dueDate, estComplete, po, secondFollowUp, quotesSales.id, leadQuality, quoteNotes, productionNotes, prefix, orderDate, postfix, Cart, Customer, SalesDiscount, lastName,firstName, `type`, phone, `quantity`,
//            companyName, startedProd, email, billingAddress1, billingAddress2, billingCity, billingState, billingZip,
//            jobAddress1, jobAddress2, jobCity, jobState, jobZip, total, quoted, SalesPerson , contacted, customerNotes, formComplete, sameAsShipping,
//            shippingName, shippingEmail, billingName, billingEmail, pickupDelivery
//            FROM quotesSales,customer ";

        $sqlSelect = " SELECT 
            orderName, completedDate, `status`, name as SalesPersonNameOnQuote, users.phone as SalesPersonPhone, salesTerms, paidInFull, version, followUp, dueDate, estComplete, po, 
            secondFollowUp, qs.id, qs.leadQuality, quoteNotes, productionNotes, paymentNotes, qs.prefix, orderDate, postfix, Cart, Customer, SalesDiscount, 
            qs.type, `quantity`, startedProd, total, quoted, SalesPerson , contacted, 
            formComplete, pickupDelivery, billing_address1, billing_address2, billing_city, billing_state, billing_zip, billing_phone, 
            billing_contact, billing_email, shipping_address1, shipping_address2, shipping_city, shipping_state, shipping_zip, shipping_phone, shipping_contact,
            shipping_email FROM quotesSales qs LEFT JOIN SQ_addresses ON qs.id = SQ_addresses.quote_id LEFT JOIN users ON qs.SalesPerson = users.id ";

        $where = " WHERE qs.id=? AND qs.status !='deleted' ";

        if (empty($getQuote['salesPerson']) or !empty($getQuote['superToken'])) {

            $sql = $sqlSelect.$where;

//            print_r($sql);
            $stmt = $db->prepare($sql);

            $stmt->bind_param("s", $getQuote['quote']);

        } else {

            $extraWhere = " AND SalesPerson=? ";
            $sql = $sqlSelect.$where.$extraWhere;

//            print_r($sql);
            $stmt = $db->prepare($sql);

            $stmt->bind_param("ss", $getQuote['quote'], $getQuote['salesPerson']);
        }


        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $newData = $this->buildQuoteAndCustomerResponse($data);
        $address = $this->getNewBillingAddress($getQuote['quote']);

        $newData['address'] = $address;

        return $newData;
    }

    public function buildVersionsFromQuoteID($quoteID)
    {

        $db = connect_db();
        $sql = "SELECT name,version FROM SQ_order_logs logs,users u  WHERE quoteID=? AND logs.UserID = u.id ORDER BY logs.logID DESC ";
        $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $quoteID);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();

        $data = array();
        while ($options = $result->fetch_assoc()) {
            $data[] = $options;
        }

        return $data;


    }

    public function mysqlToHumanDate($date)
    {
        $checkDate = preg_split("/ /", $date);

        if (!$date or $date == '0000-00-00' or $date == '0196-12-31' or $checkDate[0] == '1970-01-01') {
            return '--';
        }

        $newDate = strtotime($date);

        $finalDate = $this->validDate(date("m/d/Y", $newDate));
        if (isUK()) {
            $finalDate = $this->validDate(date("d/m/Y", $newDate));
        }
        return $finalDate;

    }

    public function humanDateToMysql($date)
    {

        $date = date('Y-m-d', strtotime(str_replace('-', '/', $date)));
        return $date;

    }


    public function getProductionOrder($getQuote)
    {

        if (empty($getQuote['quote'])) {
            throw new Exception('Quote Parameters was not passed for getProductionOrder Lookup');
        }

        $db = connect_db();

        $sql = " SELECT version, quotesSales.id, Cart, Customer, SalesDiscount, prefix, postfix, lastName,firstName, `type`, status, productionNotes, phone, `quantity`,
            companyName, email, billingAddress1, billingAddress2, billingCity, billingState, billingZip,
            jobAddress1, jobAddress2, jobCity, jobState, jobZip, total, quoted, SalesPerson , contacted, customerNotes
            FROM quotesSales,customer
            WHERE  quotesSales.id=? AND Customer=customer.id AND status !='deleted' ";

//        $sql = " SELECT quotesSales.id, Cart, Customer, SalesDiscount, total, quoted, SalesPerson , contacted
//            FROM quotesSales,customer
//            WHERE SalesPerson=? AND quotesSales.id=? AND Customer=customer.id AND status !='deleted' ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $getQuote['quote']);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        if (empty($data)) {
            return $data;
        }
        $addArray = $this->customerInfo;

        $data['quoted'] = date("m/d/Y (h:i a)", strtotime($data['quoted']));

        $newCustomer = array();
        $newCustomer['customerID'] = $data['Customer'];

        foreach ($addArray as $add) {

            if (isset ($data[$add])) {
                $newCustomer[$add] = $data[$add];
                unset($data[$add]);
            } else {
                $newCustomer[$add] = '';
            }

        }
        $data['Customer'] = $newCustomer;

        $data['Cart'] = json_decode($data['Cart']);

        $newCart = array();

        foreach ($data['Cart'] as $item) {
            $newCart[$item->unique] = $item;
        }
        $data['Cart'] = $newCart;

        $data['SalesDiscount'] = json_decode($data['SalesDiscount']);
        if ($data['SalesDiscount']) {
            $data['SalesDiscount'] = (array) $data['SalesDiscount'];
        }

        if (empty($data['SalesDiscount'])) {
            $user = new User();
            $defaults = $user->getDefaults($getQuote['salesPerson']);
            $data['SalesDiscount'] = $defaults;
            $saveData = json_encode($data);
            $saveData = json_decode($saveData);

            $this->update($getQuote['quote'], $saveData);


        }

        return $data;

    }


    public function addInvoiceNumber($quoteID)
    {
        $quoteData = $this->getQuote($quoteID);
        $cartJSON = $quoteData['Cart'];
        $cart = json_decode($cartJSON);
        if (json_last_error() != 0) {
            throw new Exception('Adding Invoice Number Failed -- Decoding Old Cart');
        }

        $newCart = array();
        foreach ($cart as $door) {
            $invoiceNumbers = $this->buildInvoiceNumbersWithDoorAndQuoteID($door, $quoteID);
            $door->invoiceNumbers = $invoiceNumbers;
            $newCart[] = $door;
        }

        $newCartJSON = json_encode($newCart);
        if (json_last_error() != 0) {
            throw new Exception('Adding Invoice Number Failed -- Encoding New Cart');
        }

        $db = connect_db();
        $sql = " update quotesSales set `Cart`=? where id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $newCartJSON, $quoteID);

        if (!$stmt->execute()) {
            throw new Exception('Adding Invoice Number Failed -- Update Cart with Invoice Numbers'.$sql);
        }

    }


    public function buildInvoiceNumbersWithDoorAndQuoteID($door, $quoteID)
    {
        $count = $door->quantity;


        $invoice = new Invoice();

        $invoiceNumber = array();
        $i = 0;
        while ($i < $count) {
            $i++;
            $invoiceNumber[] = $invoice->getNextInvoiceNumberWithQuoteIdAndCartNumber($quoteID, $door->unique);
        }

        return $invoiceNumber;
    }


    public function getQuoteForGuest($quoteID)
    {

        $db = connect_db();

        $sql = "SELECT * from quotesSales WHERE quotesSales.id=?";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;

    }

    public function getQuoteClean($quoteID)
    {
        $db = connect_db();

        $sql = "SELECT * from quotesSales WHERE quotesSales.id=?";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;
    }


    public function getQuote($quoteID)
    {
        $db = connect_db();

        $sql = "SELECT * from quotesSales
           LEFT JOIN SQ_addresses on quotesSales.id = SQ_addresses.quote_id WHERE quotesSales.id=?";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $quoteID);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;

    }

    public function deleteQuoteWithQuoteIDandSalesPerson($id, $salesPerson)
    {

        $db = connect_db();

        $sql = " update quotesSales set `status`='deleted' where SalesPerson=? and id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $salesPerson, $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Delete Quote '.$sql);
        };

    }

    public function validWood($wood)
    {
        if (empty($wood)) {
            return false;
        }
        if (empty($wood->wood)) {
            return false;
        }
        if (empty($wood->id)) {
            return false;
        }
        $goodWood = array('Fir', 'Alder', 'White Oak', 'Cherry', 'Mahogany', 'Walnut', 'Mixed', 'NA');
        foreach ($goodWood as $testWood) {
            if ($wood->wood == $testWood) {
                return true;
            }
        }

        return false;
    }

    public function woodOrdered($wood)
    {

        if (!$this->validWood($wood)) {
            return false;
        }

        $db = connect_db();

        $sql = " update quotesSales set woodOrdered=? where  id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $wood->wood, $wood->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Set Wood  '.$sql);
        };

        return true;

    }


    public function poBySalesPersonAndId($po, $SalesPerson, $id, $superToken = null)
    {
        $db = connect_db();

        if (empty($superToken)) {
            $sql = "update quotesSales set po=? where  SalesPerson=? and id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("sii", $po, $SalesPerson, $id);

        } else {
            $sql = "update quotesSales set po=? where id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $po, $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed update PO');
        };

        return true;
    }


    public function updateSaleTerms($salesTerms, $SalesPerson, $id, $superToken = null)
    {
        $db = connect_db();

        if (empty($superToken)) {
            $sql = "update quotesSales set salesTerms=? where  SalesPerson=? and id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("sii", $salesTerms, $SalesPerson, $id);

        } else {
            $sql = "update quotesSales set salesTerms=? where id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $salesTerms, $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed update PO');
        };

        return true;
    }

    public function updateHoldStatus($status, $SalesPerson, $id, $superToken = null)
    {
        if (!in_array(strtolower($status), ['hold', 'new'])) {
            throw new Exception('Invalid Status Sent');
        }

        $db = connect_db();

        if (empty($superToken)) {
            $sql = "update quotesSales set status=? where  SalesPerson=? and id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ssi", $status, $SalesPerson, $id);
        } else {
            $sql = "update quotesSales set status=? where id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $status, $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed update Status');
        };

        $this->updateHoldStatusForProduction($status, $SalesPerson, $id, $superToken = null);

        return true;
    }

    /**
     *  Hacked dual purpose status and lead quality for production management
     *  of orders requested by Robert 11/08/2017
     */
    public function updateHoldStatusForProduction($status, $SalesPerson, $id, $superToken = null)
    {

        $db = connect_db();

        if (empty($superToken)) {
            $sql = "update quotesSales set `type`='Ready For Production' where  SalesPerson=? and id=? and type = 'onHold' ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $SalesPerson, $id);
        } else {
            $sql = "update quotesSales set `type`='Ready For Production' where id=? and type = 'onHold' ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("i", $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed update Status for Production');
        };



    }


    public function leadQualityUpdateBySalesPersonAndId($leadQuality, $SalesPerson, $id, $superToken = null)
    {
        if (!in_array(strtolower($leadQuality), $this->validQuality)) {
            throw new Exception('Invalid Lead Quality Sent');
        }

        $db = connect_db();

        if (empty($superToken)) {
            $sql = "update quotesSales set leadQuality=? where  SalesPerson=? and id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ssi", $leadQuality, $SalesPerson, $id);
        } else {
            $sql = "update quotesSales set leadQuality=? where id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $leadQuality, $id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed update Lead Quality');
        };

        return true;
    }


    public function validGlass($glass)
    {
        if (empty($glass)) {
            return false;
        }
        if (empty($glass->glass)) {
            return false;
        }
        if (empty($glass->id)) {
            return false;
        }
        $goodGlass = array('No', 'Yes');
        foreach ($goodGlass as $testGlass) {
            if ($glass->glass == $testGlass) {
                return true;
            }
        }

        return false;
    }


    public function glassOrdered($glass)
    {

        if (!$this->validGlass($glass)) {
            return false;
        }

        $db = connect_db();

        $sql = " update quotesSales set glassOrdered=? where  id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $glass->glass, $glass->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Set Glass  '.$sql);
        };

        return true;

    }

    public function quoteNotes($notes)
    {
        if (empty($notes->id)) {
            throw new Exception('No id passed for quoteNotesBySalesPerson');
        }
        if (empty($notes->SalesPerson)) {
            throw new Exception('No SalesPerson ID passed for quoteNotesBySalesPerson');
        }

        $db = connect_db();

        if (isset($notes->productionNotes)) {
            $productionNotes = strip_tags($notes->productionNotes);
            $sql = " update quotesSales set productionNotes=? where  id=?  ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $productionNotes, $notes->id);
        }

        if (isset($notes->quoteNotes)) {
            $quoteNotes = strip_tags($notes->quoteNotes);
            $sql = " update quotesSales set quoteNotes = ? where  id=?  ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $quoteNotes, $notes->id);
            $notesLog = new FollowupNotesLog();
            $notesLog->insertNotesLog($notes->id, $quoteNotes);
        }

        if(isset($notes->paymentNotes)){
            $paymentNotes = strip_tags($notes->paymentNotes);
            $sql = " update quotesSales set paymentNotes = ? where id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $paymentNotes, $notes->id);
        }

        if(!isset($stmt)){
            return false;
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Update quote and production notes');
        };

        return true;

    }

    public function quoteFollowUps($followUps)
    {
        if (empty($followUps->id)) {
            throw new Exception('No id passed for quoteFollowUps');
        }
        if (empty($followUps->SalesPerson)) {
            throw new Exception('No SalesPerson ID passed for quoteNotesBySalesPerson');
        }

        $followUp = strip_tags($followUps->followUp);
        $secondFollowUp = strip_tags($followUps->secondFollowUp);

        $followUp = humanDateToMysql($followUp);
        $secondFollowUp = humanDateToMysql($secondFollowUp);

        $db = connect_db();

        if (empty($followUps->superToken)) {
            $sql = " update quotesSales set followUp=?, secondFollowUp =? where  id=? and SalesPerson = ? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ssii", $followUp, $secondFollowUp, $followUps->id, $followUps->SalesPerson);
        } else {
            $sql = " update quotesSales set followUp=?, secondFollowUp =? where  id=? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("ssi", $followUp, $secondFollowUp, $followUps->id);
        }

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Update quoteFollowUps');
        };

        return true;

    }


    public function quoteDueDate($dueDate)
    {
        if (empty($dueDate->id)) {
            throw new Exception('No id passed for quoteFollowUps');
        }
        if (empty($dueDate->SalesPerson)) {
            throw new Exception('No SalesPerson ID passed for quoteNotesBySalesPerson');
        }

        $dueDate->dueDate = strip_tags($dueDate->dueDate);

        $dueDate->dueDate = $this->humanDateToMysql($dueDate->dueDate);

        $db = connect_db();

        if (empty($dueDate->superToken)) {
            $sql = " update quotesSales set dueDate=? where  id=? and SalesPerson = ? ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("sii", $dueDate->dueDate, $dueDate->id, $dueDate->SalesPerson);

        } else {
            $sql = " update quotesSales set dueDate=? where  id=?  ";
            $stmt = $db->prepare($sql);
            $stmt->bind_param("si", $dueDate->dueDate, $dueDate->id);
        }


        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Update dueDate');
        };

        return true;

    }

    public function quoteEstCompleteDate($estComplete)
    {
        if (empty($estComplete->id)) {
            throw new Exception('No id passed for estComplete');
        }

        $estComplete->estComplete = strip_tags($estComplete->estComplete);

        $estComplete->estComplete = $this->humanDateToMysql($estComplete->estComplete);

        $db = connect_db();
        $sql = " update quotesSales set estComplete=? where  id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("si", $estComplete->estComplete, $estComplete->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Update estComplete');
        };

        return true;

    }

    public function quotestartedProdDate($startedProd)
    {
        if (empty($startedProd->id)) {
            throw new Exception('No id passed for estComplete');
        }


        $startedProd->startedProd = strip_tags($startedProd->startedProd);

        $startedProd->startedProd = $this->humanDateToMysql($startedProd->startedProd);

        $db = connect_db();
        $sql = " update quotesSales set startedProd=? where  id=?  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("si", $startedProd->startedProd, $startedProd->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Update estComplete');
        };

        return true;

    }

    public function productionNotes($notes)
    {

        if (empty($notes->id)) {
            return false;
        }
        $notes->notes = strip_tags($notes->notes);

        $db = connect_db();

        $sql = " update quotesSales set productionNotes=? where  id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("ss", $notes->notes, $notes->id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Set Glass  '.$sql);
        };

        return true;

    }


    public function deleteGuestQuote($id)
    {

        $db = connect_db();

        $sql = " update quotesGuest set status='deleted' where id=? ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $id);

        if (!$stmt->execute()) {
            throw new Exception('Database failed to set to Delete Guest Quote '.$sql);
        };

    }


    public function getCutSheets($id)
    {

        $db = connect_db();
        $sql = "SELECT cutSheets from quotesSales where id=?  ";
        $db->stmt_init();
        $stmt = $db->prepare($sql);


        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;
    }


    public function getGlassSheets($id)
    {

        $db = connect_db();
        $sql = "SELECT glassSheets from quotesSales where id=?  ";
        $db->stmt_init();
        $stmt = $db->prepare($sql);

        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;
    }

    public function getPanelSlide($door)
    {
        $numberOfPanel = $door->numberOfPanels;
        $numberOfSwings = 1;
        $swings = $door->panelOptions->swingDirection->selected;

        $panelSlide = '';

        if ($swings == 'both') {
            $numberOfSwings = 2;

            $left = $door->panelOptions->movement->numberLeft;

            $right = $numberOfPanel - ($numberOfSwings + $left);

            $panelSlide = "Left $left / Right $right";
        }



        if ($door->panelOptions->movement->selected == 'right') {
            $right = $numberOfPanel - $numberOfSwings;
            $panelSlide = "Left 0 / Right $right";
        }
        if ($door->panelOptions->movement->selected == 'left') {
            $left = $numberOfPanel - $numberOfSwings;
            $panelSlide = "Left $left / Right 0";
        }

        return $panelSlide;
    }

    public function getGlassTraits($glassChoices){
        $traits = [];
        if(is_object($glassChoices)){
            foreach ($glassChoices as $key => $value) {
                if($value === 'yes'){
                    array_push($traits, splitForSecondOption($key));
                }
            }
        }
        return $traits;
    }

    public function buildTraits($door)
    {

        $cleanDoor['Width'] = $door->width.'"';
        $cleanDoor['Height'] = $door->height.'"';
        $cleanDoor['Frame'] = $door->panelOptions->frame->selected;
        $cleanDoor['Track'] = $door->panelOptions->track->selected;

        $cleanDoor['Swing Door(s)'] = $door->panelOptions->swingDirection->selected;
        $cleanDoor['Swing Door Direction'] = $door->panelOptions->swingInOrOut->selected;
        $cleanDoor['Panel Slide'] = $this->getPanelSlide($door);

        $cleanDoor['Track'] = $door->panelOptions->track->selected;
        $cleanDoor['Track'] = $door->panelOptions->track->selected;
        $cleanDoor['Track'] = $door->panelOptions->track->selected;

        $cleanDoor['Glass'] = $this->getGlassTraits($door->glassChoices);
        $cleanDoor['Hardware'] = splitForSecondOption($door->hardwareChoices);
        if (!empty($door->hardwareExtraChoices)){
            if (!empty($door->hardwareExtraChoices->{'extras-Keyed Alike'})) {
               if ($door->hardwareExtraChoices->{'extras-Keyed Alike'}=='yes') {
                    $cleanDoor['Hardware Options'] = "Keyed Alike";
                }
            }
        }

        if (!empty($door->glassOptions)) {
            if (!empty($door->glassOptions->{'glass-Argon'})) {
                if (!empty($door->glassOptions) && $door->glassOptions->{'glass-Argon'}=='yes') {
                    $cleanDoor['Glass Options'] = "Argon";
                }
            }
        }

        $cleanDoor['Number of Panels'] = $door->numberOfPanels;

        if (!empty($door->vinylType)) {
            $cleanDoor['Vinyl'] = $door->vinylType;
        }

        if (!empty($door->extColor->id)){
            $cleanDoor['Exterior Color'] = splitForFourthOption($door->extColor->id);
        }

        if (!empty($door->intColor->id)) {
            $cleanDoor['Interior Color'] = splitForFourthOption($door->intColor->id);
        }

        $traits = $this->makeIntoTraits($cleanDoor);

        return $traits;
    }


    public function makeIntoTraits($doors)
    {
        $traits = array();

        foreach ($doors as $key=>$value) {
            if(is_array($value)){
                foreach ($value as $trait){
                    $trait  = array (
                        "name" => "$key",
                        "trait_value" => $trait
                    );
                    $traits[] = $trait;
                }
            }else{
                $trait  = array (
                    "name" => "$key",
                    "trait_value" => $value
                );
                $traits[] = $trait;
            }

        }

        return $traits;
    }
}


?>
