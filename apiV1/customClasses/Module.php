<?php

require_once 'lib/mysql.php';

class Module
{

    private $total;
    public $options = Array();
    public $selected;
    public $db;
    public $moduleTables;
    public $subModuleTables;
    public $sqlFile;

    public function __construct($db = null)
    {

        $this->sqlFile = 'my.sql';

        $this->db = connect_db($db);
        $this->moduleTables = [
            'module',
            'optionsFinishTypes',
            'optionsForExtendedButton',
            'SQ_modules',
            'SQ_modules_colors'
        ];

        $this->subModuleTables = [
            'groupNames',
            'optionsGlass',
            'optionsFixed',
            'optionsGrouped',
            'optionsPercentage',
            'optionsSized',
            'optionsForHardware'
        ];

//
//        $this->moduleTables = [
//            'SQ_modules'
//        ];
//
//        $this->subModuleTables = [
//        ];

    }


    public function getModuleInfo($id)
    {
        $db = connect_db();

        $sql = "SELECT * from module, SQ_modules where moduleID = module_id and module_id= ?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $results = $stmt->get_result();
        $moduleInfo = $results->fetch_assoc();

        if(empty($moduleInfo)){
            return false;
        }

        $moduleInfo['messageWidth'] = str_replace('$maxWidth', $moduleInfo['maxWidth'], $moduleInfo['messageWidth']);
        $moduleInfo['messageWidth'] = str_replace('$minWidth', $moduleInfo['minWidth'], $moduleInfo['messageWidth']);
        $moduleInfo['messageHeight'] = str_replace('$minHeight', $moduleInfo['minHeight'], $moduleInfo['messageHeight']);
        $moduleInfo['messageHeight'] = str_replace('$maxHeight', $moduleInfo['maxHeight'], $moduleInfo['messageHeight']);
        $moduleInfo['description'] = str_replace('$minHeight', $moduleInfo['minHeight'], $moduleInfo['description']);
        $moduleInfo['description'] = str_replace('$maxHeight', $moduleInfo['maxHeight'], $moduleInfo['description']);
        $moduleInfo['description'] = str_replace('$maxWidth', $moduleInfo['maxWidth'], $moduleInfo['description']);
        $moduleInfo['description'] = str_replace('$minWidth', $moduleInfo['minWidth'], $moduleInfo['description']);


        $options = new UIOptions();
        $results = $options->getOptionsByGroup($moduleInfo['optionGroup']);
        $moduleInfo['panelOptions'] = $results;

        $nav = new UINav();
        $results = $nav->getNavByGroup($moduleInfo['navGroup']);
        $moduleInfo['nav'] = $results;

        return $moduleInfo;
    }

    public function getModuleByID($id)
    {

        if (!is_numeric($id)) {
            throw new Exception("getMOduleINfoID wasn't passed an ID!!!");
        }

        $db = connect_db();
        $sql = "SELECT * from module where module_id = ? limit 1";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $options = $results->fetch_assoc();

        return $options;
    }


    public function getColorGroupsByID($id)
    {

        if (!is_numeric($id)) {
            throw new Exception("getColorGroupsByID wasn't passed an ID!!!");
        }

        $db = connect_db();
        $sql = "SELECT * from SQ_modules_colors where moduleID = ? ORDER BY location,myOrder";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();

        $data = array();
        while ($options = $results->fetch_assoc()) {
            ;
            unset($options['moduleID']);
            unset($options['myOrder']);
            $data[] = $options;

        }
        return $data;
    }


    public function getTableData($table, $module, $replaceModule)
    {
        $sql = "SELECT * from $table where module_id =? ";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param("i", $module);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            unset($options['id']);
            $options['module_id'] = $replaceModule;
            $data[] = $options;
        }
        return $data;
    }


    public function getSQTableData($table, $module, $replaceModule)
    {
        $sql = "SELECT * from $table where moduleID =? ";
        $stmt = $this->db->prepare($sql);
        $stmt->bind_param("i", $module);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            unset($options['id']);
            $options['moduleID'] = $replaceModule;
            $data[] = $options;
        }
        return $data;
    }


    public function completeGroups($value)
    {
        $value = rtrim($value, ',');
        $value = $value . ")";
        return $value;
    }


    public function buildInsert($row, $table)
    {

        $keyGroup = '(';
        $valueGroup = '(';
        $valueArray = '';
        $valueEscape = '(';

        foreach ($row as $key => $value) {
            $safeValue = $this->db->real_escape_string($value);
            $keyGroup = $keyGroup . "`$key`,";
            $valueGroup = $valueGroup . "'$safeValue',";
        }


        $keyGroup = $this->completeGroups($keyGroup);
        $valueGroup = $this->completeGroups($valueGroup);

        $sql = "INSERT INTO $table $keyGroup VALUES $valueGroup";
        return ($sql);

    }

    public function insertRow($sql)
    {

        if ($this->db->query($sql) === TRUE) {
            return true;
        } else {
            throw new Exception($this->db->error);
        }

    }

    public function getFreeModuleNumber()
    {

        $sql = "SELECT * from module  where module_id < 900 order by module_id DESC limit 1";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        $option = $results->fetch_assoc();

        $newModuleID = $option['module_id'] + 1;
        return ($newModuleID);

    }

    public function copyModuleFromTable($table, $module, $newModuleID)
    {

        $rowsOfModule ='';
        if ($table == 'SQ_modules' or $table == 'SQ_modules_colors' ) {
            $rowsOfModule = $this->getSQTableData($table, $module, $newModuleID);
        } else {
            $rowsOfModule = $this->getTableData($table, $module, $newModuleID);
        }

        foreach ($rowsOfModule as $row) {
            $sql = $this->buildInsert($row, $table);

            $this->addToFile($sql);
//            $this->insertRow($sql);
        }

    }


    public function deleteModuleFromTable($table, $module)
    {

        $db = connect_db();

        $sql = "delete from $table where module_id = ?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $module);

        if (!$stmt->execute()) {
            return true;
        } else {
            throw new Exception($this->db->error);
        }


    }

    public function updateSubModule($module)
    {
        $sql = "INSERT INTO `submodule` (`module_id`, `submodule`, `translatedModule`)VALUES ($module, 1, $module)";
        $this->addToFile($sql);

        $sql = "INSERT INTO `submodule` (`module_id`, `submodule`, `translatedModule`)VALUES ($module, 3, $module)";
        $this->addToFile($sql);
    }


    public function updateModuleToName($module, $name)
    {
        $sql = "UPDATE module set id=$module, name='$name' where module_id = '$module'";
        $this->addToFile($sql);

        $sql = "UPDATE SQ_modules set title='$name' where moduleID = '$module'";
        $this->addToFile($sql);

//    if ($this->db->query($sql) === TRUE) {
//            return true;
//        } else {
//            throw new Exception($this->db->error);
//        }

    }

    public function copyModuleToNewName($module, $name)
    {

        file_put_contents($this->sqlFile, '');
        $newModuleID = $this->getFreeModuleNumber();
        
        foreach ($this->moduleTables as $table) {
            $this->copyModuleFromTable($table, $module, $newModuleID);
        }

        $this->updateModuleToName($newModuleID, $name);
        $this->updateSubModule($newModuleID);

        if ($module == 1) {
            $module = 5;
        }
        foreach ($this->subModuleTables as $table) {
            $this->copyModuleFromTable($table, $module, $newModuleID);
        }

        return ($newModuleID);
    }

    public function deleteModule($module)
    {
        foreach ($this->moduleTables as $table) {
            $this->deleteModuleFromTable($table, $module);
        }

    }

    public function addToFile($sql){

        $sql = $sql. ";\n";
        file_put_contents($this->sqlFile, $sql, FILE_APPEND);
    }
}


?>
