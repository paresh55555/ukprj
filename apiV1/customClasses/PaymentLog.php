<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class PaymentLog
{

    public $discounts = array();
    protected $logId;


    public function startPayment($post)
    {
        $db = connect_db();

        if (!empty($post->ccNumber)) {
            $post->ccNumber = substr($post->ccNumber, -4);
        }

        $string = json_encode($post);

        $sql = "INSERT SQ_payment_log SET `started`=now(), postedItems=? ";
        $stmt = $db->prepare($sql);


        $stmt->bind_param("s",$string);

        if (!$stmt->execute()) {
            throw new Exception('Failed to insert start of payment');
        };

        $this->logId = $db->insert_id;


        return $this->logId;
    }


    public function updateResponse ($response)
    {

        $db = connect_db();

        $sql = "UPDATE SQ_payment_log SET `response`= ? where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("si",$response, $this->logId);

        if (!$stmt->execute()) {
            throw new Exception('Failed to update response');
        };

        return true;
    }

    public function updateTResponse ($response)
    {

        $db = connect_db();

        $sql = "UPDATE SQ_payment_log SET `tResponse`= ? where id=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("si",$response, $this->logId);

        if (!$stmt->execute()) {
            throw new Exception('Failed to update response');
        };

        return true;
    }

}


?>
