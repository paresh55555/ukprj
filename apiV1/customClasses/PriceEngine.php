<?php

require_once 'lib/mysql.php';
require_once 'SizeOption.php';
require_once 'SizeOptionSet.php';
require_once 'BuilderSet.php';
require_once 'BuilderGroup.php';
require_once 'GroupOption.php';

require_once 'PerItemOptionSet.php';
require_once 'TranslatedGroupOptions.php';
require_once 'lib/functions.php';
require_once 'Door.php';

class PriceEngine
{
    protected $name;
    protected $formula;
    public $cut_sizes;
    public $width;
    public $height;

    public $modWidth;
    public $modHeight;

    public $panels;
    public $colorOption;
    public $glassChoice;
    public $glassOptions;
    public $doorOptions;
    public $doorDetails;
    public $kickPlate;
    public $swingDoor;
    public $flushBottom;
    public $hardware;
    public $hardwareExtras;
    public $screenUS;
    public $installOptions;
    public $vinylType;
    public $module;
    public $coreModule;
    public $subModule;
    public $moduleName;
    public $panelsRight;
    public $finishes;
    public $defaultTotal;
    public $maxDiscount;
    public $discount;
    public $exemptAmount;
    public $swingDirection;

    public $coreItemsSized;
    public $coreItemsFixed;
    public $corePercentage;
    public $groupedItems = Array();
    public $optionalItems;

    public $debug;
    public $totalPrice;
    public $frame;

    public $costSheet = Array();
    public $panelMovementRight;
    public $panelWidth;

    public $sill;
    public $sillDetails;
    public $addOns;

    protected $tickle;
    protected $screen;
    protected $door;
    protected $debugOutput;




    public function __construct()
    {
        $this->door = new Door();
    }


    public function getDebugOutput()
    {
        return $this->debugOutput;
    }

    protected function addDebugOutput($key, $data) {
        if (!empty($key)) {
            $this->debugOutput[$key] = $data;
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function talk()
    {
        return "Hello world!";
    }

    public function setDefaultDoorDetails()
    {
        $this->doorDetails = ["Operation" => '', "Interior Finish" => '', "Exterior Finish" => '',
            "Frame" => '', "Track" => '', "Handles" => ''];
        return true;

    }

    public function setDoorDetails($operation, $intFinish, $extFinish, $frame, $track, $handles)
    {
        $this->doorDetails = ["Operation" => $operation, "Interior Finish" => $intFinish, "Exterior Finish" => $extFinish,
            "Frame" => $frame, "Track" => $track, "Handles" => $handles];
        return true;

    }

    public function updateNumberOfSwingsForFixedCoreItems($swingNumber)
    {
        $updatedItems = [];
        if ($swingNumber > 1) {
            foreach ($this->coreItemsFixed->options as &$item) {
                $item->swings = $swingNumber;
                $updatedItems[] = $item;
            }
            $this->coreItemsFixed->options = $updatedItems;
        }
    }

    public function updateNumberOfSwingsForSizedCoreItems($swingNumber)
    {
        $updatedItems = [];
        if ($swingNumber > 1) {
            foreach ($this->coreItemsSized->options as &$item) {
                $item->swings = $swingNumber;
                $updatedItems[] = $item;
            }
            $this->coreItemsSized->options = $updatedItems;
        }
    }


    public function updateNumberOfSwingsForGlass($swingNumber)
    {
//        $updatedItems = [];
//        if ($swingNumber > 1) {
//            foreach ($this->coreItemsSized->options as &$item) {
//                $item->swings = $swingNumber;
//                $updatedItems[] = $item;
//            }
//            $this->coreItemsSized->options = $updatedItems;
//        }
    }


    public function updateNumberOfSwingsForGroupedOptions($swingNumber)
    {

        if (!$this->groupedItems) {
            return;
        }
        if ($swingNumber < 2) {
            return;
        }


        foreach ($this->groupedItems as &$item) {

            $item->setSwings($swingNumber);

        }

//    $selected = $this->groupedItems[10]->selected;
//    $this->groupedItems[10]->options[$selected]->options[0]->swings = $swingNumber;

    }

    public function setDoorOptions($swing, $slideRight)
    {
        $numberLeft = 0;
        $numberRight = 0;
        $numberSwings = 1;
        if ($swing == "left") {
            $numberLeft = $this->panels - 1;
        }
        if ($swing == "right") {
            $numberRight = $this->panels - 1;
        }
        if ($swing == "both") {
            $numberRight = $slideRight;
            $numberLeft = $this->panels - $numberRight - 2;
            $numberSwings = 2;

            if ($numberLeft < 0) {
                throw new Exception("SetDoorOptions from PriceEnginge was pass $slideRight that doesn't match number of panels");
                return false;
            }
        }
        $this->doorOptions = ["Swing Door" => $swing, "sliding left" => $numberLeft, "sliding right" => $numberRight, "Number of Swings" => $numberSwings];

        return true;
    }

    public function getOptions()
    {

        $db = connect_db();
        $sql = "SELECT * from price_options  where active = 1 and option_type = 0  order by myOrder";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();

        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[] = $options;
        }
        return $data;
    }

    public function buildDefaultTotal()
    {
//
//        $this->defaultTotal = 0;
//        $this->getTotalPrice();
//
//        $this->defaultTotal = $this->totalPrice;
//        $this->totalPrice = 0;
//        return ;


        $moduleOptions = $this->getModuleInfoID($this->module);

        $options = json_decode($moduleOptions['defaultOptions']);
        if (json_last_error()) {
            print_r($moduleOptions['defaultOptions'] . "\n");
            print_r("####################Broken JSON###############\n");
            exit();
        }

        $options->width = $this->width;
        $options->height = $this->height;
        $options->panels = $this->panels;
        $options->module = $this->coreModule;
        $options->subModules = $moduleOptions['subModules'];

        $newPriceEngine = new PriceEngine();
//        $newPriceEngine->subModule = $this->subModule;

        $newPriceEngine->buildDoorWithOptions(json_encode($options));
        $newPriceEngine->getTotalPrice(true);
        $this->defaultTotal = $newPriceEngine->totalPrice;

        if ($this->debug) {
            echo "Default Total: " . $this->defaultTotal . "\n";
        }

    }


    public function getModuleInfoID($id)
    {

        if ($this->debug) {
            echo "getModuleInfoID: Module=" . $id . "\n";
        }

        if ($id < 1) {
            throw new Exception("getMOduleINfoID wasn't passed an ID!!!");
        }

        $db = connect_db();
        $sql = "SELECT * from module where module_id = ? limit 1";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $id);
        $stmt->execute();
        /* Fetch result to array */
        $results = $stmt->get_result();
        $options = $results->fetch_assoc();

        return $options;
    }


    public function buildCorePercentage()
    {

        $moduleInfo = $this->getModuleInfoID($this->module);
        $this->corePercentage = '';

        $builder = new BuilderPercentage();
        if ($moduleInfo['id']) {
            $this->corePercentage = $builder->getPercentageObjectByModuleAndOption($moduleInfo['id'], 0);
        }


    }


    public function buildCoreItemsSized()
    {

        if ($this->debug) {
            echo "At Build Core: Module=" . $this->module . "\n";
        }

        $moduleInfo = $this->getModuleInfoID($this->module);
        $this->coreItemsSized = '';
        $buildCoreItems = new BuilderSet();

        $data = $buildCoreItems->getSizeSetDataByModuleAndOption($moduleInfo, 0);

        $coreSet = new SizeOptionsSet();
        $coreSet->buildSetFromArrayOfJSON($data);
        $this->coreItemsSized = $coreSet;

    }

    public function buildCoreItemsFixed()
    {

        $this->coreItemsFixed = '';
        $buildCoreItems = new BuilderSet();
        $data = $buildCoreItems->getPerItemSetDataByModuleAndOption($this->module, 0);
        $coreSet = new PerItemOptionSet();
        $coreSet->buildSetFromArrayOfJSON($data);

        $this->coreItemsFixed = $coreSet;


    }

    public function buildGroupedItems()
    {
        $group = new BuilderGroup();
        $groupIDS = $group->getGroupIDSForModule($this->module);

        if ($this->debug) {
//            echo "@@@@@@@@@@@@@@@@@@\n";
//            echo "Module: ".$this->module."\n";
//            print_r($groupIDS);
        }
        $translate = new translatedGroupOptions();
        $translate->buildTranslatedOptionsByModule($this->module);

        $moduleInfo = $this->getModuleInfoID($this->module);



        foreach ($groupIDS as $key => &$id) {

            $data = $group->getSizeGroupDataByModuleAndGroup($this->module, $id['group_id']);



            $groupOptions = new GroupOption($this->debug);

            $name = $translate->idToName($id['group_id']);


            $groupOptions->buildGroupWithDataAndModuleInfo($data, $moduleInfo);

            if ($this->debug) {
                echo "Name: $name \n";
                echo "Key: $key \n";
                if ($name == "Keyed Alike" or $name == "Hardware") {
//                    print_r($groupOptions);
                }
            }


            $this->groupedItems[$name] = $groupOptions;

//            if ($name = 'glassChoice') {
//                $this->addDebugOutput('glassChoiceBuild', $this->groupedItems);
//            }

        }

        if ($this->debug) {
//            print_r($this->groupedItems['finishMaterialExt']);

        }

    }

    public function buildOptions()
    {

        if ($this->debug) {
            echo "######### module: " . $this->module . "\n";

        }

        $this->buildCoreItemsSized();
        $this->buildCoreItemsFixed();
        $this->buildGroupedItems();
        $this->buildCorePercentage();

    }


    public function getSubmodule($subModule)
    {
        $db = connect_db();
        $sql = "SELECT translatedModule from submodule  where submodule=" . $subModule . " and module_id=" . $this->coreModule;
        $result = mysqli_query($db, $sql);

        if(!$result){
            return false;
        }

        $options = mysqli_fetch_array($result, MYSQLI_NUM);

        return ($options[0]);
    }

    public function getNumberOfPanelsByGroup($group)
    {
        $this->panels = '';

        $db = connect_db();
        $sql = "SELECT panels from window_ranges  where myGroup = ? and low <= $this->width and $this->width <= high order by panels ";

        $result = mysqli_query($db, $sql);

        while ($options = mysqli_fetch_array($result, MYSQLI_NUM)) {
            $this->panels[] = $options[0];
        }

        return true;
    }


    public function getNumberOfPanels()
    {
        $this->panels = '';

        $db = connect_db();
        $sql = "SELECT panels from window_ranges  where low <= $this->width and $this->width <= high order by panels ";

        $result = mysqli_query($db, $sql);

        if(!$result){
            return false;
        }

        while ($options = mysqli_fetch_array($result, MYSQLI_NUM)) {
            $this->panels[] = $options[0];
        }

        return true;
    }


    public function getPricesPanels($width, $height, $panels)
    {
        $this->width = $width;
        $this->height = $height;
        $this->panels = $panels;

        $price = $this->getTotalPrice();
    }


    public function addTotalOfCoreItemsSized()
    {

        if ($this->coreItemsSized) {
            $this->totalPrice += $this->coreItemsSized->getTotal($this->modWidth, $this->modHeight, $this->panels);
        }

        if ($this->debug) {
            echo "CoreSized Total: " . $this->totalPrice . "\n";
        }

    }

    public function addTotalOfCoreItemsFixed()
    {


        if ($this->coreItemsFixed) {
            $this->totalPrice += $this->coreItemsFixed->getTotal($this->modWidth, $this->modHeight, $this->panels);
        }

        if ($this->debug) {
            echo "CoreFixedItems Total: " . $this->totalPrice . "\n";
        }

    }

    public function addTotalOfGroupedItems()
    {
        if ($this->debug) {
            echo "###### Grouped Items #######\n";
        }

        foreach ($this->groupedItems as $key => &$grouped) {

            $total = $grouped->getTotal($this->modWidth, $this->modHeight, $this->panels, $this->defaultTotal);
            $this->totalPrice += $total;

            if (isset($grouped->totalExempt)) {
                $this->exemptAmount += $grouped->totalExempt;
            }

            if ($this->debug) {

                $selected = $grouped->selected;

                echo "###############################!!!!!!!Name: $key \n";
                if (!empty($grouped->options[$selected])) {
                    echo "Selected: $selected --> \n". print_r($grouped->options[$selected], true);
                }

                echo "Total: $total \n\n";
            }
        }


        if ($this->debug) {
            echo "###### Grouped Items End #######\n";
        }

    }

    public function getTotalPrice($default = false)
    {


        $moduleInfo = $this->getModuleInfoID($this->module);

        if ($this->debug) {
            echo "Module Unit Type: " . $moduleInfo['unitType'] . "\n";
        }

        if ($moduleInfo['unitType'] == 'mm') {
            $width = $this->width * 25.4;
            $height = $this->height * 25.4;
            $this->modWidth = $width;
            $this->modHeight = $height;

        } else {
            $width = $this->width;
            $height = $this->height;
            $this->modWidth = $width;
            $this->modHeight = $height;
        }

        $this->totalPrice = 0;
        if (!isset($width) || !isset($height) || !isset ($this->panels)) {

            $this->totalPrice = "Error w-$width h-$height p-{$this->panels}";
            return ($this->totalPrice);
        }

        $this->updateNumberOfSwingsForFixedCoreItems($this->doorOptions['Number of Swings']);
        $this->updateNumberOfSwingsForGroupedOptions($this->doorOptions['Number of Swings']);
        $this->updateNumberOfSwingsForSizedCoreItems($this->doorOptions['Number of Swings']);

        $this->addTotalOfCoreItemsSized();
        $this->addTotalOfCoreItemsFixed();

        $this->addTotalOfGroupedItems();

        if (!$default) {
            $this->addProfitMargins();
        }

//        setlocale(LC_MONETARY, 'en_US');
        $this->totalPrice = number_format( $this->totalPrice, 2, ".", "" );

        return ($this->totalPrice);

    }

    public function addProfitMargins()
    {
        $total = $this->totalPrice - $this->exemptAmount;

        if ($this->debug) {
            echo "Current Total: $this->totalPrice \n";
            echo "ExemptAmount: $this->exemptAmount \n";
            echo "Percentage Amount: $total \n";
        }

        if (isset($this->corePercentage[0])) {
            foreach ($this->corePercentage as $globalPercentageItem) {
                if ($this->debug) {
                    print_r($globalPercentageItem);
                }

                $globalPercentageItem->getTotal($total);
                $this->totalPrice = $this->totalPrice + $globalPercentageItem->totalPrice;
            }
        }

        if ($this->debug) {
            echo "Amount after percentages: $this->totalPrice \n";
        }
    }

    public function logit($label, $data)
    {

        echo "\n######" . $label . ": " . $data . "\n";

    }

    public function displayBreakDown()
    {

        if ($this->coreItemsSized) {
            $this->coreItemsSized->displayBreakDown();
        }

        if ($this->coreItemsFixed) {
            $this->coreItemsFixed->displayBreakDown();
        }

        foreach ($this->groupedItems as &$grouped) {
            $grouped->displayBreakDown();
        }


    }


    public function setObjectFromJSON($json)
    {


        $priceValues = json_decode($json);

        if (json_last_error()) {
//            echo "We are here";
            return false;
        } else {
            $this->name = $priceValues->{"name"};
            $this->width = $priceValues->{'width'};
            $this->height = $priceValues->{'height'};
            $this->formula = $priceValues->{'formula'};

            $formula = $this->formula;
            eval('$formula = ' . $formula . ';');
            $this->cut_sizes = $formula;
//            $formula.'<br>';  // this returns: 400
//            echo "Width".$width;

            return true;
        }
    }


    public function buildWindowOptionsAndGetTotal($json)
    {
        $this->totalPrice = 300;
        $this->buildDoorWithOptions($json);
//
//        $this->buildDefaultTotal();
        $this->getTotalPrice();
//
//        $this->getPanelWidth();
    }


    public function buildDoorWithOptionsGetDefaultBuildTotal($json)
    {

        $this->buildDoorWithOptions($json);
        $this->buildDefaultTotal();
        $this->getTotalPrice();
        $this->getPanelWidth();
    }

    public function getPanelWidth()
    {
        $options = $this->coreItemsSized->options;
        foreach ($options as $option) {
            if (strtolower($option->nameOfOption) == 'panel width') {
                $this->panelWidth = $option->cut_size;
                break;
            }
        }
    }


    public function getSubModuleBaseOnFrameAndTrack($frame, $track)
    {
        $subModule = 1;
        if ($frame === 'Aluminum Block') {
            $subModule = 3;
        } else {
            if ($track === 'Flush') {
                $subModule = 2;
            }
        }

        if ($this->debug) {
            echo "Calculating SubModule \n";
            echo "Frame: $frame \n";
            echo "Track: $track \n";
            echo "Final SubModule: $subModule \n";

        }
        return $subModule;


    }

    public function translateModuleBaseOnFrameAndTrack()
    {

        if ($this->debug) {
            echo "Sub Module Pre: $this->subModule  \n";
        }

        if ($this->subModule) {
            return $this->subModule;
        }

        $subModule = $this->getSubModuleBaseOnFrameAndTrack($this->frame, $this->track);
        if ($this->debug) {
            echo "Sub Module Post: $subModule  \n";
        }

        $returnModule = $this->getSubmodule($subModule);

        if (empty($returnModule)) {
            $returnModule = $this->module;
        }

        $this->subModule = $returnModule;
        return $returnModule;
    }

    protected function setDoorOptionsFromJSON($json)
    {
        $windowOptions = json_decode($json);
        $options = $this->door->getOptions();

        foreach ($options as $option) {
            if (isset($windowOptions->{$option})) {
                $this->{$option} = $windowOptions->{$option};
            } else {
                $this->{$option} = null;
                if ($option == 'glassOptions') {
                    $this->{$option} = [];
                }
            }
        }
    }


    public function setSill()
    {
        if (!empty($this->sill->selected)) {
            $this->groupedItems['sill']->selectOption($this->sill->selected);
            $this->groupedItems["sill"]->customHeight = $this->sill->height;
            $this->groupedItems["sill"]->customWidth = $this->sill->width;
            $this->groupedItems["sill"]->customDepth = $this->sill->depth;
            if ($this->debug) {
                print_r($this->sill);
            }
        }
    }


    public function setTrickle()
    {

        if ($this->debug) {
            print_r($this->trickle);
        }


        if (!empty($this->trickle->type)) {
            $this->groupedItems['Trickle']->selectOption($this->trickle->type);
            if ($this->debug) {
                print_r($this->trickle->type);
            }
        }
    }

    public function setBayPole()
    {

        if ($this->debug) {
            print_r($this->bayPole);
        }

        if (!empty($this->bayPole->type)) {
            $this->groupedItems['BayPole']->selectOption($this->bayPole->type);
            if ($this->debug) {
                print_r($this->bayPole->type);
            }
        }
    }


    public function setScreen()
    {

        if ($this->debug) {
            print_r($this->screen);
        }

        if (!empty($this->screen->type)) {
            if (!empty($this->groupedItems['screen'])) {
                $this->groupedItems['screen']->selectOption($this->screen->type);
            }
            if ($this->debug) {
                print_r($this->screen->type);
            }
        }
    }

    public function setAddOns()
    {

        if (!empty($this->addOns)) {
            if ($this->debug) {
                echo "AddOns \n";
                print_r($this->addOns);
            }

            foreach ($this->addOns as $key => &$addOn) {

                if ($this->debug) {
                    echo "@@@@@@@@@@@@@@@@@@@@@@@\n";
                    echo "$key -->" . $this->addOns->{$key}->selected . "\n";
                }
                if ($this->addOns->{$key}->selected == 'yes') {

                    if(strpos($key, 'leftExtender') !== false){

                        if ( isset($this->groupedItems['leftExtender']) ) {
						    $this->groupedItems['leftExtender']->selectOption($key);
						}
                    }
                    if(strpos($key, 'rightExtender') !== false){
                        if ( isset($this->groupedItems['rightExtender']) ) {
						    $this->groupedItems['rightExtender']->selectOption($key);
						}
                    }
                    if(strpos($key, 'topExtender') !== false){
                        if ( isset($this->groupedItems['topExtender']) ) {
						    $this->groupedItems['topExtender']->selectOption($key);
						}
                    }
                }
                if ($this->debug) {


//                    $selected = $this->groupedItems[$key]->selected;
//                    echo "Name: $key \n";
//                    echo "First Selected: $selected \n";
                }
            }
        }
    }


    public function buildDoorWithOptions($json)
    {
        $this->setDoorOptionsFromJSON($json);
        $windowOptions = json_decode($json);

        if (isset($windowOptions->module)) {
            $this->coreModule = $windowOptions->module;
        }

        if ($this->debug) {
            echo "Module Start: $this->module \n";
            echo "Core Module  Start: $this->coreModule \n";
        }

        if (isset($windowOptions->subModules)) {
            if ($windowOptions->subModules == 0) {
                $this->module = $this->coreModule;
            } else {
                $this->module = $this->translateModuleBaseOnFrameAndTrack();
            }
        } else {
            $this->module = $this->translateModuleBaseOnFrameAndTrack();
        }

        if ($this->debug) {
            echo "Module Finish: $this->module \n";
        }

        $this->buildOptions();
        $this->setDefaultDoorDetails();

        $translate = new translatedGroupOptions();
        $translate->buildTranslatedOptionsByModule($this->module);

        $this->setAddOns();
        $this->setSill();
        $this->setTrickle();
        $this->setBayPole();
        $this->setScreen();

        if ($this->track) {
            $this->doorDetails['Track'] = $this->track;
            if (!empty($this->groupedItems["Window"])) {
                $this->groupedItems["Window"]->selectOption($this->track);
            }
        }
        if ($this->swingDirection) {
            $this->doorDetails['Operation'] = $this->swingDirection;
        }
        if ($this->installOptions) {
            $this->groupedItems["installOptions"]->selectOption($this->installOptions);
        }

        if ($this->hardware) {
            $this->groupedItems["Hardware"]->selectOption($this->hardware);
            $this->doorDetails['Handles'] = $this->hardware;
        }

        if (!empty($this->screenUS) and !empty($this->groupedItems["screenUS"])) {
            $this->groupedItems["screenUS"]->selectOption($this->screenUS);
            $this->doorDetails['Screens'] = $this->screenUS;
        }

        if ($this->debug) {
            echo "*****key alike ****\n";
            print_r($this->hardwareExtras . "\n");
        }

        if (!empty($this->hardwareExtras)) {
//            print_r($this->hardwareExtras);
            
            if (preg_match('/.*Keyed Alike.*/', $this->hardwareExtras)) {
                if (!empty($this->groupedItems["Keyed Alike"])) {
                    $this->groupedItems["Keyed Alike"]->selectOption("Keyed Alike");
                }
            }
            if (preg_match('/.*Door Restrictor.*/', $this->hardwareExtras)) {
                if (!empty($this->groupedItems["Door Restrictor"])) {
                    $this->groupedItems["Door Restrictor"]->selectOption("Door Restrictor");
                }
            }
            if (preg_match('/.*Screens.*/', $this->hardwareExtras)) {
                if (!empty($this->groupedItems["Screens"])) {
                    $this->groupedItems["Screens"]->selectOption("Screens");
                }
            }
            if (preg_match('/.*Ultion 3.*/', $this->hardwareExtras)) {
                 if (!empty($this->groupedItems["Ultion 3"])) {
                     $this->groupedItems["Ultion 3"]->selectOption("Ultion 3");
                 }
            }
        }

        if (!empty($this->hardwareExtras[0])) {
            if (!empty($this->hardwareExtras[0]->{'Keyed Alike'})) {
                if ($this->hardwareExtras[0]->{'Keyed Alike'}) {
                    $this->groupedItems["Keyed Alike"]->selectOption("Keyed Alike");
                    if ($this->debug) {
                        echo "Keyed Alike has been added \n";
                    }
                }
            }
        }

        if (!empty($this->hardwareExtras[0])) {
            if ($this->hardwareExtras[0] == "Keyed Alike") {
                if (!empty($this->groupedItems["Keyed Alike"])) {
                    $this->groupedItems["Keyed Alike"]->selectOption("Keyed Alike");
                    if ($this->debug) {
                        echo "Keyed Alike has been added \n";
                    }
                }
            }
        }

        if ($this->debug) {
            echo "*****key alike End ****\n";
            print_r($this->hardwareExtras . "\n");
        }


        if (!empty($this->glassChoice)) {
            if (is_array($this->glassChoice)) {
                $this->groupedItems["glassChoice"]->selectOption($this->glassChoice[0]);
            } else {
                $this->groupedItems["glassChoice"]->selectOption($this->glassChoice);
            }
        }

        foreach ($this->glassOptions as &$glassOption) {

            if ($this->debug) {
                echo "Glass Option: $glassOption \n";
            }

            if ($glassOption != 'undefined') {
                if(strpos($glassOption, 'Blinds') !== false){
                    if (!empty($this->groupedItems['Blinds']) ) {
                        $this->groupedItems['Blinds']->selectOption($glassOption);
                    }
                }else if(strpos($glassOption, 'Obscure') !== false){
                     if (!empty($this->groupedItems['Obscure']) ) {
                         $this->groupedItems['Obscure']->selectOption($glassOption);
                     }
                }else
                    if (!empty($this->groupedItems['Grids']) ) {
                        $this->groupedItems['Grids']->selectOption($glassOption);
                    }

//                if (!empty($this->groupedItems[$glassOption])) {
//                    $this->groupedItems[$glassOption]->selectOption($glassOption);
//                }
            }
        }


        if (is_numeric($this->swingDoor)) {
            $this->doorOptions['Number of Swings'] = intval($this->swingDoor);
        }

        if ($this->swingDoor == 'both') {
            if (!empty($this->groupedItems['Extra Swing Door Labour'])) {
                $this->groupedItems['Extra Swing Door Labour']->selectOption("Extra Swing Door Labour");
            }
            $this->doorOptions['Number of Swings'] = 2;
            $this->setDoorOptions("both", $this->panelsRight);
        }

        if (isset($this->finishes->finishOptions)) {
            $this->selectFinishOptions();
        }

        if (isset($this->finishes->vinylColor)) {
            if (isset($this->groupedItems["vinylColor"])) {
                $this->groupedItems["vinylColor"]->selectOption($this->finishes->vinylColor);
            }
        }
    }

    public function createMaterialOption($option)
    {

        if ($option != 'woodStandardOneSide' and $option != 'woodPremiumOneSide') {
            $option = 'aluminumOneSide';
        }
        return $option;
    }

    public function selectMaterialOptionWithExtOrInt($option, $extOrInt)
    {

        if ($this->debug) {

            echo "\nselectMaterialOptionWith: $extOrInt--> $option \n\n";
        }

        $newOption = $option;

        if ($extOrInt == 'int') {
            if (isset ($this->groupedItems["finishMaterialInt"])) {
                $this->groupedItems["finishMaterialInt"]->selectOption($newOption);
            }
        }
        if ($extOrInt == 'ext') {
            if (isset ($this->groupedItems["finishMaterialExt"])) {
                $this->groupedItems["finishMaterialExt"]->selectOption($newOption);
            }
        }
//        }
    }

    public function selectMaterialIntOption($option)
    {
        $this->selectMaterialOptionWithExtOrInt($option, "int");
    }

    public function selectMaterialExtOption($option)
    {
        $this->selectMaterialOptionWithExtOrInt($option, "ext");
    }

    public function decodeFinishedTypeSpecial()
    {

        if (empty($this->finishes->int) || empty($this->finishes->ext)) {
            return "AluminumBothSides";
        }

        if ($this->finishes->int->type == "woodStandard" && $this->finishes->ext->type == "woodStandard") {
            return "SW_SW";
        }
        if ($this->finishes->int->type == "woodPremium" && $this->finishes->ext->type == "woodPremium") {
            return "PW_PW";
        }
        if ($this->finishes->int->type == "woodStandard" && $this->finishes->ext->type == "woodPremium") {
            return "SW_PW";
        }
        if ($this->finishes->int->type == "woodPremium" && $this->finishes->ext->type == "woodStandard") {
            return "SW_PW";
        }

        if ($this->finishes->int->type == "woodStandard") {
            return "SW_Aluminum";
        }
        if ($this->finishes->int->type == "woodPremium") {
            return "PW_Aluminum";
        }

        return "AluminumBothSides";


    }

    public function selectFinishOptionsSpecial()
    {

        $option = $this->decodeFinishedTypeSpecial();

        $this->groupedItems["finishMaterialExt"]->selectOption($option);

        if ($this->debug) {
            echo "FinishMaterialExt: " . $option . "\n";
            echo "Options Selected: " . $this->groupedItems["finishMaterialExt"]->selected . "\n";
        }

    }

    public function selectFinishOptions()
    {

        if (!is_object($this->finishes->ext)) {
            return;
        }

        if (!isset($this->groupedItems["finishOptionsExt"])) {
            return;
        }

//        if ($this->module == '9' ) {
//            $this->selectFinishOptionsSpecial();
//        }


        $numberOfSides = $this->finishes->finishOptions->numberOfSides;
        $sameType = $this->finishes->finishOptions->sameType;

        if ($this->debug) {
            echo "Number Sides: $numberOfSides \n";
            echo "SameType: $sameType \n";

        }

        if ($this->debug) {

            echo "\nNumber of Sides: $numberOfSides \n ";
        }

        if ($numberOfSides == "bothSides") {

//            echo "##############\n";
//            echo "##############\n";
//            echo "##############\n";
//            print_r($this->finishes->ext);
//            echo "##############\n";
//            echo "##############\n";
//            echo "##############\n";


            if (empty($this->finishes->ext->kind)) {
                $option = '';
            } else {
                $option = $this->finishes->ext->kind . "TwoSides";
            }

            $this->groupedItems["finishOptionsExt"]->selectOption($option);

//            $materialOption = $this->finishes->ext->kind . "OneSide";
            $this->selectMaterialIntOption($option);
            $this->selectMaterialExtOption($option);

            return;
        }

        if ($numberOfSides == "oneSide" && $sameType == "yes" ) {

            if ($this->debug) {
                echo "\n\n-------One Side Same Type YES----------\n";
            }

            $option = $this->finishes->ext->kind . "OneSide";
            $this->groupedItems["finishOptionsExt"]->selectOption($option);
            $option = $this->finishes->int->kind . "OneSide";
            $this->groupedItems["finishOptionsInt"]->selectOption($option);

            $this->selectMaterialIntOption($option);
            $this->selectMaterialExtOption($option);

            return;
        }

        if ($numberOfSides == "swpw") {

            if ($this->debug) {
                echo "\n\n-------One Side Same Type YES----------\n";
            }

            $option = $this->finishes->ext->kind . "OneSide";
            $this->groupedItems["finishOptionsExt"]->selectOption($option);
            $option = $this->finishes->int->kind . "OneSide";
            $this->groupedItems["finishOptionsInt"]->selectOption($option);

            $this->selectMaterialIntOption($option);
            $this->selectMaterialExtOption($option);

            return;
        }

        if ($numberOfSides == "oneSide" && $sameType == "no") {

            $option = $this->finishes->ext->kind . "OneSide";

            $this->groupedItems["finishOptionsExt"]->selectOption($option);
            $this->selectMaterialExtOption($option);

            if ($this->debug) {
                echo "\n\nOutSide Finish: " . $option . "\n";
            }

            if (is_object($this->finishes->int)) {

                $option = $this->finishes->int->kind . "OneSide";
                $this->groupedItems["finishOptionsInt"]->selectOption($option);
                $this->selectMaterialIntOption($option);

                if ($this->debug) {
                    echo "Inside Finish: " . $option . "\n";
//                    print_r($this->groupedItems["finishOptionsInt"]);
                }
            }


            return;
        }


        throw new Exception("SelectFinish Didn't get the right finished passed (selectFinishOptions)");

    }


    public
    function cleanCutSheet($arrayOfParts)
    {

        $goodParts = Array();

        if (isset($arrayOfParts['forCutSheet'])) {
            if ($arrayOfParts['forCutSheet']) {
                return ($arrayOfParts);
            }
        } else {
            foreach ($arrayOfParts as &$part) {
                if ($part['forCutSheet']) {
                    $goodParts[] = $part;
                }
            }
        }
        return $goodParts;

    }

    public
    function keepOnlyForCutSheetAndRemoveTotalCost($items)
    {

        $goodItems = Array();

        foreach ($items as &$part) {
            if (isset($part['forCutSheet'])) {
                if ($part['forCutSheet'] == 1) {
                    unset($part['cost']);
                    $goodItems[] = $part;
                }
            } else {
                unset($part['cost']);
                $goodItems[] = $part;
            }

        }
        return $goodItems;

    }


    public
    function costToCutSheet($costSheet)
    {

        $costSheet['items'] = $this->keepOnlyForCutSheetAndRemoveTotalCost($costSheet['items']);
        $costSheet['parts'] = $this->keepOnlyForCutSheetAndRemoveTotalCost($costSheet['parts']);

        return $costSheet;

    }

    public
    function removeTotalCost($parts)
    {

        $newParts = Array();

        foreach ($parts as &$part) {

            if (isset($part['Total Cost'])) {
                unset($part['Total Cost']);
                $newParts[] = $part;
            }
        }

        return ($newParts);
    }


    public
    function buildCostSheetParts($moduleInfo)
    {

        $newParts = [];
        $costSheet['parts'] = $this->coreItemsSized->breakDown['parts'];

        if (!$costSheet['parts']) {
            return [];
        }

        $moduleInfo = $this->getModuleInfoID($this->module);
        if ($moduleInfo['unitType'] == 'mm') {
            foreach ($costSheet['parts'] as &$part) {

                $part['Cut Size MM'] = $part['Cut Size Inches'];
                $part['Cut Size Inches'] = round($part['Cut Size Inches'] / 25.4, 2);
                $part['Quantity of Inches'] = round($part['Quantity of Inches'] / 25.4, 2);
                $part['Quantity of Feet'] = round($part['Quantity of Feet'] / 25.4, 2);
                array_push($newParts, $part);
            }
        } else {
            $newParts = $costSheet['parts'];
        }


        return $newParts;
    }

    public
    function buildDoorDetails($passObj)
    {
        $keyedAlike = 'no';
        $doorRestrictor = 'no';
        if (isset($passObj->hardwareExtras)) {
            if (preg_match('/.*Keyed Alike.*/', $this->hardwareExtras)) {
                $keyedAlike = 'yes';
            }
            if (preg_match('/.*Door Restrictor.*/', $this->hardwareExtras)) {
                $doorRestrictor = 'yes';
            }
        }

        $intFinish = "none";
        $extFinish = "none";

        if (isset($this->finishes->ext->type)) {
            if ($this->finishes->ext->type == 'custom') {
                $extFinish = $this->finishes->ext->customName;
            }
        }

        if ($extFinish == "none") {
            if (isset($this->finishes->ext->name1)) {
                $extFinish = $this->finishes->ext->name1;
            }
        }

        if (isset($this->finishes->int->type)) {
            if ($this->finishes->int->type == 'custom') {
                $intFinish = $this->finishes->int->customName;
            }
        }

        if ($intFinish == "none") {
            if (isset($this->finishes->int->name1)) {
                $intFinish = $this->finishes->int->name1;
            }
        }


        $doorDetails = [
            "Operation" => $this->swingDirection,
            "Interior Finish" => $intFinish,
            "Exterior Finish" => $extFinish,
            "Frame" => $this->frame,
            "Track" => $this->track,
            "Handles" => $this->doorDetails['Handles'],
            "Keyed Alike" => $keyedAlike,
            "Door Restrictor" => $doorRestrictor
        ];

        return $doorDetails;


    }

    public
    function buildDoorDesign()
    {

        $numberOfSwingDoors = 1;
        if (is_numeric($this->swingDoor)) {
            $numberOfSwingDoors = $this->swingDoor;
        }

        if ($this->swingDoor == "both") {
            $numberOfSwingDoors = 2;
        }
        if (!$this->panelMovementRight) {
            $this->panelMovementRight = 0;
        }

        $design = [
            "width inches" => $this->width,
            "height inches" => $this->height,
            "width MM" => round($this->width * 25.4, 2),
            "height MM" => round($this->height * 25.4, 2),
            "panels" => $this->panels,
            "Swing Door" => $this->swingDoor,
            "sliding left" => $this->panels - $numberOfSwingDoors - $this->panelMovementRight,
            "sliding right" => $this->panelMovementRight
        ];

        return $design;

    }


    public function buildCostSheet($json = null)
    {
        $costSheet['jobInfo'] = '';


        if ($json) {
            $passObj = json_decode($json);
            if (isset($passObj->jobInfo)) {
                $costSheet['jobInfo'] = $passObj->jobInfo;
            }
        }

        $moduleInfo = $this->getModuleInfoID($this->module);
        $costSheet['parts'] = $this->buildCostSheetParts($moduleInfo);

        $costSheet['items'] = $this->coreItemsFixed->breakDown['items'];
        $moduleInfo = $this->getModuleInfoID($this->module);

        $costSheet['header'] = [
            "material" => $moduleInfo['name'],
            "job" => uniqid(),
            "image" => "default.png",
            "total" => $this->totalPrice
        ];


        $costSheet['design'] = $this->buildDoorDesign();
        $costSheet['details'] = $this->buildDoorDetails($passObj);
        $costSheet['glassChoices'] = [
        ];

        $costSheet['glass'] = [
        ];


        $groupedBreakDown = Array();

        foreach ($this->groupedItems as &$item) {
            if (!is_null($item->selected)) {
                $set = '';

                $set = $item->breakDown;

                if (isset($set['parts'])) {

                    foreach ($set['parts'] as &$part) {

                        if ($moduleInfo['unitType'] == 'mm') {
                            $part['Cut Size MM'] = $part['Cut Size Inches'];
                            $part['Cut Size Inches'] = round($part['Cut Size Inches'] / 25.4, 2);
                            $part['Quantity of Inches'] = round($part['Quantity of Inches'] / 25.4, 2);
                            $part['Quantity of Feet'] = round($part['Quantity of Feet'] / 25.4, 2);
                        }
                        array_push($costSheet['parts'], $part);

                    }
                }

                if (isset($set['items'])) {

                    foreach ($set['items'] as &$items) {

                        $costSheet['items'][] = $items;
                    }
                }
                if (isset($set['percentage'])) {
                    foreach ($set['percentage'] as &$items) {
                        $costSheet['percentage'][] = $items;
                    }
                }
                if (isset($set['glassChoices'])) {
                    foreach ($set['glassChoices'] as &$items) {
                        if ($items['GlassOrOption'] == 'option') {
                            $costSheet['glassChoices'][] = $items;
                        }

                    }
                }

                if (isset($set['glassChoices'])) {
                    foreach ($set['glassChoices'] as &$items) {
                        if ($items['GlassOrOption'] == 'glass') {
                            if ($moduleInfo['unitType'] == 'mm') {
                                $items['Glass Width MM'] = $items['Glass Width Inches'];
                                $items['Glass Width Inches'] = round($items['Glass Width Inches'] / 25.4, 2);
                                $items['Glass Height MM'] = $items['Glass Height Inches'];
                                $items['Glass Height Inches'] = round($items['Glass Height Inches'] / 25.4, 2);
                            }
                            $costSheet['glass'][] = $items;
                        }

                    }
                }
            }
        }
        $costSheet = $this->ifItemsAndReturnSortedByMyOrder($costSheet);

        $grouped['GroupArray'] = $groupedBreakDown;

        $costSheet['ProfitMargin'] = $this->corePercentage[0];

        $costID = $this->addToCostSheet($costSheet);

        $cutSheet = $this->costToCutSheet($costSheet);
        $cutID = $this->addToCutSheet($cutSheet);



//        if (isset($this->corePercentage[0])) {
//            foreach ($this->corePercentage as $globalPercentageItem) {
//                if ($this->debug) {
//                    print_r($globalPercentageItem);
//                }
//
//                $globalPercentageItem->getTotal($total);
//                $this->totalPrice = $this->totalPrice + $globalPercentageItem->totalPrice;
//            }
//        }


        $this->costSheet = $costSheet;

        //BS to remove
        return ($cutID);

        if (isset($costSheet['jobInfo']->jobNumber)) {
            return ($costSheet['jobInfo']->jobNumber);
        } else {
            return ($cutSheet);
        }
    }


    public
    function buildCutSheet($json = null)
    {

        $costSheet['jobInfo'] = '';

        if ($json) {
            $passObj = json_decode($json);
//            print_r($passObj);
            if (isset($passObj->jobInfo)) {
                $costSheet['jobInfo'] = $passObj->jobInfo;
            }
        }

        if (empty($passObj->finishes->vinylColor)) {
            $passObj->finishes->vinylColor = '';
        }

        $moduleInfo = $this->getModuleInfoID($this->module);
        $costSheet['parts'] = $this->buildCostSheetParts($moduleInfo);

        $costSheet['items'] = $this->coreItemsFixed->breakDown['items'];
        $moduleInfo = $this->getModuleInfoID($this->module);

        $costSheet['header'] = [
            "material" => $moduleInfo['name'],
            "job" => uniqid(),
            "image" => "default.png",
            "total" => $this->totalPrice,
            "vinylColor" => $passObj->finishes->vinylColor
        ];

        $costSheet['design'] = $this->buildDoorDesign();
        $costSheet['details'] = $this->buildDoorDetails($passObj);
        $costSheet['glassChoices'] = [
        ];

        $costSheet['glass'] = [
        ];


        $groupedBreakDown = Array();
        foreach ($this->groupedItems as &$item) {
            if (!is_null($item->selected)) {
                $set = '';

                $set = $item->breakDown;

                if (isset($set['parts'])) {

                    foreach ($set['parts'] as &$part) {

                        if ($moduleInfo['unitType'] == 'mm') {
                            $part['Cut Size MM'] = $part['Cut Size Inches'];
                            $part['Cut Size Inches'] = round($part['Cut Size Inches'] / 25.4, 2);
                            $part['Quantity of Inches'] = round($part['Quantity of Inches'] / 25.4, 2);
                            $part['Quantity of Feet'] = round($part['Quantity of Feet'] / 25.4, 2);
                        }
                        array_push($costSheet['parts'], $part);

                    }
                }

                if (isset($set['items'])) {

                    foreach ($set['items'] as &$items) {

                        $costSheet['items'][] = $items;
                    }
                }
                if (isset($set['percentage'])) {
                    foreach ($set['percentage'] as &$items) {
                        $costSheet['percentage'][] = $items;
                    }
                }
                if (isset($set['glassChoices'])) {
                    foreach ($set['glassChoices'] as &$items) {
                        if ($items['GlassOrOption'] == 'option') {
                            $costSheet['glassChoices'][] = $items;
                        }

                    }
                }

                if (isset($set['glassChoices'])) {
                    foreach ($set['glassChoices'] as &$items) {
                        if ($items['GlassOrOption'] == 'glass') {
                            if ($moduleInfo['unitType'] == 'mm') {
                                $items['Glass Width MM'] = $items['Glass Width Inches'];
                                $items['Glass Width Inches'] = round($items['Glass Width Inches'] / 25.4, 2);
                                $items['Glass Height MM'] = $items['Glass Height Inches'];
                                $items['Glass Height Inches'] = round($items['Glass Height Inches'] / 25.4, 2);
                            }
                            $costSheet['glass'][] = $items;
                        }

                    }
                }
            }

        }

        $costSheet = $this->ifItemsAndReturnSortedByMyOrder($costSheet);

        $grouped['GroupArray'] = $groupedBreakDown;

        $costID = $this->addToCostSheet($costSheet);

        $cutSheet = $this->costToCutSheet($costSheet);

        return ($cutSheet);


    }


    public
    function ifItemsAndReturnSortedByMyOrder($costSheet)
    {
        if (isset($costSheet['parts'])) {
            $costSheet['parts'] = $this->returnSortedItemsByMyOrder($costSheet['parts']);
        }
        if (isset($costSheet['items'])) {
            $costSheet['items'] = $this->returnSortedItemsByMyOrder($costSheet['items']);
        }
//    if (isset($costSheet['percentage'])) {
//      $costSheet['percentage'] = $this->returnSortedItemsByMyOrder($costSheet['percentage']);
//    }

        return $costSheet;
    }

    public
    function returnSortedItemsByMyOrder($items)
    {
        $orderedItems = array();
        foreach ($items as $key => $row) {

            $orderedItems[$key] = $row['myOrder'];
        }
        array_multisort($orderedItems, SORT_ASC, $items);
        return $items;

    }

    public
    function postJSONtoEndPoint($data, $endPoint)
    {


        $data = addToCostSheet($data);
        return $data;

//
//        $ch = curl_init();
//        $post_values = array('json_data' => $json_data);
//        curl_setopt($ch, CURLOPT_URL, 'https://localhost/api/' . $endPoint);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_data);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//            'Content-Type: application/json',
//            'Content-Length: ' . strlen($json_data)));
//
//        $data = curl_exec($ch);
//
//        if (curl_errno($ch)) {
//            throw new Exception("FAILED to POST".curl_errno($ch));
//        } else {
//            return ($data);
//        }
//        curl_close($ch);

    }

    public
    function addToCostSheet($data)
    {

        $json = json_encode($data);
        $myData = json_decode($json);
        $myJSON['json'] = $json;

        if (isset($myData->jobInfo->productionDate)) {
            $date = date('Y-m-d', strtotime($myData->jobInfo->productionDate));
            $myJSON['productionDate'] = $date;
        } else {
            $myJSON['productionDate'] = '';
        }

        if (isset($myData->jobInfo->dueDate)) {
            $date = date('Y-m-d', strtotime($myData->jobInfo->dueDate));
            $myJSON['dueDate'] = $date;
        } else {
            $myJSON['dueDate'] = '';
        }

        if (isset($myData->jobInfo->jobNumber)) {
            $myJSON['jobNumber'] = $myData->jobInfo->jobNumber;
        } else {
            $myJSON['jobNumber'] = '';
        }

        $db = connect_db();
        $sql = "SELECT id from costSheet where jobNumber=? ";
//    $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $myJSON['jobNumber']);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $lastID = '';
        if ($data) {
            $lastID = $data['id'];
            updateOption($db, $myJSON, "costSheet", $myJSON['jobNumber']);
        } else {
            $lastID = insertOption($db, $myJSON, "costSheet");
        }

        return $lastID;


//        updateOption($db, $myJSON, "costSheet", $myJSON['jobNumber']);
//        $lastID = insertOption($db, $myJSON, "costSheet");
        return;
    }

    public
    function addToCutSheet($data)
    {

        $json = json_encode($data);
        $myData = json_decode($json);
        $myJSON['json'] = $json;

        if (isset($myData->jobInfo->productionDate)) {
            $date = date('Y-m-d', strtotime($myData->jobInfo->productionDate));
            $myJSON['productionDate'] = $date;
        } else {
            $myJSON['productionDate'] = '';
        }

        if (isset($myData->jobInfo->dueDate)) {
            $date = date('Y-m-d', strtotime($myData->jobInfo->dueDate));
            $myJSON['dueDate'] = $date;
        } else {
            $myJSON['dueDate'] = '';
        }

        if (isset($myData->jobInfo->jobNumber)) {
            $myJSON['jobNumber'] = $myData->jobInfo->jobNumber;
        } else {
            $myJSON['jobNumber'] = '';
        }


        $db = connect_db();
        $sql = "SELECT id from cutSheet where jobNumber=? ";
//    $stmt = $db->stmt_init();
        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $myJSON['jobNumber']);
        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        $lastID = '';
        if ($data) {
            $lastID = $data['id'];
            updateOption($db, $myJSON, "cutSheet", $myJSON['jobNumber']);
        } else {
            $lastID = insertOption($db, $myJSON, "cutSheet");
        }

        // $myJSON['jobNumber']
        //BS to remove
        return $myJSON['jobNumber'];
    }


}

?>
