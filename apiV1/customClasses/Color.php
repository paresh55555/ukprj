<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';
require_once 'Module.php';



class Color
{
    public function getColorsForModule($id)
    {
        $module = new Module();
        $groups = $module->getColorGroupsByID($id);

        $colorOptions = array();
        foreach ($groups as $group) {
            $location = $group['location'];
            $group['colors']= $this->getColorsForGroupID($group['colorGroup'], $location);

            $kind = $group['kind'];

            $group = $this->cleanColors($group);

            $colorOptions[$location][$kind] = $group;
        }

        return $colorOptions;
    }


    public function cleanColors ($group)
    {
        unset($group['kind']);
        unset($group['location']);
        unset($group['colorGroup']);

        return $group;
    }


    public function getColorsForGroupID($groupID, $location)
    {
        if (!is_numeric($groupID)) {
            throw new Exception("getColorsForGroupID wasn't passed an ID!!!");
        }

        $db = connect_db();
        $sql = "SELECT *  from SQ_colors where myGroup = ? order by  myOrder";
        $stmt = $db->prepare($sql);
        $stmt->bind_param("i", $groupID);
        $stmt->execute();
        $results = $stmt->get_result();
        $colorTypes = Array();

        while ($color = $results->fetch_assoc()) {
            $color['location'] = $location;

            if ($color['kind'] == 'ral') {
                $superSection = $color['superSection'];
                if (!isset($colorTypes[$superSection])) {
                    $colorTypes[$superSection] = [];
                }
                $row = ["kind" => "ral", "location" => $color['location'], "colorGroupKind" => 'ral', "superSection" => $color['superSection'],
                    "nameOfOption" => $color['nameOfOption'], "hex" => $color['hex'], "type" => 'ral', "ralColor" => $color['ralColor']];

                array_push($colorTypes[$superSection], $row);
            } else {
                $colorTypes[] = $color;
            }
        }

        return $colorTypes;
    }
}