<?php

class translatedGroupOptions    {

  public $translatedOptions = Array();
  public $idToName = Array();


  public function buildTranslatedOptionsByModule($module) {

    $db = connect_db();
    $sql =  "SELECT * from groupNames where module_id = ?";
    $stmt= $db->prepare($sql) ;
    $stmt->bind_param("i", $module);
    $stmt->execute();
    $results = $stmt->get_result();
    $data =[];

    while ($options = $results->fetch_assoc()) {
      $name = $options['groupName'];
      $id = $options['group_id'];
      $this->translatedOptions[$name]= $id;
      $this->idToName[$id]= $name;

    }

  }

  public function translateNameToID($name) {

    if (!$name) {
      throw new Exception('Name was never Passed for translate to Name');
    }
    if (!$this->translatedOptions) {
      throw new Exception('Build Translated Options was never run');
    }

    return ($this->translatedOptions[$name]);

  }

  public function idToName ($id) {

    if (!$id) {
      throw new Exception('ID was never Passed for translate to Name');
    }
    if (!$this->translatedOptions) {
      throw new Exception('Build Translated Options was never run');
    }

    return ($this->idToName[$id]);

  }
}
?>