<?php


require_once 'lib/mysql.php';
require_once 'Auth.php';
require_once 'User.php';

class Guest
{
    protected $name;
    private $email;
    private $password;
    private $validation;


    public function validateGuest($guest)
    {

        if (!isset($guest->name)) {
            throw new Exception('missing parameter name in newGuest');
        }
        if (!isset($guest->email)) {
            throw new Exception('missing parameter email in newGuest');
        }
        if (!isset($guest->zip)) {
            throw new Exception('missing parameter zip in newGuest');
        }

        return true;
    }


    public function newGuest($guest)
    {
        $this->validateGuest($guest);
        return $this->insertNewGuest($guest);

    }

    public function insertNewGuest($guest, $assignBarry = false){
        $db = connect_db();

        $sql = "INSERT INTO  guest SET `name`=?, `email`=?, `zip`=?, `phone`=?, `leadType`=?";
        $stmt = $db->prepare($sql);

        $stmt->bind_param("sssss", $guest->name, $guest->email, $guest->zip, $guest->phone, $guest->leadType);
        $stmt->execute();

        $lastId = $db->insert_id;

        $config = Array("type" => "guest", "id" => $lastId);
        $auth = new Auth();
        $token = $auth->login($config);

        $user = new User();

        $v3token = $user->getV3Token('guest', 'guest', 'apiV3');
        $v4token = $user->getV4Token('guest', 'guest', 'api/V4');

        if ($token) {
            $response = array("id" => $lastId, "token" => $token, "type" => 'guest', 'v3token' => $v3token, 'v4token' => $v4token);
            if(isset($guest->guestForm)){
                $response['guestForm'] = true;
                if($assignBarry)$response['assignBarry'] = true;
            }
            return $response;
        }
        return false;
    }


    public function getGuest($guestID)
    {

        if (empty($guestID)) {
            throw new Exception('Guest ID Parameters was not passed for getGuest Lookup');
        }

        $db = connect_db();

        $sql = " SELECT * from guest
              WHERE id=? limit 1  ";

        $stmt = $db->prepare($sql);
        $stmt->bind_param("s", $guestID);

        $stmt->execute();
        /* Fetch result to array */
        $result = $stmt->get_result();
        $data = $result->fetch_assoc();

        return $data;
    }

}