<?php

require_once 'lib/mysql.php';

class BuilderGlass    {


  public function getGlassByModuleAndOption($module,$option_type) {

    $db = connect_db();
    $sql =  "SELECT * from optionsGlass where module_id = ? and option_type = ?";

    $stmt= $db->prepare($sql);
    $stmt->bind_param("is", $module, $option_type);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();


    while ($options = $results->fetch_assoc()) {

     $data[]= $options;

    }
    return $data;

  }


  public function getGlassByModuleAndNameOfOption($module,$nameOfOption) {

    $db = connect_db();
    $sql =  "SELECT * from optionsGlass where module_id = ? and nameOfOption = ? ";

    $stmt= $db->prepare($sql) ;
    $stmt->bind_param("is", $module, $nameOfOption);
    $stmt->execute();
    $results = $stmt->get_result();
    $data = array();


    while ($options = $results->fetch_assoc()) {

      $data[]= $options;

    }
    return $data;

  }

}



?>
