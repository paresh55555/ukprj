<?php

require_once 'lib/mysql.php';
require_once 'lib/functions.php';


class Builder
{


    public function __construct()
    {


    }


    public function getFrontEndAssets() {


        $db = connect_db();
        $sql =  "SELECT * from SQ_frontEndAssets where active = 1 order by myOrder";

        $stmt= $db->prepare($sql) ;
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[]= $options;
        }
        return $data;

    }


    public function getFrontEndInteractions() {


        $db = connect_db();
        $sql =  "SELECT * from SQ_frontEndInteractions where active = 1 order by myOrder";

        $stmt= $db->prepare($sql) ;
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[]= $options;
        }
        return $data;

    }

    public function getBackEndInteractions() {


        $db = connect_db();
        $sql =  "SELECT * from SQ_backEndInteractions where active = 1 order by myOrder";

        $stmt= $db->prepare($sql) ;
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[]= $options;
        }
        return $data;

    }


    public function getBackEndAssets() {


        $db = connect_db();
        $sql =  "SELECT * from SQ_backEndAssets where active = 1 order by myOrder";

        $stmt= $db->prepare($sql) ;
        $stmt->execute();
        $results = $stmt->get_result();
        $data = array();

        while ($options = $results->fetch_assoc()) {
            $data[]= $options;
        }
        return $data;

    }

}



