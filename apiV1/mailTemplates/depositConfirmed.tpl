<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width">
<!--[if !mso]><!-- --><meta http-equiv="X-UA-Compatible" content="IE=edge">
<!--<![endif]--><title>Panoramic Doors Post Sale Thank You Email</title>
</head>
<body style="width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0!important;padding:0!important;background-color:#FFFFFF">
<style type="text/css">


	#outlook a{padding:0;}

	body{width:100%!important;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0!important;padding:0!important;}

	.ExternalClass{width:100%;}

	.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}

	.bodytbl{margin:0;padding:0;width:100%!important;-webkit-text-size-adjust:none;}

	img{outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block;max-width:100%;}

	a img{border:none;}

	p{margin:1em 0;}


	table{border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}

	table td{border-collapse:collapse;}

	.o-fix table,.o-fix td{mso-table-lspace:0pt;mso-table-rspace:0pt;}


	body,.bodytbl{background-color:#FFFFFF/*Background Color*/;}


	table{color:#787878/*Text Color*/;}

	td,p{color:#787878;}

	.h1{color:#353535/*Headings*/;}

	.h2{color:#353535;}

	.cta .h2{color:#FFFFFF;}

	.cta .h3{color:#FFFFFF;}

	.quote{color:#AAAAAA;}

	.invert,.invert h1,.invert td,.invert p{background-color:#353535;color:#FFFFFF !important;}


	.wrap.header{}
	.wrap{background-color:#FFFFFF;}

	.wrap.body-i{background-color:#353535;}

	.wrap.footer{}
	.padd{width:20px;}


	a{color:#00A9E0;}

	a:link,a:visited,a:hover{color:#00A9E0/*Contrast*/;}

	.btn,.btn div,.btn a{color:#FFFFFF/*Contrast Link Color*/;}

	.btn a,.btn a img{background:#00A8E0/*Button Color*/;}

	.invert .btn a,.invert .btn a img{background:none;}


	h1,h2,h3,h4,h5,h6{color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;}

	h1{font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px;}

	h2{font-size:20px;margin-bottom:12px;margin-top:2px;line-height:24px;}

	h3{font-size:18px;margin-bottom:12px;margin-top:2px;line-height:24px;}

	h4{font-size:16px;}

	h5{font-size:14px;}

	h6{font-size:12px;}

	h1 a,h2 a,h3 a,h4 a,h5 a,h6 a{color:#00A9E0;}

	h1 a:active,h2 a:active,h3 a:active,h4 a:active,h5 a:active,h6 a:active{color:#00A9E0 !important;}

	h1 a:visited,h2 a:visited,h3 a:visited,h4 a:visited,h5 a:visited,h6 a:visited{color:#00A9E0 !important;}

	.h1{font-family:Helvetica,Arial,sans-serif;font-size:55px;font-weight:bold;line-height:40px !important;letter-spacing:-2px;}

	.h2{font-family:Helvetica,Arial,sans-serif;font-size:19px;font-weight:bold;letter-spacing:-1px;line-height:24px;}

	.h3{font-family:Helvetica,Arial,sans-serif;font-size:17px;letter-spacing:-1px;line-height:24px;}


	.line{border-bottom:1px solid #AAAAAA/*Separator*/;}

	table{font-family:Helvetica,Arial,sans-serif;font-size:14px;}

	td,p{line-height:24px;}

	ul,ol{margin-top:20px;margin-bottom:20px;}

	li{line-height:24px;}

	td,tr{padding:0;}

	.quote{font-family:Helvetica,Arial,sans-serif;font-size:24px;letter-spacing:0;margin-bottom:6px;margin-top:6px;line-height:24px;font-style:italic;}

	.small{font-size:10px;color:#787878;line-height:15px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px;}

	table.plan {width:100%;min-width:100%;}

	table.plan td{border-right:1px solid #EBEBEB/*Lines*/;border-bottom:1px solid #EBEBEB;text-align:center;}

	table.plan td.last{border-right:0;}

	table.plan th{text-align:center;border-bottom:1px solid #EBEBEB;}

	a{text-decoration:none;padding:2px 0px;}

	.btn{display:block;}

	.btn a{display:inline-block;padding:0;line-height:0.5em;}

	.btn img,.social img{display:inline;margin:0;}

	.social .btn a,.social .btn a img{background:none;}

	.right{text-align:right;}


	table.textbutton td{background:#00A8E0;padding:1px 14px 1px 14px;color:#FFF;display:block;height:24px;vertical-align:top;margin-right:4px;margin-bottom:4px;}

	table.textbutton a{color:#FFF;font-size:14px;font-weight:100;line-height:20px;width:100%;display:inline-block;}

	.label table.textbutton {width:auto !important;}

	.label table.textbutton td{height:auto !important;}

	.label table.textbutton a{padding:2px 0 !important;font-size:12px !important;}

	.cta table.textbutton td{height:32px;padding:4px 16px 7px 16px;}

	.cta table.textbutton a{font-size:19px;line-height:32px;}


	div.preheader{line-height:0px;font-size:0px;height:0px;display:none !important;visibility:hidden;text-indent:-9999px;}


	@media only screen and (max-width: 599px) {
		body{-webkit-text-size-adjust:120% !important;-ms-text-size-adjust:120% !important;}

		.wrap{width:96% !important;}

		.wrap .padd{width:10px !important;}

		.wrap table{width:100% !important;}

		.wrap img {max-width:100% !important;height:auto !important;}

		.wrap .m-padd {padding:0 20px !important;}

		.wrap .m-w-auto{width:auto !important;}

		.wrap .m-l{text-align:left !important;}

		.wrap .h div{letter-spacing:-1px !important;font-size:18px !important;}

		.wrap .m-0{width:0;display:none;}

		.wrap .m-b,{margin-bottom:20px !important;}

		.wrap .m-b,.m-b img{display:block;min-width:100% !important;width:100% !important;}

		table{font-size:15px !important;}

		table.textbutton td{height:44px !important;}

		.cta table.textbutton td{height:50px !important;}

		table.textbutton a{padding:10px 0 !important;font-size:18px !important;}

	}

	@media only screen and (max-width: 479px) {
	}
	@media only screen and (max-width: 320px) {
	}
	@media only screen and (min-device-width: 375px) and (max-device-width: 667px) {
		body{-webkit-text-size-adjust:170% !important;-ms-text-size-adjust:170% !important;}

	}
	@media only screen and (min-device-width: 414px) and (max-device-width: 736px) {
		body{-webkit-text-size-adjust:170% !important;-ms-text-size-adjust:170% !important;}

	}


</style>

<table class="bodytbl" width="100%" cellpadding="0" cellspacing="0" style="margin:0;padding:0;width:100%!important;-webkit-text-size-adjust:none;border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#FFFFFF;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td align="center" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">

		<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr height="20" style="padding:0">
<td width="600" align="left" valign="bottom" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			</td>
		</tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td valign="top" align="right" width="600" class="small label" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px">
				<table class="textbutton" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px;width:auto !important"><tr style="padding:0">
				  <td align="center" style="border-collapse:collapse;color:#FFF;line-height:24px;padding:1px 14px 1px 14px;background:#00A8E0;display:block;height:auto !important;vertical-align:top;margin-right:4px;margin-bottom:4px"><a href="" label="Go to top" style="color:#FFF;text-decoration:none;padding:2px 0 !important;font-size:12px !important;font-weight:100;line-height:20px;width:100%;display:inline-block">IMPORTANT INFORMATION ABOUT YOUR ORDER</a></td></tr></table>
</td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr height="80" style="padding:0">
<td align="left" valign="bottom" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px">
<tr style="padding:0">
<td rowspan="1" align="left" valign="bottom" class="h1" style="border-collapse:collapse;color:#353535;font-family:Helvetica,Arial,sans-serif;font-size:55px;font-weight:bold;line-height:40px !important;letter-spacing:-2px;padding:0">
					<span>Panoramic Doors</span>
					</td>
				</tr>
<tr style="padding:0">
<td align="left" valign="top" class="h2" style="border-collapse:collapse;color:#353535;font-family:Helvetica,Arial,sans-serif;font-size:19px;font-weight:bold;letter-spacing:-1px;line-height:24px;padding:0">
					<span>thank you for your order!</span>
					</td>
				</tr>
</table>
</td>
		</tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td height="19" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td height="40" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- ☺ header block ends here -->
<!-- Full Size Image with Text start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap cta" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="60" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" valign="top" background="https://dummyimage.revaxarts.com/blurred/42/600x260.jpg?gray" style="background-image:url(http://panoramicdoors.com/wp-content/uploads/2017/12/6_Panel-Panoramic-Door.jpg);background-position:center;background-size: cover;border-collapse:collapse;color:#787878;line-height:24px;padding:0" bgcolor="#353535">
<!--[if gte mso 9]>
<v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:600px;">
<v:fill type="frame" src="https://dummyimage.revaxarts.com/blurred/42/600x260.jpg?gray" color="#353535" />
<v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
<![endif]-->
			<div><table cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px">
<tr style="padding:0">
<td width="24" height="200" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
<td align="center" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td>
<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
			</tr>
<tr style="padding:0">
<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
				<td width="552" valign="top" align="center" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			
				</td>
				<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
			</tr>
<tr style="padding:0"><td height="24" colspan="3" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr>
<tr style="padding:0">
<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
				<td width="552" align="center" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
					<div class="btn" align="center" style="color:#FFFFFF;display:block">
						
</div>
				</td>
				<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
			</tr>
<tr style="padding:0">
<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
<td align="center" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td>
<td width="24" class="padd" style="border-collapse:collapse;color:#787878;width:20px;line-height:24px;padding:0"> </td>
			</tr>
</table></div>
<!--[if gte mso 9]>
</v:textbox>
</v:rect>
<![endif]-->
				</td></tr></table>
</td>
		</tr></table>
<!-- Full Size Image with Text end   --><!-- Intro start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">

	<br><br><div class="h3" style="font-family:Helvetica,Arial,sans-serif;font-size:17px;letter-spacing:-1px;line-height:24px"><span>Order ID: {jobId}</span></div><br>

	<div class="h3" style="font-family:Helvetica,Arial,sans-serif;font-size:17px;letter-spacing:-1px;line-height:24px"><span>Dear {customerName},</span></div><br>

	<div class="h3" style="font-family:Helvetica,Arial,sans-serif;font-size:17px;letter-spacing:-1px;line-height:24px"><span>Thank you for your order. Please read this email as it contains important information about your order.</span></div>
				</td>
			</tr></table>
</td>
		</tr></table>
<!-- Intro end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/storage.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>ESTIMATED MANUFACTURING COMPLETION DATE</span></h1>
							<div>
							  <p>You<strong>r estimated completion date is: </strong> {compDate} . You will be contacted 3 weeks before to confirm or modify this date if your project is delayed. Please allowed additional time for delivery. </p>
<p style="margin:1em 0;color:#787878;line-height:24px">&nbsp;</p></div>
							<div class="btn" style="color:#FFFFFF;display:block">
								
						</div>
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  -->
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px">
<tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/payment.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>FINAL PAYMENT </span></h1>
							<div><p style="margin:1em 0;color:#787878;line-height:24px">
							 Final payment is required upon manufacturing completion of the product and before items are shipped. 
							</p></div>
							
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/storage.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><strong>STORAGE COSTS</strong></h1>
							<div>
							  <p>If for any reason you are not ready to receive your order for delivery, <br />
							    storage costs of $200 per system per week will incur. Any storage fees due, must be paid before systems will ship. If you anticipate a delay in your project please inform your sales representative so that if possible we can adjust your producton time.</p>
<p style="margin:1em 0;color:#787878;line-height:24px">&nbsp;</p></div>
							
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/delivery.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><strong>CURBSIDE DELIVERY</strong></h1>
							<div>
							  <p style="margin:1em 0;color:#787878;line-height:24px">Panoramic Doors delivery is to the curb only. Please arrange to have the product transported into your property. You will be notified prior to deliver to allow time enough for such arrangements.</p>
							</div>
							
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/inspect.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>PRODUCT INSPECTION</span></h1>
							<div>
							  <p>Inspect delivered product for signs of damage. Immediately report any damages or missing parts. Take pictures and call at 760.722.1250 ext. 231. All damages should be reported before installation to be covered under warranty.</p></div>

							
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/manual.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>READ THE MANUAL </span></h1>
							<div><p style="margin:1em 0;color:#787878;line-height:24px">
							 Preparation is the key to a trouble-free installation. The manual and Video tutorials are available online <a href="www.panoramicdoors.com" style="color:#00A9E0;text-decoration:none;padding:2px 0px">www.panoramicdoors.com</a></a> - support tab.</p>
                             <p>FOR ANY TECHNICAL QUESTIONS PLEASE CALL 760.722.1250</p></div>
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/wood.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>WOOD COMPONENTS</span></h1>
							<div><p style="margin:1em 0;color:#787878;line-height:24px">
							 All unfinished wood components should not be exposed to water and should be sealed within 5 days from unwrapping.
							</p></div>
							
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->









<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/register.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>REGISTER YOUR SYSTEM(S) </span></h1>
							<div><p style="margin:1em 0;color:#787878;line-height:24px">
							If your system was ordered by a builder, contractor, architect, it is very important you register your system(s) online, to keep the correct information of the owner in our database. 
							</p></div>
							
						</td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="right" class="small" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px"></td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator end   -->








<!-- 1/4 Image on the Left start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
		<!-- CONTENT start -->
					<table cellpadding="0" cellspacing="0" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="135" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<img src="http://panoramicdoors.com/wp-content/uploads/2017/12/installer.jpg" alt="" width="135" height="160" border="0" style="max-width:100%;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:block">
</td>
					</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="445" valign="top" align="left" class="m-b" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
							<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>WHEN HIRING AN INSTALLER</span></h1>
							<div><p style="margin:1em 0;color:#787878;line-height:24px">
							When hiring an installer for your Panoramic Door(s) we recommend that you inquire about:
<ul>
<li>Installation Warranty - terms and length.</li> 
<li>Does the installer perform future adjustments and maintenance.</li>
<li>What are the adjustment and maintenance costs.</li></ul>

							</p></div>
						
					  </td>
					</tr></table>
</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/4 Image on the Left end   --><!-- Separator with button start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
			<table cellpadding="0" cellspacing="0" align="right" class="m-w-auto" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td valign="top" align="right" width="600" class="small label" style="border-collapse:collapse;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px">
				<table class="textbutton" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px;width:auto !important"><tr style="padding:0"><td align="center" width="auto" style="border-collapse:collapse;color:#FFF;line-height:24px;padding:1px 14px 1px 14px;background:#00A8E0;display:block;height:auto !important;vertical-align:top;margin-right:4px;margin-bottom:4px"><a href="" label="Go to top" style="color:#FFF;text-decoration:none;padding:2px 0 !important;font-size:12px !important;font-weight:100;line-height:20px;width:100%;display:inline-block">top</a></td></tr></table>
</td>
			</tr></table>
</td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="20" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<!-- Separator with button end   --><!-- 1/1 Text start  --><table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<!-- CONTENT start --><td width="600" valign="top" align="left" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
						<h1 style="color:#353535;font-family:Helvetica,Arial,sans-serif;font-weight:bold;font-size:24px;letter-spacing:-2px;margin-bottom:6px;margin-top:6px;line-height:24px"><span>We are here to help</span></h1>
						<div><p style="margin:1em 0;color:#787878;line-height:24px">
							 If you have any questions about your order please do not hesitate to contact me.
						</p>
                        <p>Name: {salesName}<br/>
                        Phone: {salesPhone}<br/>
                        eMail:{salesEmail}</p>

							<p style="margin:1em 0;color:#787878;line-height:24px;text-align: center;">
								<i>Our passion is the people whose spaces we help to transform, we deliver a product and service that allows them to feel confident of their purchase and proud to tell their friends and family they own a Panoramic Door.</i>
							</p>
							<p style="margin:1em 0;color:#787878;line-height:24px;text-align: center;">
								<i>Transforming one space at a time!</i>
							</p>
					</td>
		<!-- CONTENT end -->
				</tr></table>
</td>
		</tr></table>
<!-- 1/1 Text end   --><!-- Footer start --><table width="600" cellpadding="0" cellspacing="0" class="wrap line" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;border-bottom:1px solid #AAAAAA;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td width="600" height="39" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"> </td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0"><td style="border-collapse:collapse;color:#787878;line-height:24px;padding:0"></td></tr></table>
<table width="600" cellpadding="0" cellspacing="0" class="wrap" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;background-color:#FFFFFF;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="600" valign="top" align="center" style="border-collapse:collapse;color:#787878;line-height:24px;padding:0">
				<table cellpadding="0" cellspacing="0" class="o-fix" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px">
<tr style="padding:0">
<!-- CONTENT start --><td width="600" valign="top" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0">
						<table cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="394" class="small" align="left" valign="top" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px">
								<div><span>You have received this email because you have ordered from <a href="http://panoramicdoors.com" style="color:#00A9E0;text-decoration:none;padding:2px 0px">Panoramic Doors</a>.<br>
								</span></div>
								<div><span>© 2017 Panoramic Doors, All rights reserved</span></div>
							</td>
						</tr></table>
<table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;font-family:Helvetica,Arial,sans-serif;font-size:14px"><tr style="padding:0">
<td width="187" class="small social" align="right" valign="top" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:15px;padding:0;font-size:10px;text-transform:uppercase;word-spacing:-1px;margin-bottom:4px;margin-top:6px">
								<div>
									<a href="https://www.houzz.com/pro/panoramicdoors/panoramic-doors" style="color:#00A9E0;text-decoration:none;padding:2px 0px"><img src="{imagesUrl}/email/1466482884_houzz-hexagon-social-media.png" alt="" width="32" height="32" border="0" style="max-width:100%;max-height:32px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:inline;border:none;margin:0"></a>
                                    <a href="https://twitter.com/PanoramicDoors/" style="color:#00A9E0;text-decoration:none;padding:2px 0px"><img src="{imagesUrl}/email/1464224297_twitter_circle_color.png" alt="" width="32" height="32" border="0" style="max-width:100%;max-height:32px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:inline;border:none;margin:0"></a>
									<a href="https://facebook.com" style="color:#00A9E0;text-decoration:none;padding:2px 0px"><img src="{imagesUrl}/email/1464224282_facebook.png" alt="" width="32" height="32" border="0" style="max-width:100%;max-height:32px;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;image-rendering:optimizeQuality;display:inline;border:none;margin:0"></a>
								</div>
							</td>
						</tr></table>
</td>
		<!-- CONTENT end -->
				</tr>
<tr class="m-0" style="padding:0"><td height="24" style="border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;color:#787878;line-height:24px;padding:0"> </td></tr>
</table>
</td>
		</tr></table>
<!-- Footer end -->
</td>
</tr></table>
</body>
</html>


