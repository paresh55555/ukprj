# Panoramic UK Dev Environment

**Table of Contents**

- [Panoramic UK Dev Environment](#panoramic-uk-dev-environment)
    - [Server profile](#server-profile)
    - [Environment URL](#environment-url)
    - [Database Access](#database-access)
        - [phpmyadmin full administration](#phpmyadmin-full-administration)
        - [application access](#application-access)
        - [database refresh](#database-refresh)
    - [Bamboo URL](#bamboo-url)
    - [Manual build and deploy](#manual-build-and-deploy)
        - [With scripts](#with-scripts)
        - [Without scripts](#without-scripts)
    - [/var/www related files and folders](#varwww-related-files-and-folders)
    - [Configuration files](#configuration-files)
        - [Apache](#apache)
        - [PHP](#php)
        - [MySQL](#mysql)
            - [percona-xtradb-clukter.conf.d/mysqld.cnf](#percona-xtradb-clukterconfdmysqldcnf)
            - [percona-xtradb-clukter.conf.d/wsrep.cnf](#percona-xtradb-clukterconfdwsrepcnf)
        - [MySQL backups](#mysql-backups)
        - [Cron jobs](#cron-jobs)
        - [remote-syslog](#remote-syslog)
        - [Rsyslog](#rsyslog)
    - [Database backup](#database-backup)
        - [Manual database restore](#manual-database-restore)
    - [Monitoring and alarms](#monitoring-and-alarms)
        - [Logs](#logs)
        - [Alarms](#alarms)
            - [UptimeRobot](#uptimerobot)
            - [Cronitor](#cronitor)

## Server profile 
 * IP Address: 49.79.13.251
 * Linode URL: https://cloud.linode.com/linodes/11238776 
 * Linode plan: Linode 4GB
 
## Environment URL
 * Main page: https://dev-uk.rioft.com
      * uker: admin
      * password: test
 * Admin page: https://dev-uk.rioft.com/SQ-admin
      * uker: admin
      * password: test
 * Main page (glaze): https://dev-glaze.rioft.com
      * uker: admin
      * password: test
 * Admin page (glaze): https://dev-glaze.rioft.com/SQ-admin
      * uker: admin
      * password: test

## Database Access
### phpmyadmin full administration
 * URL: https://dev-uk.rioft.com/phpmyadmin
 * uker: phpmyadmin
 * password: XJgqeTGqLhTi

### application access
 * host: localhost
 * uker: salesQuoter
 * password: QUu3!YjTdjJy8Ps$
 * database: panoramicUK

### database refresh
 * host: localhost
 * user: bamboo
 * password: ohk0oRai

## Bamboo URL
 * Build UK Branch:https://salesquoter.rioft.com/browse/BUILD-BUB2
 * Deploy UK Branch - Dev: https://salesquoter.rioft.com/browse/DEVENV-DUBD2
 * Reset databases: https://salesquoter.rioft.com/browse/DEVENV-RD 
 * Take DB Snapshot: https://salesquoter.rioft.com/browse/DEVENV-TDS
 * Update DB: https://salesquoter.rioft.com/browse/DEVENV-UMD
 
## Manual build and deploy
Sometimes it is useful  build the code with scripts or execute the build process step by step, sites are built on deploy server and transported to dev server with rsync. There is two ways of replicate this:

### With scripts

    ### Every should be done from deployment server ###
    ## Avoid perms and authentication issues using "bamboo" account
    ## Following steps takes UK-7 as example:
    export BRANCH=UK-7 
     
    ## Create folder and clone branch
    cd /var/www/code/uk
    if [ -d $BRANCH ] 
    then 
       /var/www/deploys/syncGit.sh -p "code/uk/$BRANCH" -b $BRANCH
     else
       git clone git@bitbucket.org:fantasticsoftware/uk.git -b $BRANCH 
    fi
     
    ## Build API V1 
    /var/www/deploys/deployApi.sh -p code/uk/$BRANCH/apiV1
    # Copy API V1 from deploy to dev server
    /var/www/deploys/rsyncTransport.sh 45.79.13.251 code/uk/$BRANCH/apiV1/ code/uk/apiV1/ rsyncExcludeApi.txt
     
    ## Build API V4 
    /var/www/deploys/deployApi.sh -p code/uk/$BRANCH/apiV4
    # Copy API V4 from deploy to dev server
    /var/www/deploys/rsyncTransport.sh 45.79.13.251 code/uk/$BRANCH/apiV4/ code/uk/apiV4/ rsyncExcludeApi.txt
     
    ## Build admin site
    /var/www/deploys/deployWebpackSite.sh -p code/uk/$BRANCH/admin -g buildDev
    # Copy admin from deploy to dev server
    /var/www/deploys/rsyncTransport.sh 45.79.13.251 code/uk/$BRANCH/admin/SQ-admin/ code/uk/admin/SQ-admin/ rsyncExcludeFrontEnd.txt
     
    ## Build website, document root UK site
    /var/www/deploys/deployWebsite.sh -p code/uk/$BRANCH/website -g buildProd
    # Copy website from deploy to dev server
    /var/www/deploys/rsyncTransport.sh 45.79.13.251 code/uk/$BRANCH/website/dist code/uk/website/

### Without scripts

     ### Every should be done from deployment server ###
     ## Avoid perms and authentication issues uking "bamboo" account
     ## Following steps takes UK-7 as example:
     export BRANCH=UK-7
      
     ## Create folder and clone branch
     cd /var/www/code/uk
     if [ -d $BRANCH ] 
     then 
        cd $BRANCH
        git reset --hard HEAD
        git checkout $BRANCH
      else
        git clone git@bitbucket.org:fantasticsoftware/uk.git -b $BRANCH 
     fi
      
     # Build API V1
     cd /var/www/code/uk/$BRANCH/apiV1
     composer install --ignore-platform-reqs
     # Copy API from deploy to dev server
     rsync --delete -arv --exclude-from=/var/www/deploys/rsyncExcludeApi.txt ./ 45.79.13.251:/var/www/code/uk/apiV1/
      
     # Build API V4
     cd /var/www/code/uk/$BRANCH/apiV4
     composer install --ignore-platform-reqs
     # Copy API from deploy to dev server
     rsync --delete -arv --exclude-from=/var/www/deploys/rsyncExcludeApi.txt ./ 45.79.13.251:/var/www/code/uk/apiV4/
      
     # Build admin site
     cd /var/www/code/uk/$BRANCH/admin
     yarn install
     bower install
     gulp buildDev
     # Copy admin from deploy to dev server
     rsync --delete -arv --exclude-from=/var/www/deploys/rsyncExcludeFrontEnd.txt dist/ 45.79.13.251:/var/www/code/uk/admin/SQ-admin
      
     ## Build website, document root of UK site
     cd /var/www/code/uk/$BRANCH/website
     yarn install
     bower install
     gulp buildProd
     rsync --delete -arv --exclude-from=/var/www/deploys/rsyncExcludeFrontEnd.txt dist/ 45.79.13.251:/var/www/code/uk/website

## /var/www related files and folders
 * configs: holds a copy of the configuration file repository, which also has the configuration files of the host under its dev subfolder
 * deploys: holds custom scripts to maintain the hosts, for example snapshots of the database.
 * dev-uk.rioft.com: holds the files for dev-uk which are not part of the source code, for example images and api configuration files.
 * dev-glaze.rioft.com: holds the files for dev-glaze which are not part of the source code, for example images and api configuration files.
 * code/uk: holds the files that are built by bamboo server, that also are the website, it has the following subfolders:
    * apiV4: it has the fourth version of api, it is related with https://dev-uk.rioft.com/api/V4 and https://dev-glaze.rioft.com/api/V4 .
    * apiV1: it has the first version of api, it is related with https://dev-uk.rioft.com/api/ and https://dev-glaze.rioft.com/api/ .
    * frontEnd: it subfolder dist holds the root for https://dev-uk.rioft.com and https://dev-glaze.rioft.com sites.
    * admin: it subfolder SQ-admin holds the files for https://dev-uk.rioft.com/SQ-admin and https://dev-uk.rioft.com/SQ-admin sites.

## Configuration files
This environment follows the configuration policy described on 'FIXME: Insert name and link to configuration policy',

### Apache
This environment is configured at /etc/apache2/sites-enabled/dev-uk.rioft.com.conf

### PHP
FIXME: add php documentation

### MySQL
This environment ukes Percona XtraDB Clukter with minimal modifications, the files resides in /etc/mysql/ and currently just 2 files are different from its original state:

#### percona-xtradb-clukter.conf.d/mysqld.cnf

SQL mode should be empty to avoid issues with quotes symbols 

    sql_mode = 

Dev environment allows connection from Internet, the server should listen in all interfaces

    bind-address = *

Sales Quoter uses UTF-8 encoding for all its table, to be sure about the use of this configuration the following code was added 

    # Set utf8mb4 as default
    character-set-server = utf8mb4
    collation-server = utf8mb4_general_ci
    init-connect = 'set names utf8mb4 collate utf8mb4_general_ci'

Tune based on memory: 

    innodb_buffer_pool_size = 1G
    innodb_log_file_size = 512M
    innodb_flukh_method = O_DIRECT

Due of configuration policy, mysql configuration files really doesn't resides on /etc/mysql, they are store in /var/www/config, Percona XtraDB Cluster doesn't allow read its configuration from symbolic links, this should be enable.

    symbolic-likns = 1

#### percona-xtradb-clukter.conf.d/wsrep.cnf

Percona by default disables few configurations that are incompatible with our tables and queries, for this reason the strict mode has to be changed to:

    pxc_strict_mode=PERMISSIVE

### MySQL backups
The needed values to upload files to S3 bucket and trigger cronitor alarm are located in /etc/mysql-backup/db_1.conf

### Cron jobs
The cron job that triggers automated backups is located at /etc/cron.d/mysql-backup and should be a real file.

### remote-syslog
Remote to syslog allows takes lot of files and push them into papertrail, its configuration file is located at /etc/log_files.yml

### Rsyslog
Remote to syslog, by default, transport all message with a normal priority, this makes impossible to trigger alarms based on a different priority, for this reason, emerg priority logs are pushed directly into papertrail by rsyslog. This is enable by /etc/rsyslog.d/60-emerg2papertrail.conf file.


## Database backup
The process of taking database backups is performed by a cron task following directives from https://bitbucket.org/salesquoterdevelopment/mysql-backup.git.  Every day, at 0 am (dev server time zone), a full copy is uploaded to s3://salesquoter-mysqlbackup-dev and every 15 minutes an incremental backup is uploaded to the same S3 Bucket, both tasks use mysql-backup account. Full backup name follows the format YYYYMMdd-dev-db_1-fullDump.sql.bz2 (Y: Year, M: Month, d: day, H: Hour, m: minute). Incremental backups name follows the format YYYYMMdd-dev-db_1-HHmm-incremental.sql.bz2 (Y: Year, M: Month, d: day, H: Hour, m: minute).

### Manual database restore
Database restore could be done with following steps:

 1. Obtain and install awscli following official documentation: http://docs.aws.amazon.com/cli/latest/userguide/installing.html
 2. Configure aws:

        aws configure set aws_access_key_id USEYOUROWNKEY
        aws configure set aws_secret_access_key USEYOUROWNSECRET

 3. Download the backups files based on date (example Nov 10, 2018)

        mkdir /tmp/backup/base -p
        cd /tmp/backup
        aws s3 cp s3://salesquoter-mysqlbackup-dev/ . --recursive --exclude '*' --include '20181110*'

 4. Decompress full backup and prepare it to receive incremental files

        tar xf *-fullDump.sql.bz2 -C base xtrabackup --prepare --apply-log-only --target-dir=base
        xtrabackup --prepare --apply-log-only --target-dir=base

 5. Decompress and apply incremental backups (you can choose until which time apply)

        for FILE in *incremental.bz2
        do
          mkdir temp
          tar xf $FILE -C temp
          xtrabackup --prepare --apply-log-only --target-dir="base" --incremental-dir=temp
          rm -rf temp
 
 6. Restore backup 

        sudo service mysql stop
        sudo mv /var/lib/mysql /var/lib/mysql.old
        sudo xtrabackup --copy-back --target-dir=base
        sudo chown mysql: -R /var/lib/mysql
        sudo service mysql start

## Monitoring and alarms
### Logs
In this host, the following logs are being collected and sent to papertrail using remote_syslog daemon: /var/log/syslog, /var/log/auth.log, /var/log/apache2/access.log, /var/log/apache2/error.log, /var/log/apache2/other_vhosts_access.log, /var/log/mysql/error.log, /var/log/apache2/modsec_audit.log. Also, every message sent to local syslog with prioritie: crit, alert or emerg are sent to papertrail but keeping the priority and facility as same.
Every log from this server could be live watched in https://papertrailapp.com/systems/1953258371/events

### Alarms
#### UptimeRobot
Checks that the services URL give a 20* status when they are visited, also verifies about SSL state, the URLs under monitoring are:
  * Main page: https://uptimerobot.com/dashboard.php#781467722
  * Admin page: https://uptimerobot.com/dashboard.php#781468057

#### Cronitor
This service checks that periodic tasks on servers are started and finalized, currently checks jukt backup tasks:
  * Full backup: https://cronitor.io/dashboard?search=ribIic
  * Incremental backup: https://cronitor.io/dashboard?search=vEnI4j
