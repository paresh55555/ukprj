var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const VENDOR_LIBS = [
    'autobind-decorator', 'axios', 'classnames', 'history', 'pubsub-js', 'react',
    'react-addons-css-transition-group', 'react-bootstrap', 'react-color', 'react-contenteditable',
    'react-cropper', 'react-dom', 'react-dropzone', 'react-redux', 'react-router', 'react-router-bootstrap',
    'react-router-redux', 'redux', 'redux-thunk', 'sweetalert',
];

module.exports = {
    entry: {
        bundle: './master/jsx/App',
        vendor: VENDOR_LIBS,
    },
    output: {
        path: path.resolve(__dirname, 'SQ-admin/'),
        publicPath: '/',
        filename: '[name].[hash].js',
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['vendor', 'manifest'],
        }),
        new HtmlWebpackPlugin({
            template: 'master/index.html',
        }),
        new ExtractTextPlugin('css/style.css'),
    ],
    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loaders: ['react-hot-loader']
        }, {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
                presets: ['es2015', 'react', "stage-1"],
                compact: false
            }
        }, {
            test: /\.sass$/,
            loader: ExtractTextPlugin.extract({
                fallbackLoader: "style-loader",
                loader: "css-loader!sass-loader",
            })
        },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                    loader: 'css-loader',
                })

            }, {
                test: /\.woff|\.woff2|\.svg|.eot|\.ttf|\.gif/,
                loader: 'url?prefix=font/&limit=10000'
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {limit: 40000}
                    },
                    'image-webpack-loader'
                ]
            }]
    },
    node: {
        fs: "empty"
    }
};