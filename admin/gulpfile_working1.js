var args = require('yargs').argv,
    path = require('path'),
    fs = require('fs'),
    del = require('del'),
    gulp = require('gulp'),
    gutil = require('gulp-util'),
    $ = require('gulp-load-plugins')(),
    webpackStream = require('webpack-stream'),
    gulpsync = $.sync(gulp),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    historyApiFallback = require('connect-history-api-fallback'),
    PluginError = $.util.PluginError,
    webpackDevMiddleware = require('webpack-dev-middleware'),
    webpackHotMiddleware = require('webpack-hot-middleware'),
    webpack = require('webpack'),
    webpack_stream = require('webpack-stream'),
    WebpackDevServer = require("webpack-dev-server");

var requireDir  = require( 'require-dir' );
var configs = require('./gulp/configs');


requireDir( './gulp/', { recurse: true } );

// Configs that might go away. 

// var plugins = require('gulp-load-plugins')();    
// production mode (see build task)
// Example:
//    gulp --prod
var isProduction = !!args.prod;


if (isProduction)
    log('Starting production build...');

// styles sourcemaps
var useSourceMaps = false;

// Switch to sass mode.
// Example:
//    gulp --usesass
var useSass = true; // args.usesass // ReactJS project defaults to SASS only

// ignore everything that begins with underscore
var hidden_files = '**/_*.*';
var ignored_files = '!' + hidden_files;

// MAIN PATHS
var paths = {
    app: 'master/',
    dist: 'dist/',
    markup: 'jade/',
    styles: 'sass/',
    scripts: 'jsx/',
    build: 'build/'
}

// if sass -> switch to sass folder
if (useSass) {
    log('Using SASS stylesheets2...');
    paths.styles = 'sass/';
}

// VENDOR CONFIG
var vendor = {
    source: './vendor.json',
    dist: paths.dist + 'vendor',
    bundle: {
        js: 'vendor.bundle.js',
        css: 'vendor.bundle.css'
    }
};

// SOURCES CONFIG
var source = {
    scripts: {
        app: [paths.app + paths.scripts + '**/*.{jsx,js}'],
        entry: [paths.app + paths.scripts + 'App.jsx']
    },
    templates: {
        index: paths.app + 'index.html'
    },
    styles: {
        app: [paths.app + paths.styles + '*.*'],
        themes: [paths.app + paths.styles + 'themes/*', ignored_files],
        watch: [paths.app + paths.styles + '**/*', '!' + paths.app + paths.styles + 'themes/*']
    },
    images: [paths.app + 'img/**/*'],
    fonts: [
        paths.app + 'fonts/*.{ttf,woff,woff2,eof,svg}'
    ],
    serverAssets: [paths.app + 'server/**/*']
};

// BUILD TARGET CONFIG
var build = {
    scripts: paths.dist + 'js',
    styles: paths.dist + 'css',
    images: paths.dist + 'img',
    fonts: paths.dist + 'fonts',
    serverAssets: paths.dist + 'server'
};

// PLUGINS OPTIONS

var vendorUglifyOpts = {
    mangle: {
        except: ['$super'] // rickshaw requires this
    }
};

var cssnanoOpts = {
    safe: true,
    discardUnused: false,
    reduceIdents: false
}

var webpackConfig = require(
    isProduction ?
    './webpack.config.prod' :
    './webpack.config'
);

var bundler = webpack(webpackConfig);

//---------------
// TASKS
//---------------

// Builds a dev version of the site with the JS code running inside 
// of webpack-dev-server for fast compiling of changes
gulp.task('startDev',  gulpsync.sync(['assets', 'copyDevEnv', 'webpack-dev-server']), done);


gulp.task('buildProd',  gulpsync.sync(['cleanBuild', 'cleanDist', 'webpack:build', 'assets' ]), done);


gulp.task('assets', gulpsync.sync([
    'styles:app',
    'styles:app:rtl',
    'styles:themes',
    'fonts',
    'images',
    'templates:index',
    'server-assets',
    'scripts:app',
    'html'
]) );



gulp.task('webpack:build', function (callback) {
    webpack(webpackConfig, function (err, stats) {
        if (err)
            throw new gutil.PluginError('webpack:build', err);
        gutil.log('[webpack:build] Completed\n' + stats.toString({
            assets: true,
            chunks: false,
            chunkModules: false,
            colors: true,
            hash: false,
            timings: false,
            version: false
        }));
        callback();
    });
});

//HTML
gulp.task('html_2', function() {
    log('Copying html..');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./build/*.html')
        .pipe(gulp.dest(paths.dist))
        ;
});

gulp.task('deleteSQ-admin', function() {

    return del(paths.dist+'SQ-admin', {
        force: true // clean files outside current directory
    });

});

gulp.task('addSQ-admin', gulpsync.sync([
    'deleteSQ-admin',
    'copySQ-admin'
]));


gulp.task('copySQ-admin', function() {

    del(paths.dist+'SQ-admin', {
        force: true // clean files outside current directory
    });

    log('Copying SQ-admin files..');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./dist/*')
        .pipe(gulp.dest(paths.dist+'SQ-admin'))
        ;
});


// JS APP
gulp.task('scripts:app_new', function() {
    log('Copying scripts..');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./build/*.js')
        .pipe(gulp.dest(build.scripts));
});



gulp.task('nested', gulpsync.sync([
    'webpack-dev-server',
]), done);




// JS APP
gulp.task('scripts:app_2', function() {
    log('Building scripts..');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src(source.scripts.entry)
        .pipe(
            webpackStream(webpackConfig)
        )
        .on("error", handleError)
        .pipe(gulp.dest(build.scripts));
});

gulp.task("webpack-dev-server_old", function(callback) {
    // Start a webpack-dev-server
    var compiler = webpack(webpackConfig);

    new WebpackDevServer(compiler, {
        // server and middleware options
        historyApiFallback: true
    }).listen(8080, "localhost", function(err) {
        if(err) throw new gutil.PluginError("SQ-admin", err);
        // Server listening
        gutil.log("[webpack-dev-server]", "http://localhost:8080/SQ-admin/index.html");

        // keep the server alive or continue?
        // callback();
    });
});


// VENDOR BUILD
// copy file from bower folder into the app vendor folder
gulp.task('vendor', function() {
    log('Copying vendor assets..');

    var jsFilter = $.filter('**/*.js', {
        restore: true
    });webpackConfig
    var cssFilter = $.filter('**/*.css', {
        restore: true
    });
    var imgFilter = $.filter('**/*.{png,jpg}', {
        restore: true
    });
    var fontsFilter = $.filter('**/*.{ttf,woff,woff2,eof,svg}', {
        restore: true
    });

    var vendorSrc = JSON.parse(fs.readFileSync(vendor.source, 'utf8'));

    return gulp.src(vendorSrc, {
            base: 'bower_components'
        })
        .pipe($.expectFile(vendorSrc))
        .pipe(jsFilter)
        .pipe($.if(isProduction, $.uglify(vendorUglifyOpts)))
        .pipe($.concat(vendor.bundle.js))
        .pipe(gulp.dest(build.scripts))
        .pipe(jsFilter.restore())
        .pipe(cssFilter)
        .pipe($.if(isProduction, $.cssnano(cssnanoOpts)))
        .pipe($.concat(vendor.bundle.css))
        .pipe(gulp.dest(build.styles))
        .pipe(cssFilter.restore())
        .pipe(imgFilter)
        .pipe($.flatten())
        .pipe(gulp.dest(build.images))
        .pipe(imgFilter.restore())
        .pipe(fontsFilter)
        .pipe($.flatten())
        .pipe(gulp.dest(build.fonts));

});

// APP LESS
gulp.task('styles:app', function() {
    log('Building application styles..');
    return gulp.src(source.styles.app)
        .pipe($.if(useSourceMaps, $.sourcemaps.init()))
        .pipe(useSass ? $.sass() : $.less())
        .on("error", handleError)
        .pipe($.if(isProduction, $.cssnano(cssnanoOpts)))
        .pipe($.if(useSourceMaps, $.sourcemaps.write()))
        .pipe(gulp.dest(build.styles))
        .pipe(reload({
            stream: true
        }));
});

// APP RTL
gulp.task('styles:app:rtl_2', function() {
    log('Building application RTL styles..');
    return gulp.src(source.styles.app)
        .pipe($.if(useSourceMaps, $.sourcemaps.init()))
        .pipe(useSass ? $.sass() : $.less())
        .on("error", handleError)
        .pipe($.rtlcss())
        .pipe($.if(isProduction, $.cssnano(cssnanoOpts)))
        .pipe($.if(useSourceMaps, $.sourcemaps.write()))
        .pipe($.rename(function(path) {
            path.basename += "-rtl";
            return path;
        }))
        .pipe(gulp.dest(build.styles))
        .pipe(reload({
            stream: true
        }));
});

// LESS THEMES
gulp.task('styles:themes_2', function() {
    log('Building application theme styles..');
    return gulp.src(source.styles.themes)
        .pipe(useSass ? $.sass() : $.less())
        .on("error", handleError)
        .pipe(gulp.dest(build.styles))
        .pipe(reload({
            stream: true
        }));
});

gulp.task('fonts_2', function() {
    return gulp.src(source.fonts)
        .pipe($.flatten())
        .pipe(gulp.dest(build.fonts))
})

gulp.task('images_2', function() {
    return gulp.src(source.images)
        .pipe(gulp.dest(build.images))
})

gulp.task('server-assets_2', function() {
    return gulp.src(source.serverAssets)
        .pipe(gulp.dest(build.serverAssets))
})

gulp.task('templates:index_2', function() {
    return gulp.src(source.templates.index)
        .pipe(gulp.dest(paths.dist))
})


//---------------
// WATCH
//---------------

// Rerun the task when a file changes
gulp.task('watch', function() {
    log('Watching source files..');

    gulp.watch(source.scripts.app, ['scripts:app_new']);
    gulp.watch(source.styles.watch, ['styles:app', 'styles:app:rtl']);
    gulp.watch(source.styles.themes, ['styles:themes']);
    gulp.watch(source.templates.index, ['templates:index']);
    gulp.watch(vendor.source, ['vendor']);

});

// Serve files with auto reaload
gulp.task('browsersync', function() {
    log('Starting BrowserSync..');

    var middlewares = [historyApiFallback()];

    if (!isProduction) {
        middlewares = middlewares.concat([
            webpackDevMiddleware(bundler, {
                publicPath: webpackConfig.output.publicPath,
                stats: {
                    colors: true
                },
                historyApiFallback: {
                    index: 'index.html'
                },
                historyApiFallback: true

            }),
            webpackHotMiddleware(bundler)
        ])
    }

    browserSync({
        notify: false,
        online: true,
        server: {
            baseDir: paths.dist,
            middleware: middlewares
        },
        files: [source.scripts.app]
    });

});

//---------------
// MAIN TASKS
//---------------

// build for production (no watch)
gulp.task('build', gulpsync.sync([
    'vendor',
    'assets'
]));

gulp.task('frackaLite', gulpsync.sync([
    'vendor',
    'assets'
]), done);

gulp.task('fracka', gulpsync.sync([
    'webpack:build',
    'vendor',
    'assets'
]), done);


// Server for development
gulp.task('serve', gulpsync.sync([
    'default',
    'browsersync'
]), done);

// build with sourcemaps (no minify)
gulp.task('sourcemaps', ['usesources', 'default']);
gulp.task('usesources', function() {
    useSourceMaps = true;
});

// default (no minify)
gulp.task('default', gulpsync.sync([
    'vendor',
    'assets',
    'watch'
]));



gulp.task('assets_broken', [
    'scripts:app'
]);




/////////////////////

function done() {
    log('************');
    log('* All Done * You can start editing your code, BrowserSync will update your browser after any change..');
    log('************');
}

// Error handler
function handleError(err) {
    log(err.toString());
    this.emit('end');
}

// log to console using
function log(msg) {
    $.util.log($.util.colors.blue(msg));
}