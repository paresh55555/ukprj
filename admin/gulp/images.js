// images.js

var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('images', function() {
    return gulp.src(configs.source.images)
        .pipe(gulp.dest(configs.build.images))
})