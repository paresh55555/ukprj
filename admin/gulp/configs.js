//configs.js
var args = require('yargs').argv;

var webpack = require('webpack');
$ = require('gulp-load-plugins')();

var exports = module.exports = {};


exports.log = function(msg) {
  $.util.log($.util.colors.blue(msg));
};

exports.handleError = function(err) {
   exports.log(err.toString());
    this.emit('end');
};

exports.location = 'dist';

exports.paths =  {
    app: 'master/',
    dist: 'dist/',
    markup: 'jade/',
    styles: 'sass/',
    scripts: 'jsx/',
    build: 'build/',
    prod: 'SQ-admin/'
}

exports.build = {
    scripts: exports.paths.dist + 'js',
    styles: exports.paths.dist + 'css',
    images: exports.paths.dist + 'img',
    fonts: exports.paths.dist + 'fonts',
    serverAssets: exports.paths.dist + 'server'
};

exports.cssnanoOpts = {
    safe: true,
    discardUnused: false,
    reduceIdents: false
}

exports.hidden_files = '**/_*.*';
exports.ignored_files = '!' + exports.hidden_files;

exports.isProduction = !!args.prod;
exports.api = (args.api === undefined) ? 'https://dev-demo1.rioft.com/api/V4' : args.api;


exports.source = {
    scripts: {
        app: [exports.paths.app + exports.paths.scripts + '**/*.{jsx,js}'],
        entry: [exports.paths.app + exports.paths.scripts + 'App.jsx']
    },
    templates: {
        index: exports.paths.app + 'index.html'
    },
    styles: {
        app: [exports.paths.app + exports.paths.styles + '*.*'],
        themes: [exports.paths.app + exports.paths.styles + 'themes/*', exports.ignored_files],
        watch: [exports.paths.app + exports.paths.styles + '**/*', '!' + exports.paths.app + exports.paths.styles + 'themes/*'],
        webpack: [exports.paths.app + exports.paths.build + '*.css']

    },
    images: [exports.paths.app + 'img/**/*'],
    fonts: [
        exports.paths.app + 'fonts/*.{ttf,woff,woff2,eof,svg}'
    ],
    serverAssets: [exports.paths.app + 'server/**/*']
};

// styles sourcemaps
exports.useSourceMaps = false;

exports.useSass = true; // args.usesass // ReactJS project defaults to SASS only

exports.webpackConfig = require(
    exports.isProduction ?
    '../webpack.config' :
    '../webpack.config'
);

exports.bundler = webpack(exports.webpackConfig);

exports.vendorUglifyOpts = {
    mangle: {
        except: ['$super'] // rickshaw requires this
    }
};

exports.cssnanoOpts = {
    safe: true,
    discardUnused: false,
    reduceIdents: false
}



