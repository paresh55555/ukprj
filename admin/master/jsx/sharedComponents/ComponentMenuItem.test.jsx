import React from 'react';
import ComponentMenuItem from '../pages/components/ComponentMenuItem';
import { shallow } from 'enzyme';

describe('ComponentMenuItem', () => {
    it('shallow renders as expected', () => {
        const componentMenuItem = shallow(<ComponentMenuItem />).html();
        expect(componentMenuItem).toMatchSnapshot();
    });
})