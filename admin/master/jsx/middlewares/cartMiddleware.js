import * as types from '../actions/actionTypes';


export default store => next => action => {
    if (action.type === types.SET_CURRENT_ELEMENT) {
        sessionStorage.setItem('currentCart', JSON.stringify(action.data));
    }
    if (action.type === types.SET_CURRENT_LIST_TYPE) {
        sessionStorage.setItem('currentListType', JSON.stringify(action.data));
    }
    if (action.type === types.CHANGE_CURRENT_ELEMENT_NAME) {
        const currentCart = JSON.parse(sessionStorage.getItem('currentCart'));
        currentCart.name = action.data;
        sessionStorage.setItem('currentCart', JSON.stringify(currentCart));
    }

    return next(action)
}