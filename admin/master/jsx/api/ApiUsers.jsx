import { routerActions } from 'react-router-redux';
import * as constant from '../constant';
import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import { userAction } from '../actions';
import { CACHE_USERS_LIST } from './ApiCache';
import { API_URL } from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;

/**
 * Caches the getUsers API calls or immediate initial return.
 * @param listOfUsers
 */

function cacheUsersList(listOfUsers) {
    localStorage.setItem(CACHE_USERS_LIST, JSON.stringify(listOfUsers));
}

/**
 * Build the correct headers and options to make the API call.
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

function buildUserOptions(method = 'get', data, urlString = '') {
    const token = sessionStorage.getItem('token');
    return {
        method,
        url: `${API_URL}/users${urlString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
        data: data || null,
    };
}

/**
 * Return the cache list of user if available immediately to the store while waiting for the api to update
 */

function returnCacheListOfUsers() {
    const listOfUsers = JSON.parse(localStorage.getItem(CACHE_USERS_LIST));
    if (listOfUsers && listOfUsers.errorMessage === undefined && listOfUsers.length > 0) {
        dispatch(userAction.receiveListOfUsers(listOfUsers));
    }
}

/** api methods*/

export function apiGetUsers() {
    returnCacheListOfUsers();
    dispatch(userAction.prepUserTableForData());
    const options = GenerateOptions('users');
    return MakeTheApiCall(options).then((response) => {
        const listOfUsers = response.data;
        dispatch(userAction.receiveListOfUsers(listOfUsers));
        cacheUsersList(listOfUsers);
    });
}

export function apiSaveUser(user) {
    dispatch(userAction.requestData());
    const data = JSON.stringify(user);
    const options = GenerateOptions('users', 'POST', data);
    return MakeTheApiCall(options).then(
        (response) => {
            dispatch(userAction.dataSaved(response.data));
        },
    );
}

export function apiUpdateUser(user) {
    dispatch(userAction.requestData());
    const data = JSON.stringify(user);
    const options = GenerateOptions('users', 'PUT', data);
    return MakeTheApiCall(options).then(
        (response) => {
            dispatch(userAction.dataSaved(response.data));
        },
    );
}

export function apiGetUserData(id) {
    dispatch(userAction.requestData());
    const options = GenerateOptions(`users/${id}`, 'GET');
    return MakeTheApiCall(options).then(
        (response) => {
            if (Object.keys(response.data).length === 0 || id !== response.data.id) {
                dispatch(routerActions.replace(`${constant.SQ_PREFIX}notfound`));
            }
            dispatch(userAction.receiveUserData(response.data));
        },
    );
}

export function apiDeleteUser(id) {
    dispatch(userAction.requestData());
    const options = GenerateOptions(`users/${id}`, 'DELETE');

    console.log(options)

    return MakeTheApiCall(options).then(
        (response) => {
            if (Object.keys(response.data).length === 0 || id.length !== (`${response.data.id}`).length) {
                dispatch(routerActions.replace(`${constant.SQ_PREFIX}`));
            }
            dispatch(userAction.deleteData(response.data));
        },
    );
}
