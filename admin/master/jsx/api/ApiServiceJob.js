import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {serviceAction, serviceJobAction} from '../actions';
import store from '../store';

const {dispatch} = store;

export function ApiGetJobs(currentID){
    
    var options = GenerateOptions('service/jobs', 'GET');

    dispatch(serviceJobAction.getServiceJobs())
    
    return MakeTheApiCall(options).then(response => {
    
        dispatch(serviceJobAction.setServiceJobs(response.data.data));
        if(currentID){
            dispatch(serviceJobAction.setCurrentJob(response.data.data.find(job=>job.id==currentID)))
        }

        return response
    });
}

export function ApiUpdateJob(data) {

    var options = GenerateOptions(`service/jobs/${data.id}`, 'PUT', data);

    return MakeTheApiCall(options);
}

export function ApiGetServiceTechList(){

    var options = GenerateOptions('services/jobs/techician', 'GET');

    MakeTheApiCall(options).then(response => {

        dispatch(serviceJobAction.setServiceTechList(response.data.data));

    });
}

export function ApiGetJobCustomers(job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/customers`, 'GET');

    MakeTheApiCall(options).then(response => {
    	
        dispatch(serviceJobAction.getJobCustomer(response.data.data));

    });
}

export function ApiAddJobCustomer(data,job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/customers`, 'POST',data);

    MakeTheApiCall(options).then(response => {
        
        ApiGetJobCustomers(job_id)

    });
}

export function ApiUpdateJobCustomer(data,job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/customers/${data.id}`, 'PUT',data);

    MakeTheApiCall(options).then(response => {
        
    });
}
export function ApiDeleteJobCustomer(customers_id,job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/customers/${customers_id}`, 'DELETE');

    MakeTheApiCall(options).then(response => {
        dispatch(serviceJobAction.deleteJobCustomer(customers_id))
    });
}

export function ApiGetJobDoors(jobId) {

    var options = GenerateOptions(`service/jobs/${jobId}/doors`, 'GET');

    dispatch(serviceAction.getDoors());

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setDoors(response.data.data));

    });
}

export function ApiGetJobAddress(job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/address`, 'GET');

    MakeTheApiCall(options).then(response => {
       
        dispatch(serviceJobAction.getJobAddress(response.data.data));

    });
}

export function ApiAddJobAddress(data,job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/address`, 'POST',data);

    MakeTheApiCall(options).then(response => {
        
        ApiGetJobAddress(job_id)

    });
}

export function ApiUpdateJobAddress(data,job_id) {

    var options = GenerateOptions(`service/jobs/${job_id}/address/${data.id}`, 'PUT',data);

    return MakeTheApiCall(options).then(response => {
        return ApiGetJobAddress(job_id)
    });
}

export function ApiSearchJob(searchQuery){

    var options = GenerateOptions(`service/contacts/search`, 'POST', {search:searchQuery});
    
    return MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setSearchResults(response.data.data));

    });
}

export function ApiDeleteJobAddress(address_id,job_id) {

    var options = GenerateOptions(`/service/jobs/${job_id}/address/${address_id}`, 'DELETE');

    return MakeTheApiCall(options).then(response => {
        
        dispatch(serviceJobAction.deleteJobAddress(address_id))
        return response
    });
}

export function ApiGetAttachedDoors(orderId){

    var options = GenerateOptions(`service/orders/${orderId}/doors`, 'GET');

    return MakeTheApiCall(options).then(response => {

        dispatch(serviceJobAction.setAttachedDoors(response.data.data));
        return response
    });
}

export function ApiAddJobDoor(jobId, data){

    var options = GenerateOptions(`service/jobs/${jobId}/doors`, 'POST', data);

    MakeTheApiCall(options).then(response => {

        ApiGetJobDoors(jobId);

        return response

    });
}

export function ApiDeleteJobDoor(jobId, doorId){

    var options = GenerateOptions(`service/jobs/${jobId}/doors/${doorId}`, 'DELETE');

    MakeTheApiCall(options).then(response => {

        ApiGetJobDoors(jobId);

    });
}

export function ApiGetJobStatus(orderId){

    var options = GenerateOptions(`service/jobs/status`, 'GET');

    return MakeTheApiCall(options).then(response => {

        dispatch(serviceJobAction.setJobsStatus(response.data.data));
        return response
    });
}

export function ApiGetJobFiles(job_id){
   
    var options = GenerateOptions(`service/jobs/${job_id}/file`, 'GET');

    return MakeTheApiCall(options).then(response => {

        dispatch(serviceJobAction.setCurrentJobFiles(response.data.data));
       return response

    });
}

export function ApiAddJobFiles(JobId, file, uploadProgress) {
    let data = new FormData()

    data.append('file', file)

    var options = GenerateOptions(`service/jobs/${JobId}/file`, 'POST', data, uploadProgress);

    return MakeTheApiCall(options).then(response => {

        ApiGetJobFiles(JobId)
        return response

    });
}

export function ApiDeleteJobFile(jobId, file_id){

    var options = GenerateOptions(`service/jobs/${jobId}/file/${file_id}`, 'DELETE');

    return MakeTheApiCall(options).then(response => {

        ApiGetJobFiles(jobId)
        return response

    });
}

export function ApiGetJobActivity(JobId){
   
    var options = GenerateOptions(`service/logs/${JobId}/activity_log/jobs`, 'GET');

    return MakeTheApiCall(options).then(response => {

        dispatch(serviceJobAction.setCurrentJobActivity(response.data.data));
       return response

    });
}


export function ApiGetSendEmailDetail(JobId){
   
    var options = GenerateOptions(`service/jobs/${JobId}/sendEmail`, 'GET');
    
    return MakeTheApiCall(options).then(response => {

        dispatch(serviceJobAction.setSendEmailDetails(response.data.data));
       return response

    });
}

export function ApiSendEmail(JobId, data){

    let newData = new FormData()

    // if (data.file instanceof Array) {
    newData.append('file', [data.file])
    // }

    newData.append('message', data.message)
    newData.append('to', data.to)
    newData.append('subject', data.subject)

    var options = GenerateOptions(`service/jobs/${JobId}/sendEmail`, 'POST', newData);

    return MakeTheApiCall(options).then(response => {

        return response

    });
}