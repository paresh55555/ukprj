import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {optionTraitAction} from '../actions';
import {API_URL} from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;

/**
 * Save multiple option traits.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiSaveMultipleOptionTraits(systemId, componentId, optionId, deletedTraits, traits) {

    dispatch(optionTraitAction.savingData());

    let deleteRequests = [];
    let optionUrl = `/systems/${systemId}/components/${componentId}/options/${optionId}`
    let _saveTraits = function(traits){
        let data = JSON.stringify(traits)
        var options = GenerateOptions(`${optionUrl}/multipleTraits`, 'POST', data);
        return MakeTheApiCall(options).then((response) => {
            successReturnOptionTraitSaved(response.data.response_data,optionId);
        })
    };
    if (deletedTraits && deletedTraits.length) {
        deletedTraits.map((trait) => {
            if (trait.id) {
                let traitId = trait.id;
                deleteRequests.push(
                    MakeTheApiCall(GenerateOptions(`${optionUrl}/traits/${traitId}`, 'DELETE', false))
                )
            }
        });
    }
    if (deleteRequests.length) {
        return MakeTheApiCall(deleteRequests, true).then(function(res){
            return _saveTraits(traits);
        })
    } else {
        return _saveTraits(traits);
    }
}

/**
 * Save option trait.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiSaveOptionTrait(systemId, componentId, optionId, data) {

    var options = GenerateOptions(`/systems/${systemId}/components/${componentId}/options/${optionId}/traits`, 'POST', data);

    dispatch(optionTraitAction.savingData());

    return MakeTheApiCall(options).then((response) => {

        var trait = response.data && response.data.response_data;
        successReturnOptionTraitSaved(trait, trait[0].optionId);
        return trait && trait[0]

    });
}

function successReturnOptionTraitSaved (responseData,optionId) {
    dispatch(optionTraitAction.dataSaved(responseData,optionId))
}