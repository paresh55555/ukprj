import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {serviceAction} from '../actions';
import store from '../store';
import {setCurrentOrder, setSearchResults} from "../actions/service";

const {dispatch} = store;

/**
 * Get Service orders from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiGetOrders() {

    var options = GenerateOptions('service/orders', 'GET');

    dispatch(serviceAction.getServiceOrders())

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setServiceOrders(response.data.data));

    });
}


/**
 * Add new order
 * @constructor
 */
export function ApiAddOrder(orderNumber) {
    let data = {
        order_number: orderNumber,
        warranty: 0,
        warranty_length: "",
        warranty_end_date: "",
        complete_date: "",
        notes: []
    };
    var options = GenerateOptions(`service/orders`, 'POST', data);

    return MakeTheApiCall(options).then(response => {

        if(response.data.data && response.data.data.length){
            data.id = response.data.data[0].id
            dispatch(serviceAction.setCurrentOrder(data))
            dispatch(serviceAction.addOrder(data));
        }

    });
}

/**
 * Update existing order data
 * @param id
 * @param data
 * @constructor
 */
export function ApiUpdateOrder(id, data) {

    var options = GenerateOptions(`service/orders/${id}`, 'PUT', data);

    return MakeTheApiCall(options).then(response => {

        if(response.data.data && response.data.data.length) {
            dispatch(serviceAction.updateOrder(response.data.data[0]));
        }

    });
}

export function ApiGetOrderDoors(orderID) {

    var options = GenerateOptions(`service/orders/${orderID}/doors`, 'GET');

    dispatch(serviceAction.getDoors())

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setDoors(response.data.data));

    });
}

export function ApiGetDoorTypes(category) {

    var options = GenerateOptions(`services/doorTypes`, 'GET', {"category": category});

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setDoorTypes(response.data.data));

    });
}

export function ApiGetTraitTypes() {
    var options = GenerateOptions(`service/traitTypes`, 'GET');
    const TRAITS_DATA = [
        'Frame',
        'Track',
        'Exterior Color',
        'Interior Color',
    ]
    dispatch(serviceAction.setTraitTypes(TRAITS_DATA))
    // MakeTheApiCall(options).then(response => {
    //
    //     dispatch(serviceAction.setTraitTypes(response.data.data));
    //
    // });
}

export function getSites() {
    const SITES_DATA = [
        'CES',
        'PD',
        'OLD'
    ]
    dispatch(serviceAction.setSites(SITES_DATA))
}

export function ApiGetOrderPurchasers(orderId) {

    var options = GenerateOptions(`service/orders/${orderId}/customers`, 'GET');

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setPurchasers(response.data.data));

    });
}

export function ApiAddOrderPurchaser(orderId, data) {
    var options = GenerateOptions(`service/orders/${orderId}/customers`, 'POST', data);

    MakeTheApiCall(options).then(response => {

        if(response.data.data && response.data.data.length) {
            data.id = response.data.data[0].id
            dispatch(serviceAction.addPurchaser(data));
        }

    });
}

export function ApiUpdateOrderPurchaser(orderId, data) {
    var options = GenerateOptions(`service/orders/${orderId}/customers/${data.id}`, 'PUT', data);
    MakeTheApiCall(options).then(response => {

        if(response.data.data && response.data.data.length) {
            dispatch(serviceAction.updatePurchaser(data));
        }

    });
}

export function ApiDeleteOrderPurchaser(orderId, purchaserId){
    var options = GenerateOptions(`/service/orders/${orderId}/customers/${purchaserId}`, 'DELETE');

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.deletePurchaser(purchaserId))

    });
}

export function ApiGetDocTypes() {
    var options = GenerateOptions(`service/orders/docs_types`, 'GET');

    MakeTheApiCall(options).then(response => {

        if(response.data.data && response.data.data.length){

            dispatch(serviceAction.setDocTypes(response.data.data));

        }

    });
}

export function ApiGetOrderDocs(id){
    var options = GenerateOptions(`service/orders/${id}/docs`, 'GET');
    MakeTheApiCall(options).then(response => {
        dispatch(serviceAction.setOrderDocs(response.data.data));
    
    });
}
export function ApiDeleteOrderDocs(orderId,id){
    var options = GenerateOptions(`service/orders/${orderId}/docs/${id}`, 'DELETE');
    MakeTheApiCall(options).then(response => {
        dispatch(serviceAction.removeDoc(id));
    
    });
}


export function ApiAddDoorNote(orderId, doorId, data){
    var options = GenerateOptions(`/service/orders/${orderId}/doors/${doorId}/notes`, 'POST', data);
    MakeTheApiCall(options).then(response => {
        dispatch(serviceAction.addDoorNote(doorId, response.data.data[0]));
    
    });
}
export function ApiAddDoor(orderId, data){

    var options = GenerateOptions(`service/orders/${orderId}/doors`, 'POST', data);

    return MakeTheApiCall(options).then(response => {

        if(response.data.data && response.data.data.length) {
            data.id = response.data.data[0].id;
            data.order_id = response.data.data[0].order_id;
            data.notes=[],
            data.customers=[]
            dispatch(serviceAction.addDoor({...data, ...response.data.data[0]}));
        }

    });
}

export function ApiUpdateDoor(orderId, doorId, data){

    var options = GenerateOptions(`service/orders/${orderId}/doors/${doorId}`, 'PUT', data);

    return MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.updateDoor(data));

    });
}

export function ApiDeleteDoor(orderId, doorId, data){

    var options = GenerateOptions(`service/orders/${orderId}/doors/${doorId}`, 'DELETE', data);

    return MakeTheApiCall(options).then(response => {

        ApiGetOrderDoors(orderId)

    });
}

export function ApiDeleteDoorNote(orderId,doorId, noteId){
    var options = GenerateOptions(`/service/orders/${orderId}/doors/${doorId}/notes/${noteId}`, 'DELETE');    
    MakeTheApiCall(options).then(response => {
        dispatch(serviceAction.deleteDoorNote(doorId, noteId))
    });
}

export function ApiUpdateDoorNote(orderId, doorId, noteId, data){
    var options = GenerateOptions(`/service/orders/${orderId}/doors/${doorId}/notes/${noteId}`, 'PUT', data);    
    MakeTheApiCall(options).then(response => {
        dispatch(serviceAction.upadateDoorNote(doorId, response.data.data[0]));
    });
}

export function ApiAddDoorCustomer(orderId, doorId, data){
    var options = GenerateOptions(`service/orders/${orderId}/doors/${doorId}/customers`, 'POST', data);
    // dispatch(serviceAction.addDoorCustomer(doorId, data))
    MakeTheApiCall(options).then(response => {
        data.id = response.data.data[0].id;
        dispatch(serviceAction.addDoorCustomer(doorId, data));
    
    });
}

export function ApiUpdateDoorCustomer(orderId, doorId, data){
    var options = GenerateOptions(`service/orders/${orderId}/doors/${doorId}/customers/${data.id}`, 'PUT', data);
    // dispatch(serviceAction.addDoorCustomer(doorId, data))
    MakeTheApiCall(options).then(response => {
        data.id = response.data.data[0].id;
       // dispatch(serviceAction.updatedDoorCustomer(doorId, data));
    
    });
}


export function ApiDeleteDoorCustomer(orderId,doorId, id){
    var options = GenerateOptions(`service/orders/${orderId}/doors/${doorId}/customers/${id}`, 'DELETE');
    MakeTheApiCall(options).then(response => {
        dispatch(serviceAction.deleteDoorCustomer(doorId,id));
    
    });
}

export function ApiCreateDocType(data){
    var options = GenerateOptions(`services/docTypes`, 'POST', data);
    return MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.addDocType(response.data));
        return response.data;
    });
}

export function ApiCreateDoorType(data){

    var options = GenerateOptions(`services/doorTypes`, 'POST', data);

    return MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.addDoorType(response.data));
        return response.data;
    });
}

export function ApiAddDoc(orderId, data, uploadProgress){
    var options = GenerateOptions(`service/orders/${orderId}/docs`, 'POST', data, uploadProgress);
    return MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.addDoc(response.data));

    });
}

export function ApiSearchCustomer(searchQuery){
    var options = GenerateOptions(`service/customers/search`, 'POST', {search:searchQuery});
    return MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setSearchResults(response.data.data));

    });
}

export function ApiCreateDoorJob(door_id){
    var options = GenerateOptions(`service/door/jobs`, 'POST', {door_id});
    return MakeTheApiCall(options)
}


export function ApiGetOrderActivityLog(orderId) {

    var options = GenerateOptions(`service/logs/${orderId}/activity_log/orders`, 'GET');

    MakeTheApiCall(options).then(response => {

        dispatch(serviceAction.setOrderActivity(response.data.data));

    });
}