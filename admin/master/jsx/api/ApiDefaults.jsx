import { routerActions } from 'react-router-redux';
import * as constant from '../constant';
import MakeTheApiCall from './ApiCalls';
import { defaultsAction } from '../actions';
import { CACHE_USERS_LIST } from './ApiCache';
import { API_URL } from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;

/**
 * Caches the getUsers API calls or immediate initial return.
 * @param listOfUsers
 */

function cacheUsersList(listOfUsers) {
    localStorage.setItem(CACHE_USERS_LIST, JSON.stringify(listOfUsers));
}

/**
 * Build the correct headers and options to make the API call.
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

function buildOptions(method = 'get', data, urlString = '') {
    const token = sessionStorage.getItem('token');
    return {
        method,
        url: `${API_URL}/default${urlString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
        data: data || null,
    };
}

/** api methods*/

export function getSiteDefults() {
    dispatch(defaultsAction.requestDefaults());
    const options = buildOptions('get', null, '/site');
    return MakeTheApiCall(options).then((response) => {
        dispatch(defaultsAction.saveSiteDefaults(response.data));
    });
}

export function changeSiteDefults(id, data) {
    dispatch(defaultsAction.requestDefaults());
    const options = buildOptions('put', data, `/site/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(defaultsAction.changeSiteDefaultsAction(response.data.id, response.data));
    });
}

export function getCustomerDefults() {
    dispatch(defaultsAction.requestDefaults());
    const options = buildOptions('get', null, '/customer');
    return MakeTheApiCall(options).then((response) => {
        dispatch(defaultsAction.saveCustomerDefaults(response.data.data));
    });
}

export function changeCustomerDefults(id, data) {
    dispatch(defaultsAction.requestDefaults());
    const options = buildOptions('put', data, `/customer/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(defaultsAction.changeCustomerDefaultsAction(response.data.id, response.data));
    });
}
