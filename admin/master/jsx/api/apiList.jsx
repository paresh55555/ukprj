import MakeTheApiCall from './ApiCalls';
import { listAction } from '../actions';
import { CACHE_USERS_LIST } from './ApiCache';
import { API_URL } from './ApiServerConfigs';
import store from '../store';
import {NotifyAlert} from '../components/Common/notify.js'


const { dispatch } = store;

/**
 * Caches the getUsers API calls or immediate initial return.
 * @param listOfUsers
 */

function cacheUsersList(listOfUsers) {
    localStorage.setItem(CACHE_USERS_LIST, JSON.stringify(listOfUsers));
}

/**
 * Build the correct headers and options to make the API call.
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

function buildOptions(method = 'get', data, urlString = '') {
    const token = sessionStorage.getItem('token');
    return {
        method,
        url: `${API_URL}/listBuilder${urlString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
        data: JSON.stringify(data) || null,
    };
}

/** api methods*/

export function getListBuilderApi() {
    const options = buildOptions();
    return MakeTheApiCall(options).then((response) => {
        dispatch(listAction.getListBuilder(response.data));
    });
}


export function addListBuilderApi(data) {
    const options = buildOptions('POST',data);
    return MakeTheApiCall(options)
}

export function getListBuildeDetailsApi(id) {
    dispatch(listAction.setListLoadingStatus(true))
    const options = buildOptions('GET',{}, `/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(listAction.setBuilderDetails(response.data))
    });
}

export function ApiDeleteListBuilder(id){
    const options  = buildOptions('DELETE',{},`/${id}`)
    return MakeTheApiCall(options)
}

export function getAvailable(type){
    const options = buildOptions('GET',{}, `/available/${type}s`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(listAction.getListColumn(response.data.data))
    });
}
export function saveBuilder(data,id) {
    const options = buildOptions('PUT',data, `/${id}`);
    dispatch(listAction.setSavingStatus(true))
    return MakeTheApiCall(options).then((response) => {
        NotifyAlert('List builder has been updated')
        dispatch(listAction.setSavingStatus(false))
        return response;

    });
}
export function getSampleDatasApi() {
    const options = buildOptions('GET',{}, `/available/sampleData`);
    return MakeTheApiCall(options).then((response) => {
       dispatch(listAction.setSampleData(response.data.data))
    });
}