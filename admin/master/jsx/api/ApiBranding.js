
import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {brandingAction} from '../actions';
import store from '../store';

const { dispatch } = store;

/**
 * Get the latest Branding from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiGetBranding() {

    var options = GenerateOptions('siteBranding', 'GET');

    dispatch(brandingAction.requestData())

    MakeTheApiCall(options).then(response => {

        var Branding = response.data.data[0];
        successReturnBranding(Branding);

    });
}

/**
 * Delete the Branding from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiDeleteBranding() {

    var options = GenerateOptions('siteBranding', 'DELETE');

    dispatch(brandingAction.deletingData())

    MakeTheApiCall(options).then(response => dispatch(brandingAction.deletedData()));
}

/**
 * Get the latest Branding from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiUpdateBranding(branding) {

    var options = GenerateOptions('siteBranding', 'PUT', branding);

    dispatch(brandingAction.savingtData())

    MakeTheApiCall(options).then(response => {

        var Branding = response.data.data[0];
        successReturnBranding(Branding);

    });
}

/**
 * Return the Branding
 * @param dispatch
 * @param Branding
 */

function successReturnBranding (Branding) {
    dispatch(brandingAction.receiveData(Branding))
}



