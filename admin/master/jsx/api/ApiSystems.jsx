import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {systemAction} from '../actions';
import store from '../store';
import axios from 'axios';
const { dispatch } = store;

/**
 * Get the latest PreMadeSystems from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiGetPreMadeSystems() {

    var options = GenerateOptions('systemGroups/preMade', 'GET');

    return MakeTheApiCall(options).then(response => {

        var listOfPreMadeSystems = response.data.response_data;
        successReturnPreMadeSystem(listOfPreMadeSystems);

        return response.data

    });
}

export function getSystemGroupsList() {

    var options = GenerateOptions('systemGroups', 'GET');

    dispatch(systemAction.requestData());

    MakeTheApiCall(options).then(response => {

        var listOfSystemGroups = response.data.response_data;
        dispatch(systemAction.receiveData(listOfSystemGroups));

    });

}

export function getSampleDoorPrice(id) {

    var options = GenerateOptions(`systems/${id}/sampleDoorPrice`, 'GET');

    MakeTheApiCall(options).then(response => {

        var doorPrice = response.data.response_data;
        dispatch(systemAction.recievedSampleDoors(doorPrice));

    });

}

export function getSampleDoorParts(id) {

    var options = GenerateOptions(`systems/${id}/sampleDoorParts`, 'GET');

    MakeTheApiCall(options).then(response => {

        var doorParts = response.data.response_data;
        dispatch(systemAction.recievedSampleDoors(doorParts));

    });

}

export function saveSystemGroup() {

    var data = JSON.stringify({
        name: 'SystemGroup'
    });

    var options = GenerateOptions(`systemGroups`, 'POST', data);

    dispatch(systemAction.savingData());

    MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

}

export function publishSystemGroup() {

    var data = JSON.stringify({
        name: 'System Group'
    });

    var options = GenerateOptions(`systemGroups/publish`, 'POST', data);

    MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataPublished(response.data));

    });

}

export function deleteSystemGroup(id) {

    var options = GenerateOptions(`systemGroups/${id}`, 'DELETE');

    dispatch(systemAction.requestData());

    return MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.deleteData());

    });
}

export function saveSystemTrait (id, data){

    var options = GenerateOptions(`systems/${id}/traits`, 'POST', data);

    dispatch(systemAction.savingData());

    MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

}

export function saveSystem(data) {

    var options = GenerateOptions(`systems`, 'POST', data);

    dispatch(systemAction.savingData());

    MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

};

export function updateSystem(data) {

    var options = GenerateOptions(`systems`, 'PUT', data);

    dispatch(systemAction.savingData());

    MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

};

export function deleteSystem(id) {

    var options = GenerateOptions(`systems/${id}`, 'DELETE');

    dispatch(systemAction.requestData());

    return MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.deleteData(response.data));

    });

};

export function newWindowSystem(data) {

    var options = GenerateOptions(`systems/window`, 'POST', data);

    dispatch(systemAction.savingData());

    return MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

};
export function newDoorSystem(data) {

    var options = GenerateOptions(`systems/door`, 'POST', data);

    dispatch(systemAction.savingData());

    return MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

};

export function duplicateSystem(id, data) {

    var options = GenerateOptions(`systems/${id}/duplicate`, 'POST', data);

    dispatch(systemAction.savingData());

    return MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.dataSaved(response.data));

    });

};

//system panel ranges
export function getPanelRanges(id) {

    var options = GenerateOptions(`systems/${id}/panelRanges`, 'GET');

    dispatch(systemAction.requestData());

    return MakeTheApiCall(options).then(response => {
        
        dispatch(systemAction.recievedPanelRanges(response.data.data));

    });

};

export function savePanelRange(id, data) {

    var options = GenerateOptions(`systems/${id}/panelRanges`, 'POST', data);

    dispatch(systemAction.requestData());

    return MakeTheApiCall(options).then(response => {
        
        return response;

    });

};

export function updatePanelRange(data) {

    var options = GenerateOptions(`systems/${data.system_id}/panelRanges/${data.id}`, 'PUT', data);

    return MakeTheApiCall(options).then(response => {
        
        return response;

    });

};

export function deletePanelRange(data) {

    var options = GenerateOptions(`systems/${data.system_id}/panelRanges/${data.id}`, 'DELETE');

    return MakeTheApiCall(options).then(response => {
        
        return response;

    });

};

export function saveTitleTraits(componentId, deletedTraits, traits) {

    dispatch(systemAction.savingData());

    let deleteRequests = [];

    let _saveTraits = function(traits) {

        var options = GenerateOptions(`systems/${componentId}/traits`, 'POST', traits);

        MakeTheApiCall(options).then(response => {
        
            dispatch(systemAction.dataSaved(response.data));

        });

    };

    if (deletedTraits && deletedTraits.length) {
        deletedTraits.map((trait) => {
            if (trait.id) {

                var options = GenerateOptions(`systemGroups/${componentId}/traits/${trait.id}`, 'DELETE');

                deleteRequests.push(
                    MakeTheApiCall(options)
                )

            }
        });
    }

    if (deleteRequests.length) {

        axios.all(deleteRequests).then(function(res){
            _saveTraits(traits);
        })

    } else {

        _saveTraits(traits);

    }
};

/**
 * Return the lists of PRE MADE SYSTEMS via the store
 * @param dispatch
 * @param listOfPreMadeSystems
 */

function successReturnPreMadeSystem (listOfPreMadeSystems) {
    dispatch(systemAction.recievedPremadeSystems(listOfPreMadeSystems))
}



