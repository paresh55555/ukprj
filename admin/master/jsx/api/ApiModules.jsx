import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {moduleAction} from '../actions';
import store from '../store';
import { replace } from 'react-router-redux';

const { dispatch } = store;

/**
 * Get the latest Modules from the API
 * @constructor
 */
export function ApiGetModulesList(){

    var options = GenerateOptions('modules', 'GET');

    dispatch(moduleAction.requestData());

    MakeTheApiCall(options).then((response) => {

        var modulesList = response.data;
        successReturnModules(modulesList);

    });
}

/**
 * Get Module details
 * @param id Module ID
 * @constructor
 */
export function ApiGetModuleDetails(id) {

    var options = GenerateOptions('modules', 'GET');

    dispatch(moduleAction.requestData());

    MakeTheApiCall(options).then((response) => {

        var data = response.data.filter(function (el) {
            return el.id == id
        });
        if(data.length){
            dispatch(moduleAction.receiveListData(response.data[0]))
        }

    });
}

/**
 * Get Module table list
 * @param id Module ID
 * @param type Module type
 * @constructor
 */
export function ApiModuleTableList(id, type){

    var options = GenerateOptions('modules/'+id+'/'+type, 'GET');

    dispatch(moduleAction.requestData());

    MakeTheApiCall(options).then((response) => {
        dispatch(moduleAction.receiveTableData(response.data))
    });
}

/**
 * Get Module MinMax sizes data
 * @param id Module ID
 * @constructor
 */
export function ApiGetMinMaxData(id) {

    var options = GenerateOptions('modules/'+id+'/size', 'GET');

    dispatch(moduleAction.requestData());

    MakeTheApiCall(options).then((response) => {
        if(Object.keys(response.data).length == 0 || id.length != (''+response.data.id).length){
            dispatch(replace('notfound/'))
        }

        dispatch(moduleAction.receiveMinMaxData(response.data));

    });
}

/**
 * Update Module MinMax sizes data
 * @param data Sizes data
 * @param id Module ID
 * @constructor
 */
export function ApiUpdateMinMax(data, id){

    var options = GenerateOptions('modules/'+id+'/size', 'PUT', data);

    dispatch(moduleAction.savingData());

    MakeTheApiCall(options).then((response) => {

        response.data.msg= 'Min / Max sizes updated successfully'
        dispatch(moduleAction.dataSaved(response.data))

    });
}

/**
 * Get Module data
 * @param id Module ID
 * @param pid Range ID
 * @param type Type
 * @constructor
 */
export function ApiGetModuleData(id, pid ,type) {

    var options = GenerateOptions('modules/'+id+'/'+type, 'GET');

    dispatch(moduleAction.requestData());

    MakeTheApiCall(options).then((response) => {

        var data = response.data.filter(function (el) {
            return el.id == pid
        });
        if(data.length > 0){
            dispatch(moduleAction.receiveModuleData(data[0]))
        }

    });
}

/**
 * Get Raw panel range data
 * @param type Module type
 * @param id Module ID
 * @constructor
 */
export function ApiGetRawPanelRange(type, id) {

    dispatch(moduleAction.requestData());

    setTimeout(function(){
        var raw = rawData(type, id)
        dispatch(moduleAction.receiveModuleData(raw));
    })
}

/**
 * Update module layout
 * @param data Module layout data
 * @param id Module ID
 * @param type Module Type
 * @constructor
 */
export function ApiUpdateModuleLayout(data, id, type){

    var options = GenerateOptions('modules/'+id+'/'+type+'/'+data.id, 'PUT', data);

    dispatch(moduleAction.savingData());

    MakeTheApiCall(options).then((response) => {
        dispatch(moduleAction.dataSaved(response.data))
    });
}

/**
 * Create module layout
 * @param data Module layout data
 * @param id Module ID
 * @param type Module type
 * @constructor
 */
export function ApiSaveModuleLayout(data, id, type){

    var options = GenerateOptions('modules/'+id+'/'+type, 'POST', data);

    dispatch(moduleAction.savingData());

    MakeTheApiCall(options).then((response) => {
        if(response.data.status == 'error'){
            dispatch(moduleAction.dataError(response.data))
        }
        else{
            dispatch(moduleAction.dataSaved(response.data))
        }
    });
}

/**
 * Delete module layout
 * @param dataId Module layout ID
 * @param id Module ID
 * @param type Module type
 * @constructor
 */
export function ApiDeleteModuleLayout(dataId, id, type){

    var options = GenerateOptions('modules/'+id+'/'+type+'/'+dataId, 'DELETE');

    dispatch(moduleAction.requestData());

    MakeTheApiCall(options).then((response) => {
        dispatch(moduleAction.deleteData(response.data))
    });
}

/**
 * Return the lists of Modules via the store
 * @param modulesList
 */

function successReturnModules (modulesList) {
    dispatch(moduleAction.receiveData(modulesList))
}

/**
 * Raw data
 * @param type Module type
 * @param id Module ID
 * @returns {*}
 */
function rawData(type,id){
    switch(type){
        case 'fixed':
            return {
                module_id:id,
                nameOfOption:'',
                partNumber:'',
                costItem:'',
                quantityFormula:'',
                forCutSheet:'',
                myOrder:''
            }
            break;
        case 'percent':
            return {
                module_id:id,
                nameOfOption:'',
                percentage:''
            }
            break;
        case 'glass':
            return {
                module_id:id,
                nameOfOption:'',
                costPerPanel:'',
                costSquareMeter:'',
                height_cut_size_formula:'',
                width_cut_size_formula:'',
                description:''
            }
            break;
        case 'panelRanges':
            return {
                panels:'',
                low:'',
                high:''
            }
            break;
        case 'frame':
            return {
                module_id:id,
                nameOfOption:'',
                option_type:'',
                material:'',
                cost_meter:'',
                cost_paint_meter:'',
                waste_percent:'',
                number_of_lengths_formula:'',
                cut_size_formula:'',
                burnOff:'',
                forCutSheet:'',
                myOrder:''
            }
            break;
            dafault :
                return ''
    }
    return {
        panels:'',
        low:'',
        high:''
    }
}