var folderPrefix = 'SQ-admin';
var request = new XMLHttpRequest();
request.open("GET", window.location.protocol + "//" + window.location.host + "/"+folderPrefix+"/server/ApiConfig.json", false);
request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
request.send();

if (request.status === 200) {
    var response = JSON.parse(request.responseText);
}

export const API_URL = response ? response.config.API_URL : window.location.protocol + "//" + window.location.host + '/api/V4';
export const VERSION = response ? response.config.VERSION : 'V2';
export const SQ_URL = folderPrefix
