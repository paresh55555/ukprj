import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {costAction, optionAction} from '../actions';
import store from '../store';
import * as constant from '../constant';

const { dispatch } = store;

function getUrl(systemId, componentId, optionId, costId){
    var url = optionId === 0 ?
        `systems/${systemId}/components/${componentId}/costs` + (costId ? `/${costId}` : '')
        :
        `systems/${systemId}/components/${componentId}/options/${optionId}/costs` + (costId ? `/${costId}` : '')
    return url
}

/**
 * Create parts costs
 * @param systemId
 * @param componentId
 * @param optionId
 * @param costs
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiCreatePartsCosts(systemId, componentId, optionId, costs){

    let requests = [],
        url = getUrl(systemId, componentId, optionId);
    costs.forEach((cost) => {
        let data = JSON.stringify(cost);
        var options = GenerateOptions(url, 'POST', data);
        requests.push(
            MakeTheApiCall(options)
        )
    })
    return MakeTheApiCall(requests, true)
}

/**
 * Delete costs
 * @param systemId
 * @param componentId
 * @param optionId
 * @param costs
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiDeletePartsCosts(systemId, componentId, optionId, costs){

    let requests = [];
    costs.forEach((cost) => {
        let url = getUrl(systemId, componentId, optionId, cost.id)
        var options = GenerateOptions(url, 'DELETE');
        requests.push(
            MakeTheApiCall(options)
        )
    })
    return MakeTheApiCall(requests, true)
}

/**
 * Add cost to option part
 * @param systemId
 * @param componentId
 * @param optionId
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiAddOptionPartCost(systemId, componentId, optionId, data){

    dispatch(costAction.savingCostsData(optionId));

    data = JSON.stringify(data);
    let url = getUrl(systemId, componentId, optionId);
    var options = GenerateOptions(url, 'POST', data);
    console.log(url,optionId,data)

    return MakeTheApiCall(options).then(function (response) {
        var payload = {
            data: response.data.response_data[0],
            componnetId: componentId || 0,
            optionId: optionId || 0
        }

        dispatch(costAction.saveCostsData(payload));
    })

}

/**
 * Add cost to option part
 * @param systemId
 * @param componentId
 * @param optionId
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiAddComponentPartCost(systemId, componentId, data,existingData){

    dispatch(costAction.savingCostsData(componentId));

    data = JSON.stringify(data);
    let url = getUrl(systemId, componentId, 0);
    var options = GenerateOptions(url, 'POST', data);

    return MakeTheApiCall(options).then(function (response) {
        var payload = {
            data: existingData.concat(response.data.response_data[0]),
            componnetId: componentId
        }

        dispatch(costAction.saveCostsData(payload));
    })

}

/**
 * Update cost
 * @param systemId
 * @param componentId
 * @param optionId
 * @param id
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateCost(systemId, componentId, optionId, id, data){

    let newData = JSON.stringify(data),
        url = getUrl(systemId, componentId, optionId, id),
        options = GenerateOptions(url, 'PUT', newData);

    return MakeTheApiCall(options).then(function (response) {
        dispatch(costAction.costDataSaved(response.data.response_data));
        return response.data.response_data;
    })
}

/**
 * Update cost data
 * @param systemId
 * @param componentId
 * @param optionId
 * @param id
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateCostData(systemId, componentId, optionId, id, data){

    let newData = JSON.stringify(data),
        url = getUrl(systemId, componentId, optionId, id),
        options = GenerateOptions(url, 'PUT', newData);

    return MakeTheApiCall(options).then(function (response) {
        var payload = {
            data: response.data.response_data[0],
            componnetId: componentId || 0,
            optionId: optionId || 0
        }
        dispatch(costAction.costDataUpdate(payload));
    })
}

/**
 * Update cost data for component
 * @param systemId
 * @param componentId
 * @param id
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateComponentCostData(systemId, componentId, id, data,existingData){

    let newData = JSON.stringify(data),
        url = getUrl(systemId, componentId, 0, id),
        options = GenerateOptions(url, 'PUT', newData);

    return MakeTheApiCall(options).then(function (response) {
        const index = existingData.findIndex(item=>item.id === id);
        existingData[index] = response.data.response_data[0]
        var payload = {
            data: existingData,
            componnetId: componentId 
        }
        dispatch(costAction.saveCostsData(payload));
    })
}

/**
 * Delete cost
 * @param systemId
 * @param componentId
 * @param optionId
 * @param id
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiDeleteCost(systemId, componentId, optionId, id){

    let url = getUrl(systemId, componentId, optionId, id),
        options = GenerateOptions(url, 'DELETE');

    return MakeTheApiCall(options).then(function (response) {
        dispatch(costAction.deletedCost(id, optionId));
    })
}

/**
 * Delete cost component
 * @param systemId
 * @param componentId]
 * @param id
 * @param current costs 
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiDeleteComponentCost(systemId, componentId, id,existingData){

    let url = getUrl(systemId, componentId, 0, id),
        options = GenerateOptions(url, 'DELETE');

    return MakeTheApiCall(options).then(function (response) {
         const index = existingData.findIndex(item=>item.id === id);
         existingData.splice(index,1)
         var payload = {
            data: existingData,
            componnetId: componentId 
         }
        dispatch(costAction.saveCostsData(payload));

        //dispatch(costAction.deletedCost(id, optionId));
    })
}

/**
 * Get component cost
 * @param systemId
 * @param componentId
 * 
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetComponentCosts(systemId, componentId){

    let url = getUrl(systemId, componentId,0),
        options = GenerateOptions(url, 'GET');

    return MakeTheApiCall(options).then(function (response) {
         dispatch(costAction.receiveComponetCost(response.data.response_data, componentId));
        //dispatch(costAction.deletedCost(id, optionId));
    })
}