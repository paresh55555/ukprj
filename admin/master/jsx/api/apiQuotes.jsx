import * as constant from '../constant';
import MakeTheApiCall from './ApiCalls';
import { cartAction } from '../actions';
import { CACHE_USERS_LIST } from './ApiCache';
import { API_URL } from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;

/**
 * Caches the getUsers API calls or immediate initial return.
 * @param listOfUsers
 */

function cacheUsersList(listOfUsers) {
    localStorage.setItem(CACHE_USERS_LIST, JSON.stringify(listOfUsers));
}

/**
 * Build the correct headers and options to make the API call.
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

function buildOptions(type='',method = 'get', data, urlString = '') {
    const token = sessionStorage.getItem('token');
    return {
        method,
        url: `${API_URL}/${type}Builder${urlString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
        data: JSON.stringify(data) || null,
    };
}

/** api methods*/

export function getQuotes(type) {
    const options = buildOptions(type);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.getElementsAction(response.data));
    });
}

export function saveQuotes(type,data) {
    dispatch(cartAction.startSaving());
    const options = buildOptions(type,'post', data);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.saveElementsAction(response.data.response_data[0]));
        return response.data.response_data[0];
    });
}

export function updateQuotes(data) {
    dispatch(cartAction.requestElement());
    const options = buildOptions('quote','put', data);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.updateElementAction(response.data.response_data[0]));
    });
}

export function deleteQuotes(id) {
    dispatch(cartAction.requestElement());
    const options = buildOptions('quote','delete', null, `/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.deleteElementsAction(id));
    });
}

export function getQuotesLayouts(id) {
    dispatch(cartAction.requestElement());
    const options = buildOptions('quote','get', null, `/${id}/layouts`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.getElementLayoutsAction(response.data, id));
        return response.data;
    });
}

export function saveQuotesLayout(id, data, order, isHorizontalLine) {
    const requestData = { number_of_columns: data.number_of_columns }
    const options = buildOptions('quote','post', requestData, `/${id}/layouts`);
    return MakeTheApiCall(options)
        .then((response) => {
            const responseData = response.data.response_data[0];
            const numberOfColumns = responseData.number_of_columns;
            const layoutWithColumns = Object.assign({}, responseData, { columns: data.columns });
            dispatch(cartAction.replaceElementLayoutAction(layoutWithColumns, data.id));
            return layoutWithColumns;
        })
        .then((response) => {
            if (order.order !== response.order) {
                updateCartLayout(id, response.id, order);
            }
            const requests = response.columns.map(() => saveColumns(response.layout_id, response.id, { alignment: 'left' }));
            return Promise.all(requests);
        })
        .then((values) => {
            const layoutId = values[0].layout_group_id;
            if (!isHorizontalLine) {
                dispatch(cartAction.saveColumnsAction(values, layoutId));
            }
            return values;
        })
        .then((response) => {
            if (isHorizontalLine) {
                const responseData = response[0];
                const sendingData = {
                    component_type: 'horizontalLine',
                    item: 'spacing',
                    label: 'horizontalLine',
                };
                saveItemColumnLayout(id, responseData.layout_group_id, responseData.id, sendingData);
            }
            return response;
        });
}


export function updateQuotesLayout(cartId, id, data) {
    const options = buildOptions('quote','put', data, `/${cartId}/layouts/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.updateElementLayoutAction(response.data.response_data[0]));
        return response.data.response_data[0];
    });
}

export function deleteQuotesLayout(cartId, id, withDispatch) {
    const options = buildOptions('quote','delete', null, `/${cartId}/layouts/${id}`);
    return MakeTheApiCall(options).then(() => {
        if (withDispatch) {
            dispatch(cartAction.deleteElementLayoutAction(id));
        }
    });
}

export function saveColumns(cartId, id, data) {
    const options = buildOptions('quote','post', data, `/${cartId}/layouts/${id}/column`);
    return MakeTheApiCall(options).then((response) => {
        return response.data.response_data[0];
    });
}

export function saveColumnCartLayout(cartId, id, data) {
    //dispatch(cartAction.requestElement());
    const options = buildOptions('quote','post', data, `/${cartId}/layouts/${id}/column`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.saveColumnElementLayoutAction(response.data.response_data[0]));
        return response.data.response_data[0];
    });
}

export function updateColumnQuotesLayout(cartId, layoutId, id, data) {
    const options = buildOptions('quote','put', data, `/${cartId}/layouts/${layoutId}/column/${id}`);
    return MakeTheApiCall(options).then((response) => {
    });
}

export function deleteColumnCartLayout(cartId, layoutId, id) {
    const options = buildOptions('quote','delete', null, `/${cartId}/layouts/${layoutId}/column/${id}`);
    return MakeTheApiCall(options).then(() => {
        dispatch(cartAction.deleteColumnElementLayoutAction(layoutId, id));
    });
}

export function saveItemColumnLayout(cartId, layoutId, id, data) {
    const options = buildOptions('quote','post', data, `/${cartId}/layouts/${layoutId}/column/${id}/items`);
    return MakeTheApiCall(options).then((response) => {
        return response.data.response_data[0];
    });
}
