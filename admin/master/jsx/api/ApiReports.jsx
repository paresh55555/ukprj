import MakeTheApiCall from './ApiCalls';
import { reportsAction } from '../actions';
import { API_URL } from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;


export function getSalesReports(requestString) {
    dispatch(reportsAction.requestSalesReports());
    const options = buildReportsOptions(`/sales?${requestString}`);
    MakeTheApiCall(options).then(
        (res) => {
            dispatch(reportsAction.receiveSalesReports(res.data.data));
        });
}

export function getDeliveredOrdersReports() {
    dispatch(reportsAction.requestDeliveredOrdersReports());
    const options = buildReportsOptions('/delivered/balance');
    MakeTheApiCall(options).then(
        (res) => {
            dispatch(reportsAction.receiveDeliveredOrdersReports(res.data.data));
        });
}

function buildReportsOptions(requestString) {
    const token = sessionStorage.getItem('token');
    return {
        method: 'GET',
        url: `${API_URL}/reports${requestString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
    };
}
