
import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {premadeAction} from '../actions';
import store from '../store';

const { dispatch } = store;

/**
 * Get Premade Colors from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function getPremadeColors() {

    var options = GenerateOptions('premadecolors', 'GET');

    dispatch(premadeAction.requestData())

    MakeTheApiCall(options).then(response => {

        var Colors = response.data.data;
        successReturnPremadeColors(Colors);
    
    });
}

export function savePremadeGroups(systemId, componentId, groups) {

    var options = GenerateOptions(`systems/${systemId}/components/${componentId}/premadeGroups`, 'POST', groups);

    dispatch(premadeAction.savingData())

    MakeTheApiCall(options).then(response => {

        successReturnSavedGroups(response.data);
    
    });
}

export function savePremadeSubGroups(systemId, componentId, subGroups) {

    var options = GenerateOptions(`systems/${systemId}/components/${componentId}/premadeSubGroups`, 'POST', subGroups);

    dispatch(premadeAction.savingData())

    MakeTheApiCall(options).then(response => {

        successReturnSavedGroups(response.data);
    
    });

}

export function savePremadeColors(systemId, componentId, colors) {

    var options = GenerateOptions(`systems/${systemId}/components/${componentId}/premadeColors`, 'POST', colors);

    dispatch(premadeAction.savingData())

    MakeTheApiCall(options).then(response => {

        successReturnSavedGroups(response.data);
    
    });

}

export function getComponentDetail(systemid, id) {

    var options = GenerateOptions(`systems/${systemid}/components/${id}`, 'GET');

    dispatch(premadeAction.requestData())

    MakeTheApiCall(options).then(response => {

        var Component = response.data.response_data[0];
        successReturnComponent(Component);
    
    });
}

/**
 * Return the Colors
 * @param dispatch
 * @param Colors
 */

function successReturnPremadeColors (Colors) {
    dispatch(premadeAction.receiveData(Colors))
}

function successReturnSavedGroups (Data) {
    dispatch(premadeAction.dataSaved(Data))
}

function successReturnComponent (Component) {
    dispatch(premadeAction.receiveComponentData(Component))
}


