import React from 'react';
import axios from 'axios';
import { actions as toastrActions } from 'react-redux-toastr';
import { DEBUG_MODE } from '../constant';
import {API_URL} from './ApiServerConfigs';
import store from '../store';
import {logout, loggedIn} from '../services/authService.js'

const { dispatch } = store;
const CancelToken = axios.CancelToken;
let cancel;

/**
 * This function would universally handle API errors
 *
 * @param apiOptions
 * @param multiple Parameter for axios.all request (apiOptions should be array of axios requests)
 * @param dispatch
 * @returns {axios.Promise}
 * @constructor
 */

export default function MakeTheApiCall(apiOptions, multiple = false) {
    var success = (resolve,reject,response) => {
            let checkResponse = (response) => {
                if (response.status !== 200 && response.status !== 201) {
                    showErrorToster(response.statusText,apiOptions.method,apiOptions.url)
                    return reject(response)
                }
            }
            if(Array.isArray(response)){
                response.forEach((r)=>{
                    checkResponse(r)
                })
            }else{
                checkResponse(response)
            }
            return resolve(response);
        },
        error = (resolve,reject,err) => {
            if(err.status === 403){
                window.location.replace('forbidden')
            }
            if(err.status === 401 || (err.response && err.response.status === 401)){
                if(loggedIn())
                    logout()
            }
            else{
                showErrorToster(err.statusText,apiOptions.method,apiOptions.url)
            }
            return reject(err);
        }
    if(multiple){
        return new Promise((resolve,reject)=>{
            return axios.all(apiOptions).then((response)=>{
                success(resolve,reject,response)
            }).catch((err)=>{
                error(resolve,reject,err)
            });
        });
    }else{
        return new Promise((resolve,reject)=>{
            return axios(apiOptions).then((response)=>{
                success(resolve,reject,response)
            }).catch((err)=>{
                if (axios.isCancel(err)) {
                    console.log('Request canceled.', err.message);
                }else{
                    error(resolve,reject,err)
                }
            });
        });
    }
}

/**
 * Build the correct headers and options to make the API call.
 * @params url: string
 * @params method: string
 * @params data: object
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

export function GenerateOptions(url = '', method = 'GET', data, uploadProgress) {

    var token = sessionStorage.getItem("token");

    var options = {
        method: method,
        url: `${API_URL}/${url}`,
        crossDomain:true,
        headers: {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        json: true,
        cancelToken: new CancelToken(function executor(c) {
            // An executor function receives a cancel function as a parameter
            cancel = c;
        })
    };

    if(!data) {
        return options;
    }

    if(method == 'GET') {
        options.params = data
    } else {
        options.data = data
    }
    if(uploadProgress){
        options.onUploadProgress = uploadProgress
    }

    return options;

}

export function cancelRequest() {
    cancel('Operation canceled by the user.')
}

/**
* show error toster on error 
*/

function showErrorToster(statusText,method,url){
    if (DEBUG_MODE) {
        dispatch(toastrActions.add({
            type: 'error',
            title: 'Error',
            options: {
                timeOut: 0,
                showCloseButton: true,
            },
            message: (
                <div>
                    <div>Status text: {statusText}</div>
                    <div>API method: {method}</div>
                    <div>API URL: {url}</div>
                </div>),
        }));
    
    }
}
