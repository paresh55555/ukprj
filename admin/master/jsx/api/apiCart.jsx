import * as constant from '../constant';
import MakeTheApiCall from './ApiCalls';
import { cartAction } from '../actions';
import { CACHE_USERS_LIST } from './ApiCache';
import { API_URL } from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;

/**
 * Caches the getUsers API calls or immediate initial return.
 * @param listOfUsers
 */

function cacheUsersList(listOfUsers) {
    localStorage.setItem(CACHE_USERS_LIST, JSON.stringify(listOfUsers));
}

/**
 * Build the correct headers and options to make the API call.
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

function buildOptions(method = 'get', data, urlString = '') {
    const token = sessionStorage.getItem('token');
    return {
        method,
        url: `${API_URL}/cart${urlString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
        data: JSON.stringify(data) || null,
    };
}

/** api methods*/

export function getCarts() {
    dispatch(cartAction.requestElement());
    const options = buildOptions();
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.getElementsAction(response.data));
    });
}

export function saveCart(data) {
    dispatch(cartAction.startSaving());
    const options = buildOptions('post', data);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.saveElementsAction(response.data.response_data[0]));
        return response.data.response_data[0];
    });
}

export function updateCart(data) {
    dispatch(cartAction.requestElement());
    const options = buildOptions('put', data);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.updateElementAction(response.data.response_data[0]));
    });
}

export function deleteCart(id) {
    dispatch(cartAction.requestElement());
    const options = buildOptions('delete', null, `/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.deleteElementsAction(id));
    });
}

export function getCartLayouts(id) {
    dispatch(cartAction.requestElement());
    const options = buildOptions('get', null, `s/${id}/layouts`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.getElementLayoutsAction(response.data, id));
        return response.data;
    });
}

export function saveCartLayout(id, data, order, isHorizontalLine) {
    const requestData = { number_of_columns: data.number_of_columns }
    const options = buildOptions('post', requestData, `s/${id}/layouts`);
    return MakeTheApiCall(options)
        .then((response) => {
            const responseData = response.data.response_data[0];
            const numberOfColumns = responseData.number_of_columns;
            const layoutWithColumns = Object.assign({}, responseData, { columns: data.columns });
            dispatch(cartAction.replaceElementLayoutAction(layoutWithColumns, data.id));
            return layoutWithColumns;
        })
        .then((response) => {
            if (order.order !== response.order) {
                updateCartLayout(id, response.id, order);
            }
            const requests = response.columns.map(() => saveColumns(response.layout_id, response.id, { alignment: 'left' }));
            return Promise.all(requests);
        })
        .then((values) => {
            const layoutId = values[0].layout_group_id;
            if (!isHorizontalLine) {
                dispatch(cartAction.saveColumnsAction(values, layoutId));
            }
            return values;
        })
        .then((response) => {
            if (isHorizontalLine) {
                const responseData = response[0];
                const sendingData = {
                    component_type: 'horizontalLine',
                    item: 'spacing',
                    label: 'horizontalLine',
                };
                saveItemColumnLayout(id, responseData.layout_group_id, responseData.id, sendingData);
            }
            return response;
        });
}


export function updateCartLayout(cartId, id, data) {
    //dispatch(cartAction.requestElement());
    const options = buildOptions('put', data, `s/${cartId}/layouts/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.updateElementLayoutAction(response.data.response_data[0]));
        return response.data.response_data[0];
    });
}

export function deleteCartLayout(cartId, id, withDispatch) {
    //dispatch(cartAction.requestElement());
    const options = buildOptions('delete', null, `s/${cartId}/layouts/${id}`);
    return MakeTheApiCall(options).then(() => {
        if (withDispatch) {
            dispatch(cartAction.deleteElementLayoutAction(id));
        }
    });
}

export function saveColumns(cartId, id, data) {
    const options = buildOptions('post', data, `s/${cartId}/layouts/${id}/column`);
    return MakeTheApiCall(options).then((response) => {
        return response.data.response_data[0];
    });
}

export function saveColumnCartLayout(cartId, id, data) {
    //dispatch(cartAction.requestElement());
    const options = buildOptions('post', data, `s/${cartId}/layouts/${id}/column`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.saveColumnElementLayoutAction(response.data.response_data[0]));
        return response.data.response_data[0];
    });
}

export function updateColumnCartLayout(cartId, layoutId, id, data) {
    const options = buildOptions('put', data, `s/${cartId}/layouts/${layoutId}/column/${id}`);
    return MakeTheApiCall(options).then((response) => {
    });
}

export function deleteColumnCartLayout(cartId, layoutId, id) {
    const options = buildOptions('delete', null, `s/${cartId}/layouts/${layoutId}/column/${id}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(cartAction.deleteColumnElementLayoutAction(layoutId, id));
        //return response.data.response_data[0];
    });
}

export function saveItemColumnLayout(cartId, layoutId, id, data) {
    const options = buildOptions('post', data, `s/${cartId}/layouts/${layoutId}/column/${id}/items`);
    return MakeTheApiCall(options).then((response) => {
        return response.data.response_data[0];
    });
}
