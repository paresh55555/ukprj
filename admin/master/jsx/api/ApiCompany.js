
import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {companyAction} from '../actions';
import store from '../store';

const { dispatch } = store;

/**
 * Get the latest Companies from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiGetCompanies() {

    var options = GenerateOptions('companies', 'GET');

    dispatch(companyAction.requestData());

    MakeTheApiCall(options).then((response) => {

            var listOfCompanies = response.data;
            successReturnCompanies(listOfCompanies);

    });
}

/**
 * Get the Companies By order from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function apiGetCompaniesByOrder() {

    var options = GenerateOptions('companies/order', 'GET');

    dispatch(companyAction.requestData());

    MakeTheApiCall(options).then((response) => {

            var listOfCompanies = response.data;
            successReturnCompaniesByOrder(listOfCompanies);

    });
}

/**
 * Get the Companiy data from the API.
 *
 * @param object dispatch
 * @param string company id
 * @constructor
 */

export function ApiGetCompanyData(id) {

    var options = GenerateOptions('companies/' + id, 'GET');

    dispatch(companyAction.requestData());

    MakeTheApiCall(options).then((response) => {

        var companyData = response.data;
        successReturnCompanyData(companyData);

    });
}

/**
 * Get the Companiy User data from the API.
 *
 * @param object dispatch
 * @param string company id
 * @constructor
 */

export function ApiGetCompanyUserData(id) {

    var options = GenerateOptions('companies/' + id + '/users', 'GET');

    dispatch(companyAction.requestData());

    MakeTheApiCall(options).then((response) => {

        var userData = response.data;
        successReturnCompanyUserData(userData);

    });
}

/**
 * Return Raw Companiy data.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiGetCompanyRawData() {

    dispatch(companyAction.requestData());

    var rawcompany = {
        companyName: "",
        firstName: "",
        lastName: "",
        address1: "",
        address2: "",
        phone:"",
        city: "",
        state: "",
        zip: ""
    }

    setTimeout(function(){
        successReturnCompanyData(rawcompany);
    })
}

/**
 * Post new company data to api.
 *
 * @param object dispatch
 * @param object companyData
 * @constructor
 */

export function ApiSaveCompany(companyData) {

    var options = GenerateOptions('companies', 'POST', companyData);

    dispatch(companyAction.savingData());

    MakeTheApiCall(options).then((response) => {

        var company = response.data;
        successReturnCompanySaved(company);

    });
}

/**
 * Update Company data
 *
 * @param object dispatch
 * @param object companyData
 * @constructor
 */

export function ApiUpdateCompany(companyData) {

    var options = GenerateOptions('companies/' + companyData.id, 'PUT', companyData);

    dispatch(companyAction.savingData());

    MakeTheApiCall(options).then((response) => {

        var company = response.data;
        successReturnCompanySaved(company);


    });
}

/**
 * delete Company 
 *
 * @param object dispatch
 * @param object companyId
 * @constructor
 */

export function ApiDeleteCompany(companyId) {

    var options = GenerateOptions('companies/' + companyId, 'DELETE');

    dispatch(companyAction.savingData());

    MakeTheApiCall(options).then((response) => {

        var company = response.data;
        successReturnCompanyDeleted(company);

    });
}

/**
 * Return the lists of Companies via the store
 * @param dispatch
 * @param listOfCompanies
 */

function successReturnCompanies (listOfCompanies) {
    dispatch(companyAction.receiveData(listOfCompanies))
}

/**
 * Return the lists of Companies by order via the store
 * @param dispatch
 * @param listOfCompanies
 */

function successReturnCompaniesByOrder (listOfCompanies) {
    dispatch(companyAction.receiveDataByOrder(listOfCompanies))
}

/**
 * Return Company Data via the store
 * @param dispatch
 * @param companyData
 */

function successReturnCompanyData (companyData) {
    dispatch(companyAction.receiveCompanyData(companyData))
}

/**
 * Return Company User Data via the store
 * @param dispatch
 * @param userData
 */

function successReturnCompanyUserData (userData) {
    dispatch(companyAction.receiveCompanyUserData(userData))
}

function successReturnCompanySaved (companyData) {
    dispatch(companyAction.dataSaved(companyData))
}

function successReturnCompanyDeleted (companyData) {
    dispatch(companyAction.deleteData(companyData))
}

