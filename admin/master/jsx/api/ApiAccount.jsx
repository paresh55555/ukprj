import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {accountAction} from '../actions';
import store from '../store';

const { dispatch } = store;

/**
 * Get the Accounts from the API.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiGetAccounts() {

    var options = GenerateOptions('accounts', 'GET');

    dispatch(accountAction.requestData())

    MakeTheApiCall(options).then(response => {

        dispatch(accountAction.receiveData(response.data.data));

    });
}

/**
 * Get Account Builders
 * @constructor
 */
export function ApiGetAccountBuilders(){

    var options = GenerateOptions('accounts/builders', 'GET');

    dispatch(accountAction.requestData())

    MakeTheApiCall(options).then(response => {

        dispatch(accountAction.receiveAccountBuildersData(response.data));

    });
}

/**
 * Get Account by ID
 * @param id
 * @constructor
 */
export function ApiGetAccount(id) {

    var options = GenerateOptions('accounts/'+id, 'GET');

    dispatch(accountAction.requestData())

    MakeTheApiCall(options).then(response => {

        dispatch(accountAction.receiveAccountData(response.data.data));

    });
}

/**
 * Delete Account
 *
 * @param object dispatch
 * @constructor
 */

export function ApiDeleteAccount(id) {

    var options = GenerateOptions('accounts/'+id, 'DELETE');

    dispatch(accountAction.deletingData())

    return MakeTheApiCall(options).then(response => dispatch(accountAction.deletedData()));
}

/**
 * Create account.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiCreateAccount(account) {

    var options = GenerateOptions('accounts', 'POST', account);

    dispatch(accountAction.savingData())

    return MakeTheApiCall(options).then(response => {

        dispatch(accountAction.receiveAccountData(response.data));

    });
}

/**
 * Update account.
 *
 * @param object dispatch
 * @constructor
 */

export function ApiUpdateAccount(account) {

    var options = GenerateOptions('accounts/'+account.id, 'PUT', account);

    dispatch(accountAction.savingData())

    return MakeTheApiCall(options).then(response => {

        dispatch(accountAction.receiveAccountData(response.data));

    });
}


