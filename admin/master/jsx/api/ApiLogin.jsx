// var actions = require('actions');

import MakeTheApiCall, {GenerateOptions}  from './ApiCalls';
import {authAction} from '../actions';
import {USER_AUTH_TOKEN} from './ApiCache';
import {API_URL} from './ApiServerConfigs';

/**
 * This function call the api and validate the users trying to login
 * If user/pass is valid, the store will be updated to login the user.
 *
 * @param object dispatch
 * @param object auth
 * @constructor
 */
export function ApiLogin (dispatch, auth) {
    var apiOptions = buildLoginApiOptions(auth);
    MakeTheApiCall(apiOptions).then(function (response) {
            setToken(response.data.token);
            // let rs = {"reports":"all"};
            setAuth(response.data)
            dispatch(authAction.loginSuccess(response.data));
    }).catch((err) =>{
        dispatch(authAction.receiveError(err))
    });
}

export function ApiReset(dispatch, data) {
  
  var options = GenerateOptions('/token/reset', 'POST',data);

  return MakeTheApiCall(options).then(response => {
      if(response.data.status == 'success'){
        return response
      }else{
        dispatch(authAction.receiveError(response.data));
      }
  }).catch((err) =>{
    dispatch(authAction.receiveError(err));
  })
}
export function ApiResetPassword(dispatch, data) {
  
  var options = GenerateOptions('/token/pws', 'POST',data);

  return MakeTheApiCall(options).then(response => {
      if(response.data.status == 'success'){
        return response
      }else{
        dispatch(authAction.receiveError(response.data));
      }
  }).catch((err) =>{
    dispatch(authAction.receiveError(err));
  })
}


/**
 * Take the User/Pass, encodes them and sets the rest of the api options
 *
 * @param auth
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */
function buildLoginApiOptions (auth) {

    var password = auth.password;
    var username = auth.username;

    var basicToken =  new Buffer(username + ':' + password).toString('base64')

    var options = {
        method: 'POST',
        url: API_URL+ '/token',
        crossDomain:true,
        headers: {
            'Authorization': 'Basic ' + basicToken,
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        json: true
    };

    return options;
}


/**
 * Sends a Login action to store for Login component to take action on
 * @param object dispatch
 */
function loginUser(dispatch) {

}


/**
 * Set the User's Auth Token for future calls
 *
 * @param string token
 */
function setToken(token) {
    // Save Access Token for Future Calls
    sessionStorage.setItem('token',token);
}

function setAuth(auth_info) {
    // Save Access Token for Future Calls
    sessionStorage.setItem('auth_info',JSON.stringify(auth_info));
}
export function getAuth(dispatch) {
    // Save Access Token for Future Calls
    let auth_info=sessionStorage.auth_info;
    dispatch(authAction.loginSuccess(JSON.parse(auth_info)));
}
/**
 * Handle bad user/pass combinations and notifies the user
 * @param object response
 */
function handleErrorsAndLetUserKnow(response) {

    //Needs Implementing

}