import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {serviceCustomerAction, addressesAction} from '../actions';
import store from '../store';

const {dispatch} = store;

export function ApiGetCustomers(){

    var options = GenerateOptions('service/customers', 'GET');
    dispatch(serviceCustomerAction.getServiceCustomers())
    MakeTheApiCall(options).then(response => {
    
        dispatch(serviceCustomerAction.setServiceCustomers(response.data.data));
    });
}

export function ApiGetCustomersById(id){

    var options = GenerateOptions(`service/customers/${id}`, 'GET');
    dispatch(serviceCustomerAction.getServiceCustomers())
    MakeTheApiCall(options).then(response => {

        const customer = response.data.data[0] || {};
        const address_list = (customer && customer.address_list) || []
        
        dispatch(serviceCustomerAction.setCurrentCustomer(customer));
        dispatch(addressesAction.setAddressList(address_list))
    });
}

export function ApiGetCustomersOrders(id){
  
    var options = GenerateOptions(`service/customers/${id}/orders`, 'GET');
    dispatch(serviceCustomerAction.getServiceCustomers())
    MakeTheApiCall(options).then(response => {
    
        dispatch(serviceCustomerAction.setServiceCustomerOrders(response.data.data));
    });
}

export function ApiGetCustomersJobs(id){

    var options = GenerateOptions(`service/customers/${id}/jobs`, 'GET');
    dispatch(serviceCustomerAction.getServiceCustomers())
    MakeTheApiCall(options).then(response => {
    
        dispatch(serviceCustomerAction.setServiceCustomerJobs(response.data.data));
    });
}

export function ApiAddServiceCustomer(data) {

    var options = GenerateOptions(`service/customers`, 'POST',data);

   return MakeTheApiCall(options).then(response => {
        
        return(response)

    });
}


export function ApiUpdateServiceCustomer(data) {

    var options = GenerateOptions(`service/customers/${data.id}`, 'PUT', data);

   return MakeTheApiCall(options).then(response => {
        
        return(response)

    });
}

export function ApiAddCustomerAddress(customerId, data) {

    var options = GenerateOptions(`service/customers/${customerId}/address`, 'POST', data);

   return MakeTheApiCall(options).then(response => {
        
        return(response)

    });
}

export function ApiUpdateCustomerAddress(customerId, data) {

    var options = GenerateOptions(`service/customers/${customerId}/address/${data.id}`, 'PUT', data);

   return MakeTheApiCall(options).then(response => {
        
        return(response)

    });
}

export function ApiDeleteCustomerAddress(customerId, address_id) {

    var options = GenerateOptions(`service/customers/${customerId}/address/${address_id}`, 'DELETE');

   return MakeTheApiCall(options)
}