import { routerActions } from 'react-router-redux';
import * as constant from '../constant';
import MakeTheApiCall from './ApiCalls';
import { optionAction } from '../actions';
import { CACHE_USERS_LIST } from './ApiCache';
import { API_URL } from './ApiServerConfigs';
import store from '../store';

const { dispatch } = store;

/**
 * Caches the getUsers API calls or immediate initial return.
 * @param listOfUsers
 */

function cacheUsersList(listOfUsers) {
    localStorage.setItem(CACHE_USERS_LIST, JSON.stringify(listOfUsers));
}

/**
 * Build the correct headers and options to make the API call.
 * @returns {{method: string, url: string, crossDomain: boolean, headers: {Authorization: string, Content-Type: string}, json: boolean}}
 */

function buildUserOptions(systemId, componentId, method = 'get', data, urlString = 'options') {
    const token = sessionStorage.getItem('token');
    return {
        method,
        url: `${constant.SYSTEM_LIST_URL}/${systemId}/components/${componentId}/${urlString}`,
        crossDomain: true,
        headers: {
            Authorization: `Bearer ${token}`,
            'Content-Type': 'application/x-www-form-urlencoded',
        },
        json: true,
        data: data || null,
    };
}

/**
 * Return the cache list of user if available immediately to the store while waiting for the api to update
 */

function returnCacheListOfUsers() {
    const listOfUsers = JSON.parse(localStorage.getItem(CACHE_USERS_LIST));
    if (listOfUsers && listOfUsers.errorMessage === undefined && listOfUsers.length > 0) {
        dispatch(userAction.receiveListOfUsers(listOfUsers));
    }
}

/** api methods*/

export function getOptions(systemId, componentId) {
    dispatch(optionAction.requestData());
    const options = buildUserOptions(systemId, componentId);
    return MakeTheApiCall(options).then((response) => {
        dispatch(optionAction.receiveData(response.data.response_data));
        return response.data.response_data;
    });
}

export function saveOption(systemId, componentId, option) {
    dispatch(optionAction.savingData());
    const data = JSON.stringify({ name: option.name, allowDeleted: option.allowDeleted });
    const options = buildUserOptions(systemId, componentId, 'post', data);
    return MakeTheApiCall(options).then((response) => {
        dispatch(optionAction.dataSaved(response.data.response_data));
        return response.data.response_data;
    });
}

export function deleteOption(systemId, componentId, optionId) {
    dispatch(optionAction.savingData());
    const options = buildUserOptions(systemId, componentId, 'delete', null, `options/${optionId}`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(optionAction.dataDeleted(response.data, optionId));
        return response.data;
    });
}

export function getOptionParts(systemId, componentId, optionId) {
    dispatch(optionAction.requestData());
    const options = buildUserOptions(systemId, componentId, 'get', null, `options/${optionId}/parts`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(optionAction.receivePartsData(response.data.response_data, optionId));
        return response.data.response_data;
    });
}

export function getOptionCosts(systemId, componentId, optionId) {
    dispatch(optionAction.requestData());
    const newOptionId = optionId || 0;
    const options = newOptionId === 0 ? buildUserOptions(systemId, componentId, 'get', null, 'costs') :
        buildUserOptions(systemId, componentId, 'get', null, `options/${newOptionId}/costs`);
    return MakeTheApiCall(options).then((response) => {
        dispatch(optionAction.receiveCostsData(response.data.response_data, newOptionId));
        return response.data.response_data;
    });
}
export function generatePropertiesApi(id){
    dispatch(optionAction.generateProperties(id));
}

