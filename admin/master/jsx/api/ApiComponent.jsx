import MakeTheApiCall, {GenerateOptions} from './ApiCalls';
import {componentAction} from '../actions';
import store from '../store';
import * as types from '../actions/actionTypes';

const { dispatch } = store;
const SYSTEMS = 'systems/';

/**
 * Get components
 * @param id
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetComponentData(id) {

    var options = GenerateOptions(SYSTEMS + id + '/components', 'GET');

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.receiveData(response.data.response_data));

    });
};

/**
 * Get menus
 * @param id
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetMenuData(id) {

    var options = GenerateOptions(SYSTEMS + id + '/components/menu', 'GET');

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.menuDataSaved(response.data.response_data));

    });
};

/**
 * Get system
 * @param id
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetSystemComponent(id) {

    var options = GenerateOptions(SYSTEMS + id, 'GET');

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.receiveSystemData(response.data.response_data[0]));

    });
};

/**
 * Publish system
 * @param id
 * @param name
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiPublishSystemComponents(id, name){

    var data = JSON.stringify({
        system_id: id,
        name: name
    });

    var options = GenerateOptions(SYSTEMS + 'publish', 'POST', data);

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.dataPublished(response.data));

    });
}

/**
 * Get component grouped by menu
 * @param systemId
 * @param menuId
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetComponentsByMenu(systemId, menuId){

    var options = GenerateOptions(SYSTEMS + systemId + '/components/menu/' + menuId, 'GET');

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.receiveMenuComponentsData(response.data.response_data));

    });
}

/**
 * Get component types
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetComponentTypes() {

    var options = GenerateOptions('componentTypes', 'GET');

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.receivedComponentTypes(response.data));

    });
}

/**
 * Save component
 * @param systemid
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiSaveComponent(systemid, data) {

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemid + '/components', 'POST', data);

    dispatch(componentAction.savingData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.dataSaved(response.data));

    });
};

/**
 * Save menu
 * @param systemId
 * @param deletedItems
 * @param saveItems
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiSaveMenu(systemId, deletedItems, saveItems) {

    dispatch(componentAction.savingData());

    let deleteRequests = [];
    let _saveComponentsWithTraits = function (items) {
        for (let i = 0; i < items.length; i++) {
            if (items[i]) {
                delete items[i]['tmpid'];
            }
        }
        let data = JSON.stringify(items)
        var options = GenerateOptions(SYSTEMS + systemId + '/components/menu', 'POST', data);
        return MakeTheApiCall(options).then((response) => {
            dispatch(componentAction.menuDataSaved(response.data.response_data));
        });
    };

    if (deletedItems && deletedItems.length) {
        deletedItems.map((item) => {
            if (item.id) {
                deleteRequests.push(
                    MakeTheApiCall(GenerateOptions(SYSTEMS + systemId + '/components/' + item.id, 'DELETE'))
                )
            }
        });
    }
    if (deleteRequests.length) {
        return MakeTheApiCall(deleteRequests, true).then(function(res){
            _saveComponentsWithTraits(saveItems)
        })
    } else {
        _saveComponentsWithTraits(saveItems)
    }
};

/**
 * Save multiple traits of title component
 * @param systemId
 * @param componentId
 * @param deletedTraits
 * @param traits
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiSaveComponentTitleTraits(systemId,componentId,deletedTraits, traits) {

    dispatch(componentAction.savingData());

    let deleteRequests = [];
    let _saveTraits = function(traits){
        let data = JSON.stringify(traits)
        var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/multipleTraits', 'POST', data);
        return MakeTheApiCall(options).then((response) => {
            dispatch(componentAction.componentsUpdated());
        })
    };
    if (deletedTraits && deletedTraits.length) {
        deletedTraits.map((trait) => {
            if (trait.id) {
                deleteRequests.push(
                    MakeTheApiCall(GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/traits/' + trait.id, 'DELETE'))
                )
            }
        });
    }
    if (deleteRequests.length) {
        return MakeTheApiCall(deleteRequests, true).then(function(res){
            return _saveTraits(traits);
        })
    } else {
        return _saveTraits(traits);
    }
};

/**
 * Save multiple component traits
 * @param systemId
 * @param componentId
 * @param traits
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiSaveComponentTraits(systemId,componentId, traits) {

    let data = JSON.stringify(traits)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/multipleTraits', 'POST', data);

    dispatch(componentAction.savingData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.dataSaved(response.data));

    });
};

/**
 * Save one component trait
 * @param systemId
 * @param componentId
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiSaveComponentTrait(systemId, componentId, data){

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/traits', 'POST', data);

    dispatch(componentAction.savingData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.traitDataSaved(response.data));

    });
}

/**
 * Update component trait
 * @param systemId
 * @param componentId
 * @param traitId
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateComponentTrait(systemId, componentId, traitId, data){

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/traits/' + traitId, 'PUT', data);

    dispatch(componentAction.savingData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.traitDataSaved(response.data));

    });
}

/**
 * Add component
 * @param systemId
 * @param menuId
 * @param data
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiAddComponent(systemId, menuId, data){

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/menu/' + menuId, 'POST', data);

    dispatch(componentAction.savingData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.componentsUpdated(response.data));

    });
}

/**
 * Delete component
 * @param systemid
 * @param id
 * @param isColor
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiDeleteComponent(systemid, id, isColor) {

    isColor = isColor || false
    var options = GenerateOptions(SYSTEMS + systemid + '/components/' + id, 'DELETE');

    dispatch(componentAction.deletingData());

    return MakeTheApiCall(options).then(response => {

        (isColor)? dispatch(componentAction.deletedColorData()) : dispatch(componentAction.deleteData())

    });
};

/**
 * Update "Color" component
 * @param systemId
 * @param cId
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateColorComponent(systemId, cId){

    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + cId, 'GET');

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.colorComponentUpdated(response.data.response_data[0]))

    });
}

/**
 * Get windows
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetWindowComponents(){


    return MakeTheApiCall({method:'GET',url:'../../server/window.json'}).then(response => {

        dispatch(componentAction.setWindowComponent(response.data))

    });
}

/**
 * Get component parts
 * @param systemId
 * @param componentId
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiGetComponentParts(systemId,componentId) {

    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/parts', 'GET');

    dispatch(componentAction.requestData());

    return MakeTheApiCall(options).then(response => {

        dispatch(componentAction.receivePartsData(response.data.response_data, componentId))
        return response.data.response_data
    });
}

/**
 * Update option parts
 * @param systemId
 * @param componentId
 * @param optionId
 * @param data
 * @param partId
 * @param existing_parts
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateOptionsParts (systemId, componentId, optionId, data, partId, existing_parts) {

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/options/' + optionId + '/parts/' + partId, 'PUT', data);

    dispatch({
        type: types.SAVING_OPTION_PARTS
    });

    return MakeTheApiCall(options).then(response => {

        dispatch({
            type: types.SAVED_OPTION_PARTS,
            data: existing_parts.map(part => {
                if(response.data.response_data[0].id == part.id) {
                    return response.data.response_data[0];
                } else {
                    return part;
                }
            }),
            optionID: optionId
        });

    });
}

/**
 * Update component parts
 * @param systemId
 * @param componentId
 * @param data
 * @param partId
 * @param existing_parts
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiUpdateComponentsParts (systemId, componentId, data, partId, existing_parts) {

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/parts/' + partId, 'PUT', data);

    dispatch({
        type: types.SAVING_COMPONENT_PARTS
    });

    return MakeTheApiCall(options).then(response => {

        dispatch({
            type: types.SAVED_COMPONENT_PARTS,
            data: existing_parts.map(part => {
                if(response.data.response_data[0].id == part.id) {
                    return response.data.response_data[0];
                } else {
                    return part;
                }
            }),
            componentID: componentId
        });

    });
}

/**
 * Add part to options
 * @param systemId
 * @param componentId
 * @param optionId
 * @param data
 * @param existing_parts
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiAddPartToOptions (systemId, componentId, optionId, data, existing_parts) {

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/options/' + optionId + '/parts', 'POST', data);

    dispatch({
        type: types.SAVING_OPTION_PARTS
    });

    return MakeTheApiCall(options).then(response => {

        dispatch({
            type: types.SAVED_OPTION_PARTS,
            data: existing_parts.concat(response.data.response_data[0]),
            optionID: optionId
        });

    });
}

/**
 * Add part to components
 * @param systemId
 * @param componentId
 * @param data
 * @param existing_parts
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiAddPartToComponents (systemId, componentId, data, existing_parts) {

    data = JSON.stringify(data)
    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/parts', 'POST', data);

    dispatch({
        type: types.SAVING_COMPONENT_PARTS
    });

    return MakeTheApiCall(options).then(response => {

        dispatch({
            type: types.SAVED_COMPONENT_PARTS,
            data: existing_parts.concat(response.data.response_data[0]),
            componentID: componentId
        });

    });
}

/**
 * Delete option parts
 * @param systemId
 * @param componentId
 * @param optionId
 * @param partId
 * @param existing_parts
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiDeleteOptionsParts (systemId, componentId, optionId, partId, existing_parts) {

    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/options/' + optionId + '/parts/' + partId, 'DELETE');

    dispatch({
        type: types.SAVING_OPTION_PARTS
    });

    return MakeTheApiCall(options).then(response => {

        dispatch({
            type: types.SAVED_OPTION_PARTS,
            data: existing_parts.filter(part => {
                return part.id != partId
            }),
            optionID: optionId
        });

    });
}

/**
 * Delete component parts
 * @param systemId
 * @param componentId
 * @param partId
 * @param existing_parts
 * @returns {axios.Promise}
 * @constructor
 */
export function ApiDeleteComponentsParts (systemId, componentId, partId, existing_parts) {

    var options = GenerateOptions(SYSTEMS + systemId + '/components/' + componentId + '/parts/' + partId, 'DELETE');

    dispatch({
        type: types.SAVING_COMPONENT_PARTS
    });

    return MakeTheApiCall(options).then(response => {

        dispatch({
            type: types.SAVED_COMPONENT_PARTS,
            data: existing_parts.filter(part => {
                return part.id != partId
            }),
            componentID: componentId
        });

    });
}