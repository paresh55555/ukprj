import axios from 'axios';
import * as constant from '../constant.js'

// removed axios interceptros whcihc redirect page to login on every error causing essues with returning error

// axios.interceptors.response.use(function (response) {
//     // Do something with response data

//     console.log(response, 'interceptors')

//     if(response.data.status== 'error'){
//       sessionStorage.error = JSON.stringify(response.data);
//       return browserHistory.replace(constant.SQ_PREFIX+'login' )
//     }
//     return response;
//   }, function (error) {

//     console.log(error)

//     // Do something with response error
//     sessionStorage.error = JSON.stringify(error.data)
//     // if(error.status==401){
//     // 	return browserHistory.replace(constant.SQ_PREFIX+'login' )
//     // }
//     // else{
//     //   return browserHistory.replace(constant.SQ_PREFIX+'error' )
//     // }
//     return browserHistory.replace(constant.SQ_PREFIX+'login' )
//     return Promise.reject(error);
//   });

export function loggedIn(){
	if(!!sessionStorage.token){
		axios.defaults.headers.common['Authorization'] = 'Bearer '+ sessionStorage.token;
	}
	return !!sessionStorage.token
}
export function logout(){
	delete sessionStorage.token;
  	window.location.reload();
}
 
