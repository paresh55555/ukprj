import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import reducers from '../reducers';
import cartMiddleware from '../middlewares/cartMiddleware';

const logger = createLogger();
const middlewarer = routerMiddleware(browserHistory)
const configureStore = (initialState = {}) => {
    const middleware = applyMiddleware(thunk, middlewarer, logger, cartMiddleware);
    const store = createStore(reducers, middleware);
    if (module.hot) {
        module.hot.accept('../reducers', () => {
            store.replaceReducer(require('../reducers').default);
        });
    }
    return store;
};

export default configureStore();
