import * as types from '../actions/actionTypes';

export default function addresses (state = {
        addressList: [],
        currentAddress: {}
    }, action = {}) {

    switch(action.type) {

        case types.SET_ADDRESS_LIST:
            return {...state, addressList: action.payload}
        case types.SET_CURRENT_ADDRESS:
            return {...state, currentAddress: action.payload}
        default:
            return state;

    }

}