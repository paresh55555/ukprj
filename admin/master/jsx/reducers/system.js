import * as types from '../actions/actionTypes';

export function systemReducer(state = {
	isLoading: false,
	isSaving: false,
	isSaved: false,
	isDeleted: false,
    isPublished: false,
    sampleDoorPrice : [],
    systemGroups: [],
    panelRanges : [],
    sampleDoors: [],
    premadeSystem:[],
	error: false,
}

, action = null) {
	switch(action.type) {
		
		case types.SYSTEM_ERROR:
			return Object.assign({}, state, {isLoading: false, data: action.data, error: true});
		case types.GET_SYSTEM_GROUPS_SUCCESS:
			return Object.assign({}, state, {isLoading: false, systemGroups: action.data, isSaving:false, error: false, isDeleted:false });
		case types.GET_SYSTEM_GROUPS:
			return Object.assign({}, state, {isLoading: true, error: false, isSaving:false, isSaved:false,isDeleted:false });
		case types.GET_SYSTEM_DETAILS_SUCCESS:
			return Object.assign({}, state, {isLoading: false, error: false, isSaving:false });
		case types.SAVING_SYSTEM_DATA:
			return Object.assign({},state, {isLoading:true,error:false, isSaving:true,isSaved:false});
		case types.SAVED_SYSTEM_DATA:
			return Object.assign({},state, {isLoading:false,error:false, isSaving:false,isSaved:true});
        case types.PUBLISHED_SYSTEM_GROUP:
            return Object.assign({},state, {isLoading:false, error:false, isSaving:false, isSaved:false, isDeleted:false, isPublished:true});
		case types.DELETED_SYSTEM_GROUP:
            return Object.assign({},state, {isLoading:false,error:false, isSaving:false,isSaved:false,isDeleted:true});
	  	case types.GET_PANEL_RANGES_SUCCESS:
			return Object.assign({},state, {panelRanges:action.data,isLoading:false});
		case types.GET_SAMPLE_DOORS_SUCCESS:
			return Object.assign({},state, {sampleDoors:action.data,isLoading:false});
		case types.GET_SAMPLE_DOOR_PRICE_SUCCESS:
			return Object.assign({},state, {sampleDoorPrice:action.data,isLoading:false});
		case types.GET_PREMADE_SYSTEMS_SUCCESS:
			return Object.assign({},state,{premadeSystem:action.data});
		default:
			return state;
	}
};
