import * as types from '../actions/actionTypes';

export function serviceCustomerReducer(
    state = {
        isLoading: false,
        customers: [],
        currentCustomer:{},
        customerOrders: [],
        customerJobs: [],
        isOpen:false,

    }
    , action = null) {
    switch (action.type) {
        case types.GET_SERVICE_CUSTOMERS:
            return Object.assign({}, state, { isLoading: true, error: false });
        case types.SET_SERVICE_CUSTOMERS:
            return Object.assign({}, state, { customers: action.payload, isLoading: false, error: false });
        case types.SET_SERVICE_CUSTOMER_ORDERS:
            return Object.assign({}, state, { customerOrders: action.payload, isLoading: false, error: false });
        case types.SET_SERVICE_CUSTOMER_JOBS:
            return Object.assign({}, state, { customerJobs: action.payload, isLoading: false, error: false });
        case types.TOGGLE_CUSTOMER_ADDRESS:
            return Object.assign({}, state, {isOpen: action.payload.isOpen, identifier: action.payload.identifier});
        case types.SET_CURRENT_CUSTOMER:
            return Object.assign({}, state, {currentCustomer: action.payload});
        default:
            return state;
    }
};

