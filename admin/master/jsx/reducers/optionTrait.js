import * as types from '../actions/actionTypes';

export function optionTraitReducer(state = {
    isLoading: false,
    isSaving:false,
    isSaved:false,
    optionTrait: [],
    traits: [],
    optionId: null,
    deletedId: null,
    error: false}
    , action = null) {
    switch(action.type) {
        case types.OPTION_TRAITS_ERROR:
            return Object.assign({}, state, {isLoading: false, data: action.data, error: true});
        case types.GET_OPTION_TRAITS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, optionTrait: action.data, optionId: action.optionId, error: false, isDeleted:false });
        case types.GET_OPTION_TRAITS:
            return Object.assign({}, state, {isLoading: true, error: false, traits: [],system:[],isSaved:false, isDeleted:false });
        case types.SAVING_OPTION_TRAITS_DATA:
            return Object.assign({}, state, {isLoading:false, error:false, traits: [], isSaving:true,isSaved:false, isDeleted:false});
        case types.SAVED_OPTION_TRAITS_DATA:
            return Object.assign({}, state, {isLoading:false, optionId: action.optionId, traits: action.data, error:false,isSaving:false,isSaved:true, isDeleted:false});
        case types.DELETED_OPTION_TRAITS_DATA:
            return Object.assign({},state, {isLoading: false, error:false, deletedId: action.traitId, optionId: action.optionId, isDeleted:true});
        default:
            return state;
    }
};