import * as types from '../actions/actionTypes';

export function optionReducer(state = {
    isLoading: false,
    list: [],
    isSaving:false,
    isSaved:false,
    isDeleted: false,
    deletedId: null,
    option: [],
    savedOption: [],
    parts: [],
    price: [],
    multiples: false,
    visible: true,
    costs: {},
    propertiesOptions: {
        id: null,
        propertiesValue: []
    },
    hasDefault: false,
    guestsAllow: true,
    componentDropdownState: {
        base: {
            multiples: false,
            visible: true,
            hasDefault: false,
            guestsAllow: true
        }
    },
    error: false}
    , action = null) {
    switch(action.type) {
        case types.OPTIONS_ERROR:
            return Object.assign({}, state, {isLoading: false, data: action.data, error: true});
        case types.GET_OPTIONS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, option: action.data, error: false, isSaving:false, isSaved:false, isDeleted:false });
        case types.GET_OPTION_PARTS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, parts: action.data, optionID: action.optionID, error: false, isSaving:false, isSaved:false, isDeleted:false });
        case types.GET_OPTION_COSTS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, costs: Object.assign({}, state.costs, {[action.optionID]: action.data}), optionID: action.optionID, error: false, isSaving:false, isSaved:false, isDeleted:false });
        case types.SAVING_OPTION_PARTS:
            return Object.assign({}, state, {isLoading: false, parts: {}, optionID: null, error: false, isSaving: true, isSaved: false, isDeleted: false });
        case types.SAVED_OPTION_PARTS:
            return Object.assign({}, state, {isLoading: false, parts: action.data, optionID: action.optionID, error: false, isSaving: false, isSaved: true, isDeleted: false });
        case types.SAVING_OPTION_COSTS_DATA:
            return Object.assign({}, state, {isLoading: false, error: false, isSaving: true, isSaved: false, isDeleted: false });
        case types.SAVED_OPTION_COSTS_DATA:
            return Object.assign({}, state, {isLoading: false, costs: Object.assign({}, state.costs, {[action.optionId]: [...state.costs[action.optionId], action.data]}), optionID: action.optionId, error: false, isSaving: false, isSaved: true, isDeleted: false });
        case types.UPDATE_OPTION_COSTS_DATA:
            const items = [...state.costs[action.optionId]]
            const index = state.costs[action.optionId].findIndex(item=>item.id === action.data.id)
            items[index] = action.data;
            return Object.assign({}, state, {
                isLoading: false, costs: Object.assign({}, state.costs, {[action.optionId]: items}), optionID: action.optionId, error: false, isSaving: false, isSaved: true, isDeleted: false });
        case types.DELETED_COST:
            const prevItems = [...state.costs[action.optionId]]
            const i = state.costs[action.optionId].findIndex(item=>item.id === action.data)
            prevItems.splice(i, 1);
            return Object.assign({},state, {isLoading:false, error:false, costs: Object.assign({}, state.costs, {[action.optionId]: prevItems}), isSaved:false, isDeleted:true});
        case types.GET_OPTION_PRICE_SUCCESS:
            return Object.assign({}, state, {isLoading: false, price: action.data, optionID: action.optionID, error: false, isSaving:false, isSaved:false, isDeleted:false });
        case types.GET_OPTIONS:
            return Object.assign({}, state, {isLoading: true, error: false, list:[],system:[],option: [], isSaving:false, isSaved:false, isDeleted:false });
        case types.SAVING_OPTIONS_DATA:
            return Object.assign({},state, {isLoading:false, error:false, savedOption:[], list:[], isSaving:true, isSaved:false, isDeleted:false});
        case types.SAVED_OPTIONS_DATA:
            return Object.assign({},state, {isLoading:false, error:false, savedOption: action.data, list:[], isSaving:false, isSaved:true, isDeleted:false});
        case types.DELETED_OPTIONS_DATA:
            return Object.assign({},state, {isLoading: false, error:false, deletedId:action.optionId, isSaving:false, isSaved:false, isDeleted:true});
        case types.ALLOW_MULTIPLE_CHECK:
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]:
                    Object.assign({}, state.componentDropdownState['id'+action.id], {multiples: !state.componentDropdownState['id'+action.id].multiples})})})
        case types.VISIBLE_CHECK:
                return Object.assign({},state, {componentDropdownState:
                    Object.assign({},state.componentDropdownState, {['id'+action.id]:
                        Object.assign({},state.componentDropdownState['id'+action.id], {
                            visible: !state.componentDropdownState['id'+action.id].visible,
                            hasDefault:!!state.componentDropdownState['id'+action.id].visible ? true : state.componentDropdownState['id'+action.id].hasDefault
                            })})});
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]:
                    Object.assign({}, state.componentDropdownState['id'+action.id], {visible: true})})});
        case types.HAS_DEFAULT_CHECK:
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]:
                    Object.assign({},state.componentDropdownState['id'+action.id], {hasDefault: !state.componentDropdownState['id'+action.id].hasDefault})})});
        case types.ALLOW_GUESTS_CHECK:
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]:
                    Object.assign({}, state.componentDropdownState['id'+action.id], {guestsAllow: !state.componentDropdownState['id'+action.id].guestsAllow})})});
        case types.BASE_PROPERTIES:
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]: state.componentDropdownState.base})});
        case types.GENERATE_PROPERTIES:
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]:
                    Object.assign({}, state.componentDropdownState.base, {id: action.id})})});
        case types.CHANGE_EDITED:
            return Object.assign({},state, {componentDropdownState:
                Object.assign({},state.componentDropdownState, {['id'+action.id]:
                    Object.assign({}, state.componentDropdownState['id'+action.id])})});
        default:
            return Object.assign({},state)
    }
};