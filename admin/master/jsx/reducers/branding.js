import * as types from '../actions/actionTypes';

export function brandingReducer(state = {
	isLoading: true,
	isSaving:false,
	branding: {}
}
, action = null) {
	switch(action.type) {
		case types.GET_BRANDING:
			return Object.assign({}, state, {isLoading: true});
		case types.GET_BRANDING_SUCCESS:
			return Object.assign({}, state, {isLoading: false, isSaving:false, branding: action.data});
		case types.SAVE_BRANDING:
			return Object.assign({}, state, {isSaving: true});
		case types.DELETE_BRANDING:
			return Object.assign({}, state, {isdeleting: true});
		case types.DELETE_BRANDING_SUCCESS:
			return Object.assign({}, state, {isdeleting: false, isDeleted: true});
		default:
			return state;
	}
};