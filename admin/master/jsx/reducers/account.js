import * as types from '../actions/actionTypes';

export function accountReducer(state = {
        isLoading: true,
        isSaving: false,
        accounts: [],
        accountData: [],
        accountBuilders: []
    }, action = null) {
    switch (action.type) {
        case types.GET_ACCOUNTS:
            return Object.assign({}, state, {isLoading: true});
        case types.GET_ACCOUNTS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, isSaving: false, accounts: action.data});
        case types.GET_ACCOUNT_DATA_SUCCESS:
            return Object.assign({}, state, {isLoading: false, isSaving: false, accountData: action.data});
        case types.GET_ACCOUNT_BUILDERS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, isSaving: false, accountBuilders: action.data});
        case types.SAVE_ACCOUNT:
            return Object.assign({}, state, {isSaving: true});
        case types.DELETE_ACCOUNT:
            return Object.assign({}, state, {isDeleting: true});
        case types.DELETE_ACCOUNT_SUCCESS:
            return Object.assign({}, state, {isDeleting: false, isDeleted: true});
        default:
            return state;
    }
};