import * as types from '../actions/actionTypes';

export default function cartReducer(state = {
    isEdit: false,
    isSingleEdit: false,
    isLoading: false,
    isSaving: false,
    list: [],
    currentList: null,
    currentListType: '',
    builderDetails:{},
    column:{
            statuses: [],
            columns: [],
        },
    sampleData: [{
        first_name: "John",
        last_name: "Doe",
        company: "Temp Inc"
    }, {
        first_name: "Jane",
        last_name: "Doe",
        company: "Temp Inc"
    }],
}, action = null) {
    switch (action.type) {
        case types.GET_LIST_BUILDER:
            return Object.assign({}, state, { list: action.data });
        case types.SET_BUILDER_DETAILS:
            return Object.assign({},state, {builderDetails:action.data,isLoading:false})
        case types.SET_SAMPLE_DATA:
            return Object.assign({},state, {sampleData:action.data})
        case types.SET_CURRENT_LIST:
            return Object.assign({}, state, { currentList: action.data });
        case types.SET_CURRENT_LIST_TYPE:
            return Object.assign({}, state, { currentListType: action.data });
        case types.GET_LIST_COLUMN:
            return Object.assign({}, state, { column: action.data });
        case types.SET_LIST_SAVING_STATUS:
            return Object.assign({}, state, { isSaving: action.data });
        case types.SET_LIST_LOADING_STATUS:
            return Object.assign({}, state, { isLoading: action.data });
        default:
            return state;
        }
}

// CHANGE_LIST_ORDER
