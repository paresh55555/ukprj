import * as types from '../actions/actionTypes';

export function permissionReducer(state = {
    isLoading: false,
    list: [],
    error: false}
    , action = null) {
    switch(action.type) {
        case types.GET_PERMISSION_ERROR:
            return Object.assign({}, state, {isLoading: false, list: action.data, error: true});
        case types.GET_PERMISSION_SUCCESS:
            return Object.assign({}, state, {isLoading: false, list: action.data, error: false});
        case types.GET_PERMISSION:
            return Object.assign({}, state, {isLoading: true, error: false });
        default:
            return state;
    }
};