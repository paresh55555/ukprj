import * as types from '../actions/actionTypes';

export function moduleReducer(state = {
	isLoading: false,
	isSaving:false,
	isSaved:false,
	isDeleted:false,
	saveError: false,
	list: [],
	listDetail:[],
	table_data:[],
	min_max:{},
	detail:{},
	error: false,
	error_msg:''
}
, action = null) {
	switch(action.type) {
		case types.GET_MODULE_ERROR:
			return Object.assign({}, state, {isLoading: false, list: action.data, error: true,table_data:[],listDetail:{},min_max:{}});
		case types.GET_MODULE_SUCCESS:
			return Object.assign({}, state, {isLoading: false, list: action.data, error: false, detail:{} });
		case types.GET_MODULE_LIST_SUCCESS:
			return Object.assign({}, state, {isLoading: false, list:[],listDetail:action.data, error: false, detail:{} });
		case types.GET_MODULE_TABLE_SUCCESS:
			return Object.assign({}, state, {isLoading: false, list:[],listDetail:{},table_data:action.data, error: false, detail:{}, isDeleted:false,isSaving:false, isSaved:false});
		case types.GET_MODULE_MINMAX_SUCCESS:
			return Object.assign({}, state, {isLoading: false, list:[],listDetail:{},table_data:[], error: false, detail:{},min_max:action.data });
		case types.GET_MODULE:
			return Object.assign({}, state, {isLoading: true, error: false });
		case types.GET_MODULE_DETAIL_SUCCESS:
			return Object.assign({},state,{isLoading: false,error:false,detail:action.data})
		case types.SAVING_MODULE_DATA:
			
			return Object.assign({},state,{isSaving: true,saveError:false,isSaved:false,isLoading:false})
		case types.SAVED_MODULE_DATA:
			return Object.assign({},state,{isSaving: false,saveError:false,isSaved:true})
		case types.DELETED_MODULE_DATA:
			return Object.assign({},state,{isSaving: false,saveError:false,isDeleted:true})
		case types.MODULE_ERROR :
			return Object.assign({},state,{isSaving: false,saveError:true,isDeleted:false,error_msg:action.data.message})
		default:
			return state;
	}
};