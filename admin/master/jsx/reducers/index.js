import { routerReducer } from 'react-router-redux';
import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';
import { exampleReducer } from './example';
import { appReducer } from './app';
import { authReducer } from './auth';
import userReducer from './user';
import { companyReducer } from './company';
import { permissionReducer } from './permission';
import { moduleReducer } from './module';
import { componentReducer } from './component';
import { systemReducer } from './system';
import { optionReducer } from './option';
import { optionTraitReducer } from './optionTrait';
import { premadeReducer } from './premade';
import { brandingReducer } from './branding';
import { costReducer } from './cost';
import { reportsReducer } from './reports';
import defaultsReducer from './defaults';
import cartReducer from './cart';
import list from './list';
import { accountReducer } from './account';
import { serviceReducer } from './service';
import { serviceJobReducer } from './serviceJob';
import { serviceCustomerReducer } from './serviceCustomer';
import addresses from './addresses';

const rootReducer = combineReducers({
    routing: routerReducer,
    toastr: toastrReducer,
    example: exampleReducer,
    app: appReducer,
    auth: authReducer,
    user: userReducer,
    company: companyReducer,
    permission: permissionReducer,
    module: moduleReducer,
    component: componentReducer,
    system: systemReducer,
    option: optionReducer,
    optionTrait: optionTraitReducer,
    premade: premadeReducer,
    branding: brandingReducer,
    cost: costReducer,
    reports: reportsReducer,
    defaults: defaultsReducer,
    cart: cartReducer,
    list,
    account: accountReducer,
    service: serviceReducer,
    serviceJob: serviceJobReducer,
    serviceCustomer: serviceCustomerReducer,
    addresses
});

export default rootReducer;
