import * as types from '../actions/actionTypes';

export function componentReducer(state = {
	isLoading: false,
	list: [],
	menuList: [],
	isSaving:false,
	isDeleted:false,
	isSaved:false,
    isPublished: false,
	updateList:false,
	updateComponent: false,
	component:{},
	system:{},
	trait:{},
	component_types:[],
	components_list:[],
	edit_mode:false,
	premadeData: null,
	window:[],
	parts:[],
	costs:[],
	componentID: null,
	error: false}
, action = null) {
	switch(action.type) {
		case types.COMPONENT_EDIT_MODE:
			return Object.assign({}, state, {edit_mode:action.data});
		case types.COMPONENT_PREMADE_MODE:
			return Object.assign({}, state, {premadeData:action.data});
        case types.COLOR_COMPONENT_UPDATED:
            return Object.assign({},state, {updateComponent: true, newData: action.data})
		case types.COMPONENT_ERROR:
			return Object.assign({}, state, {isLoading: false, data: action.data, error: true, updateList:false});
		case types.GET_COMPONENT_SUCCESS:
			return Object.assign({}, state, {isLoading: false, list: action.data, error: false, isDeleted:false, updateList:false });
		case types.GET_SYSTEM_COMPONENT_SUCCESS:
			return Object.assign({}, state, {isLoading: false, system: action.data, error: false,isDeleted:false, updateList:false });
		case types.GET_COMPONENT_TYPES_SUCCESS:
			return Object.assign({}, state, {isLoading: false,component_types:action.data, error: false,isDeleted:false, updateList:false });
		case types.GET_COMPONENTS_BY_MENU_SUCCESS:
			return Object.assign({}, state, {isLoading: false,components_list:action.data, error: false,isDeleted:false, updateList:false });
		case types.GET_COMPONENT:
			return Object.assign({}, state, {isLoading: true, error: false, isSaved:false, isDeleted:false, updateList:false });
		case types.GET_COMPONENT_DETAILS_SUCCESS:
			return Object.assign({}, state, {isLoading: false, error: false, component:action.data, updateList:false,isSaving:false });
		case types.SAVING_COMPONENT_DATA:
			return Object.assign({},state, {isLoading:false,error:false, isSaving:true,isSaved:false,updateList:false})
		case types.SAVED_COMPONENT_DATA:
			return Object.assign({},state, {isLoading:false,error:false, isSaving:false, component: action.data, updateList:false,isSaved:true})
		case types.SAVING_COMPONENT_COSTS_DATA:
			return Object.assign({},state, {isLoading: false, costs: [], componentID: null, error: false, isSaving: true, isSaved: false, isDeleted: false })
		case types.SAVED_COMPONENT_COSTS_DATA:
			return Object.assign({},state, {isLoading: false, costs: action.data, componentID: action.componentID, error: false, isSaving: false, isSaved: true, isDeleted: false })
		case types.SAVED_COMPONENT_TRAIT_DATA:
			return Object.assign({},state, {isLoading:false,error:false, isSaving:false, trait: action.data, updateList:false,isSaved:true})
		case types.SAVED_MENU_COMPONENT_DATA:
			return Object.assign({},state, {isLoading:false,error:false, menuList:action.data, isSaving:false,updateList:false,isSaved:true})
		case types.COMPONENTS_UPDATED:
			return Object.assign({},state, {isLoading:false,error:false,isSaving:false,updateList:true,isSaved:true})
		case types.PUBLISHED_SYSTEM_COMPONENTS:
            return Object.assign({},state, {isLoading:false, error:false, isSaving:false, updateList:false, isSaved:false, isDeleted:false, isPublished:true})
    	case types.DELETING_COMPONENT_DATA:
        	return Object.assign({},state, {isSaving: false,isLoading:false,saveError:false,updateList:false,isDeleted:false})
		case types.DELETED_COMPONENT_DATA:
			return Object.assign({},state, {isSaving: false,isLoading:false,saveError:false,updateList:true,isDeleted:true})
    	case types.DELETED_COLOR_COMPONENT_DATA:
        	return Object.assign({},state, {isSaving: false,isLoading:false,saveError:false,updateList:false,isDeleted:true})
    	case types.SET_WINDOW_COMPONENT:
    		return Object.assign({},state,{window:action.data})
        case types.GET_COMPONENT_PARTS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, parts: action.data, componentID: action.componentID, error: false, isSaving:false, isSaved:false, isDeleted:false });
        case types.GET_COMPONENT_COSTS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, costs: action.data, componentID: action.componentID, error: false, isSaving:false, isSaved:false, isDeleted:false });
		case types.SAVING_COMPONENT_PARTS:
            return Object.assign({}, state, {isLoading: false, parts: [], componentID: null, error: false, isSaving: true, isSaved: false, isDeleted: false });
        case types.SAVED_COMPONENT_PARTS:
            return Object.assign({}, state, {isLoading: false, parts: action.data, componentID: action.componentID, error: false, isSaving: false, isSaved: true, isDeleted: false });
		default:
			return state;
	}
};