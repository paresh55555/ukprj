import * as types from '../actions/actionTypes';

export function companyReducer(state = {
	isLoading: false,
	isSaving:false,
	isSaved:false,
	isDeleted:false,
	saveError: false,
	list: [],
	detail:{},
	usersList:[],
	listByOrder:[],
	error: false}
, action = null) {
	switch(action.type) {
		case types.GET_COMPANY_ERROR:
			return Object.assign({}, state, {isLoading: false, list: action.data, error: true});
		case types.GET_COMPANY_SUCCESS:
			return Object.assign({}, state, {isLoading: false, list: action.data, error: false, detail:{}, isDeleted:false,isSaving:false, isSaved:false });
		case types.GET_COMPANY_BYORDER_SUCCESS:
			return Object.assign({}, state, {listByOrder:action.data});
		case types.GET_COMPANY:
			return Object.assign({}, state, {isLoading: true, error: false });
		case types.GET_COMPANY_DETAIL_SUCCESS:
			return Object.assign({},state,{isLoading: false,error:false,detail:action.data})
		case types.SAVING_COMPANY_DATA:
			return Object.assign({},state,{isSaving: true,saveError:false,isSaved:false})
		case types.SAVED_COMPANY_DATA:
			return Object.assign({},state,{isSaving: false,saveError:false,isSaved:true,list:[]})
		case types.GET_COMPANY_USER_DETAIL_SUCCESS:
			return Object.assign({},state,{isLoading: false,usersList:action.data})
		case types.DELETED_COMPANY_DATA:
			return Object.assign({},state,{isSaving: false,saveError:false,isDeleted:true})
		default:
			return state;
	}
};