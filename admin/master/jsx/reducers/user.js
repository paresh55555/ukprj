import * as types from '../actions/actionTypes';

export default function userReducer(state = {
    isLoading: false,
    isSaving: false,
    isSaved: false,
    isDeleted: false,
    saveError: false,
    list: [],
    detail: {},
    error: false,
}, action = null) {
    switch (action.type) {
    case types.RECEIVE_USERS:
        return Object.assign({}, state, {isLoading: false, list: action.data, detail: {}, isDeleted: false, isSaving: false, isSaved: false });
    case types.GET_USER:
        return Object.assign({}, state, { isLoading: true });
    case types.GET_USER_DETAIL_SUCCESS:
        return Object.assign({}, state, { isLoading: false, detail: action.data });
    case types.SAVING_USER_DATA:
        return Object.assign({}, state, { isSaving: true, isSaved: false });
    case types.SAVED_USER_DATA:
        return Object.assign({}, state, { isSaving: false, isSaved: true });
    case types.DELETED_USER_DATA:
        return Object.assign({}, state, { isSaving: false, isDeleted: true });
    case types.PREP_USER_TABLE_FOR_DATA:
        return Object.assign({}, state, { isLoading: true });
    default:
        return state;
    }
};