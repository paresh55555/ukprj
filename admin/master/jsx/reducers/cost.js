import * as types from '../actions/actionTypes';

export function costReducer(state = {
    isLoading: false,
    isSaving: false,
    cost: {},
    savedCost: {},
    deletedCostId: null
}
, action = null) {
    switch (action.type) {
        case types.GET_COSTS:
            return Object.assign({}, state, {isLoading: true, isSaved:false});
        case types.GET_COSTS_SUCCESS:
            return Object.assign({}, state, {isLoading: false, isSaving: false, cost: action.data, isSaved:false});
        case types.SAVED_COSTS_DATA:
            return Object.assign({},state, {isLoading:false, error:false, savedCost: action.data, isSaved:true, isDeleted:false});
        // case types.DELETED_COST:
        //     return Object.assign({},state, {isLoading:false, error:false, deletedCostId: action.data, isSaved:false, isDeleted:true});
        default:
            return state;
    }
};