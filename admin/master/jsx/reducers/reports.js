/**
 * Created by admin12345678 on 07.06.17.
 */
import * as types from '../actions/actionTypes';

export function reportsReducer(state = {
    isLoading: false,
    isSaving: false,
    isSaved: false,
    isDeleted: false,
    isPublished: false,
    salesReports: [],
    deliveredOrdersReports: [],
    error: false}
    , action = null) {
    switch(action.type) {
        case types.GET_SALES_REPORTS_REQUEST:
            return Object.assign({}, state, { isLoading: true });
        case types.GET_SALES_REPORTS_SUCCESS:
            return Object.assign({}, state, { isLoading: false, salesReports: action.data });
        case types.GET_DELIVERED_ORDERS_REPORTS_REQUEST:
            return Object.assign({}, state, { isLoading: true });
        case types.GET_DELIVERED_ORDERS_REPORTS_SUCCESS:
            return Object.assign({}, state, { isLoading: false, deliveredOrdersReports: action.data });
        default:
            return state;
    }
};