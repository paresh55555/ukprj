import * as types from '../actions/actionTypes';

export default function defaultsReducer(state = {
    isLoading: false,
    customerDefaults: [],
    customerDefaultsOriginal: [],
    siteDefaults: [],
    siteDefaultsOriginal: [],
}, action = null) {
    const customerDefaultsCopy = [...state.customerDefaults];
    const currentPlaceholderIndex = customerDefaultsCopy.findIndex(item => item.id === action.id);
    const siteDefaultsCopy = [...state.siteDefaults];
    const currentSiteDataIndex = siteDefaultsCopy.findIndex(item => item.id === action.id);
    switch (action.type) {
    case types.CHANGE_CUSTOMER_PLACEHOLDER:
        customerDefaultsCopy[currentPlaceholderIndex] = Object.assign({}, customerDefaultsCopy[currentPlaceholderIndex], { placeholder: action.data });
        return Object.assign({}, state, { customerDefaults: [...customerDefaultsCopy] });
    case types.CHANGE_CUSTOMER_TITLE:
        customerDefaultsCopy[currentPlaceholderIndex] = Object.assign({}, customerDefaultsCopy[currentPlaceholderIndex], { title: action.data });
        return Object.assign({}, state, { customerDefaults: [...customerDefaultsCopy] });
    case types.CHANGE_CUSTOMER_DISPLAY:
        customerDefaultsCopy[currentPlaceholderIndex] = Object.assign({}, customerDefaultsCopy[currentPlaceholderIndex], { display: action.data });
        return Object.assign({}, state, { customerDefaults: [...customerDefaultsCopy] });
    case types.CHANGE_CUSTOMER_REQUIRED:
        customerDefaultsCopy[currentPlaceholderIndex] = Object.assign({}, customerDefaultsCopy[currentPlaceholderIndex], { required: action.data });
        return Object.assign({}, state, { customerDefaults: [...customerDefaultsCopy] });
    case types.CHANGE_CUSTOMER_ERROR:
        customerDefaultsCopy[currentPlaceholderIndex] = Object.assign({}, customerDefaultsCopy[currentPlaceholderIndex], { error: action.data });
        return Object.assign({}, state, { customerDefaults: [...customerDefaultsCopy] });
    case types.REQUEST_DEFAULTS:
        return Object.assign({}, state, { isLoading: true });
    case types.RECEIVE_SITE_DEFAULTS:
        return Object.assign({}, state, { isLoading: false, siteDefaults: [...action.data], siteDefaultsOriginal: [...action.data] });
    case types.BACK_TO_ORIGINAL_DEFAULTS:
        return Object.assign({}, state, { siteDefaults: [...state.siteDefaultsOriginal] });
        case types.CHANGE_SITE_DEFAULTS:
        return Object.assign({}, state, { isLoading: false, siteDefaults: siteDefaultsCopy, siteDefaultsOriginal: [...siteDefaultsCopy] });
    case types.RECEIVE_CUSTOMER_DEFAULTS:
        return Object.assign({}, state, { isLoading: false, customerDefaults: [...action.data.reverse()], customerDefaultsOriginal: [...action.data.reverse()] });
    case types.BACK_TO_ORIGINAL_CUSTOMER:
        return Object.assign({}, state, { customerDefaults: [...state.customerDefaultsOriginal] });
        case types.CHANGE_CUSTOMER_DEFAULTS:
        return Object.assign({}, state, { isLoading: false, customerDefaults: customerDefaultsCopy, customerDefaultsOriginal: [...customerDefaultsCopy] });
    case types.CHANGE_DATE_FORMAT:
        siteDefaultsCopy[currentSiteDataIndex] = Object.assign({}, siteDefaultsCopy[currentSiteDataIndex], { value: action.data });
        return Object.assign({}, state, { siteDefaults: [...siteDefaultsCopy] });
    case types.CHANGE_TIME_FORMAT:
        siteDefaultsCopy[currentSiteDataIndex] = Object.assign({}, siteDefaultsCopy[currentSiteDataIndex], { value: action.data });
        return Object.assign({}, state, { siteDefaults: [...siteDefaultsCopy] });
    case types.CHANGE_CURRENCY:
        siteDefaultsCopy[currentSiteDataIndex] = Object.assign({}, siteDefaultsCopy[currentSiteDataIndex], { value: action.data });
        return Object.assign({}, state, { siteDefaults: [...siteDefaultsCopy] });
    default:
        return state;
    }
}

