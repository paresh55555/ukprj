import * as types from '../actions/actionTypes';

export function serviceReducer(
    state = {
        isLoading: false,
        loadingDoors: false,
        orders: [],
        error: false,
        // newDoor: {},
        doors: [],
        currentOrder: null,
        currentDoor: null,
        currentTraits: [],
        doorTypes: [],
        traitTypes: [],
        docTypes: [],
        sites: [],
        orderPurchasers: [],
        currentPerson: null,
        doorCustomer:[],
        orderDocs: [],
        searching: false,
        searchResults: [],
        doorNoteIds:'',
        selectedNote:null,
        selectedDoorId:null,
        showOrderActivity:false,
        currentOrderActivity:[],
    }
    , action = null) {
    switch (action.type) {
        case types.GET_SERVICE_ORDERS:
            return Object.assign({}, state, {isLoading: true, error: false});
        case types.SET_SERVICE_ORDERS:
            return Object.assign({}, state, {isLoading: false, orders: action.payload, error: false});
        case types.SERVICE_SET_CURRENT_ORDER:
            return Object.assign({}, state, {currentOrder: action.payload});
        case types.SERVICE_TOGGLE_ADD_ORDER:
            return Object.assign({}, state, {addOrderOpened: action.payload});
        case types.SERVICE_TOGGLE_ADD_DOOR:
            return Object.assign({}, state, {addDoorOpened: action.payload});
        case types.SERVICE_GET_DOORS:
            return Object.assign({}, state, {loadingDoors: true});
        case types.SERVICE_SET_DOORS:
            return Object.assign({}, state, {loadingDoors: false, doors: action.payload});
        case types.SERVICE_ADD_DOOR:
            state.doors.push(action.payload)
            return Object.assign({}, state, {doors: state.doors});
        case types.SERVICE_UPDATE_DOOR:
            let doorIndex = state.doors.findIndex((d)=>{
                return d.id == action.payload.id
            })
            state.doors[doorIndex] = action.payload
            return Object.assign({}, state, {doors: state.doors});
        case types.SERVICE_SET_DOOR_TYPES:
            return Object.assign({}, state, {doorTypes: action.payload});
        case types.SERVICE_SET_TRAIT_TYPES:
            return Object.assign({}, state, {traitTypes: action.payload});
        case types.SERVICE_SET_CURRENT_TRAITS:
            return Object.assign({}, state, {currentTraits: action.payload})
        case types.SERVICE_SET_CURRENT_DOOR:
            return Object.assign({}, state, {currentDoor: action.payload});
        case types.SERVICE_UPDATE_CURRENT_DOOR_DATA:
            return Object.assign({}, state, {currentDoor: Object.assign({}, state.currentDoor, action.payload)});
        case types.SERVICE_SET_SITES:
            return Object.assign({}, state, {sites: action.payload});
        case types.SERVICE_ADD_ORDER:
            state.orders.push(action.payload)
            return Object.assign({}, state, {orders: state.orders});
        case types.SERVICE_UPDATE_ORDER:
            let orderIndex = state.orders.findIndex((d)=>{
                return d.id == action.payload.id
            })
            Object.assign(state.orders[orderIndex],action.payload)
            return Object.assign({}, state, {orders: state.orders});
        case types.SERVICE_TOGGLE_PERSON_FORM_DIALOG:
            return Object.assign({}, state, {personFormOpened: action.payload, personFormId: action.id});
        case types.SERVICE_SET_PURCHASERS:
            return Object.assign({}, state, {orderPurchasers: action.payload})
        case types.SERVICE_ADD_PURCHASER:
            state.orderPurchasers.push(action.payload)
            return Object.assign({}, state, {orderPurchasers: state.orderPurchasers})
        case types.SERVICE_SET_CURRENT_PERSON:
            return Object.assign({}, state, {currentPerson: action.payload})
        case types.SERVICE_TOGGLE_ADD_DOC:
            return Object.assign({}, state, {addDocOpened: action.payload});
        case types.SERVICE_SET_DOC_TYPES:
            return Object.assign({}, state, {docTypes: action.payload});
        case types.SERVICE_SET_ORDER_DOCS:
            return Object.assign({}, state, {orderDocs: action.payload});
        case types.SERVICE_ADD_DOC:
            state.orderDocs.push(action.payload)
            return Object.assign({}, state, {orderDocs: state.orderDocs});
        case types.SERVICE_REMOVE_DOC:
            state.orderDocs.splice(getPurchaserIndex(action.payload), 1)
            return Object.assign({}, state, {orderDocs: state.orderDocs});
        case types.SERVICE_ADD_DOC_TYPE:
            state.docTypes.push(action.payload)
            return Object.assign({}, state, {docTypes: state.docTypes});
        case types.SERVICE_ADD_DOOR_TYPE:
            state.doorTypes.push(action.payload)
            return Object.assign({}, state, {doorTypes: state.doorTypes});
        case types.SERVICE_TOGGLE_ADD_DOOR_NOTE:
            return Object.assign({}, state, {addDoorNoteOpened: action.payload});
        case types.SERVICE_ADD_DOOR_NOTE:
            state.doors[getDoorIndex(action.doorId)].notes.push(action.payload)
            return Object.assign({}, state, {doors: state.doors});
        case types.SERVICE_DELETE_DOOR_NOTE:
            let doorNoteIdx = state.doors[getDoorIndex(action.payload.doorId)].notes.findIndex((n)=>{return n.id == action.payload.noteId})
            state.doors[getDoorIndex(action.payload.doorId)].notes.splice(doorNoteIdx, 1)
            return Object.assign({}, state, {doors: state.doors})
        case types.SERVICE_SET_DOOR_CUSTOMER:
            return Object.assign({}, state, {doorCustomer: action.payload})
        case types.SERVICE_SET_SELECTED_DOOR_ID:
            return Object.assign({},state, {selectedDoorId:action.payload})
        case types.SERVICE_ADD_DOOR_CUSTOMER:
            state.doors[getDoorIndex(action.doorId)].customers.push(action.payload)
            return Object.assign({}, state, {doors: state.doors})
        case types.SERVICE_DELETE_DOOR_CUSTOMER:
            let doorCustomerIdx = state.doors[getDoorIndex(action.payload.doorId)].customers.findIndex((c)=>{return c.id == action.payload.cId})
            state.doors[getDoorIndex(action.payload.doorId)].customers.splice(doorCustomerIdx, 1)
            return Object.assign({}, state, {doors: state.doors})
        case types.SERVICE_SET_SEARCHING:
            return Object.assign({}, state, {searching: action.payload})
        case types.SERVICE_SET_SEARCH_RESULTS:
            return Object.assign({}, state, {searchResults: action.payload})
        case types.SERVICE_ADD_ORDER_NOTE:
            state.currentOrder.notes.push({id:0, note: action.payload})
            return Object.assign({}, state, {currentOrder: state.currentOrder})
        case types.SERVICE_UPDATE_ORDER_NOTE:
            state.currentOrder.notes[action.payload.index].note = action.payload.note
            return Object.assign({}, state, {currentOrder: state.currentOrder})
        case types.SERVICE_SELECTED_NOTE:
            return Object.assign({}, state, {selectedNote: action.payload})
        case types.SERVICE_UPDATE_DOOR_NOTE:
            let noteIndx = state.doors[getDoorIndex(action.payload.doorId)].notes.findIndex((n)=>{return n.id == action.payload.note.id})
            state.doors[getDoorIndex(action.payload.doorId)].notes[noteIndx] = action.payload.note
            return Object.assign({}, state, {doors: state.doors})
        case types.SERVICE_UPDATE_PURCHASER:
            Object.assign(state.orderPurchasers[getPurchaserIndex(action.payload)],action.payload)
            return Object.assign({}, state, {orderPurchasers: state.orderPurchasers})
        case types.SERVICE_DELETE_PURCHASER:
            state.orderPurchasers.splice(getPurchaserIndex(action.payload), 1)
            return Object.assign({}, state, {orderPurchasers: state.orderPurchasers})
        case types.GOTO_ORDER_ACTIVITY:
            return Object.assign({},state,{showOrderActivity: action.payload});
        case types.SET_ORDER_ACTIVITY:
			return Object.assign({},state,{currentOrderActivity: action.payload});
        default:
            return state;
    }
    function getDoorIndex(doorId) {
        return state.doors.findIndex((d)=>{return d.id == doorId})
    }

    function getPurchaserIndex(purchaserId){
        return state.orderPurchasers.findIndex((purchaser)=>{return purchaser.id == purchaserId})
    }
    function getDocIndex(docId){
        return state.orderDocs.findIndex((doc)=>{return doc.id == docId})
    }
};