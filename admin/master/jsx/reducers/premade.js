import * as types from '../actions/actionTypes';

export function premadeReducer(state = {
    isLoading: false,
    data: [],
    component: [],
    isSaving:false,
    isSaved:false,
    error: false}
    , action = null) {
    switch(action.type) {
        case types.GET_PREMADE_DATA:
            return Object.assign({}, state, {isLoading: true, error: false, data:[], component: [], isSaving:false,isSaved:false });
        case types.GET_PREMADE_DATA_SUCCESS:
            return Object.assign({}, state, {isLoading: false, data: action.data, error: false, isSaving:false,isSaved:false });
        case types.GET_COMPONENT_PREMADE_DATA_SUCCESS:
            return Object.assign({}, state, {isLoading: false, component: action.data, error: false, isSaving:false,isSaved:false });
        case types.SAVING_PREMADE_DATA:
            return Object.assign({},state, {isLoading:false,error:false,isSaving:true,isSaved:false});
        case types.SAVED_PREMADE_DATA:
            return Object.assign({},state, {isLoading:false, error:false, data: action.data, isSaving:false, isSaved:true});
        case types.PREMADE_DATA_ERROR:
            return Object.assign({}, state, {isLoading: false, data: action.data, error: true});
        default:
            return state;
    }
};