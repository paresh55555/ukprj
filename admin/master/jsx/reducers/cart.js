import * as types from '../actions/actionTypes';

export default function cartReducer(state = {
    isEdit: false,
    isSingleEdit: false,
    isLoading: false,
    isSaving: false,
    showModal: false,
    currentCartId: null,
    currentCart: null,
    currentColumn: {
        layoutId: null,
        id: null,
    },
    currentColumnItemsCopy: [],
    carts: [],
    cartsLayoutCopy: [],
    cartsLayout: [],
}, action = null) {
    switch (action.type) {
        case types.CHANGE_EDIT_MODE:
            return Object.assign({}, state, { isEdit: !state.isEdit });
        case types.CLOSE_EDIT_MODES:
            return Object.assign({}, state, { isEdit: false, isSingleEdit: false, cartsLayout: [] });
        case types.CLEAR_LAYOUT:
            return Object.assign({}, state, { cartsLayout: [] });
        case types.START_NEW_LAYOUT_SAVING:
            return Object.assign({}, state, { isSaving: true });
        case types.FINISH_NEW_LAYOUT_SAVING:
            return Object.assign({}, state, { isSaving: false });
        case types.CHANGE_MODAL_STATE:
            return Object.assign({}, state, { showModal: !state.showModal });
        case types.CHANGE_SINGLE_EDIT_MODE:
            return Object.assign({}, state, { isSingleEdit: !state.isSingleEdit });
        case types.BACK_TO_PREVIOUS_LAYOUT:
            return Object.assign({}, state, { isEdit: false, cartsLayout: state.cartsLayoutCopy });
        case types.REQUEST_ELEMENTS:
            return Object.assign({}, state, { isLoading: true });
        case types.SET_CURRENT_COLUMN:
            const layoutItemCartCurrent = state.cartsLayout.find(item => item.id === action.data.layoutId);
            const currentColumnCurrent = layoutItemCartCurrent.columns.find(item => item.id === action.data.id);
            const currentColumnItemsCopySet = currentColumnCurrent.items ? [...currentColumnCurrent.items] : [];
            return Object.assign({}, state, { currentColumn: action.data, currentColumnItemsCopy: currentColumnItemsCopySet });
        case types.CLEAN_CURRENT_COLUMN:
            return Object.assign({}, state, { currentColumn: { layoutId: null, id: null }, currentColumnItemsCopy: [] });
        case types.GET_ELEMENTS:
            return Object.assign({}, state, { isLoading: false, carts: action.data });
        case types.SAVE_ELEMENTS:
            return Object.assign({}, state, { carts: [...state.carts, action.data] });
        case types.DELETE_ELEMENT:
            const deletedCarts = [...state.carts];
            const deletedCartIndex = state.carts.findIndex(item => item.id === action.id);
            deletedCarts.splice(deletedCartIndex, 1);
            return Object.assign({}, state, { isLoading: false, carts: deletedCarts });
        case types.UPDATE_ELEMENT:
            const updatedCarts = [...state.carts];
            const updatedCartIndex = state.carts.findIndex(item => item.id === action.data.id);
            const updatedCart = Object.assign({}, state.carts[updatedCartIndex]);
            if (updatedCart.id) {
                updatedCart.name = action.data.name;
                updatedCarts[updatedCartIndex] = updatedCart;
            } else { updatedCarts[0] = action.data; }
            return Object.assign({}, state, { isLoading: false, carts: updatedCarts });
        case types.CHANGE_ELEMENT_POSITION:
            return Object.assign({}, state, { cartsLayout: action.data });
        case types.SET_CURRENT_ELEMENT:
            return Object.assign({}, state, { currentCart: action.data });
        case types.CHANGE_CURRENT_ELEMENT_NAME:
            return Object.assign({}, state, { currentCart: Object.assign({}, state.currentCart, { name: action.data }) });
        case types.GET_ELEMENT_LAYOUTS:
            return Object.assign({}, state, { isLoading: false, currentCartId: action.id, cartsLayout: action.data, cartsLayoutCopy: [...action.data] });
        case types.SAVE_ELEMENT_LAYOUT:
            return Object.assign({}, state, { isLoading: false, cartsLayout: [...state.cartsLayout, action.data] });
        case types.REPLACE_ELEMENT_LAYOUT:
            const replacedCart = [...state.cartsLayout];
            const replacedCartIndex = replacedCart.findIndex(item => item.id === action.id);
            replacedCart[replacedCartIndex] = action.data;
            return Object.assign({}, state, { isLoading: false, cartsLayout: replacedCart });
        case types.DELETE_ELEMENT_LAYOUT:
            const deletedLayout = [...state.cartsLayout];
            const deletedLayoutIndex = state.cartsLayout.findIndex(item => item.id === action.id);
            deletedLayout.splice(deletedLayoutIndex, 1);
            return Object.assign({}, state, { cartsLayout: deletedLayout });
        case types.DELETE_COLUMN_ELEMENT_LAYOUT:
            const deletedLayoutForColumns = [...state.cartsLayout];
            const deletedRow = Object.assign({}, state.cartsLayout.find(item => item.id === action.layoutId));
            const deletedColumn = [...deletedRow.columns];
            const deletedColumnIndex = deletedColumn.findIndex(item => item.id === action.id);
            deletedColumn.splice(deletedColumnIndex, 1);
            return Object.assign({}, state, { cartsLayout: deletedLayout });
        case types.SAVE_COLUMNS_LAYOUT:
            const cartsLayoutCopy = [...state.cartsLayout];
            const cartIndex = state.cartsLayout.findIndex(item => item.id === action.id);
            const currentCart = Object.assign({}, state.cartsLayout[cartIndex]);
            currentCart.columns = action.data;
            cartsLayoutCopy[cartIndex] = currentCart;
            return Object.assign({}, state, { cartsLayout: cartsLayoutCopy, cartsLayoutCopy: [...cartsLayoutCopy] });
        case types.SET_LAYOUT_COPY:
            return state;
        case types.ADD_ITEM:
            const layoutItem = [...state.cartsLayout];
            const layoutItemIndex = layoutItem.findIndex(item => item.id === action.layoutId);
            const layoutItemCart = Object.assign({}, layoutItem[layoutItemIndex]);
            const layoutItemCartColumns = [...layoutItemCart.columns];
            const layoutItemColumnsIndex = layoutItemCartColumns.findIndex(item => item.id === action.columnId);
            const currentColumn = Object.assign({}, layoutItemCartColumns[layoutItemColumnsIndex]);
            const currentColumnItems = currentColumn.items ? [...currentColumn.items] : [];
            currentColumn.items = [...currentColumnItems, action.data];
            layoutItemCartColumns[layoutItemColumnsIndex] = currentColumn;
            layoutItemCart.columns = layoutItemCartColumns;
            layoutItem[layoutItemIndex] = layoutItemCart;
            return Object.assign({}, state, { cartsLayout: layoutItem });
        case types.BACK_TO_PREVIOUS_ITEMS:
            const layoutItemBack = [...state.cartsLayout];
            const layoutItemIndexBack = layoutItemBack.findIndex(item => item.id === action.layoutId);
            const layoutItemCartBack = Object.assign({}, layoutItemBack[layoutItemIndexBack]);
            const layoutItemCartColumnsBack = [...layoutItemCartBack.columns];
            const layoutItemColumnsIndexBack = layoutItemCartColumnsBack.findIndex(item => item.id === action.columnId);
            const currentColumnBack = Object.assign({}, layoutItemCartColumnsBack[layoutItemColumnsIndexBack]);
            const currentColumnItemsCopyBack = [];
            currentColumnBack.items = [...state.currentColumnItemsCopy];
            layoutItemCartColumnsBack[layoutItemColumnsIndexBack] = currentColumnBack;
            layoutItemCartBack.columns = layoutItemCartColumnsBack;
            layoutItemBack[layoutItemIndexBack] = layoutItemCartBack;
            return Object.assign({}, state, { cartsLayout: layoutItemBack, currentColumnItemsCopyBack });
        case types.ADD_SAVED_ITEMS:
            const layoutItemSaved = [...state.cartsLayout];
            const layoutItemIndexSaved = layoutItemSaved.findIndex(item => item.id === action.layoutId);
            const layoutItemCartSaved = Object.assign({}, layoutItemSaved[layoutItemIndexSaved]);
            const layoutItemCartColumnsSaved = [...layoutItemCartSaved.columns];
            const layoutItemColumnsIndexSaved = layoutItemCartColumnsSaved.findIndex(item => item.id === action.columnId);
            const currentColumnSaved = Object.assign({}, layoutItemCartColumnsSaved[layoutItemColumnsIndexSaved]);
            currentColumnSaved.items = [...state.currentColumnItemsCopy, ...action.data];
            layoutItemCartColumnsSaved[layoutItemColumnsIndexSaved] = currentColumnSaved;
            layoutItemCartSaved.columns = layoutItemCartColumnsSaved;
            layoutItemSaved[layoutItemIndexSaved] = layoutItemCartSaved;
            return Object.assign({}, state, { cartsLayout: layoutItemSaved });
        default:
            return state;
        }
}

