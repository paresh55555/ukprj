import * as types from '../actions/actionTypes';

export function serviceJobReducer(
    state = {
        isLoading: false,
        jobs: [],
        currentJob: null,
        techList: [],
        showAddJobs:false,
        job_customers:[],
        job_address:[],
        addJobDoorOpened: false,
        attachedDoors: [],
        openModal:false,
        jobStatus:[],
        currentJobFiles:[],
        activeTab:'detail',
        currentJobActivity:[],
        sendedEmail:[],
    }
    , action = null) {
    switch (action.type) {
        case types.GET_SERVICE_JOBS:
            return Object.assign({}, state, {error: false});
        case types.SET_SERVICE_JOBS:
            return Object.assign({}, state, {jobs: action.payload, error: false});
        case types.SET_SERVICE_CURRENT_JOB:
            return Object.assign({}, state, {currentJob: action.payload});
        case types.SET_SERVICE_TECH_LIST:
            return Object.assign({}, state, {techList: action.payload});
        case types.SET_TOGGLE_ADD_JOBS:
            return Object.assign({}, state, {showAddJobs: action.payload});
        case types.GET_SERVICE_JOB_CUSTOMERS:
            return Object.assign({}, state, {job_customers: action.payload})
        case types.DELETE_SERVICE_JOB_CUSTOMERS:
            state.job_customers.splice(getCustomerIndex(action.payload),1)
            return Object.assign({}, state, {job_customers: state.job_customers})
        case types.GET_SERVICE_JOB_ADDRESS:
            return Object.assign({}, state, {job_address: action.payload})
        case types.DELETE_SERVICE_JOB_ADDRESS:
            const newJobAdresses = state.job_address.filter(
                address => address.id !== action.payload
            )
            return Object.assign({}, state, {job_address: newJobAdresses})
        case types.TOGGLE_ADD_JOB_DOOR:
            return Object.assign({}, state, {addJobDoorOpened: action.payload});
        case types.SET_ATTACHED_DOORS:
            return Object.assign({}, state, {attachedDoors: action.payload});
        case types.SET_JOBS_LOADING:
            return Object.assign({}, state, {isLoading: action.payload});
        case types.TOGGLE_UPLOADER_MODAL:
            return Object.assign({}, state, {openModal: action.payload});
        case types.SET_JOBS_STATUS:
            return Object.assign({}, state, {jobStatus: action.payload});
        case types.SET_CURRENT_JOB_FILES:
            return Object.assign({}, state, {currentJobFiles: action.payload});
        case types.SET_JOB_TAB:
            return Object.assign({}, state, {activeTab: action.payload});
        case types.SET_CURRENT_JOB_ACTIVITY:
            return Object.assign({}, state, {currentJobActivity: action.payload});
        case types.SET_SEND_EMAIL_DETAILS:
            return Object.assign({}, state, {sendedEmail: action.payload});
        default:
            return state;
    }

    function getCustomerIndex(customerId){
        return state.job_customers.findIndex(customer=>customer.id ==customerId)
    }

};