
import {API_URL, SQ_URL} from './api/ApiServerConfigs';
import {VERSION} from './api/ApiServerConfigs';

// const API_URL = 'https://dev-demo1.rioft.com/api/V4'
const USER_LIST_URL = API_URL + '/users';
const COMPANY_LIST_URL = API_URL + '/companies';
const COMPANY_LIST_BYORDER_URL = COMPANY_LIST_URL + '/order';
const PERMISSION_LIST_URL = API_URL + '/permissions';
const MODULE_LIST_URL = API_URL + '/modules';
const COMPONENTS_LIST_URL = API_URL + '/components';
const SYSTEM_LIST_URL = API_URL + '/systems';
const SYSTEM_GROUPS_LIST_URL = API_URL + '/systemGroups'
const AUTH_URL = API_URL + '/token'
const SQ_PREFIX = '';

const COMPONENT_TYPES_URL = API_URL + '/componentTypes'
const BRANDING_URL = API_URL + '/siteBranding'
const REPORTS_URL = API_URL + '/reports'
const DEBUG_MODE = true;

export {
    API_URL,
    VERSION,
    USER_LIST_URL,
    SQ_PREFIX,
    SQ_URL,
    MODULE_LIST_URL,
    COMPANY_LIST_URL,
    COMPANY_LIST_BYORDER_URL,
    PERMISSION_LIST_URL,
    COMPONENTS_LIST_URL,
    AUTH_URL,
    SYSTEM_LIST_URL,
    SYSTEM_GROUPS_LIST_URL,
    COMPONENT_TYPES_URL,
    BRANDING_URL,
    REPORTS_URL,
    DEBUG_MODE,
}