import * as types from './actionTypes';

export function requestData() {
    return {
        type:types.GET_COSTS,
    }
}
export function receiveData(json) {
    return {
        type:types.GET_COSTS_SUCCESS,
        data:json
    }
}
export function receiveComponetCost(json,id) {
    return {
        type:types.GET_COMPONENT_COSTS_SUCCESS,
        data:json,
        componentId:id
    }
}
 
export function savingCostsData (optionId) {
    if(optionId) {
        return {
            type: types.SAVING_OPTION_COSTS_DATA,
        }
    } else {
        return {
            type: types.SAVING_COMPONENT_COSTS_DATA,
        };
    }
}
export function saveCostsData(payload){
    if(payload.optionId) {
        return Object.assign({}, {
            type: types.SAVED_OPTION_COSTS_DATA,
        }, payload);
    } else {
        return Object.assign({}, {
            type: types.SAVED_COMPONENT_COSTS_DATA,
        }, payload);
    }
}

export function costDataSaved(json){
    return{
        type:types.SAVED_COSTS_DATA,
        data:json
    }
}

export function costDataUpdate(payload){
    return Object.assign({}, {
        type: types.UPDATE_OPTION_COSTS_DATA,
    }, payload);
}

export function deletedCost(id, optionId){
    return{
        type:types.DELETED_COST,
        data:id,
        optionId
    }
};
