import * as types from './actionTypes';


export function changePlaceholder(id, data) {
    return {
        type: types.CHANGE_CUSTOMER_PLACEHOLDER,
        id,
        data,
    };
}

export function changeTitle(id, data) {
    return {
        type: types.CHANGE_CUSTOMER_TITLE,
        id,
        data,
    };
}

export function changeDisplay(id, data) {
    return {
        type: types.CHANGE_CUSTOMER_DISPLAY,
        id,
        data,
    };
}

export function changeRequired(id, data) {
    return {
        type: types.CHANGE_CUSTOMER_REQUIRED,
        id,
        data,
    };
}

export function changeErrorMessage(id, data) {
    return {
        type: types.CHANGE_CUSTOMER_ERROR,
        id,
        data,
    };
}

export function requestDefaults() {
    return {
        type: types.REQUEST_DEFAULTS,
    };
}

export function saveSiteDefaults(data) {
    return {
        type: types.RECEIVE_SITE_DEFAULTS,
        data,
    };
}

export function backToOriginalDefaults() {
    return {
        type: types.BACK_TO_ORIGINAL_DEFAULTS,
    };
}

export function changeSiteDefaultsAction(id, data) {
    return {
        type: types.CHANGE_SITE_DEFAULTS,
        id,
        data,
    };
}

export function saveCustomerDefaults(data) {
    return {
        type: types.RECEIVE_CUSTOMER_DEFAULTS,
        data,
    };
}

export function backToOriginalCustomer() {
    return {
        type: types.BACK_TO_ORIGINAL_CUSTOMER,
    };
}

export function changeCustomerDefaultsAction(id, data) {
    return {
        type: types.CHANGE_CUSTOMER_DEFAULTS,
        id,
        data,
    };
}

export function changeDateFormat(id, data) {
    return {
        type: types.CHANGE_DATE_FORMAT,
        id,
        data,
    };
}

export function changeTimeFormat(id, data) {
    return {
        type: types.CHANGE_TIME_FORMAT,
        id,
        data,
    };
}

export function changeCurrency(id, data) {
    return {
        type: types.CHANGE_CURRENCY,
        id,
        data,
    };
}

