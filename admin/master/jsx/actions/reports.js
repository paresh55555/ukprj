import * as types from './actionTypes';

export function requestSalesReports() {
    return {
        type: types.GET_SALES_REPORTS_REQUEST,
    };
}

export function receiveSalesReports(data) {
    return {
        type: types.GET_SALES_REPORTS_SUCCESS,
        data,
    };
}

export function requestDeliveredOrdersReports() {
    return {
        type: types.GET_DELIVERED_ORDERS_REPORTS_REQUEST,
    };
}

export function receiveDeliveredOrdersReports(data) {
    return {
        type: types.GET_DELIVERED_ORDERS_REPORTS_SUCCESS,
        data,
    };
}
