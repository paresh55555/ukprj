import * as types from './actionTypes';

export function savingData(json){
    return {
        type: types.SAVING_OPTION_TRAITS_DATA,
        data: json
    }
};
export function dataSaved(json, optionId){
    return{
        type:types.SAVED_OPTION_TRAITS_DATA,
        data:json,
        optionId: optionId
    }
};