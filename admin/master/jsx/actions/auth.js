import * as types from './actionTypes'
import axios from 'axios'
import * as constant from '../constant.js'
import {replace, push} from 'react-router-redux'
import {VERSION} from '../api/ApiServerConfigs'

function requestData() {
    return {type: types.LOGIN}
}

export function loginSuccess(json) {
    return {
        type: types.LOGIN_SUCCESS,
        data: json
    }
}

export function receiveError(json) {
    return {
        type: types.LOGIN_ERROR,
        data: json
    }
}

/*export function getSideBarLinks(json){
 return {
 types.SIDEBAR_LINKS,
 data:json
 }
 }*/

export function filterSidebarLinks(permission) {

    let version1 = [
        {
            type: 'heading',
            title: 'Version 1',
            permissions: []
        },
        {
            route: 'users',
            title: 'Users',
            icon: 'icon-grid',
            permissions: []
        }, {
            route: 'companies',
            title: 'Companies',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'modules',
            title: 'Modules',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            type: 'heading',
            title: 'Reports',
            permissions: [
                'admin', 'reports'
            ]
        },
        {
            route: 'reports/sales',
            title: 'Sales',
            icon: 'icon-grid',
            permissions: [
                'admin', 'reports'
            ]
        },
        {
            route: 'reports/deliveredOrders',
            title: 'Has Balance',
            icon: 'icon-grid',
            permissions: [
                'admin', 'reports'
            ]
        }
    ]

    let version2 = [
        {
            type: 'heading',
            title: 'Systems',
            permissions: []
        },
        {
            route: 'systems',
            title: 'Active Systems',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ],
            child: true
        },
        {
            route: 'systems/inactive',
            title: 'In-Active Systems',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/systems/doors',
            title: 'Door System Templates',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/systems/windows',
            title: 'Window System Templates',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            type: 'heading',
            title: 'Builders',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/accounts',
            title: 'Account Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/cart',
            title: 'Cart Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/customer',
            title: 'Customer Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/cutsheets',
            title: 'Cut Sheet Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/doors',
            title: 'Door Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/guests',
            title: 'Guest Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/list',
            title: 'List Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/quote',
            title: 'Quote Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/order',
            title: 'Order Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'builder/windows',
            title: 'Window Builder',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            type: 'heading',
            title: 'Site Settings',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'branding',
            title: 'Branding',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'defaults',
            title: 'Defaults',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
        {
            type: 'heading',
            title: 'Accounts',
            permissions: [
                'admin'
            ]
        },
        {
            route: 'accounts/users',
            title: 'Users',
            icon: 'icon-grid',
            permissions: [
                'admin'
            ]
        },
    ]

    let links = version2

    if (VERSION == 'V1') {
        links = version1
    }

    let permissionType = permission && Object.keys(permission);
    if(permissionType && permissionType.length && permissionType[0] == 'service'){
        //Service links
        links = [
            {
                route: 'service/jobs',
                title: 'Jobs',
                icon: 'icon-briefcase',
                permissions: [
                    'service'
                ]
            },
            {
                route: 'service/orders',
                title: 'Orders',
                icon: 'fa-file-text-o',
                permissions: [
                    'service'
                ]
            },
            {
                route: 'service/customers',
                title: 'Customers',
                icon: 'icon-people',
                permissions: [
                    'service'
                ]
            }]
    }

    let filteredLinks = links.filter((link) => {
        if (!!permission) {
            if (!!link.permissions.length) {
                let permissiontype = Object.keys(permission)
                if (permissiontype && permissiontype.length) {
                    return link.permissions.indexOf(permissiontype[0]) > -1
                }

            }
            return true
        }
        return false
    })
    return filteredLinks
}