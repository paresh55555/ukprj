import * as types from './actionTypes';

export function getServiceJobs() {
    return {type: types.GET_SERVICE_JOBS}
}

export function setServiceJobs(payload) {
    return {type: types.SET_SERVICE_JOBS, payload};
}

export function setCurrentJob(payload) {
    return {type: types.SET_SERVICE_CURRENT_JOB, payload};
}

export function setServiceTechList(payload) {
    return {type: types.SET_SERVICE_TECH_LIST, payload};
}

export function getJobCustomer(payload) {
    return {type: types.GET_SERVICE_JOB_CUSTOMERS, payload};
}
export function deleteJobCustomer(payload) {
    return {type: types.DELETE_SERVICE_JOB_CUSTOMERS, payload};
}
export function toggleAddJobs(payload) {
    return {type: types.SET_TOGGLE_ADD_JOBS, payload};
}

export function getJobAddress(payload) {
    return {type: types.GET_SERVICE_JOB_ADDRESS, payload};
}

export function deleteJobAddress(payload) {
    return {type: types.DELETE_SERVICE_JOB_ADDRESS, payload};
}

export function toggleAddJobDoor(payload) {
    return {type: types.TOGGLE_ADD_JOB_DOOR, payload};
}

export function setAttachedDoors(payload) {
    return {type: types.SET_ATTACHED_DOORS, payload};
}

export function setJobsLoading(payload) {
    return {type: types.SET_JOBS_LOADING, payload};
}

export function toggleUploaderModal(payload) {
    return {type: types.TOGGLE_UPLOADER_MODAL, payload};
}

export function setJobsStatus(payload) {
    return {type: types.SET_JOBS_STATUS, payload};
}

export function setCurrentJobFiles(payload) {
    return {type: types.SET_CURRENT_JOB_FILES, payload};
}

export function setJobTab(payload) {
    return {type: types.SET_JOB_TAB, payload};
}

export function setCurrentJobActivity(payload) {
    return {type: types.SET_CURRENT_JOB_ACTIVITY, payload};
}

export function setSendEmailDetails(payload) {
    return {type: types.SET_SEND_EMAIL_DETAILS, payload};
}


