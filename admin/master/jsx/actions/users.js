import * as types from './actionTypes';

export function requestData() {
    return { type: types.GET_USER };
}

export function prepUserTableForData() {
    return { type: types.PREP_USER_TABLE_FOR_DATA };
}

export function receiveListOfUsers(json) {
    return {
        type: types.RECEIVE_USERS,
        data: json,
    };
}

export function receiveUserData(json) {
    return {
        type: types.GET_USER_DETAIL_SUCCESS,
        data: json,
    };
}

export function dataSaved(json) {
    return {
        type: types.SAVED_USER_DATA,
        data: json,
    };
}
export function deleteData(json) {
    return {
        type: types.DELETED_USER_DATA,
        data: json,
    };
}
