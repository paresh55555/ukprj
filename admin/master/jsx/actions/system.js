import * as types from './actionTypes';

export function requestData() {
	return {type: types.GET_SYSTEM_GROUPS}
};

export function receiveData(json) {
	return{
		type: types.GET_SYSTEM_GROUPS_SUCCESS,
		data: json
	}
};

function requestSampleDoor() {
    return {type: types.GET_SAMPLE_DOOR}
};

function receiveSampleDoor(json) {
    return{
        type: types.GET_SAMPLE_DOOR_SUCCESS,
        data: json
    }
};

export function savingData(json){
	return {
		type: types.SAVING_SYSTEM_DATA,
		data: json
	}
};
export function dataSaved(json){
	return{
		type:types.SAVED_SYSTEM_DATA,
		data:json
	}
};
export function dataPublished(json){
	return{
		type:types.PUBLISHED_SYSTEM_GROUP,
		data:json
	}
};
export function deleteData(){
    return{
        type:types.DELETED_SYSTEM_GROUP
    }
}

export function recievedPanelRanges(json) {
    return {
        type: types.GET_PANEL_RANGES_SUCCESS,
        data: json
    }
};
export function recievedSampleDoors(json) {
    return {
        type: types.GET_SAMPLE_DOORS_SUCCESS,
        data: json
    }
};

export function recievedPremadeSystems(json){
    return {
        type: types.GET_PREMADE_SYSTEMS_SUCCESS,
        data: json
    }
}


// export function getSystemGroupsList() {
// 	return function(dispatch) {
// 		dispatch(requestData());
// 			return axios.get(constant.SYSTEM_GROUPS_LIST_URL)
// 			.then(function(res){
// 				dispatch(receiveData(res.data.response_data));
// 			})
// 	}
// };

// export function getSampleDoorPrice() {
//     return function(dispatch) {
//         dispatch(requestSampleDoor());
//         return axios.get(constant.SYSTEM_GROUPS_LIST_URL + '/sampleDoors')
//             .then(function(res){
//                 dispatch(receiveSampleDoor(res.data.response_data));
//             })
//     }
// };

// export function saveSystemGroup(){
//     return function(dispatch) {
//         dispatch(savingData());
//         var data = JSON.stringify({
//             name: 'SystemGroup'
//         });
//         return axios.post(constant.SYSTEM_GROUPS_LIST_URL, data, instance)
//             .then(function(res){
//                 dispatch(dataSaved(res.data));
//             })
//     }
// }

// export function publishSystemGroup(){
//     return function(dispatch) {
//         var data = JSON.stringify({
//             name: 'System Group'
//         });
//         return axios.post(constant.SYSTEM_GROUPS_LIST_URL + '/publish', data, instance)
//             .then(function(res){
//                 dispatch(dataPublished(res.data));
//             })
//     }
// }

// export function deleteSystemGroup(id) {
//     return function(dispatch) {
//         dispatch(requestData());
//         return axios.delete(constant.SYSTEM_GROUPS_LIST_URL+'/' +id)
//             .then(function(res){
//                 dispatch(deleteData());
//                 return res;
//             })
//     }
// }

// export function saveTitleTraits(componentId, deletedTraits, traits) {
//     return function(dispatch) {
//         dispatch(savingData());
//         let deleteRequests = [];
//         let _saveTraits = function(traits){
//             let data = JSON.stringify(traits)
//             axios.post(constant.SYSTEM_LIST_URL + '/' + componentId + '/traits', data, instance).then(function(res){
//                 dispatch(dataSaved(res.data));
//             })
//         };
//         if (deletedTraits && deletedTraits.length) {
//             deletedTraits.map((trait) => {
//                 if (trait.id) {
//                     deleteRequests.push(
//                         axios.delete(constant.SYSTEM_GROUPS_LIST_URL + '/' + componentId + '/traits/' + trait.id)
//                     )
//                 }
//             });
//         }
//         if (deleteRequests.length) {
//             axios.all(deleteRequests).then(function(res){
//                 _saveTraits(traits);
//             })
//         } else {
//             _saveTraits(traits);
//         }
//     }
// };
// export function saveSystemTrait(systemId, data){
//     return function(dispatch) {
//         dispatch(savingData());
//         data = JSON.stringify(data)
//         return axios.post(constant.SYSTEM_LIST_URL+'/'+systemId+'/traits', data, instance)
//             .then(function(res){
//                 dispatch(dataSaved(res.data));
//                 return res.data;
//             })
//     }
// }

// export function saveSystem(data) {
// 	return function(dispatch) {
// 		dispatch(savingData());
// 		data = JSON.stringify(data)
// 		return axios.post(constant.SYSTEM_LIST_URL, data, instance)
// 			.then(function(res){
// 				dispatch(dataSaved(res.data));
// 			})
// 		}
// };

// export function updateSystem(data) {
// 	return function(dispatch) {
// 		dispatch(savingData());
// 		data = JSON.stringify(data)
// 		return axios.put(constant.SYSTEM_LIST_URL, data, instance)
// 			.then(function(res){
// 				dispatch(dataSaved(res.data));
// 			})
// 		}
// };
// export function deleteSystem(id) {
// 	return function(dispatch) {
// 		dispatch(requestData());
// 			return axios.delete(constant.SYSTEM_LIST_URL + '/' +id)
// 			.then(function(res){
// 				dispatch(deleteData(res.data));
// 			})
// 	}
// };
// export function duplicateSystem(id, data) {
//     return function(dispatch) {
//         dispatch(savingData());
//         data = JSON.stringify(data);
//         return axios.post(constant.SYSTEM_LIST_URL + '/' + id + '/duplicate', data, instance)
//             .then(function(res){
//                 dispatch(dataSaved(res.data));
//             })
//     }
// };


//system panel ranges
// export function getPanelRanges(id) {
//     return function(dispatch) {
//         dispatch(requestData());
//             return axios.get(constant.SYSTEM_LIST_URL+'/' +id+'/panelRanges')
//             .then(function(res){
//                 dispatch(recievedPanelRanges(res.data.data));
//                 return res
//             })
//     }
// };
// export function savePanelRange(id, data) {
//     data = JSON.stringify(data);
//     return function(dispatch) {
//             return axios.post(constant.SYSTEM_LIST_URL+'/' +id+'/panelRanges', data,instance)
//             .then(function(res){
//                 NotifyAlert('Panel Range added successfully');
//                 return res
//             })
//     }
// };
// export function updatePanelRange(data) {
//     let panel = JSON.stringify(data);
//     return function(dispatch) {
//             return axios.put(constant.SYSTEM_LIST_URL+'/' +data.system_id+'/panelRanges/'+data.id, panel,instance)
//             .then(function(res){
//                 NotifyAlert('Panel Range updated successfully');
//                return res
//             })
//     }
// };
// export function deletePanelRange(data) {
//     return function(dispatch) {
//             return axios.delete(constant.SYSTEM_LIST_URL+'/' +data.system_id+'/panelRanges/'+data.id)
//             .then(function(res){
//                return res
//             })
//     }
// };

// export function getPreMadeSystems() {
//     return dispatch => {
//         return axios.get(`${constant.SYSTEM_GROUPS_LIST_URL}/preMade`)
//             .then((res) => {
//                 dispatch((recievedPremadeSystems(res.data.response_data)))
//                 return res.data
//             })
//     }
// }

// export function getSampleDoorParts(id){
//     return dispatch => {
//         return axios.get(`${constant.SYSTEM_LIST_URL}/${id}/sampleDoorParts`)
//             .then((res) => {
//                 dispatch(recievedSampleDoors(res.data.response_data));
//                 return res.data;
//             })
//     }
// }

// export function getSampleComponents(id){
//     return dispatch => {
//         return axios.get(`${constant.SYSTEM_LIST_URL}/${id}/components/menu/135`)
//             .then((res) => {
//                 console.log(res)
//             })
//     }
// }

// export function getSampleDoorPrice(id){
//     return dispatch => {
//         return axios.get(`${constant.SYSTEM_LIST_URL}/${id}/sampleDoorPrice`)
//             .then((res) => {
//                 dispatch(recievedSampleDoors(res.data.response_data));
//                 return res.data;
//             })
//     }
// }