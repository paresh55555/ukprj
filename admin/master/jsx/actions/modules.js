import * as types from './actionTypes';

export function requestData() {
	return {type: types.GET_MODULE}
};

export function receiveData(json) {
	return{
		type: types.GET_MODULE_SUCCESS,
		data: json
	}
};
export function receiveListData(json) {
	return{
		type: types.GET_MODULE_LIST_SUCCESS,
		data: json
	}
};
export function receiveTableData(json) {
    return{
        type: types.GET_MODULE_TABLE_SUCCESS,
        data: json
    }
};
export function receiveMinMaxData(json) {
    return{
        type: types.GET_MODULE_MINMAX_SUCCESS,
        data: json
    }
};
export function savingData(json){
    return {
        type: types.SAVING_MODULE_DATA,
        data: json
    }
};
export function dataSaved(json){
    return{
        type:types.SAVED_MODULE_DATA,
        data:json
    }
};

export function receiveModuleData(json) {
	return{
		type: types.GET_MODULE_DETAIL_SUCCESS,
		data: json
	}
};
export function dataError(json){
    return{
        type:types.MODULE_ERROR,
        data:json
    }
}
export function deleteData(json){
    return{
        type:types.DELETED_MODULE_DATA,
        data:json
    }
}