import * as types from './actionTypes'
import { actionCreator, partial } from '../components/Common/utilFunctions';

export const setAddressList = partial(actionCreator, types.SET_ADDRESS_LIST)
export const setCurrentAddress = partial(actionCreator, types.SET_CURRENT_ADDRESS)