import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';
import { replace,push } from 'react-router-redux';

var instance = axios.create({
  header: {'content-type':'text/plain;charset=UTF-8'}
});

export function requestData() {
	return {type: types.GET_COMPANY}
};

export function receiveData(json) {
	return{
		type: types.GET_COMPANY_SUCCESS,
		data: json
	}
};
export function receiveDataByOrder(json) {
	return{
		type: types.GET_COMPANY_BYORDER_SUCCESS,
		data: json
	}
};
export function receiveCompanyData(json) {
	return{
		type: types.GET_COMPANY_DETAIL_SUCCESS,
		data: json
	}
};
export function receiveCompanyUserData(json) {
	return{
		type: types.GET_COMPANY_USER_DETAIL_SUCCESS,
		data: json
	}
};

function receiveError(json) {
	return {
		type: types.GET_COMPANY_ERROR,
		data: json
	}
};

export function savingData(json){
	return {
		type: types.SAVING_COMPANY_DATA,
		data: json
	}
};
export function dataSaved(json){
	return{
		type:types.SAVED_COMPANY_DATA,
		data:json
	}
};
export function deleteData(json){
	return{
		type:types.DELETED_COMPANY_DATA,
		data:json
	}
}