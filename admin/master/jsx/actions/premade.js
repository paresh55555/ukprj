import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';
import { replace } from 'react-router-redux';

var instance = axios.create({
    header: {'content-type':'text/plain;charset=UTF-8'}
});

export function requestData() {
    return {type: types.GET_PREMADE_DATA}
};

export function receiveData(json) {
    return{
        type: types.GET_PREMADE_DATA_SUCCESS,
        data: json
    }
};
export function receiveComponentData(json) {
    return{
        type: types.GET_COMPONENT_PREMADE_DATA_SUCCESS,
        data: json
    }
};

function receiveError(json) {
    return {
        type: types.PREMADE_DATA_ERROR,
        data: json
    }
};

export function savingData(json){
    return {
        type: types.SAVING_PREMADE_DATA,
        data: json
    }
};
export function dataSaved(json){
    return{
        type:types.SAVED_PREMADE_DATA,
        data:json
    }
};

// export function fetchData(url) {
//     return function(dispatch) {
//         dispatch(requestData());
//         return axios.get(constant.API_URL + '/premadecolors')
//             .then(function(response) {
//                 dispatch(receiveData(response.data.data));
//             })
//             .catch(function(response){
//                 dispatch(receiveError(response.data));
//                 dispatch(replace(null,'/login'));
//             })
//     }
// };

// export function getComponentDetail(systemid, id) {
//     return function(dispatch) {
//         dispatch(requestData());
//         return axios.get(constant.SYSTEM_LIST_URL+'/'+systemid+'/components/' +id)
//             .then(function(res){
//                 console.log(systemid)
//                 dispatch(receiveComponentData(res.data.response_data[0]));
//             })
//     }
// };

// export function saveGroups(systemId, componentId, groups){
//     return function (dispatch) {
//         dispatch(savingData())
//         let data = JSON.stringify(groups)
//         return axios.post(`${constant.SYSTEM_LIST_URL}/${systemId}/components/${componentId}/premadeGroups`, data, instance)
//             .then(function (res) {
//                 dispatch(dataSaved(res.data));
//             })
//     }
// }
// export function saveSubGroups(systemId, componentId, subGroups){
//     return function (dispatch) {
//         dispatch(savingData())
//         let data = JSON.stringify(subGroups)
//         return axios.post(`${constant.SYSTEM_LIST_URL}/${systemId}/components/${componentId}/premadeSubGroups`, data, instance)
//             .then(function (res) {
//                 dispatch(dataSaved(res.data));
//             })
//     }
// }
// export function saveColors(systemId, componentId, colors){
//     return function (dispatch) {
//         dispatch(savingData())
//         let data = JSON.stringify(colors)
//         return axios.post(`${constant.SYSTEM_LIST_URL}/${systemId}/components/${componentId}/premadeColors`, data, instance)
//             .then(function (res) {
//                 dispatch(dataSaved(res.data));
//             })
//     }
// }