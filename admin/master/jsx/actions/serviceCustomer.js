import * as types from './actionTypes';

export function getServiceCustomers() {
    return {type: types.GET_SERVICE_CUSTOMERS}
}

export function setServiceCustomers(payload) {
    return {type: types.SET_SERVICE_CUSTOMERS, payload};
}

export function setServiceCustomerOrders(payload) {
    return {type: types.SET_SERVICE_CUSTOMER_ORDERS, payload};
}

export function setServiceCustomerJobs(payload) {
    return {type: types.SET_SERVICE_CUSTOMER_JOBS, payload};
}

export function toggleCustomerAddress(isOpen, identifier) {
    return {type: types.TOGGLE_CUSTOMER_ADDRESS, payload: {
        isOpen, identifier
    }};
}

export function setCurrentCustomer(payload) {
    return {type: types.SET_CURRENT_CUSTOMER, payload};
}