import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';
import { replace,push } from 'react-router-redux';

var instance = axios.create({
    header: {'content-type':'text/plain;charset=UTF-8'}
});

function requestData() {
    return {type: types.GET_PERMISSION}
};

function receiveData(json) {
    return{
        type: types.GET_PERMISSION_SUCCESS,
        data: json
    }
};

function receiveError(json) {
    return {
        type: types.GET_PERMISSION_ERROR,
        data: json
    }
};

export function getPermissionsList() {
    return function(dispatch) {
        dispatch(requestData());
        return axios.get(constant.PERMISSION_LIST_URL)
            .then(function(res){
                try {
                    for(var id in res.data.response_data){
                        if(res.data.response_data[id].permissions === '[ALL]') continue; // Avoid strange permission
                        var json = JSON.parse(res.data.response_data[id].permissions);
                        if(Object.keys(json).length){
                            res.data.response_data[id].name = Object.keys(json)[0]
                        }
                    }
                    dispatch(receiveData(res.data.response_data));
                } catch(e) {
                }
            })
            .catch(function(res){
                dispatch(receiveError(res.data));
                dispatch(push(null,'/login'));
            });
    }
};