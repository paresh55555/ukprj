import * as types from './actionTypes';
import axios from 'axios';
import {BRANDING_URL} from '../constant.js';
import { routerActions } from 'react-router-redux';

var instance = axios.create({
  header: {'content-type':'text/plain;charset=UTF-8'}
})

export function requestData() {
	return {type: types.GET_BRANDING}
}

export function savingtData() {
	return {type: types.SAVE_BRANDING}
}

export function receiveData(json) {
	return{
		type: types.GET_BRANDING_SUCCESS,
		data: json
	}
}

/**
 * Update isDeleting in the store
*/

export function deletingData() {
	return{
		type: types.DELETE_BRANDING,
	}
}

/**
 * Update isDeleting in the store
*/

export function deletedData() {
	return{
		type: types.DELETE_BRANDING_SUCCESS,
	}
}
