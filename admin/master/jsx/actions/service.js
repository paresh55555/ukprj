import * as types from './actionTypes';

export function getServiceOrders() {
    return {type: types.GET_SERVICE_ORDERS}
}

export function setServiceOrders(payload) {
    return {type: types.SET_SERVICE_ORDERS, payload};
}

export function setCurrentOrder(payload) {
    return {type: types.SERVICE_SET_CURRENT_ORDER, payload};
}

export function toggleAddOrder(payload) {
    return {type: types.SERVICE_TOGGLE_ADD_ORDER, payload};
}

export function toggleAddDoor(payload) {
    return {type: types.SERVICE_TOGGLE_ADD_DOOR, payload};
}

export function addDoor(payload) {
    return {type: types.SERVICE_ADD_DOOR, payload};
}

export function updateDoor(payload) {
    return {type: types.SERVICE_UPDATE_DOOR, payload};
}

export function getDoors() {
    return {type: types.SERVICE_GET_DOORS};
}

export function setDoors(payload) {
    return {type: types.SERVICE_SET_DOORS, payload};
}

export function setDoorTypes(payload) {
    return {type: types.SERVICE_SET_DOOR_TYPES, payload};
}

export function setTraitTypes(payload) {
    return {type: types.SERVICE_SET_TRAIT_TYPES, payload};
}

export function setCurrentTraits(payload) {
    return {type: types.SERVICE_SET_CURRENT_TRAITS, payload};
}

export function setCurrentDoor(payload) {
    return {type: types.SERVICE_SET_CURRENT_DOOR, payload};
}

export function updateCurrentDoorData(payload) {
    return {type: types.SERVICE_UPDATE_CURRENT_DOOR_DATA, payload};
}

export function setSites(payload) {
    return {type: types.SERVICE_SET_SITES, payload};
}

export function addOrder(payload) {
    return {type: types.SERVICE_ADD_ORDER, payload};
}

export function updateOrder(payload) {
    return {type: types.SERVICE_UPDATE_ORDER, payload};
}

export function togglePersonFormDialog(payload, id) {
    return {type: types.SERVICE_TOGGLE_PERSON_FORM_DIALOG, payload, id};
}

export function setPurchasers(payload) {
    return {type: types.SERVICE_SET_PURCHASERS, payload};
}

export function addPurchaser(payload) {
    return {type: types.SERVICE_ADD_PURCHASER, payload};
}

export function updatePurchaser(payload) {
    return {type: types.SERVICE_UPDATE_PURCHASER, payload};
}

export function deletePurchaser(payload) {
    return {type: types.SERVICE_DELETE_PURCHASER, payload};
}

export function setCurrentPerson(payload) {
    return {type: types.SERVICE_SET_CURRENT_PERSON, payload};
}

export function toggleAddDoc(payload) {
    return {type: types.SERVICE_TOGGLE_ADD_DOC, payload};
}

export function setDocTypes(payload) {
    return {type: types.SERVICE_SET_DOC_TYPES, payload};
}

export function setOrderDocs(payload) {
    return {type: types.SERVICE_SET_ORDER_DOCS, payload};
}

export function addDoc(payload) {
    return {type: types.SERVICE_ADD_DOC, payload};
}

export function removeDoc(payload) {
    return {type: types.SERVICE_REMOVE_DOC, payload};
}

export function addDocType(payload) {
    return {type: types.SERVICE_ADD_DOC_TYPE, payload};
}

export function addDoorType(payload) {
    return {type: types.SERVICE_ADD_DOOR_TYPE, payload};
}

export function toggleAddDoorNote(payload) {
    return {type: types.SERVICE_TOGGLE_ADD_DOOR_NOTE, payload};
}

export function addDoorNote(doorId, payload) {
    return {type: types.SERVICE_ADD_DOOR_NOTE, doorId, payload};
}

export function deleteDoorNote(doorId, noteId) {
    return {type: types.SERVICE_DELETE_DOOR_NOTE, payload:{doorId, noteId}};
}

export function upadateDoorNote( doorId, note) {
    return {type: types.SERVICE_UPDATE_DOOR_NOTE, payload:{doorId, note}};
}

export function setDoorCustomer(payload) {
    return {type: types.SERVICE_SET_DOOR_CUSTOMER, payload};
}

export function addDoorCustomer(doorId, payload) {
    return {type: types.SERVICE_ADD_DOOR_CUSTOMER, doorId, payload};
}

export function deleteDoorCustomer(doorId, cId) {
    return {type: types.SERVICE_DELETE_DOOR_CUSTOMER, payload:{doorId, cId}};
}

export function setSelectedDoorId(doorId){
    return {type:types.SERVICE_SET_SELECTED_DOOR_ID,payload:doorId}
}
export function setSearching(payload) {
    return {type: types.SERVICE_SET_SEARCHING, payload};
}

export function setSearchResults(payload) {
    return {type: types.SERVICE_SET_SEARCH_RESULTS, payload};
}

export function addOrderNote(payload) {
    return {type: types.SERVICE_ADD_ORDER_NOTE, payload};
}

export function updateOrderNote(note, index) {
    return {type: types.SERVICE_UPDATE_ORDER_NOTE, payload:{note, index}};
}

export function selectNote(note) {
    return {type: types.SERVICE_SELECTED_NOTE, payload:note};
}

export function gotoOrderActivity(payload) {
    return {type: types.GOTO_ORDER_ACTIVITY, payload};
}

export function setOrderActivity(payload) {
    return {type: types.SET_ORDER_ACTIVITY, payload};
}
