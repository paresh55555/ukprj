import * as types from './actionTypes';

export function getListBuilder(data) {
    return {
        type: types.GET_LIST_BUILDER,
        data,
    };
}

export function setCurrentList(data) {
    return {
        type: types.SET_CURRENT_LIST,
        data,
    };
}

export function setCurrentListType(data) {
    return {
        type: types.SET_CURRENT_LIST_TYPE,
        data,
    };
}

export function getListColumn(data) {
    return {
        type: types.GET_LIST_COLUMN,
        data,
    };
}
export function setBuilderDetails(data) {
    return {
        type:types.SET_BUILDER_DETAILS,
        data
    }
}
export function setSampleData(data) {
    return {
        type:types.SET_SAMPLE_DATA,
        data
    }
}
export function setSavingStatus(data) {
    return {
        type:types.SET_LIST_SAVING_STATUS,
        data
    }
}
export function setListLoadingStatus(data) {
    return {
        type:types.SET_LIST_LOADING_STATUS,
        data
    }
}