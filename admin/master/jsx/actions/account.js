import * as types from './actionTypes';

export function requestData() {
    return {type: types.GET_ACCOUNTS}
}

export function savingData() {
    return {type: types.SAVE_ACCOUNT}
}

export function receiveData(json) {
    return{
        type: types.GET_ACCOUNTS_SUCCESS,
        data: json
    }
}

export function receiveAccountData(json) {
    return{
        type: types.GET_ACCOUNT_DATA_SUCCESS,
        data: json
    }
}

export function receiveAccountBuildersData(json) {
    return{
        type: types.GET_ACCOUNT_BUILDERS_SUCCESS,
        data: json
    }
}

/**
 * Update isDeleting in the store
 */

export function deletingData() {
    return{
        type: types.DELETE_ACCOUNT,
    }
}

/**
 * Update isDeleting in the store
 */

export function deletedData() {
    return{
        type: types.DELETE_ACCOUNT_SUCCESS,
    }
}
