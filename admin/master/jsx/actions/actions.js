import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';
import { replace } from 'react-router-redux';

function requestData() {
	return {type: types.REQ_DATA}
};

function receiveData(json) {
	return{
		type: types.RECV_DATA,
		data: json
	}
};

function receiveError(json) {
	return {
		type: types.RECV_ERROR,
		data: json
	}
};
export function setUserPermission(){

}

export function fetchData(url) {
	return function(dispatch) {
		dispatch(requestData());
		return axios({
			url: constant.USER_LIST_URL,
			timeout: 20000,
			method: 'get',
			responseType: 'json'
		})
			.then(function(response) {
				dispatch(receiveData(response.data));
			})
			.catch(function(response){
				dispatch(receiveError(response.data));
				dispatch(replace(null,'/login'));
			})
	}
};
