import * as types from './actionTypes';

export function requestData() {
	return {type: types.GET_COMPONENT}
};
export function receiveData(json) {
	return{
		type: types.GET_COMPONENT_SUCCESS,
		data: json
	}
};
export function receiveSystemData(json) {
    return{
        type: types.GET_SYSTEM_COMPONENT_SUCCESS,
        data: json
    }
};
export function dataPublished(json){
    return{
        type:types.PUBLISHED_SYSTEM_COMPONENTS,
        data:json
    }
};
export function receiveMenuComponentsData(json) {
    return{
        type: types.GET_COMPONENTS_BY_MENU_SUCCESS,
        data: json
    }
};
export function receivedComponentTypes(json){
    return{
        type : types.GET_COMPONENT_TYPES_SUCCESS,
        data : json
    }
}
export function savingData(json){
    return {
        type: types.SAVING_COMPONENT_DATA,
        data: json
    }
};
export function dataSaved(json){
    return{
        type:types.SAVED_COMPONENT_DATA,
        data:json
    }
};
export function menuDataSaved(json){
    return{
        type:types.SAVED_MENU_COMPONENT_DATA,
        data:json
    }
};
export function componentsUpdated(json){
    return{
        type:types.COMPONENTS_UPDATED,
        data:json
    }
};
export function traitDataSaved(json){
    return{
        type:types.SAVED_COMPONENT_TRAIT_DATA,
        data:json
    }
};
export function deletingData(){
    return{
        type:types.DELETING_COMPONENT_DATA,
    }
}
export function deleteData(json){
    return{
        type:types.DELETED_COMPONENT_DATA,
        data:json
    }
}
export function deletedColorData(json){
    return{
        type:types.DELETED_COLOR_COMPONENT_DATA,
        data:json
    }
}
export function colorComponentUpdated(json){
    return{
        type:types.COLOR_COMPONENT_UPDATED,
        data:json
    }
};
export function setWindowComponent(json){
    return{
        type:types.SET_WINDOW_COMPONENT,
        data:json
    }
}
export function receivePartsData(json, componentID) {
    return {
        type:types.GET_COMPONENT_PARTS_SUCCESS,
        data:json,
        componentID: componentID
    }
}
export function setEditMode(mode){
    return function(dispatch){
        dispatch({
            type:types.COMPONENT_EDIT_MODE,
            data:mode
        })
    }
}
export function setPremadeMode(data){
    return function(dispatch){
        dispatch({
            type:types.COMPONENT_PREMADE_MODE,
            data:data
        })
    }
}