import * as types from './actionTypes';

export function changeEditMode() {
    return {
        type: types.CHANGE_EDIT_MODE,
    };
}

export function changeSingleEditMode() {
    return {
        type: types.CHANGE_SINGLE_EDIT_MODE,
    };
}

export function requestElement() {
    return {
        type: types.REQUEST_ELEMENTS,
    };
}

export function getElementsAction(data) {
    return {
        type: types.GET_ELEMENTS,
        data,
    };
}

export function saveElementsAction(data) {
    return {
        type: types.SAVE_ELEMENTS,
        data,
    };
}

export function updateElementAction(data) {
    return {
        type: types.UPDATE_ELEMENT,
        data,
    };
}

export function deleteElementsAction(id) {
    return {
        type: types.DELETE_ELEMENT,
        id,
    };
}

export function changeElementPosition(data) {
    return {
        type: types.CHANGE_ELEMENT_POSITION,
        data,
    };
}

export function getElementLayoutsAction(data, id) {
    return {
        type: types.GET_ELEMENT_LAYOUTS,
        data,
        id,
    };
}

export function saveElementLayoutAction(data) {
    return {
        type: types.SAVE_ELEMENT_LAYOUT,
        data,
    };
}

export function replaceElementLayoutAction(data, id) {
    return {
        type: types.REPLACE_ELEMENT_LAYOUT,
        data,
        id,
    };
}

export function updateElementLayoutAction(data) {
    return {
        type: types.UPDATE_ELEMENT_LAYOUT,
        data,
    };
}

export function deleteElementLayoutAction(id) {
    return {
        type: types.DELETE_ELEMENT_LAYOUT,
        id,
    };
}

export function saveColumnsAction(data, id) {
    return {
        type: types.SAVE_COLUMNS_LAYOUT,
        data,
        id,
    };
}

export function saveColumnElementLayoutAction(data) {
    return {
        type: types.SAVE_COLUMN_ELEMENT_LAYOUT,
        data,
    };
}

export function updateColumnElementLayoutAction(data) {
    return {
        type: types.UPDATE_COLUMN_ELEMENT_LAYOUT,
        data,
    };
}

export function backToPreviousLayout() {
    return {
        type: types.BACK_TO_PREVIOUS_LAYOUT,
    };
}

export function setCurrentElement(data) {
    return {
        type: types.SET_CURRENT_ELEMENT,
        data
    };
}


export function changeCurrentElementName(data) {
    return {
        type: types.CHANGE_CURRENT_ELEMENT_NAME,
        data,
    };
}

export function deleteColumnElementLayoutAction(layoutId, id) {
    return {
        type: types.DELETE_COLUMN_ELEMENT_LAYOUT,
        layoutId,
        id,
    };
}

export function closeEditModes() {
    return {
        type: types.CLOSE_EDIT_MODES,
    };
}

export function clearLayout() {
    return {
        type: types.CLEAR_LAYOUT,
    };
}

export function finishSaving() {
    return {
        type: types.FINISH_NEW_LAYOUT_SAVING,
    };
}

export function startSaving() {
    return {
        type: types.START_NEW_LAYOUT_SAVING,
    };
}

export function changeModalState() {
    return {
        type: types.CHANGE_MODAL_STATE,
    };
}

export function addItemAction(layoutId, columnId, data) {
    return {
        type: types.ADD_ITEM,
        layoutId,
        columnId,
        data,
    };
}

export function addSavedItemsAction(layoutId, columnId, data) {
    return {
        type: types.ADD_SAVED_ITEMS,
        layoutId,
        columnId,
        data,
    };
}

export function setCurrentColumn(data) {
    return {
        type: types.SET_CURRENT_COLUMN,
        data,
    };
}

export function cleanCurrentColumn() {
    return {
        type: types.CLEAN_CURRENT_COLUMN,
    };
}

export function backToPreviousItems(layoutId, columnId) {
    return {
        type: types.BACK_TO_PREVIOUS_ITEMS,
        layoutId,
        columnId,
    };
}

export function setLayoutCopy() {
    return {
        type: types.SET_LAYOUT_COPY,
    };
}

