import * as types from './actionTypes';
import axios from 'axios';
import * as constant from '../constant.js';
import { replace,push } from 'react-router-redux';

var instance = axios.create({
    header: {'content-type':'text/plain;charset=UTF-8'}
});
const blankSystem ={name:'',url:''}

export function requestData() {
    return {
        type:types.GET_OPTIONS,
    }
}
export function receiveData(json) {
    return {
        type:types.GET_OPTIONS_SUCCESS,
        data:json
    }
}

export function receivePartsData(json, optionID) {
    return {
        type:types.GET_OPTION_PARTS_SUCCESS,
        data:json,
        optionID: optionID
    }
}

export function receivePriceData(json, optionID) {
    return {
        type:types.GET_OPTION_PRICE_SUCCESS,
        data:json,
        optionID: optionID
    }
}

export function savingData(json){
    return {
        type: types.SAVING_OPTIONS_DATA,
        data: json
    }
}

export function dataSaved(json){
    return{
        type:types.SAVED_OPTIONS_DATA,
        data:json
    }
}

export function dataDeleted(json, optionId){
    return{
        type:types.DELETED_OPTIONS_DATA,
        data:json,
        optionId:optionId
    }
}

export function changeAllowMultiple(id){
    return{
        type:types.ALLOW_MULTIPLE_CHECK,
        id
    }
}

export function changeVisible(id){
    return{
        type:types.VISIBLE_CHECK,
        id
    }
}

export function changeHasDefault(id){
    return{
        type:types.HAS_DEFAULT_CHECK,
        id
    }
}

export function changeAllowGuests(id){
    return{
        type:types.ALLOW_GUESTS_CHECK,
        id
    }
}

export function changeCureentEdited(id){
    return{
        type:types.CHANGE_EDITED,
        id
    }
}

export function backToBaseProperties(id){
    return{
        type:types.BASE_PROPERTIES,
        id
    }
}

export function generateProperties(id){
    return{
        type:types.GENERATE_PROPERTIES,
        id
    }
}

export function receiveCostsData(json, optionID) {
    return {
        type:types.GET_OPTION_COSTS_SUCCESS,
        data:json,
        optionID: optionID
    }
}
