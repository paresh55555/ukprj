import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, useRouterHistory, Route, Link, hashHistory, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { createHistory } from 'history';
import {authRouteResolver} from './resolver';
import * as Auth from './services/authService';
import ReduxToastr from 'react-redux-toastr';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import {SQ_URL} from './constant';

//import configureStore from './store';
import store from './store';

//const store = configureStore();

const myBrowserHistory = useRouterHistory(createHistory)({
  basename: `/${SQ_URL}`
})

const history = syncHistoryWithStore(myBrowserHistory, store);

import initTranslation from './components/Common/localize';
import initLoadCss from './components/Common/load-css';

import Base from './components/Layout/Base';
import BasePage from './components/Layout/BasePage';
import BaseHorizontal from './components/Layout/BaseHorizontal';

import UsersList from './pages/Users/UsersList';

import CompaniesList from './pages/companies/CompaniesList';

import Module from './pages/modules/Module.jsx'
import ModuleLanding from './pages/modules/ModuleLanding.jsx'
import PanelRange from './pages/modules/PanelRange.jsx'
import EditPanelRange from './pages/modules/EditPanelRange.jsx'
import EditMinMax from './pages/modules/EditMinMax.jsx'
import WindowBuilder from './pages/WindowBuilder/WindowBuilder'

//components module
import ComponentLanding from './pages/components/ComponentLanding';
//system
import SystemGroups from './pages/system/SystemGroups';
import SystemLanding from './pages/system/SystemLanding';
import SystemPanelRanges from './pages/panelRanges/panelRanges';
import Pricing from './pages/Pricing/Pricing';
import AddSystem from './pages/system/AddSystem';
//site branding
import SiteBranding from './pages/site_branding/site_branding';
import SystemBuilder from './pages/system/SystemBuilder';
import DoorSystemBuilder from './pages/system/DoorSystemBuilder';
import WindowSystemBuilder from './pages/system/WindowSystemBuilder';
import Reports from './pages/Reports/Reports';
import Defaults from './pages/Defaults/Defaults';
import Carts from './pages/Cart/Carts';
import ItemsList from './pages/List/ItemsList';
import NewList from './pages/List/NewList';
import ListBuilderWrapper from './pages/List/ListBuilderWrapper';
import AccountLanding from './pages/Account/AccountLanding';
import AccountBuilder from './pages/Account/AccountBuilder';
import CartBuilder from './pages/Cart/CartBuilder';
import CartLayout from './pages/Cart/CartLayout';
import Quotes from './pages/Quotes/Quotes';
import QuotesBuilder from './pages/Quotes/QuotesBuilder';
import CartLayoutWrapper from './pages/Cart/CartLayoutWrapper';
import QuotesLayoutWrapper from './pages/Quotes/QuotesLayoutWrapper';
import CustomerDefaults from './pages/Defaults/CustomerDefaults';
import SiteDefaults from './pages/Defaults/SiteDefaults';
import SalesReports from './pages/Reports/SalesReports';
import DeliveredOrders from './pages/Reports/DeliveredOrders';
import NotFound from './components/Common/NotFound.jsx'
import ComingSoon from './components/Common/ComingSoon.jsx'
import Forbidden from './components/Common/Forbidden.jsx'
import ErrorPage from './components/Common/Error.jsx'

import SystemParts from './pages/parts/SystemParts'
import Login from './pages/Auth/Login.jsx'
import ResetPassword from './pages/Auth/ResetPassword'

import ListComponent from './pages/ListBuilder/ListComponent';
import ServiceOrders from "./pages/Service/ServiceOrders";
import ServiceJobsDetails from './pages/Service/ServiceJobsDetails'
import ServiceJobs from "./pages/Service/ServiceJobs";
import ServiceCustomers from "./pages/Service/ServiceCustomers";
import ServiceCustomersDetails from "./pages/Service/ServiceCustomersDetails";
//mobile screens
import ServiceTech from './components/Layout/ServiceTech';
import ServiceTechJobs from "./pages/ServiceTech/ServiceTechJobs";
import ServiceTechJobDeatils from './pages/ServiceTech/ServiceTechJobDeatils';





// Init translation system
// initTranslation();
// Init css loader (for themes)
initLoadCss();

function requireAuth(nextState, replace) {
  if (!Auth.loggedIn()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname },
    })
  }
}

function checkState(nextState, replace){
    if(nextState.routes[1].path == 'login' && Auth.loggedIn()){
       replace('/users')
    }
}
function clearSessionData(){
    var cookies = document.cookie;

    if(cookies.length){
        for (var i = 0; i < cookies.split(";").length; ++i)
        {
            var myCookie = cookies[i];
            var pos = myCookie.indexOf("=");
            var name = pos > -1 ? myCookie.substr(0, pos) : myCookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    localStorage.clear();
    sessionStorage.clear();
}

window.store = store;

function Root() {
    return (
        <Provider store={store}>
        <span>
            <Router history={history}>
                <Route path="/" component={Base} onEnter={requireAuth} >
                    <IndexRoute component={UsersList} />
                    <Route path="users" component={UsersList}/>
                    <Route path="companies" component={CompaniesList}/>
                    <Route path="modules" component={Module}/>
                    <Route path="modules/:id" component={ModuleLanding}/>
                    <Route path="modules/:id/:type" component={PanelRange}/>
                    <Route path="modules/:id/:type/:pid" component={EditPanelRange}/>
                    <Route path="module/size/:id" component={EditMinMax}/>
                    <Route path="/systems" component={SystemGroups}/>
                    <Route path="/systems/inactive" component={ComingSoon}/>
                    <Route path="systems/:id/add" component={AddSystem}/>
                    <Route path="systems/:id" component={SystemLanding}/>
                    <Route path="systems/:id/components" component={ComponentLanding}/>
                    <Route path="branding" component={SiteBranding}/>
                    <Route path="systembuilder" component={SystemBuilder}/>
                    <Route path="reports" component={Reports}/>
                    <Route path="reports/sales" component={SalesReports} />
                    <Route path="reports/deliveredOrders" component={DeliveredOrders} />
                    <Route path="defaults" component={Defaults}/>
                    <Route path="defaults/site" component={SiteDefaults}/>
                    <Route path="builder/cart" component={Carts} />
                    <Route path="builder/cart/newcart" component={CartBuilder} />
                    <Route path="builder/cart/:id" component={CartLayoutWrapper} />
                    <Route path="systems/:id/panelRanges" component={SystemPanelRanges}/>
                    <Route path="systems/:id/pricing" component={Pricing}/>
                    <Route path="systems/:id/parts" component={SystemParts}/>
                    <Route path="windowBuilder" component={WindowBuilder}/>
                    <Route path="listBuilder/:id" component={ListComponent}/>
                    <Route path="builder/windows" component={WindowBuilder}/>
                    <Route path="builder/accounts" component={AccountLanding} />
                    <Route path="builder/accounts/add" component={AccountBuilder} />
                    <Route path="builder/account/:id" component={AccountBuilder} />
                    <Route path="builder/customer" component={CustomerDefaults}/>
                    <Route path="builder/guests" component={ComingSoon} />
                    <Route path="builder/doors" component={ComingSoon} />
                    <Route path="builder/cutsheets" component={ComingSoon} />
                    <Route path="builder/list/newList" component={NewList} />
                    <Route path="builder/list" component={ItemsList} />
                    <Route path="builder/list/:type/:id" component={NewList} />
                    <Route path="builder/systems/doors" component={DoorSystemBuilder} />
                    <Route path="builder/systems/windows" component={WindowSystemBuilder} />
                    <Route path="builder/:type" component={Quotes} />
                    <Route path="builder/:type/newquote" component={QuotesBuilder} />
                    <Route path="builder/:type/:id" component={QuotesLayoutWrapper} />
                    <Route path="accounts/users" component={ComingSoon} />
                </Route>
                <Route path="/service" component={Base} onEnter={requireAuth} >
                    <IndexRoute component={ServiceOrders} />
                    <Route path="orders" component={ServiceOrders}/>
                    <Route path="jobs" component={ServiceJobs}/>
                    <Route path="jobs/:jobId" component={ServiceJobsDetails}/>
                    <Route path="customers" component={ServiceCustomers}/>
                    <Route path="customers/:customerId" component={ServiceCustomersDetails}/>
                    
                </Route>
                <Route path="/" component={BasePage} >
                    <Route path="login" component={Login} onEnter={clearSessionData}/>
                    <Route path="reset" component={ResetPassword} onEnter={clearSessionData}/>
                    <Route path="notfound" component={NotFound}/>
                    <Route path="forbidden" component={Forbidden}/>
                    <Route path="error" component={ErrorPage}/>
                </Route>
                <Route path="/" component={ServiceTech} onEnter={requireAuth}>
                    <Route path="serviceTechjobs" component={ServiceTechJobs}/>
                    <Route path="serviceTechjobDetails/:JobId" component={ServiceTechJobDeatils}/>
                </Route>
                <Route path="*" component={NotFound}/>
            </Router>
            <ReduxToastr/>
        </span>
        </Provider>
    )
}

const RootWithDnD = DragDropContext(HTML5Backend)(Root);

ReactDOM.render(<RootWithDnD />,
    document.getElementById('app')
);
