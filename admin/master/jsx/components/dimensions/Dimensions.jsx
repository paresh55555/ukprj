import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, Table, Alert, Modal, ButtonToolbar } from 'react-bootstrap';
import * as constant from '../../constant.js';
import {optionAction, componentAction} from '../../actions';
import Title from '../../components/Common/Title'
import ComponentTitle from '../../pages/components/ComponentTitle';

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class Dimensions extends React.Component {
	constructor() {
		super();
		self = this;
		this.state =
			{dimensions:{
				widthUnitOfMeasure:{},
				heightUnitOfMeasure:{}, 
				widthInfo:{}, 
				heightInfo:{}, 
			},
			isEdit:false,
		};
		this.setTraits = this.setTraits.bind(this);
		this.save = this.save.bind(this);
		this.cancelEdit = this.cancelEdit.bind(this);
		this.enableEdit = this.enableEdit.bind(this);
	}

	save(){
		this.setTraits(this.state.dimensions);
		this.setState({isEdit:false})
	}
	componentDidMount(){
		if(this.props.traits && this.props.traits.length>0){
			this.generateTraits(this.props.traits)
		}
	}
	generateTraits(traits){
		let newtrait=this.state.dimensions;
		traits.map((trait)=>{
			newtrait[trait.name] = trait;
		})
		this.setState({dimensions:newtrait})
	}

	componentWillReceiveProps(nextProps){
		if(this.props.traits !== nextProps.traits && nextProps.traits.length){
			this.generateTraits(nextProps.traits)
		}
	}

	onChange(key,e){
		let dimensions = this.state.dimensions;
		dimensions[key]['value'] = e;
		setTimeout(()=>this.setState({dimensions:dimensions}),500);
		
	}

    setTraits(dimensions) {
        let traits = []
        for (let key in dimensions) {
            if (dimensions[key] && !dimensions[key]['id']) {
                traits.push({
                    name: key,
                    value: dimensions[key]['value'] || '',
                    type: 'component',
                    visible: 1,
                    component_id: this.props.cid
                })
            } else {
                traits.push(dimensions[key])
            }
        }

        this.props.ApiSaveComponentTitleTraits(
            this.props.systemID,
            this.props.component.id,
            this.state.deletedNotes,
            traits
        );
        this.props.setEditMode(false)
    }

    enableEdit() {
        this.setState({isEdit: true})
        this.props.setEditMode(true)
    }

    cancelEdit() {
        this.setState({isEdit: false})
        this.props.setEditMode(false)
    }
	render() {
		const cacheBuster = new Date().getTime();
		var self = this
		var buttons = (buttonState)=>{
      return ''
    }
		return (
			<div>
				<ComponentTitle
					component={this.props.component}
					traits={this.props.traits}
					systemID={this.props.systemID}
					hideProperties={true}
					cid={this.props.component.id}
					additionalButtons={buttons}
					edit_mode={this.props.edit_mode}
					setEditMode={this.props.setEditMode}
				/>
				<div className="form-group dimensionFrom optionsBox">
					<div className="col-sm-5">
						<label className="control-label">
							<Title title={this.state.dimensions.widthInfo} defaultText={'< Width info >'} cssClass="dimensionedit" editing={this.state.isEdit} defaultID={cacheBuster+'widthInfo'} onTitleChange={this.onChange.bind(this,'widthInfo')}/>
							
						</label>
						<div className="input-group m-b">
							<input className="form-control" type="text" />
							<span className="input-group-addon">
								<Title title={this.state.dimensions.widthUnitOfMeasure} defaultText={'< Width measure unit >'} cssClass="dimensionedit" editing={this.state.isEdit} defaultID={cacheBuster+'widthUnitOfMeasure'} onTitleChange={this.onChange.bind(this,'widthUnitOfMeasure')}/>
							</span>
						</div>
						<br />
						<label className="control-label">
						<Title title={this.state.dimensions.heightInfo} defaultText={'< Height info >'} cssClass="dimensionedit" editing={this.state.isEdit} defaultID={cacheBuster+'heightInfo'} onTitleChange={this.onChange.bind(this,'heightInfo')}/>
						</label>
						<div className="input-group m-b">
							<input className="form-control" type="text" />
							<span className="input-group-addon">
							<Title title={this.state.dimensions.heightUnitOfMeasure} defaultText={'< Height measure unit >'} cssClass="dimensionedit" editing={this.state.isEdit} defaultID={cacheBuster+'heightUnitOfMeasure'} onTitleChange={this.onChange.bind(this,'heightUnitOfMeasure')}/>
							</span>
						</div>
						<br />
						{this.state.isEdit ?
							<ButtonToolbar>
								<Button onClick={this.save}>Save</Button>
								<Button onClick={this.cancelEdit}>Cancel</Button>
							</ButtonToolbar>
							:<Button onClick={this.enableEdit} disabled={this.props.edit_mode}>Edit</Button>
						}
						
					</div>
				</div>
			</div>
		);
	}

}

Dimensions.contextTypes = {
	router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        company: state.company
    }), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(Dimensions);