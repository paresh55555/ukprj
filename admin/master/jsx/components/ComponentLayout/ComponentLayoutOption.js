import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {optionAction, optionTraitAction} from '../../actions';
import {Button, Modal} from 'react-bootstrap';
import { compose, withProps } from 'recompose';
import ImageUploader from '../Common/ImageUploader/ImageUploader';
import Note from '../Common/Note';
import Title from '../Common/Title';
import ComponentImage from '../Common/ComponentImage';
import DeleteDialog from '../Common/DeleteDialog';
import { ApiOption, ApiOptionTrait } from '../../api';

class ComponentLayoutOption extends React.Component {
    constructor(){
        super()
        this.state = {
            title: {},
            image: {},
            notes: [],
            selected: {},
            deletedNotes: [],
            backupTraits: null,
            isEdit: false,
            componentDisabled: false,
            showUpload: false
        }
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.addNote = this.addNote.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
        this.onNoteChange = this.onNoteChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.deleteOption = this.deleteOption.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
    }

    componentWillMount(){
        var self = this;
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.trait.isSaving) return;
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
        if(this.props.trait.traits !== nextProps.trait.traits && nextProps.trait.isSaved && (this.props.optionID === nextProps.trait.optionId)){
            if(Array.isArray(nextProps.trait.traits)){
                this.generateTraits(nextProps.trait.traits)
                this.props.onChangeTraits(this.props.optionID, nextProps.trait.traits)
            }else{
                this.generateTraits(nextProps.trait.traits.response_data)
            }
        }
    }


    generateTraits(traits){
        var newTitle={
                name: 'title',
                value: '',
                visible:1
            },
            newSelected={
                name: 'selected',
                value: this.props.selected,
                visible:1
            },
            newNotes = [],
            newImage = {},
            self = this;
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
            if(trait.name === 'note'){
                newNotes.push(trait);
            }
            if(trait.name === 'imgUrl' && trait.value !== newImage.value){
                newImage = trait;
            }
            if(trait.name === 'selected'){
                newSelected = trait;
            }
        })
        if(!newImage.id && this.props.optionID){
            let newImage = {
                name: 'imgUrl',
                value: 'tmp',
                visible: 1
            }
            //console.log('asdf')
            this.props.ApiSaveOptionTrait(
                this.props.systemID,
                this.props.componentID,
                this.props.optionID,
                newImage
            ).then((imageTrait)=>{
                self.setState({title: newTitle, notes: newNotes, image: imageTrait, selected: newSelected});
                self.enableEdit()
            })
        }else{
            this.setState({title: newTitle, notes: newNotes, image: newImage, selected: newSelected});
        }
    }

    enableEdit(){
        if(this.state.isEdit) return;
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(Object.assign({},note));
        })
        var currentItems = {
            title: Object.assign({},this.state.title),
            notes: newNotes,
            image: Object.assign({},this.state.image)
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title,
            oldNotes = backupTraits.notes,
            oldImage = backupTraits.image;
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle, notes: oldNotes, image: oldImage});
        this.props.setEditMode(false)

    }

    addNote(){
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(note);
        })
        newNotes.push({
            name:'note',
            value:'',
            visible:1
        })
        this.setState({notes:newNotes});
    }

    deleteNote(){
        var notes = this.state.notes;
        var deletedNotes = this.state.deletedNotes;
        var deletedNote = notes.pop();
        if(deletedNote.id){
            deletedNotes.push(deletedNote);
        }
        this.setState({notes:notes, deletedNotes:deletedNotes});
    }

    onNoteChange(index, value){
        var notes = this.state.notes;
        notes[index].value = value;
        this.setState({notes:notes});
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    setSelectedValue() {

    }

    save(){
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [];
        saveTraits.push(this.state.title, this.state.selected);
        this.state.notes.map((note)=>{
            saveTraits.push(note);
        })
        if(this.state.image && this.state.image.value){
            saveTraits.push(this.state.image);
        }
        this.props.ApiSaveMultipleOptionTraits(
            this.props.systemID,
            this.props.componentID,
            this.props.optionID,
            this.state.deletedNotes,
            saveTraits
        );
        this.props.setEditMode(false)
    }

    deleteOption(){
        var self = this;
        var id = this.props.optionID;
        DeleteDialog({
            text: "Are you sure you want to delete this option? ",
            onConfirm: function(){
                self.props.deleteOption(self.props.systemID, self.props.componentID, id);
                self.props.setEditMode(false)
            }
        })
    }

    openUpload() {
        if(!this.state.isEdit){
            return;
        }
        this.setState({showUpload: true});
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            image['name'] = 'imgUrl';
            image['value'] = imgUrl;
            image['visible'] = 1;
        }
        this.setState({image:image});
    }

    closeUpload(){
        this.setState({showUpload: false})
    }

    render(){
        const optionStyle = this.props.ctype === 'thumb' ? {minHeight:this.props.height} : {}
        const buttonClassName = this.props.ctype === 'thumb' ? "thumbButton" : ""
        var optionClassName, optionTextClassName;
        const {addSelected, id} = this.props;
        switch(this.props.ctype){
            case 'halfline':
                optionClassName = 'halfOption'
                optionTextClassName = 'horizontalHalfText'
                break;
            case 'fullline':
                optionClassName = 'fullOption'
                optionTextClassName = 'horizontalFullText'
                break;
            case 'thumb':
                optionClassName = 'thumbOption'
                optionTextClassName = 'horizontalHalfText'
                break;
        }
        return(
            <div className={optionClassName} style={optionStyle}>
                {this.props.ctype === 'thumb' ?
                    <div className="horizontalOption">
                        <div className="horizontalImage" onClick={addSelected(id)}>
                            {this.props.isCurrentComponentEdit && this.props.selected ? <div className="demoColorSelected" style={{marginTop: '40px'}}></div> : null}
                            <ComponentImage image={this.state.image} width={121} height={191} onClick={this.openUpload} iconClass="thumbnailOptionLong"/>
                        </div>
                        <div className="thumbnailTitle">
                            <Title title={this.state.title} editing={this.state.isEdit} defaultID={this.props.optionID} onTitleChange={this.onTitleChange}/>
                        </div>
                        <div className="thumbnailNote">
                            {
                                this.state.notes.map((note, index) => {
                                    return <Note key={index} note={note} cssClass="horizontalNote" editing={this.state.isEdit} index={index} onNoteChange={this.onNoteChange} />
                                })
                            }
                        </div>
                    </div>
                    :
                    <div className="horizontalOption">
                        <div className="horizontalImage" onClick={addSelected(id)}>
                            {this.props.isCurrentComponentEdit && this.props.selected ? <div className="demoColorSelected"></div> : null}
                            <ComponentImage image={this.state.image} width={121} height={121} onClick={this.openUpload} iconClass="thumbnail2"/>
                        </div>
                        <div className={optionTextClassName}>
                            <Title title={this.state.title} cssClass="horizontalTitle" editing={this.state.isEdit} defaultID={this.props.optionID} onTitleChange={this.onTitleChange}/>
                            {
                                this.state.notes.map((note, index) => {
                                    return <Note key={index} note={note} cssClass="horizontalNote" editing={this.state.isEdit} index={index} onNoteChange={this.onNoteChange} />
                                })
                            }
                        </div>
                    </div>
                }
                { this.state.isEdit ?
                    <div className="editSaveDelete">
                        <Button onClick={this.save} className={"btn btn-default "+buttonClassName}>Save</Button>
                        <Button onClick={this.cancelEdit} className={"btn btn-default btn-outline "+buttonClassName}>Cancel</Button>
                        <Button onClick={this.addNote} className={"btn btn-default "+buttonClassName}>Add Note</Button>
                        { this.state.notes.length ?
                            <Button onClick={this.deleteNote} className={"btn btn-default "+buttonClassName}>Delete Note</Button> : null
                        }
                        <Button onClick={this.deleteOption} className={"btn btn-danger noBackground delete "+buttonClassName}>Delete</Button>
                    </div>
                    :
                    <div className="editSaveDelete">
                        <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className={"btn btn-default "+buttonClassName}>Edit</Button>
                        <Button onClick={this.deleteOption} disabled={this.props.edit_mode} className={"btn btn-danger noBackground delete "+buttonClassName}>Delete</Button>
                    </div>
                }
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.systemID,
                    component_id: this.props.componentID,
                    option_id: this.props.optionID,
                    trait_id: this.state.image.id,
                    type: 'option',
                    width: 121,
                    height: this.props.ctype === 'thumb' ? 191 : 121,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </div>
        )
    }
}

ComponentLayoutOption.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiOption, ApiOptionTrait);

export default compose(
    connect(state => ({
        trait: state.optionTrait,
    }), Object.assign({}, optionTraitAction, optionAction)),
    withProps(apiActions),
)(ComponentLayoutOption);
