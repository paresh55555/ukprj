import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {componentAction,optionAction} from '../../actions';
import {Button} from 'react-bootstrap';
import { ApiOption } from '../../api';

import ComponentLayoutOption from './ComponentLayoutOption';
import ComponentTitle from '../../pages/components/ComponentTitle';

class ComponentLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options:[],
            optionHeight:'auto',
            buttonHeight:'auto',
            isEdited: false,
            selectedIds: []

        };
        this.addOption = this.addOption.bind(this);
        this.onChangeTraits = this.onChangeTraits.bind(this);
        this.resetHeight = this.resetHeight.bind(this);
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
        this.changeComponentEditMode = this.changeComponentEditMode.bind(this);
    }

    addSelected(id) {
        return () => {
            if (!this.props.edit_mode || !this.props.componentDropdownState['id'+this.props.cid].hasDefault) {return}
            const sameId = this.state.selectedIds.find(item=>item === id);
            const selected = this.props.componentDropdownState['id'+this.props.cid].multiples ? this.state.selectedIds : [];
            if (sameId) {
                const index = selected.indexOf(id);
                selected.splice(index, 1);
            } else {
                selected.push(id);
            }
            this.setState({selectedIds: selected});
        };
    }

    checkSelection(id) {
        const result = this.state.selectedIds.find(item => {if(item === id){return item}});
        return !!result;
    }

    changeComponentEditMode(status) {
        this.setState({isEdited: status})
    }

    componentWillMount(){
        if(this.props.component.options){
            this.generateOptions(this.props.component.options);
        }
        if(!this.props.componentDropdownState['id'+this.props.cid]) {
            this.props.generatePropertiesApi(this.props.cid)
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.props.ctype !== 'thumb')return;
        if(this.state.options !== prevState.options){
            return;
        }
        this.resetHeight();
    }
    componentDidMount(){
        if(this.props.ctype !== 'thumb')return;
        this.resetHeight();
    }
    resetHeight(update){
        let height = [];
        this.state.options.map((thumb,index)=>{
            let optionHeight = 0;
            Array.prototype.slice.call(ReactDOM.findDOMNode(this.refs['thumb_'+thumb.id]).childNodes[0].childNodes).map((node,idx)=>{
                optionHeight += node.clientHeight
            })
            height[index] = {
                option:optionHeight,//ReactDOM.findDOMNode(this.refs['thumb_'+thumb.id]).childNodes[0].clientHeight,
                button:ReactDOM.findDOMNode(this.refs['thumb_'+thumb.id]).childNodes[1].clientHeight
            }
        })
        let maxOptionHeight = Math.max.apply(Math,height.map(function(o){return o.option;}));
        let maxButtonHeight = Math.max.apply(Math,height.map(function(o){return o.button;}));
        if(this.state.optionHeight == (maxOptionHeight+maxButtonHeight)){
            return
        }
        this.setState({optionHeight : maxOptionHeight+maxButtonHeight,buttonHeight:maxButtonHeight})

    }

    enableEdit(){
        this.props.setEditMode(true)
    }

    generateOptions(options){
        var optionsWithSelect = options.map(item => Object.assign({}, item));
        this.setState({options: optionsWithSelect});
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.option.isSaving || nextProps.option.isLoading) return;
        if(nextProps.option.isDeleted != this.props.option.isDeleted && nextProps.option.isDeleted &&
            this.state.options.find((option)=>{return option.id === nextProps.option.deletedId})){
            swal("Deleted!", this.props.component.name+" option has been deleted.", "success");
            var newOptions = this.state.options.filter((option)=>{return option.id !== nextProps.option.deletedId})
            this.setState({options: newOptions})
        }else
        if(nextProps.option.isSaved != this.props.option.isSaved && nextProps.option.isSaved){
            var savedOption = nextProps.option.savedOption && nextProps.option.savedOption.length && nextProps.option.savedOption[0]
            if(savedOption && savedOption.component_id === this.props.cid){
                var options = this.state.options;
                savedOption['traits'] = []
                options.push(savedOption);
                this.setState({options: options})
            }
        }else if(this.props.component.options != nextProps.component.options){
            this.generateOptions(nextProps.component.options);
        }
        if(!this.props.componentDropdownState['id'+this.props.cid]) {return}
        if(this.props.componentDropdownState['id'+this.props.cid].hasDefault !== nextProps.componentDropdownState['id'+this.props.cid].hasDefault &&
            !nextProps.componentDropdownState['id'+this.props.cid].hasDefault ||
            this.props.componentDropdownState['id'+this.props.cid].multiples !== nextProps.componentDropdownState['id'+this.props.cid].multiples &&
            !nextProps.componentDropdownState['id'+this.props.cid].multiples) {
            this.setState({selectedIds: []})
        }
    }

    addOption(){
        var newOption = {
            name: this.props.cname+'Option',
            allowDeleted: 1
        }
        this.props.saveOption(this.props.systemID,this.props.cid, newOption);
        this.props.setEditMode(true)
    }

    onChangeTraits(optionID, traits){
        var options = this.state.options,
            newOptions = [];
        options.map((option, index)=>{
            if(option.id === optionID){
                option['traits'] = traits;
            }
            newOptions.push(option);
        })
        this.setState({options: newOptions})
    }

    getClearfix(index){
        let clearfix = null;
        if(this.props.ctype === 'thumb'){
            clearfix = (index+1)%5 === 0 ? <div className='clearfix'><hr /></div> : null
        }
        if(this.props.ctype === 'halfline'){
            clearfix = (index+1)%2 === 0 ? <div className='clearfix'></div> : null
        }
        return clearfix
    }

    render() {
        var self = this
        var buttons = (buttonState)=>{
            return <Button onClick={this.addOption} disabled={buttonState} className="btn btn-default">Add option</Button>
        }
        return (
            <div>
                <ComponentTitle
                    component={this.props.component}
                    traits={this.props.traits}
                    systemID={this.props.systemID}
                    cid={this.props.component.id}
                    additionalButtons={buttons}
                    edit_mode={this.props.edit_mode}
                    setEditMode={this.props.setEditMode}
                    selectedIds={this.state.selectedIds}
                    changeComponentEditMode={this.changeComponentEditMode}
                />
                <div className="optionsBox">
                    {this.state.options && this.state.options.length ? this.state.options.map((option, index) =>{
                            return ([
                                <ComponentLayoutOption
                                    ref={self.props.ctype === 'thumb' ? 'thumb_'+option.id : ''}
                                    ctype={self.props.ctype}
                                    isCurrentComponentEdit={this.state.isEdited}
                                    onChangeTraits={self.onChangeTraits}
                                    traits={option.traits}
                                    height={self.state.optionHeight}
                                    key={index}
                                    componentID={self.props.component.id}
                                    optionID={option.id}
                                    edit_mode={this.props.edit_mode}
                                    setEditMode={this.props.setEditMode}
                                    selected={this.checkSelection(option.id)}
                                    addSelected={this.addSelected}
                                    id={option.id}
                                    systemID={self.props.systemID} />,
                                self.getClearfix(index)
                            ])
                        }) : 'No options available'}
                </div>
            </div>
        );
    }

}

ComponentLayout.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiOption, optionAction);

export default compose(
    connect(state => ({
        componentDropdownState: state.option.componentDropdownState,
        option: state.option,
    })),
    withProps(apiActions),
)(ComponentLayout);