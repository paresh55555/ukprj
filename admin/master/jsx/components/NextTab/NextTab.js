import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {componentAction,optionAction} from '../../actions';
import {Button} from 'react-bootstrap';
import {SQ_URL} from '../../constant'
import ComponentImage from '../../components/Common/ComponentImage'
import Title from '../../components/Common/Title'
import ImageUploader from "../Common/ImageUploader/ImageUploader";
import ComponentDeleteButton from "../../pages/components/ComponentDeleteButton";

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class NextTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            traits: [],
            backupTraits: [],
            isEdit: false,
            showUpload: false
        };
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
    }

    componentWillMount(){
        var self = this
        if(this.props.traits){
            this.setState({traits: this.props.traits});
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits && !nextProps.traits.length){
            var self = this
            this.setState({traits: nextProps.traits},function(){
                var image = self.state.traits.find((trait)=>{return trait.name === 'imgUrl'})
                if(!image){
                    let newTrait = {
                        name: 'imgUrl',
                        value: `${SQ_URL}/img/circle-arrow.png`,
                        visible: 1
                    }
                    self.saveTrait(newTrait);
                }
            });
        }
        if(this.props.traits !== nextProps.traits && nextProps.traits.length){
            this.setState({traits: nextProps.traits});
        }
        if(nextProps.cmp && nextProps.cmp.trait && this.props.cmp.trait !== nextProps.cmp.trait){
            let savedTrait = nextProps.cmp.trait.response_data[0],
                traits = this.state.traits
            if(savedTrait.component_id !== this.props.component.id)return;
            if(traits.length && traits.find(function(el,i){return el.name === savedTrait.name})){
                let newTraits = []
                traits.map((trait, i)=>{
                    let newTrait = trait
                    if(trait.name === savedTrait.name){
                        newTrait = savedTrait
                    }
                    newTraits.push(newTrait);
                })
                this.setState({traits: newTraits})
            }else{
                traits.push(savedTrait);
                this.setState({traits: traits})
            }
        }
    }

    changeTrait(key, value){
        let trait = this.state.traits.find(function(el,i){return el.name === key}),
            newTraits = this.state.traits;
        if(!trait){
            newTraits.push({
                name: key,
                value: value,
                visible: 1
            })
        }else{
            trait.value = value
        }
        this.setState({traits: newTraits})
    }

    enableEdit(){
        var newTraits = [];
        this.state.traits.map((trait)=>{
            newTraits.push(Object.assign({},trait));
        })
        this.setState({isEdit: true, backupTraits: newTraits})
        this.props.setEditMode(true)
    }

    cancelEdit(){
        this.setState({isEdit: false, traits: this.state.backupTraits})
        this.props.setEditMode(false)
    }

    openUpload(){
        console.log(this.state.isEdit)
        if(!this.state.isEdit){
            return;
        }
        this.setState({showUpload: true})
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            this.changeTrait('imgUrl', imgUrl)
        }
    }

    closeUpload(){
        this.setState({showUpload: false});
    }

    save(){
        this.props.ApiSaveComponentTraits(
            this.props.systemID,
            this.props.component.id,
            this.state.traits
        );
        this.setState({isEdit: false})
        this.props.setEditMode(false)
    }

    saveTrait(trait){
        if(trait && trait.id){
            this.props.ApiUpdateComponentTrait(this.props.systemID,this.props.component.id, trait.id, trait)
        }else{
            this.props.ApiSaveComponentTrait(this.props.systemID,this.props.component.id, trait)
        }
    }

    render() {
        const {traits} = this.state
        const title = traits.find((title)=>{return title.name === 'title'})
        const image = traits.find((title)=>{return title.name === 'imgUrl'}) || {}
        image.value = image.value || `${SQ_URL}/img/circle-arrow.png`;
        return (
            <div className="optionsBox text-center">
                <div id="nextSection">
                    <div className="nextButton">
                        <div className="nextTitle">
                            <Title title={title || {}} defaultText={'< Next tab title >'} cssClass="nextTab" editing={this.state.isEdit} defaultID={this.props.component.id} onTitleChange={this.changeTrait.bind(this, 'title')}/>
                        </div>
                        <div className="nextImage">
                            <ComponentImage image={image} width={39} height={39} onClick={this.openUpload} iconClass=""/>
                        </div>
                    </div>
                </div>
                {this.state.isEdit ?
                    <div className="editSaveDelete">
                        <Button onClick={this.save} className="btn btn-default">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                    </div>
                    :
                    <div className="editSaveDelete">
                        <Button onClick={this.enableEdit} className="btn btn-default" disabled={this.props.edit_mode}>Edit</Button>
                    </div>
                }
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.systemID,
                    component_id: this.props.component.id,
                    trait_id: image.id,
                    type: 'component',
                    width: 39,
                    height: 39,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </div>
        );
    }

}

NextTab.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        cmp: state.component,
    }), Object.assign({}, componentAction)),
    withProps(apiActions),
)(NextTab);