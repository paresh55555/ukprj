import React, { Component, PropTypes } from 'react';
import ReactDOMServer from 'react-dom/server';
import { connect } from 'react-redux';
import {Table} from 'react-bootstrap';
import { compose, withProps } from 'recompose';
import SQModal from '../../components/Common/SQModal/SQModal';
import {Button, Form, FormGroup, ControlLabel, FormControl, Alert, Checkbox, Panel} from 'react-bootstrap';
import {costAction} from '../../actions';
import { ApiCost } from '../../api';

const table_fields = {
    id: "#ID",
    name: "Cost Name",
    amount: "Per Swing Door",
    taxable: "Taxable",
    markup: "Markup",
}
class SwingDoor extends React.Component {
    constructor(){
        super();
        this.state = {
            data: [],
            costs: [],
            currentCost: {},
            isSaving: false
        };
        this.updateCost = this.updateCost.bind(this)
    }
    componentWillMount(){
        this.generateTable(this.generateData(this.props.parts));
    }
    componentWillReceiveProps(nextProps){
        if(this.props.parts !== nextProps.parts){
            this.generateTable(this.generateData(nextProps.parts));
        }
    }
    componentDidMount(){
        this.generateTable(this.state.data);
    }
    generateTable(data){
        var self = this,
            table = null,
            initTable = function(data){
                table = $('#'+self.props.id).dataTable( {
                    'destroy': true,
                    "data": data,
                    searching: false,
                    paging: false,
                    info: false,
                    aoColumns: table_fields,
                    "aoColumnDefs": [
                        {
                            "targets": 0,
                            "data": null,
                            "visible": false,
                        },
                        {
                            "targets": 1,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                return "<a class='costName' data-id="+data[0]+">" + data[1] + "</a>"
                            }
                        },
                        {
                            "targets": 3,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                let checked = data[3] ? 'checked' : ''
                                return "<input class='taxable' value="+!!data[3]+" type='checkbox' " + checked + " />";
                            }
                        },
                        {
                            "targets": 4,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                let checked = data[4] ? 'checked' : ''
                                return "<input class='markup' value="+!!data[4]+" type='checkbox' " + checked + " />"
                            }
                        },
                        {
                            "targets": 5,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                var sample = $('#sampledoor'),
                                    swings = sample.find('.swings').attr('data-val')
                                let value = swings*data[2]
                                return '$'+value
                            }
                        },
                        {
                            "targets": 6,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                var sample = $('#sampledoor'),
                                    markup = sample.find('.markUp').attr('data-val'),
                                    tax = sample.find('.tax').attr('data-val'),
                                    swings = sample.find('.swings').attr('data-val'),
                                    value = data[2]*swings,
                                    finalCost = value + (data[4] ? (value * (markup / 100)) : 0) + (data[3] ? (value * (tax / 100)) : 0);
                                return '$'+finalCost.toFixed(2)
                            }
                        },
                        {
                            "targets": 7,
                            "data": null,
                            "render": function(){
                                return ReactDOMServer.renderToString(<Button bsStyle={'danger'} onClick={self.deleteCost}> delete </Button>);
                            }
                        }]
                } );
            };
        initTable(data);
        $('#'+this.props.id + ' .taxable',).off('click').on( 'click', function(){
            self.updateCbInTable($(this),'taxable')
        });
        $('#'+this.props.id + ' .markup').off('click').on( 'click', function () {
            self.updateCbInTable($(this),'markup')
        } );
        $('#'+this.props.id+' .costName').off('click').on( 'click', function (e) {
            e.preventDefault();
            let id = $(this).data('id')
            self.setCurrentCost(id);
            self.openDialog();
        } );
        $('#'+this.props.id+' tbody').on('click', 'button', function (e) {
            e.stopPropagation();
            let tr = $(this).parents(tr);
            var data = table.api().row( tr ).data();
            self.deleteCost(data[0])
        });
    }
    generateData(parts){
        var data = [],
            costs = [];
        parts && parts.length && parts.map((part)=>{
            let cost = part.costs && part.costs.length && part.costs.find((cost)=>{return cost.cost_type === 'SwingDoor'})
            if(cost && (part.option_id === this.props.optionID || (this.props.optionID === 0 && part.component_id === this.props.componentID))){
                data.push([
                    cost.id,
                    part.name,
                    cost.amount,
                    cost.taxable,
                    cost.markup,
                    '',''
                ])
                costs.push({
                    id: cost.id,
                    name: cost.name,
                    taxable: cost.taxable,
                    markup: cost.markup,
                    amount: cost.amount
                })
            }
        })
        this.setState({data:data, costs: costs})
        return data
    }
    openDialog(){
        this.panelForm.open();
    }
    deleteCost(id){
        var self = this
        this.props.ApiDeleteCost(this.props.systemID, this.props.componentID, this.props.optionID, id).then(res =>{
            self.props.onDeleteCost(id);
        })
    }
    updateCost () {
        var self = this;
        this.setState({isSaving: true})
        var currentCost = this.state.currentCost;
        this.props.ApiUpdateCost(this.props.systemID, this.props.componentID, this.props.optionID, currentCost.id, currentCost).then((res) => {
            self.setState({isSaving: false});
            self.panelForm.close();
            self.props.onChangeCost(res[0]);
        });
    }
    updateCbInTable(el, key){
        var data = $('#'+this.props.id).DataTable().row( el.parents('tr') ).data();
        var val = el.is(':checked');

        this.setCurrentCost(data[0]);
        var currentCost = this.state.currentCost;
        currentCost[key] = val;
        this.props.ApiUpdateCost(this.props.systemID, this.props.componentID, this.props.optionID, data[0], currentCost).then((res)=>{
            this.props.onChangeCost(res[0]);
        });
        this.setState({currentCost:currentCost})
    }
    setCurrentCost(id){
        let cost = this.state.costs && this.state.costs.find((cost)=>{ return cost.id === id })
        if(cost)this.setState({currentCost:cost})
    }
    handleChange(key, event){
        let value = event.target.value;
        let currentCost = this.state.currentCost;
        if(key === 'amount'){
            currentCost[key] = value;
        }else{
            currentCost[key] = +value ? 0 : 1;
        }

        this.setState({currentCost:currentCost})
    }
    render(){
        const currentCost = this.state.currentCost;
        return(
            <div className="panel panel-default">
                <div className="panel-heading">Per Swing Door Cost</div>
                <div>
                    <Table id={this.props.id} responsive striped>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Part Name</th>
                                <th>Per Swing Door</th>
                                <th>Taxable</th>
                                <th>Apply Markup</th>
                                <th>Raw Cost</th>
                                <th>Final Cost</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <div className="ball-pulse">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </Table>
                </div>
                <SQModal
                    ref={(c)=>this.panelForm = c}
                    title={'Change cost'}
                    onConfirm={this.updateCost}
                    disabled={this.state.isSaving}
                    confirmText={this.state.isSaving ? "Saving..." : "Save"}>
                    <Form data-parsley-validate="" data-parsley-group="block-0" noValidate>
                        <FormGroup>
                            <ControlLabel>Cost</ControlLabel>
                            <FormControl value={currentCost.amount} onChange={this.handleChange.bind(this, 'amount')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Taxable</ControlLabel>
                            <Checkbox checked={!!currentCost.taxable ? true : false} value={currentCost.taxable} onChange={this.handleChange.bind(this, 'taxable')} />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel>Markup</ControlLabel>
                            <Checkbox checked={!!currentCost.markup ? true : false} value={currentCost.markup} onChange={this.handleChange.bind(this, 'markup')} />
                        </FormGroup>
                    </Form>
                </SQModal>
            </div>
        )
    }
}

const apiActions = Object.assign({}, ApiCost);

export default compose(
    connect(state => ({
        cost: state.cost,
    }), Object.assign({}, costAction)),
    withProps(apiActions),
)(SwingDoor);