import React from 'react';
import ReactTable, {ReactTableDefaults} from 'react-table'
//import 'react-table/react-table.css'
//require( 'react-table/react-table.css' );

Object.assign(ReactTableDefaults, {
    showPagination: false,
    showPageSizeOptions: false,
    sortable: false
})


export default function PercentageTable (props) {

    const generateNewTableData = (serverData) => {
        let totalCost = 0;
        const newTax = props.tax ? +props.tax : 0;
        const newMarkup = props.markup ? +props.markup : 0;
        const defaultTotal = props.defaultTotal ? +props.defaultTotal : 0;
        const newData = serverData.map(tableRow => {
                let rawCost = defaultTotal * tableRow.amount / 100;
                let finalCost = rawCost;
                if(tableRow.markup) {
                    finalCost = finalCost + (finalCost * newMarkup) / 100;
                }
                if(tableRow.taxable) {
                    finalCost = finalCost + (finalCost * newTax) / 100;
                }
                totalCost = totalCost + finalCost;
                return Object.assign({}, tableRow, {rawCost, finalCost, deleteBtn: true})
            }
        );
        return [...newData, {totalCost}]
    };

    const data = props.data.length > 0 ? generateNewTableData(props.data): [];

    const columns = [{
        id: 'name',
        Header: 'Cost Name',
        accessor: d => d.name !== undefined ? <div className="fill-cell" onClick={()=>props.openModal(d.cost_type, d)}>{d.name}</div> : null
    }, {
        id: 'amount',
        Header: 'Percentage',
        accessor: d => d.amount ? <div className="fill-cell" onClick={()=>props.openModal(d.cost_type, d)}>{d.amount + '%'}</div> : ''
    }, {
        id: 'taxable',
        Header: 'Taxable',
        accessor: d => d.taxable !== undefined ? <div className="fill-cell" onClick={()=>props.openModal(d.cost_type, d)}>
                <input type="checkbox" className='number' checked={!!d.taxable} disabled /></div> : null
    },{
        id: 'markUp',
        Header: 'Markup',
        accessor: d => d.markup !== undefined ? <div className="fill-cell" onClick={()=>props.openModal(d.cost_type, d)}>
                <input type="checkbox" className='number' checked={!!d.markup} disabled /></div> : null
    },{
        id: 'rawCost',
        Header: 'Raw Cost',
        accessor: d => d.rawCost !== undefined ? <div className="fill-cell" onClick={()=>props.openModal(d.cost_type, d)}>{'$'+d.rawCost}</div> :
            <span className="title-table">Total</span>
    }, {
        id: 'finalCost',
        Header: 'Final Cost',
        accessor: d => d.totalCost !== undefined ? <span className="title-table">${d.totalCost}</span> : <div className="fill-cell" onClick={()=>props.openModal(d.cost_type, d)}>${d.finalCost}</div>
    }, {
        id: 'deleteBtn',
        Header: '',
        accessor: d => d.totalCost === undefined ? <div className="center"><button type="button" className="btn btn-danger" onClick={()=>props.onDelete(d)}>delete</button></div> : null
    }];

    return (
        <div className="editSave">
            <div id="userTable_wrapper" className="dataTables_wrapper form-inline dt-bootstrap no-footer">
                <div className="panel panel-default">
                    <div className="panel-heading">{props.heading}</div>
                    <div className="row">
                        <div className="col-sm-12">
                            <ReactTable
                                data={data}
                                columns={columns}
                                pageSize={data.length}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-5">
                            <div className="dataTables_info" id="userTable_info"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}