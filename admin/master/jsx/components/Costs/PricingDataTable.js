import React from 'react';
import ReactDOMServer from 'react-dom/server'
import { Router, Route, Link, History } from 'react-router';
import {Table, Checkbox, Button} from 'react-bootstrap';
import * as constant from '../../constant.js';
import {companyAction} from '../../actions';

//var table ='';
var setTimeInstance ;

class PricingDataTable extends React.Component {

  constructor(props) {
    super(props);
    self= this;
    this.openEdit = this.openEdit.bind(this)
   }

    componentWillMount(){
        this.generateTable(this.props.data, this.props);
    }
    componentDidMount(){
        this.generateTable(this.props.data, this.props);
    }
   componentWillReceiveProps(nextProps){
      if(nextProps.data !== this.props.data){
        this.generateTable(nextProps.data,nextProps);
      }
   }
   generateTable(data,props){
      const id = this.props.id
      let table = ''
      if(!!data && data.length > 0){
           table = $('#'+id).dataTable({
            "bPaginate": false,
            "bLengthChange": false,
            'destroy': true,
            'paging': false, // Table pagination
            'ordering': true, // Column ordering
            'searching': false,
            'aaSorting':typeof props.defaultOrder!== 'undefined' ?  [[ props.defaultOrder, "asc" ]] : [],
            //'stateSave': true,
            'info': true, // Bottom left status text
            data:data,
            aoColumns: this.generateTableColumn(data),
            "aoColumnDefs": this.generateColumnDefs()
          });
        }
        
        $('#'+id+' tbody').off('click').on('click', 'button', function (e) {
          e.stopPropagation();
          let tr = $(this).parent().parent();  // get row
          var data = table.api().row( tr ).data();
          props.onDelete(data)
        });
        $('#'+id+' tbody').on('click', 'a.pricingName', function (e) {
          e.stopPropagation();
          let tr = $(this).parents('tr');  // get row
          var data = table.api().row( tr ).data();
          console.log('pricing clicked',{table:$(`#${self.props.id}`)})
          self.openEdit(data)
        });
          $('#'+id+' tbody').on('click', `input[type=checkbox]`, function (e) {
            e.stopPropagation();
            let tr = $(this).parents(tr);
            var data = table.api().row( tr ).data();
            if(data) {
              data[e.target.name] = e.target.checked
              props.onCheckbox(data)
            }
          });

   } 
   generateTableColumn(data){
      if(!!this.props.fields){
        data = [this.props.fields];
      }
      var jsonData = data[0];
      var columns =[]
      for(var obj in jsonData){
        if(obj != 'id' && obj != 'module_id'){
          var title = obj.replace(/([A-Z])/g, ' $1').trim();
          if(!!this.props.fields){
            title = jsonData[obj]
          }
          var title = title.replace(/_/g, ' ').trim();
          columns.push({'mData':obj,'title':title})
        }
      }
      return columns
    }
    openEdit(data){
      if(data){
        this.props.onEdit('FixedCost',data)
      }
    }
    generateColumnDefs(){
      var defs = [];
      let self = this;

      let checkBoxesFields = [
        'taxable',
        'markup',
        'swings'
      ]

      if(this.props.fields) {
        for (var prop in this.props.fields) {
          if(checkBoxesFields.indexOf(prop) > -1) {
            let classb = prop;
            defs.push({
              "aTargets":[Object.keys(this.props.fields).indexOf(prop)],
              "mData":null,
              "mRender": function (data, type, full) {
                  return ReactDOMServer.renderToString(<Checkbox name={classb} className="checkbox" checked={data} />);
              }
            })
          }
          if(prop === 'name'){
            defs.push({
              "aTargets":[Object.keys(this.props.fields).indexOf(prop)],
              "mData":null,
              "mRender": function (data, type, full) {
                  return ReactDOMServer.renderToString(<a id={full.id} className="pricingName">{data}</a>);
              }
            })
          }

        }
      }

      defs.push({
        "aTargets":[Object.keys(this.props.fields).length],
        'title':'Raw Cost',
        "mData":null,
        "mRender": function (data, type, full) {
          return ReactDOMServer.renderToString(<p>${data.rawCost || data.amount}</p>);
        }
      })

      defs.push({
        "aTargets":[Object.keys(this.props.fields).length+1],
        'title':'Final Cost',
        "mData":null,
        "mRender": function (data, type, full) {
          let raw_cost = parseInt(data.rawCost || data.amount, 10);
          let markup = data.markup ? parseInt(self.props.markup|| 0, 10) : 0;
          let markup_cost = parseInt(raw_cost) + (raw_cost * markup);
          let tax = data.taxable ? parseInt(self.props.tax || 0, 10) : 0;
          tax = tax / 100 * data.amount
          let final_cost = parseInt(markup_cost) + (markup_cost * parseInt(tax, 10));
          return ReactDOMServer.renderToString(<p>${final_cost}</p>);
        }
      })

      if(!!this.props.onDelete){
        defs.push({
          "aTargets":[Object.keys(this.props.fields).length+2],
          "mData":null,
          "mRender": function (data, type, full) {
              return ReactDOMServer.renderToString(<Button bsStyle={'danger'}> delete </Button>);
          }
        })
      }

      return defs;
    }
    
    render() {
        var self = this;

        let {heading, isLoading, data} = this.props;

        return (
          <div>
            <div className="panel-heading">{heading}</div>
            <div className="clearfix"></div>
            {data && data.length < 1 && !isLoading ? <div style={{textAlign:'center'}}>no records found</div> :
              <Table id={this.props.id} responsive striped>
                <tbody>
                    <tr>
                        <td>
                            <div className="ball-pulse">
                                 <div></div>
                                 <div></div>
                                 <div></div>
                             </div> 
                        </td>
                    </tr>
                </tbody>
            </Table>}
          </div>
            );
    }

}
PricingDataTable.defaultProps = {isLoading: true};
PricingDataTable.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default PricingDataTable;
