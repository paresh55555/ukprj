import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {Table} from 'react-bootstrap';
import SQModal from '../../components/Common/SQModal/SQModal';
import {Button, Form, FormGroup, ControlLabel, FormControl, Alert, Checkbox, Panel} from 'react-bootstrap';
import {costAction} from '../../actions';
import { ApiCost } from '../../api';

class PartSize extends Component{
    constructor(){
        super()
        this.state = {
            data: [],
            costs: [],
            currentCost: {},
            newCost: {
                taxable: false,
                markup: false,
                amount: 0
            },
            isSaving: false
        }
        this.updateCost = this.updateCost.bind(this)
        this.deleteCosts = this.deleteCosts.bind(this)
    }
    componentWillMount(){
        this.generateTable(this.generateData(this.props.parts));
    }
    componentWillReceiveProps(nextProps){
        if(this.props.parts !== nextProps.parts){
            this.generateTable(this.generateData(nextProps.parts));
        }
    }
    componentDidMount(){
        this.generateTable(this.state.data);
    }
    generateData(parts){
        var data = [],
            costs = [];
        parts && parts.length && parts.map((part)=>{
            let cost = part.costs && part.costs.length && part.costs.find((cost)=>{return cost.cost_type === 'PartCost'})
            if(cost && (part.option_id === this.props.optionID || (this.props.optionID === 0 && part.component_id === this.props.componentID))){
                var sample = $('#sampledoor'),
                    width = sample.find('.width').attr('data-val'),
                    panels = sample.find('.panels').attr('data-val'),
                    length = width * panels;
                data.push([
                    cost.id,
                    part.name,
                    length,
                    cost.amount,
                    cost.taxable,
                    cost.markup,
                    '',''
                ])
                costs.push({
                    id: cost.id,
                    name: cost.name,
                    taxable: cost.taxable,
                    markup: cost.markup,
                    amount: cost.amount
                })
            }
        })
        this.setState({data:data, costs: costs})
        return data
    }
    handleChange(key, event){
        let value = event.target.value;
        let currentCost = this.state.currentCost;
        if(key === 'amount'){
            currentCost[key] = value;
        }else{
            currentCost[key] = +value ? 0 : 1;
        }

        this.setState({currentCost:currentCost})
    }
    updateCost () {
        var self = this;
        this.setState({isSaving: true})
        var currentCost = this.state.currentCost;
        this.props.ApiUpdateCost(this.props.systemID, this.props.componentID, this.props.optionID, currentCost.id, currentCost).then((res) => {
            self.setState({isSaving: false});
            self.panelForm.close();
            self.props.onChangeCost(res[0]);
        });
    }
    deleteCosts(){
        this.props.ApiDeletePartsCosts(this.props.systemID, this.props.componentID, this.props.optionID, this.state.costs).then(()=>{
            this.generateTable(this.generateData([]));
            this.props.onDeleteCosts();
        })
    }
    openDialog(){
        this.panelForm.open();
    }
    generateTable(data){
        var self = this,
            table = null,
            initTable = function(data){
                table = $('#'+self.props.id).DataTable( {
                    "data": data,
                    searching: false,
                    paging: false,
                    info: false,
                    "columnDefs": [
                        {
                            "targets": 0,
                            "data": null,
                            "visible": false,
                        },
                        {
                            "targets": 3,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                return "<button class='cost' data-id="+data[0]+">$" + data[3] + "/meter</button>";
                            }
                        },
                        {
                            "targets": 4,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                let checked = data[4] ? 'checked' : ''
                                return "<input class='taxable' value="+!!data[4]+" type='checkbox' " + checked + " />";
                            }
                        },
                        {
                            "targets": 5,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                let checked = data[5] ? 'checked' : ''
                                return "<input class='markup' value="+!!data[5]+" type='checkbox' " + checked + " />"
                            }
                        },
                        {
                            "targets": 6,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                let value = (data[2]/1000)*data[3]
                                return '$'+value
                            }
                        },
                        {
                            "targets": 7,
                            "data": null,
                            "render": function ( data, type, full, meta ) {
                                var sample = $('#sampledoor'),
                                    markup = sample.find('.markUp').attr('data-val'),
                                    tax = sample.find('.tax').attr('data-val'),
                                    value = (data[2]/1000)*data[3],
                                    finalCost = value + (data[5] ? (value * (markup / 100)) : 0) + (data[4] ? (value * (tax / 100)) : 0);
                                return '$'+finalCost.toFixed(2)
                            }
                        }]
                } );
            };
        if ( $.fn.dataTable.isDataTable( '#'+this.props.id ) ) {
            table = $('#'+this.props.id).DataTable();
            table.clear();
            table.destroy();
        }
        initTable(data);
        $('#'+this.props.id + ' .taxable',).off('click').on( 'click', function(){
            self.updateCbInTable($(this),'taxable')
        });
        $('#'+this.props.id + ' .markup').off('click').on( 'click', function () {
            self.updateCbInTable($(this),'markup')
        } );
        $('#'+this.props.id+' .cost').off('click').on( 'click', function () {
            let id = $(this).data('id')
            self.setCurrentCost(id);
            self.openDialog()
        } );
    }
    updateCbInTable(el, key){
        var data = $('#'+this.props.id).DataTable().row( el.parents('tr') ).data();
        var val = el.is(':checked');

        this.setCurrentCost(data[0]);
        var currentCost = this.state.currentCost;
        currentCost[key] = val;
        this.props.ApiUpdateCost(this.props.systemID, this.props.componentID, this.props.optionID, data[0], currentCost).then((res)=>{
            this.props.onChangeCost(res[0]);
        });
        this.setState({currentCost:currentCost})
    }
    setCurrentCost(id){
        let cost = this.state.costs && this.state.costs.find((cost)=>{ return cost.id === id })
        if(cost)this.setState({currentCost:cost})
    }
    render(){
        const currentCost = this.state.currentCost;
        return(
            <div className="panel panel-default">
                <div className="panel-heading">Part Size Cost
                    <Button onClick={this.deleteCosts} className="pull-right btn btn-danger">Delete Cost By Size</Button>
                </div>
                <div className="clearfix"></div>
                <div>
                    <Table id={this.props.id} responsive striped>
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Part Name</th>
                            <th>Total Length</th>
                            <th>Cost / Meter</th>
                            <th>Taxable</th>
                            <th>Apply Markup</th>
                            <th>Raw Cost</th>
                            <th>Final Cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <div className="ball-pulse">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </Table>
                </div>
                <SQModal
                    ref={(c)=>this.panelForm = c}
                    title={'Change cost'}
                    onConfirm={this.updateCost}
                    disabled={this.state.isSaving}
                    confirmText={this.state.isSaving ? "Saving..." : "Save"}>
                    <Form className="cost-form" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                        <FormGroup>
                            <ControlLabel>Cost</ControlLabel>
                            <FormControl value={currentCost.amount} onChange={this.handleChange.bind(this, 'amount')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel htmlFor="cost-taxable">Taxable</ControlLabel>
                            <input type="checkbox" id="cost-taxable" checked={!!currentCost.taxable ? true : false} value={currentCost.taxable} onChange={this.handleChange.bind(this, 'taxable')} />
                        </FormGroup>
                        <FormGroup>
                            <ControlLabel htmlFor="cost-markup">Markup</ControlLabel>
                            <input type="checkbox" id="cost-markup" checked={!!currentCost.markup ? true : false} value={currentCost.markup} onChange={this.handleChange.bind(this, 'markup')} />
                        </FormGroup>
                    </Form>
                </SQModal>
            </div>
        )
    }
}

const apiActions = Object.assign({}, ApiCost);

export default compose(
    connect(state => ({
        cost: state.cost,
    }), Object.assign({}, costAction)),
    withProps(apiActions),
)(PartSize);