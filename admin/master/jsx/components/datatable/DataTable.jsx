import React from 'react';
import ReactDOMServer from 'react-dom/server'
import { Router, Route, Link, History } from 'react-router';
import {Table, Checkbox} from 'react-bootstrap';
import * as constant from '../../constant.js';
import Loader from "../Common/Loader";
var table ='';
class DataTable extends React.Component {

  constructor(props) {
    super(props);
   const self= this;
   }
   componentWillReceiveProps(nextProps){
      if((nextProps.data !== this.props.data) || (nextProps.fields !== this.props.fields)){
              this.generateTable(nextProps.data, nextProps);
      }
   }
    generateTable(data, props) {
        const id = props.id;
        let pageStart = 0;
        if ($.fn.DataTable.isDataTable( '#'+id ) ) {
          let api = table.api()
          if(!!api.page.info().page){
            pageStart = api.page.info().page;
          }
          api.destroy()
        }
        const pagination = props.pagination !== undefined ? props.pagination : true;
        if (!!data && data.length > 0) {

            $('#' + id).find('thead').empty();
            $('#' + id).find('tbody').empty();

            let tableTemplate = props.tableTemplate || "<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                sorting = (typeof props.defaultOrder !== 'undefined') ? [[props.defaultOrder, "asc"]] : [];

            table = $('#' + id).dataTable({
                'destroy': true,
                'paging': pagination, // Table pagination
                'ordering': true, // Column ordering
                'aaSorting': sorting,
                //'stateSave': true,
                'info': true, // Bottom left status text
                'data': data,
                "aoColumns": this.generateTableColumn(data, props),
                "aoColumnDefs": this.generateColumnDefs(props),
                "sDom": tableTemplate
            });
        }
        if(!props.noClick){
            $('#' + id + ' tbody').off('click', 'tr').on('click', 'tr', function () {
                var data = table.api().row(this).data();
                if (!!props.link) {
                    self.context.router.push(constant.SQ_PREFIX + props.link + '/' + data.id);
                }
                else if (!!props.onClick) {
                    props.onClick(data);
                }
            })
        }
        $('#' + id + ' tbody').on('click', 'button', function (e) {
            e.stopPropagation();
            let tr = $(this).parents(tr);
            var data = table.api().row(tr).data();
            props.action(data)
        });

        if (props.renderCheckbox) {
            $('#' + id + ' tbody').on('click', `input[name='${props.renderCheckbox}']`, function (e) {
                e.stopPropagation();
                let tr = $(this).parents(tr);
                var data = table.api().row(tr).data();
                if (data) {
                    props.onCheckbox(Object.assign({}, data, {on_cut_sheet: e.target.checked}))
                }
            });
        }
      if ($.fn.DataTable.isDataTable( '#'+id ) ) {
        table.fnPageChange(pageStart)
      }
    }
   generateTableColumn(data, props){

      if(!!props.fields){
        data = [props.fields];
      }
      var jsonData = data[0];
      var columns =[]
      for(var obj in jsonData){
        if(obj != 'id' && obj != 'module_id'){
          var title = obj.replace(/([A-Z])/g, ' $1').trim();
          if(!!props.fields){
            title = jsonData[obj]
          }
          var title = title.replace(/_/g, ' ').trim();
          columns.push({'mData':obj,'title':title,'defaultContent':''})
        }
      }
      return columns
    }

    generateColumnDefs(props) {
        var defs = []
        if (!!props.action) {
            defs.push({
                "aTargets": [Object.keys(props.fields).length],
                "mData": null,
                "mRender": function (data, type, full) {
                    return ReactDOMServer.renderToString(props.actionTag);
                }
            })
        }

        if (props.renderCheckbox) {

            defs.push({
                "aTargets": [Object.keys(props.fields).indexOf(props.renderCheckbox)],
                "mData": null,
                "mRender": function (data, type, full) {
                    return ReactDOMServer.renderToString(<Checkbox name={props.renderCheckbox} checked={data}/>);
                }
            })
        }
        if(props.defs){
            props.defs.map((def)=>{
                defs.push(def)
            })
        }
        return defs;
    }

    noData() {
        if (this.props.isLoading) {
            return
        }
        if (typeof this.props.data == 'undefined') {
            return
        }
        if (this.props.data.length >= 1) {
            return
        }
        return true;
    }

    renderData() {
        if (this.noData()) {
            return this.renderNoData()
        }else{
            return this.renderTable()
        }
    }

    renderTable() {
        return (
            <Table id={this.props.id} className={this.props.className} striped>
                <tbody>
                <tr>
                    <td>
                        <Loader />
                    </td>
                </tr>
                </tbody>
            </Table>
        )
    }

    renderNoData(){
      return <div className={'text-center'}>no records found</div>
    }

    render() {
        return (
            this.renderData()
        );
    }

}
DataTable.defaultProps = {isLoading: true};
DataTable.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default DataTable;
