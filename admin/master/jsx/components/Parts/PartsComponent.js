import React, { Component, PropTypes } from 'react';
import FoldingPanel from '../../components/Common/FoldingPanel';
import PartsOption from './PartsOption';

export default class PartsComponent extends React.Component {
    constructor(){
        super();
        this.state = {};
        this.getContent = this.getContent.bind(this)
    }
    componentWillMount(){
    }
    getContent(options){
        var self = this;
        let content = options && options.map((option)=>{
            return <PartsOption key={option.id} systemID={self.props.systemID} componentID={self.props.component.id} option={option} />;
        })
        return content.length ? content : [<div key={new Date()}>No options</div>]
    }
    render(){
        const {component, options} = this.props;
        const title = component && component.traits.find((trait)=>{return trait.name === 'title'});
        return(
            <div>
                <FoldingPanel key={component && component.id} title={"Parts for: " + (title && title.value)} content={this.getContent(options)}/>
            </div>
        )
    }
}