import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {Button, Form, FormGroup, ControlLabel, FormControl, Alert, Checkbox, Panel} from 'react-bootstrap';
import DataTable from '../../components/datatable/DataTable.jsx';
import {componentAction, optionAction} from '../../actions';
import SQModal from '../../components/Common/SQModal/SQModal';
import {NotifyAlert} from '../../components/Common/notify.js';
import { ApiOption, ApiComponent } from '../../api';

const table_fields = {
    number: "Part #",
    name: "Part Name",
    quantity_formula: "Quantity Formula",
    size_formula: "Size Formula",
    on_cut_sheet: "On Cut Sheet",
    size: "Size"
}
class PartsOption extends React.Component {
    constructor(){
        super()
        this.state = {
            parts: [],
            content: <div>No data</div>,
            openAddModal: false,
            isSaving: false,
            err: '',
            editing: false,
            selectedPart: null,
            deleting: false,
            newPart: {
                number: '',
                name: '',
                quantity_formula: '',
                size_formula: '',
                on_cut_sheet: true
            }
        }
        this.addModal = this.addModal.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onEdit = this.onEdit.bind(this);
        this.setErr = this.setErr.bind(this);
        this.validate = this.validate.bind(this);
        this.resetForm = this.resetForm.bind(this);
        this.evaluateTrait = this.evaluateTrait.bind(this);
        this.toggleCutSheet = this.toggleCutSheet.bind(this);
        this.updateEditData = this.updateEditData.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onCheckbox = this.onCheckbox.bind(this);
    }

    addModal () {
        this.setState({
            openAddModal: true,
            newPart: {},
            editing: false
        })

        this.panelForm.open();
    }

    handleChange(key, event){
        let value = event.target.value;
        this.setState((prevState) => {
            let {newPart} = prevState;
            newPart[key] = !!value ? value : 0;

            return {
                newPart
            }
        })
    }

    onSave () {

        //this.setState({isSaving: true})
        let {newPart} = this.state;
        console.log(newPart)

         if ((this.props.component && this.props.component.type === 'Dimensions') || (this.props.option && this.props.option.type === 'ColorGroup')) {

            let {system_id, grouped_under} = this.props.option || this.props.component;
            const id = grouped_under;
            this.props.ApiAddPartToComponents(system_id, this.props.component.id, newPart, this.state.parts)
                .then(res => {
                    this.setState({isSaving: false});
                    this.panelForm.close();
                    this.resetForm();
                    NotifyAlert(`Part saved successfully`);
                });
        
        } else { 
            const [trait] = this.props.option.traits;
            const {system_id, component_id, option_id} = trait;

            this.props.ApiAddPartToOptions(system_id, component_id, option_id, newPart, this.state.parts)
            .then(res => {
                this.setState({isSaving: false});
                this.panelForm.close();
                this.resetForm();
                NotifyAlert(`Part saved successfully`);
            });
        }

    }

    onEdit (newPart) {

        this.setState({isSaving: true})

         if ((this.props.component && this.props.component.type === 'Dimensions') || (this.props.option && this.props.option.type === 'ColorGroup')) {
            let {system_id, grouped_under} = this.props.option || this.props.component;
            let id = grouped_under;
            if(this.props.component.type === 'Dimensions'){
                id = this.props.component.id;
            }
            let partId = this.state.selectedPart.id;

            console.log(this.state.selectedPart)

            this.props.ApiUpdateComponentsParts(system_id, id, newPart, partId, this.state.parts)
                .then(res => {
                    NotifyAlert(`Part updated successfully`)
                    this.resetForm();
                });
        
        } else { 
            const [trait] = this.props.option.traits;
            const {system_id, component_id, option_id} = trait;
            let partId = this.state.selectedPart.id;

            this.props.ApiUpdateOptionsParts(system_id, component_id, option_id, newPart, partId, this.state.parts)
            .then(res => {
                NotifyAlert(`Part updated successfully`)
                this.resetForm();
            });
        }

    }

    setErr (err) {
        this.setState({err});
    }

    resetForm () {
        this.setState({
            newPart: {
                number: '',
                name: '',
                quantity_formula: '',
                size_formula: '',
                on_cut_sheet: true
            },
            editing: false,
            selectedPart: null,
            isSaving: false
        })
        this.panelForm.close();
    }

    evaluateTrait (input) {
        const {sampleDoors} = this.props.system;

        let evaluated = input;

        let traits = sampleDoors && sampleDoors[0] && sampleDoors[0].traits && sampleDoors[0].traits.length > 0 && sampleDoors[0].traits || [
            {
                name: 'width',
                value: 0
            },
            {
                name: 'height',
                value: 0
            },
            {
                name: 'panels',
                value: 0
            },
            {
                name: 'swings',
                value: 0
            },
            {
                name: 'burnOff',
                value: 0
            },
            {
                name: 'waste',
                value: 0
            }
        ];

        for(let i = 0, len = traits.length; i < len; i++) {

            let trait = traits[i];

            let quantity_index = input.toLowerCase().indexOf(trait.name);

            if(quantity_index > -1) {
                let new_quantity = input.toLowerCase().replace(trait.name, (trait.value || 0));
                evaluated = eval(new_quantity);
                break;
            }
        }

        return evaluated
    }

    validate () {

        this.setErr('');

        if(!this.state.newPart.number) {
            this.setErr('Part number is required');
            return;
        }

        if(this.state.newPart.number && isNaN(this.state.newPart.number)) {
            this.setErr('Part number should be a number');
            return;
        }

        if(!this.state.newPart.name) {
            this.setErr('Part name is required');
            return;
        }

        if(!this.state.newPart.quantity_formula) {
            this.setErr('Part quantity formula is required');
            return;
        }

        if(this.state.newPart.quantity_formula) {
            let quanity = this.evaluateTrait(this.state.newPart.quantity_formula)
            if(isNaN(quanity)) {
                this.setErr('Please input a valid Quantity Formula');
                return;
            }
        }

        if(!this.state.newPart.size_formula) {
            this.setErr('Part size formula is required');
            return;
        }

        if(this.state.newPart.size_formula) {
            let size = this.evaluateTrait(this.state.newPart.size_formula)
            if(isNaN(size)) {
                this.setErr('Please input a valid Size Formula');
                return;
            }
        }

        this.state.editing ? this.onEdit(this.state.newPart) : this.onSave();

    }

    componentWillMount(){

        if(this.props.component && this.props.component.type === 'Dimensions') {
            this.props.ApiGetComponentParts(this.props.systemID, this.props.componentID)
        }

        if(this.props.option){
            this.props.option.type === 'ColorGroup' ?
                this.props.ApiGetComponentParts(this.props.systemID, this.props.componentID)
                :
                this.props.getOptionParts(this.props.systemID, this.props.componentID, this.props.option.id)
        }
    }
    componentWillReceiveProps(nextProps){

        if((this.props.component && this.props.component.type === 'Dimensions') || (this.props.option && this.props.option.type === 'ColorGroup')){
            if(nextProps.cmp.componentID === this.props.componentID){
                this.setState({parts: nextProps.cmp.parts, isLoading: nextProps.cmp.isLoading})
            }

        }else{
            if(nextProps.opt.optionID && (nextProps.opt.optionID === this.props.option.id)){
                this.setState({parts: nextProps.opt.parts, isLoading: nextProps.opt.isLoading})
            }
        }
    }
    toggleCutSheet () {
        this.setState(prevState => {
            let {newPart} = prevState;
            newPart.on_cut_sheet = !newPart.on_cut_sheet;
            return {
                newPart
            }
        })
    }
    updateEditData (data) {

        if(data) {
            const {number,
                name,
                quantity_formula,
                size_formula,
                on_cut_sheet} = data;

            this.setState({
                newPart: {
                    number,
                    name,
                    quantity_formula,
                    size_formula,
                    on_cut_sheet
                },
                selectedPart: data,
                editing: true
            })

            this.panelForm.open()
        }

    }
    onDelete (data) {
        

        if(data) {
            let partId = data.id;
            if ((this.props.component && this.props.component.type === 'Dimensions') || (this.props.option && this.props.option.type === 'ColorGroup')) {

                let {system_id, grouped_under} = this.props.option || this.props.component;
                const id = grouped_under;

                this.props.ApiDeleteComponentsParts(system_id, id, partId, this.state.parts)
                    .then(res => NotifyAlert(`Part deleted successfully`))
        
            } else { 
                const [trait] = this.props.option.traits;
                const {system_id, component_id, option_id} = trait;

                this.props.ApiDeleteOptionsParts(system_id, component_id, option_id, partId, this.state.parts)
                    .then(res => NotifyAlert(`Part deleted successfully`))
            }
        }
    }

    onCheckbox (data) {

        if(data) {
            this.setState({
                selectedPart: data
            })

            let {number,name,quantity_formula,size_formula,on_cut_sheet} = data;

            let updateData = {number,name,quantity_formula,size_formula,on_cut_sheet}

            this.onEdit(Object.assign({}, updateData, { on_cut_sheet: data.on_cut_sheet }))
        }
    }

    render(){
        const title = this.props.option && this.props.option.traits.find((trait)=>{ return trait.name === 'title' })
        const type = this.props.option && this.props.option.type === 'ColorGroup' ? 'cmp' : 'opt';
        const {openAddModal, err, isLoading} = this.state;
        const options = this.state.newPart;
        const {sampleDoors} = this.props.system;

        let parts = this.state.parts && this.state.parts.length > 0 && this.state.parts.map(part => {

            let new_size = part.size_formula, new_quantity = part.quantity_formula;

            let traits = sampleDoors && sampleDoors[0] && sampleDoors[0].traits && sampleDoors[0].traits.length > 0 && sampleDoors[0].traits || [
                {
                    name: 'width',
                    value: 0
                },
                {
                    name: 'height',
                    value: 0
                },
                {
                    name: 'panels',
                    value: 0
                },
                {
                    name: 'swings',
                    value: 0
                },
                {
                    name: 'burnOff',
                    value: 0
                },
                {
                    name: 'waste',
                    value: 0
                }
            ];
            traits.forEach(trait => {
               let size_index = part.size_formula.toLowerCase().indexOf(trait.name);
               let quantity_index = part.quantity_formula.toLowerCase().indexOf(trait.name);

               if(size_index > -1) {
                new_size = part.size_formula.toLowerCase().replace(trait.name, (trait.value|| 0));
               }

               if(quantity_index > -1) {
                new_quantity = part.quantity_formula.toLowerCase().replace(trait.name, (trait.value|| 0));
               }

            })

            return Object.assign({}, part, {size: eval(new_size.toLowerCase()) + 'mm', quantity: eval(new_quantity.toLowerCase())});

        })

        let style = {
            wordWrap: 'break-word',
            whiteSpace: 'normal'
        }

        return(
            <div className="optionsBox">
                <div className="colorGroupMenu">
                {this.props.component ? null : (
                    this.props.option && this.props.option.type === 'ColorGroup' ?
                        <div className="groupTitle mb-sm mr-sm btn btn-success btn-outline colorGroupButton">{title && title.value}</div>
                        :
                        <div className="mb-smbtn-outline colorGroupButton optionsTitle">Option: {title && title.value}</div>
                    
                )}
                    <Button onClick={this.addModal} className="btn btn-default">Add Part</Button>
                </div>
                <div className="editSave">
                    <DataTable
                        actionTag={
                            <Button bsStyle={'danger'}> delete </Button>
                        }
                        action={this.onDelete}
                        onClick={this.updateEditData}
                        id={'parts-'+(this.props.option && this.props.option.id)}
                        data={parts || []}
                        isLoading={isLoading}
                        fields={table_fields}
                        renderCheckbox='on_cut_sheet'
                        onCheckbox={this.onCheckbox}
                        defaultOrder={0} />
                </div>
                <SQModal
                    ref={(c)=>this.panelForm = c}
                    title={this.state.editing ? 'Edit Part' : 'Add Part'}
                    onConfirm={this.validate}
                    onClose={this.onClose}
                    disabled={this.state.isSaving}
                    confirmText={this.state.isSaving ? "Saving..." : "Save"}>
                    <Form className="parts-form" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                        <FormGroup>
                             <ControlLabel>Part Number</ControlLabel>
                             <FormControl value={options.number} onChange={this.handleChange.bind(this, 'number')} type="number"/>
                        </FormGroup>
                        <FormGroup>
                             <ControlLabel>Part Name</ControlLabel>
                             <FormControl value={options.name} onChange={this.handleChange.bind(this, 'name')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                             <ControlLabel>Quanity Formula</ControlLabel>
                             <FormControl value={options.quantity_formula} onChange={this.handleChange.bind(this, 'quantity_formula')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                             <ControlLabel>Size Formula</ControlLabel>
                             <FormControl value={options.size_formula} onChange={this.handleChange.bind(this, 'size_formula')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                             <ControlLabel>Show On Cut Sheet</ControlLabel>
                             <Checkbox checked={options.on_cut_sheet} value={options.cut_sheet} onChange={this.toggleCutSheet} />
                        </FormGroup>
                        {err ? <Alert bsStyle="danger">
                            {err}
                        </Alert> : null}
                    </Form>
                </SQModal>
            </div>
        )
    }
}


const apiActions = Object.assign({}, ApiOption, ApiComponent);

export default compose(
    connect(state => ({
        opt: state.option,
        cmp: state.component,
        system: state.system,
    }), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(PartsOption);
