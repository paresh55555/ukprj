import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, ButtonToolbar, Radio } from 'react-bootstrap';
import {optionAction, componentAction} from '../../actions';
import Title from '../../components/Common/Title'
import ImageUploader from '../../components/Common/ImageUploader/ImageUploader'
import ComponentTitle from '../../pages/components/ComponentTitle';
import PanelsExample from './PanelsExample';
import {SQ_URL} from '../../constant'
import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class DoorPanels extends React.Component {
    constructor() {
        super();
        this.state = {
            isEdit:false,
            backupTraits: null,
            showUpload: false,
            panelLeft: false,
            panelRight: false,
            panelNone: false,
            panels: {
                panelTitle: {},
                imgUrl: {},
                panelPlacement: {}
            }
        };
        this.setTraits = this.setTraits.bind(this);
        this.save = this.save.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
        this.changeSelectedRadio = this.changeSelectedRadio.bind(this);
        this.onChangeRadio = this.onChangeRadio.bind(this);
    }

    save(){
        this.setTraits(this.state.panels);
        this.setState({isEdit:false})
    }
    componentWillMount(){
        if(!this.props.traits.find(item=>item.name === 'imgUrl')){
            this.props.ApiSaveComponentTitleTraits(
                this.props.systemID,
                this.props.component.id,
                [],
                [{
                    name: 'imgUrl',
                    value: 'tmp',
                    visible: 1
                }]
            );
        }
        if(this.props.traits && this.props.traits.length>0){
            this.generateTraits(this.props.traits);
            let panelPlacement = this.props.traits.find(item=>item.name === 'panelPlacement');
            if(panelPlacement) {
                this.changeSelectedRadio(panelPlacement.value);
            }
        }
    }
    generateTraits(traits){
        let newtrait = this.state.panels;
        traits.map((trait)=>{
            newtrait[trait.name] = trait;
        })
        this.setState({panels:newtrait})
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits && nextProps.traits.length){
            this.generateTraits(nextProps.traits)
        }
    }

    onChange(key, value) {
        let panels = this.state.panels;
        panels[key]['value'] = value;
        this.setState({panels: panels})
    }

    onChangeRadio(key, value) {
        let panels = this.state.panels;
        panels[key]['value'] = value;
        this.setState({panels: panels});
        this.changeSelectedRadio(value);
    }

    changeSelectedRadio(value) {
        switch(value) {
            case 'left':
                this.setState({
                    panelLeft: true,
                    panelRight: false,
                    panelNone: false
                });
                break;
            case 'right':
                this.setState({
                    panelLeft: false,
                    panelRight: true,
                    panelNone: false
                });
                break;
            case 'none':
                this.setState({
                    panelLeft: false,
                    panelRight: false,
                    panelNone: true
                });
                break;
            default:
                this.setState({
                    panelLeft: false,
                    panelRight: false,
                    panelNone: false
                });
                break;
        }
    }

    openUpload() {
        if(!this.state.isEdit){
            return;
        }
        this.setState({showUpload: true});
    }

    onUpload(imgUrl){
        var panels = this.state.panels;
        if(imgUrl){
            panels.imgUrl['name'] = 'imgUrl';
            panels.imgUrl['value'] = imgUrl;
            panels.imgUrl['visible'] = 1;
        }
        this.setState({panels});
    }

    closeUpload(){
        this.setState({showUpload: false});
    }

    setTraits(panels) {
        let traits = [];
        for (let key in panels) {
            if (panels[key] && !panels[key]['id']) {
                traits.push({
                    name: key,
                    value: panels[key]['value'] || '',
                    type: 'component',
                    visible: 1,
                    component_id: this.props.cid
                })
            } else {
                traits.push(panels[key])
            }
        }

        this.props.ApiSaveComponentTitleTraits(
            this.props.systemID,
            this.props.component.id,
            this.state.deletedNotes,
            traits
        );
        this.props.setEditMode(false)
    }

    enableEdit() {
        var currentPanels = Object.assign({}, {
            panelTitle: Object.assign({}, this.state.panels.panelTitle),
            imgUrl: Object.assign({}, this.state.panels.imgUrl),
            panelPlacement: Object.assign({}, this.state.panels.panelPlacement)
        });
        this.setState({isEdit: true, backupTraits: currentPanels});
        this.props.setEditMode(true);
    }

    cancelEdit() {
        this.changeSelectedRadio(this.state.backupTraits.panelPlacement.value);
        this.setState({isEdit: false, panels: this.state.backupTraits});
        this.props.setEditMode(false);
    }



    render() {
        var self = this;
        var buttons = (buttonState)=>{
            return ''
        };
        const imageSrc = this.state.panels.imgUrl.value && this.state.panels.imgUrl.value !== 'tmp' ? '/' + this.state.panels.imgUrl.value : '/'+SQ_URL+'/img/slide-panel.gif';
        let panelPlacement = this.props.traits.find(item=>item.name === 'panelPlacement');

        return (
            <div>
                <ComponentTitle
                    component={this.props.component}
                    traits={this.props.traits}
                    systemID={this.props.systemID}
                    hideProperties={true}
                    cid={this.props.component.id}
                    additionalButtons={buttons}
                    edit_mode={this.props.edit_mode}
                    setEditMode={this.props.setEditMode}
                />
                <div className="form-group dimensionFrom optionsBox">
                        <div className="horizontalOption">
                            <div className="horizontalContainer">
                                <div className="horizontalImages">
                                    <img onClick={this.openUpload }
                                         width="51px" height="121px"
                                         src={imageSrc} />
                                </div>
                                <Title title={this.state.panels.panelTitle} cssClass="panelTitle" editing={this.state.isEdit} onTitleChange={this.onChange.bind(this,'panelTitle')} />
                            </div>
                            <div className="numberPlacement">
                                <span>Number Placement</span>
                                <Radio disabled={this.state.isEdit ? null : true} checked={this.state.panelLeft} onClick={()=>this.onChangeRadio('panelPlacement', 'left')}>Left</Radio>
                                <Radio disabled={this.state.isEdit ? null : true} checked={this.state.panelRight} onClick={()=>this.onChangeRadio('panelPlacement', 'right')}>Right</Radio>
                                <Radio disabled={this.state.isEdit ? null : true} checked={this.state.panelNone} onClick={()=>this.onChangeRadio('panelPlacement', 'none')}>None</Radio>
                            </div>
                        </div>
                        <br />
                        {this.state.isEdit ?
                            <ButtonToolbar>
                                <Button onClick={this.save}>Save</Button>
                                <Button onClick={this.cancelEdit}>Cancel</Button>
                            </ButtonToolbar>
                            :<Button onClick={this.enableEdit} disabled={this.props.edit_mode}>Edit</Button>
                        }
                        <ImageUploader show={this.state.showUpload} config={{
                            system_id: this.props.systemID,
                            component_id: this.props.component.id,
                            trait_id: this.state.panels.imgUrl.id,
                            type: 'component',
                            width: 50,
                            height: 120,
                        }} onUpload={this.onUpload} onClose={this.closeUpload}/>
                    <hr/>
                    <div className="examplesPanel">
                        <div className="title">Examples</div>
                        <PanelsExample panelTitle={this.state.panels.panelTitle.value ? this.state.panels.panelTitle.value : null}
                                       key={4}
                                       numberOfPanels={4}
                                       panelPlacement={this.state.panels.panelPlacement.value ? this.state.panels.panelPlacement.value : null}
                                       imageSrc={imageSrc}/>
                        <PanelsExample panelTitle={this.state.panels.panelTitle.value ? this.state.panels.panelTitle.value : null}
                                       key={5}
                                       numberOfPanels={5}
                                       panelPlacement={this.state.panels.panelPlacement.value ? this.state.panels.panelPlacement.value : null}
                                       imageSrc={imageSrc}/>
                        <PanelsExample panelTitle={this.state.panels.panelTitle.value ? this.state.panels.panelTitle.value : null}
                                       key={6}
                                       numberOfPanels={6}
                                       panelPlacement={this.state.panels.panelPlacement.value ? this.state.panels.panelPlacement.value : null}
                                       imageSrc={imageSrc}/>
                    </div>
                </div>
            </div>
        );
    }

}

DoorPanels.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({}), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(DoorPanels);