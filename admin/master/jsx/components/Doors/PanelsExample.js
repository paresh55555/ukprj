import React from 'react';

const PanelsExample = (props) => {
    const createExampleArray = (number, imageSrc) => {
        let imagesArray = [];
        for(let i = 0; i < number; i++) {
            imagesArray.push(imageSrc)
        }
        return imagesArray;
    };
    return (
        <div className="panelExample">
            {createExampleArray(props.numberOfPanels, props.imageSrc).map((image, index) => (<img style={{width: 200/props.numberOfPanels, height: 120}} key={index} src={image} alt="" />))}
            <div>
                {!props.panelPlacement || props.panelPlacement === 'left' ? props.numberOfPanels+' ': null}
                {props.panelTitle ? props.panelTitle : 'Panels'}
                {props.panelPlacement === 'right' ? ' '+props.numberOfPanels: null}
            </div>
        </div>
    )

}

export default PanelsExample;