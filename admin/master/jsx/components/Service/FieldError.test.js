import React from 'react';
import {shallow} from 'enzyme';
import FieldError from './FieldError';

describe('<FieldError/>',()=>{

    let component,props={
        text:'test error',
        error:false
    };

    beforeEach(()=>{
        component = shallow(<FieldError {...props}/>);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('expected Html',()=>{
        expect(component.html()).toEqual(null);
    });

    it('when error',()=>{
       props.error = true;
        component = shallow(<FieldError {...props}/>);
        expect(component.html()).toEqual(`<p class="text-danger">${props.text}</p>`);
    });

});