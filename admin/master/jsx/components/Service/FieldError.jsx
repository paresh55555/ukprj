import React, {PropTypes} from 'react';

export default function FieldError(props) {
    let error = null
    if(props.error){
        error = <p className="text-danger">{props.text}</p>
    }
    return error
}

FieldError.propTypes = {
    text: PropTypes.string.isRequired
};