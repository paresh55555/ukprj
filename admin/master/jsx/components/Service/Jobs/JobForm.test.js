import React from 'react';
import JobForm from './JobForm';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{
        techList:[]
    }
};

const mockStore = configureStore();
let store;

describe("<JobForm/>",()=> {

    let component, jobForm;

    const props = {
        onChangeDate:jest.fn(),
        currentJob:{id:1,description:'test',job_number:1,order_number:1,tech_name:'test',start_date:'2018-08-24',start_time:10},
        onChangeText:jest.fn(),
        onChangeTech:jest.fn(),
        onChangeTime:jest.fn()

    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><JobForm {...props} /></Provider>);
        jobForm = component.find('JobForm');
    });

    it('length', () => {
        expect(jobForm).toHaveLength(1);
    });

    it('handleTimeChange',()=>{
        jobForm.node.handleTimeChange();
        expect(props.onChangeTime).toHaveBeenCalled();
    });

    it('rendered component',()=>{
        expect(jobForm.find('ServiceFormRow')).toHaveLength(4);
        expect(jobForm.find('Col')).toHaveLength(12);
        expect(jobForm.find('ServiceDatepicker')).toHaveLength(1);
        expect(jobForm.find('FormControl')).toHaveLength(4);
        expect(jobForm.find('ServiceTech')).toHaveLength(1);
    });

    it('when no id',()=>{
       delete props.currentJob.id;
        component = mount(<Provider
            store={store}><JobForm {...props} /></Provider>);
        expect(jobForm.find('FormControl').at(0).props().disabled).toEqual(true);
        expect(jobForm.find('FormControl').at(1).props().disabled).toEqual(true);
    });

    it('when no currentJob',()=>{
        delete props.currentJob;
        component = mount(<Provider
            store={store}><JobForm {...props} /></Provider>);
        expect(jobForm.find('FormControl').at(0).props().disabled).toEqual(false);
        expect(jobForm.find('FormControl').at(1).props().disabled).toEqual(false);
    });

});