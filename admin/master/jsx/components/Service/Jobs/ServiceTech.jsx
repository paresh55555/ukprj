import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import SQDropdown from "../../Common/Dropdown/SQDropdown";
import * as ApiServiceJob from "../../../api/ApiServiceJob";

class ServiceTech extends Component {
    constructor(props) {
        super(props);
        this.state = {
            serviceTech: ''
        }
    }
    componentWillMount(){
        ApiServiceJob.ApiGetServiceTechList()
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.selected){
            this.setState({serviceTech: nextProps.selected})
        }
    }
    onChange = (data) =>{
        this.setState({serviceTech: data.name})
        this.props.onChange(data)
    }
    render() {
        let {serviceJob} = this.props
        return (
            <SQDropdown data={serviceJob.techList} dataKey={'name'}
                        selectedVal={this.state.serviceTech}
                        defaultVal={'Select Service Tech'}
                        onChange={this.onChange}
                        fullWidth
                        id={'serviceTech'}/>
        );
    }
}

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTech)