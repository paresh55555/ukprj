import React from 'react'

export default class FilterByContent extends React.Component{

    render(){

        const {filterList} = this.props
        
        return(
            <div>
                <div className="filter_title">
                    <p>Filter by</p>
                </div>
                <div className='filter_Tabs'>
                    <div className='select_fileBy'>
                        {filterList && filterList.map((filter, index) => {
                            return (
                                <div className='selected_field' key={index}><input type='radio' name='filter'/><span className='filterName'>{filter.title}</span></div>
                            )
                        })}
                    </div> 
                    <div className='filter_btn'>
                        <a href="javascript:;" className='select_by' >Reset</a>
                        <a href="javascript:;" className='select_by active pull-right' >Apply</a>
                    </div>
                </div>
            </div>
        )
    }
}