import React from 'react';
import ServiceAddressMap from './ServiceAddressMap';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{}
};

const mockStore = configureStore();
let store;

describe("<ServiceAddressMap/>",()=> {

    let component, serviceAddressMap;

    const props = {
        className:'testClass',
        address:{address_1:'address1'}

    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><ServiceAddressMap {...props} /></Provider>);
        serviceAddressMap = component.find('ServiceAddressMap');
    });

    it('length', () => {
        expect(serviceAddressMap).toHaveLength(1);
    });

    it('render',()=>{
      expect(component.find('ServiceAddressMap').render().html()).toEqual(component.html());
    });

    it('componentWillReceiveProps',()=>{
        expect(serviceAddressMap.node.componentWillReceiveProps({address:{address_1:'address1'}})).toEqual(undefined);
        expect(serviceAddressMap.node.componentWillReceiveProps({address:{address_1:'address_1'}})).toEqual(undefined);
    });

    it('componentDidMount',()=>{
        expect(serviceAddressMap.node.componentDidMount()).toEqual(undefined);
    });

    it('rendered component',()=>{
        expect(serviceAddressMap.find('.testClass')).toHaveLength(1);
        expect(serviceAddressMap.find('GoogleMapReact')).toHaveLength(0);
        expect(serviceAddressMap.find('AnyReactComponent')).toHaveLength(0);
    });

});
