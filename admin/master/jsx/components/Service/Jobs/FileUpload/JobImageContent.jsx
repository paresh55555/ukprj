import React, { Component } from 'react';
import AddUploadFile from './AddUploadFile';

export default class JobImageContent extends Component {

    render() {
        
        const {onClick, open, onClose} = this.props
        return (
            <AddUploadFile
                ModalTittle='Attach Image'
                AddBtnTitle='Add Image'
                RowLabel='Upload Image'
                UploadingTitle='Uploading Image'
                types='image/png,image/jpg,image/jpeg,image/gif,application/pdf'
                dropzoneTxt='Try dropping an image file here or click "Select" to browse for an image file.'
                onClick={onClick}
                open={open}
                onClose={onClose}
            />
           
        )
    }
}