import React, {Component} from 'react';
import {ButtonToolbar, Form} from "react-bootstrap";
import ServiceButton from '../../ServiceButton';
import ServiceFormRow from '../../ServiceFormRow';
import SQModal from '../../../Common/SQModal/SQModal';
import { toggleUploaderModal } from '../../../../actions/serviceJob';
import FileSelector from '../../../Common/FileSelector';
import FileUploadInfo from '../../FileUploadInfo';
import { cancelRequest } from '../../../../api/ApiCalls';
import { NotifyAlert } from '../../../Common/notify';
import ShowError from '../../../Common/ShowError';


class AddUploadFile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            file:null,
            base64File:'',
            uploading:'',
            uploadingProgress:0,
            disableBtn: true,
            error: ''
        }
    }

    onClose = () => {
        cancelRequest()
        if(this.props.onClose){
            this.props.onClose();
        }
    }

    onClick = () => {
       
        const {file} = this.state;

        if (!file) {
            this.setState({
                error: 'Plese select a file first'
            })

            return
        }

        this.setState({ disableBtn: true })

        if(this.props.onClick){
            this.props.onClick(file, this.uploadProgress);
        }

    }

    componentWillReceiveProps(nextProps){
        if(nextProps.open && !this.props.open){
            this.setState({
                file:'',
                base64File:'',
                uploading:'',
                uploadingProgress:'',
                disableBtn: false
            })
        }

    }

    onFileSelect = (file) =>{
        this.setState({file, error: ''})
    }

    onFileLoaded = (fileObj) =>{
        this.setState({base64File: fileObj.base64})
    }

    cancelUpload = () =>{
        cancelRequest()
        this.setState({uploadingProgress: 0})
    }

    deleteFile = () =>{
        this.setState({file: null, base64File: null})
    }

    uploadProgress = (progressEvent) => {
        var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )
        this.setState({uploadingProgress:percentCompleted})
    }
  
    renderButtons(){

        return (
            <ServiceFormRow label={''}>
                <ButtonToolbar>
                    <ServiceButton onClick={this.onClick} bsStyle="primary"  label={this.props.AddBtnTitle} disabled={this.state.disableBtn}/>
                    <ServiceButton onClick={this.onClose} label={'Cancel'} disabled={this.state.disableBtn}/>
                </ButtonToolbar>
            </ServiceFormRow>
        )
    }

    renderFileUpload(){

        let {file} = this.state;
        let noCloud = 'noCloud'

        if(!file){
            return ''
        }
        return (
            <ServiceFormRow label={this.props.UploadingTitle}>
                <FileUploadInfo 
                    noCloud={noCloud}
                    file={file}
                    disabled={this.state.disableBtn}
                    uploading={this.state.uploadingProgress}
                    uploadingProgress={this.state.uploadingProgress}
                    onFileLoaded={this.onFileLoaded}
                    onCancel={this.cancelUpload}
                    onDelete={this.deleteFile}
                />
            </ServiceFormRow>
        )
    }
   
    render() {

        const {ModalTittle, open, RowLabel, types, dropzoneTxt} = this.props;

        const { error } = this.state

        const errorStyle = {
            paddingTop: 10
        }

        return (
            <SQModal title={ModalTittle}
                     isOpen={open}
                     disabled={this.state.disableBtn}
                     onClose={this.onClose}
                     className={'service-modal AddUpload'}
                     customButtons
            >
                <Form className={'service'}> 
                    <ServiceFormRow label={RowLabel} >
                        <FileSelector 
                            disabled={this.state.disableBtn}
                            onSelect={this.onFileSelect}
                            fileTypes={types}
                            dropzoneTxt={dropzoneTxt}
                        />
                        <p>
                            <ShowError style={errorStyle} error={error} />
                        </p>
                    </ServiceFormRow>
                    {this.renderFileUpload()}
                    {this.renderButtons()}
                </Form>
            </SQModal>
        );
    }
}

export default AddUploadFile;