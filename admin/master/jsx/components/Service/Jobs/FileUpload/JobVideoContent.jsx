import React, { Component } from 'react';
import AddUploadFile from './AddUploadFile';

export default class JobVideoContent extends Component {

    render() {
        
        const {onClick, open, onClose} = this.props
        return (
            <AddUploadFile
                ModalTittle='Attach Video'
                AddBtnTitle='Add Video'
                RowLabel='Upload Video'
                UploadingTitle='Uploading Video'
                types='video/mp4,video/x-m4v,video/*'
                dropzoneTxt='Try dropping a video file here or click "Select" to browse for a video file.'
                onClick={onClick}
                open={open}
                onClose={onClose}
            />
        )
    }
}