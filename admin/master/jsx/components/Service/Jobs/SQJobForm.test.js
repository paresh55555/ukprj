import React from 'react';
import SQJobForm from './SQJobForm';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState={
    serviceJob:{
        techList:[]
    }
};
const mockStore = configureStore();
let store;


describe('<SQJobForm/>',()=>{

    let component,props={
        currentJob:{},
        restProps:{}
    },sqJobForm;

    beforeEach(()=>{
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><SQJobForm {...props} /></Provider>);
        sqJobForm = component.find('SQJobForm');
    });

    it('length',()=>{
        expect(sqJobForm).toHaveLength(1);
    });

    it('render',()=>{
       expect(component.find('SQJobForm').render().html().replace("&#x2039;","‹").replace("&#x203A;","›")).toEqual(component.html());
    });

    it('rendered component',()=>{
       expect(sqJobForm.find('Form')).toHaveLength(1);
       expect(sqJobForm.find('JobForm')).toHaveLength(1);
    });

});