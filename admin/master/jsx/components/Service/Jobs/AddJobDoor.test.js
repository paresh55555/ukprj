import React from 'react';
import AddJobDoor from './AddJobDoor';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{
        attachedDoors:[{id:1,door_type:'dt1'},{id:2,door_type:'dt2'}],
        currentJob:{id:1},
        addJobDoorOpened:true
    }
};
const mockStore = configureStore();
let store;

describe("<AddJobDoor/>",()=> {

    let component, addJobDoor;

    const props = {
        orderId:1
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddJobDoor {...props} /></Provider>);
        addJobDoor = component.find('AddJobDoor');
    });

    it('length', () => {
        expect(addJobDoor).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('AddJobDoor').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       expect(addJobDoor.node.componentWillMount()).toEqual(undefined);
    });

    it('onClick',()=>{
        addJobDoor.node.onClick({id:1});
        expect(addJobDoor.node.state.selectedDoor).toEqual({id:1});
    });

    it('onClose',()=>{
        addJobDoor.node.onClose(()=>{
            expect(addJobDoor.node.props.serviceJob.addJobDoorOpened).toEqual(false);
        });
        expect(addJobDoor.node.state.selectedDoor).toEqual(initialState.serviceJob.attachedDoors[0]);
    });

    it('onSubmit',()=>{
        expect(addJobDoor.node.onSubmit()).toEqual(undefined);
        addJobDoor.node.setState({selectedDoor:{id:1}});
        addJobDoor.node.onSubmit()
        expect(addJobDoor.node.state.selectedDoor).toEqual(initialState.serviceJob.attachedDoors[0]);
    });

    it('renderSelectedDoor',()=>{
       expect(addJobDoor.node.renderSelectedDoor()).toEqual(null);
    });



});