import React from 'react';
import ServiceAddressList from './ServiceAddressList';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{
        job_address:[{id:1}],
        currentJob:{id:1},
    },
    service:{
        personFormOpened:true
    }
};
const mockStore = configureStore();
let store;

describe("<ServiceAddressList/>",()=> {

    let component, serviceAddresslist;

    const props = {
        orderId: 1
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><ServiceAddressList {...props} /></Provider>);
        serviceAddresslist = component.find('ServiceAddressList');
    });

    it('length', () => {
        expect(serviceAddresslist).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('ServiceAddressList').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       expect(serviceAddresslist.node.componentWillMount()).toEqual(undefined);
    });

    it('getAddress',()=>{
        expect(serviceAddresslist.node.getAddress()).toEqual(undefined);
    });

    it('editAddress',()=>{
        serviceAddresslist.node.editAddress({},()=>{
            expect(serviceAddresslist.node.props.service.personFormOpened).toEqual(true);
            expect(serviceAddresslist.node.props.service.personFormId).toEqual('address');
            expect(serviceAddresslist.node.props.service.currentPerson).toEqual({});
        });
    });

    it('deleteAddress',()=>{
       serviceAddresslist.node.deleteAddress({});
       expect(serviceAddresslist.node.state.isLoading).toEqual(true);
    });

    it('openAddAddress',()=>{
        serviceAddresslist.node.openAddAddress(()=>{
            expect(serviceAddresslist.node.props.service.personFormOpened).toEqual(true);
            expect(serviceAddresslist.node.props.service.personFormId).toEqual('address');
        });
    });

    it('addAddress',()=>{
        serviceAddresslist.node.addAddress({id:1},{},()=>{
            expect(serviceAddresslist.node.props.service.personFormOpened).toEqual(true);
            expect(serviceAddresslist.node.props.service.personFormId).toEqual(null);
        });
        serviceAddresslist.node.addAddress({id:0},{},()=>{
            expect(serviceAddresslist.node.props.service.personFormOpened).toEqual(true);
            expect(serviceAddresslist.node.props.service.personFormId).toEqual(null);
        });
    });

    it('rendered component',()=>{
        expect(serviceAddresslist.find('.purchasers-list')).toHaveLength(1);
        expect(serviceAddresslist.find('ServiceFormRow')).toHaveLength(1);
        expect(serviceAddresslist.find('PersonForm')).toHaveLength(1);
        expect(serviceAddresslist.find('ButtonToolbar')).toHaveLength(2);
        expect(serviceAddresslist.find('ServiceButton')).toHaveLength(3);
        expect(serviceAddresslist.find('ServiceAddress')).toHaveLength(initialState.serviceJob.job_address.length)
    });

});
