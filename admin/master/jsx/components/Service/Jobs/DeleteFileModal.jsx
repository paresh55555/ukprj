import React from 'react';

import ServiceButton from '../ServiceButton';
import SQModal from '../../Common/SQModal/SQModal';

import { ButtonToolbar } from "react-bootstrap";
import { partial } from '../../Common/utilFunctions';

export default class DeleteFileModal extends React.Component {

    state = {
        open: false
    }

    _toggleModal = open => this.setState({ open })

    closeModal = partial(this._toggleModal, false)
    openModal = partial(this._toggleModal, true)

    render () {

        const { confirm, onCancel, selectedFile, deleteMsg, disableConfirm, DeletePreview = () => null } = this.props
        const { url, file_name } = selectedFile || {}

        const { open } = this.state
        
        return (
            <SQModal
                title={'Delete'}
                isOpen={open}
                onClose={onCancel}
                className={'service-modal deleteJobFile'}
                customButtons
            >
                <div className='text-center'>
                    <div className='content_'>
                        <p>{deleteMsg}</p>
                        <DeletePreview url={url} />
                        <p>{file_name}</p>
                    </div>
                    <div className='removeButton '>
                        <ButtonToolbar>
                            <ServiceButton disabled={disableConfirm} onClick={confirm} bsStyle="primary" label={'Yes, Delete'} />
                            <ServiceButton onClick={onCancel} label={'No'} />
                        </ButtonToolbar>
                    </div>
                </div>
            </SQModal>
        );

    }

}