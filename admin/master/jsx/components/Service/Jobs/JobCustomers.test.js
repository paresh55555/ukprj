import React from 'react';
import JobCustomers from './JobCustomers';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{job_customers:[{id:1}],selectedDoorId:1,currentJob:{order_id:1}},
    service:{personFormOpened:true},
};

const mockStore = configureStore();
let store;

describe('<JobCustomers/>',()=>{

    let component, jobCustomers;

    const props = {
       customer:{},
        jobId:'TestJob',
    };
    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><JobCustomers {...props} /></Provider>);
        jobCustomers = component.find('JobCustomers');
    });

    it('length', () => {
        expect(jobCustomers).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('JobCustomers').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
        expect(jobCustomers.node.componentWillMount()).toEqual(undefined);
    });

    it('getCustomers',()=>{
        expect(jobCustomers.node.getCustomers()).toEqual(undefined);
    });

    it('openAddCustomer',()=>{
        jobCustomers.node.openAddCustomer(()=>{
           expect(jobCustomers.node.props.service.personFormOpened).toEqual(true);
           expect(jobCustomers.node.props.service.personFormId).toEqual('job-customer');
        });
    });

    it('addCustomer',()=>{
        jobCustomers.node.addCustomer({id:1},()=>{
            expect(jobCustomers.node.props.service.personFormOpened).toEqual(false);
            expect(jobCustomers.node.props.service.personFormId).toEqual(null);
        });
    });

    it('rendered component',()=>{
        expect(jobCustomers.find('.purchasers-list')).toHaveLength(1);
        expect(jobCustomers.find('ServiceFormRow')).toHaveLength(1);
        expect(jobCustomers.find('PersonForm')).toHaveLength(1);
        expect(jobCustomers.find('JobCustomerInfo')).toHaveLength(initialState.serviceJob.job_customers.length);
        expect(jobCustomers.find('ButtonToolbar')).toHaveLength(2);
        expect(jobCustomers.find('ServiceButton')).toHaveLength(3);
    });

    it('when job_customer',()=>{

        store = mockStore(Object.assign({},initialState,{serviceJob:{job_customers:[],selectedDoorId:1,currentJob:{order_id:1}}}));
        component = mount(<Provider
            store={store}><JobCustomers {...props} /></Provider>);
        jobCustomers = component.find('JobCustomers');

        expect(jobCustomers.find('ButtonToolbar')).toHaveLength(0);

        jobCustomers.node.addCustomer({id:1},()=>{
            expect(jobCustomers.node.props.service.personFormOpened).toEqual(false);
            expect(jobCustomers.node.props.service.personFormId).toEqual(null);
        });

    });

});