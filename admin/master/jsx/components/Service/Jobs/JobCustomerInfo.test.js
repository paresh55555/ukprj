import React from 'react';
import JobCustomerInfo from './JobCustomerInfo';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {

};
const mockStore = configureStore();
let store;
describe('<JobCustomerInfo/>',()=>{

    let component, jobCustomerInfo;

    const props = {
        customer:{customerInfo:{id:1,job_id:1,first_name:'Test',last_name:'SQ'}},
    };
    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><JobCustomerInfo {...props} /></Provider>);
        jobCustomerInfo = component.find('JobCustomerInfo');
    });

    it('length', () => {
        expect(jobCustomerInfo).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('JobCustomerInfo').render().html()).toEqual(component.html());
    });

    it('editCustomer',()=>{
       jobCustomerInfo.node.editCustomer(()=>{
           expect(jobCustomerInfo.node.props.service.personFormOpened).toEqual(false);
           expect(jobCustomerInfo.node.props.service.personFormId).toEqual(null);
       });
    });

    it('deleteCustomer',()=>{
       expect(jobCustomerInfo.node.deleteCustomer()).toEqual(undefined);
    });

    it('rendered component',()=>{
       expect(jobCustomerInfo.find('.purchaser-info')).toHaveLength(1);
       expect(jobCustomerInfo.find('Row')).toHaveLength(1);
       expect(jobCustomerInfo.find('Col')).toHaveLength(3);
       expect(jobCustomerInfo.find('PersonAvatar')).toHaveLength(1);
       expect(jobCustomerInfo.find('PersonInfo')).toHaveLength(1);
        expect(jobCustomerInfo.find('ButtonToolbar')).toHaveLength(1);
        expect(jobCustomerInfo.find('ServiceButton')).toHaveLength(2);
    });

    it('onClick Edit',()=>{
        const ServiceButton = jobCustomerInfo.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(0).simulate('click',()=>{
            expect(jobCustomerInfo.node.props.service.personFormOpened).toEqual(false);
            expect(jobCustomerInfo.node.props.service.personFormId).toEqual(null);
        })
    });

    it('onClick Delete',()=>{
        const ServiceButton = jobCustomerInfo.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(1).simulate('click')
    });

});