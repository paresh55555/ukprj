import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import PersonForm from "../Purchasers/PersonForm";
import * as ApiService from "../../../api/ApiService";
import {Button, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../../Service/ServiceButton";
import * as ApiServiceJob from "../../../api/ApiServiceJob";
import {togglePersonFormDialog, setCurrentPerson} from "../../../actions/service";
import {deleteJobAddress} from "../../../actions/serviceJob";
import ServiceFormRow from "../ServiceFormRow";
import ServiceAddress from "./ServiceAddress";

class ServiceAddressList extends Component {

    state = {
        isLoading: false
    }

    componentWillMount() {
        this.getAddress(this.props)
    }

    getAddress(props) {
        if (this.props.jobId && this.props.jobId !== 'new') {
            ApiServiceJob.ApiGetJobAddress(this.props.jobId)

        }
    }

    editAddress = (data) => {
        this.props.dispatch(togglePersonFormDialog(true, 'address'))
        this.props.dispatch(setCurrentPerson(data))
    }

    deleteAddress = (data) => {

        this.setState({
            isLoading: true
        })

        ApiServiceJob.ApiDeleteJobAddress(data.id, data.job_id)
            .then(
                response => {
                    this.setState({
                        isLoading: false
                    })
                    swal("Deleted!", "Address has been successfully deleted.", "success");
                    return response
                }
            )
            .catch(
                err => {
                    this.setState({
                        isLoading: false
                    })
                    swal("Deleted!", "Some error occured.", "error");
                }
            )
    }

    renderAddress() {
        let {job_address} = this.props.serviceJob

        return (
            job_address.map((address, id) => {
                return <div key={id}>
                    <ServiceAddress data={address} onClickEdit={this.editAddress} onClickDelete={this.deleteAddress}/>
                </div>

            })
        )
    }
    
    openAddAddress = () => {
      this.props.dispatch(togglePersonFormDialog(true, 'address'))
    }

  renderButton() {
      if (this.props.serviceJob.job_address.length) {
          return (
              <ButtonToolbar className={'buttons pull-right anotherAddress'}>
                  <ServiceButton className={'no-margin'} label={"Add another address"} onClick={this.openAddAddress}/>
              </ButtonToolbar>)
      }
      else {
          return <ServiceButton className={'no-margin full-width'} label={"Add address"}
                                onClick={this.openAddAddress}/>
      }
  }

  addAddress = (person, note) => {

      // let { customer_id = 0, address_id = 0 } = person;
      let customer_id = 0, address_id = 0;

      if (!customer_id) {
          customer_id = 0;
      }

      if (!address_id) {
          address_id = 0;
      }

      let {job_address} = this.props.serviceJob

      if (job_address.find(address => address.id === person.id)) {
          ApiServiceJob.ApiUpdateJobAddress({...person, customer_id, address_id, note}, this.props.jobId)
              .then(
                  response => swal("Updated!", "Address has been successfully updated.", "success")
              )
      }
      else {
          delete person.id
          ApiServiceJob.ApiAddJobAddress({...person, customer_id, address_id, note}, this.props.jobId)
      }

      this.props.dispatch(togglePersonFormDialog(false, null))
  }

  render() {

      let {job_address, currentJob} = this.props.serviceJob

      let orderId = currentJob && currentJob.order_id
      return (
          <div className={'purchasers-list'}>
              <ServiceFormRow label={'Service Address'}>
                  <PersonForm attachedOrderId={orderId} title={'Attach Service Address'} id={'address'} onSubmit={this.addAddress}
                              searchAPI={ApiServiceJob.ApiSearchJob} showNote />
                  {this.renderAddress(job_address)}
                  {this.renderButton()}
              </ServiceFormRow>
          </div>
      );
   }
}


const mapDispatchToProps = dispatch => ({
    dispatch
})

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceAddressList)