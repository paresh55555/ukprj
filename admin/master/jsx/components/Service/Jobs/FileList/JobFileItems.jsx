import React, {Component} from 'react';
import {Col} from "react-bootstrap";
export default class JobFileItems extends Component{

    state = {
        open:false,
        progress: 0,
        color:'',
        isOpen:false
    }

    timeInterval = null

    handleOpen = ()=>{
        this.setState({
            open:true
        })
    }

    handleClose = ()=>{
        this.setState({
            open:false
        })
    }

    cancelDelete = ()=>{
        this.setState({
            isOpen:false
        })
    }

    

    removeFile = () =>{
        const {file} = this.props;

        if(this.props.removeFile){
            this.props.removeFile(file)
        }
        this.confirmDelete()
    }
    
    
 
    downloadFile = () => {
        this.timeInterval = setInterval(
            () => {

                if(this.state.progress >= 100) {
                    clearInterval(this.timeInterval)
                } else {
                    this.setState({
                        progress: this.state.progress + 10,
                        color:'white'
                    })    
                }
            }, 500
         )
    }

    cancelDownload = () => {
        clearInterval(this.timeInterval)
        this.setState({
            progress: 0,
            color:'black'
        })
    }

    getBackgroundColor = () => {
        return {
            backgroundColor: '#ffffff'
        }
    }

    _onDelete = () => {
        this.props.onDelete(this.props.file)
    }

    _viewModal = () => {
        this.props.onView(this.props.file)
    }

    render(){
        
        const {fileName, url, file} = this.props;

        const validImg = /(\.jpg|\.jpeg|\.png)$/i

        const isImage = validImg.exec(fileName)

        let Icon = '';

        if(file.file_type == "image"){
            Icon = <img className="file_poster_img" src={url} />
        }
        else{
            Icon = <div>
            <div className='playPoster'>
                    <i style={iconStyle} className="fa fa-play-circle-o" aria-hidden="true"></i></div>
                    <img className="file_poster_img" src={file.poster_image} />
            </div>
            
        }

        let {color, open, isOpen, progress} = this.state

        const iconStyle = {
            fontSize: 20,
            cursor: 'pointer'
        }

        return (
            <div className={'service-file fileInfo posterFile'} style={this.getBackgroundColor()}>

            
                <div className={'fileinfoDiv'}>
                    <div className="jobFilePoster_img">
                        <Col sm={9} >
                            <a onClick={this._viewModal} className={color}><div className={'filename jobFile_img'}>{Icon}<span className={'Textcolor'}>{fileName}</span></div></a>
                        </Col>
                        <Col sm={3} className="text-right">
                            <span onClick={this._onDelete}>
                                <i style={iconStyle} className="fa fa-trash-o" aria-hidden="true"></i>
                            </span>
                        </Col>
                    </div>

                </div>
                <div id="progress" style={{width: `${progress}%`}} className={'fileinfDownload'}></div>
            </div>
        );
    }

   
}