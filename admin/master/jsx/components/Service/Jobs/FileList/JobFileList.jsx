import React, {Component} from 'react';
import JobFileItems from './JobFileItems';

class JobFileList extends Component {

    render () {

        const {
            Files,
            onDelete,
            onView
        } = this.props;
        
        return (
            <div>
                {Files && Files.map((file, i) => {
                    
                    return <JobFileItems
                        key={i}
                        file={file}
                        fileName={file.file_name}
                        url={file.url}
                        onDelete={onDelete}
                        onView={onView}
                    />
                })}
            </div>
        );
    }

}

export default JobFileList;