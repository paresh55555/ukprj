import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import PersonForm from "../Purchasers/PersonForm";
import {Button, Col, ButtonToolbar} from "react-bootstrap";
import JobCustomerInfo from "./JobCustomerInfo";
import ServiceButton from "../../Service/ServiceButton";
import * as ApiServiceJob from "../../../api/ApiServiceJob";
import {togglePersonFormDialog} from "../../../actions/service";
import ServiceFormRow from "../ServiceFormRow";

class JobCustomers extends Component {

    componentWillMount() {
        this.getCustomers(this.props)
    }

    getCustomers(props) {
        if (this.props.jobId && this.props.jobId !== 'new') {
            ApiServiceJob.ApiGetJobCustomers(this.props.jobId)
        }
    }

    renderCustomer() {
        let {job_customers} = this.props.serviceJob

        return (
            job_customers.map((customer, id) => {
                return <div key={id}>
                    <JobCustomerInfo customer={customer}/>
                </div>
            })
        )
    }

    openAddCustomer = () => {
        this.props.dispatch(togglePersonFormDialog(true, 'job-customer'))
    }

    renderButton() {
        if (this.props.serviceJob.job_customers.length) {
            return (
                <ButtonToolbar className={'buttons pull-right'}>
                    <ServiceButton className={'no-margin'} label={"Add another customer"}
                                   onClick={this.openAddCustomer}/>
                </ButtonToolbar>)
        }
        else {
            return <ServiceButton className={'no-margin full-width'} label={"Add customer"}
                                  onClick={this.openAddCustomer}/>
        }
    }

    addCustomer = (person) => {
        let {job_customers} = this.props.serviceJob
        if (job_customers.find(customer => customer.id === person.id)) {
            ApiServiceJob.ApiUpdateJobCustomer(person, this.props.jobId)
        }
        else {
            delete person.id
            ApiServiceJob.ApiAddJobCustomer(person, this.props.jobId)
        }

        this.props.dispatch(togglePersonFormDialog(false, null))
    }

    render() {

        let {currentJob} = this.props.serviceJob

        let orderId = currentJob && currentJob.order_id
        return (
            <div className={'purchasers-list'}>
                <ServiceFormRow label={'Attach Customer'}>
                    <PersonForm attachedOrderId={orderId} title={'Attach Customer'} id={'job-customer'}
                                onSubmit={this.addCustomer}/>
                    {this.renderCustomer()}
                    {this.renderButton()}
                </ServiceFormRow>
            </div>
        );
    }
}


const mapDispatchToProps = dispatch => ({
    dispatch
})

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob
})

export default connect(mapStateToProps, mapDispatchToProps)(JobCustomers)