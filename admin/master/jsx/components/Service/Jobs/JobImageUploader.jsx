import React from 'react';
import JobImageContent from './FileUpload/JobImageContent';
import { Image } from 'react-bootstrap';
import JobsFileManager from './JobsFileManager';

class JobImageUploader extends React.Component {

    render () {

        const { image } = this.props

        return (

            <JobsFileManager
                deleteMessage={'Image has been deleted.'}
                type={'Image'}
                files={image}
                Uploader={JobImageContent}
                rowLabel={'Attach Image(s)'}
                deleteMsg={'Are you sure you want to delete this image?'}
                deletePreview={({ url }) => <p><img className="deletePreview" src={url} /></p>}
                Viewer={({ url }) => {
                    return <div className="JobFileViewer">
                        <Image src={url} style={{
                            maxWidth:'100%'
                        }}/> 
                    </div>
                }}
            />

        )

    }

}

export default JobImageUploader