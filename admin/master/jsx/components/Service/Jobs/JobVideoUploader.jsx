import React from 'react';

import JobsFileManager from './JobsFileManager';
import JobVideoContent from './FileUpload/JobVideoContent';

class JobVideoUploader extends React.Component {

    render () {

        const { video } = this.props

        return (

            <JobsFileManager
                deleteMessage={'Video has been deleted.'}
                type={'Video'}
                files={video}
                Uploader={JobVideoContent}
                rowLabel={'Attach Video(s)'}
                deleteMsg={'Are you sure you want to delete this video?'}
                Viewer={({ url }) => {
                    return <div className="JobFileViewer">
                        <video src={url} style={{width:"100%",height:"100%"}} controls />
                    </div>
                }}
            />

        )

    }

}

export default JobVideoUploader