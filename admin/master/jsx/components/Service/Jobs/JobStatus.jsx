import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import SQDropdown from '../../Common/Dropdown/SQDropdown';
import { ApiGetJobStatus } from '../../../api/ApiServiceJob';
import SQPopover from './SQPopover';
import StatusList from './StatusList';

class JobStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showStatus: false,
            statusIndex: 0,
            filter:''
        }
    }

    componentWillReceiveProps (nextProps) {

        if (
            (nextProps.selected !== this.props.selected && nextProps.serviceJob.jobStatus.length)
            || nextProps.serviceJob.jobStatus.length !== this.props.serviceJob.jobStatus.length
        ) {

            const { filter = {}, filterIndex = 0 } = this._findParentFilter(nextProps.selected, nextProps.serviceJob.jobStatus)

            this.setState({
                filter,
                statusIndex: filterIndex
            })

        }

    }

    componentWillMount(){
        ApiGetJobStatus();
    }

    componentDidMount () {

        const { filter = {}, filterIndex = 0 } = this._findParentFilter(this.props.selected, this.props.serviceJob.jobStatus)

        this.setState({
            filter,
            statusIndex: filterIndex
        })
    }

    _findParentFilter = (status, jobStatus) => {

        if (status && jobStatus.length) {

            let flatArray = []

            jobStatus.forEach(a => flatArray = flatArray.concat(a.statusLists) )

            let foundStatus = flatArray.find(statusObject => statusObject.status == status)

            let index = jobStatus.findIndex(filter => filter.id == foundStatus.filter_id)

            if (index == -1) {
                index = 0
            }

            return {
                filter: jobStatus[index],
                filterIndex: index
            }
        }

        return {}
    }

    clickStatus = (statusIndex) => {
        this.setState({
            statusIndex
        })
    }

    _onStatusClick = status => {

        this.props.onChange(this.state.filter, status)

    }

    render() {
        
        let { jobStatus = [] } = this.props.serviceJob
        let { statusIndex, filter } = this.state

        let className = ''

        const selectedJobStatus = jobStatus[statusIndex] || {}

        let selectedStatus = 'Not Selected'

        if (this.props.selected) {

            selectedStatus = `${filter.filter || ''} > ${this.props.selected}`

        } 

        if (this.props.disabled) {

            className = 'disabled'

        }


        return (
            <SQPopover
                label={
                    <div className={`selectStatus ${className}`}>
                       <p>{selectedStatus}</p>
                    </div>
                }
                disabled={this.props.disabled}
            >
            <div className='jobStatusList'>
                <div className='jobFilterList'>
                    {
                        jobStatus && jobStatus.map((statusRes,index) => {
                            return (<div key={index} className={`filterList ${statusIndex == index ? 'active' : ''}`} onClick={() => this.clickStatus(index)}> <p>{statusRes.filter} <em className="fa fa-caret-right"></em></p></div>)
                        } )
                    }
                </div>
                <StatusList
                    header={selectedJobStatus}
                    list={selectedJobStatus.statusLists}
                    onStatusClick={this._onStatusClick}
                    selectedStatus={this.props.selected}
                />
            </div>
            
            
            </SQPopover>
        );
    }
}

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobStatus)