import React from 'react';
import ServiceAddress from './ServiceAddress';
import {shallow} from 'enzyme';

describe('<ServiceAddress/>',()=>{

    let component,props={
        data:{},
        onClickEdit:jest.fn(),
        onClickDelete:jest.fn()
    };

    beforeEach(()=>{
        component = shallow(<ServiceAddress {...props}/>);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('rendered component',()=>{
        expect(component.find('.ServiceAddress')).toHaveLength(1);
        expect(component.find('InfoComponent')).toHaveLength(1);
        expect(component.find('.addressMap')).toHaveLength(1);
    });

    it('when no render',()=>{
        component.instance().setState({rendered:false});

        const html = component.html();

        const expectedHtml = '<div class="ServiceAddress"><div></div></div>';

        expect(html).toEqual(expectedHtml);

    });

});