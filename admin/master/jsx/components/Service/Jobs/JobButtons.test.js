import React from 'react';
import JobButtons from './JobButtons';
import {shallow} from 'enzyme';

describe('<JobButtons/>',()=>{

    let component,props={
        onSave:jest.fn(),
        onCancel:jest.fn(),
        disabled:false
    };

    beforeEach(()=>{
       component = shallow(<JobButtons {...props}/>)
    });

    it('rendered component',()=>{
       expect(component.find('ServiceFormRow')).toHaveLength(1);
        expect(component.find('.no-pd')).toHaveLength(2);
        expect(component.find('.formbtns')).toHaveLength(1);
        expect(component.find('.buttons')).toHaveLength(1);
        expect(component.find('ServiceButton')).toHaveLength(2);
    });

    it('onClick save',()=>{
        const save = component.find('.savebtn');
        expect(save).toHaveLength(1);
        save.simulate('click');
        expect(props.onSave).toHaveBeenCalled();
    });

    it('onClick cancel',()=>{
        const cancel = component.find('.cancelbtn');
        expect(cancel).toHaveLength(1);
        cancel.simulate('click');
        expect(props.onCancel).toHaveBeenCalled();
    });

});