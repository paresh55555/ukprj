import React, {Component} from 'react';
import {connect} from "react-redux";
import { FormControl,Col,ButtonToolbar} from "react-bootstrap";
import ServiceFormRow from "../ServiceFormRow";
import ServiceTech from "./ServiceTech";
import ServiceDatepicker from "../ServiceDatepicker";
import ServiceButton from "../../Service/ServiceButton";
import {setServiceJobTime} from "../../../actions/serviceJob";
import moment from 'moment'
import TimePicker from 'react-bootstrap-time-picker';
import JobImageUploader from './JobImageUploader';
import JobVideoUploader from './JobVideoUploader';
import JobStatus from './JobStatus';
import { group } from '../../Common/utilFunctions';


class JobForm extends Component{

    handleTimeChange = (time) => {
        this.props.onChangeTime(time)
    }
    
    renderDatePicker(date){
        return (
           <Col className={'no-pd pull-left innerJob'} sm={6}><ServiceDatepicker onChange={this.props.onChangeDate} value={moment(date)} /></Col>
        )
    }

    renderInstallDate(date){
        return (
           <Col className={'no-pd pull-left innerJob'} sm={6}><ServiceDatepicker onChange={() => {}} value={moment(date)} /></Col>
        )
    }

    renderTimePicker(time) {
        return (
            <Col className={'no-pd pull-right innerJob'} sm={6}>
                <TimePicker format={12} value={time} onChange={this.handleTimeChange} />
            </Col>
        )
    }
    render(){

        let {currentJob, onChangeText, onChangeTech, onChangeStatus} = this.props


         const {
            description,
            job_number,
            order_number,
            tech_name,
            start_date,
            start_time,
            job_status,
            filter,
            id
        } = currentJob || {};

        let isNew = false;

        if (!id) {
            isNew = true;
        }

        const {currentJobFiles} = this.props.serviceJob;

        let { image = [], video= [] } = group(currentJobFiles,'file_type')

        return(
            <div>
                    <ServiceFormRow label={'Job ID & Order ID'}>
                        <Col sm={6} className={'no-pd pull-left innerJob'}><FormControl onChange={onChangeText} name={'job_number'} value={job_number} disabled={!isNew}/></Col>
                        <Col sm={6} className={'no-pd pull-right innerJob'}> <FormControl onChange={onChangeText} name={'order_number'} value={order_number} disabled={!isNew}/>  </Col>  
                    </ServiceFormRow>
                    <ServiceFormRow label={'Status'}>   
                            <JobStatus
                                selected={job_status}
                                filter={filter}
                                onChange={onChangeStatus}
                            />
                    </ServiceFormRow> 
                     <ServiceFormRow label={'Start Date/Time'}>
                       {this.renderDatePicker(start_date)}
                       {this.renderTimePicker(start_time)}  
                    </ServiceFormRow>
                    <ServiceFormRow label={'Description'}>                            
                        <FormControl name={'description'} onChange={onChangeText} value={description} componentClass="textarea" style={{height: 120}} placeholder={'Description'}/> 
                    </ServiceFormRow>
                    <ServiceFormRow label={'Service Tech'}>   
                            <ServiceTech
                                selected={tech_name}
                                onChange={onChangeTech}
                            />
                    </ServiceFormRow>                     

                    <JobImageUploader image={image}/>
                    <JobVideoUploader video={video}/>
                    <ServiceFormRow label={'Installed Date/By'}>
                       {this.renderInstallDate(start_date)}
                       <Col sm={6} className={'no-pd pull-right innerJob'}> <FormControl name={'order_number'}  />  </Col> 
                    </ServiceFormRow>
            </div>
            )
    }
}

const mapDispatchToProps = dispatch => ({
    dispatch
 })
 
 const mapStateToProps = (state) => ({
   serviceJob: state.serviceJob
 })
 
 export default connect(mapStateToProps, mapDispatchToProps)(JobForm)

