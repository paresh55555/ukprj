import React from 'react';
import JobDoors from './JobDoors';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{
        currentJob:{id:1},
        attachedDoors:[{id:1,door_type:'dt1'},{id:2,door_type:'dt2'}],
    },
    service:{
        loadingDoors:false
    }
};
const mockStore = configureStore();
let store;

describe("<JobDoors/>",()=> {

    let component, jobDoors;

    const props = {
        jobId: 1
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><JobDoors {...props} /></Provider>);
        jobDoors = component.find('JobDoors');
    });

    it('length', () => {
        expect(jobDoors).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('JobDoors').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       expect(jobDoors.node.componentWillMount()).toEqual(undefined);
    });

    it('componentWillUnmount',()=>{
        jobDoors.node.componentWillUnmount(()=>{
           expect(jobDoors.node.props.service.doors).toEqual([])
        });
    });

    it('openAddDoor',()=>{
        jobDoors.node.openAddDoor(()=>{
           expect(jobDoors.node.props.serviceJob.addJobDoorOpened).toEqual(true);
        });
    });

    it('rendered component',()=>{
        expect(jobDoors.find('ServiceFormRow')).toHaveLength(1);
        expect(jobDoors.find('AddJobDoor')).toHaveLength(1);
    });

    it('when no currentJob and no jobId',()=>{
       delete props.jobId;
        store = mockStore(Object.assign({},initialState,{serviceJob:{}}));
        component = mount(<Provider
            store={store}><JobDoors {...props} /></Provider>);
        jobDoors = component.find('JobDoors');

        expect(jobDoors.node.componentWillMount()).toEqual(undefined);
        expect(component.find('JobDoors').render().html()).toEqual(null);
    });

});