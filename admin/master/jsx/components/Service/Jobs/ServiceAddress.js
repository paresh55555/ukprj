import React, {Component, PropTypes} from 'react';
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import InfoComponent from "../../Common/InfoComponent";
import ServiceAddressMap from "./ServiceAddressMap";

class ServiceAddress extends Component {

    state = {
        rendered: true
    }

    renderAddress(){
        let {data, onClickEdit, onClickDelete} = this.props

        if(this.state.rendered){
            return(
                <div>
                <InfoComponent data={data} onClickEdit={onClickEdit} onClickDelete={onClickDelete}/>
                <ServiceAddressMap address={this.props.data} className='addressMap'  />
                </div>
            )
        }
        else{
            return(
                <div></div>
            )
        }
    }

    render() {
        return (
            <div className='ServiceAddress'>
                {this.renderAddress()}
            </div>
        );
    }
}

export default ServiceAddress;