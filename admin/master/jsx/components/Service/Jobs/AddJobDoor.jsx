import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {toggleAddJobDoor} from "../../../actions/serviceJob";
import SQModal from "../../Common/SQModal/SQModal";
import * as ApiServiceJob from '../../../api/ApiServiceJob'
import DoorTraits from "../Doors/DoorTraits";
import ServiceFormRow from "../ServiceFormRow";
import DoorWarrantyStatus from "../Doors/DoorWarrantyStatus";
import {ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../ServiceButton";

class AddJobDoor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedDoor: null
        }
    }
    componentWillMount() {
        ApiServiceJob.ApiGetAttachedDoors(this.props.orderId).then((response)=>{
            if(response.data.data && response.data.data.length){
                this.setState({selectedDoor:response.data.data[0]})
            }
        })
    }
    onClick = (door) =>{
        this.setState({selectedDoor: door})
    }
    onClose = () => {
        let {attachedDoors} = this.props.serviceJob
        if(attachedDoors.length){
            this.setState({selectedDoor: attachedDoors[0]})
        }
        this.props.dispatch(toggleAddJobDoor(false))
    }
    onSubmit = () =>{
        if(!this.state.selectedDoor)return;
        let {serviceJob} = this.props
        let data = {
            order_id: this.props.orderId,
            job_id: serviceJob.currentJob.id,
            door_id: this.state.selectedDoor.id
        }
        ApiServiceJob.ApiAddJobDoor(data)
        this.onClose()
    }

    renderDoors() {
        let {attachedDoors} = this.props.serviceJob,
            selected = (doorId)=>{
                if(!this.state.selectedDoor)return '';
                return this.state.selectedDoor.id === doorId ? 'selected' : ''
            }
        return (
            attachedDoors.map((door, i)=>{
                return <div key={i} className={'attached-item '+ selected(door.id)} onClick={() => this.onClick(door)}>
                    <span>{door.door_type}</span>
                </div>
            })
        )
    }
    renderSelectedDoor(){
        let door = this.state.selectedDoor
        if(!door){
            return null
        }
        return (
            <div>
                <ServiceFormRow label={'Door Detail'}>
                    <div className={'door-info selected-door-info'}>
                        <div className={'door-type'}>{door.door_type} <DoorWarrantyStatus date={door.warranty_end_date}/></div>
                        <div className={'door-warranty'}><span>Warranty Ends:</span> {door.warranty_end_date}</div>
                        <DoorTraits traits={door.traits}/>
                    </div>
                </ServiceFormRow>
            </div>
        )
    }
    renderButtons = () =>{
        return (
            <ServiceFormRow>
                <ButtonToolbar>
                    <ServiceButton bsStyle="primary" onClick={this.onSubmit} label={'Add Door'} />
                    <ServiceButton onClick={this.onClose} label={'Cancel'} />
                </ButtonToolbar>
            </ServiceFormRow>
        )
    }

    render() {
        let {serviceJob} = this.props
        return (
            <SQModal title={'Add Door'}
                     isOpen={serviceJob.addJobDoorOpened}
                     onClose={this.onClose}
                     className={'service-modal'}
                     customButtons
            >
                <div className={'attached-addresses service clearfix'}>
                    <div className={'clearfix'}>
                        <div className={'header'}>Door(s) attached to Order ID: <span>{this.props.orderId}</span></div>
                        {this.renderDoors()}
                    </div>
                    <div className={'form-divider'}></div>
                    {this.renderSelectedDoor()}
                    {this.renderButtons()}
                </div>
            </SQModal>
        );
    }
}

AddJobDoor.propTypes = {};

const mapStateToProps = (state) => ({
    service: state.service,
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddJobDoor)