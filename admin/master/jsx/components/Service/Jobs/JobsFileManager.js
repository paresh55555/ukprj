import React, {Component} from 'react';
import {connect} from "react-redux";
import ServiceButton from '../ServiceButton';
import ServiceFormRow from '../ServiceFormRow';
import JobFileList from './FileList/JobFileList';
import { ApiAddJobFiles, ApiDeleteJobFile } from '../../../api/ApiServiceJob';
import DeleteFileModal from './DeleteFileModal';
import { cancelRequest } from '../../../api/ApiCalls';
import SQModal from '../../Common/SQModal/SQModal';

class JobsFileManager extends Component {
   
    state = {
        open : false,
        selectedFile: null,
        disableConfirm: false
    }
   
    openAddModal = () =>{
       this.setState({
           open:true
       })
    }

    closeModal = () => {
        this.setState({
            open:false
        })
    }

    uploadFile = (file, uploadProgress) => {
        
        const { currentJob } = this.props.serviceJob

        ApiAddJobFiles(currentJob.id, file, uploadProgress).then(res => {
            this.closeModal()
        })
    }

    removeFile = () => {

        const { deleteMessage } = this.props;

        const { selectedFile } = this.state

        const { currentJob } = this.props.serviceJob

        this.setState({
            disableConfirm: true
        })

        if ( selectedFile ) {
            ApiDeleteJobFile(currentJob.id, selectedFile.id).then(res => {
                swal("Deleted!", deleteMessage, "success");
                this.deleteModal.closeModal()

                this.setState({
                    disableConfirm: false
                })
            })
        }

    }  

    renderModalButton (files) {

        const { type } = this.props

        if (files && files.length) {

            return <ServiceButton className={'pull-right another'} onClick={this.openAddModal} label={`Add Another ${type}`} /> 
        
        } else {
            
            return <ServiceButton className={'full-width'} onClick={this.openAddModal} label={`Attach ${type}`} />
        
        }
    }

    _openDeleteModal = selectedFile => {

        this.setState({
            selectedFile
        })

        this.deleteModal.openModal()

    }

    _cancelDeleteModal = () => {

        cancelRequest()
        this.deleteModal.closeModal()

        this.setState({
            disableConfirm: false
        })

    }

    _openViewModal = selectedFile => {

        this.setState ({
            selectedFile
        })

        this.viewModal.open()
    }

    render() {

        const { files, Viewer, Uploader, rowLabel, deleteMsg, deletePreview } = this.props;
        const { selectedFile = {}, disableConfirm } = this.state

        const {
            url,
            file_name
        } = selectedFile || {};

        return (
            <ServiceFormRow label={rowLabel}>
                <JobFileList
                    Files={files}
                    onView={this._openViewModal}
                    onDelete={this._openDeleteModal}
                />
                {this.renderModalButton(files)}
                <Uploader 
                    onClick={this.uploadFile}
                    open={this.state.open}
                    onClose={this.closeModal}
                />
                <DeleteFileModal
                    ref={c => this.deleteModal = c}
                    selectedFile={selectedFile}
                    confirm={this.removeFile}
                    onCancel={this._cancelDeleteModal}
                    deleteMsg={deleteMsg}
                    disableConfirm={disableConfirm}
                    DeletePreview={deletePreview}
                />
                <SQModal
                    title={file_name}
                    className={'service-modal fileupload'}
                    ref={c => this.viewModal = c}
                >
                    <Viewer
                        url={url}
                    />
                </SQModal>
            </ServiceFormRow>
        );
    }
}

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobsFileManager)