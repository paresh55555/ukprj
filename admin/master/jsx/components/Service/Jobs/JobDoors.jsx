import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import ServiceFormRow from "../ServiceFormRow";
import * as ApiServiceJob from "../../../api/ApiServiceJob";
import AddDoorNote from "../Doors/AddDoorNote";
import {setDoors, setCurrentDoor, toggleAddDoor} from "../../../actions/service";
import {toggleAddJobDoor} from "../../../actions/serviceJob";
import AddJobDoor from "./AddJobDoor";
import DoorsListContainer from '../Doors/DoorsListContainer';
import AddDoor from '../Doors/AddDoor';
import AddDoorButton from '../Doors/AddDoorButton';

class JobDoors extends Component {
    componentWillMount(){
        let {jobId} = this.props
        if(jobId){
            ApiServiceJob.ApiGetJobDoors(jobId)
        }
    }
    componentWillUnmount(){
        this.props.dispatch(setDoors([]))
    }

    addDoor = door => {
        const { currentJob } = this.props.serviceJob
        return ApiServiceJob.ApiAddJobDoor(currentJob.id, door)
    }

    onDelete = door => {
        let {currentJob} = this.props.serviceJob
        ApiServiceJob.ApiDeleteJobDoor(currentJob.id, door.id)
    }

    render() {
        const currentJob = this.props.serviceJob.currentJob
        if(!currentJob) return null
        return (
            <ServiceFormRow label={'Doors'}>

                <DoorsListContainer
                    showStatus={this.props.showStatus}
                    hideCreateJobButton={this.props.hideCreateJobButton}
                    onDelete={this.onDelete}
                />

                <AddDoor
                    onAdd={this.addDoor}
                />

                {/* <AddJobDoor orderId={currentJob.order_id} /> */}
                {/* <AddDoorButton /> */}

                <AddDoorNote orderId={currentJob.order_id}/>
            </ServiceFormRow>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service,
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobDoors)