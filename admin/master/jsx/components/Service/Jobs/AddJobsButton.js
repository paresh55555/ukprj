import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {} from "../../../actions/serviceJob";


class AddJobsButton extends Component {
    render() {
        return (
            <a onClick={this.props.onClick} className="btn btn-labeled btn-info pull-right">Create Jobs Details</a>
        );
    }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddJobsButton)