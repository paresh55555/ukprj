import React from 'react';
import moment from 'moment';
import ServiceButton from '../../ServiceButton';
import MaterialIcon from "material-icons-react";

export default class EmailMassageBox extends React.Component{

    renderDate = (datetime) => {
        if(datetime == null||''){
           return moment(new Date()).format('DD MMM YY')
        }else{
            return moment(datetime).format('DD MMM YY')
        }
    }

    render(){

        const { from_name, datetime, subject, message, email } = this.props.mail || {};

        return(
            <div className='email_massanger'>
                <div className='openEmailHeader'>
                    <div className='openEmail_title'>{subject}</div>
                    <div className='openEmail_userInfo'>
                        <div className='openEmail_titleName'>
                            <div className='openEmail_name'>{from_name}</div>
                            <div className='openEmail_elapse'>{this.renderDate(datetime)}</div>
                        </div>
                        <div className='openEmail_mail'><span className='emailFrom'>{'From:'}</span>{email}</div>
                    </div>
                </div>
                <div className='dividerLine'></div>
                <div className='openEmailDiv' >
                    <div className='email_textArea-2'>{message}</div>
                    <div className='email_textArea-3'>
                        <div className='emailTxt-1'>{''}</div>
                        <div className='emailTxt-2'>{from_name}</div>
                    </div>
                </div>
                <div className='dividerLine'></div>
                <div className='OpenEmailFooter' >
                    <ServiceButton
                        className="btn btn-default emailReplyBtn"
                        label={
                            <div className='jobEmailReply'>
                                <MaterialIcon icon="reply" />
                                <span>Reply</span>
                            </div>
                        } 
                        onClick={() => {}} 
                    />
                    <ServiceButton
                        className="btn btn-default emailReplyBtn"
                        label={
                            <div className='jobEmailReply forword'>
                                <MaterialIcon icon="reply" />
                                <span>Forword</span>
                            </div>
                        } 
                        onClick={() => {}} 
                    />
                </div>
            </div>
        )
    }
}