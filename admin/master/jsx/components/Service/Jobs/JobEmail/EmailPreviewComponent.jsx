import React, { PureComponent } from 'react';
import moment from 'moment';

export default class EmailPreviewComponent extends PureComponent  {

    activeCss = () => {
        
        if(this.props.active == true){
            return  'active'
        }
        else{
            return ''
        }
    }

    renderMassage = (message) => {
        if(message.length >= 150){
            return (`${message.substring(0, 150)}...`)
        }
        else{
            return (message)
        }
    }

    renderDate = (datetime) => {
        if(datetime == null||''){
           return moment(new Date()).format('DD MMM YY')
        }else{
            return moment(datetime).format('DD MMM YY')
        }
    }

    render () {

        const { from_name, datetime, subject, message } = this.props.email;

        return(
            <div className={`preview_component ${this.activeCss()}`} onClick={this.props.onClick}>
                <div className='email_componentHeader'>
                    <div className='name'>{from_name}</div>
                    <div className='day_time'>{this.renderDate(datetime)}</div>
                </div>
                <div className='component_Title '>
                    {'Re: '}{subject}
                </div>
                <div className='component_Text'>
                    <p className='email_description'>
                       {this.renderMassage(message)}
                    </p>
                </div>
            </div>
        )
    }

}