import React from 'react';
import {Col,ButtonToolbar, FormControl, Form} from "react-bootstrap";
import ServiceButton from '../../ServiceButton';
import Dropzone from 'react-dropzone'
import ShowError from '../../../Common/ShowError';
import MaterialIcon from "material-icons-react";

export default class SendNewMailDiv extends React.Component{

    state = {
        data: {
            file: {},
            to: '',
            subject: '',
            message: ''
        },
        error:''
    }

    updateForm = e => {

        const { value, name } = e.target

        this.setState(
            prevState => ({
                data: {
                    ...prevState.data,
                    [name]: value
                }
            })
        )

    }

    onSubmit = () => {

        const { message, to, subject } = this.state.data

        if (!to) {
            this.setState({
                error: 'Plese specify to first'
            })

            return
        }


        if (!subject) {
            this.setState({
                error: 'Plese write a subject first'
            })

            return
        }

        if (!message) {
            this.setState({
                error: 'Plese write a message first'
            })

            return
        }


        this.props.onEmail(this.state.data).then(response => {
            this.props.close()
            swal("Email Send!", " Email has been send.", "success");
        })
    }

    onDrop = (files) => {
        this.setState(
            prevState => ({
                data: {
                    ...prevState.data,
                    file: files[0]
                }
            })
        );
    }

    openDropzone = () => this.dropzone.open()

    render () {
        
        const { data } = this.state

        const errorStyle = {
            paddingTop: 10
        }

        return(
          
            <div className='newMessage_container'>
                <div className='newMessage_header'>
                    <div className='newMessage_title pull-left'>New Message</div> 
                    <div className='newMessage_close pull-right' onClick={this.props.close} >
                    <MaterialIcon icon="close" />
                    </div>
                </div>
                <div className='newMessage_Content_Div'>
                    <Col sm={12} className={'newMessage_input'}>
                        <FormControl
                            onChange={this.updateForm}
                            name="to"
                            placeholder='To'
                            value={data.to}
                        />
                    </Col>
                    <Col sm={12} className={'newMessage_input'}>
                        <FormControl
                            onChange={this.updateForm}
                            name="subject"
                            placeholder='Subject'
                            value={data.subject}
                        />
                    </Col>
                    <Col sm={12} className={'newMessage_input'} style={{'height': 'calc(100% - 250px)'}}>
                        <textarea
                            onChange={this.updateForm}
                            name="message"
                            value={data.message}
                        ></textarea>
                    </Col>
                    <Col className='tabWithError' sm={12}>
                        <div className='newMessage_footer pull-left'>
                        <p>
                            <ShowError style={errorStyle} error={this.state.error} />
                        </p>
                            <div className='newMessage_bold_text'>{data.file.name}</div>
                            <div className="newMessageButtons">
                                <ServiceButton className={'pull-left'} label={'Send'} onClick={this.onSubmit}/>
                                <em className="fa fa-font FontSelector" aria-hidden="true"></em>
                                <em onClick={this.openDropzone} className="fa fa-paperclip attach" aria-hidden="true"></em>

                                <Dropzone
                                    onDrop={this.onDrop}
                                    ref={c => this.dropzone = c}
                                    style={{
                                        display: 'none'
                                    }}    
                                />
                            </div>
                        </div>
                    </Col>
                </div>
            </div>
        )
    }
}