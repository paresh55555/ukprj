import React, {Component} from 'react';
import { FormControl,Col,ButtonToolbar} from "react-bootstrap";
import ServiceFormRow from "../ServiceFormRow";
import ServiceTech from "./ServiceTech";
import ServiceDatepicker from "../ServiceDatepicker";
import ServiceButton from "../../Service/ServiceButton";
import moment from 'moment'
import TimePicker from 'react-bootstrap-time-picker';

class JobButton extends Component{

    renderJobButtons(){
        let {jobId} = this.props

        return(
            <Col sm ={12} className={'no-pd'}>
               <div className="formbtns">
                    <ButtonToolbar className={'buttons'}>
                        <ServiceButton disabled={this.props.disabled} onClick={this.props.onSave} className="btn btn-primary savebtn" label={'Save' }/>
                        <ServiceButton disabled={this.props.disabled} onClick={this.props.onCancel} className="btn btn-default cancelbtn" label={'Cancel'} />
                    </ButtonToolbar>
                </div>
            </Col>
        )
    }
    render(){

        return(
            <ServiceFormRow>
                <Col sm={12} className={'no-pd'}>       
                   {this.renderJobButtons()}
                </Col>
            </ServiceFormRow>
        )
    }
}


export default JobButton;

