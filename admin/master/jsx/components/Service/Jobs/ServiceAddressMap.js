import React, {Component} from 'react';
import {connect} from "react-redux";
import GoogleMapReact from 'google-map-react';
import { Image } from 'react-bootstrap';
import { getLongitude } from '../../Common/utilFunctions';

const AnyReactComponent = ({ text }) => <div>{text}</div>

class ServiceAddressMap extends Component {

  state = {
    center: {
      lat: 0,
      lng: 0
    },
    rendered: false
  }

  componentWillReceiveProps (nextProps) {

    const prevAddress = this.props.address;
    const { address } = nextProps

    if( address.address_1 !== prevAddress.address_1) {

      this.setState({
        rendered: false
      })

      getLongitude(address)
          .then(
              res => {
                  this.setState({
                      center: {
                          lat: res.lat(),
                          lng: res.lng()
                      },
                      rendered: true
                  })
              }
          )
    }

  }

    componentDidMount () {
      getLongitude(this.props.address)
          .then(
              res => {
                  this.setState({
                      center: {
                          lat: res.lat(),
                          lng: res.lng()
                      },
                      rendered: true
                  })
              }
          )
    }

   render() {  

    let {className} = this.props
    
       return (
          <div className={className}>
            <div style={{ height: '100%', width: '100%' }}>
                {this.state.rendered ?  <GoogleMapReact
                bootstrapURLKeys={{ key: "AIzaSyDsPFecdClIX8WS9z0I-CTWaqxJdz_r6Ss" }}
                defaultCenter={this.state.center}
                defaultZoom={11}
              >
                <AnyReactComponent
                  lat={this.state.center.lat}
                  lng={this.state.center.lng}
                  text={<Image src="/SQ-admin/img/map-marker.png"/>}
                />
              </GoogleMapReact> : <div></div> }
            </div>
          </div>
       );
   }
}


const mapDispatchToProps = dispatch => ({
   dispatch
})

const mapStateToProps = (state) => ({
  serviceJob: state.serviceJob
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceAddressMap)