import React, { Component } from 'react';
import JobForm from './JobForm';
import { Form } from 'react-bootstrap';

class SQJobForm extends Component {

    render() {
        
        const {currentJob = {}, ...restProps} = this.props
         
        return (
                <Form className={'add-order'}>
                   <JobForm 
                        {...restProps}
                        currentJob={currentJob}
                   />
                </Form>
           
        )
    }
}

export default SQJobForm;