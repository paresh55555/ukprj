import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../../Service/ServiceButton";
import PersonAvatar from "../Purchasers/PersonAvatar";
import PersonInfo from "../Purchasers/PersonInfo";
import * as ApiServiceJob from "../../../api/ApiServiceJob";
import {setCurrentPerson, togglePersonFormDialog} from "../../../actions/service";

class JobCustomerInfo extends Component {
 
  editCustomer = () => {
    this.props.dispatch(togglePersonFormDialog(true,'job-customer'))
    this.props.dispatch(setCurrentPerson(this.props.customer))
  }
  deleteCustomer = () =>{
    ApiServiceJob.ApiDeleteJobCustomer(this.props.customer.id,this.props.customer.job_id)
  }
  renderButtons(){
      return (
          <ButtonToolbar className={'buttons pull-right'}>
              <ServiceButton label={'Edit'} className="Doorbtn" onClick={this.editCustomer}/>
              <ServiceButton  label={'Delete'} className="Doorbtn"onClick={this.deleteCustomer}/>
          </ButtonToolbar>
      )
  }
  render() {
      let customerInfo = this.props.customer
      return (
          <div className={'purchaser-info JobCustomer'} >
              <Row>
             
                  <Col md={1} >
                      <PersonAvatar info={customerInfo}/>
                  </Col>
                  <Col md={7}>
                      <PersonInfo info={customerInfo}/>
                  </Col>
                  <Col md={4} >
                      {this.renderButtons()}
                  </Col>
              </Row>
          </div>
      );
  }
}

JobCustomerInfo.propTypes = {
};

const mapStateToProps = (state) => ({
  service: state.service
})

const mapDispatchToProps = dispatch => ({
  dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobCustomerInfo)