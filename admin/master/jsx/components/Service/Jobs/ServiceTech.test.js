import React from 'react';
import ServiceTech from './ServiceTech';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceJob:{}
};
const mockStore = configureStore();
let store;

describe('<ServiceTech/>',()=> {

    let component, serviceTech;

    const props = {
        onChange:jest.fn()
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><ServiceTech {...props} /></Provider>);
        serviceTech = component.find('ServiceTech');
    });

    it('length', () => {
        expect(serviceTech).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('ServiceTech').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       expect(serviceTech.node.componentWillMount()).toEqual(undefined);
    });

    it('componentWillReceiveProps',()=>{
        serviceTech.node.componentWillReceiveProps({selected:true});
        expect(serviceTech.node.state.serviceTech).toEqual(true);
        expect(serviceTech.node.componentWillReceiveProps({selected:false})).toEqual(undefined);
    });

    it('onChange',()=>{
        serviceTech.node.onChange({name:'test'});
        expect(serviceTech.node.state.serviceTech).toEqual('test');
        expect(props.onChange).toHaveBeenCalled();
    });

    it('rendered component',()=>{
       expect(serviceTech.find('SQDropdown')).toHaveLength(1);
    });

});
