import React from 'react';

export default class StatusList extends React.Component{

    activeLink = (status) => {

        const { selectedStatus } = this.props;

        if(selectedStatus == status){
            return 'active'
        }
        else{
            return ''
        }

    }

    render(){

        const { onStatusClick } = this.props;

        return(
            <div className="optionStatusList">
                <div className="headerFilter filterList">
                    <p>{this.props.header.filter}</p>
                </div>
                {this.props.list && this.props.list.map(
                        (status,index) => {
                            return  <div key={index} onClick={() => onStatusClick(status.status)} className={`filterList ${this.activeLink(status.status)}`}>
                                <p>{status.status}</p>
                            </div>
                        }
                    )
                }
            </div>
        )
    }
}