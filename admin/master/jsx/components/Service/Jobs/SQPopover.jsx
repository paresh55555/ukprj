import React from 'react';
import OutsideClickHandler from 'react-outside-click-handler';

import { partial } from '../../../components/Common/utilFunctions'

export default class SQPopover extends React.Component{

    state = {
        open: false
    }

    _togglePopup = open => this.setState({ open })

    _openPopup = partial(this._togglePopup, true)

    _closePopup = partial(this._togglePopup, false)

    render () {

        let popoverContent = ''
        let onClick = () => this._togglePopup(!this.state.open)

        if (this.state.open) {
            popoverContent = (
                <div className="sq-popover-content" >
                    {this.props.children}
                </div>
            )
        }
        
        if (this.props.disabled) {

            onClick = () => {}

        }

        return (

            <OutsideClickHandler
                    onOutsideClick={this._closePopup}
                >
                <div
                    onClick={onClick}
                >
                    {this.props.label}
                </div>
                {popoverContent}
            </OutsideClickHandler>

        )

    }
}