import React, {Component, PropTypes} from 'react';
import {Col, ProgressBar} from "react-bootstrap";
import MaterialIcon from "material-icons-react";

const reader = new FileReader();

export default class FileUploadInfo extends Component {
    constructor(props) {
        super(props);
    }
    componentWillMount(){
        if(this.props.file){
            this.loadFile(this.props.file)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.file && this.props.file !== nextProps.file){
            this.loadFile(nextProps.file)
        }
    }
    loadFile = (file) =>{
        reader.onload = (event) => {
            let base64 = event.target.result;
            this.props.onFileLoaded({file,base64})
        };
        reader.readAsDataURL(file);
    }
    getIcon(){

        const iconStyle = {
            fontSize: 20,
            cursor: 'pointer'
        }
        
        let icon = <span onClick={this.props.onDelete}>
            <i style={iconStyle} className="fa fa-trash-o" aria-hidden="true"></i>
        </span>

        if(this.props.uploading){
            icon = <span onClick={this.props.onCancel}><MaterialIcon icon="clear" /></span>
        }
        return icon
    }

    getCloudIcon(){
        
        if(!this.props.noCloud){
            <MaterialIcon icon="cloud_download" />
        }
    }

    getCssClass(){
        return (this.props.uploading ? ' uploading' : '')
    }
    getFileName(){
        if(this.props.file){
            return this.props.file.name
        }
        if(this.props.fileName){
            return this.props.fileName
        }
    }
    getProgress(){
        if(!this.props.uploading){
            return '';
        }
        return <ProgressBar className={'service-progress-bar'} now={this.props.uploadingProgress} />
    }
    render() {
        return (
            <div className={'service-file' + this.getCssClass()}>
                <Col sm={10}>
                    <div className={'filename'}>{this.getFileName()}</div>
                    {this.getProgress()}
                </Col>
                <Col sm={2} className="text-right"><span className="downloadicon">{this.getCloudIcon()}</span>{this.getIcon()}</Col>
            </div>
        );
    }
}

FileUploadInfo.propTypes = {
    onDelete: PropTypes.func.isRequired,
};