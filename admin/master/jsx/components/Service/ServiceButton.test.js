import React from 'react';
import ServiceButton from './ServiceButton';
import {shallow} from 'enzyme';

describe('<ServiceButton/>',()=>{

    let component,props={
        onClick:jest.fn(),
        bsStyle:'bsStyle',
        className:'testClass',
        disabled:false,
        label:'test'
    };

    beforeEach(()=>{
       component = shallow(<ServiceButton {...props}/>)
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('expected Html',()=>{
        expect(component.html()).toEqual(`<button type="button" class="testClass btn btn-bsStyle">${props.label}</button>`);
    });

    it('onClick button',()=>{
        const button = component.find('Button');
        expect(button).toHaveLength(1);
        button.simulate('click');
        expect(props.onClick).toHaveBeenCalled();
    });

});