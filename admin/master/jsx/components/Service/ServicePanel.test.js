import React from 'react';
import ServicePanel from './ServicePanel';
import {shallow} from 'enzyme';

describe('<ServicePanel/>',()=>{

    let component,props={
        header:'test',
        children:'testing'
    };

    beforeEach(()=>{
        component=shallow(<ServicePanel {...props}/>);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('rendered component',()=>{
        expect(component.find('.service')).toHaveLength(1);
    });

});