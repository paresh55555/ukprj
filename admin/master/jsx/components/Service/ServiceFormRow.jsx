import React, {PropTypes} from 'react';
import {Col, ControlLabel, FormGroup, Row} from "react-bootstrap";

export default function ServiceFormRow(props) {
    return (
        <Row className={props.className || ''}>
            <FormGroup validationState={props.error ? "error" : null}>
                <Col componentClass={ControlLabel} sm={2}>{props.label}</Col>
                <Col sm={10}>
                    {props.children}
                </Col>
            </FormGroup>
        </Row>
    );
}

ServiceFormRow.propTypes = {

};