import React from 'react';
import ServiceDatepicker from './ServiceDatepicker';
import {shallow} from 'enzyme';
import moment from 'moment';

describe('<ServiceDatepicker/>', () => {

    let component, props = {
        onChange: jest.fn(),
        placeholder:'test',
        disabled:false,
    };

    beforeEach(()=>{
        component=shallow(<ServiceDatepicker {...props}/>)
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('onChange',()=>{
        const datetime = component.find('.service-datepicker');
        expect(datetime).toHaveLength(1);
        datetime.simulate('change');
        expect(props.onChange).toHaveBeenCalled();
    });

    it('rendered component',()=>{
        expect(component.find('.service-datepicker')).toHaveLength(1);
    });

    it('with value',()=>{
       props.value = '2018-08-31';
       component=shallow(<ServiceDatepicker {...props}/>);
        expect(component).toHaveLength(1);
    });

});
