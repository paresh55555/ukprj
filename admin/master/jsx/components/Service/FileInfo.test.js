import React from 'react';
import {shallow} from 'enzyme';
import FileInfo from './FileInfo';

describe('<FileInfo/>',()=>{
     let component, props={
          url:'',
         fileName:'test file'
     };

     beforeEach(()=>{
          component=shallow(<FileInfo {...props} />)
     });

    it('length',()=>{
         expect(component).toHaveLength(1);
    });

    it('handleOpen',()=>{
        component.instance().handleOpen()
        expect(component.instance().state.open).toEqual(true);

    });

    it('handleClose',()=>{
        component.instance().handleClose()
        expect(component.instance().state.open).toEqual(false);

    });

    it('cancelDownload',()=>{
        component.instance().handleClose()
        expect(component.instance().state.progress).toEqual(0);
    });

    it('getBackgroundColor',()=>{
        expect(component.instance().getBackgroundColor()).toEqual({backgroundColor:'#ffffff'});
        component.instance().setState({progress:50});
        expect(component.instance().getBackgroundColor()).toEqual({backgroundColor:'#f5f7fa'});
    });


});