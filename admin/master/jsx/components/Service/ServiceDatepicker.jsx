import React, {PropTypes} from 'react';
import Datetime from "react-datetime";
import {DATE_FORMAT} from "../Common/constants";

export default function ServiceDatepicker(props) {
    let yesterday = Datetime.moment().subtract( 1, 'day' );
    let valid = function( current ){
        return current.isAfter( yesterday );
    };
    let value = props.value ? Datetime.moment(props.value, DATE_FORMAT) : ''

    return (
        <Datetime value={value}
                  onChange={(date) => props.onChange(date)}
                  inputProps={{
                      placeholder: props.placeholder,
                      className: 'custom-select form-control',
                      disabled:!!props.disabled,
                      onKeyDown: (e)=>{
                          if ([46, 8, 9, 27, 13, 110, 111].indexOf(e.keyCode) !== -1 ||
                              // Allow: Ctrl+A
                              (e.keyCode == 65 && e.ctrlKey === true) ||
                              // Allow: home, end, left, right
                              (e.keyCode >= 35 && e.keyCode <= 39)) {
                              // let it happen, don't do anything
                              return;
                          }
                          // Ensure that it is a number and stop the keypress
                          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                              e.preventDefault();
                          }
                      }
                  }}
                  dateFormat={DATE_FORMAT}
                  closeOnSelect
                  timeFormat={false}
                  isValidDate={ valid }
                  className={'service-datepicker'}/>
    );
}

ServiceDatepicker.propTypes = {
    onChange: PropTypes.func.isRequired
};