import React from 'react';
import ServiceFormRow from './ServiceFormRow';
import {shallow} from 'enzyme';

describe('<ServiceFormRow/>',()=>{
    let component,props={
        label:'testLabel',
        children:'testChildren',
        className:'testClass'
    };

    beforeEach(()=>{
        component=shallow(<ServiceFormRow {...props}/>);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('rendered component',()=>{
        expect(component.find('.testClass')).toHaveLength(1);
        expect(component.find('FormGroup')).toHaveLength(1);
        expect(component.find('Col')).toHaveLength(2);
    });

});
