import React, {Component,PropTypes} from 'react';
import {Col, ProgressBar, Image} from "react-bootstrap";
import MaterialIcon from "material-icons-react";
import SQModal from "./../Common/SQModal/SQModal";


export default class FileInfo extends Component{

    state = {
        open:false,
        progress: 0,
        color:'',
    }

    timeInterval = null

    handleOpen = ()=>{
        this.setState({
            open:true
        })
    }

    handleClose = ()=>{
        this.setState({
            open:false
        })
    }

    downloadFile = () => {
        this.timeInterval = setInterval(
            () => {

                if(this.state.progress >= 100) {
                    clearInterval(this.timeInterval)
                } else {
                    this.setState({
                        progress: this.state.progress + 10,
                        color:'white'
                    })    
                }
            }, 500
         )
    }

    cancelDownload = () => {
        clearInterval(this.timeInterval)
        this.setState({
            progress: 0,
            color:'black'
        })
    }

    getBackgroundColor = () => {
        return {
            backgroundColor: this.state.progress > 0 ? '#f5f7fa' : '#ffffff'
        }
    }

    renderButtons = () => {
        let icon = this.state.progress ? <span onClick={this.cancelDownload}><MaterialIcon icon="close" /></span> : <span onClick={this.props.onDelete || null}><MaterialIcon icon="delete" /></span>
        let url = `${window.location.origin}/${this.props.url}`
        
        return (
            <div>
                <span className="downloadicon" >
                    {
                        this.state.progress ?
                            "Downloading..."
                        : <a href={url} download>
                            <MaterialIcon icon="cloud_download" />
                        </a>
                    }
                </span>
                {icon}
            </div>
        )
    }

    DocModalCss = (isImage) => {

        if(isImage){
            return 'service-modal fileupload'
        }
        else{
            return 'service-modal fileupload DocsPreview'
        }
    }

    PreviewContent = (isImage, url) => {
        
        if(isImage){
            return <Image src={url} style={{maxWidth:'100%'}}/> 
        }
        else{
            return <iframe src={`https://docs.google.com/viewer?url=${url}&embedded=true`} style={{width:"100%",height:"100%"}}></iframe>
        }
    }

    render(){
        let url = `${window.location.origin}/${this.props.url}`
        let ImgUrl = this.props.url

        const validImg = /(\.jpg|\.jpeg|\.png)$/i

        const isImage = validImg.exec(this.props.fileName)

        return (
            <div className={'service-file fileInfo'} style={this.getBackgroundColor()}>

            
                <div className={'fileinfoDiv'}>
                    <div>
                        <Col sm={9} >
                            <a onClick={this.handleOpen} className={this.state.color}><div className={'filename'}><MaterialIcon icon="photo_size_select_actual" /><span className={'Textcolor'}>{this.props.fileName}</span></div></a>
                        </Col>
                        <Col sm={3} className="text-right">
                            {this.renderButtons()}
                        </Col>
                    </div>

                </div>
                <div id="progress" style={{width: `${this.state.progress}%`}} className={'fileinfDownload'}></div>
        
                    <SQModal title={this.props.fileName}
                        isOpen={this.state.open}
                        onClose={this.handleClose}
                        className={this.DocModalCss(isImage)}
                        customButtons
                    >
                       {this.PreviewContent(isImage, url)}
                    </SQModal>
            </div>
        );
    }

   
}

FileInfo.propTypes = {
    fileName: PropTypes.string.isRequired,
    onDelete: PropTypes.func.isRequired
};