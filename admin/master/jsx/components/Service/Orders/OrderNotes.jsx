import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Button, FormGroup} from "react-bootstrap";
import ServiceFormRow from "../ServiceFormRow";
import OrderNote from "./OrderNote";
import ServiceButton from "../ServiceButton";
import {addOrderNote, updateOrderNote} from "../../../actions/service";

class OrderNotes extends Component {
    componentWillMount(){
        let {currentOrder} = this.props.service
        if(currentOrder == null){
            return
        }
        if(typeof currentOrder.notes == 'undefined'){
            return
        }
        if(currentOrder.notes.length > 0){
            return
        }
        this.addNote();
    }
    onChange = (event, i) =>{
        this.props.changeNote()
        this.props.dispatch(updateOrderNote(event.target.value, i))
    }
    addNote = () =>{
        this.props.dispatch(addOrderNote(''))
    }
    renderNotesList(){
        let {currentOrder} = this.props.service
        if(currentOrder === null){
            return
        }
        if(typeof currentOrder.notes == 'undefined'){
            return
        }
        if(currentOrder.notes.length <= 0){
            return
        }
        return (
            currentOrder.notes.map((note, i)=>{
                return <OrderNote key={i} onChange={(event)=> this.onChange(event, i)} note={note}/>
            })
        )
    }
    render() {
        return (
            <div>
                {this.renderNotesList()}
                <FormGroup>
                    <ServiceButton onClick={this.addNote} className={'pull-right'} label={'Add another note'}/>
                </FormGroup>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderNotes)