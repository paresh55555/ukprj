import React from 'react';
import OrderNotes from './OrderNotes';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{currentOrder:{order_number:1},orderPurchasers:[]}
};

const mockStore = configureStore();
let store;

describe('<UpdateOrder/>',()=> {
    let component, orderNotes;

    const props = {
        changeNote:jest.fn()
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><OrderNotes {...props} /></Provider>);
        orderNotes = component.find('OrderNotes');
    });

    it('length', () => {
        expect(orderNotes).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('OrderNotes').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
        expect(orderNotes.node.componentWillMount()).toEqual(undefined);
    });

    it('onChange',()=>{
        orderNotes.node.onChange({target:{value:'test'}},1,()=>{
            expect(orderNotes.node.props.service.currentOrder.note[1].note).toEqual('test');
        });
        expect(props.changeNote).toHaveBeenCalled();
    });

    it('addNote',()=>{
        orderNotes.node.addNote(()=>{
           expect(orderNotes.node.props.service.currentOrder.note).toEqual([{id:0,note:''}]);
        });
    });

    it('renderNotesList',()=>{
      expect(orderNotes.node.renderNotesList()).toEqual(undefined);
    });

    it('rendered component',()=>{
       expect(orderNotes.find('FormGroup')).toHaveLength(1);
       expect(orderNotes.find('ServiceButton')).toHaveLength(1);
    });

    it('when 1 or more notes',()=>{

        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,currentOrder:{order_number:1,notes:[{id:1,note:'t'}]}}}));
        component = mount(<Provider
            store={store}><OrderNotes {...props} /></Provider>);
        orderNotes = component.find('OrderNotes');

        expect(orderNotes.node.componentWillMount()).toEqual(undefined);

    });

    it('when no currentOrder',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,currentOrder:null}}));
        component = mount(<Provider
            store={store}><OrderNotes {...props} /></Provider>);
        orderNotes = component.find('OrderNotes');

        expect(orderNotes.node.componentWillMount()).toEqual(undefined);
    });

    it('when no notes',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,currentOrder:{order_number:1,notes:[]}}}));
        component = mount(<Provider
            store={store}><OrderNotes {...props} /></Provider>);
        orderNotes = component.find('OrderNotes');

        expect(orderNotes.node.componentWillMount()).toEqual(undefined);
    })

});