import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {
    setCurrentOrder,
    toggleAddOrder,
    gotoOrderActivity,
} from "../../../actions/service";
import AddDoor from "../Doors/AddDoor";
import * as ApiService from "../../../api/ApiService";
import Purchasers from "../Purchasers/Purchasers";
import Docs from "../Docs/Docs";
import ServicePanel from "../ServicePanel";
import OrderForm from "./OrderForm";
import Doors from "../Doors/Doors";
import ServiceButton from '../ServiceButton';
import ServiceFormRow from '../ServiceFormRow';
import {Col,ButtonToolbar, FormControl, Form} from "react-bootstrap";
import OrderActivityLog from './OrderActivityLog';

class UpdateOrder extends Component {
    componentWillMount(){
        ApiService.getSites()
    }
    componentWillUnmount(){
        this.props.dispatch(setCurrentOrder(null));
        this.props.dispatch(toggleAddOrder(false));
        ApiService.ApiGetOrders();
    }

    componentDidMount(){
        this.props.dispatch(gotoOrderActivity(false))
    }

    gotoOrderActivityLog = () => {
        this.props.dispatch(gotoOrderActivity(true))
    }

    gotoOrderDetail = () => {
        this.props.dispatch(gotoOrderActivity(false))
    }

    onLoadOrderActivity = () => {
        const {currentOrder} = this.props.service
        ApiService.ApiGetOrderActivityLog(currentOrder.id)
    }

    render() {
        let {service} = this.props
        let {showOrderActivity, currentOrder, currentOrderActivity} = service
        let createRequest = 'createServiceRequest';

            if(!showOrderActivity ){
                return( <ServicePanel 
                            header={
                                <div>
                                    <div className="pull-left">{'Update order: '+service.currentOrder.order_number}</div>
                                    <div className='pull-right'><ServiceButton  className="btn btn-default cancelbtn" label={'Order Activity'} onClick={this.gotoOrderActivityLog} /></div>
                                </div>
                            }
                        >
                        <OrderForm />
                        <Purchasers />
                        <Docs />
                        <Doors createRequest={createRequest}/>
                    </ServicePanel>
                )
            }else{
                return(<OrderActivityLog 
                            gotoOrderDetail={this.gotoOrderDetail}
                            currentOrder={currentOrder}
                            currentOrderActivity={currentOrderActivity}
                            onLoad = {this.onLoadOrderActivity}
                        />)
            }
    }
}

UpdateOrder.propTypes = {};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(UpdateOrder)