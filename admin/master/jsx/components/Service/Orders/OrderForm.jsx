import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import moment from "moment/moment";
import {ButtonToolbar, Col, Form, FormControl} from "react-bootstrap";
import SQDropdown from "../../Common/Dropdown/SQDropdown";
import * as ApiService from "../../../api/ApiService";
import {toggleAddOrder} from "../../../actions/service";
import OrderNotes from "./OrderNotes";
import ServiceFormRow from "../ServiceFormRow";
import ServiceDatepicker from "../ServiceDatepicker";
import ServiceButton from "../ServiceButton";
import {DATE_FORMAT} from "../../Common/constants";

class OrderForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentOrder:{
                order_number: '',
                complete_date: null,
                warranty_end_date: null,
                warranty_length: 0,
                site: '',
            },
            hasChanges: false
        }
    }
    componentWillMount(){
        const {currentOrder} = this.props.service
        if(currentOrder && currentOrder.id){
            this.setState({currentOrder})
        }
    }
    getWarrantyEnd(complete_date, length){
        if(complete_date){
            return moment(complete_date, DATE_FORMAT).add(length,'years').format(DATE_FORMAT)
        }
    }
    saveOrder = () => {
        const {currentOrder} = this.state
        let data = {
            id: currentOrder.id,
            order_number: currentOrder.order_number,
            complete_date: currentOrder.complete_date,
            warranty_end_date: currentOrder.warranty_end_date,
            warranty_length: currentOrder.warranty_length,
            site: currentOrder.site,
            notes: currentOrder.notes
        }
        ApiService.ApiUpdateOrder(currentOrder.id, data).then(()=>{
            this.setState({hasChanges:false})
        })
    }
    cancel = () =>{
        this.props.dispatch(toggleAddOrder(false));
    }
    onChange = (value, type) =>{
        if(type === 'warranty_length'){
            value = value.value
            this.state.currentOrder.warranty_end_date = this.getWarrantyEnd(this.state.currentOrder.complete_date, value)
        }
        if(type === 'complete_date' && this.state.currentOrder.warranty_length){
            this.state.currentOrder.warranty_end_date = this.getWarrantyEnd(value,this.state.currentOrder.warranty_length)
        }
        if(type === 'complete_date' || type === 'warranty_end_date'){
            if(value instanceof moment){
                value = value.format(DATE_FORMAT)
            }
        }
        this.state.currentOrder[type] = value;
        this.setState({currentOrder: this.state.currentOrder, hasChanges:true})
    }
    onChangeNote = () => {
        this.setState({hasChanges:true})
    }
    renderCompleteDate(){
        return (
            <Col className={'no-pd'} sm={4}>
                <ServiceDatepicker value={this.state.currentOrder.complete_date}
                                   onChange={(date) => this.onChange(date, 'complete_date')}
                                   placeholder={'Completed Date'}/>
            </Col>
        )
    }
    renderWarrantyLength(){
        const WLNEGTHS = [
            {title: '5 years', value: 5},
            {title: '10 years', value: 10},
        ]
        let length = WLNEGTHS.find((l)=>{return l.value === +this.state.currentOrder.warranty_length})
        return (
            <Col sm={4}>
                <SQDropdown data={WLNEGTHS} selectedVal={length ? length.title : ''} dataKey={'title'} defaultVal={'Warranty Length'}
                            onChange={(length) => this.onChange(length,'warranty_length')} id={'warranty-length'} fullWidth />
            </Col>
        )
    }
    renderWarrantyEnd(){
        return (
            <Col className={'no-pd'} sm={4}>
                <ServiceDatepicker value={this.state.currentOrder.warranty_end_date}
                                   onChange={(date) => this.onChange(date, 'warranty_end_date')}
                                   placeholder={'Warranty End Date'}/>
            </Col>
        )
    }
    renderSite(){
        let {currentOrder} = this.state
        let {service} = this.props
        return (
            <ServiceFormRow label={'Site'}>
                <SQDropdown data={service.sites} selectedVal={currentOrder.site ? currentOrder.site : ''} defaultVal={'Site'}
                            onChange={(site) => this.onChange(site,'site')} id={'order-site'} fullWidth />
            </ServiceFormRow>
        )
    }
    renderButtons(){
        if(!this.state.hasChanges)return '';
        return (
            <ServiceFormRow label={''}>
                <ButtonToolbar className={'wide'}>
                    <ServiceButton bsStyle="primary" onClick={this.saveOrder} label={'Update Order'} />
                    <ServiceButton onClick={this.cancel} label={'Cancel'} />
                </ButtonToolbar>
            </ServiceFormRow>
        )
    }
    render() {
        const {currentOrder} = this.state
        const {service} = this.props
        return (
            <Form className={'add-order'}>
                <ServiceFormRow label={'Order ID'}>
                    <FormControl value={currentOrder.order_number} disabled/>
                </ServiceFormRow>
                <ServiceFormRow label={'Select Date'}>
                    {this.renderCompleteDate()}
                    {this.renderWarrantyLength()}
                    {this.renderWarrantyEnd()}
                </ServiceFormRow>
                <ServiceFormRow label={'Notes'}>
                    <OrderNotes changeNote={this.onChangeNote} />
                </ServiceFormRow>
                {this.renderSite()}
                {this.renderButtons()}
            </Form>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(OrderForm)