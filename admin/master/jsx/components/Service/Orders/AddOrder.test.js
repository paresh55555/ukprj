import React from 'react';
import AddOrder from './AddOrder';
import {mount} from 'enzyme';
import {Provider} from 'react-redux'
import configureStore from 'redux-mock-store'

const initialState={

};

const mockStore = configureStore();
let store;

describe('<AddOrder/>',()=>{

    let component, addOrder;

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddOrder  /></Provider>);
        addOrder = component.find('AddOrder');
    });
    it('length', () => {
        expect(addOrder).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('AddOrder').render().html()).toEqual(component.html());
    });

    it('componentWillUnmount',()=>{
        expect(addOrder.node.componentWillUnmount()).toEqual(undefined);
    });

    it('createOrder',()=>{
        expect(addOrder.node.createOrder()).toEqual(undefined);
        expect(addOrder.node.state.error).toEqual('Order number is required');

        addOrder.node.setState({order_number:1});

        expect(addOrder.node.createOrder()).toEqual(undefined);
        expect(addOrder.node.state.error).toEqual('');

    });

    it('cancel',()=>{
        addOrder.node.cancel(()=>{
            expect(addOrder.node.props.service.addOrderOpened).toEqual(false);
        })
    });

    it('onChange',()=>{
        addOrder.node.onChange(2);
        expect(addOrder.node.state.order_number).toEqual(2);
    });

    it('validate',()=>{
        expect(addOrder.node.validate()).toEqual(false);
        expect(addOrder.node.state.error).toEqual('Order number is required');

        addOrder.node.setState({order_number:1});
        expect(addOrder.node.validate()).toEqual(true);
        expect(addOrder.node.state.error).toEqual('');
    });

    it('onClick createOrder',()=>{
        const Buttons=addOrder.find('Button');
        expect(Buttons).toHaveLength(2);
        Buttons.at(0).simulate('click');
        expect(addOrder.node.state.error).toEqual('Order number is required');
        addOrder.node.setState({order_number:1});
        Buttons.at(0).simulate('click');
        expect(addOrder.node.state.error).toEqual('');
    });

    it('onClick Cancel',()=>{
        const Buttons=addOrder.find('Button');
        expect(Buttons).toHaveLength(2);
        Buttons.at(1).simulate('click',()=>{
            expect(addOrder.node.props.service.addOrderOpened).toEqual(false);
        });
    });


    it('rendered Component',()=>{
      expect(addOrder.find('ServicePanel')).toHaveLength(1);
      expect(addOrder.find('Form')).toHaveLength(1);
      expect(addOrder.find('FormGroup')).toHaveLength(1);
      expect(addOrder.find('Col')).toHaveLength(3);
      expect(addOrder.find('FormControl')).toHaveLength(1);
      expect(addOrder.find('HelpBlock')).toHaveLength(1);
      expect(addOrder.find('HelpBlock')).toHaveLength(1);

      expect(addOrder.find('ButtonToolbar')).toHaveLength(1);

     expect(addOrder.find('Button')).toHaveLength(2);
    });



});