import React from 'react';
import OrderForm from './OrderForm';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux';
import {DATE_FORMAT} from "../../Common/constants";
import moment from "moment/moment";

const initialState = {
    service:{currentOrder:{site:'',id:1}}
};
const mockStore = configureStore();
let store;

describe('<OrderForm/>',()=> {

    let component, orderForm;

    const props = {
        onChange: jest.fn()
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><OrderForm {...props} /></Provider>);
        orderForm = component.find('OrderForm');
    });

    it('length', () => {
        expect(orderForm).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('OrderForm').render().html().replace(new RegExp("&#x2039;",'g') ,"‹").replace(new RegExp("&#x203A;",'g'),"›").replace('disabled','disabled=\"\"')).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       orderForm.node.componentWillMount();
       expect(orderForm.node.state.currentOrder).toEqual({site:'',id:1});
    });

    it('getWarrantyEnd',()=>{
        expect(orderForm.node.getWarrantyEnd("2018-08-27",1)).toEqual(moment("2018-08-27", DATE_FORMAT).add(1,'years').format(DATE_FORMAT));
        expect(orderForm.node.getWarrantyEnd("2018-08-27",1)).toEqual(moment("2018-08-27", DATE_FORMAT).add(1,'years').format(DATE_FORMAT));
    });

    it('saveOrder',()=>{
       expect(orderForm.node.saveOrder()).toEqual(undefined);
    });

    it('cancel',()=>{
       orderForm.node.cancel(()=>{
            expect(orderForm.node.props.service.addOrderOpened).toEqual(false)
       });
    });

    it('onChange',()=>{
        orderForm.node.setState({currentOrder:{complete_date:'2018-08-27'}});
        orderForm.node.onChange({value:1},'warranty_length');
        expect(orderForm.node.state.currentOrder.warranty_length).toEqual(1);
        expect(orderForm.node.state.currentOrder.warranty_end_date).toEqual(moment("2018-08-27", DATE_FORMAT).add(1,'years').format(DATE_FORMAT));
        expect(orderForm.node.state.hasChanges).toEqual(true);
        orderForm.node.onChange("2018-08-27",1);
        expect(orderForm.node.state.currentOrder.complete_date).toEqual("2018-08-27");
        expect(orderForm.node.state.currentOrder.warranty_end_date).toEqual(moment("2018-08-27", DATE_FORMAT).add(1,'years').format(DATE_FORMAT));
        expect(orderForm.node.state.hasChanges).toEqual(true);
    });

    it('onChangeNote',()=>{
        orderForm.node.setState({hasChanges:false});
        orderForm.node.onChangeNote();
        expect(orderForm.node.state.hasChanges).toEqual(true);
    });

    it('renderButtons',()=>{
        orderForm.node.setState({hasChanges:true});
       orderForm.node.renderButtons()
    });

    it('rendered component',()=>{
       expect(orderForm.find('.add-order')).toHaveLength(1);
       expect(orderForm.find('ServiceFormRow')).toHaveLength(4);
       expect(orderForm.find('FormControl')).toHaveLength(1);
        expect(orderForm.find('Col')).toHaveLength(11);
        expect(orderForm.find('ServiceDatepicker')).toHaveLength(2);
        expect(orderForm.find('SQDropdown')).toHaveLength(2);
        expect(orderForm.find('OrderNotes')).toHaveLength(1);
    });


});