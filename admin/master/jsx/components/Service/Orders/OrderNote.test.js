import React from 'react';
import OrderNote from './OrderNote';
import {shallow} from 'enzyme';

describe('<OrderNote/>',()=>{

    let component,props={
        onChange:jest.fn(),
        note:{note:''}
    };

    beforeEach(()=>{
        component = shallow(<OrderNote {...props}/>)
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('onChange note',()=>{
       const note = component.find('.note');
       expect(note).toHaveLength(1);
       note.simulate('change');
       expect(props.onChange).toHaveBeenCalled();
    });

    it('rendered component',()=>{
        expect(component.find('textarea')).toHaveLength(1);
        expect(component.find('.note')).toHaveLength(1);
    });

});