import React from 'react';
import AddOrderButton from './AddOrderButton';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState={

}
const mockStore = configureStore();
let store;

describe('<AddOrderButton/>',()=>{
    let component, addOrderButton;

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddOrderButton  /></Provider>);
        addOrderButton = component.find('AddOrderButton');
    });

    it('length', () => {
        expect(addOrderButton).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('AddOrderButton').render().html()).toEqual(component.html());
    });

     it('newOrder',()=>{
       addOrderButton.node.newOrder(()=>{
           expect(addOrderButton.node.props.service.addOrderOpened).toEqual(true);
       })
     });

     it('onClick a tag',()=>{
         const a = addOrderButton.find('a');
         expect(a).toHaveLength(1);
         a.simulate('click',()=>{
             expect(addOrderButton.node.props.service.addOrderOpened).toEqual(true);
         });
     });

     it('rendered component',()=>{
         expect(addOrderButton.find('a')).toHaveLength(1);
     })

});