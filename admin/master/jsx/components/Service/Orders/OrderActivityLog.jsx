import React from 'react';
import ServicePanel from '../ServicePanel';
import {Col ,ButtonToolbar, FormControl, Form} from "react-bootstrap";
import ServiceButton from '../ServiceButton';
import MaterialIcon from "material-icons-react";
import ServiceFormRow from '../ServiceFormRow';
import ActivityList from '../../Common/ActivityLog/ActivityList';

export default class OrderActivityLog extends React.Component {


    componentWillMount(){
        this.props.onLoad()
    }

    render () {
        const {currentOrder, currentOrderActivity} = this.props;
        return(
            <ServicePanel>
                <div>
                    <div className="JobDetail_Header activity">
                        <ButtonToolbar className='job_headerBtn' >
                            <ServiceButton
                                className="btn btn-default cancelbtn pull-right"
                                label={
                                    <div className='backtoDetail'>
                                        <MaterialIcon icon="arrow_back" />
                                        <span>Order detail</span>
                                    </div>
                                } 
                                onClick={this.props.gotoOrderDetail} />
                        </ButtonToolbar>
                    </div>

                    <Form className={'add-order'}>
                        <ServiceFormRow label={'Order ID'}>
                            <FormControl value={currentOrder.order_number} disabled/>
                        </ServiceFormRow>
                        <ServiceFormRow label={'Warranty'} className='orderWarranty'>
                            <Col className={'no-pd'} sm={4}>
                                <div className='warranty_tab_title'>{'Order Complete Date'}</div>
                                <FormControl value={currentOrder.complete_date} disabled />
                            </Col>
                            <Col  sm={4}>
                             
                            <div className='warranty_tab_title'>{'Warranty Length'}</div>
                                <FormControl value={currentOrder.warranty_length} disabled />
                            </Col>
                            <Col className={'no-pd'} sm={4}>
                            
                            <div className='warranty_tab_title'>{'Warranty End Date'}</div>
                                <FormControl value={currentOrder.warranty_end_date} disabled />
                            </Col>
                        </ServiceFormRow>
                        <hr />
                        <ActivityList
                            heading={'Recent Activity'}
                            logs={currentOrderActivity}
                        />
                    </Form>
                </div>
            </ServicePanel>
        )
    }
}