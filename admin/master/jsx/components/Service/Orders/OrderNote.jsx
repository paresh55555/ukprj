import React, {PropTypes} from 'react';

export default function OrderNote(props) {
    return (
        <textarea onChange={props.onChange} className={'note form-control'} placeholder={"Add Note"} value={props.note.note} />
    );
}

OrderNote.propTypes = {
    note: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired
};