import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {toggleAddOrder} from "../../../actions/service";
import * as ApiService from "../../../api/ApiService";

class AddOrderButton extends Component {
    newOrder = () =>{
        this.props.dispatch(toggleAddOrder(true));
    }
    render() {
        return (
            <a onClick={this.newOrder} className="btn btn-labeled btn-info pull-right">Create Order</a>
        );
    }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddOrderButton)