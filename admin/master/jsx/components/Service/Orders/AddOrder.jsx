import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col, Button, ButtonToolbar, Form, ControlLabel, FormGroup, FormControl, HelpBlock} from "react-bootstrap";
import * as ApiService from "../../../api/ApiService";
import ServicePanel from "../ServicePanel";
import {toggleAddOrder} from "../../../actions/service";

class AddOrder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            order_number: '',
            error: ''
        }
    }
    componentWillUnmount(){
        ApiService.ApiGetOrders();
    }
    createOrder = () =>{
        if(!this.validate()){
            return
        }
        ApiService.ApiAddOrder(this.state.order_number)
    }
    cancel = () =>{
        this.props.dispatch(toggleAddOrder(false))
    }
    onChange = (value) =>{
        this.setState({order_number: value},()=>{
            this.validate()
        })
    }
    validate(){
        if(!this.state.order_number){
            this.setState({error: 'Order number is required'})
            return false;
        }else{
            this.setState({error: ''})
        }
        return true;
    }
    renderButtons(){
        return (
            <ButtonToolbar>
                <Col smOffset={2} sm={10}>
                    <Button bsStyle="primary" onClick={this.createOrder}>Create order</Button>
                    <Button onClick={this.cancel}>Cancel</Button>
                </Col>
            </ButtonToolbar>
        )
    }
    renderForm(){
        return (
            <FormGroup validationState={this.state.error ? "error" : null}>
                <Col componentClass={ControlLabel} sm={2}>Order ID</Col>
                <Col sm={10}>
                    <FormControl onChange={(v) => this.onChange(v.target.value)} value={this.state.order_number}/>
                    <HelpBlock>{this.state.error}</HelpBlock>
                </Col>
            </FormGroup>
        )
    }
    render() {
        return (
            <ServicePanel header={'Create order'}>
                <Form horizontal>
                    {this.renderForm()}
                    {this.renderButtons()}
                </Form>
            </ServicePanel>
        );
    }
}

const mapStateToProps = (state) => ({})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddOrder)