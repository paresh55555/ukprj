import React from 'react';
import UpdateOrder from './UpdateOrder';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{currentOrder:{order_number:1},orderPurchasers:[]}
};

const mockStore = configureStore();
let store;

describe('<UpdateOrder/>',()=>{
    let component, updateOrder;
    const props = {};

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><UpdateOrder {...props} /></Provider>);
        updateOrder = component.find('UpdateOrder');
    });

    it('length', () => {
        expect(updateOrder).toHaveLength(1);
    });

    it('render',()=>{
        expect(component.find('UpdateOrder').render().html().replace(new RegExp("&#x2039;",'g') ,"‹").replace(new RegExp("&#x203A;",'g'),"›").replace('disabled','disabled=\"\"')).toEqual(component.html());
    });

    it('componentWillMount',()=>{
        expect(updateOrder.node.componentWillMount()).toEqual(undefined);
    });

    it('componentWillUnmount',()=>{
        expect(updateOrder.node.componentWillUnmount(()=>{
            expect(UpdateOrder.node.props.setCurrentOrder).toEqual(null)
            expect(UpdateOrder.node.props.toggleAddOrder).toEqual(true)
        })).toEqual(undefined);
    });
    it ('rendered Component',()=>{
        expect(updateOrder.find('ServicePanel')).toHaveLength(1);
        expect(updateOrder.find('OrderForm')).toHaveLength(1);
        expect(updateOrder.find('Purchasers')).toHaveLength(1);
        expect(updateOrder.find('Docs')).toHaveLength(1);
        expect(updateOrder.find('Doors')).toHaveLength(1);
    });
});
