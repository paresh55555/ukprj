import React from 'react';
import FileUploadInfo from './FileUploadInfo';
import {shallow} from 'enzyme';

describe('<FileUploadInfo/>',()=> {

    let component,props = {
        uploading:false,
        file:null,
        fileName:'filename',
        onFileLoaded:jest.fn(),
        uploadingProgress:50
    };

    beforeEach(() => {
        component = shallow(<FileUploadInfo {...props}/>)
    });

    it('length', () => {
        expect(component).toHaveLength(1);
    });

    it('componentWillMount',()=>{
        expect(component.instance().componentWillMount()).toEqual(undefined);
    });

    it('componentWillReceiveProps',()=>{
       expect(component.instance().componentWillReceiveProps({file:new Blob()})).toEqual(undefined);
    });

   it('getCssClass',()=>{
        expect(component.instance().getCssClass()).toEqual('');
   });

   it('getFileName',()=>{
      expect(component.instance().getFileName()).toEqual('filename');
   });

   it('getProgress',()=>{
      expect(component.instance().getProgress()).toEqual('');
   });

   it('rendered component',()=>{
      expect(component.find('.service-file')).toHaveLength(1);
      expect(component.find('Col')).toHaveLength(2);
      expect(component.find('.filename')).toHaveLength(1);
      expect(component.find('.downloadicon')).toHaveLength(1);
   });

});