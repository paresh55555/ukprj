import React from 'react';
import ServiceCheckbox from './ServiceCheckbox';
import {shallow} from 'enzyme';

describe('<ServiceCheckbox/>', () => {

    let component, props = {
        onChange: jest.fn(),
        checked: false,
        label: 'test'
    };

    beforeEach(() => {
        component = shallow(<ServiceCheckbox {...props}/>)
    });
    it('length', () => {
        expect(component).toHaveLength(1);
    });

    it('expected Html', () => {
         expect(component.html()).toEqual(`<div class="checkbox c-checkbox"><label><input type="checkbox"/><em class="fa fa-check"></em>${props.label}</label></div>`);
    });

    it('onChange Button',()=>{
        const input = component.find('input');
        expect(input).toHaveLength(1);
        input.simulate('change');
        expect(props.onChange).toHaveBeenCalled();

    })
});
