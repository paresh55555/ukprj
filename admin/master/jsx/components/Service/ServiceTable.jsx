import React, {Component, PropTypes} from 'react';
import {Col, FormControl, Row} from "react-bootstrap";
import DataTable from "../datatable/DataTable";

export default class ServiceTable extends Component {
    search = (event) =>{
        let oTable = $('#serviceTable').DataTable();
        oTable.search(event.target.value).draw() ;
    }
    onClick(data){
        if(this.props.onClick){
            this.props.onClick(data)
        }else{
            return false
        }
    }

    renderTopButton(){
        if(this.props.topButton){
            return(
                <Col sm={6}>{this.props.topButton}</Col>
            )
        }else{
            <Col sm={6}></Col>
        }
        
    }

    renderSearch(){
        return (
            <Row className={'top-section'}>
                <Col sm={6}>
                    <FormControl className={'pull-left orders-search'} placeholder={"Search"} onKeyUp={this.search} />
                </Col>
                {this.renderTopButton()}
            </Row>
        )
    }
    render() {
        return (
            <div>
                {this.renderSearch()}
                <DataTable id="serviceTable" className={"panel panel-default table-bordered table-striped table-hover"}
                           onClick={(data) => this.onClick(data)} data={this.props.data}
                           tableTemplate = {"<'row'<'col-sm-12'tr>><'row'<'col-sm-12'p>>"}
                           fields={this.props.fields} defs={this.props.defs} defaultOrder={0} />
            </div>
        );
    }
}

ServiceTable.propTypes = {
    data: PropTypes.array.isRequired,
    fields: PropTypes.object.isRequired
};