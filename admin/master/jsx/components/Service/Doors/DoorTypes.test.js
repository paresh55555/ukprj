import React from 'react';
import DoorTypes from './DoorTypes';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{
        doorTypes:[{id:1,type:'tp1'},{id:1,type:'tp2'}]
    }
};

const mockStore = configureStore();
let store;

describe("<DoorTypes/>",()=> {

    let component, doorsTypes;

    const props = {
        selectedVal:1,
        onChange:jest.fn()
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><DoorTypes {...props} /></Provider>);
        doorsTypes = component.find('DoorTypes');
    });

    it('length', () => {
        expect(doorsTypes).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('DoorTypes').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
        expect(doorsTypes.node.componentWillMount()).toEqual(undefined);
    });

    it('getTypes',()=>{
        expect(doorsTypes.node.getTypes()).toEqual(initialState.service.doorTypes);
    });

    it('getSelectedValue',()=>{
       expect(doorsTypes.node.getSelectedValue()).toEqual('tp1');
    });

    it('rendered component',()=>{
       expect(doorsTypes.find('SQDropdown')).toHaveLength(1);
    });

    it('onChane SQDropdown',()=>{
       const SQDropdown = doorsTypes.find('SQDropdown');
       expect(SQDropdown).toHaveLength(1);
       SQDropdown.simulate('change',()=>{
           expect(props.onChange).toHaveBeenCalled();
       });
    });

    it('when no doortypes',()=>{

        store = mockStore(Object.assign({},initialState,{service:{}}));
        component = mount(<Provider
            store={store}><DoorTypes {...props} /></Provider>);
        doorsTypes = component.find('DoorTypes');

        expect(doorsTypes.node.getTypes()).toEqual([]);

    });

    it('when selectedVal no match',()=>{
        props.selectedVal = 0;
        component = mount(<Provider
            store={store}><DoorTypes {...props} /></Provider>);
        doorsTypes = component.find('DoorTypes');

        expect(doorsTypes.node.getSelectedValue()).toEqual(false);

    });

});
