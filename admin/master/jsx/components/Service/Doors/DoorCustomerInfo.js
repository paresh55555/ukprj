import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../ServiceButton";
import PersonAvatar from "../Purchasers/PersonAvatar";
import PersonInfo from "../Purchasers/PersonInfo";
import * as ApiService from "../../../api/ApiService";
import {setCurrentPerson, togglePersonFormDialog, setSelectedDoorId} from "../../../actions/service";

class DoorCustomerInfo extends Component {

    // openAddCustomer = () =>{
    //    this.props.dispatch(setSelectedDoorId(this.props.data.id))
    //    this.props.dispatch(togglePersonFormDialog(true,'customer'))
    // }

    deleteCustomer = () => {
        let {id} = this.props.data
        let door_id = this.props.door.id
        const {currentOrder} = this.props.service
        ApiService.ApiDeleteDoorCustomer(currentOrder.id, door_id, id);
    }
    editCustomer = () => {
        this.props.dispatch(setCurrentPerson(this.props.data))
        this.props.dispatch(setSelectedDoorId(this.props.data.door_id))
        this.props.dispatch(togglePersonFormDialog(true, 'customer'))
    }
    
   renderButtons(){
       return (
           <ButtonToolbar className={'buttons pull-right no-pd'}>
               <ServiceButton label={'Edit'} onClick={this.editCustomer} className="customer_edit"/>
               <ServiceButton onClick={this.deleteCustomer} label={'Delete'} className="customer_edit"/>
                {/* <ServiceButton onClick={this.openAddCustomer} label={'Add'}/> */}
           </ButtonToolbar>
       )
   }
   render() {
       let customerInfo = this.props.data
       
       return (
           <div className={'purchaser-info Customer-info Doorjob'} >
               <Row>
               
                   <Col md={1} className='avatar_'>
                   <div className='customer_title'> <p className='notes_section'>Customer Info</p></div>
                       <PersonAvatar info={customerInfo}/>
                   </Col>
                   <Col md={6} className='headtext'>
                       <PersonInfo info={customerInfo}/>
                   </Col>
                   <Col md={5} >
                       {this.renderButtons()}
                   </Col>
               </Row>
           </div>
       );
   }
}

DoorCustomerInfo.propTypes = {
    data: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(DoorCustomerInfo)