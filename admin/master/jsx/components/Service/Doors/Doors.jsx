import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import ServiceFormRow from "../ServiceFormRow";
import {setCurrentDoor, setDoors, toggleAddDoor} from "../../../actions/service";
import AddDoorNote from "./AddDoorNote";
import * as ApiService from "../../../api/ApiService";
import DoorsListContainer from './DoorsListContainer';
import AddDoor from './AddDoor';

class Doors extends Component {
    componentWillMount() {
        let {currentOrder} = this.props.service
        if (currentOrder) {
            ApiService.ApiGetOrderDoors(currentOrder.id)
        }
    }
    componentWillUnmount() {
        this.props.dispatch(setDoors([]))
    }

    onDelete = door => {
        let {currentOrder} = this.props.service
        ApiService.ApiDeleteDoor(currentOrder.id, door.id)
    }

    render() {
        const {service, createRequest} = this.props
        return (
            <ServiceFormRow label={'Doors'}>
                <DoorsListContainer onDelete={this.onDelete} hideCreateJobButton={createRequest}/>
                <AddDoor />
                <AddDoorNote orderId={service.currentOrder.id}/>
            </ServiceFormRow>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Doors)