import React, {PropTypes} from 'react';
import {Badge} from "react-bootstrap";
import moment from "moment";
import {DATE_FORMAT} from "../../Common/constants";


export default function DoorWarrantyStatus(props) {
    if(!props.date)return null

    let isExpired = !moment(props.date, DATE_FORMAT).isAfter(moment())

    let badge = <Badge className={'badge-primary'}>under warranty</Badge>

    if(isExpired){
        badge = <Badge className={'badge-danger'}>warranty expired</Badge>
    }

    return badge;
}

DoorWarrantyStatus.propTypes = {
    date: PropTypes.string.isRequired
};