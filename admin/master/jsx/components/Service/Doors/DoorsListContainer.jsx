import React from 'react';
import { connect } from 'react-redux';
import DoorsList from './DoorsList';
import { setCurrentDoor, toggleAddDoor } from '../../../actions/service';
import Loader from '../../Common/Loader';

class DoorsListContainer extends React.Component {

    onEdit = door => {
        this.props.dispatch(setCurrentDoor(door))
        this.props.dispatch(toggleAddDoor(true))
    }

    render () {

        const {
            hideCreateJobButton,
            onDelete,
            showStatus
        } = this.props

        const {
            doors,
            loadingDoors
        } = this.props.service

        if (loadingDoors) {
            return <Loader />
        } else {
            return (
                <DoorsList
                    showStatus={showStatus}
                    doors={doors}
                    onDelete={onDelete}
                    hideCreateJobButton={hideCreateJobButton}
                    onEdit={this.onEdit}
                />
            )
        }

    }

}

DoorsListContainer.propTypes = {};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(DoorsListContainer)