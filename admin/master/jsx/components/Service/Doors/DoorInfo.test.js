import React from 'react';
import DoorInfo from './DoorInfo';
import {shallow} from 'enzyme';

describe('<DoorInfo/>',()=>{

    let component,props={
        door:{warranty_end_date:"22-08-2018",traits:[]},
        DoorEdit:jest.fn(),
        DeleteDoor:jest.fn(),
        showStatus:true
    };

    beforeEach(()=>{
        component=shallow(<DoorInfo {...props}/>);
    });

    it('length',()=>{
       expect(component).toHaveLength(1);
    });

    it('rendered component',()=>{
        expect(component.find('.content')).toHaveLength(1);
        expect(component.find('Row')).toHaveLength(1);
        expect(component.find('.door-info')).toHaveLength(1);
        expect(component.find('.door-info > .door-type')).toHaveLength(1);
        expect(component.find('DoorWarrantyStatus')).toHaveLength(1);
        expect(component.find('.door-info > .door-warranty')).toHaveLength(1);
        expect(component.find('.door-warranty').children()).toHaveLength(3);
        expect(component.find('.door-info > .door-warranty').children().at(0).text()).toEqual('Warranty:');
        expect(component.find('DoorTraits')).toHaveLength(1);
        expect(component.find('ButtonToolbar')).toHaveLength(1);
        expect(component.find('ServiceButton')).toHaveLength(2);
        expect(component.find('ServiceButton').at(0).props().label).toEqual('Edit');
        expect(component.find('ServiceButton').at(1).props().label).toEqual('Delete');
    });

    it('onClick Edit',()=>{
        const ServiceButton = component.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(0).simulate('click');
        expect(props.DoorEdit).toHaveBeenCalled();
    });

    it('onClick Delete',()=>{
        const ServiceButton = component.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(1).simulate('click');
        expect(props.DeleteDoor).toHaveBeenCalled();
    });

    it('when no showStatus false',()=>{
        props.showStatus = false;
        props.door.warranty_end_date = '';
        component=shallow(<DoorInfo {...props}/>);
        expect(component.find('DoorWarrantyStatus')).toHaveLength(0);
    })

});