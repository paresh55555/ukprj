import React from 'react';
import DoorCustomers from './DoorCustomers';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{doors:[{id:1,order_id:1,customers:[{id:1}]}],selectedDoorId:1}
};
const mockStore = configureStore();
let store;

describe("<DoorCustomers/>",()=> {

    let component, doorCustomers;

    const props = {
        data: {id: 1},
        door: {id: 1,customers:[{id:1}]}
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><DoorCustomers {...props} /></Provider>);
        doorCustomers = component.find('DoorCustomers');
    });

    it('length', () => {
        expect(doorCustomers).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('DoorCustomers').render().html()).toEqual(component.html());
    });

    it('addCustomer',()=>{

        doorCustomers.node.addCustomer({id:1},()=>{
            expect(doorCustomers.node.props.service.selectedDoorId).toEqual(null);
            expect(doorCustomers.node.props.service.personFormOpened).toEqual(false);
            expect(doorCustomers.node.props.service.personFormId).toEqual(null);
        });
        doorCustomers.node.addCustomer({id:2},()=>{
            expect(doorCustomers.node.props.service.selectedDoorId).toEqual(null);
            expect(doorCustomers.node.props.service.personFormOpened).toEqual(false);
            expect(doorCustomers.node.props.service.personFormId).toEqual(null);
        });

    });

    it('rendered component',()=>{
        expect(doorCustomers.find('.purchasers-list')).toHaveLength(1);
        expect(doorCustomers.find('PersonForm')).toHaveLength(1);
        expect(doorCustomers.find('DoorCustomerInfo')).toHaveLength(props.door.customers.length);
    });

    it('when selecteddoorId no match',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,selectedDoorId:2}}));
        component = mount(<Provider
            store={store}><DoorCustomers {...props} /></Provider>);
        doorCustomers = component.find('DoorCustomers');

        doorCustomers.node.addCustomer({id:2},()=>{
            expect(doorCustomers.node.props.service.selectedDoorId).toEqual(null);
            expect(doorCustomers.node.props.service.personFormOpened).toEqual(false);
            expect(doorCustomers.node.props.service.personFormId).toEqual(null);
        });
    })



});