import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import SQModal from "../../Common/SQModal/SQModal";
import {
    setCurrentDoor,
    toggleAddDoor,
} from "../../../actions/service";
import {Row, Col, Form, FormControl, FormGroup, ButtonToolbar} from "react-bootstrap";
import AddDoorTraits from "./AddDoorTraits";
import ServiceFormRow from "../ServiceFormRow";
import ServiceButton from "../ServiceButton";
import ServiceDatepicker from "../ServiceDatepicker";
import ServiceCheckbox from "../ServiceCheckbox";
import DoorTypes from "./DoorTypes";
import * as ApiService from "../../../api/ApiService";
import moment from 'moment';
import FieldError from "../FieldError";
import {DATE_FORMAT} from "../../Common/constants";
import { partial } from '../../Common/utilFunctions';

class AddDoor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasWarranty: false,
            currentDoor:{
                warranty: '',
                customType: '',
                door_type_id: 0,
                traits: []
            },
            error: {}
        }
    }
    componentWillMount(){
        let {currentDoor} = this.props.service
        this.setCurrentDoor(currentDoor);
    }
    componentWillUnmount(){
        this.props.dispatch(setCurrentDoor(null))
    }
    componentWillReceiveProps(nextProps){
        let {currentDoor} = nextProps.service
        this.setCurrentDoor(currentDoor)
    }
    resetState(){
        this.setState({
            hasWarranty: false,
            currentDoor:{
                warranty_end_date: '',
                customType: '',
                door_type_id: 0,
                traits: []
            },
            error: {}
        })
    }
    setCurrentDoor(currentDoor){
        if(currentDoor && currentDoor.id){
            this.setState({currentDoor})
            this.setState({
                hasWarranty: !!currentDoor.warranty_end_date
            })
        }
    }
    closeAddDoor = () =>{
        this.resetState()
        this.props.dispatch(setCurrentDoor(null))
        this.props.dispatch(toggleAddDoor(false))
    }
    onChange = (value, type) =>{
        if(type === 'warranty_end_date'){
            if(moment.isMoment(value)){
                value = value.format(DATE_FORMAT)
            }
        }
        if(type === 'door_type_id'){this.state.currentDoor.customType = ''}
        if(type === 'customType'){this.state.currentDoor.door_type_id = 0}
        this.state.currentDoor[type] = value;
        this.setState({currentDoor: this.state.currentDoor})
        this.checkField(type)
    }
    checkField(field){
        if(field === 'door_type_id' || field === 'customType'){
            this.state.error['type'] = !this.state.currentDoor[field]
        }
        if(field === 'warranty_end_date' && this.state.hasWarranty){
            this.state.error['warranty_end_date'] = !moment(this.state.currentDoor.warranty_end_date, DATE_FORMAT).isValid()
        }
        this.setState(this.state)
    }
    validate(){
        let valid = true
        if(!this.state.currentDoor.door_type_id && !this.state.currentDoor.customType){
            this.checkField('door_type_id')
            valid = false
        }
        if(this.state.hasWarranty && !this.state.currentDoor.warranty_end_date){
            this.checkField('warranty_end_date')
            valid = false
        }
        if(valid){
            this.state.error = {}
        }
        return valid
    }
    addDoor = () =>{
        let type = this.state.currentDoor.door_type_id;
        if(!this.validate())return
        if(this.state.currentDoor.customType){
            ApiService.ApiCreateDoorType({type:this.state.currentDoor.customType, category: "order"}).then((response)=>{
                type = response.data[0].id
                this.createDoor(type);
            });
        }else{
            this.createDoor(type)
        }
    }
    createDoor(type){
        let {service, onAdd} = this.props

        this.state.currentDoor.door_type_id = type
        this.state.currentDoor.traits = service.currentTraits

        if(this.state.currentDoor.id){
            ApiService.ApiUpdateDoor(this.state.currentDoor.order_id, this.state.currentDoor.id, this.state.currentDoor).then(()=>{
                this.closeAddDoor()
            })
        }else{

            const actionToHit = onAdd || partial(ApiService.ApiAddDoor, service.currentOrder.id)
            
            actionToHit(this.state.currentDoor).then(()=>{
                this.closeAddDoor()
            })
        }
    }
    toggleWarranty = () =>{
        if(this.state.hasWarranty){
            this.state.currentDoor.warranty_end_date = '';
            this.state.error.warranty_end_date = false;
            this.setState({currentDoor:this.state.currentDoor})
        }
        this.setState({hasWarranty:!this.state.hasWarranty})
    }
    renderTypeSelection(){
        return(
            <ServiceFormRow label={'Door System'}>
                <DoorTypes category={"order"}
                           selectedVal={this.state.currentDoor.door_type_id}
                           onChange={(type) => this.onChange(type, 'door_type_id')} />
                <FieldError error={this.state.error.type} text={'Please select type'}/>
            </ServiceFormRow>
        )
    }
    renderCustomType(){
        return (
            <FormGroup>
                <FormControl placeholder={'Custom Door System'}
                             onChange={(e) => this.onChange(e.target.value, 'customType')}
                             value={this.state.currentDoor.customType}/>
                <FieldError error={this.state.error.type} text={'Please select type'}/>
            </FormGroup>
        )
    }
    renderWarranty(){
        return (
            <Row>
                <Col sm={3}>
                    <ServiceCheckbox checked={this.state.hasWarranty} onChange={this.toggleWarranty} label={'Has Warranty'} />
                </Col>
                <Col sm={9}>
                    <ServiceDatepicker value={this.state.currentDoor.warranty_end_date}
                                       onChange={(date) => this.onChange(date, 'warranty_end_date')}
                                       disabled={!this.state.hasWarranty}
                                       placeholder={'Warranty Ends'}/>
                    <FieldError error={this.state.error.warranty_end_date} text={'Please select warranty end date'}/>
                </Col>
            </Row>
        )
    }
    renderButtons = () =>{
        let buttonLabel = this.state.currentDoor.id ? 'Update Door' : 'Add Door'
        return (
            <ButtonToolbar>
                <ServiceButton bsStyle="primary" onClick={this.addDoor} label={buttonLabel} />
                <ServiceButton onClick={this.closeAddDoor} label={'Cancel'} />
            </ButtonToolbar>
        )
    }

    openAddDoor = () => {
        this.props.dispatch(setCurrentDoor(null))
        this.props.dispatch(toggleAddDoor(true))
    }

    renderAddButton () {
        const {doors} = this.props.service

        let props = {}

        if (doors & doors.length) {

            props.className = 'pull-right'
            props.label = 'Add Door'

        } else {

            props.fullWidth = true
            props.label = 'Add Another Door'

        }

        return <ServiceButton {...props} onClick={this.openAddDoor}/>

    }

    render() {
        return (
            <div>
                <SQModal title={'Add Door'} className={'service-modal'}
                        isOpen={this.props.service.addDoorOpened}
                        onClose={this.closeAddDoor}
                        customButtons>
                    <Form className={'service'}>
                        {this.renderTypeSelection()}
                        <div className={'form-divider'}></div>
                        <ServiceFormRow label={'Custom Door'}>
                            {this.renderCustomType()}
                            {this.renderWarranty()}
                            <AddDoorTraits />
                            {this.renderButtons()}
                        </ServiceFormRow>
                    </Form>
                </SQModal>
                {this.renderAddButton()}
            </div>
        );
    }
}

AddDoor.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddDoor)