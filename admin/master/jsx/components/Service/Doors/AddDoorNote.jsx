import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import SQModal from "../../Common/SQModal/SQModal";
import * as ApiService from "../../../api/ApiService";
import {toggleAddDoorNote, selectNote} from "../../../actions/service";
import {ButtonToolbar, Form, FormControl} from "react-bootstrap";
import ServiceFormRow from "../ServiceFormRow";
import ServiceButton from "../ServiceButton";

class AddDoorNote extends Component {
    constructor(props) {
        super(props);
        this.state = {
            note: ''
        }
    }

    onChange = (e) => {
        this.setState({note: e.target.value})
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.service.selectedNote) {
            this.setState({note: nextProps.service.selectedNote.note})
        } else {
            this.setState({note: ''})
        }
    }

    addNote = () => {
        const note = {
            note: this.state.note
        }
        let {selectedNote, selectedDoorId} = this.props.service;
        if (selectedNote) {
            ApiService.ApiUpdateDoorNote(selectedNote.orderId, selectedNote.doorId, selectedNote.id, note);
        } else {
            ApiService.ApiAddDoorNote(this.props.orderId, selectedDoorId, note)
        }
        this.onClose()
    }
    onClose = () => {
        this.props.dispatch(toggleAddDoorNote(false))
        this.props.dispatch(selectNote(null))
    }

    renderForm() {
        return (
            <ServiceFormRow label={'Note Details'}>
                <FormControl componentClass="textarea" style={{height: 120}} placeholder={'Custom Description'}
                             onChange={this.onChange} value={this.state.note}/>
            </ServiceFormRow>
        )
    }

    renderButtons() {
        let label = 'Add Note';
        if (this.props.service.selectedNote) {
            label = 'Update Note'
        }
        return (
            <ServiceFormRow label={''}>
                <ButtonToolbar>
                    <ServiceButton bsStyle="primary" className="cancelbtn" label={label} onClick={() => this.addNote()}/>
                    <ServiceButton className="cancelbtn" onClick={this.onClose} label={'Cancel'}/>
                </ButtonToolbar>
            </ServiceFormRow>
        )
    }

    render() {
        const {service} = this.props

        return (
            <SQModal
                title={'Add Note'}
                className="service-modal"
                onClose={this.onClose}
                isOpen={service.addDoorNoteOpened} customButtons>
                <Form className={'service'}>
                    {this.renderForm()}
                    {this.renderButtons()}
                </Form>
            </SQModal>
        );
    }
}

AddDoorNote.propTypes = {};

const mapStateToProps = (state) => ({
    service: state.service

})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddDoorNote)