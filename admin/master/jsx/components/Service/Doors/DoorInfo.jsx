import React from 'react';
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../ServiceButton";
import DoorTraits from "./DoorTraits";
import DoorWarrantyStatus from "./DoorWarrantyStatus";

export default function DoorInfo(props) {
    
    const {door} = props
    const date = door.warranty_end_date ? door.warranty_end_date :''

    return (
        <div className={'content'}>
            <Row>
                <Col md={7} className="door-info">
                    <div className='door-type'>{door.door_type} {props.showStatus && <DoorWarrantyStatus date={door.warranty_end_date} />}</div>
                    <div className='door-warranty'><span>Warranty:</span> {date}</div>
                    <DoorTraits traits={door.traits}/>
                </Col>
                <Col md={5}>
                <ButtonToolbar className='pull-right customerbtns'>
                    <ServiceButton onClick={() => props.DoorEdit()} label={'Edit'} className="customer_edit"/>
                    <ServiceButton onClick={() => props.DeleteDoor()} label={'Delete'} className="customer_edit"/>
                </ButtonToolbar>
                </Col>
            </Row>
        </div>
    );
}