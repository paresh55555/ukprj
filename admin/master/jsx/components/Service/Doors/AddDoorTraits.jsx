import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col, Button, FormControl, Checkbox} from "react-bootstrap";
import SQDropdown from "../../Common/Dropdown/SQDropdown";
import * as ApiService from "../../../api/ApiService";
import {setCurrentTraits} from "../../../actions/service";
import FontIcon from "../../Common/FontIcon";
import ServiceButton from "../ServiceButton";

class AddDoorTraits extends Component {
    constructor(props) {
        super(props);
        this.state = {
            defaultTrait: {
                id: 0,
                name:'',
                trait_value: ''
            },
            traits: []
        }
    }
    componentWillMount(){
        ApiService.ApiGetTraitTypes()
        if(this.props.service && this.props.service.currentDoor){
            let traits = []
            this.props.service.currentDoor.traits.map((trait)=>{
                traits.push({id: trait.id, name: trait.name, trait_value: trait.trait_value})
            })
            this.props.dispatch(setCurrentTraits(traits))
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.service && nextProps.service.traitTypes && nextProps.service.traitTypes.length){
            this.setState({defaultTrait: {id: 0, name: nextProps.service.traitTypes[0], trait_value:''}})
        }
    }
    componentWillUnmount(){
        this.props.dispatch(setCurrentTraits([]))
    }
    updateTrait = (val, type, index) =>{
        let traits = this.getTraits()
        traits[index][type] = val
        this.props.dispatch(setCurrentTraits(traits))
    }
    getTraits = () =>{
        let {service} = this.props
        if(!service){
            return []
        }
        return service.currentTraits
    }
    addTrait = () =>{
        let traits = this.getTraits()
        traits.push(this.state.defaultTrait);
        this.props.dispatch(setCurrentTraits(traits))
    }
    removeTrait = (index)=>{
        let traits = this.getTraits()
        traits.splice(index, 1)
        this.props.dispatch(setCurrentTraits(traits))
    }
    renderTraits = () =>{
        const {service} = this.props
        if(!service){
            return ''
        }
        let traits = service.currentTraits
        if(!traits){
            return ''
        }
        return (
            traits.map((trait, i)=>{
                return <Row className={'trait'} key={i}>
                    <Col sm={4}><SQDropdown data={service.traitTypes}
                                            selectedVal={trait.name}
                                            onChange={(e) => this.updateTrait(e,'name', i)} id={'trait-'+i} fullWidth /></Col>
                    <Col sm={7}><FormControl placeholder={'Add description'} onChange={(e) => this.updateTrait(e.target.value, 'trait_value', i)} value={trait.trait_value}/></Col>
                    <Col sm={1}><FontIcon className={'trait-remove'} onClick={()=>this.removeTrait(i)} iconClass={'fa-trash-o'}/></Col>
                </Row>
            })
        )
    }
    renderButton(){
        let {service} = this.props,
            button = <ServiceButton className={'pull-right'} onClick={this.addTrait} label={'Add trait'} fullWidth />
        if(service && service.currentTraits.length){
            button = <ServiceButton className={'pull-right'} onClick={this.addTrait} label={'Add another'}/>
        }
        return (
            <Row>
                <Col sm={12}>{button}</Col>
            </Row>
        )
    }
    render() {
        return (
            <div>
                {this.renderTraits()}
                {this.renderButton()}
            </div>
        );
    }
}

AddDoorTraits.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddDoorTraits)