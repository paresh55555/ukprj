import React from 'react';
import AddDoorNote from './AddDoorNote';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{
        addDoorNoteOpened:true,
        selectedNote:{note:'test note'},
        selectedDoorId:1
    },
};

const mockStore = configureStore();
let store;

describe("<AddDoorNote/>",()=> {

    let component, addDoorNote;

    const props = {};

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddDoorNote {...props} /></Provider>);
        addDoorNote = component.find('AddDoorNote');
    });

    it('length', () => {
        expect(addDoorNote).toHaveLength(1);
    });

    it('onChange',()=>{
        addDoorNote.node.onChange({target:{value:'te'}});
        expect(addDoorNote.node.state.note).toEqual('te');
    });

    it('componentWillReceiveProps',()=>{
        addDoorNote.node.componentWillReceiveProps({service:{
            addDoorNoteOpened:true,
            selectedNote:{note:'test no'}
        }});
        expect(addDoorNote.node.state.note).toEqual('test no');
        addDoorNote.node.componentWillReceiveProps({service:{
            addDoorNoteOpened:true,
        }});
        expect(addDoorNote.node.state.note).toEqual('');
    });

    it('addNote',()=>{
        addDoorNote.node.addNote(()=>{
            expect(addDoorNote.node.props.addDoorNoteOpened).toEqual(false);
            expect(addDoorNote.node.props.selectedNote).toEqual(null);
        });
    });

    it('onClose',()=>{
        addDoorNote.node.onClose(()=>{
            expect(addDoorNote.node.props.addDoorNoteOpened).toEqual(false);
            expect(addDoorNote.node.props.selectedNote).toEqual(null);
        });
    });

    it('when no note selected',()=>{
        props.orderId = 1;
        store = mockStore(Object.assign({},initialState,{service:{selectedDoorId:1}}));
        component = mount(<Provider
            store={store}><AddDoorNote {...props} /></Provider>);
        addDoorNote = component.find('AddDoorNote');

        addDoorNote.node.addNote(()=>{
            expect(addDoorNote.node.props.addDoorNoteOpened).toEqual(false);
            expect(addDoorNote.node.props.selectedNote).toEqual(null);
        });
    })

});