import React from 'react';
import DoorsList from './DoorsList';
import {mount} from 'enzyme'
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{doors:[{order_id:1},{order_id:2}],createRequest:{},currentOrder:{id:1}}

};
const mockStore = configureStore();
let store;

describe("<DoorsList/>",()=> {

    let component, doorList;

    const props = {
        showStatus:true
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider  store={store}><DoorsList {...props} /></Provider>);
        doorList = component.find('DoorsList');
    });

    it('length', () => {
        expect(doorList).toHaveLength(1);
    });

    it('render',()=>{
        expect(component.find('DoorsList').render().html()).toEqual(component.html());
    });

    it('editDoor',()=>{
        doorList.node.editDoor({order_id:2},()=>{
            expect(doorList.node.props.service.currentDoor).toEqual({order_id:2});
            expect(doorList.node.props.service.addDoorOpened).toEqual(true);
        });
    });

    it('deleteDoor',()=>{
        expect(doorList.node.deleteDoor({id:2})).toEqual(undefined);
    });

    it('rendered component',()=>{
        expect(doorList.find('.doors-list')).toHaveLength(initialState.service.doors.length);
        expect(doorList.find('.door')).toHaveLength(initialState.service.doors.length);
        expect(doorList.find('Col')).toHaveLength(10);
        expect(doorList.find('DoorInfo')).toHaveLength(initialState.service.doors.length);
        expect(doorList.find('DoorNotes')).toHaveLength(initialState.service.doors.length);
        expect(doorList.find('DoorCustomers')).toHaveLength(initialState.service.doors.length);
        expect(doorList.find('DoorButtons')).toHaveLength(initialState.service.doors.length);
        expect(doorList.find('.clearfix')).toHaveLength(1);
    });

    it('when no doors',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,doors:[]}}));
        component = mount(<Provider  store={store}><DoorsList {...props} /></Provider>);
        doorList = component.find('DoorsList');

        const expectedHtml = '<div><div class="clearfix"></div></div>';

        expect(component.html()).toEqual(expectedHtml);
    });

});