import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col} from "react-bootstrap";
import DoorInfo from "./DoorInfo";
import {setCurrentDoor, toggleAddDoor} from "../../../actions/service";
import DoorNotes from "./DoorNotes";
import DoorCustomers from "./DoorCustomers";
import DoorButtons from "./DoorButtons";
import * as ApiService from '../../../api/ApiService'

class DoorsList extends Component {

    editDoor = (door) => {
        this.props.onEdit(door)
    }

    deleteDoor = (door) => {
        this.props.onDelete(door)
    }

    render() {
        
        const {
            doors = [],
            hideCreateJobButton = false,
            showStatus = false
        } = this.props

        return (
            <div>
                {doors.map((door, i) => {
                    return (
                        <div className="doors-list" key={i}>
                            <Row className={'door'}>
                                <Col md={12}>
                                    <DoorInfo door={door} DeleteDoor={() => this.deleteDoor(door)} DoorEdit={() => this.editDoor(door)}/>
                                </Col>
                                <Col md={12}>
                                    <DoorNotes door={door} orderId={door.order_id}/>
                                </Col>
                                <Col md={12}>
                                    <DoorCustomers door={door}/>
                                </Col>
                                <DoorButtons door={door} createRequest={hideCreateJobButton}/>
                            </Row>
                        </div>)
                })}
                <div className={'clearfix'}></div>
            </div>
        );
    }
}

DoorsList.propTypes = {};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(DoorsList)