import React, {Component} from 'react';
import {connect} from "react-redux";
import {toggleAddDoorNote, togglePersonFormDialog, setSelectedDoorId} from "../../../actions/service";
import ServiceButton from "../ServiceButton";
import {ButtonToolbar} from "react-bootstrap";
import * as ApiService from "../../../api/ApiService";

class DoorButtons extends Component {

    openAddNote = () => {
        this.props.dispatch(setSelectedDoorId(this.props.door.id))
        this.props.dispatch(toggleAddDoorNote(true))
    }
    
    openAddCustomer = () => {
        this.props.dispatch(setSelectedDoorId(this.props.door.id))
        this.props.dispatch(togglePersonFormDialog(true, 'customer-' + this.props.door.id))
    }

    createJob = (id) => {
        ApiService.ApiCreateDoorJob(id)
            .then(response => {
                this.context.router.push('service/jobs/' + response.data.data[0].id);
            })
    }

    renderNotesButton() {
        return (
            <div className="text-center customer_btn">
                <ServiceButton onClick={this.openAddNote} label={'Add Note'}/>
            </div>
        )
    }

    renderCustomerButton() {
        return (
            <div className="text-center customer_btn">
                <ServiceButton onClick={this.openAddCustomer} label={'Attach Customer'}/>
            </div>
        )
    }

    renderCreateJobButton = () => {
        let {door, createRequest} = this.props
        if (createRequest) {
            return (
                <div className="text-center col-md-12">
                    <ButtonToolbar className="text-center ">
                        <ServiceButton className='createJob' onClick={() => this.createJob(door.id)}
                                       label={'Create Service Request'}/>
                    </ButtonToolbar>
                </div>
            )
        }
    }

    renderButtons() {
        return (
            <div>
                <div className="text-center col-md-12">
                    {this.renderNotesButton()}
                    {this.renderCustomerButton()}
                </div>
                {this.renderCreateJobButton()}
            </div>
        )
    }

    render() {
        return (
            this.renderButtons()
        );
    }
}

DoorButtons.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => ({
    dispatch
})
export default connect(mapDispatchToProps)(DoorButtons)
