import React from 'react';

export default function DoorTraits(props) {
    const {traits} = props
   
    return (
        <div>
            {traits && traits.map((t, i)=>{
                return <div key={i} className='door-traits'><span className={'trait-type'}>{t.name}</span> : {t.trait_value}</div>
            })}
        </div>
    );
}