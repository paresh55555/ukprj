import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {toggleAddDoorNote, selectNote,setSelectedDoorId} from "../../../actions/service";
import * as ApiService from "../../../api/ApiService";
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../ServiceButton";



class DoorNotes extends Component {
    // openAddNote = () =>{
    //     this.props.dispatch(setSelectedDoorId(this.props.door.id))
    //     this.props.dispatch(toggleAddDoorNote(true))
    // }
    deleteNote = (noteId) => {
        let doorId = this.props.door.id
        ApiService.ApiDeleteDoorNote(this.props.orderId, doorId, noteId);
    }
    editNote = (note) => {
        let doorId = this.props.door.id
        this.props.dispatch(toggleAddDoorNote(true))
        let selectedNote = {
            note: note.note,
            id: note.id,
            orderId: this.props.orderId,
            doorId: doorId
        }
        this.props.dispatch(selectNote(selectedNote))
    }
    renderNotesButton(){
        const {notes} = this.props.door
        let label = "Add note"
        if(notes && notes.length){
            label = "Add another note"
        }
        return (
            <div className="text-center customer">
            <ServiceButton onClick={this.openAddNote}  label={label} />
            </div>
        )
    }
    renderNotes() {
        const {notes} = this.props.door
        return (
            notes && notes.map((note, i) => {
                return (
                    <div className='bottom_border' key={i}>
                        <Row className='door_margin'>
                            <Col md={7}>
                                <div key={note.id}>
                                    <div className='notes_section'>{"Notes"}</div>
                                    <div className="notes_text">{note.note}</div>
                                </div>
                            </Col>
                            <Col md={5}>
                                <ButtonToolbar className={'buttons pull-right'}>
                                    <ServiceButton onClick={() => this.editNote(note)} label={'Edit'}/>
                                    <ServiceButton onClick={() => this.deleteNote(note.id)} label={'Delete'}/>
                                     {/* <ServiceButton onClick={this.openAddNote} label={'Add'}/> */}
                                </ButtonToolbar>
                            </Col>
                        </Row>
                    </div>
                )
            })
        )
    }

    render() {
        return (
            <div className={'nootes-door'}>
                {this.renderNotes()}
            </div>
        );
    }
}

DoorNotes.propTypes = {
    door: PropTypes.object.isRequired,
    orderId: PropTypes.number.isRequired
}

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapDispatchToProps)(DoorNotes)