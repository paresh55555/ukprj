import React from 'react';
import AddDoorTraits from './AddDoorTraits';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{
        addDoorOpened:true,
        currentDoor:{traits:[{id:1,name:'test traits',trait_value:0},],id:1,warranty_end_date:'21-08-2018'},
        currentTraits:[{id:1,name:'test traits',trait_value:0},{id:2,name:'test traits1',trait_value:0}],
        currentOrder:{id:1},
        traitTypes:['tt1']
    }
};

const mockStore = configureStore();
let store;

describe("<AddDoorTraits/>",()=> {

    let component, addDoorTraits;

    const props = {};

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddDoorTraits {...props} /></Provider>);
        addDoorTraits = component.find('AddDoorTraits');
    });

    it('length', () => {
        expect(addDoorTraits).toHaveLength(1);
    });

    it('render',()=>{
       expect(component.find('AddDoorTraits').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
        addDoorTraits.node.componentWillMount(()=>{
            expect(addDoorTraits.node.props.currentTraits).toEqual([{id:1,name:'test traits',trait_value:0}]);
        });
    });

    it('componentWillReceiveProps',()=>{
        addDoorTraits.node.componentWillReceiveProps({service:{
            addDoorOpened:true,
            currentDoor:{traits:[{id:1,name:'test traits',trait_value:0}],id:1,warranty_end_date:'21-08-2018'},
            currentTraits:[],
            currentOrder:{id:1},
            traitTypes:['tt2','tt1']
        }});
        expect(addDoorTraits.node.state.defaultTrait).toEqual({id:0,name:'tt2',trait_value:''});
    });

    it('componentWillUnmount',()=>{
        addDoorTraits.node.componentWillUnmount(()=>{
            expect(addDoorTraits.node.props.currentTraits).toEqual([])
        });
    });

    it('updateTrait',()=>{
        addDoorTraits.node.updateTrait('tt3','name',0,()=>{
           expect(addDoorTraits.node.props.service.currentTraits).toEqual({id:1,name:'tt3',trait_value:0});
        });
    });

    it('getTraits',()=>{
        expect(addDoorTraits.node.getTraits()).toEqual(initialState.service.currentTraits);
    });

    it('addTrait',()=>{
        addDoorTraits.node.addTrait(()=>{
           expect(addDoorTraits.node.props.currentTraits).toEqual([{id:1,name:'test traits',trait_value:0},
           {
               id: 0,
               name:'',
               trait_value: ''
           }]);
        });
    });

    it('removeTrait',()=>{
       addDoorTraits.node.removeTrait(0,()=>{
           expect(addDoorTraits.node.props.currentTraits).toEqual([]);
       })
    });

    it('onClick trait-remove',()=>{
        const trait_remove = addDoorTraits.find('.trait-remove');
        expect(trait_remove).toHaveLength(initialState.service.currentTraits.length);
        trait_remove.at(0).simulate('click',()=>{
            expect(addDoorTraits.node.props.currentTraits).toEqual([]);
        })
    });

    it('onChange FormControl',()=>{
        const FormControl = addDoorTraits.find('FormControl');
        expect(FormControl).toHaveLength(initialState.service.currentTraits.length);
        FormControl.at(0).simulate('change','test t',()=>{
            expect(addDoorTraits.node.props.service.currentTraits).toEqual({id:1,name:'test t',trait_value:0});
        });
    });

    it('onChange SQDropdown',()=>{
        const SQDropdown = addDoorTraits.find('SQDropdown');
        expect(SQDropdown).toHaveLength(initialState.service.currentTraits.length);
        SQDropdown.at(0).simulate('change','test t',()=>{
            expect(addDoorTraits.node.props.service.currentTraits).toEqual({id:1,name:'test t',trait_value:0});
        });
    });

    it('rendered Component',()=>{
       expect(addDoorTraits.find('.trait')).toHaveLength(initialState.service.currentTraits.length);
       expect(addDoorTraits.find('SQDropdown')).toHaveLength(initialState.service.currentTraits.length);
       expect(addDoorTraits.find('Col')).toHaveLength((initialState.service.currentTraits.length * 3)+1);
       expect(addDoorTraits.find('FormControl')).toHaveLength(initialState.service.currentTraits.length);
       expect(addDoorTraits.find('.trait-remove')).toHaveLength(initialState.service.currentTraits.length);
       expect(addDoorTraits.find('ServiceButton')).toHaveLength(1);
       expect(addDoorTraits.find('ServiceButton').props().label).toEqual('Add another');
    });

    it('when no currentTraits',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,currentTraits:[]}}));
        component = mount(<Provider
            store={store}><AddDoorTraits {...props} /></Provider>);
        addDoorTraits = component.find('AddDoorTraits');

        expect(addDoorTraits.find('ServiceButton')).toHaveLength(1);
        expect(addDoorTraits.find('ServiceButton').props().label).toEqual('Add trait');

        expect(addDoorTraits.node.renderTraits()).toEqual([]);

    })

});