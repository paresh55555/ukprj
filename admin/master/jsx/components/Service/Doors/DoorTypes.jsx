import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import * as ApiService from "../../../api/ApiService";
import SQDropdown from "../../Common/Dropdown/SQDropdown";

class DoorTypes extends Component {
    componentWillMount(){
        ApiService.ApiGetDoorTypes(this.props.category)
    }
    getTypes(){
        let {service} = this.props,
            types = []
        if(service && service.doorTypes){
            types = service.doorTypes
        }
        return types
    }
    getSelectedValue(){
        let types = this.getTypes()
        let selected = types.find((type)=>{
            return type.id === this.props.selectedVal
        })
        if(selected){
            return selected.type
        }
        return false
    }
    render() {
        return (
            <SQDropdown data={this.getTypes()}
                        onChange={(type) => this.props.onChange(type.id)}
                        id={'doorType'}
                        defaultVal={'Door System'}
                        selectedVal={this.getSelectedValue()}
                        dataKey={'type'}
                        fullWidth
            />
        );
    }
}

DoorTypes.propTypes = {
    category: PropTypes.string.isRequired,
    selectedVal: PropTypes.number.isRequired,
    onChange: PropTypes.func.isRequired
};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(DoorTypes)