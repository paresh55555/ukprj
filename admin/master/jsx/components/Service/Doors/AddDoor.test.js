import React from 'react';
import AddDoor from './AddDoor';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'
import {DATE_FORMAT} from "../../Common/constants";
import moment from 'moment';

const initialState = {
    service:{
        addDoorOpened:true,
        currentDoor:{traits:[],id:1,warranty_end_date:'21-08-2018'},
        currentTraits:[],
        currentOrder:{id:1}
    },
};

const mockStore = configureStore();
let store;

describe("<AddDoor/>",()=> {

    let component, addDoor;

    const props = {};

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddDoor {...props} /></Provider>);
        addDoor = component.find('AddDoor');
    });

    it('length', () => {
        expect(addDoor).toHaveLength(1);
    });

    it('componentWillMount',()=>{
        expect(addDoor.node.componentWillMount()).toEqual(undefined);
        expect(addDoor.node.state.currentDoor).toEqual(initialState.service.currentDoor);
        expect(addDoor.node.state.hasWarranty).toEqual(true);
    });

    it('componentWillUnmount',()=>{
       expect(addDoor.node.componentWillUnmount(()=>{
           expect(addDoor.node.props.currentDoor).toEqual(null)
       })).toEqual(undefined);
    });

    it('componentWillReceiveProps',()=>{
        expect(addDoor.node.componentWillReceiveProps({service:{
            addDoorOpened:true,
            currentDoor:{traits:[],id:1,warranty_end_date:''},
            currentTraits:[]
        }})).toEqual(undefined);
        expect(addDoor.node.state.currentDoor).toEqual({traits:[],id:1,warranty_end_date:''});
        expect(addDoor.node.state.hasWarranty).toEqual(false);
    });

    it('resetState',()=>{
       addDoor.node.resetState();
       expect(addDoor.node.state).toEqual({
           hasWarranty: false,
           currentDoor:{
               warranty_end_date: '',
               customType: '',
               door_type_id: 0,
               traits: []
           },
           error: {}
       });
    });

    it('setCurrentDoor',()=>{
        addDoor.node.setCurrentDoor({traits:[],id:1,warranty_end_date:''});
        expect(addDoor.node.state.currentDoor).toEqual({traits:[],id:1,warranty_end_date:''});
        expect(addDoor.node.state.hasWarranty).toEqual(false);
        addDoor.node.setCurrentDoor({traits:[],id:1,warranty_end_date:'21-08-2018'});
        expect(addDoor.node.state.currentDoor).toEqual({traits:[],id:1,warranty_end_date:'21-08-2018'});
        expect(addDoor.node.state.hasWarranty).toEqual(true);
        expect(addDoor.node.setCurrentDoor()).toEqual(undefined);
    });

    it('closeAddDoor',()=>{
        addDoor.node.closeAddDoor(()=>{
            expect(addDoor.node.props.currentDoor).toEqual(null);
            expect(addDoor.node.props.addDoorOpened).toEqual(false)
        });
        expect(addDoor.node.state).toEqual({
            hasWarranty: false,
            currentDoor:{
                warranty_end_date: '',
                customType: '',
                door_type_id: 0,
                traits: []
            },
            error: {}
        });
    });

    it('onChange',()=>{
        let Mmoment = moment();
        addDoor.node.onChange(Mmoment,'warranty_end_date');
        expect(addDoor.node.state.currentDoor.warranty_end_date).toEqual(Mmoment.format(DATE_FORMAT));
        expect(addDoor.node.state.error.warranty_end_date).toEqual(false);
        addDoor.node.onChange(1,'door_type_id');
        expect(addDoor.node.state.currentDoor.customType).toEqual('');
        expect(addDoor.node.state.currentDoor.door_type_id).toEqual(1);
        expect(addDoor.node.state.error.type).toEqual(false);
        addDoor.node.onChange('test custom','customType');
        expect(addDoor.node.state.currentDoor.customType).toEqual('test custom');
        expect(addDoor.node.state.currentDoor.door_type_id).toEqual(0);
        expect(addDoor.node.state.error.type).toEqual(false);
    });

    it('checkField',()=>{
       addDoor.node.checkField('door_type_id');
       expect(addDoor.node.state.error.type).toEqual(true);
       addDoor.node.checkField('customType');
       expect(addDoor.node.state.error.type).toEqual(false);
        addDoor.node.checkField('warranty_end_date');
        expect(addDoor.node.state.error.type).toEqual(false);
    });

    it('validate',()=>{
       expect(addDoor.node.validate()).toEqual(true);
       expect(addDoor.node.state.error).toEqual({});
       addDoor.node.setState({currentDoor:{door_type_id:false,customType:true},hasWarranty:false,warranty_end_date:true});
        expect(addDoor.node.validate()).toEqual(true);
        expect(addDoor.node.state.error).toEqual({});
        addDoor.node.setState({currentDoor:{door_type_id:false,customType:false}});
        expect(addDoor.node.validate()).toEqual(false);
        expect(addDoor.node.state.error).toEqual({type:true});
        addDoor.node.setState({currentDoor:{door_type_id:true,customType:true,warranty_end_date:''},hasWarranty:true});
        expect(addDoor.node.validate()).toEqual(false);
        expect(addDoor.node.state.error).toEqual({type:true,warranty_end_date:true});
    });

    it('addDoor',()=>{
        expect(addDoor.node.addDoor()).toEqual(undefined);
        addDoor.node.setState({currentDoor:{door_type_id:1,customType:true}});
        expect(addDoor.node.addDoor()).toEqual(undefined);
        addDoor.node.setState({currentDoor:{door_type_id:true,customType:true,warranty_end_date:''},hasWarranty:true});
        expect(addDoor.node.addDoor()).toEqual(undefined);

    });

    it('createDoor',()=>{
        addDoor.node.setState({currentDoor:{id:0}});
       expect(addDoor.node.createDoor('test custom')).toEqual(undefined);
        addDoor.node.setState({currentDoor:{id:1,order_id:1}});
        expect(addDoor.node.createDoor('test custom')).toEqual(undefined);
    });

    it('toggleWarranty',()=>{
        addDoor.node.setState({hasWarranty:false});
        addDoor.node.toggleWarranty();
        expect(addDoor.node.state.hasWarranty).toEqual(true);
        addDoor.node.toggleWarranty();
        expect(addDoor.node.state.currentDoor.warranty_end_date).toEqual('');
        expect(addDoor.node.state.error.warranty_end_date).toEqual(false);
    });

});