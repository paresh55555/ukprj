import React, {Component} from 'react';
import {connect} from "react-redux";
import PersonForm from "../Purchasers/PersonForm";
import * as ApiService from "../../../api/ApiService";
import { togglePersonFormDialog, setSelectedDoorId} from "../../../actions/service";
import DoorCustomerInfo from "./DoorCustomerInfo";

class DoorCustomers extends Component {

    addCustomer = (person) =>{
        const {doors} = this.props.service
        const {door} = this.props
        let selectedDoorId = this.props.service.selectedDoorId;
        let selectedDoor = doors.find(door=>door.id === selectedDoorId);
        if(selectedDoor){
            let foundPerson = selectedDoor.customers.find(customer=> customer.id === person.id);

            if(foundPerson){
                ApiService.ApiUpdateDoorCustomer(door.order_id, selectedDoorId, person)
            }
            else{
                delete person.id;
                ApiService.ApiAddDoorCustomer(door.order_id, selectedDoorId, person)
            }
        }
        this.props.dispatch(togglePersonFormDialog(false, null))
        this.props.dispatch(setSelectedDoorId(null))
    }



    renderCustomer(){
        const {customers=[]} = this.props.door
        return(
            customers.map((customer)=>{
                return <DoorCustomerInfo key={customer.id} data={customer} door={this.props.door}/>
            })
        )
    }

    render() {  
        return (
            <div className={'purchasers-list'}>
                <PersonForm title={'Attach Customer'} id={'customer-'+this.props.door.id} onSubmit={this.addCustomer}/>
                {this.renderCustomer()}
            </div>
        );
    }
}


const mapDispatchToProps = dispatch => ({
    dispatch
})

const mapStateToProps = (state) => ({
   service: state.service
})

export default connect(mapStateToProps, mapDispatchToProps)(DoorCustomers)