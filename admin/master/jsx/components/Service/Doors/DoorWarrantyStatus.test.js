import React from 'react';
import DoorWarrantyStatus from './DoorWarrantyStatus';
import {shallow} from 'enzyme';
import moment from 'moment';

describe('<DoorWarrantyStatus/>',()=>{

    let component,props={
        date:moment().add(5, 'days')
    };

    beforeEach(()=>{
        component = shallow(<DoorWarrantyStatus {...props} />);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('rendered component',()=>{
        expect(component.find('.badge-primary')).toHaveLength(1);
    });

    it('when expired',()=>{
        props.date = moment();
        component = shallow(<DoorWarrantyStatus {...props} />);
        expect(component.find('.badge-danger')).toHaveLength(1);
    });

    it('when no date',()=>{
        delete props.date;
        component = shallow(<DoorWarrantyStatus {...props} />);
        expect(component.html()).toEqual(null);
    })

});