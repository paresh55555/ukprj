import React from 'react';
import DoorNotes from './DoorNotes';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {

};

const mockStore = configureStore();
let store;

describe("<DoorNotes/>",()=> {

    let component, doorNotes;

    const props = {
        door:{id:1,notes:[{id:1,note:'test note'}]},
        orderId:1
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><DoorNotes {...props} /></Provider>);
        doorNotes = component.find('DoorNotes');
    });

    it('length', () => {
        expect(doorNotes).toHaveLength(1);
    });

    it('render',()=>{
        expect(component.find('DoorNotes').render().html()).toEqual(component.html());
    });

    it('deleteNote',()=>{
        expect(doorNotes.node.deleteNote(1)).toEqual(undefined);
    });

    it('editNote',()=>{
        doorNotes.node.editNote({note:'test',id:1,},()=>{
            expect(doorNotes.node.props.service.addDoorNoteOpened).toEqual(false);
            expect(doorNotes.node.props.service.selectedNote).toEqual({note: 'test',
                id: 1,
                orderId: props.orderId,
                doorId: props.door.id});
        });
    });

    it('onClick Edit',()=>{
        const ServiceButton = doorNotes.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(0).simulate('click',()=>{
            expect(doorNotes.node.props.service.addDoorNoteOpened).toEqual(false);
            expect(doorNotes.node.props.service.selectedNote).toEqual({note: 'test note',
                id: 1,
                orderId: props.orderId,
                doorId: props.door.id});
        });
    });

    it('onClick Delete',()=>{
        const ServiceButton = doorNotes.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(1).simulate('click')
    });

    it('rendered component',()=>{
        expect(doorNotes.find('.nootes-door')).toHaveLength(1);
        expect(doorNotes.find('.bottom_border')).toHaveLength(props.door.notes.length);
        expect(doorNotes.find('.door_margin')).toHaveLength(props.door.notes.length);
        expect(doorNotes.find('.notes_section')).toHaveLength(props.door.notes.length);
       expect(doorNotes.find('.notes_section').at(0).text()).toEqual('Notes');
        expect(doorNotes.find('.notes_text')).toHaveLength(props.door.notes.length);
        expect(doorNotes.find('.notes_text').at(0).text()).toEqual('test note');
        expect(doorNotes.find('ButtonToolbar')).toHaveLength(1);
        expect(doorNotes.find('ServiceButton')).toHaveLength(2);
    });

});