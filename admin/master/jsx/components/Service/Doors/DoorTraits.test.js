import React from 'react';
import DoorTraits from './DoorTraits';
import {shallow} from 'enzyme'

describe("<DoorTraits/>",()=> {

    let component,props={
        traits:[{name:'test1',trait_value:'0'},{name:'test2',trait_value:'0'}]
    };

    beforeEach(() => {
        component = shallow(<DoorTraits {...props} />);
    });

    it('length', () => {
        expect(component).toHaveLength(1);
    });

    it('expected html',()=>{

        const html = component.html();

       let expectedHtml = `<div>`;

       props.traits.map((trait)=>{
           expectedHtml += `<div class="door-traits"><span class="trait-type">${trait.name}</span> : ${trait.trait_value}</div>`
       });

        expectedHtml += '</div>';

        expect(html).toEqual(expectedHtml);

    });

    it('rendered component',()=>{
        expect(component.find('.door-traits')).toHaveLength(props.traits.length);
        expect(component.find('.trait-type')).toHaveLength(props.traits.length);
    });

});