import React from 'react';
import DoorCustomerInfo from './DoorCustomerInfo';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{currentOrder:{id:1}}
};
const mockStore = configureStore();
let store;

describe("<DoorCustomerInfo/>",()=> {

    let component, doorCustomerForm;

    const props = {
        data:{id:1},
        door:{id:1}
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><DoorCustomerInfo {...props} /></Provider>);
        doorCustomerForm = component.find('DoorCustomerInfo');
    });

    it('length', () => {
        expect(doorCustomerForm).toHaveLength(1);
    });

    it('render',()=>{
       expect(component.find('DoorCustomerInfo').render().html()).toEqual(component.html());
    });

    it('deleteCustomer',()=>{
        expect(doorCustomerForm.node.deleteCustomer()).toEqual(undefined);
    });

    it('editCustomer',()=>{
        doorCustomerForm.node.editCustomer(()=>{
            expect(doorCustomerForm.node.props.service.currentPerson).toEqual(props.data);
            expect(doorCustomerForm.node.props.service.selectedDoorId).toEqual(props.door.id);
            expect(doorCustomerForm.node.props.service.personFormOpened).toEqual(true);
            expect(doorCustomerForm.node.props.service.personFormId).toEqual('customer');
        });
    });

    it('onClick Edit',()=>{
        const ServiceButton = doorCustomerForm.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(0).simulate('click',()=>{
            expect(doorCustomerForm.node.props.service.currentPerson).toEqual(props.data);
            expect(doorCustomerForm.node.props.service.selectedDoorId).toEqual(props.door.id);
            expect(doorCustomerForm.node.props.service.personFormOpened).toEqual(true);
            expect(doorCustomerForm.node.props.service.personFormId).toEqual('customer');
        })
    });

    it('onClick Delete',()=>{
        const ServiceButton = doorCustomerForm.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(1).simulate('click')
    });

    it('rendered components',()=>{
       expect(doorCustomerForm.find('.purchaser-info')).toHaveLength(1);
       expect(doorCustomerForm.find('.avatar_')).toHaveLength(1);
        expect(doorCustomerForm.find('.customer_title')).toHaveLength(1);
        expect(doorCustomerForm.find('.notes_section')).toHaveLength(1);
        expect(doorCustomerForm.find('.notes_section').text()).toEqual('Customer Info');
        expect(doorCustomerForm.find('PersonAvatar')).toHaveLength(1);
        expect(doorCustomerForm.find('.headtext')).toHaveLength(1);
        expect(doorCustomerForm.find('PersonInfo')).toHaveLength(1);
        expect(doorCustomerForm.find('ButtonToolbar')).toHaveLength(1);
        expect(doorCustomerForm.find('ServiceButton')).toHaveLength(2);
    });



});