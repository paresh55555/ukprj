import React from 'react';
import DoorButtons from './DoorButtons';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux';

const initialState = {

};

describe('<DoorButtons/>',()=>{

    let component, doorButtons;
    const mockStore = configureStore();
    let store;

    const props = {
        door:{id:1},
        createRequest:true
    };
    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><DoorButtons {...props} /></Provider>);
        doorButtons = component.find('DoorButtons');
    });

    it('length', () => {
        expect(doorButtons).toHaveLength(1);
    });

    it('render',()=>{
        expect(component.find('DoorButtons').render().html()).toEqual(component.html());
    });

    it('openAddNote',()=>{
        doorButtons.node.openAddNote(()=>{
            expect(doorButtons.node.props.service.selectedDoorId).toEqual(1);
            expect(doorButtons.node.props.service.addDoorNoteOpened).toEqual(true)
        });
    });

    it('openAddCustomer',()=>{
       doorButtons.node.openAddCustomer(()=>{
           expect(doorButtons.node.props.service.selectedDoorId).toEqual(1);
           expect(doorButtons.node.props.service.personFormOpened).toEqual(true);
           expect(doorButtons.node.props.service.personFormId).toEqual('customer-'+1);
       });
    });

    it('createJob',()=>{
       expect(doorButtons.node.createJob(1)).toEqual(undefined);
    });

    it('onClick Add Note',()=>{
        const ServiceButton = doorButtons.find('ServiceButton');
        expect(ServiceButton).toHaveLength(3);
        ServiceButton.at(0).simulate('click',()=>{
            expect(doorButtons.node.props.service.selectedDoorId).toEqual(1);
            expect(doorButtons.node.props.service.addDoorNoteOpened).toEqual(true)
        });
    });

    it('onClick Attach Customer',()=>{
        const ServiceButton = doorButtons.find('ServiceButton');
        expect(ServiceButton).toHaveLength(3);
        ServiceButton.at(1).simulate('click',()=>{
            expect(doorButtons.node.props.service.selectedDoorId).toEqual(1);
            expect(doorButtons.node.props.service.personFormOpened).toEqual(true);
            expect(doorButtons.node.props.service.personFormId).toEqual('customer-'+1);
        });
    });

    it('onClick Create Service Request',()=>{
        const ServiceButton = doorButtons.find('ServiceButton');
        expect(ServiceButton).toHaveLength(3);
        ServiceButton.at(2).simulate('click');
    });

    it('rendered component',()=>{
       expect(doorButtons.find('.text-center')).toHaveLength(5);
       expect(doorButtons.find('.customer_btn')).toHaveLength(2);
       expect(doorButtons.find('ButtonToolbar')).toHaveLength(1);
       expect(doorButtons.find('ServiceButton')).toHaveLength(3);
    });

    it('when no create request',()=>{
        props.createRequest = false;
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><DoorButtons {...props} /></Provider>);
        doorButtons = component.find('DoorButtons');

        expect(doorButtons.find('.text-center')).toHaveLength(3);
        expect(doorButtons.find('ServiceButton')).toHaveLength(2);
        expect(doorButtons.find('ButtonToolbar')).toHaveLength(0);
    });


});
