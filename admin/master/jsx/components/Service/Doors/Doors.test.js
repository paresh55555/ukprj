import React from 'react';
import Doors from './Doors';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{
        currentOrder:{id:1}
    }
};
const mockStore = configureStore();
let store;

describe("<Doors/>",()=> {

    let component, doors;

    const props = {
        createRequest:true
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><Doors {...props} /></Provider>);
        doors = component.find('Doors');
    });

    it('length', () => {
        expect(doors).toHaveLength(1);
    });

    it('render',()=>{
       expect(component.find('Doors').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       expect(doors.node.componentWillMount()).toEqual(undefined);
    });

    it('componentWillUnmount',()=>{
        doors.node.componentWillUnmount(()=>{
            expect(doors.node.props.service.doors).toEqual([]);
        });
    });

    it('openAddDoor',()=>{
        doors.node.openAddDoor(()=>{
            expect(doors.node.props.service.currentDoor).toEqual(null);
            expect(doors.node.props.service.addDoorOpened).toEqual(true);
        });
    });

    it('rendered component',()=>{
        expect(doors.find('ServiceFormRow')).toHaveLength(1);
        expect(doors.find('AddDoorNote')).toHaveLength(1);
    });

    it('when no currentOrder',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,currentOrder:false}}));
        component = mount(<Provider
            store={store}><Doors {...props} /></Provider>);
        doors = component.find('Doors');
        expect(doors.node.componentWillMount()).toEqual(undefined);
    });

});