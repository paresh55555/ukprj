import React from 'react';
import { connect } from 'react-redux';
import ServiceButton from '../ServiceButton';
import { setCurrentDoor, toggleAddDoor } from '../../../actions/service';

class AddDoorButton extends React.Component {

    openAddDoor = () => {
        this.props.dispatch(setCurrentDoor(null))
        this.props.dispatch(toggleAddDoor(true))
    }

    renderAddButton () {
        const {doors} = this.props.service

        let props = {}

        if (doors & doors.length) {

            props.className = 'pull-right'
            props.label = 'Add Door'

        } else {

            props.fullWidth = true
            props.label = 'Add Another Door'

        }

        return <ServiceButton {...props} onClick={this.openAddDoor}/>

    }

    render () {

        return this.renderAddButton()

    }

}

AddDoorButton.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddDoorButton)