import React from 'react';
import ServiceButton from "../../Service/ServiceButton";

export default function CustomerButton(props) {
    
    return (
        <div className="text-center">
            <div className="text-center">
                {props.label ? <ServiceButton  onClick={props.onclick} label={props.label} className={'customer_btn full-width'}/>:''}
            </div>
        </div>
    );
}