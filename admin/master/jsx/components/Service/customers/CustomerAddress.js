import React, {Component} from 'react';
import {connect} from "react-redux";
import {toggleCustomerAddress} from "../../../actions/serviceCustomer";
import * as ApiServiceCustomer from "../../../api/ApiServiceCustomer";
import {Row, Col, ButtonToolbar,FormControl,Image, Form} from "react-bootstrap";
import ServiceButton from "../ServiceButton";
import FontIcon from "../../Common/FontIcon";
import ServiceFormRow from "../ServiceFormRow";
import AddressModal from './AddressModal';
import ServiceAddressMap from '../Jobs/ServiceAddressMap';
import { setCurrentAddress } from '../../../actions/addresses';

class CustomerAddress extends Component {

    state = {
        title:'Add Address',
        center: {
            lat: 37.7749,
            lng: -122.4194
        },
        buttonLabel:'Add'
    }

    editAddress = address => {
        this.props.dispatch(toggleCustomerAddress(true, 'editAddress' + address.id))
        this.props.dispatch(setCurrentAddress(address))
    }

    deleteAddress = address => {
        let {currentCustomer} = this.props.serviceCustomer
        ApiServiceCustomer.ApiDeleteCustomerAddress(currentCustomer.id, address.id)
            .then(
                response => {
                    swal("Deleted!", "Deleted address.", "success");
                    ApiServiceCustomer.ApiGetCustomersById(currentCustomer.id);
                }
            )
    }

    attachAddress = address =>{
        let {currentCustomer} = this.props.serviceCustomer
        ApiServiceCustomer.ApiUpdateCustomerAddress(currentCustomer.id, address)
            .then(
                response => {
                    swal("Updated!", "Address updated.", "success");
                    ApiServiceCustomer.ApiGetCustomersById(currentCustomer.id);
                    this.closeAddressModal();
                }
            )
    }

    closeAddressModal = () =>{
        this.props.dispatch(toggleCustomerAddress(false, ''))
    }

  
    renderAdress = () =>{

        let {address} = this.props
        let {isOpen} = this.props.serviceCustomer

        let {title, buttonLabel} = this.state
        let AddressData = address.address_1||address.address_2 + ',' + address.city + ',' + address.state + address.zip;
        
        return (
            <div className='doors-list door address'>
                <div >
                    <Row className='door_margin'>
                        <Col md={7}>
                            <div className="notes_text"><FontIcon iconClass={'icon icon-location-pin'}/>{AddressData}</div>
                        </Col>
                        <Col md={5}>
                            <ButtonToolbar className={'buttons pull-right'}>
                                <ServiceButton label={'Edit'} onClick={() => this.editAddress(address)}/>
                                <ServiceButton label={'Delete'} onClick={() => this.deleteAddress(address)}/>

                            </ButtonToolbar>
                        </Col>
                    </Row>
                    <AddressModal 
                        title='Update Address'
                        isOpen={isOpen}
                        close={this.closeAddressModal}
                        buttonLabel={'Update'}
                        onSave={this.attachAddress}
                        id={'editAddress' + address.id}
                    />
                </div>
                <ServiceAddressMap address={address} className='addressMap' />
            </div>
        )
    }

    render() {
        return (
            <div className={'Address_es'}>
                {this.renderAdress()}
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
    serviceCustomer: state.serviceCustomer
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerAddress)