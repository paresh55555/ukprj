import React from 'react';
import CustomerFrom from './CustomerFrom';
import {mount} from 'enzyme'
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceCustomer:{
        isOpen:true,
        currentCustomer:{id:1}
    },
    addresses:{addressList:[]}
};
const mockStore = configureStore();
let store;

describe("<CustomerFrom/>",()=> {

    let component, customerForm;

    const props = {
        customerOrders:[],
        customerJobs:[],
        isNewCustomer:false,
        gotoServiceJob:jest.fn(),
        onCancel:jest.fn(),
        customer:{
            id:1,
            first_name:'test',
            last_name:'test',
            email:'test@test.com',
            phone:'012345',
            address_list:[]
        },
        onSave:jest.fn()
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><CustomerFrom {...props} /></Provider>);
        customerForm = component.find('CustomerFrom');
    });

    it('length', () => {
        expect(customerForm).toHaveLength(1);
    });

    it('componentWillReceiveProps',()=>{
        customerForm.node.componentWillReceiveProps(props);
        expect(customerForm.node.state.customer).toEqual(props.customer);
        customerForm.node.componentWillReceiveProps({
            customerOrders:[],
            customerJobs:[],
            isNewCustomer:false,
            gotoServiceJob:jest.fn(),
            onCancel:jest.fn(),
            customer:{
                id:2,
                first_name:'test1',
                last_name:'test1',
                email:'test@test.com',
                phone:'012345',
                address_list:[]
            }
        });
        expect(customerForm.node.state.customer).toEqual({
            id:2,
            first_name:'test1',
            last_name:'test1',
            email:'test@test.com',
            phone:'012345',
            address_list:[]
        });
    });

    it('componentDidMount',()=>{
       customerForm.node.componentDidMount();
       expect(customerForm.node.state.customer).toEqual(props.customer);
    });

    it('render',()=>{
        expect(component.find('CustomerFrom').render().html()).toEqual(customerForm.html());
    });

    it('onChangeCustomer',()=>{
        customerForm.node.onChangeCustomer('first_name')({target:{value:''}});
        expect(customerForm.node.state.customer.first_name).toEqual('');
        expect(customerForm.node.state.errors.first_name).toEqual('First Name is required');
        customerForm.node.onChangeCustomer('first_name')({target:{value:'tes'}});
        expect(customerForm.node.state.customer.first_name).toEqual('tes');
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        customerForm.node.onChangeCustomer('last_name')({target:{value:''}});
        expect(customerForm.node.state.customer.last_name).toEqual('');
        expect(customerForm.node.state.errors.last_name).toEqual('Last Name is required');
        customerForm.node.onChangeCustomer('last_name')({target:{value:'tes'}});
        expect(customerForm.node.state.customer.last_name).toEqual('tes');
        expect(customerForm.node.state.errors.last_name).toEqual(null);
        customerForm.node.onChangeCustomer('email')({target:{value:''}});
        expect(customerForm.node.state.customer.email).toEqual('');
        expect(customerForm.node.state.errors.email).toEqual('Email is required');
        customerForm.node.onChangeCustomer('email')({target:{value:'tes'}});
        expect(customerForm.node.state.customer.email).toEqual('tes');
        expect(customerForm.node.state.errors.email).toEqual('Please provide a valid email');
        customerForm.node.onChangeCustomer('email')({target:{value:'test@test.com'}});
        expect(customerForm.node.state.customer.email).toEqual('test@test.com');
        expect(customerForm.node.state.errors.email).toEqual(null);
        customerForm.node.onChangeCustomer('phone')({target:{value:''}});
        expect(customerForm.node.state.customer.phone).toEqual('');
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.onChangeCustomer('phone')({target:{value:'012345'}});
        expect(customerForm.node.state.customer.phone).toEqual('012345');
        expect(customerForm.node.state.errors.phone).toEqual(null);
    });

    it('getFieldTitle',()=>{
        expect(customerForm.node.getFieldTitle('test')).toEqual('Test');
        expect(customerForm.node.getFieldTitle('first_name')).toEqual('First Name');
    });

    it('validateField',()=>{
       customerForm.node.validateField('','test');
       expect(customerForm.node.state.errors).toEqual({});
       customerForm.node.validateField('','first_name');
       expect(customerForm.node.state.errors.first_name).toEqual('First Name is required');
        customerForm.node.validateField('test','first_name');
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        customerForm.node.validateField('','last_name');
        expect(customerForm.node.state.errors.last_name).toEqual('Last Name is required');
        customerForm.node.validateField('test','last_name');
        expect(customerForm.node.state.errors.last_name).toEqual(null);
        customerForm.node.validateField('','email');
        expect(customerForm.node.state.errors.email).toEqual('Email is required');
        customerForm.node.validateField('test','email');
        expect(customerForm.node.state.errors.email).toEqual('Please provide a valid email');
        customerForm.node.validateField('test@test.com','email');
        expect(customerForm.node.state.errors.email).toEqual(null);
        customerForm.node.validateField('','phone');
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.validateField('0','phone');
        expect(customerForm.node.state.errors.phone).toEqual(null);
    });

    it('validForm',()=>{

        customerForm.node.setState({customer:{first_name:'',
            last_name:'',
            email:'',
            phone:''}});
        expect(customerForm.node.validForm()).toEqual(false);
        expect(customerForm.node.state.errors.first_name).toEqual('First Name is required');
        expect(customerForm.node.state.errors.last_name).toEqual('Last Name is required');
        expect(customerForm.node.state.errors.email).toEqual('Email is required');
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.setState({customer:{first_name:'test',
            last_name:'',
            email:'',
            phone:''}});
        expect(customerForm.node.validForm()).toEqual(false);
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        expect(customerForm.node.state.errors.last_name).toEqual('Last Name is required');
        expect(customerForm.node.state.errors.email).toEqual('Email is required');
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.setState({customer:{first_name:'test',
            last_name:'test',
            email:'',
            phone:''}});
        expect(customerForm.node.validForm()).toEqual(false);
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        expect(customerForm.node.state.errors.last_name).toEqual(null);
        expect(customerForm.node.state.errors.email).toEqual('Email is required');
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.setState({customer:{first_name:'test',
            last_name:'test',
            email:'test',
            phone:''}});
        expect(customerForm.node.validForm()).toEqual(false);
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        expect(customerForm.node.state.errors.last_name).toEqual(null);
        expect(customerForm.node.state.errors.email).toEqual('Please provide a valid email');
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.setState({customer:{first_name:'test',
            last_name:'test',
            email:'test@test.com',
            phone:''}});
        expect(customerForm.node.validForm()).toEqual(false);
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        expect(customerForm.node.state.errors.last_name).toEqual(null);
        expect(customerForm.node.state.errors.email).toEqual(null);
        expect(customerForm.node.state.errors.phone).toEqual('Phone is required');
        customerForm.node.setState({customer:{first_name:'test',
            last_name:'test',
            email:'test@test.com',
            phone:'0'}});
        expect(customerForm.node.validForm()).toEqual(true);
        expect(customerForm.node.state.errors.first_name).toEqual(null);
        expect(customerForm.node.state.errors.last_name).toEqual(null);
        expect(customerForm.node.state.errors.email).toEqual(null);
        expect(customerForm.node.state.errors.phone).toEqual(null);
    });

    it('onSave',()=>{
        customerForm.node.setState({customer:{first_name:'',
            last_name:'',
            email:'',
            phone:''}});
       customerForm.node.onSave({});
        expect(props.onSave).not.toHaveBeenCalled();
        customerForm.node.setState({customer:{first_name:'test',
            last_name:'test',
            email:'test@test.com',
            phone:'0'}});
        customerForm.node.onSave({});
        expect(props.onSave).toHaveBeenCalled();
    });

    it('closeAddressModal',()=>{
        customerForm.node.closeAddressModal(()=>{
            expect(customerForm.node.props.serviceCustomer.isOpen).toEqual(false);
            expect(customerForm.node.props.serviceCustomer.identifier).toEqual('');
        });
    });

    it('AddAddress',()=>{
        customerForm.node.AddAddress(()=>{
            expect(customerForm.node.props.serviceCustomer.isOpen).toEqual(true);
            expect(customerForm.node.props.serviceCustomer.identifier).toEqual('addAddress');
            expect(customerForm.node.props.addresses.currentAddress).toEqual({});
        });
    });

    it('attachAddress',()=>{
        expect(customerForm.node.attachAddress()).toEqual(undefined);
    });

    it('rendered component',()=>{
       expect(customerForm.find('.add-order')).toHaveLength(1);
       expect(customerForm.find('ServiceFormRow')).toHaveLength(7);
       expect(customerForm.find('UpdateButton')).toHaveLength(1);
       expect(customerForm.find('CustomerButton')).toHaveLength(props.customerJobs.length+props.customerOrders.length);
       expect(customerForm.find('ServiceButton')).toHaveLength(2);
        expect(customerForm.find('AddressModal')).toHaveLength(1);
        expect(customerForm.find('AddressList')).toHaveLength(1);
        expect(customerForm.find('.no-pd')).toHaveLength(4);

    });

    it('when newCustomer',()=>{
        props.isNewCustomer = true;
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><CustomerFrom {...props} /></Provider>);
        customerForm = component.find('CustomerFrom');

        expect(customerForm.find('ServiceFormRow')).toHaveLength(3);
    });

});