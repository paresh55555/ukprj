import React, {Component} from 'react';
import {Col,ButtonToolbar} from "react-bootstrap";
import ServiceFormRow from "../ServiceFormRow";
import ServiceButton from "../../Service/ServiceButton";

class UpdateButton extends Component {
    renderCustomerButtons(){

        return(
            <Col sm ={12}>
               <div className="formbtns">
                    <ButtonToolbar className={'buttons'}>
                        <ServiceButton onClick={this.props.addCustomer} className="btn btn-primary" label={`${ this.props.isNewCustomer ? 'Add' : 'Update'} Customer`}/>
                        { this.props.isNewCustomer ? <ServiceButton onClick={this.props.onCancel} className="btn" label={'Cancel' }/> : ''}
                    </ButtonToolbar>
                </div>
            </Col>
        )
    }
    render(){

        return(
            <ServiceFormRow>
                <Col sm={12}>       
                   {this.renderCustomerButtons()}
                </Col>
            </ServiceFormRow>
        )
    }
}

export default UpdateButton;