import React, { Component } from 'react';
import {connect} from "react-redux";
import UpdateButton from './UpdateButton';
import ServiceFormRow from "../ServiceFormRow";
import {FormControl,Col,ButtonToolbar ,Form, Row, FormGroup, HelpBlock} from "react-bootstrap";
import CustomerAddress from './CustomerAddress';
import ServiceAddressMap from '../Jobs/ServiceAddressMap';
import CustomerButton from './CustomerButton';
import AddressList from '../../Common/Addresses/AddressList';
import ServiceButton from '../ServiceButton';
import AddressModal from './AddressModal';
import {toggleCustomerAddress} from "../../../actions/serviceCustomer";
import * as ApiServiceCustomer from "../../../api/ApiServiceCustomer";
import { setCurrentAddress } from '../../../actions/addresses';

const VALIDATE_FIELDS = ['first_name', 'last_name', 'phone', 'email']

class CustomerFrom extends Component {

    state = {
        rendered: false,
        Jobs:[],
        errors: {},
        customer:{
            first_name:'',
            last_name:'',
            email:'',
            phone:'',
            address_list:[]
        }
    }

    componentWillReceiveProps (nextProps) {
        if(nextProps.customer.id !== this.props.customer.id) {
            this.setState({
                customer: nextProps.customer
            })
        }
    }

    componentDidMount () {
        this.setState({
            customer: this.props.customer
        })
    }
 
    onChangeCustomer = name => e => {

        this.validateField(e.target.value, name)

         this.setState({
            customer: {
                ...this.state.customer,
                [name]: e.target.value
            }
        })
    }

    attachAddress = address =>{
        let {currentCustomer} = this.props.serviceCustomer
        ApiServiceCustomer.ApiAddCustomerAddress(currentCustomer.id, address)
            .then(
                response => {
                    swal("Added!", "Added new Address.", "success");
                    ApiServiceCustomer.ApiGetCustomersById(currentCustomer.id);
                    this.closeAddressModal();
                }
            )
    }

    AddAddress = () => {
        this.props.dispatch(toggleCustomerAddress(true, 'addAddress'))
        this.props.dispatch(setCurrentAddress({}))
    }

    closeAddressModal = () =>{
        this.props.dispatch(toggleCustomerAddress(false, ''))
    }

    onSave = customer => {
        if(this.validForm()) {
            this.props.onSave(customer)
        }
    }

    onChangeNote = (value) => {
        this.setState({note: value})
    }

    validForm = () => {
        VALIDATE_FIELDS.forEach(field => {
            this.validateField(this.state.customer[field], field);
        })
        let errors = Object.keys(this.state.errors).filter((field) => {
            return !!this.state.errors[field]
        })
        console.log('hello')
        return !errors.length
    }

    validateField = (value, field) => {

        if (VALIDATE_FIELDS.indexOf(field) !== -1) {
            this.state.errors[field] = !value ? this.getFieldTitle(field) + ' is required' : null;
        }

        if (field == 'email' && !this.state.errors[field]) {

            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value.toLowerCase())) {
                this.state.errors[field] = 'Please provide a valid email'
            }

        }

        this.setState({errors: this.state.errors})
    }

    getFieldTitle(field) {
        let splitted = field.split('_')
        splitted[0] = splitted[0].charAt(0).toUpperCase() + splitted[0].substr(1).toLowerCase()
        if (field === 'first_name' || field === 'last_name') {
            splitted[1] = splitted[1].charAt(0).toUpperCase() + splitted[1].substr(1).toLowerCase()
        }

        return splitted.join(' ')
    }

    renderFormField = (field, size, className) => {

        const { customer } = this.state

        return (
            <Col key={field} sm={size} className={`no-pd innerJob ${className}`}>
                <FormGroup validationState={this.state.errors[field] ? "error" : null}>
                    <FormControl
                        type='text'
                        placeholder={this.getFieldTitle(field)}
                        onChange={this.onChangeCustomer(field)}
                        value={customer[field] || ''}
                    />
                    <HelpBlock>{this.state.errors[field]}</HelpBlock>
                </FormGroup>
            </Col>
        )
    }

    render() {
        
        const { customer } = this.state;
        const {customerOrders,customerJobs ,gotoServiceJob, isNewCustomer, onCancel} = this.props;
        let {isOpen} = this.props.serviceCustomer

        return (
                <Form className={'add-order'}>
                    <ServiceFormRow label={'Customer Info'}>
                        {this.renderFormField('first_name', 6, 'no-pd pull-left innerJob')}
                        {this.renderFormField('last_name', 6, 'no-pd pull-right innerJob')}
                    </ServiceFormRow>
                    <ServiceFormRow label={''}>
                        {this.renderFormField('email', 6, 'no-pd pull-left innerJob')}
                        {this.renderFormField('phone', 6, 'no-pd pull-right innerJob')}
                    </ServiceFormRow>
                    <UpdateButton onCancel={onCancel} isNewCustomer={isNewCustomer} addCustomer={() => this.onSave(customer)}/>
                    { !isNewCustomer ?
                        <div>
                            <ServiceFormRow label={'Address(es)'}>
                                <AddressList />
                            </ServiceFormRow>
                            <ServiceFormRow label={''}>
                                <ServiceButton className='pull-right' label='Add Address' onClick={this.AddAddress}/>
                                <AddressModal 
                                    title='Add Address'
                                    isOpen={isOpen}
                                    buttonLabel={'Attach'}
                                    close={this.closeAddressModal}
                                    onSave={this.attachAddress}
                                    id="addAddress"
                                />
                            </ServiceFormRow>
                            <ServiceFormRow label={'Orders'}>
                                {customerOrders && customerOrders.map(order =>{
                                    return (<CustomerButton label={order.order_number || 'N/A'} onclick ={() => this.props.gotoServiceOrder(order)}/>)
                                })}
                                {
                                    !customerOrders.length && 'No Orders'
                                }
                            </ServiceFormRow>
                            <ServiceFormRow label={'Jobs'}>
                                {customerJobs && customerJobs.map(jobs =>{
                                    return(<CustomerButton label={jobs.job_number || 'N/A'} onclick ={() => gotoServiceJob(jobs)}/>)
                                })}
                                {
                                    !customerJobs.length && 'No Jobs'
                                }
                            </ServiceFormRow>
                        </div> 
                    : null }
                </Form>
           
        )
    }
}

CustomerFrom.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    serviceCustomer: state.serviceCustomer
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(CustomerFrom)