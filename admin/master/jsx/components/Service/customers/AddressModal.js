import React, {Component} from 'react';
import {connect} from "react-redux";
import ServiceFormRow from "../ServiceFormRow";
import * as ApiServiceCustomer from "../../../api/ApiServiceCustomer";
import {setAddressList, setCurrentAddress} from "../../../actions/addresses";
import {Row,Col,FormControl,Image, Form,ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../../Service/ServiceButton";
import AddressFormContainer from '../../Common/Addresses/AddressFormContainer';
import SQModal from '../../Common/SQModal/SQModal';

class AddressModal extends Component {
    render(){
        let { close, buttonLabel, onSave ,isOpen, title, id} = this.props;
        const { identifier } = this.props.serviceCustomer

        return(

            <SQModal title={title} className={'service-modal'}
                isOpen={isOpen && identifier == id}
                onClose={close}
                customButtons>
                    <Form className={'service'}>
                        {/* <div className={'form-divider'}></div> */}
                        <AddressFormContainer
                            renderButtons={
                                (currentAddress, validate) => {
                                    return (
                                        <ServiceFormRow label={''}>  
                                            <ButtonToolbar className={'buttons'}>
                                                <ServiceButton onClick={() => {
                                                    if(validate()) {
                                                        onSave(currentAddress)
                                                    }
                                                }} className="btn btn-primary" label={buttonLabel}/>
                                                <ServiceButton  onClick={close} className="btn" label={'Cancel'}/>
                                            </ButtonToolbar>
                                        </ServiceFormRow>
                                    )
                                }
                            }
                        />
                    </Form>
            </SQModal>
        )
    }
}

const mapStateToProps = (state) => ({
    serviceCustomer: state.serviceCustomer
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddressModal);