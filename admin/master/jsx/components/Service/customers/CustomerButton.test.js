import React from 'react';
import CustomerButton from './CustomerButton';
import {shallow} from 'enzyme';

describe('<CustomerButton/>',()=>{

    let component,props={
        label:'test label',
        onclick:jest.fn()
    };

    beforeEach(()=>{
        component = shallow(<CustomerButton {...props}/>);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('onClick ServiceButton',()=>{
       const ServiceButton = component.find('.text-center > .text-center > .customer_btn');
       expect(ServiceButton).toHaveLength(1);
       ServiceButton.simulate('click');
       expect(props.onclick).toHaveBeenCalled();
    });

    it('rendered component',()=>{
        expect(component.find('.text-center')).toHaveLength(2);
        expect(component.find('.text-center > .text-center > .customer_btn')).toHaveLength(1);
        expect(component.find('.text-center > .text-center > .customer_btn').props().label).toEqual(props.label);
    });

    it('when no label',()=>{
       props.label = null;
       component = shallow(<CustomerButton {...props}/>);
       expect(component.find('.text-center > .text-center > .customer_btn')).toHaveLength(0);
    });



});