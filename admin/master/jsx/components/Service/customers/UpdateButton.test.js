import React from 'react';
import UpdateButton from './UpdateButton';
import {shallow} from 'enzyme';
describe('<UpdateButton/>',()=>{

    let component,props={
        isNewCustomer:false,
        onCancel:jest.fn(),
        addCustomer:jest.fn()

    };
     beforeEach(()=>{
         component = shallow(<UpdateButton {...props}/>);
     });

     it('length',()=> {
         expect(component).toHaveLength(1);
     });

     it('onClick ServiceButton',()=>{
         const ServiceButton = component.find('.formbtns > .buttons > .btn-primary');
         expect(ServiceButton).toHaveLength(1);
         ServiceButton.simulate('click');
         expect(props.addCustomer).toHaveBeenCalled();
     });

    it('rendered component',()=>{
        expect(component.find('Col')).toHaveLength(2);
        expect(component.find('.formbtns')).toHaveLength(1);
        expect(component.find('.formbtns > .buttons')).toHaveLength(1);
        expect(component.find('.formbtns > .buttons > .btn-primary')).toHaveLength(1);
        expect(component.find('.formbtns > .buttons > .btn-primary').props().label).toEqual('Update Customer');
    });

    it('When newCustomer',()=>{
        props.isNewCustomer = true;
        component = shallow(<UpdateButton {...props}/>);
        expect(component.find('.formbtns > .buttons > .btn-primary').props().label).toEqual('Add Customer');

        const btn = component.find('.formbtns > .buttons > .btn');
        expect(btn).toHaveLength(2);

        btn.at(1).simulate('click');
        expect(props.onCancel).toHaveBeenCalled();

    })

});
