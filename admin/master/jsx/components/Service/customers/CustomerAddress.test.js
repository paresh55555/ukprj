import React from 'react';
import CustomerAddress from './CustomerAddress';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceCustomer:{
        isOpen:true,
        currentCustomer:{id:1}
    },
    addresses:{addressList:[]}
};
const mockStore = configureStore();
let store;

describe("<CustomerAddress/>",()=> {

    let component, customerAddress;

    const props = {
        address:{address_1:'address 1 test',address_2:'address 2 test',city:'test city',state:'test state',zip:'000000'}
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><CustomerAddress {...props} /></Provider>);
        customerAddress = component.find('CustomerAddress');
    });

    it('length', () => {
        expect(customerAddress).toHaveLength(1);
    });

    it('render',()=>{
        expect(component.find('CustomerAddress').render().html()).toEqual(component.html());
    });

    it('editAddress',()=>{
        customerAddress.node.editAddress({id:1},()=>{
            expect(customerAddress.node.props.serviceCustomer).toEqual({open:true,identifier:'editAddress' + 1});
            expect(customerAddress.node.props.addresses.currentAddress).toEqual({id:1});
        });
    });

    it('deleteAddress',()=>{
        expect(customerAddress.node.deleteAddress({id:1})).toEqual(undefined);
    });

    it('attachAddress',()=>{
       expect(customerAddress.node.attachAddress({id:1})).toEqual(undefined);
    });

    it('closeAddressModal',()=>{
        customerAddress.node.closeAddressModal(()=>{
            expect(customerAddress.node.props.serviceCustomer).toEqual({open:false,identifier:''});
        });
    });

    it('rendered Html',()=>{
       expect(customerAddress.find('.Address_es')).toHaveLength(1);
       expect(customerAddress.find('.Address_es > .doors-list')).toHaveLength(1);
       expect(customerAddress.find('.door_margin')).toHaveLength(1);
       expect(customerAddress.find('.notes_text')).toHaveLength(1);
       expect(customerAddress.find('.buttons')).toHaveLength(1);
       expect(customerAddress.find('ServiceButton')).toHaveLength(2);
       expect(customerAddress.find('AddressModal')).toHaveLength(1);
       expect(customerAddress.find('ServiceAddressMap')).toHaveLength(1);
    });

    it('onClick Edit',()=>{

        const ServiceButton = customerAddress.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(0).simulate('click',{id:1});

    });

    it('onClick Delete',()=>{

        const ServiceButton = customerAddress.find('ServiceButton');
        expect(ServiceButton).toHaveLength(2);
        ServiceButton.at(1).simulate('click',{id:1});

    });

});