import React from 'react';
import AddressModal from './AddressModal';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    serviceCustomer:{
        identifier:1
    },
    addresses:{addressList:[],currentAddress:{address_1:'address 1 test',address_2:'address 2 test'}}
};
const mockStore = configureStore();
let store;

describe("<AddressModal/>",()=> {

    let component, addressModal;

    const props = {
        close:jest.fn(),
        buttonLabel:'test label',
        onSave:jest.fn(),
        isOpen:true,
        title:'test title',
        id:1
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddressModal {...props} /></Provider>);
        addressModal = component.find('AddressModal');
    });

    it('length', () => {
        expect(addressModal).toHaveLength(1);
    });

    it('render',()=>{
       expect(component.find('AddressModal').render().html()).toEqual(component.html());
    });

});