import React, {PropTypes} from 'react';

export default function PersonAvatar(props) {

    //This needs refactoring the passing of Data object. Only label should be passed with a max of 2 characters
    let first;
    let last;

    if (props.info.first_name) {
        first = props.info.first_name.charAt(0);
    }

    if (props.info.last_name) {
        last = props.info.last_name.charAt(0);
    }

    return (
        <div className={'avatar img-circle'}>
            <span>{first}{last}</span>
        </div>
    );
}

PersonAvatar.propTypes = {
    info: PropTypes.object.isRequired
};