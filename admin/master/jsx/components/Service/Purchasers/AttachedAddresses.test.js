import React from 'react';
import AttachedAddresses from './AttachedAddresses';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{orderPurchasers:[]}
};
const mockStore = configureStore();
let store;

describe('<AttachedAddresses/>',()=> {

    let component, attachedAddresses;

    const props = {
        orderId: 1
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AttachedAddresses {...props} /></Provider>);
        attachedAddresses = component.find('AttachedAddresses');
    });

    it('length', () => {
        expect(attachedAddresses).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('AttachedAddresses').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
       expect(attachedAddresses.node.componentWillMount()).toEqual(undefined);
    });

    it('onClick',()=>{
       attachedAddresses.node.onClick({},()=>{
           expect(attachedAddresses.node.props.service.currentPerson).toEqual({});
       });
    });

    it('renderAddresses',()=>{
        expect(attachedAddresses.node.renderAddresses()).toEqual('');
    });

    it('rendered component',()=>{
        expect(attachedAddresses.find('.attached-addresses')).toHaveLength(1);
        expect(attachedAddresses.find('.clearfix')).toHaveLength(2);
        expect(attachedAddresses.find('.header')).toHaveLength(1);
        expect(attachedAddresses.find('.form-divider')).toHaveLength(1);
    });

    it('when no orderPurchasers',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,orderPurchasers:undefined}}));
        component = mount(<Provider
            store={store}><AttachedAddresses {...props} /></Provider>);
        attachedAddresses = component.find('AttachedAddresses');

        expect(attachedAddresses.node.renderAddresses()).toEqual('');
    });

    it('when orderPurchasers',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,orderPurchasers:[{}]}}));
        component = mount(<Provider
            store={store}><AttachedAddresses {...props} /></Provider>);
        attachedAddresses = component.find('AttachedAddresses');

        expect(attachedAddresses.find('.attached-item')).toHaveLength(1);
        expect(attachedAddresses.find('.name')).toHaveLength(1);
        expect(attachedAddresses.find('PersonAddress')).toHaveLength(1);

        attachedAddresses.find('.attached-item').simulate('click',{},()=>{
            expect(attachedAddresses.node.props.service.currentPerson).toEqual({});
        });
    });

});