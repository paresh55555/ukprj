import React, {PropTypes} from 'react';

export default function PersonAddress(props) {
    let address = '',
        {info} = props
    if (info.address_1) {
        address += info.address_1
    }
    if (info.address_2) {
        address += ' ' + info.address_2
    }
    if (info.city) {
        address += ', ' + info.city
    }
    if (info.state) {
        address += ', ' + info.state
    }
    if (info.zip) {
        address += ' ' + info.zip
    }
    return (
        <span>{address}</span>
    );
}

PersonAddress.propTypes = {
    info: PropTypes.object.isRequired
};