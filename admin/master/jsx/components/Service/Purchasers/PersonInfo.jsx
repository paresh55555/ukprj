import React, {Component, PropTypes} from 'react';
import FontIcon from "../../Common/FontIcon";
import PersonAddress from "./PersonAddress";

export default class PersonInfo extends Component {
    getName() {
        let {info} = this.props


        let name = info.first_name||info.last_name ? info.first_name +  ' ' +info.last_name : ''

        return <div className={'name'}>{name}</div>
    }

    getPhoneAndMail() {
        let {info} = this.props

        return (
            <div>
                <span className={'email-phone'}><FontIcon iconClass={'icon icon-envelope'}/>{info.email}</span>
                <span className={'email-phone'}><FontIcon iconClass={'icon icon-phone'}/>{info.phone}</span>
            </div>
        )
    }

    getAddress() {
        return (
            <div className={'address'}>
                <FontIcon iconClass={'icon icon-location-pin'}/><PersonAddress info={this.props.info}/>
            </div>
        )
    }

    getText() {
        if(this.props.info.note){
            return (
                <div className={'text_content'}>
                   {this.props.info.note}
                </div>
            )
        }   
    }

    render() {
        return (
            <div className={'info'}>
                {this.getName()}
                {this.getPhoneAndMail()}
                {this.getAddress()}
                {this.getText()}
            </div>
        );
    }
}

PersonInfo.propTypes = {
    info: PropTypes.object.isRequired
};