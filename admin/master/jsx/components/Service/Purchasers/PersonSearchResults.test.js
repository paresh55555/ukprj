import React from 'react';
import PersonSearchResults from './PersonSearchResults';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{searchResults:[{id:1,first_name:'test',last_name:'test'}]}
};
const mockStore = configureStore();
let store;

describe('<PersonSearchResults/>',()=> {

    let component, personSearchResult;

    const props = {};

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><PersonSearchResults {...props} /></Provider>);
        personSearchResult = component.find('PersonSearchResults');
    });

    it('length', () => {
        expect(personSearchResult).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PersonSearchResults').render().html()).toEqual(component.html());
    });

    it('setPerson',()=>{
        personSearchResult.node.setPerson({},()=>{
            expect(personSearchResult.node.props.service.searching).toEqual(false);
            expect(personSearchResult.node.props.service.searchResults).toEqual([]);
            expect(personSearchResult.node.props.service.currentPerson).toEqual({});
        });
    });

    it('onClick person',()=>{

        const person = personSearchResult.find('.person');
        expect(person).toHaveLength(initialState.service.searchResults.length);
        person.at(0).simulate('click',()=>{
            expect(personSearchResult.node.props.service.searching).toEqual(false);
            expect(personSearchResult.node.props.service.searchResults).toEqual([]);
            expect(personSearchResult.node.props.service.currentPerson).toEqual({id:1,first_name:'test',last_name:'test'});
        })


    });

    it('rendered component',()=>{
       expect(personSearchResult.find('.search-results')).toHaveLength(1);
       expect(personSearchResult.find('.person')).toHaveLength(initialState.service.searchResults.length);
        expect(personSearchResult.find('.name')).toHaveLength(initialState.service.searchResults.length);
        expect(personSearchResult.find('.name').at(0).text()).toEqual(initialState.service.searchResults[0].first_name+' '+ initialState.service.searchResults[0].last_name);
    });

    it('when no searchResults',()=>{

        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,searchResults:[]}}));
        component = mount(<Provider
            store={store}><PersonSearchResults {...props} /></Provider>);
        personSearchResult = component.find('PersonSearchResults');

        expect(personSearchResult.find('.no-results')).toHaveLength(1);
        expect(personSearchResult.find('.no-results').text()).toEqual('No customer found');

    });

});