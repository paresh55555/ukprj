import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Form, FormControl, Overlay} from "react-bootstrap";
import {setSearching, setSearchResults} from "../../../actions/service";
import * as ApiService from "../../../api/ApiService";
import PersonSearchResults from "./PersonSearchResults";
import * as ReactDOM from "react-dom";
import ServiceFormRow from "../ServiceFormRow";

class PersonSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchQuery: '',
        }
    }
    onSearch = (value) =>{
        this.setState({
            searchQuery: value,
        })

        const {
            searchAPI
        } = this.props;

        const apiToHit = searchAPI || ApiService.ApiSearchCustomer

        if(value.length >= 3){
            apiToHit(value).then(()=>{
                this.props.dispatch(setSearching(true))
            })
        }else{
            this.props.dispatch(setSearchResults([]))
            this.props.dispatch(setSearching(false))
        }
    }
    render() {
        let {service} = this.props
        return (
            <Form className={'service'}>
                <ServiceFormRow label={'Search'}>
                    <FormControl
                        ref={input => {this.target = input}}
                        type='text'
                        placeholder={'Search'}
                        onChange={(event) => this.onSearch(event.target.value)}
                        value={this.state.searchQuery}
                    />
                    <Overlay
                        show={service.searching}
                        container={this}
                        animation={false}
                        target={() => ReactDOM.findDOMNode(this.target)}
                        placement="bottom"
                    >
                        <PersonSearchResults />
                    </Overlay>
                </ServiceFormRow>
            </Form>
        );
    }
}

PersonSearch.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonSearch)