import React from 'react';
import PersonSearch from './PersonSearch';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{searching:'',searchResults:[]}
};
const mockStore = configureStore();
let store;

describe('<PersonSearch/>',()=> {

    let component, attachedAddresses;

    const props = {
        searchAPI: ''
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><PersonSearch {...props} /></Provider>);
        attachedAddresses = component.find('PersonSearch');
    });

    it('length', () => {
        expect(attachedAddresses).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PersonSearch').render().html()).toEqual(component.html());
    });

    it('onSearch',()=>{
        attachedAddresses.node.onSearch('te',()=>{
            expect(attachedAddresses.node.props.service.searching).toEqual(false);
            expect(attachedAddresses.node.props.service.searchResults).toEqual([]);
        });
        expect(attachedAddresses.node.state.searchQuery).toEqual('te')
        attachedAddresses.node.onSearch('test',()=>{
            expect(attachedAddresses.node.props.service.searching).toEqual(true);
        });
        expect(attachedAddresses.node.state.searchQuery).toEqual('test')
    });

    it('rendered component',()=>{
       expect(attachedAddresses.find('.service')).toHaveLength(1);
       expect(attachedAddresses.find('ServiceFormRow')).toHaveLength(1);
        expect(attachedAddresses.find('FormControl')).toHaveLength(1);
        expect(attachedAddresses.find('Overlay')).toHaveLength(2);
    });

});