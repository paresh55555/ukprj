import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col, ButtonToolbar, Form, FormGroup, FormControl, HelpBlock} from "react-bootstrap";
import SQModal from "../../Common/SQModal/SQModal";
import {setCurrentPerson, setSearching, setSearchResults, togglePersonFormDialog} from "../../../actions/service";
import PersonSearch from "./PersonSearch";
import ServiceFormRow from "../ServiceFormRow";
import ServiceButton from "../ServiceButton";
import AttachedAddresses from "./AttachedAddresses";
import Geosuggest from 'react-geosuggest';
import {findPostalCode, findCity, findState, findCountry, findStreet} from '../../Common/utilFunctions';

const VALIDATE_FIELDS = ['first_name', 'last_name', 'phone', 'email', 'address_1', 'city']

class PersonForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            person: {
                first_name: '',
                last_name: '',
                email: '',
                phone: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                zip: '',
            },
            note: '',
            errors: {}
        }
    }

    componentWillReceiveProps(nextProps) {
        const {currentPerson} = nextProps.service
        if (currentPerson) {
            this.setState({person: currentPerson})
        }
    }

    resetState = () => {
        this.setState({
            person: {
                first_name: '',
                last_name: '',
                email: '',
                phone: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                zip: '',
            },
            errors: {}
        })
        this.props.dispatch(setCurrentPerson(null))
        this.props.dispatch(setSearchResults([]))
        this.props.dispatch(setSearching(false))
    }

    onPlaceSelect = selectedAddress => {

        if (!selectedAddress) {
            return;
        }

        const {gmaps = {}} = selectedAddress
        const {address_components = {}, formatted_address = ''} = gmaps

        let city = findCity(address_components).long_name || ''
        let address_1 = findStreet(address_components).long_name || ''

        if (city) {
            let cityIndex = formatted_address.indexOf(city)
            if (cityIndex > -1) {
                address_1 = formatted_address.substr(0, cityIndex - 2)
            }

        }

        this.setState({
            person: {
                ...this.state.person,
                address_1,
                address_2: findStreet(address_components).long_name || '',
                city: city,
                state: findState(address_components).long_name || '',
                zip: findPostalCode(address_components).long_name || ''
            }
        })
    }

    onChange = (value, field) => {
        this.validateField(value, field)
        this.state.person[field] = value;
        this.setState({person: this.state.person})
    }
    onChangeNote = (value) => {
        this.setState({note: value})
    }
    onSubmit = (e) => {
        if (this.validForm()) {
            this.props.onSubmit(this.state.person, this.state.note)
            this.resetState();
        }
    }
    onClose = () => {
        this.resetState()
        this.props.dispatch(togglePersonFormDialog(false, null))
    }
    validForm = () => {
        VALIDATE_FIELDS.forEach(field => {
            this.validateField(this.state.person[field], field);
        })
        let errors = Object.keys(this.state.errors).filter((field) => {
            return !!this.state.errors[field]
        })
        return !errors.length
    }
    validateField = (value, field) => {

        if (VALIDATE_FIELDS.indexOf(field) !== -1) {
            this.state.errors[field] = !value ? this.getFieldTitle(field) + ' is required' : null;
        }

        if (field == 'email' && !this.state.errors[field]) {

            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value.toLowerCase())) {
                this.state.errors[field] = 'Please provide a valid email'
            }

        }

        this.setState({errors: this.state.errors})
    }

    getFieldTitle(field) {
        let splitted = field.split('_')
        splitted[0] = splitted[0].charAt(0).toUpperCase() + splitted[0].substr(1).toLowerCase()
        if (field === 'first_name' || field === 'last_name') {
            splitted[1] = splitted[1].charAt(0).toUpperCase() + splitted[1].substr(1).toLowerCase()
        }


        return splitted.join(' ')
    }

    renderFieldsRow(fields, fieldSize) {
        return (
            <Row>
                {fields.map((field) => {
                    return (
                        this.renderFormField(field, fieldSize)
                    )
                })}
            </Row>
        )
    }

    renderNoteField = () => {
        if(!this.props.showNote)return
        return (
            <ServiceFormRow label={'Add Note'}>
                <FormControl
                    componentClass="textarea"
                    placeholder={'Add Note'}
                    onChange={(event) => this.onChangeNote(event.target.value)}
                    value={this.state.note}
                />
            </ServiceFormRow>
        )
    }
    renderGeoSuggest = () =>{
        return (
            <Row>
                <Col sm={12}>
                    <Geosuggest
                        inputClassName="form-control"
                        onSuggestSelect={this.onPlaceSelect}
                    />
                </Col>
            </Row>
        )
    }

    renderForm = () => {
        let infoLabel = this.props.infoLabel || 'Customer Info'
        return (
            <Form className={'service person-form'}>
                <div className={'form-divider'}></div>
                <ServiceFormRow label={infoLabel} className={'mb-0'}>
                    {this.renderFieldsRow(['first_name', 'last_name'], 6)}
                    {this.renderFieldsRow(['email', 'phone'], 6)}
                    {this.renderGeoSuggest()}
                    {this.renderFieldsRow(['address_1'], 12)}
                    {this.renderFieldsRow(['address_2'], 12)}
                    {this.renderFieldsRow(['city', 'state', 'zip'], 4)}
                </ServiceFormRow>
                {this.renderNoteField()}
                <ServiceFormRow className={'mb-0'}>
                    {this.renderButtons()}
                </ServiceFormRow>
            </Form>
        )
    }
    renderFormField = (field, size) => {
        let {person} = this.state
        return (
            <Col key={field} sm={size}>
                <FormGroup validationState={this.state.errors[field] ? "error" : null}>
                    <FormControl
                        type='text'
                        placeholder={this.getFieldTitle(field)}
                        onChange={(event) => this.onChange(event.target.value, field)}
                        value={person[field] || ''}
                    />
                    <HelpBlock>{this.state.errors[field]}</HelpBlock>
                </FormGroup>
            </Col>
        )
    }
    renderButtons = () => {
        return (
            <ButtonToolbar>
                <ServiceButton bsStyle="primary" onClick={this.onSubmit} label={'Attach'}/>
                <ServiceButton onClick={this.onClose} label={'Cancel'}/>
            </ButtonToolbar>
        )
    }
    renderAttachedAddresses = () => {
        if (this.props.attachedOrderId) {
            return <AttachedAddresses orderId={this.props.attachedOrderId}/>
        }
        return null
    }

    render() {
        let {service, searchAPI} = this.props
        return (
            <SQModal title={this.props.title}
                     isOpen={service.personFormOpened && (service.personFormId === this.props.id)}
                     onClose={this.onClose}
                     className={'service-modal person'}
                     customButtons
            >
                {this.renderAttachedAddresses()}
                <PersonSearch searchAPI={searchAPI}/>
                {this.renderForm()}
            </SQModal>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonForm)