import React from 'react';
import PurchasersList from './PurchasersList';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux';


const initialState = {
    service:{orderPurchasers:[{}]}
};

const mockStore = configureStore();
let store;

describe('<PurchasersList/>',()=>{
    let component,purchasersList;

    beforeEach(()=>{
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><PurchasersList  /></Provider>);
        purchasersList = component.find('PurchasersList');

    });

    it('length', () => {
        expect(purchasersList).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PurchasersList').render().html()).toEqual(component.html());
    });

    it('rendered Component',()=>{
        expect(purchasersList.find('.purchasers-list')).toHaveLength(1);
        expect(purchasersList.find('PurchaserInfo')).toHaveLength(initialState.service.orderPurchasers.length);
    });

    it('when no orderPurchasers',()=>{
        store = mockStore(Object.assign({},initialState,{service:{...initialState.service,orderPurchasers:null}}));
        component = mount(<Provider
            store={store}><PurchasersList  /></Provider>);
        purchasersList = component.find('PurchasersList');

        expect(purchasersList.node.renderPurchasers()).toEqual(undefined);

    });

});