import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../ServiceButton";
import PersonAvatar from "./PersonAvatar";
import PersonInfo from "./PersonInfo";
import * as ApiService from "../../../api/ApiService";
import {setCurrentPerson, togglePersonFormDialog} from "../../../actions/service";

class PurchaserInfo extends Component {
    editPurchaser = () => {
        this.props.dispatch(setCurrentPerson(this.props.data))
        this.props.dispatch(togglePersonFormDialog(true,'purchaser'))
    }
    deletePurchaser = () =>{
        let purchaserId = this.props.data.id
        let orderId = this.props.service.currentOrder.id
        ApiService.ApiDeleteOrderPurchaser(orderId, purchaserId)
    }
    renderButtons(){
        return (
            <ButtonToolbar className={'buttons pull-right'}>
                <ServiceButton onClick={this.editPurchaser} label={'Edit'}/>
                <ServiceButton onClick={this.deletePurchaser} label={'Delete'}/>
            </ButtonToolbar>
        )
    }
    render() {
        let orderPurchaser = this.props.data
        
        return (
            <div className={'purchaser-info'}>
                <Row>
                    <Col md={1}>
                        <PersonAvatar info={orderPurchaser}/>
                    </Col>
                    <Col md={7}>
                        <PersonInfo info={orderPurchaser}/>
                    </Col>
                    <Col md={4}>
                        {this.renderButtons()}
                    </Col>
                </Row>
            </div>
        );
    }
}

PurchaserInfo.propTypes = {
    data: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(PurchaserInfo)