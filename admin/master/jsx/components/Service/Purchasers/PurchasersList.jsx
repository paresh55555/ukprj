import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import PurchaserInfo from "./PurchaserInfo";

class PurchasersList extends Component {
    renderPurchasers(){
        const {orderPurchasers} = this.props.service
        if(orderPurchasers === null){
            return
        }
        if(orderPurchasers.length === 0){
            return
        }
        return(
            orderPurchasers.map((orderPurchaser)=>{
                return <PurchaserInfo key={orderPurchaser.id} data={orderPurchaser}/>
            })
        )
    }
    render() {
        return (
            <div className={'purchasers-list purchase'}>
                {this.renderPurchasers()}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(PurchasersList)