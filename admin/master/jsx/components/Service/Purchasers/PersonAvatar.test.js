import React from 'react';
import PersonAvatar from './PersonAvatar';
import {mount} from 'enzyme';
import {Provider} from 'react-redux'
import configureStore from 'redux-mock-store'

const initialState = {};

const mockStore = configureStore();
let store;

describe('<PersonAvatar/>', () => {
    let component, personAvatar;
    let props={info:{first_name:'abc', last_name:'xyz' }};

    beforeEach(()=>{
        store = mockStore(initialState);
        component = mount(<Provider store={store}><PersonAvatar {...props}/></Provider>);
        personAvatar = component.find('PersonAvatar');
    });

    it('length', () => {
        expect(personAvatar).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PersonAvatar').render().html()).toEqual(component.html());
    });

    it('rendered component',()=>{
        expect(personAvatar.find('.img-circle')).toHaveLength(1);
        expect(personAvatar.find('.img-circle > span')).toHaveLength(1);
        expect(personAvatar.find('.img-circle > span').text()).toEqual(props.info.first_name.charAt(0)+props.info.last_name.charAt(0));
    });

});



