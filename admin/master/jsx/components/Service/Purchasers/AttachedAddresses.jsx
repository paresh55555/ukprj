import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import * as ApiService from '../../../api/ApiService'
import {setCurrentPerson} from "../../../actions/service";
import PersonAddress from "./PersonAddress";

class AttachedAddresses extends Component {
    componentWillMount(){
        ApiService.ApiGetOrderPurchasers(this.props.orderId)
    }
    onClick = (purchaser) =>{
        this.props.dispatch(setCurrentPerson(purchaser))
    }
    renderAddresses(){
        let {orderPurchasers} = this.props.service
        if(typeof orderPurchasers == 'undefined'){
            return ''
        }
        if(orderPurchasers.length == 0){
            return ''
        }
        return orderPurchasers.map((purchaser, i)=>{
            return (
                <div key={i} className={'attached-item'} onClick={() => this.onClick(purchaser)}>
                    <span className={'name'}>{purchaser.first_name + ' ' + purchaser.last_name}</span>
                    <PersonAddress info={purchaser}/>
                </div>
            )
        })
    }
    render() {
        return (
            <div className={'attached-addresses clearfix'}>
                <div className={'clearfix'}>
                    <div className={'header'}>Address(es) attached to Order ID: <span>{this.props.orderId}</span></div>
                    {this.renderAddresses()}
                </div>
                <div className={'form-divider'}></div>
            </div>
        );
    }
}

AttachedAddresses.propTypes = {
    orderId: PropTypes.number.isRequired
};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AttachedAddresses)