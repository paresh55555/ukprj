import React from 'react';
import PersonInfo from './PersonInfo';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{searching:'',searchResults:[]}
};
const mockStore = configureStore();
let store;

describe('<PersonInfo/>',()=> {

    let component, personInfo;

    const props = {
        info: { first_name:'test',last_name:'test' ,email :'test@test.com',phone:'012345',note:'testNote'}
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><PersonInfo {...props} /></Provider>);
        personInfo = component.find('PersonInfo');
    });

    it('length', () => {
        expect(personInfo).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PersonInfo').render().html()).toEqual(component.html());
    });

    it('rendered component',()=>{
        expect(personInfo.find('.info')).toHaveLength(1);
        expect(personInfo.find('.name')).toHaveLength(1);
        expect(personInfo.find('.name').text()).toEqual(props.info.first_name +' '+props.info.last_name);
        expect(personInfo.find('.email-phone')).toHaveLength(2);
        expect(personInfo.find('.address')).toHaveLength(1);
        expect(personInfo.find('.text_content')).toHaveLength(1);
        expect(personInfo.find('.text_content').text()).toEqual(props.info.note);
    });

});
