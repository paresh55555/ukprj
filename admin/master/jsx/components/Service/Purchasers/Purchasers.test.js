import React from 'react';
import Purchasers from './Purchasers';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux';


const initialState = {
    service:{currentOrder:{id:1},orderPurchasers:[{id:1}]}
};

const mockStore = configureStore();
let store;

describe('<Purchasers/>',()=>{
    let component,purchasers;

    beforeEach(()=>{
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><Purchasers /></Provider>);
        purchasers = component.find('Purchasers');

    })
    it('length', () => {
        expect(purchasers).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('Purchasers').render().html()).toEqual(component.html());
    });

    it('componentWillMount',()=>{
        expect(purchasers.node.componentWillMount()).toEqual(undefined);
    });

    it('componentWillUnmount',()=>{
        expect(purchasers.node.componentWillUnmount()).toEqual(undefined);
    });
    it('openAddPurchaser',()=>{
        purchasers.node.openAddPurchaser(()=>{
            expect(purchasers.node.props.service.personFormOpened).toEqual(true);
            expect(purchasers.node.props.service.personFormId).toEqual('purchaser');
        });
    });
    it('addPurchaser',()=>{
        purchasers.node.addPurchaser({id:1},()=>{
            expect(purchasers.node.props.service.personFormOpened).toEqual(false);
            expect(purchasers.node.props.service.personFormId).toEqual(null);
        });
        purchasers.node.addPurchaser({id:0},()=>{
            expect(purchasers.node.props.service.personFormOpened).toEqual(false);
            expect(purchasers.node.props.service.personFormId).toEqual(null);
        });
    });

    it('onClick openAddPurchaser',()=>{
        const Buttons=purchasers.find('ServiceButton');
        expect(Buttons).toHaveLength(3);
        Buttons.at(0).simulate('click',()=>{
            expect(purchasers.node.props.service.personFormOpened).toEqual(false);
            expect(purchasers.node.props.service.personFormId).toEqual('purchaser');
        });
    });

    it('rendered Component',()=>{
        expect(purchasers.find('ServiceFormRow')).toHaveLength(1);
        expect(purchasers.find('PurchasersList')).toHaveLength(1);
        expect(purchasers.find('ServiceButton')).toHaveLength(3);
        expect(purchasers.find('PersonForm')).toHaveLength(1);
    })
});
