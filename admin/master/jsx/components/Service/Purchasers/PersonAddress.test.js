import React from 'react';
import PersonAddress from './PersonAddress';
import {shallow} from 'enzyme';

describe('<PersonAddress/>',()=>{

    let component,props={info:{}};

   it('when no info',()=>{
      component = shallow(<PersonAddress {...props}/>);
      expect(component.html()).toEqual('<span></span>');
   });

   it('when address_1',()=>{
       props.info.address_1 = 'address_1';
       component = shallow(<PersonAddress {...props}/>);
       expect(component.html()).toEqual('<span>address_1</span>');
   });

    it('when address_2',()=>{
        props.info.address_1 = 'address_1';
        props.info.address_2 = 'address_2';
        component = shallow(<PersonAddress {...props}/>);
        expect(component.html()).toEqual('<span>address_1 address_2</span>');
    });

    it('when city',()=>{
        props.info.address_1 = 'address_1';
        props.info.address_2 = 'address_2';
        props.info.city = "testCity"
        component = shallow(<PersonAddress {...props}/>);
        expect(component.html()).toEqual('<span>address_1 address_2, testCity</span>');
    });

    it('when state',()=>{
        props.info.address_1 = 'address_1';
        props.info.address_2 = 'address_2';
        props.info.city = "testCity";
        props.info.state = "testState";
        component = shallow(<PersonAddress {...props}/>);
        expect(component.html()).toEqual('<span>address_1 address_2, testCity, testState</span>');
    });

    it('when zip',()=>{
        props.info.address_1 = 'address_1';
        props.info.address_2 = 'address_2';
        props.info.city = "testCity";
        props.info.state = "testState";
        props.info.zip = 111111;
        component = shallow(<PersonAddress {...props}/>);
        expect(component.html()).toEqual('<span>address_1 address_2, testCity, testState 111111</span>');
    });



});