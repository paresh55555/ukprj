import React from 'react';
import PurchaserInfo from './PurchaserInfo';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{currentOrder:{id:1}}
};
const mockStore = configureStore();
let store;

describe('<PurchaserInfo/>',()=> {

    let component, purchaserInfo;

    const props = {
        data: {id:1}
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><PurchaserInfo {...props} /></Provider>);
        purchaserInfo = component.find('PurchaserInfo');
    });

    it('length', () => {
        expect(purchaserInfo).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PurchaserInfo').render().html()).toEqual(component.html());
    });

    it('editPurchaser',()=>{
        purchaserInfo.node.editPurchaser(()=>{
           expect(purchaserInfo.node.props.service.currentPerson).toEqual(props.data);
            expect(purchaserInfo.node.props.service.personFormOpened).toEqual(true);
            expect(purchaserInfo.node.props.service.personFormId).toEqual(purchaser);
        });
    });

    it('deletePurchaser',()=>{
        expect(purchaserInfo.node.deletePurchaser()).toEqual(undefined);
    });

    it('rendered component',()=>{
        expect(purchaserInfo.find('.purchaser-info')).toHaveLength(1);
        expect(purchaserInfo.find('Row')).toHaveLength(1);
        expect(purchaserInfo.find('Col')).toHaveLength(3);
        expect(purchaserInfo.find('PersonAvatar')).toHaveLength(1);
        expect(purchaserInfo.find('PersonInfo')).toHaveLength(1);
        expect(purchaserInfo.find('ButtonToolbar')).toHaveLength(1);
        expect(purchaserInfo.find('ServiceButton')).toHaveLength(2);
    });

    it('onClick ServiceButton',()=>{
       const ServiceButton = purchaserInfo.find('ServiceButton');
       expect(ServiceButton).toHaveLength(2);

       ServiceButton.at(0).simulate('click',()=>{
           expect(purchaserInfo.node.props.service.currentPerson).toEqual(props.data);
           expect(purchaserInfo.node.props.service.personFormOpened).toEqual(true);
           expect(purchaserInfo.node.props.service.personFormId).toEqual(purchaser);
       });

        ServiceButton.at(1).simulate('click');

    });

});