import React from 'react';
import PersonForm from './PersonForm';
import {mount} from 'enzyme';
import {Provider} from 'react-redux'
import configureStore from 'redux-mock-store'

const initialState={
        service:{currentPerson:'',personFormOpened:false,personFormId:1}
};

const mockStore = configureStore();
let store;

describe('<PersonForm/>',()=>{

    let component,personForm,props={
        onSubmit:jest.fn(),
        showNote:true
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><PersonForm {...props}/></Provider>);
        personForm = component.find('PersonForm');
    });

    it('length', () => {
        expect(personForm).toHaveLength(1);
    });

    it('render', () => {
        expect(component.find('PersonForm').render().html()).toEqual(component.html());
    });

    it('componentWillReceiveProps',()=>{
      expect(personForm.node.componentWillReceiveProps({service:{}})).toEqual(undefined);
        personForm.node.componentWillReceiveProps({service:{currentPerson:{}}});
        expect(personForm.node.state.person).toEqual({});
    });

    it('resetState',()=>{
        personForm.node.resetState(()=>{
            expect(personForm.node.props.service.currentPerson).toEqual(null);
            expect(personForm.node.props.service.searchResults).toEqual([]);
            expect(personForm.node.props.service.searching).toEqual(null);
        });
        expect(personForm.node.state).toEqual({
            person: {
                first_name: '',
                last_name: '',
                email: '',
                phone: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                zip: '',
            },
            note: '',
            errors: {}
        });
    });

    it('onChangeNote',()=>{
       personForm.node.onChangeNote('test');
       expect(personForm.node.state.note).toEqual('test');
    });

    it('onChangeNote',()=>{
        personForm.node.onChangeNote('test');
        expect(personForm.node.state.note).toEqual('test');
    });

    it('onChange',()=>{
        personForm.node.onChange('','first_name');
        expect(personForm.node.state.errors.first_name).toEqual('First Name is required');
        expect(personForm.node.state.person.first_name).toEqual('');
        personForm.node.onChange('test','first_name');
        expect(personForm.node.state.errors.first_name).toEqual(null);
        expect(personForm.node.state.person.first_name).toEqual('test');
    });

    it('validateField',()=>{
        personForm.node.validateField('','first_name');
        expect(personForm.node.state.errors.first_name).toEqual('First Name is required');
        personForm.node.validateField('test','first_name');
        expect(personForm.node.state.errors.first_name).toEqual(null);
        personForm.node.validateField('','last_name');
        expect(personForm.node.state.errors.last_name).toEqual('Last Name is required');
        personForm.node.validateField('test','last_name');
        expect(personForm.node.state.errors.last_name).toEqual(null);
        personForm.node.validateField('','phone');
        expect(personForm.node.state.errors.phone).toEqual('Phone is required');
        personForm.node.validateField('012345','phone');
        expect(personForm.node.state.errors.phone).toEqual(null);
        personForm.node.validateField('','email');
        expect(personForm.node.state.errors.email).toEqual('Email is required');
        personForm.node.validateField('test','email');
        expect(personForm.node.state.errors.email).toEqual('Please provide a valid email');
        personForm.node.validateField('test@test.com','email');
        expect(personForm.node.state.errors.email).toEqual(null);
        personForm.node.validateField('','address_1');
        expect(personForm.node.state.errors.address_1).toEqual('Address 1 is required');
        personForm.node.validateField('test','address_1');
        expect(personForm.node.state.errors.address_1).toEqual(null);
        personForm.node.validateField('','city');
        expect(personForm.node.state.errors.city).toEqual('City is required');
        personForm.node.validateField('test','city');
        expect(personForm.node.state.errors.city).toEqual(null);
    });

    it('validForm',()=>{
       expect(personForm.node.validForm()).toEqual(false);
       personForm.node.setState({person:{
           first_name: 'test',
           last_name: 'test',
           email: 'test@test.com',
           phone: '01234',
           address_1: 'test',
           address_2: 'test',
           city: 'test',
       }});
       expect(personForm.node.validForm()).toEqual(true);
    });

    it('onSubmit',()=>{
        expect(personForm.node.onSubmit()).toEqual(undefined);
        personForm.node.setState({person:{
            first_name: 'test',
            last_name: 'test',
            email: 'test@test.com',
            phone: '01234',
            address_1: 'test',
            address_2: 'test',
            city: 'test',
        }});
        personForm.node.onSubmit();
        expect(props.onSubmit).toHaveBeenCalled();
        expect(personForm.node.state).toEqual({
            person: {
                first_name: '',
                last_name: '',
                email: '',
                phone: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                zip: '',
            },
            note: '',
            errors: {}
        });

    });

    it('getFieldTitle',()=>{
      expect(personForm.node.getFieldTitle('first_name')).toEqual('First Name');
    });

    it('onClose',()=>{
        personForm.node.onClose(()=>{
            expect(personForm.node.props.service.personFormOpened).toEqual(false);
            expect(personForm.node.props.service.personFormId).toEqual(null);
        });
        expect(personForm.node.state).toEqual({
            person: {
                first_name: '',
                last_name: '',
                email: '',
                phone: '',
                address_1: '',
                address_2: '',
                city: '',
                state: '',
                zip: '',
            },
            note: '',
            errors: {}
        });

    })

});
