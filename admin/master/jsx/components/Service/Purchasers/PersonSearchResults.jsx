import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {setCurrentPerson, setSearching, setSearchResults} from "../../../actions/service";
import PersonAddress from "./PersonAddress";

class PersonSearchResults extends Component {
    setPerson = (customer) =>{
        this.props.dispatch(setCurrentPerson(customer))
        this.props.dispatch(setSearchResults([]))
        this.props.dispatch(setSearching(false))
    }
    renderSearchResults = (searchResults) =>{
        if(!searchResults.length){
            return <span className={'no-results'}>No customer found</span>
        }
        return (searchResults.map((customer)=>{
                return (
                    <div key={customer.id} className={'person'} key={customer.id} onClick={() => this.setPerson(customer)}>
                        <span className={'name'}>{customer.first_name} {customer.last_name}</span>
                        <div className={'address'}><PersonAddress info={customer}/></div>
                    </div>
                )
            })
        )
    }
    render() {
        let {service} = this.props
        return (
            <div className={'search-results'}>
                {this.renderSearchResults(service.searchResults)}
            </div>
        );
    }
}

PersonSearchResults.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonSearchResults)