import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import PurchasersList from "./PurchasersList";
import PersonForm from "./PersonForm";
import {setPurchasers, togglePersonFormDialog} from "../../../actions/service";
import * as ApiService from "../../../api/ApiService";
import ServiceFormRow from "../ServiceFormRow";
import ServiceButton from "../ServiceButton";

class Purchasers extends Component {
    componentWillMount(){
        const {currentOrder} = this.props.service
        if(currentOrder && currentOrder.id){
            ApiService.ApiGetOrderPurchasers(currentOrder.id)
        }
    }
    componentWillUnmount(){
        this.props.dispatch(setPurchasers([]))
    }
    openAddPurchaser = () =>{
        this.props.dispatch(togglePersonFormDialog(true,'purchaser'))
    }
    addPurchaser = (person) =>{
        const {currentOrder, orderPurchasers} = this.props.service
        let foundPerson = orderPurchasers.filter(function (purchaser) { return purchaser.id === person.id });
        if(foundPerson.length){
            ApiService.ApiUpdateOrderPurchaser(currentOrder.id, person)
        }else{
            delete person.id;
            ApiService.ApiAddOrderPurchaser(currentOrder.id, person)
        }
        this.props.dispatch(togglePersonFormDialog(false, null))
    }
    renderButton(){
        const {orderPurchasers} = this.props.service
        return (
            orderPurchasers && orderPurchasers.length ?
                <ServiceButton className={'pull-right'} onClick={this.openAddPurchaser} label={'Add another purchaser'}/>
                :
                <ServiceButton fullWidth onClick={this.openAddPurchaser} label={'Attach Purchaser'} />
        )
    }
    render() {
        return (
            <ServiceFormRow label={'Purchasers'}>
                <PurchasersList />
                {this.renderButton()}
                <PersonForm title={'Attach Purchaser'} id={'purchaser'} infoLabel={'Purchaser Info'} onSubmit={this.addPurchaser}/>
            </ServiceFormRow>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Purchasers)