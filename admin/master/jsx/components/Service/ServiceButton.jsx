import React, {PropTypes} from 'react';
import {Button} from "react-bootstrap";

export default function ServiceButton(props) {
    let full = props.fullWidth ? ' full-width' : ''
    let buttonStyle = props.bsStyle || 'default'
    let className = (props.className ? props.className : '') + full
    return (
        <Button
            bsStyle={buttonStyle}
            className={className}
            onClick={props.onClick}
            disabled={props.disabled}>{props.label}</Button>
    );
}

ServiceButton.propTypes = {
    label: PropTypes.string.isRequired
};