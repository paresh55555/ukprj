import React from 'react';
import ServiceTable from './ServiceTable';
import {shallow} from 'enzyme';

describe('<ServiceTable/>',()=>{

    let component,props={
        data:[],
        fields:{},
        onClick:jest.fn(),
        topButton:'topButton'
    };
    beforeEach(()=>{
        component=shallow(<ServiceTable {...props}/>);
    });

    it('length',()=>{
        expect(component).toHaveLength(1);
    });

    it('rendered component',()=>{
        expect(component.find('.top-section')).toHaveLength(1);
    });

    it('onClick',()=>{
       component.instance().onClick({});
       expect(props.onClick).toHaveBeenCalled();
    });

    it('rendered component',()=>{
        expect(component.find('.top-section')).toHaveLength(1);
        expect(component.find('Col')).toHaveLength(2);
        expect(component.find('.orders-search')).toHaveLength(1);
    });

    it('when no top button and onClick',()=>{
        delete props.onClick;
        delete props.topButton;
        component=shallow(<ServiceTable {...props}/>);
        expect(component.instance().onClick({})).toEqual(false);
    });

});