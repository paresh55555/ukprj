import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import SQModal from "../../Common/SQModal/SQModal";
import {toggleAddDoc} from "../../../actions/service";
import {ButtonToolbar, Form, FormControl} from "react-bootstrap";
import * as ApiService from "../../../api/ApiService";
import ServiceFormRow from "../ServiceFormRow";
import FileSelector from "../../Common/FileSelector";
import ServiceButton from "../ServiceButton";
import FileUploadInfo from "../FileUploadInfo";
import {cancelRequest} from "../../../api/ApiCalls";
import DocTypes from "./DocTypes";
import FieldError from "../FieldError";

class AddDoc extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 0,
            customType: '',
            file: null,
            base64File: null,
            uploading: false,
            uploadingProgress: 0,
            error: {}
        }
    }
    componentWillMount(){
        ApiService.ApiGetDocTypes()
    }
    onClose = () =>{
        this.props.dispatch(toggleAddDoc(false))
        this.resetState()
    }
    onChange = (value, type) =>{
        if(type === 'type'){this.state.customType = ''}
        if(type === 'customType'){this.state.type = 0}
        this.state[type] = value;
        this.setState(this.state)
        this.checkField(type)
    }
    resetState(){
        this.setState({type: 0, customType: '', file: null, base64File: null,
            uploading: false, uploadingProgress: 0, error:{}})
    }
    checkField(field){
        if(field === 'type' || field === 'customType'){
            this.state.error['type'] = !this.state[field]
        }
        if(field === 'file'){
            this.state.error['file'] = !this.state.base64File
        }
        this.setState(this.state)
    }
    validate(){
        let valid = true
        if(!this.state.type && !this.state.customType){
            this.checkField('type')
            valid = false
        }
        if(!this.state.base64File){
            this.checkField('file')
            valid = false
        }
        if(valid){
            this.state.error = {}
        }
        this.setState(this.state)
        return valid
    }
    uploadProgress = (progressEvent) =>{
        var percentCompleted = Math.round( (progressEvent.loaded * 100) / progressEvent.total )
        this.setState({uploadingProgress:percentCompleted})
    }
    addDoc = () =>{
        let type = this.state.type;
        if(!this.validate())return
        if(this.state.customType){
            ApiService.ApiCreateDocType({type:this.state.customType, category: "order"}).then((response)=>{
                type = response.data[0].id
                this.createDoc(type);
            });
        }else{
            this.createDoc(type)
        }
    }
    createDoc(type){
        this.setState({uploading: true})
        let data = new FormData()
        data.append('docs', this.state.base64File);
        data.append('docType', type)
        data.append('fileName', this.state.file.name)
        ApiService.ApiAddDoc(this.props.service.currentOrder.id, data, this.uploadProgress).then(()=>{
            this.resetState();
            this.props.dispatch(toggleAddDoc(false))
        })
    }
    onFileSelect = (file) =>{
        this.setState({file})
    }
    renderTypeSelection(){
        return (
            <ServiceFormRow label={'Doc Type'} >
                <DocTypes category={"order"}
                           selectedVal={this.state.type}
                           onChange={(type) => this.onChange(type, 'type')} />
                <FieldError error={this.state.error.type} text={'Please select type'}/>
            </ServiceFormRow>
        )
    }
    renderCustomType(){
        return (
            <ServiceFormRow label={'Сustom Doc Type'} >
                <FormControl placeholder={'Custom Doc'} onChange={(e) => this.onChange(e.target.value, 'customType')} value={this.state.customType}/>
                <FieldError error={this.state.error.type} text={'Please select type'}/>
            </ServiceFormRow>
        )
    }
    renderUploaderZone(){
        return(
            <ServiceFormRow label={'Upload Document'} >
                <FileSelector onSelect={this.onFileSelect}
                              fileTypes={'image/png,image/jpg,image/jpeg,image/gif,application/pdf'}/>
                <FieldError error={this.state.error.file} text={'Please select file'}/>
            </ServiceFormRow>
        )
    }
    onFileLoaded = (fileObj) =>{
        this.setState({base64File: fileObj.base64})
        this.checkField('file')
    }
    cancelUpload = () =>{
        cancelRequest()
        this.setState({uploading: false, uploadingProgress: 0})
    }
    deleteFile = () =>{
        this.setState({file: null, base64File: null})
    }
    renderFileUpload(){
        if(!this.state.file){
            return ''
        }
        return (
            <ServiceFormRow label={'Upload Queue'}>
                <FileUploadInfo file={this.state.file}
                                uploading={this.state.uploading}
                                uploadingProgress={this.state.uploadingProgress}
                                onFileLoaded={this.onFileLoaded}
                                onCancel={this.cancelUpload}
                                onDelete={this.deleteFile}/>
            </ServiceFormRow>
        )
    }
    renderButtons(){
        return (
            <ServiceFormRow label={''}>
                <ButtonToolbar>
                    <ServiceButton bsStyle="primary" onClick={this.addDoc} label={'Add Doc'} disabled={this.state.uploading} />
                    <ServiceButton onClick={this.onClose} label={'Cancel'} />
                </ButtonToolbar>
            </ServiceFormRow>
        )
    }
    render() {
        return (
            <SQModal title={'Add Doc'}
                     isOpen={this.props.service.addDocOpened}
                     onClose={this.onClose}
                     className={'service-modal'}
                     customButtons
            >
                <Form className={'service'}>
                    {this.renderTypeSelection()}
                    <div className={'form-divider'}></div>
                    {this.renderCustomType()}
                    {this.renderUploaderZone()}
                    {this.renderFileUpload()}
                    {this.renderButtons()}
                </Form>
            </SQModal>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(AddDoc)