import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
//import DocInfo from "./DocInfo";
import FileInfo from "../FileInfo";
import * as ApiService from "../../../api/ApiService";

class DocsList extends Component {
    componentWillMount(){
        if(this.props.service.currentOrder && this.props.service.currentOrder){
            ApiService.ApiGetOrderDocs(this.props.service.currentOrder.id)
        
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.service.currentOrder && this.props.service.currentOrder.id !== nextProps.service.currentOrder.id){
            ApiService.ApiGetOrderDocs(nextProps.service.currentOrder.id)
        }
    }
    deleteDoc(id){
        ApiService.ApiDeleteOrderDocs(this.props.service.currentOrder.id,id)
    }
    render() {
        const {orderDocs} = this.props.service
        return (
            <div>
                {orderDocs && orderDocs.map((doc, i)=>{
                    return <FileInfo key={i} fileName={doc.file_name} url={doc.url} onDelete = {this.deleteDoc.bind(this,doc.id)}/>
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(DocsList)