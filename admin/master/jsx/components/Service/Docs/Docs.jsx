import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import AddDoc from "./AddDoc";
import {setOrderDocs, toggleAddDoc} from "../../../actions/service";
import * as ApiService from "../../../api/ApiService";
import DocsList from "./DocsList";
import ServiceButton from "../ServiceButton";
import ServiceFormRow from "../ServiceFormRow";



class Docs extends Component {
    componentWillMount(){
        const {currentOrder} = this.props.service
        if(currentOrder && currentOrder.id){
            ApiService.ApiGetOrderDocs(currentOrder.id)
        }
    }
    openAddDocs = () =>{
        this.props.dispatch(toggleAddDoc(true))
    }
    componentWillUnmount(){
        this.props.dispatch(setOrderDocs([]))
    }
    renderButton(){
        const {orderDocs} = this.props.service
        
        return (
            orderDocs && orderDocs.length ?
                <ServiceButton className={'pull-right another'} onClick={this.openAddDocs} label={'Add another doc'} />
                :
                <ServiceButton fullWidth onClick={this.openAddDocs} label={'Add Docs'} />
        )
    } 
    render() {
        const {orderDocs} = this.props.service
        
        return (
            <ServiceFormRow label={'Docs'}>
                <DocsList />
                {this.renderButton()}
                <AddDoc />
              
            </ServiceFormRow>
        );
    }
}

Docs.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(Docs)