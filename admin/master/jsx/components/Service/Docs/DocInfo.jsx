import React, {PropTypes} from 'react';
import {Col, Image} from "react-bootstrap";

export default function DocInfo(props) {
    //TODO: use props.doc.url for image
    return (
        <Col sm={3} className={'text-center'}>
            <Image src={'https://react-bootstrap.github.io/thumbnail.png'} rounded />
            <div>{props.doc.type || props.doc.customType}</div>
        </Col>
    );
}

DocInfo.propTypes = {
    doc: PropTypes.object.isRequired
};