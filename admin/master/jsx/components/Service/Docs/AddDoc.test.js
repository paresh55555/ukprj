import React from 'react';
import AddDoc from './AddDoc';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{currentOrder:{id:1},addDocOpened:true}
};
const mockStore = configureStore();
let store;

describe("<AddDoc/>",()=> {

    let component, addDoc;

    const props = {
        customerOrders: [],
        customerJobs: [],
        isNewCustomer: false,
        gotoServiceJob: jest.fn(),
        onCancel: jest.fn(),
        customer: {
            id: 1,
            first_name: 'test',
            last_name: 'test',
            email: 'test@test.com',
            phone: '012345',
            address_list: []
        },
        onSave: jest.fn()
    };

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><AddDoc {...props} /></Provider>);
        addDoc = component.find('AddDoc');
    });

    it('length', () => {
        expect(addDoc).toHaveLength(1);
    });

    it('componentWillMount',()=>{
       expect(addDoc.node.componentWillMount()).toEqual(undefined);
    });

    it('onClose',()=>{
       addDoc.node.onClose(()=>{
           expect(addDoc.node.props.service.addDocOpened).toEqual(false);
       });
       expect(addDoc.node.state).toEqual({type: 0, customType: '', file: null, base64File: null,
           uploading: false, uploadingProgress: 0, error:{}});
    });

    it('checkField',()=>{
        addDoc.node.checkField('type');
        expect(addDoc.node.state.error.type).toEqual(true);
        addDoc.node.checkField('customType');
        expect(addDoc.node.state.error.type).toEqual(true);
        addDoc.node.checkField('file');
        expect(addDoc.node.state.error.file).toEqual(true);
    });

    it('validate',()=>{

        addDoc.node.setState({type:true,customType:false,base64File:true});
        expect(addDoc.node.validate()).toEqual(true);
        expect(addDoc.node.state.error).toEqual({});
        addDoc.node.setState({type:false,customType:true,base64File:true});
        expect(addDoc.node.validate()).toEqual(true);
        expect(addDoc.node.state.error).toEqual({});
        addDoc.node.setState({type:false,customType:false,base64File:true});
        expect(addDoc.node.validate()).toEqual(false);
        expect(addDoc.node.state.error.type).toEqual(true);
        addDoc.node.setState({type:false,customType:false,base64File:false});
        expect(addDoc.node.validate()).toEqual(false);
        expect(addDoc.node.state.error.type).toEqual(true);
        expect(addDoc.node.state.error.file).toEqual(true);
        addDoc.node.setState({type:true,customType:false,base64File:false});
        expect(addDoc.node.validate()).toEqual(false);
        expect(addDoc.node.state.error.file).toEqual(true);

    });

    it('uploadProgress',()=>{
        addDoc.node.uploadProgress({loaded:5,total:50});
        expect(addDoc.node.state.uploadingProgress).toEqual(Math.round( (5 * 100) / 50 ));
    });

    it('addDoc',()=>{
        addDoc.node.setState({type:true,customType:false,base64File:false});
        expect(addDoc.node.addDoc()).toEqual(undefined);
        addDoc.node.setState({type:true,customType:false,base64File:true,file:new Blob([JSON.stringify({name:'testFile'}, null, 2)])});
        addDoc.node.addDoc();
    });

    it('onFileSelect',()=>{
        addDoc.node.onFileSelect('');
        expect(addDoc.node.state.file).toEqual('');
    });

    it('onFileLoaded',()=>{
        addDoc.node.onFileLoaded({base64:'Base64'});
        expect(addDoc.node.state.base64File).toEqual('Base64');
        expect(addDoc.node.state.error).toEqual({file:false});
    });

    it('cancelUpload',()=>{
        addDoc.node.cancelUpload();
        expect(addDoc.node.state.uploading).toEqual(false);
        expect(addDoc.node.state.uploadingProgress).toEqual(0);
    });

    it('deleteFile',()=>{
        addDoc.node.deleteFile();
        expect(addDoc.node.state.file).toEqual(null);
        expect(addDoc.node.state.base64File).toEqual(null);
    });

    it('renderFileUpload',()=>{
        addDoc.node.setState({file:''});
        expect(addDoc.node.renderFileUpload()).toEqual('');
    });

    it('resetState',()=>{
        addDoc.node.resetState();
        expect(addDoc.node.state).toEqual({type: 0, customType: '', file: null, base64File: null,
            uploading: false, uploadingProgress: 0, error:{}});
    });

    it('onChange',()=>{
        addDoc.node.onChange('','type');
        expect(addDoc.node.state.customType).toEqual('');
        expect(addDoc.node.state.type).toEqual('');
        expect(addDoc.node.state.error).toEqual({type:true});
        addDoc.node.onChange('','customType');
        expect(addDoc.node.state.type).toEqual(0);
        expect(addDoc.node.state.customType).toEqual('');
        expect(addDoc.node.state.error).toEqual({type:true});
    });

});