import React from 'react';
import DocInfo from './DocInfo';
import {shallow} from 'enzyme';

describe('<DocInfo/>',()=>{

    let component,props={
        doc:{type:'test type'}
    };

    beforeEach(()=>{
       component = shallow(<DocInfo {...props}/>);
    });

    it('length',()=>{
       expect(component).toHaveLength(1);
    });

    it('rendered Html',()=>{
       expect(component.find('.text-center')).toHaveLength(1);
       expect(component.find('.text-center > div')).toHaveLength(1);
       expect(component.find('.text-center > div').text()).toEqual(props.doc.type);
    });

    it('when set customType',()=>{
        props.doc.customType = 'test custom';
        delete props.doc.type;
        component = shallow(<DocInfo {...props}/>);

        expect(component.find('.text-center > div')).toHaveLength(1);
        expect(component.find('.text-center > div').text()).toEqual(props.doc.customType);
    });

});
