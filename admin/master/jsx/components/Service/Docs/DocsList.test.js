import React from 'react';
import DocsList from './DocsList';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';

const initialState={
    service:{orderDocs:[{id:1},{id:2}],currentOrder:{id:1}}
};

const mockStore=configureStore();
let store;

describe('<DocsList/>',()=>{
    let component,docsList;
    const props={};
    beforeEach(()=>{
        store=mockStore(initialState);
        component=mount(<Provider store={store}><DocsList {...props}/></Provider>);
        docsList=component.find('DocsList');
    });
    it('length',()=>{
        expect(docsList).toHaveLength(1);
    });

    it('componentWillMount',()=>{
        expect(docsList.node.componentWillMount()).toEqual(undefined);
    });

    it('componentWillReceiveProps',()=>{
        expect(docsList.node.componentWillReceiveProps({service:{orderDocs:[{id:1},{id:2}],currentOrder:{id:2}}})).toEqual(undefined);
        expect(docsList.node.componentWillReceiveProps({service:{}})).toEqual(undefined);

    });

    it('render',()=>{
        expect(component.find('DocsList').render().html()).toEqual(component.html());
    });

    it('deleteDoc',()=>{
        docsList.node.deleteDoc(1,()=>{
           expect(docsList.node.props.service.orderDocs).toEqual([{id:2}]);
        });
    });

    it('rendered component',()=>{
        expect(docsList.find('FileInfo')).toHaveLength(initialState.service.orderDocs.length);
    });

});