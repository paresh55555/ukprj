import React from 'react';
import DocTypes from './DocTypes';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store';
import {Provider} from 'react-redux';

const initialState={
    service:{docTypes:[{id:1,type:'test doc1'},{id:2,type:'test doc2'}],currentOrder:{id:1}}
}
const mockStore=configureStore();
let store;

describe('<DocTypes/>',()=>{
     let component,docsType;

    const props={selectedVal:1,onChange:jest.fn()};

    beforeEach(()=>{
        store=mockStore(initialState);
        component=mount(<Provider store={store}><DocTypes {...props}/></Provider>);
        docsType=component.find('DocTypes');
    });

    it('length',()=>{
        expect(docsType).toHaveLength(1);
    });

    it('componentWillMount',()=>{
        expect(docsType.node.componentWillMount()).toEqual(undefined);
    });

    it('render',()=>{
        expect(component.find('DocTypes').render().html()).toEqual(component.html());
    });

    it('getTypes',()=>{
        expect(docsType.node.getTypes()).toEqual(initialState.service.docTypes);
    });

    it('getSelectedValue',()=>{
        expect(docsType.node.getSelectedValue()).toEqual('test doc1');
    });

    it('onChange SQDropdown',()=>{
        const SQDropdown = docsType.find('SQDropdown');
        expect(SQDropdown).toHaveLength(1);
        SQDropdown.simulate('change',()=>{
            expect(props.onChange).toHaveBeenCalled();
        });

    });

    it('rendered components',()=>{
        expect(docsType.find('SQDropdown')).toHaveLength(1);
    });

    it('when docTypes not exist',()=>{
        store=mockStore({});
        component=mount(<Provider store={store}><DocTypes {...props}/></Provider>);
        docsType=component.find('DocTypes');
        expect(docsType.node.getTypes()).toEqual([]);
        expect(docsType.node.getSelectedValue()).toEqual(false);
    })

});