import React from 'react';
import Docs from './Docs';
import {mount} from 'enzyme';
import configureStore from 'redux-mock-store'
import {Provider} from 'react-redux'

const initialState = {
    service:{orderDocs:[{}],currentOrder:{id:1}}
};

const mockStore = configureStore();
let store;

describe("<Docs/>",()=> {

    let component, docs;

    const props = {};

    beforeEach(() => {
        store = mockStore(initialState);
        component = mount(<Provider
            store={store}><Docs {...props} /></Provider>);
        docs = component.find('Docs');
    });

    it('length', () => {
        expect(docs).toHaveLength(1);
    });

    it('componentWillMount',()=>{
       expect(docs.node.componentWillMount()).toEqual(undefined);
    });

    it('render',()=>{
        expect(component.find('Docs').render().html()).toEqual(component.html());
    });

    it('openAddDocs',()=>{
        docs.node.openAddDocs(()=>{
           expect(docs.node.props.service.addDocOpened) .toEqual(true);
        });
    });

    it('componentWillUnmount',()=>{
        docs.node.componentWillUnmount(()=>{
            expect(docs.node.props.service.orderDocs).toEqual([]);
        });
    });

    it('rendered Component',()=>{
       expect(docs.find('ServiceFormRow')).toHaveLength(1);
       expect(docs.find('DocsList')).toHaveLength(1);
       expect(docs.find('.another')).toHaveLength(1);
       expect(docs.find('AddDoc')).toHaveLength(1);
    });

    it('when no currentOrder and orderDocs',()=>{
        store = mockStore(Object.assign({},initialState,{service:{}}));
        component = mount(<Provider
            store={store}><Docs {...props} /></Provider>);
        docs = component.find('Docs');

        expect(docs.find('.another')).toHaveLength(0);
        expect(docs.node.componentWillMount()).toEqual(undefined);
    });


});
