import React, {PropTypes} from 'react';
import {Panel} from "react-bootstrap";

export default function ServicePanel(props) {
    return (
        <div>
            <Panel className={'service'} header={props.header}>{props.children}</Panel>
        </div>
    );
}