import React, {PropTypes} from 'react';

export default function ServiceCheckbox(props) {
    return (
        <div className="checkbox c-checkbox">
            <label>
                <input type="checkbox" checked={props.checked} onChange={props.onChange} />
                <em className="fa fa-check"></em>{props.label}
            </label>
        </div>
    );
}

ServiceCheckbox.propTypes = {
    label: PropTypes.string.isRequired,
    checked: PropTypes.bool.isRequired,
    onChange: PropTypes.func.isRequired,
};