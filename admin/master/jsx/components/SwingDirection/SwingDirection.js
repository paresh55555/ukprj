import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {componentAction,optionAction} from '../../actions';
import {Button} from 'react-bootstrap';
import { ApiOption, ApiComponent } from '../../api';

import ComponentTitle from '../../pages/components/ComponentTitle';
import DeleteDialog from '../Common/DeleteDialog';
import SwingDirectionTrait from './SwingDirectionTrait';
import SwingDirectionOption from './SwingDirectionOption';

class SwingDirection extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: [],
            traits: [],
            autoEditID: null,
        };
        this.saveTrait = this.saveTrait.bind(this);
    }

    componentWillMount(){
        if(this.props.component.options && this.props.component.options.length){
            this.setState({options: this.props.component.options});
        }
        if(this.props.component.traits && this.props.component.traits.length){
            this.setState({traits: this.props.component.traits});
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.option && (nextProps.option.isSaving || nextProps.option.isLoading)) return;
        if(nextProps.cmp && nextProps.cmp.trait && this.props.cmp.trait !== nextProps.cmp.trait){
            let savedTrait = nextProps.cmp.trait.response_data[0],
                traits = this.state.traits
            if(savedTrait.component_id !== this.props.component.id)return;
            if(traits.length && traits.find(function(el,i){return el.name === savedTrait.name})){
                let newTraits = []
                traits.map((trait, i)=>{
                    let newTrait = trait
                    if(trait.name === savedTrait.name){
                        newTrait = savedTrait
                    }
                    newTraits.push(newTrait);
                })
                this.setState({traits: newTraits})
            }else{
                traits.push(savedTrait);
                this.setState({traits: traits})
            }
        }
        if(nextProps.option.isDeleted != this.props.option.isDeleted && nextProps.option.isDeleted &&
            this.state.options.find((option)=>{return option.id === nextProps.option.deletedId})){
            swal("Deleted!", this.props.component.name+" option has been deleted.", "success");
            var newOptions = this.state.options.filter((option)=>{return option.id !== nextProps.option.deletedId})
            this.setState({options: newOptions})
        }else
        if(nextProps.option.isSaved != this.props.option.isSaved && nextProps.option.isSaved){
            this.props.setEditMode(false)
            var savedOption = nextProps.option.savedOption && nextProps.option.savedOption.length && nextProps.option.savedOption[0]
            if(savedOption && savedOption.component_id === this.props.component.id){
                var options = this.state.options;
                savedOption['traits'] = []
                options.push(savedOption);
                this.setState({options: options, autoEditID:savedOption.id})
            }
        }
    }

    addOption(type){
        var newOption = {
            name: type,
            allowDeleted: 1
        }
        this.props.saveOption(this.props.systemID,this.props.component.id, newOption);
        this.props.setEditMode(true)
    }

    removeOption(type){
        var self = this;
        let option = this.state.options.find(function(el,i){return el.name === type});
        var id = option && option.id;
        DeleteDialog({
            text: "Are you sure you want to delete this option? ",
            onConfirm: function(){
                self.props.deleteOption(self.props.systemID, self.props.component.id, id);
                self.props.setEditMode(false)
            }
        })
    }

    saveTrait(trait){
        if(trait && trait.id){
            this.props.ApiUpdateComponentTrait(this.props.systemID,this.props.component.id, trait.id, trait)
        }else{
            this.props.ApiSaveComponentTrait(this.props.systemID,this.props.component.id, trait)
        }
    }

    render() {
        const {options, traits} = this.state;
        const panelImg = traits.find(function(el,i){return el.name === 'panelImg'});
        const leftImg = traits.find(function(el,i){return el.name === 'leftImg'});
        const rightImg = traits.find(function(el,i){return el.name === 'rightImg'});
        let buttons = (buttonState)=>{
            const hasLeft = options.find(function(el,i){return el.name === 'left'})
            const hasRight = options.find(function(el,i){return el.name === 'right'})
            const hasBoth = options.find(function(el,i){return el.name === 'both'})
            return [
                <Button key={0} onClick={hasLeft ? this.removeOption.bind(this,'left') : this.addOption.bind(this,'left')}
                        disabled={buttonState} className="btn btn-default">{hasLeft ? 'Remove left' : 'Add left'}</Button>,
                <Button key={1} onClick={hasRight ? this.removeOption.bind(this,'right') : this.addOption.bind(this,'right')}
                        disabled={buttonState} className="btn btn-default">{hasRight ? 'Remove right' : 'Add right'}</Button>,
                <Button key={2} onClick={hasBoth ? this.removeOption.bind(this,'both') : this.addOption.bind(this,'both')}
                        disabled={buttonState} className="btn btn-default">{hasBoth ? 'Remove both' : 'Add both'}</Button>
            ]
        }

        return (
            <div>
                <ComponentTitle
                    component={this.props.component}
                    traits={this.props.traits}
                    systemID={this.props.systemID}
                    hideProperties={true}
                    cid={this.props.component.id}
                    additionalButtons={buttons}
                    edit_mode={this.props.edit_mode}
                />
                <div className="optionsBox">
                    <div className="fullOption">
                        <SwingDirectionTrait traitType="panel" image={panelImg}
                                             systemID={this.props.systemID}
                                             componentID={this.props.component.id}
                                             onSave={this.saveTrait}
                                             edit_mode={this.props.edit_mode}
                                             setEditMode={this.props.setEditMode} />
                        <SwingDirectionTrait traitType="left" image={leftImg}
                                             systemID={this.props.systemID}
                                             componentID={this.props.component.id}
                                             onSave={this.saveTrait}
                                             edit_mode={this.props.edit_mode}
                                             setEditMode={this.props.setEditMode} />
                        <SwingDirectionTrait traitType="right" image={rightImg}
                                             systemID={this.props.systemID}
                                             componentID={this.props.component.id}
                                             onSave={this.saveTrait}
                                             edit_mode={this.props.edit_mode}
                                             setEditMode={this.props.setEditMode} />
                    </div>
                    <div className="fullOption">
                        {options && options.map((option, i)=>{
                            return <SwingDirectionOption key={option.id} option={option} traits={this.state.traits}
                                                         systemID={this.props.systemID}
                                                         componentID={this.props.component.id}
                                                         edit_mode={this.props.edit_mode}
                                                         autoEditID={option.id === this.state.autoEditID && this.state.autoEditID}
                                                         setEditMode={this.props.setEditMode} />
                        })}
                    </div>
                </div>
            </div>
        );
    }

}

SwingDirection.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiOption, ApiComponent);

export default compose(
    connect(state => ({
        cmp: state.component,
        option: state.option,
    }), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(SwingDirection);