import React, {Component, PropTypes} from 'react';
import {Button} from 'react-bootstrap';

import DoorImage from '../Common/DoorImage'
import ImageUploader from '../Common/ImageUploader/ImageUploader'

class SwingDirectionTrait extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            image: {
                name: props.traitType+'Img'
            },
            isEdit: false,
            showUpload: false
        }
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
    }

    componentWillMount(){
        if(!this.props.image){
            let newTrait = {
                name: this.props.traitType+'Img',
                value: '',
                visible: 1
            }
            this.props.onSave(newTrait)
        }else{
            this.setState({image:this.props.image})
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.image !== nextProps.image){
            this.setState({image:nextProps.image})
        }
    }

    enableEdit(){
        this.setState({isEdit:true})
        this.props.setEditMode(true)
    }

    cancelEdit(){
        this.setState({isEdit:false})
        this.props.setEditMode(false)
    }

    save(){
        this.props.onSave(this.state.image)
        this.cancelEdit()
    }

    openUpload(){
        if(!this.state.isEdit){
            return;
        }
        this.setState({showUpload: true})
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            image['name'] = this.props.traitType+'Img';
            image['value'] = imgUrl;
            image['visible'] = 1;
        }
        this.setState({image:image});
    }

    closeUpload(){
        this.setState({showUpload: false});
    }

    getImage(type){
        return <DoorImage doorType={type} imgUrl={this.state.image && this.state.image.value} onClick={this.openUpload}/>
    }

    getTitle(type){
        switch (type){
            case 'panel':
                return 'Panel'
                break;
            case 'left':
                return 'Left'
                break;
            case 'right':
                return 'Right'
                break;
            default:
                return ''
        }
    }

    render(){
        return(
            <div className="panelOptions">
                <div className="horizontalOption">
                    <div className="horizontalPanelImage">
                        {this.getImage(this.props.traitType)}
                    </div>
                    <div className="thumbnailTitle horizontalPanelImage leftTitle">
                        {this.getTitle(this.props.traitType)}
                    </div>
                </div>
                {this.state.isEdit ?
                    <div className="editSaveDelete">
                        <Button onClick={this.save} className="btn btn-default">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                    </div>
                    :
                    <div className="editSaveDelete">
                        <Button onClick={this.enableEdit} className="btn btn-default" disabled={this.props.edit_mode}>Edit</Button>
                    </div>
                }
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.systemID,
                    component_id: this.props.componentID,
                    trait_id: this.state.image.id,
                    type: 'component',
                    width: 50,
                    height: 120,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </div>
        )
    }
}
// export default connect(state => ({
//     trait: state.component.trait
// }), Object.assign({}, componentAction))(SwingDirectionTrait);
export default SwingDirectionTrait;