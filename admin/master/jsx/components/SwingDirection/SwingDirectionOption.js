import React, {Component, PropTypes} from 'react';
import {Button} from 'react-bootstrap';
import { connect } from 'react-redux';
import {optionAction, optionTraitAction} from '../../actions';

import DoorImage from '../Common/DoorImage'
import Title from '../Common/Title'
import {ApiOption, ApiOptionTrait} from "../../api";
import {compose, withProps} from "recompose";

class SwingDirectionOption extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            title: {},
            isEdit: false,
            showUpload: false
        }
        this.save = this.save.bind(this)
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
    }

    componentWillMount(){
        var callbackFn = this.props.autoEditID && this.props.autoEditID === this.props.option.id ? this.enableEdit : false
        this.generateTraits(this.props.option.traits, callbackFn)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.trait.isSaving) return;
        if(nextProps.trait.isSaved && (this.props.option.id === nextProps.trait.optionId)){
            this.generateTraits(nextProps.trait.traits);
        }
    }

    generateTraits(traits, callbackFn){
        var newTitle={
            name: 'title',
            value: '',
            visible:1
        }
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
        })
        this.setState({title: newTitle}, callbackFn);
    }

    enableEdit(){
        this.setState({isEdit:true})
        this.props.setEditMode(true)
    }

    cancelEdit(){
        this.setState({isEdit:false})
        this.props.setEditMode(false)
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    save(){
        this.setState({isEdit:false});
        var saveTraits = [];
        saveTraits.push(this.state.title);
        this.props.ApiSaveMultipleOptionTraits(
            this.props.systemID,
            this.props.componentID,
            this.props.option.id,
            [],
            saveTraits
        );
        this.props.setEditMode(false)
    }

    getPattern(type){
        let traits = this.props.traits
        const panelImg = traits.find(function(el,i){return el.name === 'panelImg'});
        const leftImg = traits.find(function(el,i){return el.name === 'leftImg'});
        const rightImg = traits.find(function(el,i){return el.name === 'rightImg'});
        let funcCap = function(){return false},
            panel = (i)=>{return <DoorImage key={i} doorType={'panel'} imgUrl={panelImg && panelImg.value} onClick={funcCap} />},
            left = (i)=>{return <DoorImage key={i} doorType={'left'} imgUrl={leftImg && leftImg.value} onClick={funcCap} />},
            right = (i)=>{return <DoorImage key={i} doorType={'right'} imgUrl={rightImg && rightImg.value} onClick={funcCap} />};
        switch (type){
            case 'left':
                return [left, panel, panel, panel].map((func, i)=>{ return func(i) })
                break;
            case 'right':
                return [panel, panel, panel, right].map((func, i)=>{ return func(i) })
                break;
            case 'both':
                return [left, panel, panel, right].map((func, i)=>{ return func(i) })
                break;
            default:
                return ''
        }
    }

    render(){
        return(
            <div className="panelOptions">
                <div className="horizontalOption">
                    <div className="horizontalPanelImage">
                        {this.getPattern(this.props.option.name)}
                    </div>
                    <Title title={this.state.title} cssClass="thumbnailTitle horizontalPanelImage"
                           editing={this.state.isEdit} defaultID={this.props.option.id}
                           onTitleChange={this.onTitleChange}/>
                </div>
                {this.state.isEdit ?
                    <div className="editSaveDelete">
                        <Button onClick={this.save} className="btn btn-default">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                    </div>
                    :
                    <div className="editSaveDelete">
                        <Button onClick={this.enableEdit} className="btn btn-default" disabled={this.props.edit_mode}>Edit</Button>
                    </div>
                }
            </div>
        )
    }
}

const apiActions = Object.assign({}, ApiOption, ApiOptionTrait);

export default compose(
    connect(state => ({
        trait: state.optionTrait
    }), Object.assign({}, optionTraitAction, optionAction)),
    withProps(apiActions),
)(SwingDirectionOption);