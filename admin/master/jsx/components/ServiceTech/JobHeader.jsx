import React, {PropTypes} from 'react';
import {Navbar,Nav, NavItem, Glyphicon} from "react-bootstrap";
import HeaderSidebar from './HeaderSidebar';

export default function JobHeader(props) {

    return (
        <div className={'jobheader'}>
                <Nav bsStyle="pills" >
                    <NavItem >
                    <HeaderSidebar />
                    </NavItem>
                    <NavItem className='text-center'>
                        Jobs
                    </NavItem>
                    <NavItem className='pull-right'>
                            <Glyphicon glyph="glyphicon glyphicon-search" className='pull-right'/> 
                    </NavItem>
                </Nav>
        </div>
    );
}

JobHeader.propTypes = {
   
};