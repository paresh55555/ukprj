import React from 'react';
import {connect} from "react-redux";
import * as ApiServiceJob from '../../api/ApiServiceJob';
import {Row, Col, Grid, NavItem} from "react-bootstrap";
import FontIcon from '../Common/FontIcon';
import PersonAvatar from '../Service/Purchasers/PersonAvatar';
import ServiceButton from '../Service/ServiceButton';
import ItemComponent from './ItemComponent';
import CustomerItem from './CustomerItem';

class JobsDetails extends React.Component{


    render(){
        let {data} = this.props;
        let name = data.first_name +' '+ data.last_name;

        return (

            <div className='item_div'>
                <Grid>
                    <Row >
                        <Col  md={12}>
                            <div className='contentDiv'>
                                <div className='introInfo pull-left'>
                                    <span className='JobNumber'> {data.job_number}</span>
                                    <span className='tech'>Service Tech: {name}</span>
                                    <span className='tech'>Start Date/Time: {data.start_date}|{data.start_time}</span>
                                </div>
                                <div className="jobs-customer-list">
                                    <CustomerItem data={data} />
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}


const mapStateToProps = state => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobsDetails)