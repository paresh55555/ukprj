import React, {PropTypes} from 'react';
import {Row, Col, Grid, NavItem} from "react-bootstrap";
import FontIcon from '../Common/FontIcon';
import PersonAvatar from '../Service/Purchasers/PersonAvatar';
import ServiceButton from '../Service/ServiceButton';

export default function CustomerItem(props) {

    let name = props.data.first_name +' '+ props.data.last_name;
    
    return (
        <div className='item_div items'>
            <Grid>
                <Row >
                    <Col className="no-padding" md={12}>
                    <div className='itemRectagle'>
                        <div className='itemContent pull-left'>
                            <Col xs={2}  className='no-padding'>
                                <PersonAvatar info={props.data}/>
                            </Col>
                            <Col xs={10}  className='info'>
                                <NavItem >
                                   <span className='name'>{name}</span>
                                </NavItem>
                                <NavItem className="info-item">
                                    <FontIcon iconClass={'icon icon-envelope'}/>{props.data.email}
                                </NavItem>
                                <NavItem className="info-item">
                                    <FontIcon iconClass={'icon icon-phone'}/>{props.data.phone}
                                </NavItem>
                                <NavItem className="info-item">
                                    <FontIcon iconClass={'icon icon-location-pin'}/>{props.data.address}
                                </NavItem>
                            </Col>
                           
                        </div>
                    </div>
                    </Col>
                </Row>
            </Grid>
        </div>
    );
}

CustomerItem.propTypes = {
   
};