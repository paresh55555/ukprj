import React, {PropTypes} from 'react';
import {Row, Col, Grid, NavItem} from "react-bootstrap";
import FontIcon from '../Common/FontIcon';
import PersonAvatar from '../Service/Purchasers/PersonAvatar';
import ServiceButton from '../Service/ServiceButton';

export default function ItemComponent(props) {

    const handleClick = () =>{
        if(props.onClick){
            props.onClick()
        }
    }
    
    let name = props.data.first_name +' '+ props.data.last_name;
    
    return (
        <div className='item_div'>
            <Grid>
                <Row >
                    <Col  md={12}>
                    <div className='itemRectagle'>
                        <div className='itemHeader pull-left'>
                            <span className='pull-left jobTitle'>{props.data.job_number}</span> 
                            <span className='pull-right timing'>{props.data.start_time}</span>
                        </div>
                        <div className='itemContent pull-left'>
                            <Col xs={2}  className='no-padding'>
                                <PersonAvatar info={props.data}/>
                            </Col>
                            <Col xs={6}  className='info'>
                                <NavItem >
                                   <span className='name'>{name}</span>
                                </NavItem>
                                <NavItem >
                                    <span className='contacts'>
                                        <FontIcon iconClass={'icon icon-phone'}/>{props.data.phone}
                                    </span>
                                </NavItem>
                            </Col>
                            <Col  xs={4}  className='pull-right text-center button_'>
                                <ServiceButton label={props.button} className='pull-right' onClick={handleClick}/>
                            </Col>
                        </div>
                    </div>
                    </Col>
                </Row>
            </Grid>
        </div>
    );
}

ItemComponent.propTypes = {
   
};