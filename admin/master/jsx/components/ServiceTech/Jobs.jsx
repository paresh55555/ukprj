import React, {Component} from 'react';
import {Row, Col, PageHeader, Tabs, Tab} from "react-bootstrap";
import JobHeader from './JobHeader'
import ServiceDatepicker from '../Service/ServiceDatepicker';
import ServiceFormRow from '../Service/ServiceFormRow';
import FontIcon from '../Common/FontIcon';
import moment from 'moment';
import JobItems from './JobItems';
import JobAddress from './JobAddress';

class Jobs extends Component {

    state = {
        currentJob:{
            start_date:''
        },
        activeTab: 'jobs'
    }

    onDateChange = date => this.setState({
        ...this.state.currentJob,
        start_date: moment(date).format('YYYY-MM-DD')
    })

    renderUser(){
        const name = "Jane Doe"
        return(
            <div className='user_name'>
                <PageHeader className='no-padding no-margin'>
                    <span className='userText'>Welcome!</span> {name}
                </PageHeader>
            </div>
        )
    }
  
    renderDatePicker(){
        return(
           <div className='dateLocator'>
                
                    <Col md={12}>
                        <span className={'pull-left datePicker'} >
                            <ServiceDatepicker onChange={this.onDateChange} value={moment(this.state.start_date)} />
                        </span>
                        <span className={'pull-right pickerIcon'}>
                            <em onClick={() => this.updateTab('jobs')} className="fa fa-navicon spaceIcon"></em> 
                            <FontIcon onClick={() => this.updateTab('map')} iconClass={'icon icon-location-pin'} className='locationIcon'/>
                        </span>
                    </Col >
               
           </div>
        )
    }

    renderJobsList(){

        switch (this.state.activeTab) {

            case 'jobs':
                return <JobItems />
            case 'map':
                return <JobAddress />

        }

    }

    updateTab = key => {

        this.setState({
            activeTab: key
        })

    }


  
    render(){

        return(
            <div className='rendertechJobs'>
                <JobHeader />
                {this.renderUser()}
               {this.renderDatePicker()}
               {this.renderJobsList()}
            </div>
        )
    }
}

export default Jobs;