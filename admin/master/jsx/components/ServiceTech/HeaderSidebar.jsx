import React from 'react';
import { NavDropdown, MenuItem, Image } from 'react-bootstrap';
import FontIcon from '../Common/FontIcon';
import { logout } from '../../services/authService';


class HeaderSidebar extends React.Component {

    state = {
        show:false,
        user_name:'',
        list:'Jobs'

    }

    toggle =()=> {
       this.setState({
           show:!this.state.show, 
       })  
    }

    logoutTab(e){
        e.preventDefault();
        logout()
    }

    componentDidMount(){
        let auth = sessionStorage.getItem('auth_info');
        auth = JSON.parse(auth)
        
        this.setState({
            user_name : auth.userName
        })
    }

    sideBar (){
        let {show, user_name, list} = this.state
        let className = 'sideBar'

        if (show) {
            className = `${className} open`
        }

        return (
            <div className={className}>
                <div className='sideHeader'>
                    <div className='closeSidebar pull-right'><a href='javascript:;' onClick={this.toggle} >X</a></div>
                    <div className='header_ pull-left'>
                        <Image src="https://www.sample-videos.com/img/Sample-jpg-image-500kb.jpg" style={{height:'50px',width:'50px'}} circle />
                        <div className='header_line'>
                            <span className='userName'>{user_name}</span>
                            <span className='profile'><a href='javascript:;'>view profile</a></span>
                        </div>
                    </div>
                </div>
                <div className='sideContent'>
                    <ul>
                        <li>
                            <a href='##'><FontIcon iconClass="fa fa-briefcase" />{list}</a>
                        </li>
                    </ul>
                </div>
                <div className='sideFooter'>
                    <a href='javascript:doSomething();' onClick={this.logoutTab.bind(this)} ><FontIcon iconClass={'fal fa-sign-out'}/>Logout</a>
                </div>
            </div>
        )
    }
    
    render() {
       
        return (
           <div>
               <em className="fa fa-navicon" onClick={this.toggle}></em>
               {this.sideBar()}
            </div>
            );
    }

}

export default HeaderSidebar;
