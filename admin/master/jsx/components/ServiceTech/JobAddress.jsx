import React from 'react';
import {connect} from "react-redux";
import * as ApiServiceJob from '../../api/ApiServiceJob';
import SeviceAddressMaplist from '../Common/Addresses/SeviceAddressMaplist';
import { modifyCustomer } from '../Common/utilFunctions';
import ItemComponent from './ItemComponent';
import { setJobsLoading } from '../../actions/serviceJob';
import Loader from '../Common/Loader';

class JobAddress extends React.Component{

    componentWillMount(){
        this.props.dispatch(setJobsLoading(true))

        ApiServiceJob.ApiGetJobs().then(res => {
           
            this.props.dispatch(setJobsLoading(false))
        });
    }

    renderAddres(jobs, isLoading){

        const modfiedJobs =  jobs.map(
            job => modifyCustomer(job)
        )
        if(isLoading){
            return (<div className='techLoader'><Loader /></div>)
        }else{
            return(
                <div>
                    <SeviceAddressMaplist addressList={modfiedJobs} className='addressMap' renderPopup={
                        index => <ItemComponent data={modfiedJobs[index]} button='detail'/>
                    }/>
                </div> 
                
            )
        }

    }

    render(){
       
        let {jobs, isLoading} = this.props.serviceJob;
        return(

            <div>
               {this.renderAddres(jobs, isLoading)}
            </div>

        )
    }
}


const mapStateToProps = state => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobAddress)