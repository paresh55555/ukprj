import React, {Component} from 'react';
import {connect} from "react-redux";
import ServiceAddressMap from '../Service/Jobs/ServiceAddressMap';
import * as ApiServiceJob from '../../api/ApiServiceJob';
import ItemComponent from './ItemComponent';
import { modifyCustomer } from '../Common/utilFunctions';
import { setJobsLoading } from '../../actions/serviceJob';
import Loader from '../Common/Loader';


class JobItems extends Component {

    componentWillMount(){
        this.props.dispatch(setJobsLoading(true))

        ApiServiceJob.ApiGetJobs().then(res => {
           
            this.props.dispatch(setJobsLoading(false))
        });
    }

    gotoDetail = JobId => {
        this.context.router.push('serviceTechjobDetails/' + JobId);
    }

    renderItemList(jobs, isLoading){
    
        const modifiedJobs = jobs.map(
            job => modifyCustomer(job)
        )
        if(isLoading){
            return (<div className='techLoader'><Loader /></div>)

        }else{
            return(
            
                modifiedJobs.map((job,index) =>{
                    
                    return(<div key={index}>
                            <ItemComponent data={job} button='detail' onClick={() =>this.gotoDetail(job.id)}/>
                        </div>
                    )
                })
              
            )
        }
        

    }
  
    render(){

        let {jobs, isLoading} = this.props.serviceJob;
        
        return(
            <div className=''>
               {this.renderItemList(jobs,isLoading)}
            </div>

        )
    }
}

JobItems.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobItems)