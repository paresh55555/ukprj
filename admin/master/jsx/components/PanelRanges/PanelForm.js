import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, FormGroup, ControlLabel, FormControl, Form } from 'react-bootstrap';

export default class PanelForm extends React.Component {
	constructor() {
      super();
      this.state = {showModal:false}
    }
    _handleChange(key,event){
        this.props.onChange(key,event)
    }
    render() {
        const panel = this.props.panel
        console.log(panel)
        return (
            <Form data-parsley-validate="" data-parsley-group="block-0" noValidate>
                <FormGroup>
                    <ControlLabel>Number of Panels</ControlLabel>
                    <FormControl 
                        type="text"
                        value={panel.panels} 
                        placeholder="Number of Panels" 
                        onChange={this._handleChange.bind(this, 'panels')} 
                        required
                        data-parsley-type="number"
                        pattern="[0-9]+([,\.][0-9]+)?"
                    />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Min Width</ControlLabel>
                    <FormControl 
                        type="text"
                        value={panel.min_width} 
                        placeholder="Min Width" 
                        onChange={this._handleChange.bind(this,'min_width')} 
                        required
                        data-parsley-type="number"
                        pattern="[0-9]+([,\.][0-9]+)?"
                        data-parsley-min={1}
                    />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Max Width</ControlLabel>
                    <FormControl 
                        type="text"
                        value={panel.max_width} 
                        placeholder="Max Width" 
                        onChange={this._handleChange.bind(this,'max_width')} 
                        required
                        data-parsley-type="number"
                        pattern="[0-9]+([,\.][0-9]+)?"
                        data-parsley-min={panel.min_width+1}
                    />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Min Height</ControlLabel>
                    <FormControl 
                        type="text"
                        value={panel.min_height} 
                        placeholder="Min Height" 
                        onChange={this._handleChange.bind(this,'min_height')} 
                        required
                        data-parsley-type="number"
                        pattern="[0-9]+([,\.][0-9]+)?"
                        data-parsley-min={1}
                    />
                </FormGroup>
                <FormGroup>
                    <ControlLabel>Min Width</ControlLabel>
                    <FormControl 
                        type="text"
                        value={panel.max_height} 
                        placeholder="Max Height" 
                        onChange={this._handleChange.bind(this,'max_height')} 
                        required
                        data-parsley-type="number"
                        pattern="[0-9]+([,\.][0-9]+)?"
                        data-parsley-min={panel.min_height+1}
                    />
                </FormGroup>
            </Form>
        );
    }

}

