import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {componentAction,optionAction} from '../../actions';
import {Button} from 'react-bootstrap';
import {SQ_PREFIX} from '../../constant.js'
import ComponentTitle from '../../pages/components/ComponentTitle';
import { ApiOption, ApiComponent } from '../../api';

import ColorOption from './ColorOption'
import ColorGroups from './ColorGroups'

class Color extends React.Component {
    constructor() {
        super();
        self = this;
        this.state = {
            colors:[],
            groups:[],
            isEdit:false,
            selectedGroupID: null,
            isEdited: false,
            selectedIds: []
        };
        this.goBack = this.goBack.bind(this);
        this.addColor = this.addColor.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.selectGroup = this.selectGroup.bind(this);
        this.addPremadeColors = this.addPremadeColors.bind(this);
        this.addPremadeGroups = this.addPremadeGroups.bind(this);
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
        this.changeComponentEditMode = this.changeComponentEditMode.bind(this);
    }
    componentWillMount(){
        var selectedGroup = null;
        if(this.props.data && this.props.data.groups && this.props.data.groups.length && this.state.selectedGroupID === null){
            selectedGroup = this.props.data.groups[0].id;
        }
        if(!this.props.componentDropdownState['id'+this.props.cid]) {
            this.props.generatePropertiesApi(this.props.cid)
        }
        this.setState({selectedGroupID:selectedGroup});
    }

    componentDidMount(){
        window.onbeforeunload = function () {
            localStorage.removeItem('adminSelectedColorsInfo');
        }
        var selectedInfo = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
        if(selectedInfo && selectedInfo[this.props.cid]){
            var selectedGroup = parseInt(selectedInfo[this.props.cid].selectedGroup);
            if(selectedGroup){
                this.selectGroup(selectedGroup);
            }
        }
    }

    addSelected(id) {
        return () => {
            if (!this.props.edit_mode || !this.props.componentDropdownState['id'+this.props.cid].hasDefault) {return}
            const sameId = this.state.selectedIds.find(item=>item === id);
            const selected = this.props.componentDropdownState['id'+this.props.cid].multiples ? this.state.selectedIds : [];
            if (sameId === id) {
                const index = selected.indexOf(id);
                selected.splice(index, 1);
            } else {
                selected.push(id);
            }
            this.setState({selectedIds: selected});
        };
    }

    checkSelection(id) {
        const result = this.state.selectedIds.find(item => {if(item === id){return item}});
        return !!result;
    }

    changeComponentEditMode(status) {
        this.setState({isEdited: status})
    }

    goBack(){
        this.context.router.push(SQ_PREFIX + 'systems/' + this.props.systemID + '/components/');
    }

    componentWillReceiveProps(nextProps){
        if(this.props.option && nextProps.option.isDeleted != this.props.option.isDeleted && nextProps.option.isDeleted){
            swal("Deleted!", "Color option has been deleted.", "success");
        }
        if(this.props.component && nextProps.component.isDeleted != this.props.component.isDeleted && nextProps.component.isDeleted){
            swal("Deleted!", "Group has been deleted.", "success");
        }
        if(nextProps.data && nextProps.data.groups && !nextProps.data.groups.length){
            this.setState({selectedGroupID:null});
        }
        if(!this.props.componentDropdownState['id'+this.props.cid]) {return}
        if(this.props.componentDropdownState['id'+this.props.cid].hasDefault !== nextProps.componentDropdownState['id'+this.props.cid].hasDefault &&
            !nextProps.componentDropdownState['id'+this.props.cid].hasDefault ||
            this.props.componentDropdownState['id'+this.props.cid].multiples !== nextProps.componentDropdownState['id'+this.props.cid].multiples &&
            !nextProps.componentDropdownState['id'+this.props.cid].multiples) {
            this.setState({selectedIds: []})
        }
    }

    addColor(){
        var newOption = {
            name: 'colorOption',
            allowDeleted: 1
        },
        self = this;
        if(this.state.selectedGroupID === null){
            self.props.saveOption(this.props.systemID,this.props.cid, newOption).then(()=>{
                self.props.ApiUpdateColorComponent(self.props.systemID,self.props.cid);
            });
        }else{
            // Add color to group
            this.props.saveOption(this.props.systemID,this.state.selectedGroupID, newOption).then((res)=>{
                self.props.ApiUpdateColorComponent(self.props.systemID, this.props.cid);
            });
        }
    }

    addGroup(){
        // if(!this.state.selectedGroupID && this.state.groups.length){
        //     NotifyAlert('Please select group',{status:'warning'})
        // }
        var data = {
            name: 'ColorGroup',
            type: 'ColorGroup',
            grouped_under: this.props.cid
        },
        self = this;
        this.props.ApiSaveComponent(this.props.systemID, data).then(()=>{
            self.props.ApiUpdateColorComponent(self.props.systemID,self.props.cid);
        });
    }

    addPremadeColors(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.cid,
            colorComponentID: this.props.cid,
            mode: 'color'
        })
    }
    addPremadeGroups(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.cid,
            colorComponentID: this.props.cid,
            mode: 'group'
        });
    }

    selectGroup(id){
        var storageObj = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
        if (storageObj && storageObj[this.props.cid]) {
            storageObj[this.props.cid]['selectedGroup'] = id;
        } else if(storageObj){
            storageObj[this.props.cid] = {selectedGroup:id}
        }else{
            storageObj = {}
            storageObj[this.props.cid] = {selectedGroup:id}
        }
        localStorage.setItem('adminSelectedColorsInfo', JSON.stringify(storageObj));

        this.setState({selectedGroupID:id});
    }

    getContent(colors, groups){
        var content = null,
            buttons = (buttonState)=>{return null},
            self = this;
        const groupsButtons = (
            <span>
                <Button onClick={this.addColor} disabled={this.props.edit_mode} className="btn btn-default">Add Color</Button>
                <Button onClick={this.addGroup} disabled={!!colors.length || this.props.edit_mode} className="btn btn-default">Add Color Group</Button>
                {groups && groups.length ?
                    <Button onClick={this.addSubGroup} disabled={!!colors.length || this.props.edit_mode} className="btn btn-default">Add Sub-Group</Button> : null
                }
            </span>
        )
        const premadeButtons = (
            <span>
                <Button onClick={this.addPremadeGroups} disabled={!!colors.length || this.props.edit_mode} className="btn btn-default rightButtons">Add PreMade Groups</Button>
                <Button onClick={this.addPremadeColors} disabled={this.props.edit_mode} className="btn btn-default rightButtons">Add PreMade Colors</Button>
            </span>
        )
        if(colors && colors.length){
            content = (
                <div>
                    <ComponentTitle
                        component={this.props.data}
                        traits={this.props.traits}
                        cid={this.props.data.id}
                        systemID={this.props.systemID}
                        edit_mode={this.props.edit_mode}
                        additionalButtons={buttons}
                        selectedIds={this.state.selectedIds}
                        changeComponentEditMode={this.changeComponentEditMode}
                    />
                    <div className="optionsBox">
                        {colors.map((color,index) =>{
                            return ([
                                <ColorOption
                                    isCurrentComponentEdit={this.state.isEdited}
                                    traits={color.traits}
                                    key={index} index={index}
                                    optionID={color.id}
                                    systemID={self.props.systemID}
                                    componentID={self.props.cid}
                                    mainComponentID={self.props.cid}
                                    selected={this.checkSelection(color.id)}
                                    addSelected={this.addSelected}
                                    id={color.id}
                                    edit_mode={self.props.edit_mode}/>,
                                (index+1)%5 === 0 ? <div className='clearfix'></div> : null
                            ])
                        })}
                    </div>
                    <div className="editSave">
                        { groupsButtons }
                        { premadeButtons }
                    </div>
                </div>
            )
        } else if(groups && groups.length){
            content = (
                <div>
                    <ComponentTitle
                        component={this.props.data}
                        traits={this.props.traits}
                        cid={this.props.data.id}
                        systemID={this.props.systemID}
                        edit_mode={this.props.edit_mode}
                        additionalButtons={buttons}
                        selectedIds={this.state.selectedIds}
                        changeComponentEditMode={this.changeComponentEditMode}
                    />
                    <ColorGroups
                        isCurrentComponentEdit={this.state.isEdited}
                        data={groups}
                        onSelectGroup={self.selectGroup}
                        onAddColor={self.addColor}
                        onAddGroup={self.addGroup}
                        selectedGroupID={self.state.selectedGroupID}
                        systemID={self.props.systemID}
                        componentID={self.props.cid}
                        mainComponentID={self.props.cid}
                        component={this.props.component}
                        isEdit={this.state.isEdit}
                        edit_mode={self.props.edit_mode}/>
                </div>
            )
        } else {
            buttons = (buttonState)=>{
                return (<span>{ groupsButtons }{ premadeButtons }</span>)
            }
            content = (
                <div>
                    <ComponentTitle
                        component={this.props.data}
                        traits={this.props.traits}
                        cid={this.props.data.id}
                        systemID={this.props.systemID}
                        edit_mode={this.props.edit_mode}
                        additionalButtons={buttons}
                        selectedIds={this.state.selectedIds}
                        changeComponentEditMode={this.changeComponentEditMode}
                    />
                    <div className="optionsBox">
                        No options available
                    </div>
                </div>
            )
        }
        return content;
    }

    render() {
        var self = this,
            colors = this.props.data.options,
            groups = this.props.data.groups;
        const content = this.getContent(colors, groups);
        return (
            <div>
                { content }
            </div>
        );
    }

}

Color.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiOption, ApiComponent);

export default compose(
connect(state => ({
    componentDropdownState: state.option.componentDropdownState,
    option: state.option,
    component: state.component,
}), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(Color);
