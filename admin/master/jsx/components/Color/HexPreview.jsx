import React, {Component} from 'react';

class HexPreview extends Component{
    constructor() {
        super();
        this.state = {
            hex: ''
        }
        this.change = this.change.bind(this);
    }

    componentWillMount(){
        if(this.props.image && this.props.image.value){
            this.setState({hex: this.props.image.value});
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.image && this.props.image.value !== nextProps.image.value){
            this.setState({hex:nextProps.image.value})
        }
    }

    componentDidMount(){
        this.initEditable();
    }

    componentDidUpdate(){
        this.initEditable();
    }


    initEditable(){
        var self = this,
            hex = this.props.image && this.props.image.name === 'hex' && this.props.image.value ? this.props.image.value : "",
            imageID = this.props.image.id ? this.props.image.id : 'tmp-'+this.props.defaultID,
            imageElement = $('.hexPreview.item-'+imageID);
        if(imageElement.data('editable')){
            imageElement.editable('option', 'disabled', !this.props.editing);
            imageElement.addClass('editable editable-click')
        }else{
            imageElement.editable({
                disabled: !self.props.editing,
                inputclass: 'sq-popover-input',
                value: hex,
                emptytext: '',
                autotext: 'never',
                highlight: false,
                placeholder: 'FFFFFF',
                success: function(response, newValue) {
                    self.change(newValue);
                }
            });
            imageElement.on('shown', function(e, editable) {
                setTimeout(function(){
                    var $popover = editable.container.$form.parents('.popover');
                    $popover.css('left', '4%');
                    $popover.find('.arrow').css('left','25px');
                },0);
            });
        }
    }

    change(newValue){
        this.setState({hex: newValue});
        this.props.onHexChange(newValue);
    }

    render(){
        const hex = this.state.hex ? this.state.hex : 'FFFFFF';
        const imageID = this.props.image.id ? this.props.image.id : 'tmp-'+this.props.defaultID;
        const {addSelected, id} = this.props;
        return(
            <div className="horizontalImage" onClick={addSelected(id)}>
                {this.props.isCurrentComponentEdit && this.props.selected ? <div className="demoColorSelected"></div> : null}
                <div style={{background: "#"+hex.replace('#','')}} className={"hexPreview thumbnail2 thinBorder2 item-"+imageID}
                onClick={e => !this.props.editing || this.props.componentDisabled ? e.preventDefault() : null}></div>
            </div>
        );
    }
}

HexPreview.propTypes = {
};

export default HexPreview;