import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Button } from 'react-bootstrap';
import { componentAction, optionAction } from '../../actions';
import ColorSubGroup from './ColorSubGroup';
import ColorOption from './ColorOption';
import { ApiOption, ApiComponent } from '../../api';

class ColorSubGroups extends React.Component {
    constructor() {
        super()
        this.state = {
            isEdited: false,
            selectedIds: []
        }
        this.onSelectSubGroup = this.onSelectSubGroup.bind(this);
        this.addColor = this.addColor.bind(this);
        this.addSubGroup = this.addSubGroup.bind(this);
        this.addPremadeColors = this.addPremadeColors.bind(this);
        this.addPremadeSubGroups = this.addPremadeSubGroups.bind(this);
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
        this.changeComponentEditMode = this.changeComponentEditMode.bind(this);
    }

    componentWillMount(){
        if(!this.selectPreselectedSubGroup(this.props.data)) {
            this.setSelectedSubGroup(this.props.data, this.props);
        }
        if(!this.props.componentDropdownState['id'+this.props.componentID]) {
            this.props.generatePropertiesApi(this.props.componentID)
        }
    }

    componentWillReceiveProps(nextProps){
        if(!this.selectPreselectedSubGroup(nextProps.data)) {
            this.setSelectedSubGroup(nextProps.data, nextProps);
        }
        if(!this.props.componentDropdownState['id'+this.props.componentID]) {return}
        if(this.props.componentDropdownState['id'+this.props.componentID].hasDefault !== nextProps.componentDropdownState['id'+this.props.componentID].hasDefault &&
            !nextProps.componentDropdownState['id'+this.props.componentID].hasDefault ||
            this.props.componentDropdownState['id'+this.props.componentID].multiples !== nextProps.componentDropdownState['id'+this.props.componentID].multiples &&
            !nextProps.componentDropdownState['id'+this.props.componentID].multiples) {
            this.setState({selectedIds: []})
        }
    }

    addSelected(id) {
        return () => {
            if (!this.props.edit_mode || !this.props.componentDropdownState['id'+this.props.componentID].hasDefault) {return}
            const sameId = this.state.selectedIds.find(item=>item === id);
            const selected = this.props.componentDropdownState['id'+this.props.componentID].multiples ? this.state.selectedIds : [];
            if (sameId === id) {
                const index = selected.indexOf(id);
                selected.splice(index, 1);
            } else {
                selected.push(id);
            }
            this.setState({selectedIds: selected});
        };
    }

    checkSelection(id) {
        const result = this.state.selectedIds.find(item => {if(item === id){return item}});
        return !!result;
    }

    changeComponentEditMode(status) {
        this.setState({isEdited: status})
    }

    selectPreselectedSubGroup(subGroups){
        var selectedInfo = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
        if(selectedInfo && selectedInfo[this.props.mainComponentID]){
            var selectedSubGroup = parseInt(selectedInfo[this.props.mainComponentID].selectedSubGroup);
            if(selectedSubGroup && subGroups.find((e)=>{return e.id === selectedSubGroup})){
                this.props.onSelectSubGroup(selectedSubGroup);
                return true;
            }
        }
        return false;
    }

    setSelectedSubGroup(subGroups, props){
        var self = this,
            foundID = false;
        subGroups.map((subGroup, index)=>{
            if(subGroup.id === props.selectedSubGroupID){
                foundID = subGroup.id;
            }
        })
        if(foundID){
            this.props.onSelectSubGroup(foundID);
        }else if(subGroups && subGroups.length){
            this.props.onSelectSubGroup(subGroups[0].id);
        }else{
            this.props.onSelectSubGroup(null);
        }
    }

    onSelectSubGroup(id){
        this.props.onSelectSubGroup(id);
    }

    addColor(){
        var newOption = {
            name: 'colorOption',
            allowDeleted: 1
        },
        self = this;
        if(this.props.selectedSubGroupID !== null){
            // Add color to sub-group
            this.props.saveOption(this.props.systemID,this.props.selectedSubGroupID, newOption).then(()=>{
                self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
            });
        }
    }

    addSubGroup(){
        this.props.onAddSubGroup();
    }

    addPremadeColors(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.selectedSubGroupID,
            colorComponentID: this.props.mainComponentID,
            mode: 'color'
        })
    }
    addPremadeSubGroups(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.groupID,
            colorComponentID: this.props.mainComponentID,
            mode: 'subGroup'
        })
    }

    getContent(subGroups){
        var self = this,
            result = null,
            disabled = this.props.edit_mode;
        var subGroupsContent = (
            <div className="optionsBox">
                {subGroups.map((subGroup,index) =>{
                    return (
                        <ColorSubGroup
                            traits={subGroup.traits}
                            selected={subGroup.id === self.props.selectedSubGroupID}
                            onSelectSubGroup={this.onSelectSubGroup}
                            key={index}
                            index={index}
                            subGroupID={subGroup.id}
                            systemID={self.props.systemID}
                            mainComponentID={self.props.mainComponentID}
                            edit_mode={self.props.edit_mode}/>
                    )
                })}
            </div>
        )
        var additionalContent = subGroups.map((subGroup,index) =>{
            if(subGroup.id === self.props.selectedSubGroupID){
                let colors = subGroup.options;
                if(colors && colors.length){
                    return (
                        <div key={index}>
                            <div className="editSave">
                                <Button onClick={this.addColor} disabled={disabled} className="btn btn-default">Add Color</Button>
                                <Button onClick={this.addSubGroup} disabled={disabled} className="btn btn-default">Add Sub-Group</Button>
                                <Button onClick={this.addPremadeSubGroups} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Sub-Groups</Button>
                                <Button onClick={this.addPremadeColors} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Colors</Button>
                            </div>
                            <div className="optionsBox">
                                {colors.map((color,index) =>{
                                    return ([
                                        <ColorOption
                                            isCurrentComponentEdit={this.props.isCurrentComponentEdit}
                                            traits={color.traits}
                                            key={index} index={index}
                                            optionID={color.id}
                                            systemID={self.props.systemID}
                                            componentID={subGroup.id}
                                            selected={this.checkSelection(color.id)}
                                            addSelected={this.addSelected}
                                            id={color.id}
                                            mainComponentID={self.props.mainComponentID}
                                            edit_mode={self.props.edit_mode}/>,
                                        (index+1)%5 === 0 ? <div className='clearfix'></div> : null
                                    ])
                                })}
                            </div>
                        </div>
                    )
                } else {
                    return (
                        <div className="editSave" key={index}>
                            <Button onClick={this.addColor} disabled={disabled} className="btn btn-default">Add Color</Button>
                            <Button onClick={this.addSubGroup} disabled={disabled} className="btn btn-default">Add Sub-Group</Button>
                            <Button onClick={this.addPremadeSubGroups} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Sub-Groups</Button>
                            <Button onClick={this.addPremadeColors} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Colors</Button>
                        </div>
                    )
                }
            }
        })

        result = (
            <div>
                {subGroupsContent}
                {additionalContent}
            </div>
        )
        return result;
    }

    render(){
        var self = this,
            subGroups = this.props.data;
        const content = this.getContent(subGroups);
        return( content )
    }
}

ColorSubGroups.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiOption,ApiComponent);

export default compose(
    connect(state => ({
        componentDropdownState: state.option.componentDropdownState,
        option: state.option,
    }), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(ColorSubGroups);
