import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {Button} from 'react-bootstrap';
import {componentAction,optionAction, optionTraitAction} from '../../actions';
import { ApiOption, ApiOptionTrait, ApiComponent} from '../../api';

import ImageUploader from '../Common/ImageUploader/ImageUploader'
import Note from '../Common/Note'
import Title from '../Common/Title'
import ComponentImage from '../Common/ComponentImage'
import DeleteDialog from '../Common/DeleteDialog'
import HexPreview from './HexPreview'

class ColorOption extends React.Component {
    constructor(){
        super()
        this.state = {
            title: {},
            image: {},
            notes: [],
            selected: {},
            deletedNotes: [],
            backupTraits: null,
            mode: 'imgUrl',
            isEdit: false,
            showUpload: false
        }
        this.save = this.save.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.addNote = this.addNote.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
        this.onNoteChange = this.onNoteChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.deleteOption = this.deleteOption.bind(this);
        this.useImage = this.useImage.bind(this);
        this.onHexChange = this.onHexChange.bind(this);
        this.initHex = this.initHex.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
    }

    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.trait.isSaving) return;
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
    }

    generateTraits(traits){
        var self = this,
            newTitle={
                name: 'title',
                value: '',
                visible:1
            },
            newSelected={
                name: 'selected',
                value: this.props.selected,
                visible:1
            },
            newNotes = [],
            newImage = {},
            mode = 'imgUrl';
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
            if(trait.name === 'note'){
                newNotes.push(trait);
            }
            if((trait.name === 'imgUrl' || trait.name === 'hex') && trait.value !== newImage.value){
                newImage = trait;
            }
            if(trait.name === 'selected'){
                newSelected = trait;
            }
        })
        if(newImage && newImage.name){
            mode = newImage.name
        }
        if(!newImage.id){
            let newImage = {
                name: 'imgUrl',
                value: 'tmp',
                visible: 1
            }
            this.props.ApiSaveOptionTrait(
                this.props.systemID,
                this.props.componentID,
                this.props.optionID,
                newImage
            ).then(()=>{
                self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
            })
        }else {
            this.setState({title: newTitle, notes: newNotes, image: newImage, selected: newSelected, mode: mode});
        }
    }

    enableEdit(){
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(Object.assign({},note));
        })
        var currentItems = {
            title: Object.assign({},this.state.title),
            image: Object.assign({},this.state.image),
            notes: newNotes
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title,
            oldImage = backupTraits.image,
            oldNotes = backupTraits.notes,
            mode = 'imgUrl';
        if(oldImage && oldImage.name){
            mode = oldImage.name
        }
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle, image: oldImage, mode: mode, notes: oldNotes});
        this.props.setEditMode(false)
    }

    addNote(){
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(note);
        })
        newNotes.push({
            name:'note',
            value:'',
            visible:1
        })
        this.setState({notes:newNotes});
    }

    deleteNote(){
        var notes = this.state.notes;
        var deletedNotes = this.state.deletedNotes;
        var deletedNote = notes.pop();
        if(deletedNote.id){
            deletedNotes.push(deletedNote);
        }
        this.setState({notes:notes, deletedNotes:deletedNotes});
    }

    onNoteChange(index, value){
        var notes = this.state.notes;
        notes[index].value = value;
        this.setState({notes:notes});
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    save(){
        var self = this;
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [];
        saveTraits.push(this.state.title);
        if(this.state.image && this.state.image.value){
            saveTraits.push(this.state.image);
        }
        this.state.notes.map((note)=>{
            saveTraits.push(note);
        })
        this.props.ApiSaveMultipleOptionTraits(
            this.props.systemID,
            this.props.componentID,
            this.props.optionID,
            this.state.deletedNotes,
            saveTraits
        ).then(()=>{
            self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
            self.props.setEditMode(false)
        });

    }

    useImage(){
        this.setState({mode: 'imgUrl'})
    }

    initHex(){
        var self = this,
            hexButton = $('.hexButton.item-'+this.props.optionID);
        hexButton.editable({
            disabled: !self.state.isEdit,
            inputclass: 'sq-popover-input',
            value: '',
            emptytext: '',
            autotext: 'never',
            highlight: false,
            placeholder: 'FFFFFF',
            success: function(response, newValue) {
                self.onHexChange(newValue);
                self.setState({mode: 'hex'});
            }
        });
        hexButton.on('shown', function(e, editable) {
            setTimeout(function(){
                var $popover = editable.container.$form.parents('.popover');
                $popover.css('left', '4%');
                $popover.find('.arrow').css('left','25px');
            },0);
        });
    }

    onHexChange(newValue){
        var image = this.state.image;
        image['value'] = newValue;
        image['name'] = 'hex';
        image['visible'] = 1;
        this.setState({image:image});
    }

    deleteOption(){
        var self = this;
        DeleteDialog({
            text: "Are you sure you want to delete this color? ",
            onConfirm: function(){
                self.props.deleteOption(self.props.systemID,self.props.componentID, self.props.optionID).then(()=>{
                    self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
                });
                self.props.setEditMode(false)
            }
        })
    }

    openUpload() {
        if(!this.state.isEdit){
            return;
        }
        this.setState({showUpload: true});
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            image['name'] = 'imgUrl';
            image['value'] = imgUrl;
            image['visible'] = 1;
        }
        this.setState({image:image});
    }

    closeUpload(){
        this.setState({showUpload: false})
    }

    render(){
        const {addSelected, id, selected} = this.props;
        return(
            <div className="thumbOption">
                { this.state.mode === 'imgUrl' ?
                    <div className="horizontalImage" onClick={addSelected(id)}>
                        {this.props.isCurrentComponentEdit && selected ? <div className="demoColorSelected"></div> : null}
                        <ComponentImage image={this.state.image} width={121} height={121} onClick={this.openUpload} iconClass="thumbnail2"/>
                    </div>
                    :
                    <HexPreview isCurrentComponentEdit={this.props.isCurrentComponentEdit} image={this.state.image} selected={selected} addSelected={addSelected} id={id} componentDisabled={this.props.edit_mode} editing={this.state.isEdit} defaultID={this.props.optionID} onHexChange={this.onHexChange} />
                }
                <div className="text-center">
                    <Title title={this.state.title} cssClass="thumbnailTitle" editing={this.state.isEdit} defaultID={this.props.optionID} onTitleChange={this.onTitleChange}/>
                    {
                        this.state.notes.map((note, index) => {
                            return <Note key={index} note={note} cssClass="thumbnailNote" editing={this.state.isEdit} index={index} onNoteChange={this.onNoteChange} />
                        })
                    }
                </div>
                { this.state.isEdit ?
                    <div className="editSaveDelete">
                        <Button onClick={this.save} className="btn btn-default thumbButton">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default thumbButton">Cancel</Button>
                        <Button onClick={this.addNote} className="btn btn-default thumbButton">Add Note</Button>
                        { this.state.notes.length ?
                            <Button onClick={this.deleteNote} className="btn btn-default thumbButton">Delete Note</Button> : null
                        }
                        { this.state.mode === 'imgUrl' ?
                            <div onMouseOver={this.initHex} className={"btn btn-default thumbButton hexButton item-"+this.props.optionID}>Use Hex</div>
                            :
                            <Button onClick={this.useImage} className="btn btn-default thumbButton">Use Image</Button>
                        }
                        <Button onClick={this.deleteOption} className="btn btn-danger noBackground thumbButton">Delete</Button>
                    </div>
                    :
                    <div className="editSaveDelete">
                        <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className="btn btn-default thumbButton">Edit</Button>
                        <Button onClick={this.deleteOption} disabled={this.props.edit_mode} className="btn btn-danger noBackground thumbButton">Delete</Button>
                    </div>
                }
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.systemID,
                    component_id: this.props.componentID,
                    option_id: this.props.optionID,
                    trait_id: this.state.image.id,
                    type: 'option',
                    width: 121,
                    height: 121,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </div>
        )
    }
}

ColorOption.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiOption, ApiOptionTrait, ApiComponent);

export default compose(
    connect(state => ({
        trait: state.optionTrait,
        option: state.option,
    }), Object.assign({}, componentAction, optionAction, optionTraitAction)),
    withProps(apiActions),
)(ColorOption);