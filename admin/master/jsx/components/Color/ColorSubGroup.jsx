import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {componentAction} from '../../actions';
import ImageUploader from '../Common/ImageUploader/ImageUploader'
import ComponentImage from '../Common/ComponentImage'
import DeleteDialog from '../Common/DeleteDialog'
import Title from '../../components/Common/Title'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ColorSubGroup extends React.Component {
    constructor() {
        super()
        this.state = {
            title: {},
            image: {},
            backupTraits: {},
            isEdit: false,
            showUpload: false
        }
        this.delete = this.delete.bind(this);
        this.selectSubGroup = this.selectSubGroup.bind(this);
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
    }
    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
    }

    generateTraits(traits){
        var self = this,
            newTitle={
                name: 'title',
                type: 'component',
                value: ''
            },
            newImage = {};
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
            if(trait.name === 'imgUrl' && trait.value !== newImage.value){
                newImage = trait;
            }
        })
        if(!newImage.id){
            let newImage = {
                name: 'imgUrl',
                value: 'tmp',
                visible: 1
            }
            this.props.ApiSaveComponentTrait(
                this.props.systemID,
                this.props.subGroupID,
                newImage
            ).then(()=>{
                self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
            })
        }else {
            this.setState({title: newTitle, image: newImage});
        }
    }

    enableEdit(){
        this.selectSubGroup();
        var currentItems = {
            title: Object.assign({},this.state.title),
            image: Object.assign({},this.state.image),
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        this.props.setEditMode(true);
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title,
            oldImage = backupTraits.image;
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle, image: oldImage});
        this.props.setEditMode(false)
    }

    delete(){
        var self = this,
            subGroupName = this.state.title && this.state.title.value ? this.state.title.value : "< Add Title >";
        DeleteDialog({
            text: 'Are you sure you want to delete "' + subGroupName + '" sub-group?',
            onConfirm: function(){
                self.props.ApiDeleteComponent(self.props.systemID, self.props.subGroupID, true).then(()=>{
                    self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
                })
                self.props.setEditMode(false)
                var storageObj = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
                if(storageObj && storageObj[self.props.mainComponentID && storageObj[self.props.mainComponentID]['selectedSubGroup'] === self.props.subGroupID]){
                    delete storageObj[self.props.mainComponentID]['selectedSubGroup'];
                    localStorage.setItem('adminSelectedColorsInfo', JSON.stringify(storageObj));
                }
            }
        })
    }

    selectSubGroup(){
        this.props.onSelectSubGroup(this.props.subGroupID)
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    save(){
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [],
            self = this;
        saveTraits.push(this.state.title);
        if(this.state.image && this.state.image.value){
            saveTraits.push(this.state.image);
        }
        this.props.ApiSaveComponentTitleTraits(
            this.props.systemID,
            this.props.subGroupID,
            [],
            saveTraits
        ).then(()=>{
            self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
        });
        self.props.setEditMode(false)
    }

    openUpload() {
        this.setState({showUpload: true});
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            image['name'] = 'imgUrl';
            image['value'] = imgUrl;
            image['visible'] = 1;
        }
        this.setState({image:image});
    }

    closeUpload(){
        this.setState({showUpload: false})
    }

    render(){
        var self = this;
        const titleClass = (this.props.selected) ? "" : "subColorMenuNotSelected";
        return(
            <div className="subColorMenu">
                <div className={titleClass}>
                    <ComponentImage image={this.state.image} width={90} height={90} onClick={(e)=> {self.state.isEdit ? self.openUpload() : (!this.props.edit_mode ? self.selectSubGroup() : null)}} iconClass="subColorThumbnail"/>
                    <div className="thumbnailTitle">
                        <Title title={this.state.title} cssClass="thumbnailTitle" editing={this.state.isEdit} defaultID={this.props.subGroupID} onTitleChange={this.onTitleChange}/>
                    </div>
                </div>
                {this.state.isEdit ?
                    <div className="editSaveMenu">
                        <Button onClick={this.save} className="btn btn-default ">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                        <Button onClick={this.delete} className="btn btn-danger noBackground delete">Delete</Button>
                    </div>
                    :
                    <div className="editSaveMenu">
                        <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className="btn btn-default">Edit</Button>
                        <Button onClick={this.delete} disabled={this.props.edit_mode} className="btn btn-danger noBackground delete">Delete</Button>
                    </div>
                }
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.systemID,
                    component_id: this.props.subGroupID,
                    trait_id: this.state.image.id,
                    type: 'component',
                    width: 90,
                    height: 90,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </div>
        )
    }
}

ColorSubGroup.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({}), Object.assign({}, componentAction)),
    withProps(apiActions),
)(ColorSubGroup);