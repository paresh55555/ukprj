import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {SQ_PREFIX} from '../../constant.js'
import {componentAction,optionAction} from '../../actions';
import ColorOption from './ColorOption'
import ColorGroup from './ColorGroup'
import ColorSubGroups from './ColorSubGroups'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ColorGroups extends React.Component {
    constructor() {
        super()
        this.state = {
            selectedSubGroupID: null,
            selectedIds: []
        }
        this.addColor = this.addColor.bind(this);
        this.addGroup = this.addGroup.bind(this);
        this.addSubGroup = this.addSubGroup.bind(this);
        this.addPremadeColors = this.addPremadeColors.bind(this);
        this.addPremadeGroups = this.addPremadeGroups.bind(this);
        this.addPremadeSubGroups = this.addPremadeSubGroups.bind(this);
        this.onSelectGroup = this.onSelectGroup.bind(this);
        this.selectSubGroup = this.selectSubGroup.bind(this);
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
    }
    componentWillMount(){
        if(!this.selectPreselectedGroup()){
            this.setSelectedGroup(this.props.data, this.props);
        }
        if(!this.props.componentDropdownState['id'+this.props.componentID]) {
            this.props.generatePropertiesApi(this.state.componentID)
        }
    }

    componentWillReceiveProps(nextProps){
        if(!this.selectPreselectedGroup()){
            this.setSelectedGroup(nextProps.data, nextProps);
        }
        if(!this.props.componentDropdownState['id'+this.props.componentID]) {return}
        if(this.props.componentDropdownState['id'+this.props.componentID].hasDefault !== nextProps.componentDropdownState['id'+this.props.componentID].hasDefault &&
            !nextProps.componentDropdownState['id'+this.props.componentID].hasDefault ||
            this.props.componentDropdownState['id'+this.props.componentID].multiples !== nextProps.componentDropdownState['id'+this.props.componentID].multiples &&
            !nextProps.componentDropdownState['id'+this.props.componentID].multiples) {
            this.setState({selectedIds: []})
        }
    }

    addSelected(id) {
        return () => {
            if (!this.props.edit_mode || !this.props.componentDropdownState['id'+this.props.componentID].hasDefault) {return}
            const sameId = this.state.selectedIds.find(item=>item === id);
            const selected = this.props.componentDropdownState['id'+this.props.componentID].multiples ? this.state.selectedIds : [];
            if (sameId === id) {
                const index = selected.indexOf(id);
                selected.splice(index, 1);
            } else {
                selected.push(id);
            }
            this.setState({selectedIds: selected});
        };
    }

    checkSelection(id) {
        const result = this.state.selectedIds.find(item => {if(item === id){return item}});
        return !!result;
    }

    selectPreselectedGroup(){
        var selectedInfo = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
        if(selectedInfo && selectedInfo[this.props.componentID]){
            var selectedGroup = parseInt(selectedInfo[this.props.componentID].selectedGroup);
            if(selectedGroup){
                this.props.onSelectGroup(selectedGroup);
                return true;
            }
        }
        return false;
    }

    setSelectedGroup(groups, props){
        var self = this,
            foundID = false;
        groups.map((group, index)=>{
            if(group.id === props.selectedGroupID){
                foundID = group.id;
            }
        })
        if(foundID){
            this.props.onSelectGroup(foundID);
        }else if(groups && groups.length){
            this.props.onSelectGroup(groups[0].id);
        }else{
            this.props.onSelectGroup(null);
        }
    }

    addColor(){
        this.props.onAddColor();
    }
    addGroup(){
        this.props.onAddGroup();
    }
    onSelectGroup(id){
        this.props.onSelectGroup(id);
    }

    addSubGroup(){
        if(!this.props.selectedGroupID && this.props.data.length){
            NotifyAlert('Please select group',{status:'warning'})
        }
        var data = {
            name: 'ColorSubGroup',
            type: 'ColorSubGroup',
            grouped_under: this.props.selectedGroupID
        },
        self = this
        this.props.ApiSaveComponent(this.props.systemID, data).then(()=>{
            self.props.ApiUpdateColorComponent(self.props.systemID,self.props.componentID);
        });
    }
    selectSubGroup(id){
        var storageObj = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
        if (storageObj && storageObj[this.props.componentID]) {
            storageObj[this.props.componentID]['selectedSubGroup'] = id;
        }
        localStorage.setItem('adminSelectedColorsInfo', JSON.stringify(storageObj));
        this.setState({selectedSubGroupID:id});
    }

    addPremadeColors(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.selectedGroupID,
            colorComponentID: this.props.mainComponentID,
            mode: 'color'
        })
    }
    addPremadeGroups(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.componentID,
            colorComponentID: this.props.mainComponentID,
            mode: 'group'
        })
    }
    addPremadeSubGroups(){
        this.props.setPremadeMode({
            enable: true,
            systemID: this.props.systemID,
            componentID: this.props.selectedGroupID,
            colorComponentID: this.props.mainComponentID,
            mode: 'subGroup'
        })
    }

    getContent(groups){
        var self = this,
            result = null,
            disabled = this.props.edit_mode;
        var groupsContent = (
            <div className="optionsBox">
                {groups.map((group,index) =>{
                    return (
                        <ColorGroup
                            traits={group.traits}
                            selected={group.id === self.props.selectedGroupID}
                            onSelectGroup={self.onSelectGroup}
                            key={index}
                            index={index}
                            groupID={group.id}
                            systemID={self.props.systemID}
                            mainComponentID={self.props.mainComponentID}
                            edit_mode={self.props.edit_mode} />
                    )
                })}
            </div>
        )
        var additionalContent = groups.map((group,index) =>{
            if(group.id === self.props.selectedGroupID){
                let colors = group.options;
                let subGroups = group.subGroups;
                if(colors && colors.length){
                    return (
                        <div key={index}>
                            <div className="editSave">
                                <Button onClick={this.addGroup} disabled={disabled} className="btn btn-default">Add Group</Button>
                                <Button onClick={this.addPremadeGroups} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Groups</Button>
                            </div>
                            <div className="optionsBox">
                                {colors.map((color,index) =>{
                                    return ([
                                        <ColorOption
                                            isCurrentComponentEdit={this.props.isCurrentComponentEdit}
                                            traits={color.traits}
                                            key={index} index={index}
                                            optionID={color.id}
                                            systemID={self.props.systemID}
                                            componentID={group.id}
                                            selected={this.checkSelection(color.id)}
                                            addSelected={this.addSelected}
                                            id={color.id}
                                            mainComponentID={self.props.mainComponentID}
                                            edit_mode={self.props.edit_mode}/>,
                                        (index+1)%5 === 0 ? <div className='clearfix'></div> : null
                                    ])
                                })}
                            </div>
                            <div className="editSave">
                                <Button onClick={this.addColor} disabled={disabled} className="btn btn-default">Add Color</Button>
                                <Button onClick={this.addSubGroup} disabled={!!colors.length || disabled} className="btn btn-default">Add Sub-Group</Button>
                                <Button onClick={this.addPremadeSubGroups} disabled={!!colors.length || disabled} className="btn btn-default rightButtons">Add PreMade Sub-Groups</Button>
                                <Button onClick={this.addPremadeColors} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Colors</Button>
                            </div>
                        </div>
                    )
                }else if(subGroups && subGroups.length){
                    return (
                        <div key={index}>
                            <div className="editSave">
                                <Button onClick={this.addGroup} disabled={disabled} className="btn btn-default">Add Group</Button>
                                <Button onClick={this.addPremadeGroups} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Groups</Button>
                            </div>
                            <ColorSubGroups
                                isCurrentComponentEdit={this.props.isCurrentComponentEdit}
                                data={subGroups}
                                onSelectSubGroup={self.selectSubGroup}
                                onAddColor={self.addColor}
                                onAddSubGroup={self.addSubGroup}
                                selectedSubGroupID={self.state.selectedSubGroupID}
                                systemID={self.props.systemID}
                                componentID={self.props.componentID}
                                mainComponentID={self.props.mainComponentID}
                                groupID={self.props.selectedGroupID}
                                isEdit={self.props.isEdit}
                                edit_mode={self.props.edit_mode}/>
                        </div>
                    )
                } else {
                    return (
                        <div className="editSave" key={index}>
                            <Button onClick={this.addColor} disabled={disabled} className="btn btn-default">Add Color</Button>
                            <Button onClick={this.addGroup} disabled={disabled} className="btn btn-default">Add Color Group</Button>
                            <Button onClick={this.addSubGroup} disabled={disabled} className="btn btn-default">Add Sub-Group</Button>
                            <Button onClick={this.addPremadeSubGroups} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Sub-Groups</Button>
                            <Button onClick={this.addPremadeGroups} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Groups</Button>
                            <Button onClick={this.addPremadeColors} disabled={disabled} className="btn btn-default rightButtons">Add PreMade Colors</Button>
                        </div>
                    )
                }
            }
        })

        result = (
            <div>
                {groupsContent}
                {additionalContent}
            </div>
        )
        return result;
    }

    render(){
        var groups = this.props.data;

        const content = this.getContent(groups);
        return( content )
    }
}

ColorGroups.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        option: state.option,
        componentDropdownState: state.option.componentDropdownState
    }), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(ColorGroups);