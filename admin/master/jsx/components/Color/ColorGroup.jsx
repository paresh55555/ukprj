import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {componentAction} from '../../actions';
import Title from '../Common/Title'
import DeleteDialog from '../Common/DeleteDialog'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ColorGroup extends React.Component {
    constructor() {
        super()
        this.state = {
            title: {},
            backupTraits: {},
            isEdit: false
        }
        this.delete = this.delete.bind(this);
        this.selectGroup = this.selectGroup.bind(this);
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
    }
    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
    }

    generateTraits(traits){
        var newTitle={
                name: 'title',
                type: 'component',
                value: ''
            };
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
        })
        this.setState({title: newTitle});
    }

    enableEdit(){
        this.selectGroup();
        var currentItems = {
            title: Object.assign({},this.state.title),
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title;
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle});
        this.props.setEditMode(false)
    }

    delete(){
        var self = this,
            groupName = this.state.title && this.state.title.value ? this.state.title.value : "< Add Color Group >";
        DeleteDialog({
            text: 'Are you sure you want to delete "' + groupName + '" group?',
            onConfirm: function(){
                self.props.ApiDeleteComponent(self.props.systemID, self.props.groupID, true).then(()=>{
                    self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
                })
                self.props.setEditMode(false)
                var storageObj = JSON.parse(localStorage.getItem('adminSelectedColorsInfo'));
                if(storageObj && storageObj[self.props.mainComponentID] && storageObj[self.props.mainComponentID]['selectedGroup'] === self.props.groupID){
                    delete storageObj[self.props.mainComponentID]['selectedGroup'];
                    localStorage.setItem('adminSelectedColorsInfo', JSON.stringify(storageObj));
                }
            }
        })
    }

    selectGroup(){
        if(this.props.edit_mode){
            return;
        }
        this.props.onSelectGroup(this.props.groupID)
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    save(){
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [],
            self = this;
        saveTraits.push(this.state.title);
        this.props.ApiSaveComponentTitleTraits(
            this.props.systemID,
            this.props.groupID,
            [],
            saveTraits
        ).then(()=>{
            self.props.ApiUpdateColorComponent(self.props.systemID,self.props.mainComponentID);
            self.props.setEditMode(false)
        });
    }

    render(){
        const buttonClass = "colorGroupButton" + ((this.props.selected) ? "" : "-NotSelected");
        return(
            <div className="colorGroupMenu">
                <div onClick={this.selectGroup} disabled={this.props.edit_mode && !this.state.isEdit} className={"groupTitle mb-sm mr-sm btn btn-success btn-outline "+buttonClass}>
                    <Title title={this.state.title} editing={this.state.isEdit} defaultText="< Add Color Group >" defaultID={this.props.groupID} onTitleChange={this.onTitleChange}/>
                </div>
                {this.state.isEdit ?
                    <div className="editSaveMenu">
                        <Button onClick={this.save} className="btn btn-default ">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                        <Button onClick={this.delete} className="btn btn-danger noBackground delete">Delete</Button>
                    </div>
                :
                    <div className="editSaveMenu">
                        <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className="btn btn-default">Edit</Button>
                        <Button onClick={this.delete} disabled={this.props.edit_mode} className="btn btn-danger noBackground delete">Delete</Button>
                    </div>
                }
            </div>
        )
    }
}

ColorGroup.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({}), Object.assign({}, componentAction)),
    withProps(apiActions),
)(ColorGroup);