import React from 'react';
import reactCSS from 'reactcss'
import { SketchPicker } from 'react-color'
let value = `rgba(224, 77, 77, 1)`;
class ColorInput extends React.Component{

	constructor(props){
		super(props)
	
	this.state = {
    displayColorPicker: false,
    color: '#ffffff',
  };
  this.handleClick = this.handleClick.bind(this);
  this.handleClose = this.handleClose.bind(this);
  this.handleChange = this.handleChange.bind(this);
}

  handleClick(){
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };

  handleClose(){
    this.setState({ displayColorPicker: false })
  };

  handleChange(color){
    color = `rgba(${color.rgb.r}, ${color.rgb.g}, ${color.rgb.b}, ${color.rgb.a})`
  	this.props.onChange(color)
  };

	render(){


		const styles = {
       
        swatch: {
          padding: '5px',
          background: '#fff',
          borderRadius: '1px',
          boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
          display: 'inline-block',
          cursor: 'pointer',
        },
        popover: {
          position: 'absolute',
          zIndex: '2',
        },
        cover: {
          position: 'fixed',
          top: '0px',
          right: '0px',
          bottom: '0px',
          left: '0px',
        },
    };

      const value = this.props.value
      var match = value.match(/^rgba\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3}),\s*(\d*(?:\.\d+)?)\)$/);
      const rgba =  match ? {r: Number(match[1]),g: Number(match[2]),b: Number(match[3]),a: Number(match[4])} : value;
		return(
			<div>
        <div onClick={ this.handleClick }>
          <div className="form-control">
          {value}
          </div>
        </div>
        { this.state.displayColorPicker ? <div style={ styles.popover }>
          <div style={ styles.cover } onClick={ this.handleClose }/>
          <SketchPicker color={ rgba } onChange={ this.handleChange } />
        </div> : null }

      </div>
			)
	}
}

export default ColorInput;