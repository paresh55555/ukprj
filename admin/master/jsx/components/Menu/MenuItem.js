import React from 'react'
import ContentEditable from  'react-contenteditable';

class MenuItem extends React.Component {
	constructor(){
		super()
		this.onClick = this.onClick.bind(this);
	}
	onClick(){
		this.props.onClicks(this.props.index)
	}
	setMenu(i,e){
	}
	render(){
		const {menu, index, color} = this.props;
		const isactive = this.props.isActive;
	return(
     	<li className='active'>
     		<a style={{background:color.never_selected_background_color,color:color.never_selected_font_color}} onClick={this.props.editMenu.bind(this,menu.option_id)}>
     			<div>{menu.value}</div>
     		</a>
	    </li>
	  )
}
}
export default MenuItem;