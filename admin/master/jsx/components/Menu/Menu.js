import React from 'react'
import MenuItem from './MenuItem'
import MenuColor from './MenuColor'
import {SQ_PREFIX} from '../../constant.js'
class Menu extends React.Component {
	constructor(props) {
		super(props);
		self = this;
		const menu = []
		const color= {
			never_selected_background_color:"#e7e7e7",
			never_selected_font_color:"#ffffff",
			selected_background_color:"#e7e7e7",
			selected_font_color:"#ffffff",
			visited_background_color:"#e7e7e7",
			visited_font_color:"#ffffff"
		}
		this.state = {active:0,menus:menu,color:color,traitids:[]};
		this.changeColor= this.changeColor.bind(this);
		if(!props.traits.length){
			this.setTraits(color,[]);
		}

	}

	componentWillReceiveProps(nextProps){
			

		if(this.props.traits !== nextProps.traits && nextProps.traits.length){
			this.generateTraits(nextProps.traits)
		}
		if(!!nextProps.options.length){
			//setTimeout(()=>{
			this.generateOptions(nextProps.options)

			//},1000)
		}
	}
	generateTraits(traits){
		let newtrait={};
		let traitids = {}
		traits.map((trait)=>{
			newtrait[trait.name] = trait.value;
			traitids[trait.name]=trait.id
		})
		this.setState({color:newtrait,traitids:traitids})
		this.setTraits(newtrait,traitids)
	}
	generateOptions(options){

		let newtrait=[];
		let traitids = {}
		for(let key = 0;key < options.length;key++){
			for(let i=0;i < options[key].traits.length; i++){
				newtrait.push(options[key].traits[i])
			}
		}

		this.setState({menus:newtrait})
	}
	changeColor(key,value){
		let color = this.state.color;
		color[key] = value;
		this.setState({color:color})
		this.setTraits(color,this.state.traitids)

	}
	setTraits(color,traitids){
		let traits = []
		for (let key in color) {
			let newt = {
		   	name:key,
		   	value:color[key],
		   	type:'component',
		   	visible:1
		   }
		  if(traitids[key]){
		  	newt['id'] = traitids[key];
		  }
		  traits.push(newt)
		}
		this.props.onChange(traits)
	}
	render(){
	return(
		<div className="panel menu-panel">
		<MenuColor color={this.state.color} onChange={this.changeColor} />
		<hr className="bluebar"/>
			<div className="menu-list">
	     <ul role="tablist" className="nav nav-tabs nav-justified manu-panel-list">
	     {!!this.state.menus ? this.state.menus.map((menu,index) =>{
		     return (
		     	<MenuItem 
		     	menu={menu} 
		     	editMenu={this.props.editOption}
		     	key={index} 
		     	index={index} 
		     	color={this.state.color}
		     	isActive={index === this.state.active} 
		     />
		     	)
	     	}) : null}
	     
	     </ul>
	    

	     <div className="addMenuBtn">
	     	<a onClick={this.props.editOption.bind(this,'new')}>+</a>
	     </div>
	     </div>
	     <hr className="bluebar" />
	  </div>
	  )
}
}

Menu.contextTypes = {
        router: React.PropTypes.object.isRequired
      };
export default Menu;