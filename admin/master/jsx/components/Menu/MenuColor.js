import React from 'react';
import { SketchPicker } from 'react-color'
import { Grid, Row, Col, Button, FormGroup,FormControl} from 'react-bootstrap';
import ColorInput from './ColorInput'

class MenuColor extends React.Component{
	render(){

		return(
			<div>
			<Row>
				<Col sm={12}>
					<h4>Selected: </h4>
				</Col>
				<Col sm={7}>
					<FormGroup controlId="formHorizontalEmail">
			      <Col sm={5}>
			        Font Color
			      </Col>
			      <Col sm={7}>
			        <ColorInput value={this.props.color.selected_font_color} onChange={this.props.onChange.bind(this,'selected_font_color')} />
			      </Col>
			    </FormGroup>
			    <FormGroup controlId="formHorizontalEmail">
			      <Col sm={5}>
			        Background Color
			      </Col>
			      <Col sm={7}>
			        <ColorInput value={this.props.color.selected_background_color} onChange={this.props.onChange.bind(this,'selected_background_color')} />
			      </Col>
			    </FormGroup>
				</Col>
				<Col sm={5}>
					<div className="ExampleMenu">
						<div style={{padding:10,background:this.props.color.selected_background_color,color:this.props.color.selected_font_color}}>Example Menu</div>
					</div>
				</Col>
			</Row>

			<Row>
				<Col sm={12}>
					<h4>Never Selected: </h4>
				</Col>
				<Col sm={7}>
					<FormGroup controlId="formHorizontalEmail">
			      <Col sm={5}>
			        Font Color
			      </Col>
			      <Col sm={7}>
			        <ColorInput value={this.props.color.never_selected_font_color} onChange={this.props.onChange.bind(this,'never_selected_font_color')} />
			      </Col>
			    </FormGroup>
			    <FormGroup controlId="formHorizontalEmail">
			      <Col sm={5}>
			        Background Color
			      </Col>
			      <Col sm={7}>
			        <ColorInput value={this.props.color.never_selected_background_color} onChange={this.props.onChange.bind(this,'never_selected_background_color')} />
			      </Col>
			    </FormGroup>
				</Col>
				<Col sm={5}>
					<div className="ExampleMenu">
						<div style={{padding:10,background:this.props.color.never_selected_background_color,color:this.props.color.never_selected_font_color}}>Example Menu</div>
					</div>
				</Col>
			</Row>

			<Row>
				<Col sm={12}>
					<h4>Visited: </h4>
				</Col>
				<Col sm={7}>
					<FormGroup controlId="formHorizontalEmail">
			      <Col sm={5}>
			        Font Color
			      </Col>
			      <Col sm={7}>
			        <ColorInput value={this.props.color.visited_font_color} onChange={this.props.onChange.bind(this,'visited_font_color')} />
			      </Col>
			    </FormGroup>
			    <FormGroup controlId="formHorizontalEmail">
			      <Col sm={5}>
			        Background Color
			      </Col>
			      <Col sm={7}>
			        <ColorInput value={this.props.color.visited_background_color} onChange={this.props.onChange.bind(this,'visited_background_color')} />
			      </Col>
			    </FormGroup>
				</Col>
				<Col sm={5}>
					<div className="ExampleMenu">
						<div style={{padding:10,background:this.props.color.visited_background_color,color:this.props.color.visited_font_color}}>Example Menu</div>
					</div>
				</Col>
			</Row>

      </div>
			)
	}
}

export default MenuColor;