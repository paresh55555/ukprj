import React from 'react'
import ContentEditable from  'react-contenteditable';
import { Grid,FormGroup,FormControl,ControlLabel, Row, Col, Panel, Button, Table, Alert, Modal,Form } from 'react-bootstrap';

class AddMenu extends React.Component {
	constructor(){
		super()
		const color= {
			never_selected_background_color:"#e7e7e7",
			never_selected_font_color:"#ffffff",
			selected_background_color:"#e7e7e7",
			selected_font_color:"#ffffff",
			visited_background_color:"#e7e7e7",
			visited_font_color:"#ffffff"
		} 
		this.state={trait:{},color:color}
		this.saveMenu = this.saveMenu.bind(this);
		this.onChange = this.onChange.bind(this);
	}
	componentWillReceiveProps(nextProps){
		if(this.props.currentTrait !== nextProps.currentTrait){
			this.setState({trait:nextProps.currentTrait});
		}
		if(this.props.traits !== nextProps.traits && nextProps.traits.length){
			this.generateTraits(nextProps.traits)
		}
	}
	generateTraits(traits){
		let newtrait={};
		let traitids = {}
		traits.map((trait)=>{
			newtrait[trait.name] = trait.value;
			traitids[trait.name]=trait.id
		})
		this.setState({color:newtrait})
	}
	saveMenu(){
		let trait = this.state.trait
		if(trait.id){
			this.props.onUpdate(trait)
		}
		else{
			let option = {
				traits:[trait]
			}
			this.props.onSave(trait.value , option)
		}

	}
	onChange(e){
		let trait = this.state.trait;
		trait.value = e.target.value
		this.setState({trait:trait})
	}
	render(){
	return(
		<Grid>
    	<Row>
				<div className="panel-default panel">
					<div className="panel-heading">
						
					</div>
					<div className="panel-body">
						<FormGroup controlId="formHorizontalEmail" style={{overflow:'auto'}}>
				      <Col componentClass={ControlLabel} sm={2}>
				        Menu Text
				      </Col>
				      <Col sm={10}>
				        <FormControl type="text" value={this.state.trait.value} placeholder="Menu Text" onChange={this.onChange}  />
				      </Col>
				    </FormGroup>
				    <hr className="bluebar" />
				    <div className="">
				    <Row>
							<Col sm={12}>
								<h4>Selected: </h4>
							</Col>
							<Col sm={5}>
								<div className="ExampleMenu">
									<div style={{padding:10,background:this.state.color.selected_background_color,color:this.state.color.selected_font_color}}>{this.state.trait.value || null}</div>
								</div>
							</Col>
						</Row>

						<Row>
							<Col sm={12}>
								<h4>Never Selected: </h4>
							</Col>
							
							<Col sm={5}>
								<div className="ExampleMenu">
									<div style={{padding:10,background:this.state.color.never_selected_background_color,color:this.state.color.never_selected_font_color}}>{this.state.trait.value|| null}</div>
								</div>
							</Col>
						</Row>

						<Row>
							<Col sm={12}>
								<h4>Visited: </h4>
							</Col>
							<Col sm={5}>
								<div className="ExampleMenu">
									<div style={{padding:10,background:this.state.color.visited_background_color,color:this.state.color.visited_font_color}}>{this.state.trait.value || null}</div>
								</div>
							</Col>
						</Row>
				    </div>
					</div>
					<div className="panel-footer">
						<div className="pull-left">
							<button type="button" onClick={this.saveMenu} className="btn btn-success">Save</button>
							<button type="button" className="btn btn-default" onClick={this.props.goBack}>Cancel</button>
						</div>
						
					</div>
				</div>


     	
 			 

 		</Row>
 	</Grid>
	  )
}
}
export default AddMenu;