import React from 'react'
import {Col} from 'react-bootstrap';
import {Link} from 'react-router';
import {API_URL} from '../../constant.js';
class LinkBox extends React.Component {
    render() {
        const { url, title, params, className } = this.props;
        const additionalClasses = className ? className : '';
        return (
            <Link to={{ pathname: url }}>
                <div className="panel widget">
                    <div className={`panel-body text-center text-muted ${additionalClasses}`}>
                        <p className="mb0">{title}</p>
                    </div>
                </div>
            </Link>
        )
    }
}
export default LinkBox;