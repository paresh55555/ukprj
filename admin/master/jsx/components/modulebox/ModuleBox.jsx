import React from 'react'
import {Col} from 'react-bootstrap';
import { Link } from 'react-router';
import {API_URL} from '../../constant.js';
class ModuleBox extends React.Component {
	render(){
		const {url, title} = this.props;
	return(
		<Col md={4} >
        <Link to={this.props.link}>
          <div className="panel panel-primary">
         <div className="panel-content">
         <img src={API_URL+'/'+url} alt="Image" className="img-responsive center-block" />
         </div>
         <div className="panel-footer bg-info-dark bt0 clearfix btn-block">
            <span className="pull-left">{title}</span>
            <span className="pull-right">
               <em className="fa fa-chevron-circle-right"></em>
            </span>
         </div>
         
         {/* END panel */}
      </div>
          </Link>
   		</Col>
	  )
}
}
export default ModuleBox;