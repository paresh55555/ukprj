import React from 'react';
import { Grid, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import ColorInput from '../Menu/ColorInput'
import ImageUploader from '../../components/Common/ImageUploader/ImageUploader'
class BrandingForm extends React.Component {
    constructor() {
      super();
      this.state = {showUpload:false}
      this.onUpload = this.onUpload.bind(this)
      this.openUpload = this.openUpload.bind(this)
      this.closeUpload = this.closeUpload.bind(this)
      
    }

    onUpload(imgUrl){
        this.props.onChangeColor('banner_url',imgUrl)
    }
    openUpload(){
      this.setState({showUpload:true})
    }
    closeUpload(){
        this.setState({showUpload: false})
    }
    render() {
      const {branding} = this.props
        return (
          <form className="form-vertical">
              <div className="form-group clearfix">
                  <label className=" control-label">Banner</label> <br />
                  {!branding.banner_url ?<div className="banner  thinBorder2 icon-cloud-upload " onClick={this.openUpload}></div>
                  :<img src={'/'+branding.banner_url} onClick={this.openUpload}/> }
              </div>
              <FormGroup>
                <ControlLabel>Background Color</ControlLabel>
                <ColorInput value={branding.background_color || ''} onChange={this.props.onChangeColor.bind(this,'background_color')} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>First Link Text</ControlLabel>
                <FormControl type="text" placeholder="Return To Site" value={branding.first_link_text} onChange={this.props.onChange.bind(this,'first_link_text')} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>First Link Color</ControlLabel>
                <ColorInput value={branding.first_link_color || ''} onChange={this.props.onChangeColor.bind(this,'first_link_color')} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>First Link URL</ControlLabel>
                <FormControl type="text" placeholder="Return To URL" value={branding.first_link_url} onChange={this.props.onChange.bind(this,'first_link_url')} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Second Link Text</ControlLabel>
                <FormControl type="text" placeholder="Contact Us" value={branding.second_link_text} onChange={this.props.onChange.bind(this,'second_link_text')} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Second Link Color</ControlLabel>
                <ColorInput value={branding.second_link_color || ''} onChange={this.props.onChangeColor.bind(this,'second_link_color')} />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Second Link URL</ControlLabel>
                <FormControl type="text" placeholder="panoramicdoors.com/contact/" value={branding.second_link_url} onChange={this.props.onChange.bind(this,'second_link_url')} />
              </FormGroup>
              <FormGroup>
                  <Button bsStyle="primary" onClick={this.props.onSave}>{
                    this.props.isSaving ? 'Saving...' : 'Save'
                  }</Button>
                  <Button bsStyle="default" className="mh" onClick={this.props.onCancel}>Cancel</Button>
                  <Button bsStyle="danger" className="pull-right" onClick={this.props.onDelete}>Delete</Button>
              </FormGroup>
              <ImageUploader show={this.state.showUpload} config={{
                    width: 310,
                    height: 65,
                    type:'siteBranding',
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
          </form>
            );
    }

}

export default BrandingForm;
