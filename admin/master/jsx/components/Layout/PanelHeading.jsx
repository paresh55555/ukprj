import React from 'react';

const PanelHeading = (props) => {
	return <div className="bg-info-dark defaultWidth">
		<span className="optionTitle item-515 editable editable-click editable-disabled">{props.title}</span>
	</div>
}

export default PanelHeading;