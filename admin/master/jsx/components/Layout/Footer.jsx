import React from 'react';

class Footer extends React.Component {
    render() {
    	const year = new Date().getFullYear()
        return (
            <footer>
                <span>&copy; {year} - Sales Quoter</span>
            </footer>
        );
    }

}

export default Footer;
