import React from 'react';
import { Link } from 'react-router';
import FontIcon from "../Common/FontIcon";

function LinkList(props){
	return (
		<li key={props.link.route} className={ props.routeActive(props.link.route) ? 'active' : '' }>
        <Link to={props.link.route} title={props.link.title}>
        {!!props.link.icon && <FontIcon iconClass={props.link.icon}/>  }
        <span><h5>{props.link.title}</h5></span>
        </Link>
        {props.children}
    </li>
		)
}

export default (props) => {
	return (
		<ul className="nav">
		{props.links.map((link,index)=>{
				return (link.type && link.type === 'heading' ?
                    <li key={link.title} className="nav-heading ">
                        <span data-localize="sidebar.heading.HEADER">{link.title}</span>
                    </li>
				:
                    <LinkList key={index} link={link} routeActive={props.routeActive} />
				)
		})
		}
		
    </ul>
    )
}