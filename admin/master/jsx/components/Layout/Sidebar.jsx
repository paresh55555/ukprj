import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import pubsub from 'pubsub-js';
import { Collapse } from 'react-bootstrap';
import SidebarRun from './Sidebar.run';
import { SQ_PREFIX } from '../../constant';
import {systemAction, authAction} from '../../actions';
import {ApiLogin} from '../../api';
import {ApiSystems} from '../../api';
import SideBarLink from './SideBarLink';

class Sidebar extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            userBlockCollapse: false,
            collapse: {
                singleview: this.routeActive(['singleview']),
                submenu: this.routeActive(['submenu'])
            }
        };
        this.pubsub_token = pubsub.subscribe('toggleUserblock', () => {
            this.setState({
                userBlockCollapse: !this.state.userBlockCollapse
            });
        });
    };

    componentDidMount() {
        SidebarRun();
    }
    componentWillMount(){
        const {dispatch, getAuth} = this.props
        getAuth(dispatch);
    }
   
    componentWillUnmount() {
        // React removed me from the DOM, I have to unsubscribe from the pubsub using my token
        pubsub.unsubscribe(this.pubsub_token);
    }

    routeActive(paths) {
        paths = Array.isArray(paths) ? SQ_PREFIX + paths : [SQ_PREFIX + paths];
        let currentPath = this.props.routing.locationBeforeTransitions.pathname.replace(/\/components|\/panelRanges|\/pricing|\/parts/i, '');

        for (let p in paths) {
            if (currentPath == '/'+paths[p] || currentPath.toLowerCase().indexOf(paths[p].toLowerCase()) > -1)
                return true;
        }
        return false;
    }

    toggleItemCollapse(stateName) {
        var newCollapseState = {};
        for (let c in this.state.collapse) {
            if (this.state.collapse[c] === true && c !== stateName)
                this.state.collapse[c] = false;
        }
        this.setState({
            collapse: {
                [stateName]: !this.state.collapse[stateName]
            }
        });
    }
    render() {
        const {permissions} = this.props.auth && this.props.auth.data || {};
        let links = authAction.filterSidebarLinks(permissions);
        return (
            <aside className='aside'>
                { /* START Sidebar (left) */ }
                <div className="aside-inner">
                    <nav data-sidebar-anyclick-close="" className="sidebar">
                        <SideBarLink links={links} routeActive={this.routeActive.bind(this)}/>
                    </nav>
                </div>
                { /* END Sidebar (left) */ }
            </aside>
            );
    }

}


Sidebar.contextTypes = {
    router: React.PropTypes.object.isRequired
};
const mapDispatch = (dispatch) => {
    return Object.assign({}, systemAction, ApiLogin, dispatch)
}
export default connect(
    state => ({
        system: state.system,
        auth: state.auth
    }, mapDispatch)  )(Sidebar);
