import React, {PropTypes} from 'react';

export default function PageTitle(props) {
    return (
        <h3>{props.title}</h3>
    );
}

PageTitle.propTypes = {
    title: PropTypes.string.isRequired
};