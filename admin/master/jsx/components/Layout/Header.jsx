import React from 'react';
import pubsub from 'pubsub-js';
import HeaderRun from './Header.run'
import { NavDropdown, MenuItem } from 'react-bootstrap';
import BrandingComponent from './HeaderComponent/BrandingComponent'
import NotificationsComponent from './HeaderComponent/NotificationsComponent'
import UserMenuComponent from './HeaderComponent/UserMenuComponent'


class Header extends React.Component {

    componentDidMount() {

        HeaderRun();

    }
    toggleUserblock(e) {
        e.preventDefault();
        pubsub.publish('toggleUserblock');
    }
    
    render() {
        const ddAlertTitle = (
            <span>
                <em className="icon-bell"></em>
                <span className="label label-danger">11</span>
            </span>
        )
        return (
            <header className="topnavbar-wrapper">
                { /* START Top Navbar */ }
                <nav role="navigation" className="navbar topnavbar innernav">
                         <BrandingComponent />
                           <ul className="nav navbar-nav">
                            <li>
                                { /* Button used to collapse the left sidebar. Only visible on tablet and desktops */ }
                                <a href="#" data-trigger-resize="" data-toggle-state="aside-collapsed" className="hidden-xs">
                                    <em className="fa fa-navicon"></em>
                                </a>
                                { /* Button to show/hide the sidebar on mobile. Visible on mobile only. */ }
                                <a href="#" data-toggle-state="aside-toggled" data-no-persist="true" className="visible-xs sidebar-toggle">
                                    <em className="fa fa-navicon"></em>
                                </a>
                            </li>
                        </ul>
                     { /* END Nav wrapper */ }
                     <div className="user_header">
                        <ul className="nav navbar-nav navbar-right">
                            <li><NotificationsComponent /></li> 
                            <li><UserMenuComponent /></li> 
                        </ul>
                    </div>

                </nav>
                { /* END Top Navbar */ }
            </header>
            );
    }

}

export default Header;
