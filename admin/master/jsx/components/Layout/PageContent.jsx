import React, {PropTypes} from 'react';
import {Grid, Row, Col} from "react-bootstrap";

export default function PageContent(props) {
    return (
        <Grid fluid>
            <Row>
                <Col lg={ 12 }>
                    {props.children}
                </Col>
            </Row>
        </Grid>
    );
}

PageContent.propTypes = {
};