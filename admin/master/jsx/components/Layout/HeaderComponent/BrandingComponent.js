import React from 'react';
import { Image} from "react-bootstrap";

export default class BrandingComponent extends React.Component{
	render(){
		
		
		return(
			<div className="navbar-header">
                <a href="#/" className="navbar-brand">
                  <Image src="/SQ-admin/img/sqlogo.jpg" className="innerlogo"/> <h2 className="logotext">Sales Quoter</h2>
                </a>
            </div>
            
			)
	}
}