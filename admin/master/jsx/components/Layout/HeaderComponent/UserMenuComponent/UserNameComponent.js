import React from 'react';
import { DropdownButton, MenuItem } from 'react-bootstrap';
import {logout} from '../../../../services/authService.js';
import MaterialIcon from "material-icons-react";

export default class UserNameComponent extends React.Component{

	_logout(e){
        e.preventDefault();
        logout()
    }

	render(){

		let dropIcon = <div className="user expan_more"><a className="username">Logout</a><MaterialIcon icon="expand_more" /></div>
		return(
			<div className='NavDropdown'>
				  <DropdownButton
				    bsStyle="default"
				    title={dropIcon}
				    noCaret
				    id="dropdown-no-caret"
				  >
				    <MenuItem onClick={this._logout.bind(this)}>Logout</MenuItem>
				  </DropdownButton>
			</div>
			)
	}
}