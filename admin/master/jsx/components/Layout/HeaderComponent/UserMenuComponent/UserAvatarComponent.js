import React from 'react';
import { Image } from 'react-bootstrap';

export default class UserAvatarComponent extends React.Component{
	render(){
		return(<div className="inneravatar"> <Image src="/img/mb-sample.jpg" circle className="avtarimg"/></div>)
	}
}