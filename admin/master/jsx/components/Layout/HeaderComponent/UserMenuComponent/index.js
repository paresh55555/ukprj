import React from 'react';
import UserAvatarComponent from './UserAvatarComponent'
import UserNameComponent from './UserNameComponent'


export default class HeaderComponent extends React.Component{
	render(){
		return(
			<div className='headerComponent'>
			<UserAvatarComponent />
			<UserNameComponent />
			</div>
			)
	}
}