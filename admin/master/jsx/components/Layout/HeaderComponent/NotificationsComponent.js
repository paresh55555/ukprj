import React from 'react';
import MaterialIcon from "material-icons-react";

export default class NotificationsComponent extends React.Component{
	render(){
		return(
			<a href="" className='notification_Icon'><MaterialIcon icon="notifications_none" /></a>
			)
	}
}