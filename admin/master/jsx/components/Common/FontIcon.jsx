import React, {PropTypes} from 'react';

export default function FontIcon(props) {
    return (
        <span className={props.className} onClick={props.onClick}><i className={"fa "+props.iconClass}></i></span>
    );
}

FontIcon.propTypes = {
    iconClass: PropTypes.string.isRequired
};