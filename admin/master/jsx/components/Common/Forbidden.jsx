import React from 'react';

class Forbidden extends React.Component {
    render() {
        return (
            <div className="abs-center wd-xl">
                <div className="text-center mb-xl">
                    <div className="text-lg mb-lg">403</div>
                    <p className="lead m0">Forbidden</p>
                    <p>You don't have enough permission.</p>
                </div>
                <div className="input-group mb-xl">
                    <input type="text" placeholder="Try with a search" className="form-control" />
                    <span className="input-group-btn">
             <button type="button" className="btn btn-default">
                <em className="fa fa-search"></em>
             </button>
          </span>
                </div>
                <ul className="list-inline text-center text-sm mb-xl">
                    <li><a href="/dashboard" className="text-muted">Go to App</a>
                    </li>
                    <li className="text-muted">|</li>
                    <li><a href="/login" className="text-muted">Login</a>
                    </li>
                    <li className="text-muted">|</li>
                    <li><a href="/register" className="text-muted">Register</a>
                    </li>
                </ul>
            </div>
        )
    }
}

export default Forbidden;

