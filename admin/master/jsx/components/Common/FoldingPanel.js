import React, { Component, PropTypes } from 'react';

export default class FoldingPanel extends React.Component {
    constructor(){
        super()
        this.state = {
            show: false
        }
    }
    toggleState(){
        this.setState({show: this.state.show ? false : true})
    }
    render(){
        return (
            <div>
                <div className="bg-info-dark defaultWidth clickable" onClick={this.toggleState.bind(this)}>
                    <span className="optionTitle">{this.state.show ? '▼' : '▶'} {this.props.title}</span>
                </div>
                {this.state.show &&
                    <div className="editSave">
                        {this.props.content}
                    </div>
                }
                <div className="space"></div>
            </div>
        )
    }
}
FoldingPanel.propTypes = {
    title: PropTypes.string.isRequired,
    // content: PropTypes.array.isRequired
}