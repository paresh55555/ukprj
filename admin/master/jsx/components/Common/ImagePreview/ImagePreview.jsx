import React from 'react';

export default class ImagePreview extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			previewImage: {
				src: this.props.src
			}
		}
		this.openDialog = this.openDialog.bind(this);
		this.imageUpload = this.imageUpload.bind(this);
		this.validate = this.validate.bind(this);
	}

	componentDidMount() {
		require('./ImagePreview.css');
	}

	componentWillReceiveProps(nextProps){
		let previewImage = {
			src: nextProps.src
		}
		this.setState({previewImage: previewImage})
	}

	openDialog () {
		this.refs.fileInput.getDOMNode().click();
	}

	imageUpload (e) {
		e.preventDefault();

		let file = e.target.files[0];

		if(!this.validate(file.name)) {
			alert('file type not supported');
			return;
		}

		let reader = new FileReader();

		reader.onloadend = () => {
			this.setState({
				previewImage: {
					src: reader.result
				}
			})
		}

		reader.readAsDataURL(file);
	}

	validate (filename) {
		var valid = /(\.jpg|\.jpeg|\.png)$/i;
		var valid_image = valid.exec(filename);
		if(!valid_image) {
			return false;
		}
		return true;
	}

	render() {
		return(
			<div className="ImagePreview">
				<img width={this.props.width} height={this.props.height} src={this.state.previewImage.src} />
				<div className="text-center">
					<button onClick={this.openDialog} type="button" className="btn btn-default">Change</button>
					<input ref="fileInput" type="file" className="image-upload-input" onChange={this.imageUpload}></input>
				</div>
			</div>
		);
	}
}