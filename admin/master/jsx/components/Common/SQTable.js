import React from 'react';
import DataTable from '../datatable/DataTable'

export default function SQTable (props) {
    const { id, data, columns, isLoading, pagination, noClick } = props;
    return (
        <DataTable
            data={data}
            fields={columns}
            isLoading={isLoading}
            pagination={pagination}
            noClick={noClick}
            id={id}
            link="reports"
        />
    )
}
