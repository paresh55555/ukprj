import React, { PureComponent } from 'react';


export default class ShowError extends PureComponent  {

    render () {

        const { error, ...restProps } = this.props;

        if ( error ) {

            return <p {...restProps} className="text-danger"> {error} </p>

        }

        return null

    }

}