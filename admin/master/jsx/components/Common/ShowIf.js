import React from 'react'

const ShowIf = ({
    show = false,
    children = null
}) => {

    if ( show ) {

        return children

    }

    return null

}

export default ShowIf