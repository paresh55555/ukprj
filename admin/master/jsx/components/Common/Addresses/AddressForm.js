import React from 'react';
import {Row,Col,FormControl,Image, Form,ButtonToolbar,FormGroup, HelpBlock} from "react-bootstrap";
import ServiceFormRow from '../../Service/ServiceFormRow';
import { findCity, findStreet, findPostalCode, findState } from '../utilFunctions';
import Geosuggest from 'react-geosuggest';

const VALIDATE_FIELDS = ['address_1', 'city']

export default class AddressForm extends React.Component {
    
    state = {
        errors: {}
    }

    validForm = () => {
        VALIDATE_FIELDS.forEach(field => {
            this.validateField(this.props.address[field], field);
        })
        let errors = Object.keys(this.state.errors).filter((field) => {
            return !!this.state.errors[field]
        })

        return !errors.length
    }

    validateField = (value, field) => {

        if (VALIDATE_FIELDS.indexOf(field) !== -1) {
            this.state.errors[field] = !value ? this.getFieldTitle(field) + ' is required' : null;
        }

        if (field == 'email' && !this.state.errors[field]) {

            const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (!re.test(value.toLowerCase())) {
                this.state.errors[field] = 'Please provide a valid email'
            }

        }

        this.setState({errors: this.state.errors})
    }

    getFieldTitle(field) {
        let splitted = field.split('_')
        splitted[0] = splitted[0].charAt(0).toUpperCase() + splitted[0].substr(1).toLowerCase()
        if (field === 'first_name' || field === 'last_name') {
            splitted[1] = splitted[1].charAt(0).toUpperCase() + splitted[1].substr(1).toLowerCase()
        }

        return splitted.join(' ')
    }

    renderFormField = (field, size, className) => {

        const { address } = this.props

        return (
            <Col key={field} sm={size} className={`${className}`}>
                <FormGroup validationState={this.state.errors[field] ? "error" : null}>
                    <FormControl
                        type='text'
                        placeholder={this.getFieldTitle(field)}
                        onChange={this.updateAddress(field)}
                        value={address[field] || ''}
                    />
                    <HelpBlock>{this.state.errors[field]}</HelpBlock>
                </FormGroup>
            </Col>
        )
    }

    updateAddress = key => e => {

        const { address = {} } = this.props
        
        this.props.onChange({
            ...address,
            [key]: e.target.value
        })
        
        this.validateField(e.target.value, key)

    }

    onPlaceSelect = selectedAddress => {

        if(!selectedAddress) {
            return;
        }

        const { address } = this.props;

        const { gmaps = {} } = selectedAddress
        const { address_components = {}, formatted_address = '' } = gmaps

        let city = findCity(address_components).long_name || ''
        let address_1 = findStreet(address_components).long_name || ''

        if(city) {
            let cityIndex = formatted_address.indexOf(city)
            if(cityIndex > -1) {
                address_1 = formatted_address.substr(0, cityIndex - 2)
            }

        }

        this.props.onChange({
            ...address,
            address_1,
            address_2: findStreet(address_components).long_name || '',
            city: city,
            state: findState(address_components).long_name || '',
            zip: findPostalCode(address_components).long_name || ''
        })

    }

    render () {

        const { address = {} } = this.props;

        return (
            <div>
                <ServiceFormRow label={'Address'}>  
                    <Geosuggest
                        inputClassName="form-control"
                        onSuggestSelect={this.onPlaceSelect}
                    /> 
                </ServiceFormRow>
                <ServiceFormRow label={''}>  
                    {this.renderFormField('address_1', 12, 'no-pd pull-right full-width')}
                </ServiceFormRow>
                <ServiceFormRow label={''}>  
                    {this.renderFormField('address_2', 12, 'no-pd pull-right full-width')} 
                </ServiceFormRow>
                <ServiceFormRow label={''}>
                {this.renderFormField('city', 4, 'no-pd pull-left')}
                {this.renderFormField('state', 4, '')}
                {this.renderFormField('zip', 4, 'no-pd pull-right')} 
                </ServiceFormRow>
            </div>
        ) 
    }
}