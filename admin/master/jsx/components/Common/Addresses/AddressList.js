import React from 'react';
import { connect } from 'react-redux';
import CustomerAddress from '../../Service/customers/CustomerAddress';

class AddressList extends React.Component {

    render () {

        const { addressList = [] } = this.props.addresses
        return (
            <div>
                {
                    addressList.map(
                        (address, index) => 
                            <div key={index} >
                                <CustomerAddress address={address}/>
                            </div>
                    )
                }
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    addresses: state.addresses
})

export default connect(mapStateToProps)(AddressList)