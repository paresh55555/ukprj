import React from 'react';
import AddressForm from './AddressForm';
import { connect } from 'react-redux';

class AddressFormContainer extends React.Component {

    state = {
        currentAddress: {}
    }

    componentWillReceiveProps (nextProps) {

        const { currentAddress } = this.state
        const nextAddress = nextProps.addresses.currentAddress

        if(nextAddress.id !== currentAddress.id) {
            this.setState({
                currentAddress: nextAddress
            })
        }

    }

    componentDidMount () {

        const { currentAddress } = this.props.addresses

        this.setState({
            currentAddress
        })

    }

    updateAddress = currentAddress => {
        this.setState({
            currentAddress
        })
    }

    validate = () => {
        return this.form.validForm()
    }

    render () {

        return (
            <div>
                <AddressForm
                    address={this.state.currentAddress}
                    onChange={this.updateAddress}
                    ref={c => this.form = c}
                />
                {this.props.renderButtons(this.state.currentAddress, this.validate)}
            </div>
        )

    }

        
}

const mapStateToProps = state => ({
    addresses: state.addresses
})

export default connect(mapStateToProps)(AddressFormContainer)