import React, {Component} from 'react';
import {connect} from "react-redux";
import GoogleMapReact from 'google-map-react';
import { Image } from 'react-bootstrap';
import { getLongitude } from '../utilFunctions';
import {Popover,OverlayTrigger} from "react-bootstrap";

class SeviceAddressMaplist extends Component {

    state = {
        locations: [],
        isOpen: null,
        rendered: false
    }

    openPop = index => {
        this.setState({
            isOpen: index
        })
    }

    componentDidMount () {
        const { addressList } = this.props

        const AddressPromisess = addressList.map(
            address => {
                return getLongitude(address)
            }
        )

        Promise.all(AddressPromisess)
            .then(
                values => {
                    const locations =  values.map(
                        location => {
                            return {
                                lat: location.lat(),
                                lng: location.lng()
                            }
                        }
                    )

                    this.setState({
                        locations,
                        rendered: true
                    })
                }
            )
    }

    renderGeoMap(locations,rendered){

        if(rendered){
            return(
                <GoogleMapReact
                    bootstrapURLKeys={{ key: "AIzaSyDsPFecdClIX8WS9z0I-CTWaqxJdz_r6Ss" }}
                    defaultCenter={locations[0]}
                    defaultZoom={11}
                >
                    {locations.map(
                        (location, index) => 
                      
                        <div
                            key={index}
                            lat={location.lat}
                            lng={location.lng}
                        >
                            <OverlayTrigger trigger="click" placement="bottom" rootClose
                                overlay={
                                    <Popover id="service-address-list-popup">
                                        {this.props.renderPopup(index)}
                                    </Popover>
                                }>
                                <Image src="/SQ-admin/img/locationIcon.png" style={{height:'28px',width:'28px', boxShadow: '0px 3px 8px 0px rgba(50, 50, 50, 0.36)', borderRadius: '50%'}}/>
                            </OverlayTrigger>
                       </div>
                    )}
                </GoogleMapReact>
            )
        }
        else{
            return <div></div>
        }
    }

   render() {  

    let {className} = this.props;
    let {locations,rendered} = this.state;
    
       return (
          <div className={className}>
            <div style={{ height: '100%', width: '100%' }}>
                {this.renderGeoMap(locations,rendered)}
            </div>
          </div>
       );
   }
}


const mapDispatchToProps = dispatch => ({
   dispatch
})

const mapStateToProps = (state) => ({
  serviceJob: state.serviceJob
})

export default connect(mapStateToProps, mapDispatchToProps)(SeviceAddressMaplist)