import React from 'react';
import ContentWrapper from '../Layout/ContentWrapper';

class NotFoundContainer extends React.Component {
    render() {
        return (
            <ContentWrapper>
                <h3>This page is coming soon...</h3>
            </ContentWrapper>
        )
    }
}

export default NotFoundContainer;

