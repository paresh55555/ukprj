import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button, Form, FormGroup, FormControl, ControlLabel} from 'react-bootstrap';
import {systemAction, componentAction} from '../../actions';
import SQModal from '../../components/Common/SQModal/SQModal'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class SampleDoor extends React.Component {
    constructor(){
        super()
        this.state = {
            traits: {
                width: {},
                height: {},
                panels: {},
                swings: {},
                burnOff: {},
                waste: {},
            },
            backupTraits: {},
            saveTraits: {},
            isEdit: false,
            showLoading: false
        }
        this.edit = this.edit.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onClose = this.onClose.bind(this);
    }
    componentWillMount(){
        if(this.props.traits){
            this.generateTraits(this.props.traits);
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.component.isSaved){
            this.setState({backupTraits:{},showLoading:false})
            this.panelForm.close()
            this.generateTraits(nextProps.component.component.response_data)
        }else{
            this.generateTraits(nextProps.traits)
        }
    }
    generateTraits(traits){
        let newtrait = traits && traits.length ? {} : {
            width: {},
            height: {},
            panels: {},
            swings: {},
            burnOff: {},
            waste: {},
        } ;
        traits && traits.map((trait)=>{
            newtrait[trait.name] = trait;
        })
        this.setState({traits:newtrait})
    }
    getPostfix(name){
        return name == 'width' || name == 'height' || name == 'burnOff' ? 'mm' :
            name == 'waste' ? '%' : ''
    }
    edit(){
        var backupItems = {},
            saveItems = {}
        Object.keys(this.state.traits).map((key)=>{
            backupItems[key] = Object.assign({},this.state.traits[key])
            saveItems[key] = Object.assign({},this.state.traits[key])
        })
        this.setState({isEdit: true, backupTraits:backupItems, saveTraits: saveItems});
        this.panelForm.open()
    }
    onSave(){
        this.setState({showLoading:true})
        let newTraits = []
        let traits = this.state.saveTraits;
        for (let key in traits) {
            if (traits[key] && !traits[key]['id']) {
                newTraits.push({
                    name: key,
                    value: traits[key]['value'] || '',
                    type: 'component',
                    visible: 1,
                    component_id: this.props.componentID
                })
            } else {
                newTraits.push(traits[key])
            }
        }
        this.props.ApiSaveComponentTraits(
            this.props.systemID,
            this.props.componentID,
            newTraits
        );
    }
    onClose(){
        this.setState({traits: this.state.backupTraits})
    }
    handleChange(key,event){
        let traits = this.state.saveTraits;
        let value = parseInt(event.target.value);
        traits[key]['value'] = !!value ? value : 0;
        this.setState({saveTraits:traits});
    }
    render(){

        console.log(this.props.system)

        return(
            <div>
                <div className="bg-info-dark defaultWidth">
                    <span className="optionTitle">Sample Door</span>
                    <ul>
                        {Object.keys(this.state.traits).map((key)=>{
                            let trait = this.state.traits[key];
                            return (<li key={key}>{trait.name || key} = {trait.value || 0} {this.getPostfix(trait.name || key)}</li>)
                        })}
                    </ul>
                </div>
                <div className="editSave">
                    <Button onClick={this.edit} className="btn btn-default">Edit</Button>
                </div>
                <SQModal
                    ref={(c)=>this.panelForm = c}
                    title={'Sample door'}
                    onConfirm={this.onSave}
                    onClose={this.onClose}
                    disabled={this.state.showLoading}
                    confirmText={this.state.showLoading ? "Saving.." : "Save"}>
                    <Form data-parsley-validate="" data-parsley-group="block-0" noValidate>
                        {Object.keys(this.state.saveTraits).map((key)=>{
                            let trait = this.state.saveTraits[key];
                            return (
                                <FormGroup key={trait.id || key}>
                                    <ControlLabel>{trait.name || key}</ControlLabel>
                                    <FormControl
                                        type="text"
                                        value={trait.value || 0}
                                        placeholder={trait.name || key}
                                        onChange={this.handleChange.bind(this, trait.name || key)}
                                        required
                                        data-parsley-type="number"
                                        pattern="[0-9]+([,\.][0-9]+)?"
                                    />
                                </FormGroup>
                            )
                        })}
                    </Form>
                </SQModal>
            </div>
        )
    }
}

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        system: state.system,
        component: state.component
    }), Object.assign({}, componentAction, systemAction)),
    withProps(apiActions),
)(SampleDoor);