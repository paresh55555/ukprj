import React, {Component} from 'react';
import {Form, FormGroup, ControlLabel, FormControl, Checkbox, Panel} from 'react-bootstrap';
import SQModal from '../../components/Common/SQModal/SQModal';
import DataTable from '../../components/datatable/DataTable.jsx'
import AddOptionsParts from './AddOptionsParts';

class AddParts extends Component {

	constructor(props) {
		super(props);
		this.state = {
			open: false,
		}

		this.toggle = this.toggle.bind(this);
	}

	toggle () {
		this.setState(prevState => ({ open: !prevState.open }));
	}

	render () {

		const {open} = this.state;
		const options = this.state.saveOptions;
		const {component} = this.props;
		let title = component.traits.find((trait) => trait.name === 'title')

		if(typeof title === 'undefined') {
			title = {value:'Empty'}
		}

		return (
			<div>
				<div onClick={this.toggle} className="bg-info-dark defaultWidth pointer">
					<div style={{left:0}} className="ui-icon treeclick ui-icon-triangle-1-s tree-minus"></div>
					<span className="optionTitle item-1629 editable editable-click editable-disabled" tabIndex="-1"> {open ? '▼' : '▶' } Parts for: {`${title && title.value}`} </span>
				</div>
				<div className={`editSave ${open ? '' : 'closed'}`}>
					{component && component.options && component.options.map((option, index) => <AddOptionsParts onSave={this.props.onPartsSave} option={option} key={index} />)}
                </div>
			</div>
		)
	}
}

export default AddParts;