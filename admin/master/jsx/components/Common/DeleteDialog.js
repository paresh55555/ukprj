export default (config) => {
    swal({
        title: config.title || "Are you sure?",
        text: config.text,
        type: config.type || "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, config.onConfirm);
}