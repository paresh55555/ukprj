import React, { PureComponent } from 'react';
import Activity from './Activity';

export default class ActivityList extends PureComponent  {

    render () {

        const { heading, logs } = this.props;

        return(
            <div className='activily_ListDiv'>
                <h4>{heading}</h4>
                {
                    logs && logs.map(
                        (log, index) => <Activity
                            key={index}
                            message={log.logs}
                            elapsedTime={log.elapsed_time}
                            type={log.type}
                        />
                    )
                }
            </div>
        )
    }

}