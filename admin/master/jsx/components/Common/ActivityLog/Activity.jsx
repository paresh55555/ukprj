import React, { PureComponent } from 'react';

export default class Activity extends PureComponent  {



    render () {

        const { message, elapsedTime, type} = this.props

        let Icon = '';

        switch(type){
            case 'image':
                Icon = "fa fa-image"
            break;
            case 'video':
                Icon = "fa fa-video-camera"
            break;
            case 'docs':
                Icon = "fa fa-file"
            break;
            default:
                Icon = "fa fa-question"
        }

        return(
            <div className='activityDiv'>
                <div className='activity_icon pull-left'>
                    <em className={Icon}></em>
                </div>
                <div className='borderLine pull-right'>
                    <div className='pull-left' dangerouslySetInnerHTML={{
                        __html: message
                    }}>
                    </div>
                    <div className='pull-right'>{elapsedTime}</div>
                </div>
            </div>
        )

    }

}