import React, {Component, PropTypes} from 'react';
import {Modal, Button} from 'react-bootstrap';
import FontIcon from "../FontIcon";

export default class SQModal extends React.Component {
    constructor() {
        super();
        this.state = {showModal: false}
        this.close = this.close.bind(this)
        this.confirm = this.confirm.bind(this)
        this.open = this.open.bind(this)
    }

    close() {
        if (this.props.onClose) {
            this.props.onClose()
        }
        this.setState({showModal: false});
    }

    open() {
        if (this.props.onOpne) {
            this.props.onOpne()
        }
        this.setState({showModal: true});
    }

    confirm() {
        if (this.props.onConfirm) {
            this.props.onConfirm()
        }
    }

    render() {
        return (
            <Modal show={this.props.isOpen || this.state.showModal} onHide={this.close} className={this.props.className}>
                <Modal.Header>
                    <Modal.Title>
                        {this.props.title}
                        <FontIcon onClick={this.close} iconClass={'fa-close'}/>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.props.children}
                </Modal.Body>
                {!this.props.customButtons &&
                    <Modal.Footer>
                        {this.props.onConfirm ? <Button disabled={this.props.disabled} bsStyle={'primary'}
                                                        onClick={this.confirm}>{this.props.confirmText}</Button> : null}
                        <Button onClick={this.close}>Cancel</Button>
                    </Modal.Footer>
                }
            </Modal>
        );
    }

}

