import React, {Component, PropTypes} from 'react';

class ComponentImage extends React.Component{
    constructor() {
        super();
    }
    render(){
        return(
            <div>
            { this.props.image && this.props.image.value && this.props.image.value !== 'tmp' ?
                <img onClick={this.props.onClick} width={this.props.width} height={this.props.height} src={'/' + this.props.image.value} />
                :
                <div className={this.props.iconClass + " thinBorder2 icon-cloud-upload"} onClick={this.props.onClick}></div>
            }
            </div>
        )
    }
}
ComponentImage.propTypes = {
    image: PropTypes.object.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    iconClass: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}
export default ComponentImage;