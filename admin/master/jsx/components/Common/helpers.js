export function isEmptyObject(obj){
    return Object.keys(obj).length === 0 && obj.constructor === Object
}
export function isDoorTemplate(system){
    return system && system.name == 'Door System' && system.type == 'system'
}
export function getTraitByName(component, traitName){
    return component && component.traits && component.traits.length &&
        component.traits.find((trait)=>trait.name === traitName)
}