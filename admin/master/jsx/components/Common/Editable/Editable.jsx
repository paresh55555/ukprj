import React from 'react';
import ReactDOM from 'react-dom';

export default class Editable extends React.Component {
	constructor(props) {
		super(props);
		
		this.callOnChange = this.callOnChange.bind(this); 
		this.callOnClick = this.callOnClick.bind(this); 
	} 

	componentDidMount() {
		self=this;
		$.fn.editable.defaults.mode = 'popup';
		var editable = $('#'+this.props.id).editable();
		var id = this.props.id;
		$('#'+this.props.id).on('save', function(e, params) {
			self.callOnChange(id,params)		    
		});
		$('#'+this.props.id).on('shown',function (e, editable) {
			self.callOnClick();
		});
		require('./Editable.css');
	}

	callOnChange(id,params){
		this.props.onChange(id,params)
	}
	callOnClick(id,params){
		this.props.onClick()
	}

	render() {
		return(
			<div>
			<span 
				className="editable"
				id={this.props.id}
				name={this.props.name} 
				data-type="text" 
				data-title="Edit value" 
				data-value={this.props.content}
				>
				{this.props.content}
			</span>

			</div>
		)
	}
}