export const getLongitude = (address) => {
        
    return new Promise (
        (response, reject) => {

            let geocoder = new google.maps.Geocoder();

            const store_address = `${address.address_1 + ', '}${address.city + ', '}${address.state + ', '}${address.country + ', '}`;
            geocoder.geocode({
                'address': store_address
            }, (results, status) => {
                if (status == google.maps.GeocoderStatus.OK && results.length) {
                    response(results[0].geometry.location);
                } else {
                    response(new google.maps.LatLng(37.7749, -122.4194));
                }
            });

        }
    )
};

export const filterAddressComponents = (keys, components) => {

	let foundItem = null;

	if(!keys.length) {
		return;
	}

	if(keys.length == 1) {
		return findAddressItem(keys[0].name, components);
	}

	let sortedKeys = [...keys].sort(
		(key1, key2) => key1.priority - key2.priority
	)

	for(let i = 0, len = sortedKeys.length; i < len; i++) {

		if(foundItem)
			break;

		const key = sortedKeys[i];

		const item = findAddressItem(key.name, components)

		if(item) {
			foundItem = item
		}
	}

	return foundItem;

}

export const findAddressItem = (name, components) => {
	return components.find(
		({types}) => 
		 types.find(
				type =>
					type == name
			)
	) || {}
}

export const findStreet = components => filterAddressComponents(
		[
			{
				name: "street_number"
			}
		],
		components
	)

export const findPostalCode = (components) => {

	return filterAddressComponents(
		[
			{
				name: "postal_code"
			}
		],
		components
	)

}

export const findCity = (components) => {

	return filterAddressComponents(
		[
			{
				name: "locality",
				priority: 0
			},
			{
				name: "administrative_area_level_3",
				priority: 1
			},
			{
				name: "sublocality",
				priority: 2
			},
			{
				name: "neighborhood",
				priority: 3
			},
		],
		components
	)

}

export const findState = (components) => {

	return filterAddressComponents(
		[
			{
				name: 'administrative_area_level_1'
			}
		],
		components
	)

}

export const findCountry = (components) => {

	return filterAddressComponents(
		[
			{
				name: 'country'
			}
		],
		components
	)

}

export const findDescription = (components) => {

	const street_number = filterAddressComponents(
		[
			{
				name: 'street_number'
			}
		],
		components
	);
	const route = filterAddressComponents(
		[
			{
				name: 'route'
			}
		],
		components
	)

	const neighborhood = filterAddressComponents(
		[
			{
				name: 'neighborhood'
			}
		],
		components
	)
	const description = `${street_number && street_number.long_name || ''} ${route && route.long_name || ''} ${neighborhood && neighborhood.long_name || ''}`;
	return description;
}

export const partial = (func, ...firstArguments) => {
    return (...laterArguments) => func(...firstArguments, ...laterArguments)
}

export const actionCreator = (type, payload) => {
	return {
		type,
		payload
	}
}

export const modifyCustomer = (customer) => {
	return ({
			...customer,
			address_1 : customer.address_address_1,
			address_2: customer.address_address_2,
			city:customer.address_city,
			state:customer.address_state,
			zip:customer.address_zip,
			email:customer.customer_email,
			first_name:customer.customer_first_name,
			last_name:customer.customer_last_name,
			phone:customer.customer_phone,
			job_number:customer.job_number,
			order_id:customer.order_id,
			order_number:customer.order_number,
			start_date:customer.start_date,
			start_time:customer.start_time,
			tech_name:customer.tech_name,
		})
	
}

export const group = (data, groupBy) => {

	let groupedData = {}
   
	data.forEach(element => {
   
	  const groupByValue = element[groupBy]
   
	  groupedData[groupByValue] = groupedData[groupByValue] || []
	  groupedData[groupByValue].push(element)
   
	})
   
	return groupedData
   
   }