import React, {Component} from 'react';

class Note extends Component{
    constructor() {
        super();
        this.change = this.change.bind(this);
    }

    componentDidUpdate(prevProps, prevState){
        this.initEditable();
    }

    componentDidMount(){
        this.initEditable();
    }

    getText(note){
        var emptyText = '< Add Note >'
        if(note && note.length){
            emptyText = note
            if(!note.replace(/\s/g, '').length){
                emptyText = note.replace(/\s/g, '&nbsp;')
            }
        }
        return emptyText
    }

    initEditable(){
        let self = this,
            note = this.props.note && this.props.note.value && this.props.note.value.length ? this.props.note.value : "",
            noteID = this.props.note.id ? this.props.note.id : 'tmp-'+this.props.index,
            noteElement = $('.'+this.props.cssClass+'.item-'+noteID);
            if(noteElement.data('editable')){
                noteElement.editable('option', 'value', note ? note : (this.props.editing ? "" : "< Add Note >"));
                noteElement.editable('option', 'emptytext', self.getText(note));
                noteElement.editable('option', 'disabled', !this.props.editing);
                noteElement.addClass('editable editable-click');
            }else{
                noteElement.editable({
                    disabled: !self.props.editing,
                    value: note,
                    emptytext: self.getText(note),
                    inputclass: 'sq-popover-input',
                    placeholder: '< Add Note >',
                    success: function(response, newValue) {
                        self.change(newValue);
                    }
                });
                noteElement.on('shown', function(e, editable) {
                    setTimeout(function(){
                        var $popover = editable.container.$form.parents('.popover');
                        $popover.css('left', '4%');
                        $popover.find('.arrow').css('left','25px');
                    },0);
                });
            }
        
    }

    change(newValue){
        this.props.onNoteChange(this.props.index, newValue);
    }

    render(){
        const note = this.getText(this.props.note && this.props.note.value);
        const noteID = this.props.note.id ? this.props.note.id : 'tmp-'+this.props.index;
        return(
            <span className={this.props.cssClass+" item-"+noteID+" clearfix"} dangerouslySetInnerHTML={{__html: note}}></span>
        );
    }
}

Note.propTypes = {
};

export default Note;