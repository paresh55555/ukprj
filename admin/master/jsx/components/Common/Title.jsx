import React, {Component} from 'react';

class Title extends Component{
    constructor() {
        super();
        this.state = {
            title: ''
        }
        this.change = this.change.bind(this);
    }

    componentWillMount(){
        if(this.props.title && this.props.title.value){
            this.setState({title: this.props.title.value});
        }
    }

    componentDidMount(){
        this.initEditable();
    }

    componentDidUpdate(){
        this.initEditable();
    }

    initEditable(){
        var self = this,
            title = this.props.title && this.props.title.value ? this.props.title.value : "",
            titleID = this.props.title.id ? this.props.title.id : 'tmp-'+this.props.defaultID,
            cssClassSelector = this.props.cssClass && this.props.cssClass.split(' ').join('.'),
            titleElement = $('.'+cssClassSelector+'.item-'+titleID),
            defaultText = this.props.defaultText ? this.props.defaultText : "< Add Title >";
        if(titleElement.data('editable')){
            titleElement.editable('option', 'value', title ? title : (this.props.editing ? "" : defaultText));
            titleElement.editable('option', 'disabled', !this.props.editing);
            titleElement.addClass('editable editable-click')
        }else{
            titleElement.editable({
                disabled: !self.props.editing,
                value: title,
                inputclass: 'sq-popover-input',
                emptytext: ''+defaultText,
                placeholder: ''+defaultText,
                success: function(response, newValue) {
                    self.change(newValue);
                }
            });
            titleElement.on('shown', function(e, editable) {
                setTimeout(function(){
                    var $popover = editable.container.$form.parents('.popover');
                    $popover.css('left', '4%');
                    $popover.find('.arrow').css('left','25px');
                },0);
            });
        }
    }

    change(newValue){
        this.setState({title: newValue});
        this.props.onTitleChange(newValue);
    }

    render(){
        const defaultText = this.props.defaultText ? this.props.defaultText : "< Add Title >";
        const title = this.state.title ? this.state.title : defaultText;
        const titleID = this.props.title.id ? this.props.title.id : 'tmp-'+this.props.defaultID;
        return(
            <span className={this.props.cssClass+" item-"+titleID}>{title}</span>
        );
    }
}

Title.propTypes = {
};

export default Title;