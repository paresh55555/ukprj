import React, {Component, PropTypes} from 'react';
import {SQ_URL} from '../../constant';

class DoorImage extends React.Component{
    constructor() {
        super();
    }
    getDefaultImage(type){ 
        switch (type){
            case 'panel':
                return "/" + SQ_URL + "/img/slide-panel.gif"
                break;
            case 'left':
                return "/" + SQ_URL + "/img/slide-panel.gif"
                break;
            case 'right':
                return "/" + SQ_URL + "/img/slide-panel.gif"
                break;
            case 'leftSlide':
                return "/" + SQ_URL + "/img/slide-panel.gif"
                break;
            case 'rightSlide':
                return "/" + SQ_URL + "/img/slide-panel.gif"
                break;
            case 'divider':
                return "/" + SQ_URL + "/img/slide-panel.gif"
                break;
            default:
                return ''
        }
    }
    render(){
        return(
            <img className="doorImage" src={this.props.imgUrl ? '/'+this.props.imgUrl : this.getDefaultImage(this.props.doorType)}
                 onClick={this.props.onClick} />
        )
    }
}
DoorImage.propTypes = {
    doorType: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}
export default DoorImage;
