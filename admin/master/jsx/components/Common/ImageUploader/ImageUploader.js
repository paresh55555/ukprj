import React, {Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import Cropper from 'react-cropper';
import Dropzone from 'react-dropzone';
import '../../../../../node_modules/cropperjs/dist/cropper.css';
import { Grid, Row, Col, Panel, Button, Table, Alert, Modal,Form } from 'react-bootstrap';
import * as constant from '../../../constant.js'
import swal from 'sweetalert'
// If you choose not to use import, you need to assign Cropper to default
// var Cropper = require('react-cropper').default

class ImageUploader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null,
            uploading: false
        }
        this.upload = this.upload.bind(this);
        this.onDrop = this.onDrop.bind(this);
        this.chooseImage = this.chooseImage.bind(this);
        this.close = this.close.bind(this);
    }

    onDrop(files,rejected) {
        if(!!rejected.length){
          return swal('Invalid file type.','Only png, jpg and jpeg files allowed', 'error');
        }
        this.setState({image: files[0].preview})
    }

    chooseImage(){
        this.refs.dropzone.open();
    }

    close(){
        this.setState({image: null, uploading: false})
        this.props.onClose()
    }

    upload(){
        var self = this,
            image = this.refs.cropper.getCroppedCanvas().toDataURL(),
            data = new FormData(),
            uploadUrl = constant.API_URL + '/image';

        this.setState({uploading: true, image: image})

        data.append('file', image);
        for(let key in this.props.config){
            data.append(key, this.props.config[key]);
        }

        axios.post(uploadUrl, data).then(function (res) {
            if(res.data.status === 'success'){
                self.props.onUpload(res.data.imgUrl);
                self.close()
            }else{
                swal('Image upload error',res.data.msg, 'error');
            }
        })
        .catch(function (err) {
            //console.error(err)
        });
    }

    render() {
        const { config } = this.props
        var style = {
            width: "100%",
            height: 200,
            borderWidth: 2,
            borderColor: '#666',
            borderStyle: 'dashed',
            borderRadius: 5,
            position:'relative'
        };
        var panelBody = {
            height: '51px',
            position: 'absolute',
            textAlign: 'center',
            width:'100%',
            margin:'auto',
            top:0,
            bottom:0
       }
        return (
            <Modal show={ this.props.show } onHide={ this.close }>
                <Modal.Header closeButton>
                    <Modal.Title>Image upload</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {
                        this.state.image ?
                            <div className="panel-body">
                                {this.state.uploading ?
                                    <div className="text-center">
                                        <img className="img-responsive" src={this.state.image} alt="Image" />
                                    </div>
                                    :
                                    <Cropper
                                        ref='cropper'
                                        src={this.state.image}
                                        style={{height: 400, width: '100%'}}
                                        // Cropper.js options
                                        guides={false}
                                        viewMode={0}
                                        aspectRatio={ config.width / config.height } />
                                }
                                {this.state.uploading &&
                                    <div className="ball-pulse">
                                        <div></div>
                                        <div></div>
                                        <div></div>
                                    </div>
                                }
                            </div>
                        :
                            <Dropzone ref='dropzone'
                                      accept="image/*"
                                      multiple={false}
                                      style={style}
                                      onDrop={this.onDrop}>
                                <div style={panelBody} className="panel-body">Try dropping some files here, or click to select files to upload.</div>
                            </Dropzone>
                    }
                </Modal.Body>
                <Modal.Footer>
                    <Button type="button" onClick={this.state.image ? this.upload : this.chooseImage} className="btn btn-success">{this.state.image ? 'Upload' : 'Choose image'}</Button>
                    <Button onClick={ this.close }>Close</Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

ImageUploader.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

ImageUploader.propTypes = {
    show: React.PropTypes.bool.isRequired,
    config: React.PropTypes.object.isRequired,
    onUpload: React.PropTypes.func.isRequired,
    onClose: React.PropTypes.func.isRequired
}

export default connect(state => ({
}), Object.assign({}))(ImageUploader);