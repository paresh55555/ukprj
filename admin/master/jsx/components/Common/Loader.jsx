import React from 'react';

export default function Loader(props) {
    return (
        <div className="ball-pulse">
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
}