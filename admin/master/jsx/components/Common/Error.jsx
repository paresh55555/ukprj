import React from 'react';
import { Router, Route, Link, History } from 'react-router';
import ContentWrapper from '../Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {API_URL} from '../../constant.js';

class ErrorPage extends React.Component {
	
    render() {
        const val = sessionStorage.error == "undefined" ? JSON.stringify({message:"error"}) : sessionStorage.error;
        const error = JSON.parse(val);
        return ( 
            <div className="abs-center wd-xl">
                <div className="text-center mb-xl">
                    <div className="text-lg mb-lg">Oops</div>
                    <p className="lead m0">Something went wrong</p>
                    <p>We are working on it and we'll get it fixed as soon as we can</p>
                    <pre>
                        {error.message}
                    </pre>
                </div>
                <div className="input-group mb-xl">
                    
                </div>
                
            </div>
            );
    }

}

export default ErrorPage;
