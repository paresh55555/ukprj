import React, {Component} from 'react';
import {Form, FormGroup, ControlLabel, FormControl, Checkbox, Panel} from 'react-bootstrap';
import SQModal from '../../components/Common/SQModal/SQModal';
import DataTable from '../../components/datatable/DataTable.jsx'

class AddOptionsParts extends Component {

	constructor(props) {
		super(props);
		this.state = {
			openAddModal: false,
			showLoading: false,
			saveOptions: {
				number: '',
				name: '',
				quantity_formula: '',
				size_formula: '',
				on_cut_sheet: true
			}
		}

		this.addModal = this.addModal.bind(this);
		this.onSave = this.onSave.bind(this);
	}

	addModal () {
		this.setState({
			openAddModal: true
		})

		this.panelForm.open();
	}

	handleChange(key, event){
        let value = event.target.value;
        this.setState((prevState) => {
        	let {saveOptions} = prevState;
        	saveOptions[key] = !!value ? value : 0;

        	return {
        		saveOptions
        	}
        })
    }

    onSave () {
    	const [trait] = this.props.option.traits;
    	const {system_id, component_id, option_id} = trait;

    	let partOptions = this.state.saveOptions;

    	delete partOptions.number;

    	this.props.onSave(system_id, component_id, option_id, partOptions)
    }


	render () {

		const {openAddModal} = this.state;
		const options = this.state.saveOptions;
		const {option} = this.props;
		let title = option.traits.find((trait) => trait.name === 'title')

		if(typeof title === 'undefined') {
			title = {value:'Empty'}
		}

		return (
			<div className="optionsBox">
				<div className="colorGroupMenu">
                    <div className="groupTitle mb-sm mr-sm btn btn-success btn-outline colorGroupButton optionsTitle">
                        <span className="undefined item-tmp-2391 editable editable-click editable-disabled ">Option: {`${title && title.value}`}</span>
                    </div>
                    <div className="editSaveMenu">
                        <button onClick={this.addModal} className="btn btn-default">Add Part</button>
                    </div>
                </div>
                <div className="editSave">
                	<Panel>
	                    <DataTable id="companiesTable" link="companies" data={[]} isLoading={false} />
	                </Panel>
                </div>
                <SQModal
                    ref={(c)=>this.panelForm = c}
                    title={'Add Part'}
                    onConfirm={this.onSave}
                    onClose={this.onClose}
                    disabled={this.state.showLoading}
                	confirmText={this.state.showLoading ? "Saving.." : "Save"}>
                    <Form data-parsley-validate="" data-parsley-group="block-0" noValidate>
                        <FormGroup>
                        	 <ControlLabel>Part Number</ControlLabel>
                        	 <FormControl value={options.number} onChange={this.handleChange.bind(this, 'number')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                        	 <ControlLabel>Part Name</ControlLabel>
                        	 <FormControl value={options.name} onChange={this.handleChange.bind(this, 'name')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                        	 <ControlLabel>Quanity Formula</ControlLabel>
                        	 <FormControl value={options.quantity_formula} onChange={this.handleChange.bind(this, 'quantity_formula')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                        	 <ControlLabel>Size Formula</ControlLabel>
                        	 <FormControl value={options.size_formula} onChange={this.handleChange.bind(this, 'size_formula')} type="text"/>
                        </FormGroup>
                        <FormGroup>
                        	 <ControlLabel>Show On Cut Sheet</ControlLabel>
                        	 <Checkbox checked={options.on_cut_sheet} value={options.cut_sheet} onChange={this.handleChange.bind(this, 'on_cut_sheet')} />
                        </FormGroup>
                    </Form>
                </SQModal>
            </div>
		)
	}
}

export default AddOptionsParts;