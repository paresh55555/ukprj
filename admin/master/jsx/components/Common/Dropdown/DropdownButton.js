import React, { PropTypes } from 'react';
import { Dropdown, MenuItem } from 'react-bootstrap';

function DropdownButton(props) {
    const { children, data, changeData, id, type,Datakey } = props;
    console.log(data,'data,datad')
    return (
        <Dropdown id={id}>
            <Dropdown.Toggle>
                {children}
            </Dropdown.Toggle>
            <Dropdown.Menu className={'animated fadeIn'}>
                {data.filter(item=>item).map((item, i) => (
                    <MenuItem onClick={() => changeData(item, type)} key={i}>{typeof item === 'object' ?  item[Datakey] || '' : item }</MenuItem>
                ))}
            </Dropdown.Menu>
        </Dropdown>
    );
}

DropdownButton.propTypes = {
    data: PropTypes.array.isRequired,
    children: PropTypes.node.isRequired,
    changeData: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
};

export default DropdownButton;
