import React, {PropTypes} from 'react';
import {Dropdown, Button, MenuItem} from 'react-bootstrap';

export default function SQDropdown(props) {
    let full = props.fullWidth ? 'full-width ' : ''
    return (
        <Dropdown id={props.id} className={full + (props.className ? props.className : '')}>
            <Dropdown.Toggle noCaret className={full} className={'custom-select ' + (props.selectedVal ? '' : 'no-val')}>
                {props.selectedVal || props.defaultVal}
            </Dropdown.Toggle>
            <Dropdown.Menu className={full}>
                {props.data && props.data.map((item, i) => (
                    <MenuItem onClick={() => props.onChange(item)} key={i}>{typeof item === 'object' ?  item[props.dataKey] || '' : item }</MenuItem>
                ))}
            </Dropdown.Menu>
        </Dropdown>
    );
}

SQDropdown.propTypes = {
    data: PropTypes.array.isRequired,
    selectedVal: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
    id: PropTypes.string.isRequired,
};