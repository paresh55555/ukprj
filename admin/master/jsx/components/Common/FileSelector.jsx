import React, {Component, PropTypes} from 'react';
import Dropzone from "react-dropzone";
import swal from "sweetalert";
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import * as constant from "../../constant";

class FileSelector extends Component {
    constructor(props) {
        super(props);
    }
    onDrop = (files,rejected) =>{
        if(!!rejected.length){
            return swal('Invalid file type.','Only pdf, png, jpg and jpeg files allowed', 'error');
        }
        let uploadData = this.props.multiple ? files : files[0]
        this.props.onSelect(uploadData)
    }
    chooseFile = () =>{
        this.refs.dropzone.open();
    }

    dropzoneText = () =>{
        if(this.props.dropzoneTxt){
            return this.props.dropzoneTxt
        }
        else{
            return 'Try dropping some files here or click “Select” to upload.'
        }
    }
    
    render() {
        return (
            <Dropzone ref='dropzone'
                      accept={this.props.fileTypes}
                      multiple={!!this.props.multiple}
                      className={'dropzone'}
                      disableClick={true}
                      onDrop={this.onDrop}>
                <div className="panel-body">
                    {this.dropzoneText()}
                    <div className={'file-select-btn'}><Button onClick={this.chooseFile} disabled={this.props.disabled}>Select</Button></div>
                </div>
            </Dropzone>
        );
    }
}

FileSelector.propTypes = {
    onSelect: PropTypes.func.isRequired,
    fileTypes: PropTypes.string.isRequired
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(FileSelector)