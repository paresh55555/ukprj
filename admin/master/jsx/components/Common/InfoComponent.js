import React, {Component, PropTypes} from 'react';
import {Row, Col, ButtonToolbar} from "react-bootstrap";
import ServiceButton from "../Service/ServiceButton";
import PersonAvatar from "../Service/Purchasers/PersonAvatar";
import PersonInfo from "../Service/Purchasers/PersonInfo";

class InfoComponent extends Component {


  onEdit(data){
    if(this.props.onClickEdit){
      this.props.onClickEdit(data)
    }
  }

  onDelete(data){
    if(this.props.onClickDelete){
      this.props.onClickDelete(data)
    }
  }

  renderButtons(){
    let data = this.props.data
      return (
          <ButtonToolbar className={'buttons pull-right'}>
              <ServiceButton label={'Edit'} className="Doorbtn" onClick={() => this.onEdit(data)}/>
              <ServiceButton  label={'Delete'} className="Doorbtn" onClick={() => this.onDelete(data)}/>
          </ButtonToolbar>
      )
  }
  render() {
    
      let data = this.props.data

      return (
          <div className={'purchaser-info JobCustomer'}>
              <Row>
                  <Col md={1} >
                      <PersonAvatar info={data}/>
                  </Col>
                  <Col md={7}>
                      <PersonInfo info={data} text={this.props.text}/>
                  </Col>
                  <Col md={4} >
                      {this.renderButtons()}
                  </Col>
              </Row>
          </div>
      );
  }
}

export default InfoComponent