import React, {Component, PropTypes} from 'react';
import {Breadcrumb} from 'react-bootstrap'

class Breadcrumbs extends React.Component {
    onBreadCrubmClick(url, e){
       e.preventDefault()
        this.context.router.push(url)

    }
    render() {
        const list = this.props.data
    return (
        <Breadcrumb>
            {list && list.map((item,i)=>{
                 return (
                    <Breadcrumb.Item onClick={this.onBreadCrubmClick.bind(this,item.url)} active={item.active} href={item.url} key={i}>
                      {item.title}
                    </Breadcrumb.Item>
                    )
            })}
          </Breadcrumb>
        );
    }

}

Breadcrumbs.contextTypes = {
    router: React.PropTypes.object.isRequired
};
export default Breadcrumbs