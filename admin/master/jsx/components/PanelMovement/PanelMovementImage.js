import React, {Component, PropTypes} from 'react';
import {Button} from 'react-bootstrap';

import DoorImage from '../Common/DoorImage'
import ImageUploader from '../Common/ImageUploader/ImageUploader'

class PanelMovementImage extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            image: {
                name: props.traitType+'Img'
            },
            showUpload: false
        }
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
    }

    componentWillMount(){
        if(!this.props.image){
            let newTrait = {
                name: this.props.traitType+'Img',
                value: '',
                visible: 1
            }
            this.props.onSave(newTrait)
        }else{
            this.setState({image:this.props.image})
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.image !== nextProps.image){
            this.setState({image:nextProps.image})
        }
    }

    openUpload(){
        if(!this.props.isEdit){
            return;
        }
        this.setState({showUpload: true})
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            image['name'] = this.props.traitType+'Img';
            image['value'] = imgUrl;
            image['visible'] = 1;
        }
        this.setState({image:image});
    }

    closeUpload(){
        this.setState({showUpload: false});
    }

    getImage(type){
        return <DoorImage doorType={type} imgUrl={this.state.image && this.state.image.value} onClick={this.openUpload}/>
    }

    getTitle(type){
        switch (type){
            case 'left':
                return 'Left Swing'
                break;
            case 'right':
                return 'Right Swing'
                break;
            case 'leftSlide':
                return 'Slide Left'
                break;
            case 'rightSlide':
                return 'Slide Right'
                break;
            case 'divider':
                return 'Divider Bar'
                break;
            default:
                return ''
        }
    }

    render(){
        return(
            <div className="panelOptions short">
                <div className="horizontalOption">
                    <div className="horizontalPanelImage">
                        {this.getImage(this.props.traitType)}
                    </div>
                    <div className="thumbnailTitle horizontalPanelImage leftTitle">
                        {this.getTitle(this.props.traitType)}
                    </div>
                </div>
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.systemID,
                    component_id: this.props.componentID,
                    trait_id: this.state.image.id,
                    type: 'component',
                    width: this.props.traitType === 'divider' ? 11 : 50,
                    height: 120,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </div>
        )
    }
}
export default PanelMovementImage;