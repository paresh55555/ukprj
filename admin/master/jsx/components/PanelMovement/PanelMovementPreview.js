import React, {Component, PropTypes} from 'react';
import {Button} from 'react-bootstrap';

import DoorImage from '../Common/DoorImage'
import Title from '../Common/Title'

class PanelMovementPreview extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            numPlace: null,
            isEdit: false,
            showUpload: false
        }
    }

    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
    }

    generateTraits(traits){
        let self = this,
            numPlace = traits.find(function(el,i){return el.name === self.props.traitType+'NumPlace'});
        this.setState({numPlace: (numPlace && numPlace.value)})
    }

    onChange(key, value) {
        this.props.onChangeTrait(key, value)
    }

    onChangeRadio(key, value) {
        this.setState({numPlace: value})
        this.props.onChangeTrait(key, value)
    }

    getPattern(type){
        let traits = this.props.traits;
        const leftImg = traits.find(function(el,i){return el.name === 'leftImg'});
        const rightImg = traits.find(function(el,i){return el.name === 'rightImg'});
        const leftSlideImg = traits.find(function(el,i){return el.name === 'leftSlideImg'});
        const rightSlideImg = traits.find(function(el,i){return el.name === 'rightSlideImg'});
        const dividerImg = traits.find(function(el,i){return el.name === 'dividerImg'});
        let funcCap = function(){return false},
            left = (i)=>{return <DoorImage key={i} doorType={'left'} imgUrl={leftImg && leftImg.value} onClick={funcCap} />},
            right = (i)=>{return <DoorImage key={i} doorType={'right'} imgUrl={rightImg && rightImg.value} onClick={funcCap} />},
            leftSlide = (i)=>{return <DoorImage key={i} doorType={'leftSlide'} imgUrl={leftSlideImg && leftSlideImg.value} onClick={funcCap} />},
            rightSlide = (i)=>{return <DoorImage key={i} doorType={'rightSlide'} imgUrl={rightSlideImg && rightSlideImg.value} onClick={funcCap} />},
            divider = (i)=>{return <DoorImage key={i} doorType={'divider'} imgUrl={dividerImg && dividerImg.value} onClick={funcCap} />};
        switch (type){
            case 'left':
                return [left, leftSlide, leftSlide, divider].map((func, i)=>{ return func(i) })
                break;
            case 'right':
                return [divider, rightSlide, rightSlide, right].map((func, i)=>{ return func(i) })
                break;
            case 'both':
                return [left, leftSlide, divider, rightSlide, rightSlide, right].map((func, i)=>{ return func(i) })
                break;
            default:
                return ''
        }
    }

    getNumber(number, type){
        if(this.state.numPlace === type)return number
    }

    render(){
        let self = this
        const leftTitle = this.props.traits.find(function(el,i){return el.name === self.props.traitType+'LeftTitle'})
        const rightTitle = this.props.traits.find(function(el,i){return el.name === self.props.traitType+'RightTitle'})
        const title = this.props.traits.find(function(el,i){return el.name === self.props.traitType+'Title'})
        const place = this.state.numPlace;
        return(
            <div>
                <div className="panelOptionsSecondRow">
                    <div className="horizontalOption">
                        <div className="thumbnailTitle horizontalPanelImage leftTitle">Number Placement</div>
                        <form>
                            <div className="radio">
                                <label>
                                    <input type="radio" key={'left'}
                                           checked={place === 'left'}
                                           disabled={!this.props.isEdit}
                                           name={this.props.traitType+'NumPlace'}
                                           onClick={()=>this.onChangeRadio(this.props.traitType+'NumPlace', 'left')}/>
                                    Left
                                </label>
                            </div>
                            <div className="radio">
                                <label>
                                    <input type="radio" key={'right'}
                                           checked={place === 'right'}
                                           disabled={!this.props.isEdit}
                                           name={this.props.traitType+'NumPlace'}
                                           onClick={()=>this.onChangeRadio(this.props.traitType+'NumPlace', 'right')}/>
                                    Right
                                </label>
                            </div>
                            <div className="radio">
                                <label>
                                    <input type="radio" key={'none'}
                                           checked={place === 'none'}
                                           disabled={!this.props.isEdit}
                                           name={this.props.traitType+'NumPlace'}
                                           onClick={()=>this.onChangeRadio(this.props.traitType+'NumPlace', 'none')}/>
                                    None
                                </label>
                            </div>
                        </form>
                    </div>
                </div>
                <div className={"panelOptionsSecondRow" + (this.props.traitType === 'both' ? ' long' : '')}>
                    <div className="horizontalOption">
                        <div className="horizontalPanelImage">
                            {this.getPattern(this.props.traitType)}
                        </div>
                        {this.props.traitType === 'both' ?
                            <div className="thumbnailTitle text-center">
                                {this.getNumber(1, 'left')}&nbsp;
                                <Title title={leftTitle || {}} cssClass="panelMovTitleLeft"
                                       editing={this.props.isEdit} defaultID={'movLeft'}
                                       onTitleChange={this.onChange.bind(this, this.props.traitType + 'LeftTitle')}/>
                                &nbsp;{this.getNumber(1, 'right')} /
                                {this.getNumber(2, 'left')}&nbsp;
                                <Title title={rightTitle || {}} cssClass="panelMovTitleRight"
                                       editing={this.props.isEdit} defaultID={'movRight'}
                                       onTitleChange={this.onChange.bind(this, this.props.traitType + 'RightTitle')}/>
                                &nbsp;{this.getNumber(2, 'right')}
                            </div>
                            :
                            <div className="text-center">
                                {this.getNumber(2, 'left')}&nbsp;
                                <Title title={title || {}} cssClass="thumbnailTitle"
                                       editing={this.props.isEdit} defaultID={'mov'+this.props.index}
                                       onTitleChange={this.onChange.bind(this, this.props.traitType + 'Title')}/>
                                &nbsp;{this.getNumber(2, 'right')}
                            </div>
                        }
                    </div>
                </div>
                <div className="clearfix"></div>
            </div>
        )
    }
}
export default PanelMovementPreview;