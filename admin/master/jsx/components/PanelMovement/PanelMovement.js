import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import {componentAction,optionAction} from '../../actions';
import {Button} from 'react-bootstrap';

import ComponentTitle from '../../pages/components/ComponentTitle';
import PanelMovementImage from './PanelMovementImage';
import PanelMovementPreview from './PanelMovementPreview';

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class PanelMovement extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            traits: [],
            backupTraits: [],
            isEdit: false,
        };
        this.save = this.save.bind(this);
        this.saveTrait = this.saveTrait.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.changeTrait = this.changeTrait.bind(this);
    }

    componentWillMount(){
        if(this.props.traits && this.props.traits.length){
            this.setState({traits: this.props.traits});
        }
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits && nextProps.traits.length){
            this.setState({traits: nextProps.traits});
        }
        if(nextProps.cmp && nextProps.cmp.trait && this.props.cmp.trait !== nextProps.cmp.trait){
            let savedTrait = nextProps.cmp.trait.response_data[0],
                traits = this.state.traits
            if(savedTrait.component_id !== this.props.component.id)return;
            if(traits.length && traits.find(function(el,i){return el.name === savedTrait.name})){
                let newTraits = []
                traits.map((trait, i)=>{
                    let newTrait = trait
                    if(trait.name === savedTrait.name){
                        newTrait = savedTrait
                    }
                    newTraits.push(newTrait);
                })
                this.setState({traits: newTraits})
            }else{
                traits.push(savedTrait);
                this.setState({traits: traits})
            }
        }
    }

    enableEdit(){
        var newTraits = [];
        this.state.traits.map((trait)=>{
            newTraits.push(Object.assign({},trait));
        })
        this.setState({isEdit: true, backupTraits: newTraits})
        this.props.setEditMode(true)
    }

    cancelEdit(){
        this.setState({isEdit: false, traits: this.state.backupTraits})
        this.props.setEditMode(false)
    }

    changeTrait(key, value){
        let trait = this.state.traits.find(function(el,i){return el.name === key}),
            newTraits = this.state.traits;
        if(!trait){
            newTraits.push({
                name: key,
                value: value,
                visible: 1
            })
        }else{
            trait.value = value
        }
        this.setState({traits: newTraits})
    }

    saveTrait(trait){
        if(trait && trait.id){
            this.props.ApiUpdateComponentTrait(this.props.systemID,this.props.component.id, trait.id, trait)
        }else{
            this.props.ApiSaveComponentTrait(this.props.systemID,this.props.component.id, trait)
        }
    }

    save(){
        this.props.ApiSaveComponentTraits(
            this.props.systemID,
            this.props.component.id,
            this.state.traits
        );
        this.setState({isEdit: false})
        this.props.setEditMode(false)
    }

    render() {
        const {traits} = this.state;
        const imageTraitsList = ['left', 'right', 'leftSlide', 'rightSlide', 'divider']
        const previewList = ['left', 'right', 'both']
        return (
            <div>
                <ComponentTitle
                    component={this.props.component}
                    traits={this.props.traits}
                    systemID={this.props.systemID}
                    hideProperties={true}
                    cid={this.props.component.id}
                    edit_mode={this.props.edit_mode}
                    additionalButtons={()=>{return false}}
                />
                <div className="optionsBox">
                    <div className="fullOption">
                        {imageTraitsList.map((traitType, i)=>{
                            return <PanelMovementImage key={i} traitType={traitType}
                                                       image={traits.find(function (el, i) {
                                                           return el.name === traitType + 'Img'
                                                       })}
                                                       systemID={this.props.systemID}
                                                       componentID={this.props.component.id}
                                                       onSave={this.saveTrait}
                                                       isEdit={this.state.isEdit}/>
                        })}
                    </div>
                    <div className="fullOption">
                        {previewList.map((previewType, i)=>{
                            return <PanelMovementPreview key={i} traitType={previewType}
                                                         index={i}
                                                         traits={traits}
                                                         onChangeTrait={this.changeTrait}
                                                         systemID={this.props.systemID}
                                                         componentID={this.props.component.id}
                                                         isEdit={this.state.isEdit}/>
                        })}
                    </div>
                    {this.state.isEdit ?
                        <div className="editSaveDelete">
                            <Button onClick={this.save} className="btn btn-default">Save</Button>
                            <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                        </div>
                        :
                        <div className="editSaveDelete">
                            <Button onClick={this.enableEdit} className="btn btn-default" disabled={this.props.edit_mode}>Edit</Button>
                        </div>
                    }
                </div>
            </div>
        );
    }

}

PanelMovement.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        cmp: state.component,
    }), Object.assign({}, componentAction)),
    withProps(apiActions),
)(PanelMovement);