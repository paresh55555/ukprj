import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {componentAction, systemAction} from '../../actions';
import WindowOptions from './WindowOptions';
import ComponentTitle from '../../pages/components/ComponentTitle';

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class WindowComponent extends React.Component {
	constructor(props){
		super(props)
		this.state = {
            options: [],
            traits: [{name:'title',value:''},{name:'title',value:''}],
            autoEditID: null,
        };
	}
	componentWillMount(){
		this.props.ApiGetWindowComponents()
	}
	render(){
		const windows = this.props.cmp.window
		var newwnds=[]
		windows.map((win)=>{
			if(!!newwnds[win.windowSection]){
				newwnds[win.windowSection].push(win)
			}
			else{
				newwnds[win.windowSection] = [win]
			}
		})
		console.log(this.props.component, 'this.props.component')
        return (

			<div id="windowOptions">
				<ComponentTitle
					component={this.props.component}
					traits={this.props.traits}
					systemID={this.props.systemID}
					hideProperties={true}
					cid={this.props.component.id}
					edit_mode={this.props.edit_mode}
					additionalButtons={() => {
                        return false
                    }}
				/>
				<div className="space">Select Windows to be Included</div>
				<div className="space"></div>

                {newwnds.map((wind, index) => {
                    return (
						<WindowOptions options={wind} index={index} key={index}/>
                    )
                })}
			</div>
        )
	}
}

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        cmp: state.component,
    }), Object.assign({}, componentAction)),
    withProps(apiActions),
)(WindowComponent);