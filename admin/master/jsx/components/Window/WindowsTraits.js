import React, { Component, PropTypes } from 'react';
export default function WindowTraits(props) {
  return (
  	 <div id="V-1" className="windowButton">
    	<div className="windowButtonImage">
			<img src={`/api/V4${props.trait.img}`} />
    	</div>
	   </div>
		)
}