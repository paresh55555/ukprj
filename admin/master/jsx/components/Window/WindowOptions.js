import React, { Component, PropTypes } from 'react';
import WindowTraits from './WindowsTraits'
export default function WindowOptions(props) {
  return (
  	<div>
  		<div className="windowSection"> 
	    	<div className={'window_counter'}>{props.index}</div>
	    </div>
	    <div className="pull-left window-options">
	    {!!props.options && props.options.length && props.options.map((trait, index)=>{
	    	return (<WindowTraits trait={trait} key={index} />)
	    })}
	    </div>
	    <div className="windowSeparator">
 			 </div>
  	</div>
		)
}