import React, { PropTypes } from 'react';
import OrderButton from './OrderButton';

function DrawModalButtons(props) {
    const { data, changeOrders, changePosition } = props;
    return (
        <div>
            {data.map((item, i) => (
                <OrderButton
                    key={i}
                    id={i}
                    changeOrders={changeOrders}
                    changePosition={changePosition}
                >
                    {item.name}
                </OrderButton>
            ))}
        </div>
    );
}

DrawModalButtons.propTypes = {
    data: PropTypes.array.isRequired,
    changeOrders: PropTypes.func.isRequired,
    changePosition: PropTypes.func.isRequired,
};

export default DrawModalButtons;
