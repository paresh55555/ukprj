import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { listAction } from '../../actions';
import { apiList } from '../../api';

class ListBuilder extends Component {
    render() {
        return (
            <div>List Builder</div>
        );
    }
}

ListBuilder.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

ListBuilder.propTypes = {
};

export default compose(
    connect(() => ({
    }), listAction),
    withProps(apiList),
)(ListBuilder);

