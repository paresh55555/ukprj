import React, { PropTypes } from 'react';
import { compose } from 'recompose';
import { DragSource, DropTarget } from 'react-dnd';

const listSource = {
    beginDrag(props) {
        return { id: props.id };
    },
};

const listTarget = {
    drop(props, monitor) {
        const selected = monitor.getItem();
        if (selected.id === props.id) {
            return;
        }
        const newData = props.changeOrders(selected.id, props.id);
        props.changePosition(newData, 'statuses');
    },
};

const collectSource = (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging(),
});

const collectTarget = connect => ({ connectDropTarget: connect.dropTarget() });

function OrderButton(props) {
    const { children, connectDragSource, connectDropTarget, isDragging } = props;
    const opacity = isDragging ? 0 : 1;

    return (
        connectDragSource(connectDropTarget(
            <button className="btn btn-default btn-primary" type="button" style={{ opacity }}>{children}</button>,
        ))
    );
}

OrderButton.propTypes = {
    children: PropTypes.node.isRequired,
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
};

export default compose(
    DragSource('buttons', listSource, collectSource),
    DropTarget('buttons', listTarget, collectTarget),
)(OrderButton);
