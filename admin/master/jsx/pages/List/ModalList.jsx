import React, { PropTypes } from 'react';
import { Modal, Button } from 'react-bootstrap';

function ModalCart(props) {
    const { show, onClose, title, children, onSave } = props;
    return (
        <Modal show={show} onHide={onClose}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={onSave}>Save</Button>
                <Button onClick={onClose}>Cancel</Button>
            </Modal.Footer>
        </Modal>
    );
}

ModalCart.propTypes = {
    onClose: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    show: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
};

export default ModalCart;
