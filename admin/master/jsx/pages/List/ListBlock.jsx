import React, { PropTypes } from 'react';
import { Button } from 'react-bootstrap';
import DropdownButton from '../../components/Common/Dropdown/DropdownButton';

function ListBlock(props) {
    const { title, dropdownData, type, addData, deleteData, children, openModal, deleteList } = props;
    const typeName = type === 'statuses' ? 'Status' : 'Column'
    return (
        <div className="panel-default panel mainBox">
            <div className="panel-body">
                <div>
                    <div className="bg-info-dark defaultWidth">
                        <div className="optionTitle item-515 editable editable-click editable-disabled">
                            {title}:
                        </div>
                    </div>
                    <div className="editSave">
                        <DropdownButton
                            data={dropdownData}
                            changeData={addData}
                            id="addDropdown"
                            type={type}
                            Datakey={'ui_name'}
                        >
                            Add {typeName}
                        </DropdownButton>
                        <DropdownButton
                            data={deleteList}
                            changeData={deleteData}
                            id="deleteDropdown"
                            type={type}
                            Datakey={'ui_name'}
                        >
                            Delete {typeName}
                        </DropdownButton>
                        <Button onClick={() => openModal(type)}>Re-Order</Button>
                    </div>
                    {children}
                </div>
            </div>
        </div>
    );
}

ListBlock.propTypes = {
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    dropdownData: PropTypes.array.isRequired,
    addData: PropTypes.func.isRequired,
    deleteData: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
};

export default ListBlock;

