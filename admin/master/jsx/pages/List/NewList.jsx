import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert'
import { compose, withProps } from 'recompose';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { listAction } from '../../actions';
import { apiList } from '../../api';
import LayoutNameEdit from '../Cart/LayoutNameEdit';
import Modal from './ModalList';
import { findNextOrder } from '../Cart/helpers';
import DrawButtons from './DrawButtons';
import DrawModalData from './DrawModalData';
import DrawTableList from './DrawTableList';
import ListBlock from './ListBlock';
import DeleteDialog from '../../components/Common/DeleteDialog'
import Loader from "../../components/loader/Loader";


class NewList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            builderDetails: {
                name:'',
                statuses:[],
                columns:[]
            },
            showListModal:false,
            selectedData:[],
            selectedType:''
        };
        this.changeName = this.changeName.bind(this);
        this.addData = this.addData.bind(this);
        this.deleteData = this.deleteData.bind(this);
        this.changeOrders = this.changeOrders.bind(this);
        this.openModal = this.openModal.bind(this);
        this.onClose = this.onClose.bind(this);
        this.saveOrder = this.saveOrder.bind(this);
        this.backToList = this.backToList.bind(this)
        this.saveBuilder = this.saveBuilder.bind(this)
        this.changeListOrder = this.changeListOrder.bind(this)
    }
    componentWillMount(){
        this.props.setCurrentListType(this.props.params.type)
        this.props.getAvailable(this.props.params.type.toLowerCase())
        this.props.getListBuildeDetailsApi(this.props.params.id)
        this.props.getSampleDatasApi(this.props.params.id)
    }
    componentWillReceiveProps(nextProps){
        if(this.props.list.builderDetails!== nextProps.list.builderDetails){
            let builderDetails = nextProps.list.builderDetails;
            if(Array.isArray(builderDetails)){
                builderDetails = {}
            }
            if(!builderDetails['statuses']){
                builderDetails['statuses'] = []
            }
            if(!builderDetails['columns']){
                builderDetails['columns'] = []
            }
            this.setState({builderDetails:builderDetails});
        }
    }
    getBuilderColumns(){
        let {builderDetails} = this.state;
        let columns = []
        let statuses = []
        statuses = builderDetails.statuses.map(status=>status.name);
        columns = builderDetails.columns.map(column=>column.name);
        return {statuses,columns}
    }
    changeName(e) {
        let builderDetails = this.state.builderDetails;
        builderDetails['name'] = e.target.value 
        this.setState({builderDetails });
    }
    backToList() {
        this.context.router.push('/builder/list');
    }
    addData(name, type) {
       let builderDetails = this.state.builderDetails;
        const order = findNextOrder(builderDetails[type]);
        const data = {
            name,
            order,
        };
        if(type === 'columns'){
            data.name = data.name.column_name;
            //data = this.props.column.columns.find(column => column.ui_name  === data)
           
        }        
        builderDetails[type].push(data);
        this.setState({builderDetails})
    }
    deleteData(name, type) {
        let builderDetails = this.state.builderDetails;
        const deleteIndex =  builderDetails[type].findIndex(item => item.name === name);
        builderDetails[type].splice(deleteIndex,1);
        this.setState({builderDetails})
    }
    changeOrders(selectedId, dropedId) {
        const { selectedData } = this.state;
        const newColumnItemCopy = [...selectedData];
        const selectedObject = Object.assign({}, newColumnItemCopy[selectedId]);
        const dropedObject = Object.assign({}, newColumnItemCopy[dropedId]);
        const selectedOrder = selectedObject.order;
        const dropedOrder = dropedObject.order;
        dropedObject.order = selectedOrder;
        selectedObject.order = dropedOrder;
        newColumnItemCopy[selectedId] = dropedObject;
        newColumnItemCopy[dropedId] = selectedObject;
        return newColumnItemCopy;
    }
    openModal(type) {
        let {builderDetails} = this.state;
        this.setState({showListModal:true,selectedData:builderDetails[type],selectedType:type})
    }
    onClose(){
        this.setState({showListModal:false,selectedData: [],selectedType:''})

    }
    saveOrder() {
        let {builderDetails,selectedData,selectedType} = this.state;
        builderDetails[selectedType] = selectedData;
        this.setState({builderDetails})
        this.onClose()
    }
    deleteList(){
        DeleteDialog({
            text: "Are you sure you want to delete this list builder? ",
            onConfirm:  ()=> {
                this.props.ApiDeleteListBuilder(this.props.params.id).then(res=>{
                     swal("Deleted!",
                         "List builder has been deleted.",
                        "success");
                    this.backToList()
                })
            }
        })
    }
    saveBuilder(){
        let builderDetails = this.state.builderDetails;
        this.props.saveBuilder(builderDetails,this.props.params.id).then(res=>{
            this.backToList()
        })
    }
    changeListOrder(data){
        this.setState({selectedData:data})
    
    }
    render() {
        const { builderDetails } = this.state;
        const {
            currentListType,
            sampleData,
        } = this.props;
        const currentType = currentListType ? currentListType : JSON.parse(sessionStorage.getItem('currentListType'));
        let builderColumns = this.getBuilderColumns()
        const availableStatus = this.props.column && this.props.column.statuses.filter(status => builderColumns.statuses.indexOf(status) < 0 )
        const availableColumn = this.props.column &&  this.props.column.columns.filter(column => builderColumns.columns.indexOf(column.column_name) < 0 )
        const deleteColumn = this.props.column &&  this.props.column.columns.filter(column => builderColumns.columns.indexOf(column.column_name) > -1 )

        return (
            <ContentWrapper>
                <h3><a onClick={this.backToList}>List Builder</a> / <a onClick={this.backToList}>For {currentType}s</a> / {builderDetails.name || ''}</h3>
                {this.props.list.isLoading ? <Loader />:
                <div>
                <LayoutNameEdit
                    name={builderDetails.name || ''}
                    changeName={this.changeName}
                    fieldName="List"
                />
                <ListBlock
                    title={`${currentType} Status Bar`}
                    dropdownData={availableStatus || []}
                    deleteList = {builderColumns.statuses}
                    type="statuses"
                    openModal={this.openModal}
                    addData={this.addData}
                    deleteData={this.deleteData}
                >
                    <DrawButtons data={builderDetails.statuses} />
                </ListBlock>
                <ListBlock
                    title={`${currentType} Table`}
                    dropdownData={availableColumn || []}
                    deleteList={deleteColumn}
                    data={builderDetails.columns}
                    type="columns"
                    addData={this.addData}
                    openModal={this.openModal}
                    deleteData={this.deleteData}
                >
                    <DrawTableList
                        fieldData={builderDetails.columns}
                        data={sampleData}
                        columns={this.props.column.columns}
                    />
                </ListBlock>
                <div className="bottom mainBox">
                    <button type="button" className="btn btn-default" onClick={this.saveBuilder} disabled={this.props.list.isSaving}>{this.props.list.isSaving ? 'Saving' : 'Save'}</button>
                    <button type="button" className="btn btn-default" onClick={this.backToList}>Cancel</button>
                    <button type="button" className="btn btn-default pull-right" onClick={this.deleteList.bind(this)}>Delete</button>
                </div>
                 </div>

                }
               
                <Modal
                    show={this.state.showListModal}
                    onClose={this.onClose}
                    title={`Re-Order ${this.state.selectedType === 'statuses' ? 'Status Bar' : currentType+' Table'}`}
                    onSave={this.saveOrder}
                >
                    <DrawModalData
                        data={this.state.selectedData}
                        changeOrders={this.changeOrders}
                        changePosition={this.changeListOrder}
                    />
                </Modal>
            </ContentWrapper>
        );
    }
}

NewList.contextTypes = {
    router: React.PropTypes.object.isRequired,
};
export default compose(
    connect(state => ({
        list:state.list,
        currentListType: state.list.currentListType,
        column: state.list.column,
        sampleData: state.list.sampleData,
    }), listAction),
    withProps(apiList),
)(NewList);

