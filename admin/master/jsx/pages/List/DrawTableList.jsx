import React, { PropTypes } from 'react';
import DataTable from '../../components/datatable/DataTable';

function DrawTableList(props) {
    const { data, fieldData, columns } = props;
    let blankData = {}
    columns.map(c=>{
        blankData[c.column_name] = ''
    })
    const generateFields = () => {
        const fields = {};
        fieldData.forEach((item) => {
            fields[item.name] = item.name.replace(/_/g, ' ');
        });
        return fields;
    };
    const fields = fieldData.length > 0 ? generateFields() : false;
    let cdata = []
    data.forEach(d=>{
        console.log(fields,d)
        cdata.push(Object.assign({},blankData, d));
    })
    return (
        <div className="listTable">
            {fields ?
                <DataTable
                    pagination={false}
                    noClick
                    data={cdata}
                    fields={fields}
                    isLoading={false}
                    id="listTable"
                    link="list"
                />
                : null
            }
        </div>
    );
}

DrawTableList.propTypes = {
    data: PropTypes.array.isRequired,
    fieldData: PropTypes.array.isRequired,
};

export default DrawTableList;
