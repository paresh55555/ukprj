import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import { cartAction } from '../../actions';
import { apiCart } from '../../api';
import LinkBox from '../../components/linkbox/LinkBox';

function BuilderLayout(props) {
    const { type, data, setCurrentList, setCurrentListType, addListBuilder } = props;   
    console.log(props, 'ths.parosdf')
    return (
        <div className="panel-default panel mainBox">
            <div className="panel-body">
                <div>
                    <div className="bg-info-dark defaultWidth">
                        <div className="optionTitle item-515 editable editable-click editable-disabled">
                            For {type}s
                        </div>
                    </div>
                    <div className="editSave">
                        <Button onClick={() => addListBuilder(type)}>
                           
                                Add {type} List
                           
                        </Button>
                    </div>
                </div>
                <Grid fluid>
                    <Row>
                        {data.map(item => (
                            <Col md={2} sm={4} key={item.id}>
                                <span role="button" tabIndex={0} onClick={() => setCurrentList(item)}>
                                    <LinkBox
                                        url={`builder/list/${type}/${item.id}`}
                                        params={{ name: item.name }}
                                        title={item.name ? item.name : '< Empty >'}
                                        className="bg-gray"
                                    />
                                </span>
                            </Col>
                        ))}
                    </Row>
                </Grid>
            </div>
        </div>
    );
}

BuilderLayout.propTypes = {
    setCurrentList: PropTypes.func.isRequired,
    setCurrentListType: PropTypes.func.isRequired,
    data: PropTypes.array.isRequired,
    type: PropTypes.string.isRequired,
    addListBuilder: PropTypes.func.isRequired
};

export default compose(
    connect(() => ({
    }), cartAction),
    withProps(apiCart),
)(BuilderLayout);

