import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { listAction } from '../../actions';
import { apiList } from '../../api';
import BuilderLayout from './BuilderLayout'

class ItemsList extends Component {
    constructor(props){
        super(props)
    }
    componentDidMount() {
        const { getListBuilderApi } = this.props;
        getListBuilderApi();
    }
    generateList(type) {
        const { list } = this.props;
        return list.filter(item => item.type === type);
    }
    addListBuilder(type){
        this.props.addListBuilderApi({type:type.toLowerCase()}).then((res)=>{
            let id = res.data.data[0].id;
            this.props.getListBuilderApi();
            this.context.router.push('/builder/list/'+type+'/'+id)
       })
    }
    render() {
        const { setCurrentList, setCurrentListType } = this.props;
        const quotes = this.generateList('quote');
        const orders = this.generateList('order');
        return (
            <ContentWrapper>
                <h3>List Builder</h3>
                <BuilderLayout
                    type="Quote"
                    data={quotes}
                    setCurrentList={setCurrentList}
                    setCurrentListType={setCurrentListType}
                    addListBuilder= {this.addListBuilder.bind(this)}
                />
                <BuilderLayout
                    type="Order"
                    data={orders}
                    setCurrentList={setCurrentList}
                    setCurrentListType={setCurrentListType}
                    addListBuilder= {this.addListBuilder.bind(this)}
                />
            </ContentWrapper>
        );
    }
}

ItemsList.propTypes = {
    setCurrentListType: PropTypes.func.isRequired,
    setCurrentList: PropTypes.func.isRequired,
    getListBuilderApi: PropTypes.func.isRequired,
    list: PropTypes.array.isRequired
};
ItemsList.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

export default compose(
    connect(state => ({
        list: state.list.list,
    }), listAction),
    withProps(apiList),
)(ItemsList);

