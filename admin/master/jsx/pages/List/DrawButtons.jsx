import React, { PropTypes } from 'react';
import { Button } from 'react-bootstrap';

function DrawButtons(props) {
    const { data } = props;
    return (
        <div>
            {data.map((item, i) => (
                <Button className="btn-primary" key={i}>{item.name}</Button>
            ))}
        </div>
    );
}

DrawButtons.propTypes = {
    data: PropTypes.array.isRequired,
};

export default DrawButtons;
