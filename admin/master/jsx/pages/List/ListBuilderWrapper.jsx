import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { listAction } from '../../actions';
import { apiList } from '../../api';
import ListBuilder from './ListBuilder';


class ListBuilderWrapper extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {}
    backToPage() {
        this.context.router.push('/builder/list');
    }
    render() {
        return (
            <ListBuilder
                backToPage={this.backToPage}
            />
        );
    }
}

ListBuilderWrapper.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

ListBuilderWrapper.propTypes = {
};

export default compose(
    connect(state => ({
        currentList: state.cart.currentList,
    }), listAction),
    withProps(apiList),
)(ListBuilderWrapper);

