import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import {Button} from 'react-bootstrap';
import {systemAction, componentAction} from '../../actions';
import {NotifyAlert} from '../../components/Common/notify.js'
import DataTable from '../../components/datatable/DataTable.jsx'

import SampleDoor from '../../components/Common/SampleDoor'
import AddParts from '../../components/Common/AddParts'
import FoldingPanel from '../../components/Common/FoldingPanel'
import PartsComponent from '../../components/Parts/PartsComponent'
import PartsOption from '../../components/Parts/PartsOption'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

import {ApiSystems, ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class SystemParts extends React.Component {
    constructor() {
        super();
        this.state = {
            action: '',
            published:false,
            components: []
        }
        this.routerWillLeave = this.routerWillLeave.bind(this);
        this.getPartsDoorContent =  this.getPartsDoorContent.bind(this);
    }
    componentWillMount() {

        const {id} = this.props.params;

        ApiSystems.getSampleDoorParts(id)
        this.props.ApiGetSystemComponent(id)
        this.props.ApiGetComponentData(id)
    }
    componentDidMount(){
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
        // return false to prevent a transition w/o prompting the user,
        // or return a string to allow the user to decide:
        if (this.props.component.edit_mode)
            return 'Your work is not saved! Are you sure you want to leave?'
    }
    componentWillUnmount(){
        this.props.setEditMode(false)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.component.isPublished && this.state.published){
            NotifyAlert(`System published successfully`);
            this.setState({published:false})
        }
    }
    getPartsDoorContent(component){
        let {system} = this.props.component;
        return [
            <PartsOption key="dimensions" systemID={system && system.id} component={component} componentID={component && component.id || null}/>
        ]
    }
    publishSystem(){
        const systemTitle = this.props.component && this.props.component.system && this.props.component.system.traits && this.props.component.system.traits.length &&
            this.props.component.system.traits.find((trait)=>trait.name ==='title')
        this.setState({published:true})
        this.props.ApiPublishSystemComponents(this.props.params.id, systemTitle && systemTitle.value)
    }

    render() {
        var self = this
        const {system, list} = this.props.component;
        const {sampleDoors} = this.props.system;
        const title = system && system.traits && system.traits.find((trait)=>{return trait.name === 'title'})
        const componentsList = list.filter((component)=>{ return ['system','menu','SampleDoorParts','SampleDoorPrice','ColorGroup','ColorGroupSubGroup', 'Dimensions'].indexOf(component.type) < 0 })

        const dimensions = list.find(component => {
            return component.type == 'Dimensions';
        });
        const name =  this.props.component.system.name === 'Door System' ? 'Door System Template': (this.props.component.system.name === 'Window System' ? 'Window System Template' : 'Active System')
        const url =  this.props.component.system.name === 'Door System' ? '/builder/systems/doors': (this.props.component.system.name === 'Window System' ? '/builder/systems/windows' : '/systems')
        const breadCrumbData = [
            {
                title: name, 
                url:url,
                active:false
            },{
                title : title && title.value || '',
                url:`/systems/${this.props.params.id}`,
                active:false
            },
            {
                title : 'Parts',
                url:'',
                active:true
            }

        ]


        return (
            <ContentWrapper>
                <Breadcrumbs data={breadCrumbData} />
                <div>
                    
                    <div className="panel-default panel mainBox">
                        <Button disabled={this.props.component.isLoading} onClick={this.publishSystem.bind(this)} className="publish-system btn btn-labeled btn-info pull-left">
                            Publish "{title && title.value}"
                        </Button>
                        <div className="panel-body">
                            <SampleDoor
                                systemID={system && system.id}
                                componentID={sampleDoors && sampleDoors.length && sampleDoors[0].id}
                                traits={sampleDoors && sampleDoors.length && sampleDoors[0].traits || []} />
                            <FoldingPanel title="Parts on Every Door" content={this.getPartsDoorContent(dimensions)}/>
                            {componentsList && componentsList.map((component)=>{
                                if(component.type === 'Color'){

                                    let groups = list.filter((cmp)=>{ return cmp.type === 'ColorGroup' && cmp.grouped_under === component.id })

                                    return <PartsComponent key={component.id} systemID={system && system.id} component={component} options={groups}/>
                                }else{
                                    return <PartsComponent key={component.id} systemID={system && system.id} component={component} options={component.options} />
                                }
                            })}
                        </div>
                    </div>
                </div>
            </ContentWrapper>
        );
    }

}
SystemParts.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiSystems, ApiComponent);

export default compose(
    connect(state => ({
        system: state.system,
        component: state.component,
    }), Object.assign({}, systemAction, componentAction)),
    withProps(apiActions),
)(SystemParts);