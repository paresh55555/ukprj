import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {componentAction} from '../../actions';
import {SQ_PREFIX} from '../../constant.js'

class PremadeSubGroup extends React.Component {
    constructor() {
        super()
        this.state = {}
        this.selectSubGroup = this.selectSubGroup.bind(this);
        this.chooseSubGroup = this.chooseSubGroup.bind(this);
    }
    componentWillMount(){
    }

    componentWillReceiveProps(nextProps){
    }

    selectSubGroup(){
        this.props.onSelectSubGroup(this.props.subGroupID)
    }

    chooseSubGroup(){
        this.props.onChooseSubGroup(this.props.subGroupID, 'subGroups')
    }

    render(){
        const titleClass = (this.props.selected) ? "" : "subColorMenuNotSelected";
        const title = this.props.data.traits.find(function(el,i){return el.name === 'title'});
        const imgUrl = this.props.data.traits.find(function(el,i){return el.name === 'imgUrl'});
        return(
            <div className="subColorMenu">
                <div className={titleClass} onClick={this.selectSubGroup}>
                    <div className="subColorImage">
                        <div className="subColorThumbnail thinBorder2" style={{backgroundImage: `url(${'/'+(imgUrl && imgUrl.value)})`}}></div>
                    </div>
                    <div className="thumbnailTitle">
                        <span>{title && title.value}</span>
                    </div>
                    {this.props.chosen ? <div className="demoSubGroupSelected"></div> : null}
                </div>
                {this.props.isEdit && !this.props.exitsing &&
                    <div className="editSaveDelete">
                        <Button onClick={this.chooseSubGroup} className="btn btn-default thumbButton">{this.props.chosen ? 'De-select' : 'Select'}</Button>
                    </div>
                }
            </div>
        )
    }
}

PremadeSubGroup.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default connect(state => ({
}), Object.assign({}, componentAction))(PremadeSubGroup);