import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {premadeAction, componentAction} from '../../actions';
import {Button} from 'react-bootstrap';
import Loader from '../../components/loader/Loader';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import _ from 'lodash';
import {SQ_PREFIX} from '../../constant.js'
import PremadeGroup from './PremadeGroup'
import PremadeSubGroup from './PremadeSubGroup'
import PremadeColor from './PremadeColor'

import {ApiPremade, ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class PremadeLanding extends React.Component {
    constructor() {
        super();
        this.state = {
            data:[],
            previousChoosen: null,
            isChosen: true,
            selectedGroupID: null,
            selectedSubGroupID: null,
            choosenIDs:{
                groups: [],
                subGroups: [],
                colors:[]
            },
            existingIDs:{
                groups: [],
                subGroups: [],
                colors:[]
            },
        };
        this.goBack = this.goBack.bind(this);
        this.saveSelection = this.saveSelection.bind(this);
        this.getColorsForSave = this.getColorsForSave.bind(this);
        this.selectGroup = this.selectGroup.bind(this);
        this.selectSubGroup = this.selectSubGroup.bind(this);
        this.chooseItem = this.chooseItem.bind(this);
        this.onChooseColor = this.onChooseColor.bind(this);
    }
    componentWillMount(){
        var self = this
        if(this.props.data && this.props.data.length){
            this.setState({
                data:this.props.data,
                selectedGroupID: this.props.data && this.props.data[0].id,
                selectedSubGroupID: this.getFirstSubGroup(this.props.data)
            });
            this.setState({data:this.props.data}, function(){
                ApiPremade.getComponentDetail(parseInt(self.props.systemID), parseInt(self.props.componentID));
            })
        }else{

            ApiPremade.getPremadeColors();
            ApiPremade.getComponentDetail(parseInt(self.props.systemID), parseInt(self.props.componentID));
        
        }
    }

    goBack(save){
        save = save || false
        this.props.setPremadeMode(false)
        this.props.ApiUpdateColorComponent(this.props.systemID,this.props.colorComponentID);
    }

    getFirstSubGroup(data){
        let firstSubGroupID = 0;
        if(data && data.length){
            data.map((group, i)=>{
                if(group.subGroups && group.subGroups.length){
                    firstSubGroupID = group.subGroups[0].id
                }
            })
        }
        return firstSubGroupID;
    }

    componentWillReceiveProps(nextProps){
        // console.log('Receive: ', this.props, nextProps.premade.isSaved);
        if(nextProps.premade.isSaving)return;
        if(nextProps.premade.isSaved){
            this.goBack(true);
        }else{
            if(nextProps.premade.data && nextProps.premade.data.length && !nextProps.premade.isLoading && !nextProps.premade.isSaved){
                this.props.setPremadeData(nextProps.premade.data);
                this.setState({
                    data:nextProps.premade.data,
                    selectedGroupID: nextProps.premade.data && nextProps.premade.data[0].id,
                    selectedSubGroupID: this.getFirstSubGroup(nextProps.premade.data)
                });
            }
            if(this.props.premade.component !== nextProps.premade.component && !nextProps.premade.isSaved){
                this.checkChoosen(nextProps.premade.component);
            }
        }
    }

    checkChoosen(data){
        var self = this;
        switch (this.props.mode){
            case 'color':
                if(data.options && data.options.length){
                    var existingIDs = this.state.existingIDs;
                    data.options.map((color, i)=>{
                        if(color.traits && color.traits.length){
                            var name = color.traits.find(function(el,i){
                                return el.name === 'title'
                            });
                            name = name ? name.value : '';
                            self.state.data.map((group, i)=>{
                                if(group.subGroups && group.subGroups.length){
                                    group.subGroups.map((subGroup, i)=>{
                                        subGroup.colors.map((color, i)=>{
                                            if(color.title === name){
                                                self.chooseItem(color.id, 'colors');
                                                existingIDs.colors.push(color.id);
                                            }
                                        })
                                    })
                                }else if (group.colors){
                                    group.colors.map((color, i)=>{
                                        if(color.title === name){
                                            self.chooseItem(color.id, 'colors');
                                            existingIDs.colors.push(color.id);
                                        }
                                    })
                                }
                            })

                        }
                    })
                    this.setState({existingIDs:existingIDs})
                }
                break;
            case 'group':
                if(data.groups && data.groups.length){
                    var existingIDs = this.state.existingIDs;
                    data.groups.map((group, i)=>{
                        if(group.traits && group.traits.length){
                            var name = group.traits.find(function(el,i){
                                return el.name === 'title'
                            });
                            name = name ? name.value : '';
                            self.state.data.map((group, i)=>{
                                let title = group.traits.find((el,i)=>{return el.name === 'title'});
                                if(title.value === name){
                                    self.chooseItem(group.id, 'groups');
                                    existingIDs.groups.push(group.id);
                                }
                            })
                        }
                    })
                    this.setState({existingIDs:existingIDs})
                }
                break;
            case 'subGroup':
                if(data.subGroups && data.subGroups.length){
                    var existingIDs = this.state.existingIDs;
                    data.subGroups.map((subGroup, i)=>{
                        if(subGroup.traits && subGroup.traits.length){
                            var name = subGroup.traits.find(function(el,i){
                                return el.name === 'title'
                            });
                            name = name ? name.value : '';
                            self.state.data.map((group, i)=>{
                                if(group.subGroups && data.subGroups.length){
                                    group.subGroups.map((subGroup, i)=>{
                                        let title = subGroup.traits.find((el,i)=>{return el.name === 'title'})
                                        if(title && title.value === name){
                                            self.chooseItem(subGroup.id, 'subGroups');
                                            existingIDs.subGroups.push(subGroup.id);
                                        }
                                    })
                                }
                            })
                        }
                    })
                    this.setState({existingIDs:existingIDs})
                }
                break;
        }
    }

    selectGroup(id){
        this.setState({selectedGroupID:id})
        // Select first sub-group for the first time
        if(!this.state.selectedSubGroupID){
            var result = $.grep(this.state.data, function(e){ return e.id == id; });
            if(result && result.length && result[0].subGroups && result[0].subGroups.length){
                this.selectSubGroup(result[0].subGroups[0].id)
            }
        }
    }

    selectSubGroup(id){
        this.setState({selectedSubGroupID:id});
    }

    chooseItem(id, type){
        var chosenIDs = this.state.choosenIDs,
            index = chosenIDs[type].indexOf(id),
            existing = this.state.existingIDs[type].indexOf(id)>=0;
        if(index>=0 && !existing){
            chosenIDs[type].splice(index, 1);
        }else if(!existing){
            chosenIDs[type].push(id);
        }
        this.setState({chosenIDs: chosenIDs});
    }

    onChooseColor(id, type) {
        if(!this.state.previousChoosen) {
            const previousChoosen = [...this.state.choosenIDs.colors];
            this.setState({previousChoosen});
            this.setState({isChosen: false});
        }
        this.chooseItem(id, type);
        if (!_.difference(this.state.choosenIDs.colors, this.state.previousChoosen).length > 0) {
            this.setState({isChosen: true});
        } else if (this.state.isChosen) {this.setState({isChosen: false})}
    }


    saveSelection(){
        var self = this,
            data;
        switch (this.props.mode){
            case 'color':
                data = this.getColorsForSave();
                ApiPremade.savePremadeColors(this.props.systemID,this.props.componentID,data);
                break;
            case 'group':
                data = this.getGroupsForSave();
                ApiPremade.savePremadeGroups(this.props.systemID,this.props.componentID,data);
                break;
            case 'subGroup':
                data = this.getSubGroupsForSave();
                ApiPremade.savePremadeSubGroups(this.props.systemID,this.props.componentID,data);
                break;
        }
    }

    getColorsForSave(){
        var data = [],
            self = this;
        $.each(this.state.choosenIDs.colors, function(i,id){
            self.state.data.map((group, i)=>{
                if(group.subGroups && group.subGroups.length){
                    group.subGroups.map((subGroup, i)=>{
                        subGroup.colors.map((color, i)=>{
                            if(color.id === id && self.state.existingIDs.colors.indexOf(color.id)<0){
                                data.push(color);
                            }
                        })
                    })
                }else if (group.colors){
                    group.colors.map((color, i)=>{
                        if(color.id === id && self.state.existingIDs.colors.indexOf(color.id)<0){
                            data.push(color);
                        }
                    })
                }
            })
        })
        return data;
    }

    getGroupsForSave(){
        var data = [],
            self = this;
        $.each(this.state.choosenIDs.groups, function(i,id){
            self.state.data.map((group, i)=>{
                if(group.id === id && self.state.existingIDs.groups.indexOf(group.id)<0){
                    data.push(group);
                }
            })
        })
        return data;
    }

    getSubGroupsForSave(){
        var data = [],
            self = this;
        $.each(this.state.choosenIDs.subGroups, function(i,id){
            self.state.data.map((group, i)=>{
                if(group.subGroups && group.subGroups.length){
                    group.subGroups.map((subGroup, i)=>{
                        if(subGroup.id === id && self.state.existingIDs.subGroups.indexOf(subGroup.id)<0){
                            data.push(subGroup);
                        }
                    })
                }
            })
        })
        return data;
    }

    getContent(data){
        var groups, subGroups, colors,
            self = this,
            buttons = <div className="editSave">
                <Button onClick={this.saveSelection} disabled={this.props.mode === 'color' ? this.state.isChosen : false} className="btn btn-default">Save Selections</Button>
                <Button onClick={this.goBack} className="btn btn-default">Cancel</Button>
            </div>;
        if(data && !data.length){
            return <div>{this.props.premade.isLoading ? <Loader/> : 'No data available'}</div>
        }

        if(this.props.mode === 'subGroup'){
            groups = data.map((group, i)=>{
                if(group.subGroups && group.subGroups.length){
                    subGroups = group.subGroups.map((subGroup, i)=>{
                            if(self.state.selectedSubGroupID === subGroup.id){
                                colors = subGroup.colors.map((color, i)=>{
                                    return <PremadeColor
                                        data={color}
                                        colorID={color.id}
                                        selected={self.state.choosenIDs.colors.indexOf(color.id)>=0}
                                        isEdit={self.props.mode === 'color'}
                                        onChooseColor={this.onChooseColor}
                                        key={i}/>
                                })
                            }
                            return <PremadeSubGroup
                                data={subGroup}
                                subGroupID={subGroup.id}
                                selected={self.state.selectedSubGroupID === subGroup.id}
                                chosen={self.state.choosenIDs.subGroups.indexOf(subGroup.id)>=0}
                                exitsing={self.state.existingIDs.subGroups.indexOf(subGroup.id)>=0}
                                isEdit={self.props.mode === 'subGroup'}
                                onSelectSubGroup={self.selectSubGroup}
                                onChooseSubGroup={self.chooseItem}
                                key={i}/>
                        })
                }
                return null
            })
        }else{
            groups = data.map((group, i)=>{
                if(self.state.selectedGroupID === group.id){
                    if(group.subGroups && group.subGroups.length){
                        subGroups = group.subGroups && group.subGroups.map((subGroup, i)=>{
                                if(self.state.selectedSubGroupID === subGroup.id){
                                    colors = subGroup.colors.map((color, i)=>{
                                        return <PremadeColor
                                            data={color}
                                            colorID={color.id}
                                            selected={self.state.choosenIDs.colors.indexOf(color.id)>=0}
                                            isEdit={self.props.mode === 'color'}
                                            onChooseColor={this.onChooseColor}
                                            key={i}/>
                                    })
                                }
                                return <PremadeSubGroup
                                    data={subGroup}
                                    subGroupID={subGroup.id}
                                    selected={self.state.selectedSubGroupID === subGroup.id}
                                    chosen={self.state.choosenIDs.subGroups.indexOf(subGroup.id)>=0}
                                    isEdit={self.props.mode === 'subGroup'}
                                    onSelectSubGroup={self.selectSubGroup}
                                    onChooseSubGroup={self.chooseItem}
                                    key={i}/>
                            })
                    }else if (group.colors){
                        colors = group.colors.map((color, i)=>{
                            return <PremadeColor
                                data={color}
                                colorID={color.id}
                                selected={self.state.choosenIDs.colors.indexOf(color.id)>=0}
                                isEdit={self.props.mode === 'color'}
                                onChooseColor={this.onChooseColor}
                                key={i}/>
                        })
                    }
                }
                return <PremadeGroup
                    data={group}
                    groupID={group.id}
                    selected={self.state.selectedGroupID === group.id}
                    chosen={self.state.choosenIDs.groups.indexOf(group.id)>=0}
                    exitsing={self.state.existingIDs.groups.indexOf(group.id)>=0}
                    isEdit={self.props.mode === 'group'}
                    onSelectGroup={self.selectGroup}
                    onChooseGroup={self.chooseItem}
                    key={i}/>
            })
        }
        return <div>
            <div className="optionsBox optionsBoxFrontEnd">
                { groups }
                {this.props.mode === 'group' && groups &&
                    buttons
                }
            </div>
            <div className="optionsBox optionsBoxFrontEnd">
                { subGroups }
                {this.props.mode === 'subGroup' &&
                    buttons
                }
            </div>
            <div className="optionsBox optionsBoxFrontEnd">
                { colors }
                {this.props.mode === 'color' && colors &&
                    buttons
                }
            </div>
        </div>
    }

    getTitle(){
        switch (this.props.mode){
            case 'color':
                return <div className="bg-info-dark defaultWidth">
                    <div className="optionTitle">Choose From the Following PreMade Colors:</div>
                </div>
            case 'group':
                return <div className="bg-info-dark defaultWidth">
                    <div className="optionTitle">Choose From the Following PreMade Colors Groups:</div>
                    <div className="optionNote">All Subgroups and colors will come along with the selected groups.</div>
                </div>
            case 'subGroup':
                return <div className="bg-info-dark defaultWidth">
                    <div className="optionTitle">Choose From the Following PreMade Sub-Groups:</div>
                    <div className="optionNote">All colors will come along with the selected Sub-Groups.</div>
                </div>
            default:
                return '';
        }
    }

    render() {
        var self = this,
            data = this.state.data;
        const content = this.getContent(data);
        const title = this.getTitle();

        return (
            <ContentWrapper>
                <div className="panel-default panel mainBox">
                    <div className="panel-body">
                        { title }
                        { content }
                    </div>
                </div>
            </ContentWrapper>
        );
    }

}

PremadeLanding.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        premade: state.premade
    }), Object.assign({}, componentAction, premadeAction)),
    withProps(apiActions),
)(PremadeLanding);