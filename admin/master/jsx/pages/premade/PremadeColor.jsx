import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {componentAction} from '../../actions';

class PremadeColor extends React.Component {
    constructor() {
        super()
        this.state = {
        }
        this.chooseColor = this.chooseColor.bind(this);
    }
    componentWillMount(){
    }

    componentWillReceiveProps(nextProps){
    }

    chooseColor(){
        if(this.props.isEdit){
            this.props.onChooseColor(this.props.colorID, 'colors')
        }
    }

    render(){
        var style = this.props.data.hex ? {backgroundColor:this.props.data.hex} :
                        this.props.data.imgUrl ? {backgroundImage:`url(/${this.props.data.imgUrl})`} : {};
        return(
            <div className="thumbOption" onClick={this.chooseColor}>
                <div className="horizontalImage">
                    <div className="thumbnail2 thinBorder2" style={style}></div>
                </div>
                {this.props.selected ? <div className="demoColorSelected"></div> : null}
                {this.props.data.note ?
                    <div>
                        <div className="thumbnailTitle">{this.props.data.note}</div>
                        <div className="thumbnailNote">{this.props.data.title}</div>
                    </div>
                    :
                    <div className="thumbnailTitle">{this.props.data.title}</div>
                }
            </div>
        )
    }
}

PremadeColor.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default connect(state => ({
}), Object.assign({}, componentAction))(PremadeColor);