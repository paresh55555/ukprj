import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {componentAction} from '../../actions';

class PremadeGroup extends React.Component {
    constructor() {
        super()
        this.state = {
        }
        this.selectGroup = this.selectGroup.bind(this);
        this.chooseGroup = this.chooseGroup.bind(this);
    }
    componentWillMount(){
    }

    componentWillReceiveProps(nextProps){
    }

    selectGroup(){
        this.props.onSelectGroup(this.props.groupID)
    }

    chooseGroup(){
        this.props.onChooseGroup(this.props.groupID, 'groups')
    }

    render(){
        const buttonClass = "colorGroupButton" + ((this.props.selected) ? "" : "-NotSelected");
        const title = this.props.data.traits.find(function(el,i){return el.name === 'title'});
        return(
            <div className="colorGroupMenu">
                <Button onClick={this.selectGroup} className={"groupTitle mb-sm mr-sm btn btn-success btn-outline "+buttonClass}>
                    {title && title.value}
                </Button>
                {this.props.chosen ? <div className="demoGroupSelected"></div> : null}
                {this.props.isEdit && !this.props.exitsing &&
                    <div className="editSaveDelete">
                        <Button onClick={this.chooseGroup} className="btn btn-default thumbButton">{this.props.chosen ? 'De-select' : 'Select'}</Button>
                    </div>
                }
            </div>
        )
    }
}

PremadeGroup.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default connect(state => ({
}), Object.assign({}, componentAction))(PremadeGroup);