export const generateColumns = (number) => {
    const columns = [];
    for (let i = 0; i < number; i++) {
        columns.push({ id: i, items: [] });
    }
    return columns;
};

export const findNextOrder = (array) => {
    const ordersArray = array.map(item => item.order);
    let order = Math.max.apply(null, ordersArray) + 1;
    if (order < 0) { order = 0; }
    return order;
};

export function generateSendingItem(title, item, isImage) {
    const titleWithoutSpaces = title.replace(/\s+/g, '');
    const componentTitle = titleWithoutSpaces.substr(0, 1).toLowerCase() + titleWithoutSpaces.substr(1);
    const componentType = !item ? `${componentTitle}Component` : componentTitle;
    let newItem = item;
    if (!item) {
        newItem = isImage ? 'componentImage' : 'componentText';
    }
    return {
        component_type: componentType,
        item: newItem,
        label: title,
    };
}

export function confirmMessage(text, callback) {
    swal({
        title: 'Are you sure?',
        text: `Are you sure you want to delete this ${text}?`,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
    }, callback);
}

