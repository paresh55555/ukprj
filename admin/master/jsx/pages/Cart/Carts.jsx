import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import { cartAction } from '../../actions';
import { apiCart } from '../../api';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import LinkBox from '../../components/linkbox/LinkBox';

class Carts extends Component {
    componentWillMount() {
        const { getCarts } = this.props;
        getCarts();
    }
    render() {
        const { carts, setCurrentElement } = this.props;
        return (
            <ContentWrapper>
                <h3>Cart Builder</h3>
                <Button style={{"marginBottom":"10px"}} onClick={() => this.context.router.push('builder/cart/newcart')}>
                    Add Cart
                </Button>
                <Grid fluid>
                    <Row>
                        {carts.length > 0 ?
                            carts.map(
                                cart => (
                                    <Col md={2} sm={4} key={cart.id}>
                                        <span role="button" tabIndex={0} onClick={() => setCurrentElement(cart)}>
                                            <LinkBox
                                                onClick={this.onClick}
                                                url={`builder/cart/${cart.id}`}
                                                params={{ name: cart.name }}
                                                title={cart.name}
                                            />
                                        </span>
                                    </Col>
                                ),
                            )
                            : null
                        }
                        
                    </Row>
                </Grid>
            </ContentWrapper>
        );
    }
}

Carts.propTypes = {
    getCarts: PropTypes.func.isRequired,
    setCurrentElement: PropTypes.func.isRequired,
    carts: PropTypes.array.isRequired,
};
Carts.contextTypes = {
    router: React.PropTypes.object.isRequired,
};
export default compose(
    connect(state => ({
        carts: state.cart.carts,
    }), cartAction),
    withProps(apiCart),
)(Carts);