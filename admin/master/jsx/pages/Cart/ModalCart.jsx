import React, { PropTypes } from 'react';
import SQModal from '../../components/Common/SQModal/SQModal';
import { Modal, Button } from 'react-bootstrap';

function ModalCart(props) {
    const { show, onClose, title, children } = props;
    return (
        <Modal show={show} onHide={onClose}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                {children}
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={onClose}>Cancel</Button>
            </Modal.Footer>
        </Modal>
    );
}

ModalCart.propTypes = {

};

export default ModalCart;
