import React, { PropTypes } from 'react';
import {API_URL} from '../../constant.js';

function DrawLayoutElement(props) {
    const { currentItem, alignment } = props;
    return (
        <div
            className={currentItem.component_type}
            style={{ textAlign: alignment }}
        >
            {currentItem.item === 'componentImage' ?
                <div className="cartImage">
                    <div className="cartImageText">Image</div>
                    <div>{currentItem.label}</div>
                </div>
                : null
            }
            {currentItem.item === 'logo' ?
                <div className="cartImage">
                    <div className="cartImageText">{currentItem.label}</div>
                </div>
                : null
            }
            {currentItem.item === 'componentText' ?
                <div>
                    <div className="boldTitle">{currentItem.label}:</div>
                    <div>value</div>
                </div>
                : null
            }
            {currentItem.item === 'systemName' ?
                <div>{currentItem.label}</div>
                : null
            }
            {currentItem.item === 'imageSystem' ?
                <div className="cartImage">
                    <div className="cartImageText">{currentItem.label}</div>
                </div>
                : null
            }
            {currentItem.item === 'numberOfItems' ?
                <div>{currentItem.label}</div>
                : null
            }
            {currentItem.item === 'qedd' ?
                <div>
                    <div className="quantityBox">
                        <div className="quantityLabel">Qty </div>
                        <div className="quantityInput"></div>
                    </div>
                    <div className="qeddIcons">
                        <img className="cartIcons" src={API_URL + "/images/cart/icon-edit.svg"} />
                        <img className="cartIcons" src={API_URL + "/images/cart/icon-duplicate.svg"} />
                        <img className="cartIcons" src={API_URL + "/images/cart/icon-delete.svg"} />
                    </div>
                </div>
                : null
            }
            {currentItem.item === 'text' ?
                currentItem.component_type !== 'textBox' ? <div>{currentItem.label}</div> : null
                : null
            }
            {currentItem.item === 'textWithLabel' ?
                <div><span className="boldTitle">Label:</span><div className="withLabel">{currentItem.label}</div></div>
                : null
            }
            {currentItem.item === 'inputField' ?
                <div className="inputField">{currentItem.label}</div>
                : null
            }
            {currentItem.item === 'inputFieldWithLabel' ?
                <div><span className="boldTitle">Label:</span><div className="inputField withLabel">{currentItem.label}</div></div>
                : null
            }
            {currentItem.item === 'price' ?
                <div>{currentItem.label} $1000</div>
                : null
            }

            {currentItem.item === 'sub-total' ?
                <div>{currentItem.label}: $2000</div>
                : null
            }
            {currentItem.item === 'note' ?
                <div>
                    <div>{currentItem.label}</div>
                    <div className="textBox" />
                </div>
                : null
            }
        </div>
    );
}

DrawLayoutElement.propTypes = {
    currentItem: PropTypes.shape({
        component_type: PropTypes.string.isRequired,
        item: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
    }).isRequired,
    alignment: PropTypes.string.isRequired,
};

export default DrawLayoutElement;
