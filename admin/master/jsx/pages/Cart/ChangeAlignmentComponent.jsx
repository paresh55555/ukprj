import React, { PropTypes } from 'react';

function ChangeAlignmentComponent(props) {
    const { changeAlignment, alignment } = props;
    return (
        <div className="alignment">
            <span>Alignment: </span>
            <span>
                <input
                    onChange={() => changeAlignment('left')}
                    type="radio"
                    checked={alignment === 'left'}
                />
                Left
            </span>
            <span>
                <input
                    onChange={() => changeAlignment('center')}
                    type="radio"
                    checked={alignment === 'center'}
                />
                Center
            </span>
            <span>
                <input
                    onChange={() => changeAlignment('right')}
                    type="radio"
                    checked={alignment === 'right'}
                />
                Right
            </span>
        </div>
    );
}

ChangeAlignmentComponent.propTypes = {
    changeAlignment: PropTypes.func.isRequired,
    alignment: PropTypes.string.isRequired,
};

export default ChangeAlignmentComponent;
