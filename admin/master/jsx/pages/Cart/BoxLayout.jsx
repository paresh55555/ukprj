import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps, withState } from 'recompose';
import { Col } from 'react-bootstrap';
import { cartAction } from '../../actions';
import DrawLayoutElement from './DrawLayoutElement';
import ChangeAlignmentComponent from './ChangeAlignmentComponent';

class BoxLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            alignment: props.column.alignment,
        };
        this.openEdit = this.openEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.changeAlignment = this.changeAlignment.bind(this);
        this.onSaveColumn = this.onSaveColumn.bind(this);
        this.saveItems = this.saveItems.bind(this);
    }
    onSaveColumn() {
        const { cartId, layoutId, id, updateItemsRequest, column } = this.props;
        if (column.alignment !== this.state.alignment) {
            const data = { alignment: this.state.alignment, order: column.order };
            updateItemsRequest(cartId, layoutId, id, data);
        }
        this.saveItems();
        this.cancelEdit(true);
    }
    changeAlignment(alignment) {
        this.setState({ alignment });
    }
    openEdit() {
        const { changeSingleEditMode, changeColumnEdit, setCurrentColumn, layoutId, id } = this.props;
        setCurrentColumn({ layoutId, id });
        changeColumnEdit(true);
        changeSingleEditMode();
    }
    cancelEdit(isSave) {
        const { changeSingleEditMode, changeColumnEdit, backToPreviousItems, layoutId, id } = this.props;
        if (!isSave) {
            backToPreviousItems(layoutId, id);
        }
        changeColumnEdit(false);
        changeSingleEditMode();
    }
    saveItems() {
        const { cartId, layoutId, id, column, saveItemsRequest, addSavedItemsAction } = this.props;
        const items = column.items.entries();
        const newItems = [];
        const iterateItems = () => {
            const item = items.next();
            if (item.done) {
                addSavedItemsAction(layoutId, id, newItems);
                return;
            }
            const sendingData = item.value[1];
            if (!sendingData.id) {
                saveItemsRequest(cartId, layoutId, id, sendingData).then((res) => {
                    newItems.push(res);
                    iterateItems();
                });
            } else {
                iterateItems();
            }
        };
        iterateItems();
    }
    render() {
        const { isEdit, isSingleEdit, isColumnEdit, col, column, openModal, isSaving } = this.props;
        const { alignment } = this.state;
        const isHorizontalLine = column.items && column.items.length > 0 && column.items[0].component_type === 'horizontalLine' ? column.items[0].component_type : null;
        const marginBottom = { marginBottom: !isEdit && !isSingleEdit ? 70 : 10 };
        return (
                isHorizontalLine ?
                    <Col md={col}>
                        <div className="horizontalLine" style={marginBottom} />
                    </Col>
                    : <Col md={col}>
                        <div className="optionsBox optionsBoxContent columnContainer">
                            {column.items && column.items.map((item, i) => (
                                <DrawLayoutElement
                                    key={i}
                                    currentItem={item}
                                    alignment={alignment}
                                />),
                            )}
                        </div>
                        {isEdit ?
                            null
                            : !isSingleEdit ?
                                <div className="editSave">
                                    <button onClick={this.openEdit} type="button" className="btn btn-default" disabled={isSaving}>Edit</button>
                                </div>
                                : !isColumnEdit ?
                                    null
                                    : <div className="editSave">
                                        <button onClick={this.onSaveColumn} type="button" className="btn btn-default">Save</button>
                                        <button onClick={() => this.cancelEdit()} type="button" className="btn btn-default">Cancel</button>
                                        <button onClick={openModal} type="button" className="btn btn-default">Add</button>
                                        <ChangeAlignmentComponent
                                            changeAlignment={this.changeAlignment}
                                            alignment={alignment}
                                        />
                                    </div>
                        }
                    </Col>
        );
    }
}

BoxLayout.propTypes = {
    isEdit: PropTypes.bool.isRequired,
    isSingleEdit: PropTypes.bool.isRequired,
    isColumnEdit: PropTypes.bool.isRequired,
    col: PropTypes.number.isRequired,
    cartId: PropTypes.number.isRequired,
    layoutId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    column: PropTypes.object.isRequired,
    changeSingleEditMode: PropTypes.func.isRequired,
    openModal: PropTypes.func.isRequired,
    changeColumnEdit: PropTypes.func.isRequired,
    backToPreviousItems: PropTypes.func.isRequired,
    addSavedItemsAction: PropTypes.func.isRequired,
    updateItemsRequest: PropTypes.func.isRequired,
    saveItemsRequest: PropTypes.func.isRequired,
    setCurrentColumn: PropTypes.func.isRequired,
    isSaving: PropTypes.bool.isRequired,
};

export default compose(
    connect(() => ({
    }), cartAction),
    withState('isColumnEdit', 'changeColumnEdit', false),
)(BoxLayout);
