import React, { PropTypes } from 'react';

function LayoutNameEdit(props) {
    const { name, changeName, fieldName, placeholder } = props;
    return (
        <div className="mainBox">
            <div className="form-group">
                <label htmlFor="cartName" style={{textTransform:'capitalize'}}>{fieldName} Name:</label>
                <input onChange={changeName} className="form-control" value={name} placeholder={placeholder || 'Cart Name'} type="text" />
            </div>
        </div>
    );
}

LayoutNameEdit.propTypes = {
    name: PropTypes.string.isRequired,
    fieldName: PropTypes.string.isRequired,
    changeName: PropTypes.func.isRequired,
};

export default LayoutNameEdit;
