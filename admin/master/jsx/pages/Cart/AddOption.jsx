import React, { PropTypes } from 'react';
import { Button } from 'react-bootstrap';

function AddOption(props) {
    const { isImage, addItem } = props;
    return (
        <div>
            {isImage ? <div className="cartImage"><div className="cartImageText">Image</div></div> : null}
            <div className="form-group">
                <div className="control-label">Size & Panels</div>
                <Button onClick={() => addItem('Frame')} className="btn-primary">Choose Frame</Button>
                <Button onClick={() => addItem('Track')} className="btn-primary">Choose Track</Button>
                <Button onClick={() => addItem('Number Of Panels')} className="btn-primary">Choose Number of Panels</Button>
                <Button onClick={() => addItem('Swing Door')} className="btn-primary">Choose Swing Door Option & Panels</Button>
                <Button onClick={() => addItem('Panel Movement')} className="btn-primary">Panel Movement</Button>
                <Button onClick={() => addItem('Swing Direction')} className="btn-primary">Swing Direction</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Color / Finish</div>
                <Button onClick={() => addItem('Exterior Color')} className="btn-primary">Choose Exterior Color and Finish</Button>
                <Button onClick={() => addItem('Interior Color')} className="btn-primary">Choose Interior Color and Finish</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Glass</div>
                <Button onClick={() => addItem('Glass')} className="btn-primary">Choose Glass Type</Button>
                <Button onClick={() => addItem('Glass Options')} className="btn-primary">Choose Glass Options</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Hardware</div>
                <Button onClick={() => addItem('Hardware')} className="btn-primary">Choose Hardware</Button>
                <Button onClick={() => addItem('Hardware Options')} className="btn-primary">Choose Hardware Options</Button>
            </div>
        </div>
    );
}

AddOption.propTypes = {
    isImage: PropTypes.bool.isRequired,
    addItem: PropTypes.func.isRequired,
};

export default AddOption;
