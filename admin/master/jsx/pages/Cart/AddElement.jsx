import React, { PropTypes } from 'react';
import { Button } from 'react-bootstrap';

function AddElement(props) {
    const { addItem, changeOptionModal } = props;
    return (
        <div>
            <div className="form-group">
                <div className="control-label">System Options:</div>
                <Button onClick={() => changeOptionModal(true)} className="btn-primary">Option With Image</Button>
                <Button onClick={() => changeOptionModal(false)} className="btn-primary">Option With Text</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Cart:</div>
                <Button onClick={() => addItem('System Name', 'systemName')} className="btn-primary">System Name</Button>
                <Button onClick={() => addItem('Image System', 'imageSystem')} className="btn-primary">System Image</Button>
                <Button onClick={() => addItem('Number Of Items', 'numberOfItems')} className="btn-primary">Number of Items</Button>
                <Button onClick={() => addItem('Quantity, Edit, Duplicate, Delete', 'qedd')} className="btn-primary">Quantity, Edit, Duplicate, Delete</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Text:</div>
                <Button onClick={() => addItem('Large Title', 'text')} className="btn-primary">Large Title</Button>
                <Button onClick={() => addItem('Bold Title', 'text')} className="btn-primary">Bold Title</Button>
                <Button onClick={() => addItem('Simple Text', 'text')} className="btn-primary">Text</Button>
                <Button onClick={() => addItem('Small Text', 'text')} className="btn-primary">Small Text</Button>
                <Button onClick={() => addItem('Text Box', 'text')} className="btn-primary">Text Box</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Notes:</div>
                <Button onClick={() => addItem('Notes', 'note')} className="btn-primary">Customer</Button>
                <Button onClick={() => addItem('Production Notes', 'note')} className="btn-primary">Production</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Price:</div>
                <Button onClick={() => addItem('Price', 'price')} className="btn-primary">Price</Button>
                <Button onClick={() => addItem('Small Price', 'price')} className="btn-primary">Small Price</Button>
                <Button onClick={() => addItem('Sub Total', 'sub-total')} className="btn-primary">Sub Total</Button>
                <Button onClick={() => addItem('Small Sub Total', 'sub-total')} className="btn-primary">Small Sub Total</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Spacing:</div>
                <Button onClick={() => addItem('Large Space', 'spacing')} className="btn-primary">Large Space</Button>
                <Button onClick={() => addItem('Small Space', 'spacing')} className="btn-primary">Small Space</Button>
                <Button onClick={() => addItem('Horizontal Line', 'spacing')} className="btn-primary">Horizontal Line</Button>
            </div>
        </div>
    );
}

AddElement.propTypes = {
    addItem: PropTypes.func.isRequired,
    changeOptionModal: PropTypes.func.isRequired,
};

export default AddElement;
