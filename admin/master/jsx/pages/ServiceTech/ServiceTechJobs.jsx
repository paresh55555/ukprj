import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import Jobs from '../../components/ServiceTech/Jobs';



class ServiceTechJobs extends Component {

   
    render() {

      
        return (
            <div>
                <Jobs />
            </div>
        );
    }
}

ServiceTechJobs.propTypes = {

};

ServiceTechJobs.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
   
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTechJobs)