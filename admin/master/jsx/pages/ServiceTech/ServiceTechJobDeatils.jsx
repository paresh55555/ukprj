import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import {Nav, NavItem, Glyphicon} from "react-bootstrap";
import JobsDetails from '../../components/ServiceTech/JobsDetails';
import { ApiServiceJob } from '../../api';
import { modifyCustomer } from '../../components/Common/utilFunctions';
import { setJobsLoading } from '../../actions/serviceJob';
import Loader from '../../components/loader/Loader';




class ServiceTechJobDetails extends Component {

    state = {
        currentJob : {}
    }

    gotoJobList = () => {
        this.context.router.push('serviceTechjobs');
    }

    componentWillReceiveProps (nextProps) {

        const nextJob = (nextProps.serviceJob && nextProps.serviceJob.currentJob) || {}
        const { currentJob } = this.props.serviceJob

        if((nextJob && nextJob.id) !== (currentJob && currentJob.id)) {
            this.setState({
                currentJob:nextJob
            })
        }
    }

    componentDidMount () {
        const { currentJob } = this.props.serviceJob
        this.setState({
            currentJob
        })
    }

    componentWillMount () {
        this.getJob()
    }

    getJob = () => {
        let {JobId} = this.props.params;
        this.props.dispatch(setJobsLoading(true))

        ApiServiceJob.ApiGetJobs(JobId).then(res =>{
            this.props.dispatch(setJobsLoading(false))
        });
    }

    renderNav(){
       return <div className={'jobheader'}>
                <Nav bsStyle="pills" >
                    <NavItem className='text-center DetailNav'>
                        <Glyphicon glyph="glyphicon glyphicon-chevron-left" className='pull-left back' onClick={this.gotoJobList}/> 
                        Job Detail
                    </NavItem>  
                </Nav>
            </div>
    }

    renderDetails(currentJobDetail){
        const { isLoading } = this.props.serviceJob

        if(isLoading){
            return (<div className='techLoader'><Loader /></div>)
        }
        else{
             return (<JobsDetails data={currentJobDetail}/>)
        }
    }

    render() {

        const { currentJob } = this.state

        const currentJobDetail = modifyCustomer(currentJob || {})

        return (
            <div className='techDetails'>
                {this.renderNav()}
                {this.renderDetails(currentJobDetail || {})}
            </div>
        );
    }
}

ServiceTechJobDetails.propTypes = {

};

ServiceTechJobDetails.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => state

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceTechJobDetails)