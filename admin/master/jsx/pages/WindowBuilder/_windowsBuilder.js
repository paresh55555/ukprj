'use strict'

var sash = {"marginHeight": 5, "marginWidth": 5};
var frame = {"width": 700, "height": 400, "marginHeight": 10, "marginWidth": 10, "border": 1};
var xMult = 1;
var yMult = 1;
var border = 1;

/*
* New realization
* */

export function updateWindow(config){
    $('#windowBuilder').html('');
    var numberOfRow = config.rows.length;
    var size = {
        id: '#mainFrame',
        height: frame.height-10,
        width: frame.width-10
    }
    windowsHolder();
    buildRows(config.rows, size)
}

function buildRows(rows, size){
    var height = Math.round(size.height * yMult / rows.length);
    rows.forEach(function (row, key) {
        var width = Math.floor(size.width * xMult / row.windows.length);
        var parentInfo = {
            id: size.id,
            height: height,
            width: width,
            numOfRows: rows.length
        }
        buildRowWindows(row.windows, key, parentInfo)
    });
}

function buildRowWindows(windows, rownum, parentInfo){
    windows.forEach((window, key)=>{
        var frameHeightToRemove = Math.floor(( (frame.marginHeight * parentInfo.numOfRows) + (2 * border * parentInfo.numOfRows) ) / parentInfo.numOfRows);
        var frameWidthToRemove = Math.floor(( (frame.marginWidth * windows.length) + (2 * border * windows.length) ) / windows.length);
        var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);
        var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

        var parentPrefix = (parentInfo.id == '#mainFrame' ? '' : parentInfo.id + '_')
        var cid = 'r-'+rownum+'-w-'+key;
        let innerCid = (parentPrefix + cid).replace('#','');
        if(typeof window == 'object'){

            jQuery('<div/>', {
                id: innerCid,
                class: 'sash-container',
                width: (parentInfo.width / xMult),
                height: (parentInfo.height / yMult) - 2
            }).appendTo(parentInfo.id);
            buildRows(window.rows, {
                id: '#' + innerCid,
                height: parentInfo.height,
                width: parentInfo.width
            })
        }else{
            while(window > 0){
                jQuery('<div/>', {
                    class: 'frame',
                    width: (parentInfo.width / xMult) - frameWidthToRemove,
                    height: (parentInfo.height / yMult) - frameHeightToRemove
                }).append($('<div>', {
                    id: innerCid,
                    class: 'sash',
                    width: ( parentInfo.width / xMult) - sashWidthToRemove,
                    height: ( parentInfo.height / yMult) - sashHeightToRemove
                })).appendTo(parentInfo.id);
                window--;
            }
        }
    })
}

/*
* Old realization
* */

// Size and sash

'use strict';
var rows;
var columns;

function addWidthsToRow(row) {

    for (var a = 1; a < row.numberOfWindows; a++) {
        row['width' + a] = Math.round(frame.width * xMult / row.numberOfWindows)
    }

    return row;
}

function isEmptyObject(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

export function renderWindowPassedType(type) {

    columns = '';
    rows = '';

    var res = type.split("-");
    var direction = res.shift();

    if (direction == 'V') {

        if (isEmptyObject(rows)) {
            rows = buildVerticalData(res);
        }
        buildVerticalWindows(rows);

    } else {
        if (isEmptyObject(columns)) {
            columns = buildHorizontalData(res);
        }
        buildHorizontalWindows(columns);
    }
}

// Builder

function getWidthOfWindows(windows) {

    var width = 0
    for (var a = 0; a < windows.length; a++) {
        width = windows[a].width + width;
    }
    return width;
}


function getHeightOfWindows(windows) {

    var height = 0
    for (var a = 0; a < windows.length; a++) {
        height = windows[a].height + height;
    }
    return height;
}


function remainingHeight(windows) {

    var heigthOfWindows = getHeightOfWindows(windows);

    var remainingSpace = (frame.height * yMult) - heigthOfWindows;
    return remainingSpace;
}

function buildHolders() {
    windowsHolder();
}

function windowsHolder() {

    jQuery('<div/>', {
        id: 'mainFrame',
        width: frame.width,
        height: frame.height
    }).appendTo('#windowBuilder');
}

// Vertical

function buildVerticalWindows(rowsPassed) {

    $('#windowBuilder').html('');

    buildHolders();
    buildVertical(rows);
}

export function buildVerticalData(res) {

    var numberOfRow = res.length;
    var height = Math.round(frame.height * yMult / numberOfRow);

    var rows = [];
    for (var a = 0; a < numberOfRow; a++) {

        var row = {"numberOfWindows": res[a], "height": height, "number": a};
        row = addWidthsToRow(row);
        rows.push(row);
    }

    return (rows);
}


function buildVertical(rows) {

    var numberOfRows = parseInt(rows.length);

    $('#mainFrame').html('');

    var windows = [];
    for (var a = 0; a < numberOfRows - 1; a++) {
        var row = rows[a];
        row.number = a + 1;
        windows.push({"height": row.height});
        buildRow(row, numberOfRows);
    }

    var height = remainingHeight(windows);

    var lastRow = rows[numberOfRows - 1];
    lastRow.number = numberOfRows;
    lastRow.height = height;

    buildRow(lastRow, numberOfRows);
}


function remainingWidthVertical(windows) {

    var widthOfWindows = getWidthOfWindows(windows);
    var remainingSpace = (frame.width * xMult) - (widthOfWindows);

    return remainingSpace;
}

function buildRow(row, numberOfRows) {

    var mySize = {
        "height": row.height,
        "place": '#mainFrame',
        "number": row.number,
        "numberOfWindows": row.numberOfWindows
    }

    var windows = [];
    for (var a = 1; a < row.numberOfWindows; a++) {

        var newWidth = row['width' + a]
        mySize.width = newWidth;
        mySize.column = a;
        windows.push({"width": mySize.width});
        addWindowVertical(mySize, numberOfRows);

    }

    mySize.column = row.numberOfWindows;
    mySize.width = remainingWidthVertical(windows);
    addWindowVertical(mySize, numberOfRows);

}


function addWindowVertical(size, numberOfRows) {

    var rowsMultiplier = parseFloat(numberOfRows);
    var frameHeightToRemove = ( (frame.marginHeight * rowsMultiplier) + (2 * border * rowsMultiplier) ) / numberOfRows;
    if (size.number == 1 || size.number == numberOfRows) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }

    var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

    var windowMultiplier = parseFloat(size.numberOfWindows);

    var frameWidthToRemove = ( (frame.marginWidth * windowMultiplier) + (2 * border * windowMultiplier) ) / size.numberOfWindows;

    if (size.numberOfWindows == 1) {
        frameWidthToRemove = frameWidthToRemove + 12;
    } else if (size.column == 1 || size.column == size.numberOfWindows) {
        frameWidthToRemove = frameWidthToRemove + 6;
    }

    var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);

    jQuery('<div/>', {
        class: 'frame',
        width: ( size.width / xMult) - frameWidthToRemove,
        height: (size.height / yMult) - frameHeightToRemove
    }).append($('<div>', {
        id: 'window-' + size.number + "-" + size.column,
        class: 'sash',
        width: (size.width / xMult) - sashWidthToRemove,
        height: (size.height / yMult) - sashHeightToRemove
    })).appendTo(size.place);

}

// Horizontal (currently not used)

function buildHorizontalWindows(columnsPassed) {

    columns = columnsPassed;
    $('#windowBuilder').html('');

    buildHolders();
    buildHorizontal(columns);
}

export function buildHorizontalData(res) {

    var numberOfColumns = res.length;
    var width = Math.round(frame.width * xMult / numberOfColumns);

    var columns = [];
    for (var a = 0; a < numberOfColumns; a++) {

        var column = {"numberOfWindows": res[a], "width": width, "number": a};
        column = addHeightToColumn(column);
        columns.push(column);
    }

    return (columns);
}

function addHeightToColumn(column) {

    for (var a = 1; a < column.numberOfWindows; a++) {
        column['height' + a] = Math.round(frame.height * yMult / column.numberOfWindows)
    }

    return column;
}

function remainingWidthForColumns(windows) {

    var widthOfWindows = getWidthOfWindows(windows);
    var remainingSpace = (frame.width * xMult) - widthOfWindows;

    return remainingSpace;
}


function buildHorizontal(columns) {

    var windows = [];

    for (var a = 0; a < columns.length - 1; a++) {
        var column = columns[a];
        windows.push({"width": column.width});
        column.columnNumber = parseInt(a) + 1;
        buildColumn(column, columns.length);
    }

    var width = remainingWidthForColumns(windows);
    var lastColumn = columns[columns.length - 1];
    lastColumn.columnNumber = columns.length;
    lastColumn.width = width;

    buildColumn(lastColumn, columns.length);
}


function buildColumn(column, numberOfColumns) {

    var frameWidthToRemove = widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, column.columnNumber);

    jQuery('<div/>', {
        id: 'column' + column.number,
        class: 'holder',
        width: parseInt(column.width / xMult) - parseInt(frameWidthToRemove) + 12,
        height: frame.height
    }).appendTo('#mainFrame');

    var size = {
        "columnNumber": column.columnNumber,
        "width": column.width,
        "place": '#column' + column.number,
        "column": column.number,
        "numberOfWindows": parseInt(column.numberOfWindows)
    };

    var windows = [];
    for (var a = 1; a < column.numberOfWindows; a++) {

        var height = column['height' + a];

        windows.push({"height": height});
        size.number = parseInt(a);
        size.height = height;
        size.column = a;
        addWindowHorizontal(size, numberOfColumns);
    }

    size.column = column.numberOfWindows;
    size.number = parseInt(column.numberOfWindows);
    size.height = remainingHeight(windows);
    size.numberOfWindows = parseInt(column.numberOfWindows);
    size.row = column.number;

    addWindowHorizontal(size, numberOfColumns);
}


function widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, columnNumber) {

    var numberOfMargins = parseInt(numberOfColumns) + 1;

    var frameWidthToRemove = parseInt(frame.marginWidth) + parseInt(2 * border) - 1;
    if (columnNumber == 1) {
        frameWidthToRemove = ((frame.marginWidth * numberOfMargins ) / numberOfColumns ) + (border * 2 * numberOfColumns) - 1;
    }
    if (columnNumber == numberOfColumns) {
        frameWidthToRemove = ((frame.marginWidth * numberOfMargins ) / numberOfColumns ) + (border * 2 * numberOfColumns) - 2;
    }

    return frameWidthToRemove;
}


function addWindowHorizontal(size, numberOfColumns) {

    var numberOfRows = parseInt(size.numberOfWindows);

    var rowsMultiplier = parseInt(numberOfRows);
    var frameHeightToRemove = ( (frame.marginHeight * rowsMultiplier) + (2 * border * rowsMultiplier) ) / numberOfRows;

    if (size.number == 1) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }
    if (size.number == numberOfRows) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }

    var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

    var frameWidthToRemove = widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, size.columnNumber);
    var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);

    jQuery('<div/>', {
        class: 'frame',
        width: parseInt(size.width / xMult) - parseInt(frameWidthToRemove),
        height: parseInt(size.height / yMult) - frameHeightToRemove
    }).append($('<div>', {
        id: 'window-' + size.row + "-" + size.column,
        class: 'sash',
        width: (size.width / xMult) - sashWidthToRemove,
        height: (size.height / yMult) - sashHeightToRemove
    })).appendTo(size.place);
}