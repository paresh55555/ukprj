import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import * as builder from './_windowsBuilder';
import { Button } from 'react-bootstrap';
import {NotifyAlert} from '../../components/Common/notify.js'

class WindowBuilder extends React.Component {
    constructor() {
        super();
        this.state = {
            windowConfig:{rows:[{windows:[1]}]},
            isEdit: false,
            combine: false,
            allowCombine: true
        }
        this.combine = this.combine.bind(this)
        this.addWindow = this.addWindow.bind(this)
        this.selectSash = this.selectSash.bind(this)
    }

    getDeepestElements(parentWindow, data, type){
        parentWindow = parentWindow || this.state.windowConfig;
        var window = parentWindow.rows[data[1]].windows[data[3]]
        if(typeof window == 'object'){
            return window;
        }else{
            return type == 'windows' ? parentWindow.rows[data[1]].windows : parentWindow.rows;
        }
    }

    getElementByPath(path, type){
        var chunks = path.split('_'),
            element = this.state.windowConfig,
            getElement = function(parentWindow, data, last, type){
                return last ?
                    type === 'windows' ? parentWindow.rows[data[1]].windows : parentWindow.rows
                    :
                    parentWindow.rows[data[1]].windows[data[3]];
            };
        chunks.forEach((sash, key)=>{
            let data = sash.split("-")
            element = getElement(element, data, ((key + 1) === chunks.length), type)
        })
        return element;
    }

    split(splitType){
        var selectedWindow = $('.selectedWindow'),
            id = selectedWindow.attr('id'),
            self = this;

        if(!id)return;
        var sashes = id.split('_');
        if(sashes.length > 1){
            var deepestWindows = this.state.windowConfig,
                deepestRows = this.state.windowConfig,
                latestData = null;
            sashes.forEach((sash, key)=>{
                let data = sash.split("-")
                latestData = data;
                deepestWindows = self.getDeepestElements(deepestWindows, data, 'windows')
                deepestRows = self.getDeepestElements(deepestRows, data, 'rows')
            })
            if(splitType === 'V') {
                deepestWindows.splice(latestData[3], 0, 1)
            }else{
                deepestRows[latestData[1]].windows.length > 1 ?
                    deepestRows[latestData[1]].windows[latestData[3]] = {rows:[{windows:[1]},{windows:[1]}]}
                    :
                    deepestRows.push({'windows':[1]})
            }
        }else{
            let data = sashes[0].split("-")
            if(splitType === 'V') {
                this.state.windowConfig.rows[data[1]].windows.splice(data[3], 0, 1)
            }else{
                this.state.windowConfig.rows[data[1]].windows.length > 1 ?
                    this.state.windowConfig.rows[data[1]].windows[data[3]] = {rows:[{windows:[1]},{windows:[1]}]}
                    :
                    this.state.windowConfig.rows.push({'windows':[1]})
            }
        }
        this.updateWindow();
    }

    createNewType(data){
        var newType = data.windowMode + '';
        for (var a = 0; a < data.rows.length; a++) {
            newType = newType + '-' + data.rows[a].numberOfWindows;
        }
        this.updateWindow(newType);
    }

    combine(){
        var self = this,
            selectedWindows = $('.selectedWindow'),
            id1 = $(selectedWindows[0]).attr('id'),
            id2 = $(selectedWindows[1]).attr('id'),
            sashes1 = id1.split('_'),
            sashes2 = id2.split('_');

        if(sashes1.length === 1 && sashes2.length === 1){
            var sash1 = sashes1[0].split('-'),
                sash2 = sashes2[0].split('-');
            if(sash1[1] === sash2[1]){ // If the same row
                this.state.windowConfig.rows[sash1[1]].windows.splice(sash2[3], 1)
            }else{
                this.state.windowConfig.rows.splice(sash2[1], 1)
            }
        }else{
            var sash1 = sashes1[0].split('-'),
                sash2 = sashes2[0].split('-');
            if(sash1[1] === sash2[1]){
                var deepestWindows1 = this.state.windowConfig,
                    deepestRows1 = this.state.windowConfig,
                    latestData1 = null;
                sashes1.forEach((sash, key)=>{
                    let data = sash.split("-")
                    latestData1 = data;
                    deepestWindows1 = self.getDeepestElements(deepestWindows1, data, 'windows')
                    deepestRows1 = self.getDeepestElements(deepestRows1, data, 'rows')
                })
                var deepestWindows2 = this.state.windowConfig,
                    deepestRows2 = this.state.windowConfig,
                    latestData2 = null;
                sashes2.forEach((sash, key)=>{
                    let data = sash.split("-")
                    latestData2 = data;
                    deepestWindows2 = self.getDeepestElements(deepestWindows2, data, 'windows')
                    deepestRows2 = self.getDeepestElements(deepestRows2, data, 'rows')
                })

                // Combine
                if(latestData1[1] === latestData2[1]){ // If the same row
                    deepestWindows1.splice(latestData2[3], 1) // Remove second window
                }else{
                    if(deepestRows1.length > 2){
                        deepestRows1.splice(latestData2[1], 1)
                    }else{
                        var a = this.state.windowConfig;
                        sashes1.pop()
                        if(sashes1.length === 1){
                            let data = sashes1[0].split("-")
                            this.state.windowConfig.rows[data[1]].windows[data[3]] = 1;
                        }else{
                            var windows = this.getElementByPath(sashes1.join('_'), 'windows')
                            var sashData = sashes1[sashes1.length-1].split("-")
                            windows[sashData[3]] = 1
                        }
                    }
                }
            }else{
                NotifyAlert('Please select pieces from the same sash')
            }
        }
        this.updateWindow()
    }

    updateWindow(){
        this.setState({combine: false, windowConfig: this.state.windowConfig})
        console.log('NEW CONFIG: ',this.state.windowConfig)
        builder.updateWindow(this.state.windowConfig)
        $('.sash').off("click").on("click", this.selectSash);
    }

    addWindow(){
        this.setState({isEdit: true});
        this.updateWindow()
    }

    allowCombine(){
        var selectedWindows = $('.selectedWindow'),
            id1 = $(selectedWindows[0]).attr('id'),
            id2 = $(selectedWindows[1]).attr('id'),
            sashes1 = id1.split('_'),
            sashes2 = id2.split('_');
        if(selectedWindows[0].offsetWidth !== selectedWindows[1].offsetWidth || selectedWindows[0].offsetHeight !== selectedWindows[1].offsetHeight){
            return false;
        }
        if(sashes1.length === 1 && sashes2.length === 1){
            var sash1 = sashes1[0].split('-'),
                sash2 = sashes2[0].split('-');
            if(sash1[1] === sash2[1]){
                return true;
            }
        }else{
            return sashes1[0] == sashes2[0];
        }
        return true;
    }

    selectSash(e){
        if($('.selectedWindow').length === 2 && !$(e.currentTarget).hasClass('selectedWindow'))return false;
        this.setState({combine: false});
        $(e.currentTarget).toggleClass('selectedWindow');
        var selectedWindows = $('.selectedWindow')
        if(selectedWindows.length === 2) {
            this.setState({combine: true, allowCombine: this.allowCombine()})
        }
    }

    render() {
        return (
            <ContentWrapper>
                <div className="panel-default panel mainBox">
                    <div className="panel-body">
                        <div className="bg-info-dark defaultWidth">
                            <span className="optionTitle">Window Builder</span>
                        </div>
                        { this.state.isEdit ?
                            <div>
                                { this.state.combine ?
                                    <div className="editSave">
                                        {this.state.allowCombine ? <Button onClick={this.combine} className="btn btn-default">Combine</Button> : ''}
                                    </div>
                                    :
                                    <div className="editSave">
                                        <Button onClick={this.split.bind(this,'H')} className="btn btn-default">Split horizontal</Button>
                                        <Button onClick={this.split.bind(this,'V')} className="btn btn-default btn-outline ">Split vertical</Button>
                                    </div>
                                }
                            </div>
                            :
                            <div className="editSave">
                                <Button onClick={this.addWindow} className="btn btn-default">Add window</Button>
                            </div>
                        }
                        <div id="windowBuilder"></div>
                    </div>
                </div>
            </ContentWrapper>
        );
    }

}

export default connect(state => ({
}), Object.assign({}))(WindowBuilder);
