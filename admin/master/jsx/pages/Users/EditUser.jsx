import React, {Component, PropTypes} from 'react';
import {connect } from 'react-redux';
import { compose, withProps } from 'recompose'
import {Router, Route, browserHistory} from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import {checkDuplicate} from './userService.js';
import { Grid, Row, Col, Panel, Button, FormControl, DropdownButton, MenuItem, Modal } from 'react-bootstrap';
import * as boot from 'react-bootstrap';
import * as constant from '../../constant.js'
import {NotifyAlert} from '../../components/Common/notify.js'
import DeleteDialog from '../../components/Common/DeleteDialog'
import {userAction, companyAction, permissionAction} from '../../actions';
import { ApiCompany, ApiUsers } from '../../api';

import swal from 'sweetalert'
let form = ''
var self = '';
var handle = 0;

class EditUser extends React.Component {
    constructor() {
        super();
        self = this;
        this.state = {
            editemail: false,
            showLoader: true,
            user: {},
            title: 'Edit User',
            disableDiscount: true,
            showDelete: true,
            edited: false,
            showDeleteModal: false,
            dublicateError: true,
            email_state: '',
            checking: false
        };
        this._handleChange = this._handleChange.bind(this);
        this._saveUser = this._saveUser.bind(this);
        this.routerWillLeave = this.routerWillLeave.bind(this);
        this.showDelete = this.showDelete.bind(this);
        this.closeDelete = this.closeDelete.bind(this);
        this.checkDuplicate = this.checkDuplicate.bind(this);
        this.keyHandle = this.keyHandle.bind(this);
        this._backtoUser = this._backtoUser.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.user.detail != this.props.user.detail) {
            var user = nextProps.user.detail
            user['anytimeQuoteEdits'] = user['anytimeQuoteEdits'] == 1 ? true : false;
            this.setState({
                user: nextProps.user.detail,
                disableDiscount: user.salesAdmin == 'on' ? true : false,
                dublicateError: false
            });
        }
        if (nextProps.user.isSaved != this.props.user.isSaved && nextProps.user.isSaved) {
            if (this.props.userId === 'new') {
                NotifyAlert('User added successfully');
            }
            else {
                NotifyAlert('User updated successfully');
            }
            this._backtoUser();
        }
        if (nextProps.user.isDeleted != this.props.user.isDeleted && nextProps.user.isDeleted) {
            swal("Deleted!",
                this.state.user.name + " has been deleted.",
                "success");
            self.setState({edited: false})
            this._backtoUser();
        }
     }
     componentWillMount() {
        this.props.apiGetCompaniesByOrder();
        this.props.getPermissionsList();
        var self = this;
        const userId = this.props.userId;
        if (userId === 'new') {
            const rawUser = {
                name: '',
                email: '',
                password: '',
                type: 'salesPerson',
                discount: '',
                companyName: '',
                prefix: '',
                salesAdmin: 'off',
                anytimeQuoteEdits: '0',
            };
            setTimeout(function () {
                self.props.receiveUserData(rawUser)
            })
            self.setState({title: 'Add User'})
        }
        else {
            self.setState({title: 'Edit User'});
            this.props.apiGetUserData(userId);
        }
    }
    componentDidMount() {
        const {route} = this.props;
        const {router} = this.context;
        // router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
        // return false to prevent a transition w/o prompting the user,
        // or return a string to allow the user to decide:
        if (this.state.edited)
            return 'Your work is not saved! Are you sure you want to leave?'
    }

    keyHandle() {
        clearTimeout(handle);
        if (this.state.user.email.length <= 2) {
            this.setState({editemail: false})

        }
        handle = setTimeout(self.checkDuplicate, 1000);
    }

    _handleChange(value, e) {
        var user = this.state.user;
        user[value] = e.target.value;
        this.setState({user: user, edited: true})
        if (value == 'email' && e.target.value.length > 2) {
            this.setState({editemail: true})
        }
    }

    _handleCheckChange(value, e) {
        var user = this.state.user;
        user[value] = this.refs[value].checked;
        if (value != 'anytimeQuoteEdits') {
            user['discount'] = 0;
            this.setState({user: user, disableDiscount: user.salesAdmin})
        }
        this.setState({user: user})
    }

    //check duplicatate code 
    checkDuplicate() {
        this.setState({checking: true})
        if (this.state.user.email.length <= 2) {
            this.setState({checking: false, editemail: false})
            return;
        }
        if (this.state.user.email != this.state.email_state && this.state.user.email.length > 2) {
            checkDuplicate(this.state.user.email).then(function (result) {
                self.setState({dublicateError: !result, checking: false})
            })
        }
        else {
            self.setState({dublicateError: true, checking: false, editemail: false})
        }
    }

    _saveUser(e) {
        e.preventDefault();
        var user = this.state.user;

        user.salesAdmin = user.salesAdmin && user.salesAdmin != 'off' ? 'on' : 'off';
        user.anytimeQuoteEdits = user.anytimeQuoteEdits ? 1 : 0;
        if ($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0}) && !this.state.dublicateError && !this.state.checking) {
            this.setState({edited: false})
            if (this.props.userId === 'new') {
                this.props.apiSaveUser(user);
            }
            else {
                delete user.status;
                this.props.apiUpdateUser(user);
            }
        }

    }

    showDelete() {
        this.setState({edited: false})
        const {user} = this.state;
        var self = this;
        DeleteDialog({
            text: "Are you sure you want to delete this user? ",
            onConfirm: function () {
                self.props.apiDeleteUser(user.id);
            }
        })
    }

    closeDelete() {

    }

    _backtoUser() {
        self.props.onClose();
    }

    render() {
        let EmailClass = this.state.dublicateError ? 'form-control' : this.state.editemail ? 'form-control has-success' : 'form-control';
        const salesAdminType = {on: true, off: false};
        const anytimeQuoteEdits = {'1': true, '0': false};
        const innerIcon = <em className="fa fa-check"></em>;
        const innerButton = <Button>Before</Button>;
        const innerDropdown = (
            <DropdownButton title="Action" id="input-dropdown-addon">
                <MenuItem key="1">Item</MenuItem>
            </DropdownButton>
        );
        const innerRadio = <input type="radio" aria-label="..."/>;
        const innerCheckbox = <input type="checkbox" aria-label="..."/>;
        return (
            <ContentWrapper>
                <h3>{this.state.title} </h3>
                { /* START row */ }
                <Row>
                    <Col sm={ 12 }>
                        { /* START panel */ }
                        <Panel header="">
                            {this.props.user.isLoading ? <div className="ball-pulse">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div> :
                                <form className="form-vertical" data-parsley-validate=""
                                      data-parsley-group="block-0" noValidate>
                                    <div className="form-group">
                                        <label className="control-label">Type</label>
                                        <FormControl componentClass="select" name="type"
                                                     className="form-control m-b"
                                                     value={this.state.user.type}
                                                     onChange={this._handleChange.bind(this, 'type')}
                                                     required>
                                            <option value="salesPerson">Sales Person</option>
                                            <option value="accounting">Accounting</option>
                                            <option value="accountingLimited">Accounting Limited
                                            </option>
                                            <option value="production">Production</option>
                                            <option value="leads">Leads</option>
                                            <option value="survey">Survey</option>
                                            <option value="surveyLimited">Survey Limited</option>
                                            <option value="service">Service</option>
                                        </FormControl>
                                    </div>
                                    {this.state.user.type == 'salesPerson' ?
                                        <div className="form-group">
                                            <label className=" control-label">Prefix</label>
                                            <FormControl type="text" placeholder="Prefix"
                                                         className="form-control"
                                                         value={this.state.user.prefix}
                                                         onChange={this._handleChange.bind(this, 'prefix')}
                                                         required/>
                                        </div>
                                        : null }

                                    <div className="form-group">
                                        <label className=" control-label">Company Name</label>
                                        <FormControl componentClass="select" name="type"
                                                     className="form-control m-b"
                                                     value={this.state.user.companyName}
                                                     onChange={this._handleChange.bind(this, 'companyName')}
                                                     required>

                                            {
                                                this.props.company.listByOrder.map(function (company) {
                                                    return <option value={company.companyName}
                                                                   key={company.id}>{company.companyName}</option>
                                                })
                                            }
                                        </FormControl>
                                    </div>
                                    <div className="form-group">
                                        <label className=" control-label">Permission</label>
                                        <FormControl componentClass="select" name="type"
                                                     className="form-control m-b text-capitalize"
                                                     value={this.state.user.permissions_group_id}
                                                     onChange={this._handleChange.bind(this, 'permissions_group_id')}
                                                     required>

                                            {
                                                this.props.permission.list.map(function (permission) {
                                                    return permission.name ?
                                                        <option value={permission.id}
                                                                className="text-capitalize"
                                                                key={permission.id}>{permission.name}</option> : false
                                                })
                                            }
                                        </FormControl>
                                    </div>

                                    <div className="form-group">
                                        <label className=" control-label">Name</label>
                                        <FormControl type="text" placeholder="Name"
                                                     className="form-control"
                                                     value={this.state.user.name}
                                                     onChange={this._handleChange.bind(this, 'name')}
                                                     required/>
                                    </div>

                                    <div className="form-group">
                                        <label className=" control-label">Email / Username</label>
                                        <FormControl
                                            id="email"
                                            type="text"
                                            onKeyUp={this.keyHandle}
                                            onBlur={this.checkDuplicate}
                                            data-parsley-minlength="3"
                                            placeholder="Email / Username"
                                            className={EmailClass}
                                            value={this.state.user.email}
                                            onChange={this._handleChange.bind(this, 'email')}
                                            required/>

                                        {!!this.state.user.email && this.state.editemail && !this.state.dublicateError && this.state.user.email.length > 2 ?
                                            <ul className="parsley-errors-list filled success">
                                                <li className="parsley-required">Username / email is
                                                    available
                                                </li>
                                            </ul> : null}
                                        {!!this.state.user.email && this.state.editemail && this.state.dublicateError && this.state.user.email.length > 2 ?
                                            <ul className="parsley-errors-list filled">
                                                <li className="parsley-required">Username / email is
                                                    not available
                                                </li>
                                            </ul> : null}
                                    </div>

                                    <div className="form-group">
                                        <label className=" control-label">Password</label>
                                        <FormControl type="text" data-parsley-minlength="4"
                                                     placeholder="Password" className="form-control"
                                                     value={this.state.user.password}
                                                     onChange={this._handleChange.bind(this, 'password')}
                                                     required/>
                                    </div>

                                    {this.state.user.type == 'salesPerson' ?
                                        <div>
                                            <div className="form-group">
                                                <label className="control-label">Discount</label>
                                                <FormControl type="text"
                                                             pattern="[0-9]+([.][0-9]+)?"
                                                             data-parsley-max="100"
                                                             placeholder="Discount"
                                                             className="form-control"
                                                             value={this.state.user.discount}
                                                             onChange={this._handleChange.bind(this, 'discount')}
                                                             required={!this.state.disableDiscount}
                                                             disabled={this.state.disableDiscount}/>
                                            </div>

                                            <div className="form-group">
                                                <label className="col-sm-5 control-label"><Row>Has
                                                    Sales Admin</Row></label>
                                                <Col sm={7 }>
                                                    <label className="switch">
                                                        <input name="salesAdmin" type="checkbox"
                                                               onChange={this._handleCheckChange.bind(this, 'salesAdmin')}
                                                               ref="salesAdmin"
                                                               checked={salesAdminType[this.state.user.salesAdmin]}/>
                                                        <em></em>
                                                    </label>
                                                </Col>
                                                <label className="col-sm-5 control-label"><Row>Any
                                                    time quote edits</Row></label>
                                                <Col sm={ 7 }>
                                                    <label className="switch">
                                                        <input name="anytimeQuoteEdits"
                                                               type="checkbox"
                                                               onChange={this._handleCheckChange.bind(this, 'anytimeQuoteEdits')}
                                                               ref="anytimeQuoteEdits"
                                                               checked={this.state.user.anytimeQuoteEdits}/>
                                                        <em></em>
                                                    </label>
                                                </Col>
                                            </div>
                                        </div>
                                        : null}


                                    <div className="form-group">
                                        <Button
                                            disabled={this.state.checking || this.props.user.isSaving}
                                            type="button" bsStyle="primary" bsSize="small"
                                            onClick={this._saveUser}>Save</Button>
                                        <Button type="button" className="mh" bsStyle="default"
                                                bsSize="small"
                                                onClick={this._backtoUser}>Cancel</Button>
                                        {this.props.userId !== 'new' ?
                                            <Button type="button" className="pull-right"
                                                    bsStyle="danger" bsSize="small"
                                                    onClick={this.showDelete}>Delete</Button> : null }
                                    </div>
                                </form> }
                        </Panel>
                        { /* END panel */ }
                    </Col>
                </Row>
                { /* END row */ }
            </ContentWrapper>
        );
    }

}

EditUser.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

EditUser.propTypes = {
    getPermissionsList: PropTypes.func.isRequired,
    apiGetCompaniesByOrder: PropTypes.func.isRequired,
    apiGetUserData: PropTypes.func.isRequired,
    apiSaveUser: PropTypes.func.isRequired,
    apiUpdateUser: PropTypes.func.isRequired,
    apiDeleteUser: PropTypes.func.isRequired,
};

const actions = Object.assign({},
    userAction,
    companyAction,
    permissionAction);

const apiActions = Object.assign({}, ApiUsers, ApiCompany);

export default compose(
    connect(state => ({
        user: state.user,
        company: state.company,
        permission: state.permission,
    }), actions),
    withProps(apiActions),
)(EditUser);
