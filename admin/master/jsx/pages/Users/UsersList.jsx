import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, Table, Alert, Modal } from 'react-bootstrap';
import DataTable from '../../components/datatable/DataTable.jsx'
import EditUser from './EditUser';

import {userAction} from '../../actions';

import {ApiUsers} from '../../api';

const table_fields = {
    name:'name',
    email:'email', 
    type:'type', 
    discount:'discount', 
    companyName:'companyName',
    prefix:'prefix'
}

class UsersList extends React.Component {
    constructor() {
      super();
      self = this;
      this.state = {
        "user":{},
        editModal: false,
        editingUserId: 'new'
      };

      this.showEditModal = this.showEditModal.bind(this);
      this.hideEditModal = this.hideEditModal.bind(this);
      this.editUser = this.editUser.bind(this);
      this.onEditModalClose = this.onEditModalClose.bind(this);
      this.newUser = this.newUser.bind(this);
      this.isAdmin = this.isAdmin.bind(this);
    }


    componentWillReceiveProps(nextProps){

        //Adding the Users Array to state variable
        this.setState({
            user:  nextProps.user
        })
    }


    shouldComponentUpdate(nextProps, nextState) {

        //Only if the users array has change re-render the component

        if(nextState.editModal != this.state.editModal) {
            return true
        }

        if (nextState.user == this.state.user) {
            return false;
        }

        return true;
    }

    isAdmin () {
        return this.props.auth.data && this.props.auth.data.permissions && this.props.auth.data.permissions.admin || false;
    }

     componentWillMount(){
        if(this.isAdmin()) {
            ApiUsers.apiGetUsers();
        }
     }

     onClickDT(data){
        if(this.isAdmin() === 'all' || data.id === this.props.auth.data.id ){
            this.editUser(data);
        }
    }

    showEditModal () {
        this.setState({
            editModal: true
        })
    }

    hideEditModal () {
        this.setState({
            editModal: false
        })
    }

    editUser (data) {
        this.setState({
            editingUserId: data.id
        })
        this.showEditModal();
    }

    onEditModalClose () {
        this.hideEditModal();
        if(this.isAdmin() === 'all'){
            ApiUsers.apiGetUsers();
        }
    }

    newUser () {
        this.editUser({
            id: 'new'
        })
    }

    render() {
        var self = this;

        const id = this.props.auth.data && this.props.auth.data.id || 0;

        const button = (
            this.isAdmin() ? <a onClick={this.newUser} className="mb mv btn btn-labeled btn-info mr pull-right">
                                <span className="btn-label"><i className="fa fa-plus"></i></span>Add User
                            </a> : 
                            <a onClick={() => this.editUser({id})} className="mb mv btn btn-labeled btn-info mr pull-right">
                                <span className="btn-label"><i className="fa fa-plus"></i></span>Edit User
                            </a>
        );
        const {editModal, editingUserId} = this.state;

        return (
            <ContentWrapper>
                <h3>Users List</h3>
                <Grid fluid>
                    <Row>
                        <Col lg={ 12 }>
                            {button}
    	                    <div className="clearfix"></div>
                            {this.isAdmin() ? <Panel>
                                <DataTable id="userTable" onClick={this.onClickDT.bind(this)} data={this.state.user.list} fields={table_fields} isLoading={this.state.user.isLoading} defaultOrder={0} />
                            </Panel> : null}
                            <Modal show={editModal} onHide={this.hideEditModal}>
                                <EditUser userId={editingUserId} onClose={this.onEditModalClose}/>
                            </Modal>
                         </Col>
                    </Row>
                </Grid>
            </ContentWrapper>
            );
    }

}


UsersList.contextTypes = {
    router: React.PropTypes.object.isRequired
};


UsersList.propTypes = {
};


const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, userAction,  ApiUsers, dispatch);
    return allActionProps;
}

// Notice mapDispatch is in the same block as the state, if added after the parentheses,
// the functions get wrapped with the dispatch call but you no longer have dispatch attached to the props.
export default connect(state => ({
  user: state.user,
  auth: state.auth
}), mapDispatch)(UsersList);
