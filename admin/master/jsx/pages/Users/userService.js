import axios from 'axios';
import * as constant from '../../constant.js';
import { browserHistory } from 'react-router';


//var hostUsers = 'https://' + apiHost + '/users';

var instance = axios.create({
  header: {'content-type':'text/plain;charset=UTF-8'}
});
export function checkDuplicate(email){
	return axios.get(constant.USER_LIST_URL+'/emailAvailable/'+email)
		.then(function(res){
			return res.data.availableEmail;
		})
}