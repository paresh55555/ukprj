import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import { Grid, Row, Col, Form, FormGroup, ControlLabel, FormControl, Button } from 'react-bootstrap';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import {brandingAction} from '../../actions';
import BrandingForm from '../../components/branding/BrandingForm'
import BrandingPreview from '../../components/branding/BrandingPreview'
import Loader from '../../components/loader/Loader'
import {NotifyAlert} from '../../components/Common/notify.js'
import DeleteDialog from '../../components/Common/DeleteDialog'

import {ApiBranding} from '../../api';

class SiteBranding extends React.Component {
    constructor() {
      super();
      this.state={
        branding:{
          banner_url : '',
          background_color : '',
          first_link_text : '',
          first_link_color : '',
          first_link_url : '',
          second_link_text : '',
          second_link_color : '',
          second_link_url : ''
        }
      }
      self = this;
      this.onChange = this.onChange.bind(this)
      this.onChangeColor = this.onChangeColor.bind(this)
      this.onSave = this.onSave.bind(this)
      this.AddBranding = this.AddBranding.bind(this)
      this.onDelete = this.onDelete.bind(this)
      this.onCancel = this.onCancel.bind(this)
    }
    componentWillMount(){
        this.props.ApiGetBranding();
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.branding.branding !== this.state.branding){
            console.log(nextProps.branding.branding)
            this.setState({branding:JSON.parse(JSON.stringify(nextProps.branding.branding))})
        }

        if(!nextProps.branding.isSaving && this.props.branding.isSaving) {
          NotifyAlert('Site branding added successfully')
        }

        if(nextProps.branding.isDeleted && !this.props.branding.isDeleted) {
          self.props.getBranding()
          swal("Deleted!",
                "Site branding has been removed.",
                 "success");
        }

    }
    onCancel(){
      this.setState({branding:JSON.parse(JSON.stringify(this.props.branding.branding))})
    }
    onChange(key,event){
        let branding = this.state.branding;
        branding[key] = event.target.value;
        this.setState({branding:branding})
    }
    onChangeColor(key,color){
        let branding = this.state.branding;
        branding[key] = color;
        this.setState({branding:branding})
    }
    onSave(){
        this.props.ApiUpdateBranding(this.state.branding)
    }
    AddBranding(){
        let branding = this.state.branding;
        branding.active=1
        this.props.ApiUpdateBranding(branding)
    }
    onDelete(){
      DeleteDialog({
            text: "Are you sure you want to remove site branding? ",
            onConfirm: function(){
              self.props.ApiDeleteBranding(self.props.dispatch)
              // self.props.ApiDeleteBranding().then((res)=>{
              // self.props.getBranding()
              // swal("Deleted!",
              //       "Site branding has been removed.",
              //        "success");
              // });
            }
        })
    }

    
    render() {
        return (
            <ContentWrapper>
                <h3>Site Branding</h3>
                <Grid fluid>
                {this.props.branding.isLoading ?
                    <Loader /> :
                    this.state.branding.active !== 1 ?
                    <Col lg={ 12 }>
                        
                    <Button onClick={this.AddBranding} className="mb mv btn btn-labeled btn-info mr">
                        <span className="btn-label"><i className="fa fa-plus"></i></span>"Add Site Branding"
                    </Button>
                    </Col>:
                    <div>
                    <BrandingPreview branding={this.state.branding}/>
                    <Row>
                        <Col lg={6}>
                            <div className="panel panel-default">
                                <div className="panel-body">
                                    <BrandingForm 
                                        onChangeColor={this.onChangeColor}
                                        onChange={this.onChange}
                                        branding={this.state.branding}
                                        onSave={this.onSave}
                                        onDelete={this.onDelete}
                                        onCancel={this.onCancel}
                                        isSaving={this.props.branding.isSaving}
                                    />
                                </div>
                            </div>
                        </Col>
                    </Row>
                    </div>
                }
                </Grid>
            </ContentWrapper>
            );
    }

}

SiteBranding.contextTypes = {
  router: React.PropTypes.object.isRequired
};

SiteBranding.propTypes = {
    deleteBranding: PropTypes.func.isRequired,
};

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, brandingAction,  ApiBranding, dispatch);
    return allActionProps;
}

// Notice mapDispatch is in the same block as the state, if added after the parentheses,
// the functions get wrapped with the dispatch call but you no longer have dispatch attached to the props.

export default connect(state => ({
  branding: state.branding
}, mapDispatch))(SiteBranding);


