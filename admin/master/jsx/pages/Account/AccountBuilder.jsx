import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import LayoutNameEdit from '../Cart/LayoutNameEdit';
import DropdownButton from '../../components/Common/Dropdown/DropdownButton';
import { ApiAccount } from '../../api';
import { accountAction } from '../../actions';
import AccountBuilderList from './AccountBuilderList';
import { isEmptyObject } from '../../components/Common/helpers';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

class AccountBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account: {
                name:'',
                quotes: {quote_builder_id: 0, list_builder_id: 0, cart_builder_id: 0},
                orders: {order_builder_id: 0, list_builder_id: 0, cart_builder_id: 0}
            },
            isEdit: false
        };
        this.changeName = this.changeName.bind(this);
        this.selectItem = this.selectItem.bind(this);
        this.cancel = this.cancel.bind(this)
        this.delete = this.delete.bind(this)
        this.save = this.save.bind(this)
    }
    componentWillMount(){
        ApiAccount.ApiGetAccountBuilders();
        if(this.props.routeParams.id){
            ApiAccount.ApiGetAccount(this.props.routeParams.id);
            this.setState({isEdit:true});
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.accountData && nextProps.accountData.length && this.state.isEdit){
            this.setState({account: nextProps.accountData[0]})
        }
    }
    changeName(e) {
        let newAccount = this.state.account;
        newAccount.name = e.target.value;
        this.setState({ account: newAccount });
    }
    selectItem(item, type){
        let newAccount = this.state.account;
        let types = type.split('-');
        newAccount[types[0]][types[1]] = item.id
        this.setState({account: newAccount})
    }
    cancel() {
        this.context.router.push('/builder/accounts');
    }
    delete(){
        ApiAccount.ApiDeleteAccount(this.props.routeParams.id).then(() => {this.cancel()});
    }
    save() {
        if (!this.state.account.name) {
            swal({
                title: 'Name field is required!',
                text: 'Please, fill the name field!',
                type: 'warning',
            });
        } else {
            if(!this.hasErrors()){
                if(this.state.isEdit){
                    ApiAccount.ApiUpdateAccount(this.state.account).then(() => {this.cancel()});
                }else{
                    ApiAccount.ApiCreateAccount(this.state.account).then(() => {this.cancel()});
                }
            }
        }
    }
    hasErrors(){
        let quotes = this.state.account.quotes;
        let orders = this.state.account.orders;
        if(!isEmptyObject(quotes)){
            if(quotes['quote_builder_id'] == 0 && quotes['list_builder_id'] == 0 && quotes['cart_builder_id'] == 0)return false;
            if(!quotes['quote_builder_id'] || !quotes['list_builder_id'] || !quotes['cart_builder_id']){
                swal({
                    title: 'All "Quotes" builders required!',
                    text: 'Please, select all quotes builders!',
                    type: 'warning',
                });
                return true;
            }
        }
        if(!isEmptyObject(orders)){
            if(orders['order_builder_id'] === 0 && orders['list_builder_id'] === 0 && orders['cart_builder_id'] === 0)return false;
            if(!orders['order_builder_id'] || !orders['list_builder_id'] || !orders['cart_builder_id']){
                swal({
                    title: 'All "Orders" builders required!',
                    text: 'Please, select all orders builders!',
                    type: 'warning',
                });
                return true;
            }
        }
        return false;
    }
    
    render() {
        const breadCrumbData = [
            {
                title: 'Account Builder', 
                url:'/builder/accounts',
                active:false
            },{
                title : this.state.account.name || '',
                url:``,
                active:true
            }
        ]   
        const {accountBuilders} = this.props;
        return (
            <ContentWrapper>
                <Breadcrumbs data={breadCrumbData} />
                <LayoutNameEdit
                    name={this.state.account.name || ''}
                    changeName={this.changeName}
                    fieldName="Account"
                    placeholder="List Name"
                />
                <div className="panel-default panel mainBox">
                    <div className="panel-body">
                        <div>
                            <div className="bg-info-dark defaultWidth">
                                <div className="optionTitle editable editable-click editable-disabled">
                                    Assign Builders to use for Displaying Quotes:
                                </div>
                            </div>
                            <div className="editSave">
                                {accountBuilders && accountBuilders.quotes ?
                                    <div key={'quote'}>
                                        <AccountBuilderList
                                            title={'List Builder'}
                                            onSelect={this.selectItem}
                                            buildersData={accountBuilders.quotes}
                                            accountData={this.state.account}
                                            type={'quotes'}
                                            builderType={'lists_builders'}
                                            selectedId={'list_builder_id'}
                                        />
                                        <AccountBuilderList
                                            title={'Cart Builder'}
                                            onSelect={this.selectItem}
                                            buildersData={accountBuilders.quotes}
                                            accountData={this.state.account}
                                            type={'quotes'}
                                            builderType={'cart_builders'}
                                            selectedId={'cart_builder_id'}
                                        />
                                        <AccountBuilderList
                                            title={'Quote Builder'}
                                            onSelect={this.selectItem}
                                            buildersData={accountBuilders.quotes}
                                            accountData={this.state.account}
                                            type={'quotes'}
                                            builderType={'quote_builders'}
                                            selectedId={'quote_builder_id'}
                                        />
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        </div>
                        <div>
                            <div className="bg-info-dark defaultWidth">
                                <div className="optionTitle editable editable-click editable-disabled">
                                    Assign Builders to use for Displaying Orders:
                                </div>
                            </div>
                            <div className="editSave">
                                {accountBuilders && accountBuilders.orders ?
                                    <div key={'order'}>
                                        <AccountBuilderList
                                            title={'List Builder'}
                                            onSelect={this.selectItem}
                                            buildersData={accountBuilders.orders}
                                            accountData={this.state.account}
                                            type={'orders'}
                                            builderType={'lists_builders'}
                                            selectedId={'list_builder_id'}
                                        />
                                        <AccountBuilderList
                                            title={'Cart Builder'}
                                            onSelect={this.selectItem}
                                            buildersData={accountBuilders.orders}
                                            accountData={this.state.account}
                                            type={'orders'}
                                            builderType={'cart_builders'}
                                            selectedId={'cart_builder_id'}
                                        />
                                        <AccountBuilderList
                                            title={'Order Builder'}
                                            onSelect={this.selectItem}
                                            buildersData={accountBuilders.orders}
                                            accountData={this.state.account}
                                            type={'orders'}
                                            builderType={'order_builders'}
                                            selectedId={'order_builder_id'}
                                        />
                                    </div>
                                    :
                                    null
                                }
                            </div>
                        </div>
                    </div>
                </div>
                <div className="mainBox bottom">
                    <button type="button" className="btn btn-default" onClick={this.save}>Save</button>
                    <button type="button" className="btn btn-default" onClick={this.cancel}>Cancel</button>
                    {this.state.isEdit ? <button type="button" className="btn btn-default noBackground" onClick={this.delete}>Delete</button> : null}
                </div>
            </ContentWrapper>
        );
    }
}

AccountBuilder.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

export default compose(
    connect(state => ({
        accounts: state.account.accounts,
        accountData: state.account.accountData,
        accountBuilders: state.account.accountBuilders,
    }), accountAction),
    withProps(ApiAccount),
)(AccountBuilder);