import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import LinkBox from '../../components/linkbox/LinkBox';
import { ApiAccount } from '../../api';
import { accountAction } from '../../actions';

class AccountLanding extends Component {
    componentWillMount(){
        ApiAccount.ApiGetAccounts();
    }
    addAccountBuilder(){
        this.context.router.push('builder/accounts/add')
    }
    render() {
        const { accounts } = this.props;
        return (
            <ContentWrapper>
                <h3>Account Builder</h3>
                <Button style={{"marginBottom":"10px"}} onClick={() => this.addAccountBuilder()}>
                    Add Account
                </Button>
                <Grid fluid>
                    <Row>
                        {accounts.length > 0 ?
                            accounts.map(
                                account => (
                                    <Col md={2} sm={4} key={account.id}>
                                        <LinkBox url={`builder/account/${account.id}`} title={account.name} />
                                    </Col>
                                ),
                            )
                            : null
                        }
                    </Row>
                </Grid>
            </ContentWrapper>
        );
    }
}

AccountLanding.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

export default compose(
    connect(state => ({
        accounts: state.account.accounts,
    }), accountAction),
    withProps(ApiAccount),
)(AccountLanding);