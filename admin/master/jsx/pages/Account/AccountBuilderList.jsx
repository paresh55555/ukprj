import React, { PropTypes } from 'react';
import DropdownButton from '../../components/Common/Dropdown/DropdownButton';
import { isEmptyObject } from '../../components/Common/helpers';

function AccountBuilderList(props) {
    const { title, onSelect, buildersData, accountData, type, builderType, selectedId } = props;
    var selectedItem = null;
    buildersData[builderType].find((b)=>{return b.id == 0}) ? false : buildersData[builderType].unshift({id: 0, name: "None"})
    if(accountData && (!isEmptyObject(accountData.quotes) || !isEmptyObject(accountData.orders))){
        selectedItem = buildersData[builderType].find((b)=>{
            return b.id == accountData[type][selectedId]
        })
        selectedItem = selectedItem ? selectedItem.name : 'None'
    }else{
        selectedItem = "None"
    }
    return (
        <div className="accountListWrapper">
            <div className="accountLabels"> {title}:</div>
            <DropdownButton
                data={buildersData[builderType]}
                changeData={onSelect}
                id={type+builderType}
                type={type+'-'+selectedId}
                Datakey={'name'}
            >
                {selectedItem}
            </DropdownButton>
        </div>
    );
}

export default AccountBuilderList;