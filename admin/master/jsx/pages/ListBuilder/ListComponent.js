import React from 'react';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import PanelHeading from '../../components/Layout/PanelHeading';
import {Dropdown, MenuItem, Button} from 'react-bootstrap';

class ListComponent extends React.Component {

    render() {

    	const statuses = [{
	        "name": "View All",
	        "order": "1"
	    }, {
	        "name": "Production",
	        "order": "2"
	    }]

        const available = {
            statuses: [
                "View All",
                "New",
                "Hot"
            ],
            columns: [
                "first_name",
                "last_name",
                "Company"
            ]
        }

        

        return (
            <ContentWrapper>
                <h3>Quote List Builder</h3>
                <label> List Name: </label>
            	<input placeholder="List Name" type="text" className="form-control" />
            	<br />
            	<div className="mySpace"></div>
            	<div className="panel-default panel mainBox">
            		<div className="panel-body">
            			<PanelHeading title={'Quote Status Bar:'} />
            			<div className="editSave">

                            <div>
                                <Dropdown>
                                  <Dropdown.Toggle>
                                    Add Status
                                  </Dropdown.Toggle>
                                  <Dropdown.Menu className={'animated fadeIn'}>
                                    {available 
                                        && available.statuses
                                        && available.statuses.map((status, index) => 
                                        <MenuItem key={index}>
                                            {status}
                                        </MenuItem>
                                    )}
                                  </Dropdown.Menu>
                                </Dropdown>
                                <Dropdown>
                                  <Dropdown.Toggle>
                                    Delete Status
                                  </Dropdown.Toggle>
                                  <Dropdown.Menu className={'animated fadeIn'}>
                                    {statuses
                                        && statuses.map((status, index) => 
                                        <MenuItem key={index}>
                                            {status.name}
                                        </MenuItem>
                                    )}
                                  </Dropdown.Menu>
                                </Dropdown>
                                <Button className="btn btn-default">Re-Order</Button>
                            </div>
                            <div style={{marginTop: 30}}>
                                {statuses && statuses.map(status => <button className="btn btn-primary myRight">{status.name}</button>)}
                            </div>
            			</div>
            		</div>
            		<div className="panel-body">
            			<PanelHeading title={'Quote Table:'} />
            		</div>
            	</div>
            </ContentWrapper>
        )
    }
}

export default ListComponent;