import React from 'react';
import {Form} from 'react-bootstrap';
import FormField from './FormField'

export default function FormPricingModal (props) {
    return (
        <Form data-parsley-validate="" data-parsley-group="block-0" noValidate className="pricing-form">
            {props.title === 'Part Size Cost' ? <FormField label="Cost / Meter" name="amount" handleChange={props.handleChange} formType={props.type} /> : null}
            {props.title !== 'Part Size Cost' ? <FormField value={props.data ? props.data.name : ''} label="Name" name="name" handleChange={props.handleChange} formType={props.type} /> : null}
            {props.title === 'Percentage Cost' ? <FormField value={props.data ? props.data.amount : ''} label="Percentage" name="amount" handleChange={props.handleChange} formType={props.type} /> : null}
            {props.title === 'Fixed Cost' ? <FormField label="Amount" value={props.data ? props.data.amount : ''} name="amount" handleChange={props.handleChange} formType={props.type} /> : null}
            {props.title === 'Per Swing Door Cost' ? <FormField label="Per Swing Door Amount" name="amount" handleChange={props.handleChange} formType={props.type} /> : null}
            {props.title === 'Per Panel Cost' ? <FormField label="Per Panel Amount" name="amount" handleChange={props.handleChange} formType={props.type} /> : null}
            {props.title === 'Per Panel Cost' ? <FormField label="Include Swings" name="swings" handleChange={props.handleChange} fieldType="checkbox" formType={props.type} /> : null}
            <FormField checked={props.data ? !!props.data.taxable : false} label="Taxable" name="taxable" handleChange={props.handleChange} fieldType="checkbox" formType={props.type} />
            <FormField checked={props.data ? !!props.data.markup : false} label="Apply Markup" name="markup" handleChange={props.handleChange} fieldType="checkbox" formType={props.type} />
    </Form>
    )
}