import React from 'react';
import {Dropdown, MenuItem} from 'react-bootstrap';

export default function CostDropdown (props) {
    return (
        <Dropdown id={props.id}>
            <Dropdown.Toggle>
                {props.name}
            </Dropdown.Toggle>
            <Dropdown.Menu className={'animated fadeIn'}>
                {props.types.map((obj, index) => (
                    <MenuItem key={index} onClick={()=>props.checkItem(obj.type)}>{obj.title}</MenuItem>
                ))}
            </Dropdown.Menu>
        </Dropdown>
    )
}