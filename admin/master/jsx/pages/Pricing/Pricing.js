import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import {Dropdown, MenuItem, Button} from 'react-bootstrap';
import {systemAction, componentAction} from '../../actions';
import {NotifyAlert} from '../../components/Common/notify.js'

import SampleDoorPricing from './SampleDoorPricing'
import FoldingPanel from '../../components/Common/FoldingPanel'
import PricingSections from './PricingSections'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import PriceOption from './PriceOption';

import {ApiSystems, ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class Pricing extends React.Component {
    constructor() {
        super();
        this.state = {
            action: '',
            published:false,
        }
        this.routerWillLeave = this.routerWillLeave.bind(this);
        this.getContent = this.getContent.bind(this);
    }
    componentWillMount() {
        this.props.ApiGetSystemComponent(this.props.params.id);
        ApiSystems.getSampleDoorPrice(this.props.params.id);
        this.props.ApiGetComponentData(this.props.params.id);
    }
    componentDidMount(){
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
        // return false to prevent a transition w/o prompting the user,
        // or return a string to allow the user to decide:
        if (this.props.component.edit_mode)
            return 'Your work is not saved! Are you sure you want to leave?'
    }
    componentWillUnmount(){
        this.props.setEditMode(false)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.component.isPublished && this.state.published){
            NotifyAlert(`System published successfully`);
            this.setState({published:false})
        }
    }

    identifyComponentByType(component) {
        return !!this.props.component.component_types.find(type => type.name === component.name);
    }

    getContent(dimensions){
        return(
            <div className="editSave">
                {dimensions ?
                    <PriceOption key={dimensions.id} systemID={dimensions.system_id} component={dimensions} componentID={dimensions.id} option={dimensions} />
                    :
                    'Please add "Dimesnions" component'
                }
            </div>
        )
    }
    publishSystem(){
        const systemTitle = this.props.component && this.props.component.system && this.props.component.system.traits && this.props.component.system.traits.length &&
            this.props.component.system.traits.find((trait)=>trait.name ==='title')
        this.setState({published:true})
        this.props.ApiPublishSystemComponents(this.props.params.id, systemTitle && systemTitle.value)
    }

    render() {
        const {system, list} = this.props.component;
        const {sampleDoors} = this.props.system;
        const title = system && system.traits && system.traits.find((trait)=>{return trait.name === 'title'})
        const componentsList = list.filter((component)=>{ return ['system','menu','SampleDoorParts','SampleDoorPrice','ColorGroup','Dimensions','ColorSubGroup', 'NextTab'].indexOf(component.type) < 0 })
        const dimensions = list.find((component)=>{ return component.type === 'Dimensions'})
        const name =  this.props.component.system.name === 'Door System' ? 'Door System Template': (this.props.component.system.name === 'Window System' ? 'Window System Template' : 'Active System')
        const url =  this.props.component.system.name === 'Door System' ? '/builder/systems/doors': (this.props.component.system.name === 'Window System' ? '/builder/systems/windows' : '/systems')
        const breadCrumbData = [
            {
                title: name, 
                url:url,
                active:false
            },{
                title : title && title.value || '',
                url:`/systems/${this.props.params.id}`,
                active:false
            },
            {
                title : 'Pricing',
                url:'',
                active:true
            }

        ]

        return (
            <ContentWrapper>
                <Breadcrumbs data={breadCrumbData} />
                <div>
                    
                    <div className="panel-default panel mainBox">
                    <Button disabled={this.props.component.isLoading} onClick={this.publishSystem.bind(this)} className="publish-system btn btn-labeled btn-info pull-left">
                        Publish "{title && title.value}"
                    </Button>
                        <div className="panel-body">
                            
                            <SampleDoorPricing
                                systemID={system && system.id}
                                componentID={sampleDoors && sampleDoors.length && sampleDoors[0].id}
                                traits={sampleDoors && sampleDoors.length && sampleDoors[0].traits || []} />
                            <FoldingPanel title="Costs on Every Door" content={this.getContent(dimensions)}/>
                            {componentsList && componentsList.map((component)=>{
        console.log(component, '96')

                                if (component.type === 'Color'){
                                    //console.log(1, this.props.component, component)
                                    let groups = list.filter((cmp)=>{ return cmp.type === 'ColorGroup' && cmp.grouped_under === component.id })
                                    //console.log(2, groups)
                                    return <PricingSections key={component.id} systemID={system && system.id} component={component} options={groups}/>
                                }else{
                                    //console.log(3, component.options, component)
                                    return <PricingSections key={component.id} systemID={system && system.id} component={component} options={component.options} />
                                }
                            })}
                        </div>
                    </div>
                </div>
            </ContentWrapper>
        );
    }

}
Pricing.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiSystems, ApiComponent);

export default compose(
    connect(state => ({
        system: state.system,
        component: state.component,
    }), Object.assign({}, systemAction, componentAction)),
    withProps(apiActions),
)(Pricing);