import React, { Component, PropTypes } from 'react';
import FoldingPanel from '../../components/Common/FoldingPanel';
import PriceOption from './PriceOption';

export default class PricingSections extends Component {
    constructor(){
        super();
        this.state = {};
        this.getContent = this.getContent.bind(this)
    }
    getContent(options){
        let content = options && options.map((option)=>{
                return <PriceOption key={option.id} systemID={this.props.systemID} component={this.props.component} componentID={this.props.component.id} option={option} />;
            })
        return content.length ? content : <div>No options</div>
    }
    render(){
        const {component, options} = this.props;

        const titleElement = this.props.title || component && component.traits.find((trait)=>{return trait.name === 'title'});
        const title = component.type === 'Color' ? 'Exterior Color:' : 'Costs For: '+(titleElement ? titleElement.value : '');
        return(
            <div>
                <FoldingPanel key={component && component.id} title={title} content={this.getContent(options)}/>
            </div>
        )
    }
}