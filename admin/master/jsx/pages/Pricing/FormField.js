import React from 'react';
import {FormGroup, ControlLabel, FormControl, Checkbox} from 'react-bootstrap';

export default function FormField (props) {
    return (
        <FormGroup>
            <ControlLabel>{props.label}</ControlLabel>
            {props.fieldType !== "checkbox" ? <FormControl value={props.value} onChange={props.handleChange(props.name, props.formType)} type="text"/> :
                <Checkbox checked={props.checked} onChange={props.handleChange(props.name, props.formType, props.fieldType)} />
            }
        </FormGroup>
    )
}