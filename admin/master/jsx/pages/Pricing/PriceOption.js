import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import {componentAction, optionAction, costAction} from '../../actions';
import PartSize from '../../components/Costs/PartSize';
import PricingDataTable from '../../components/Costs/PricingDataTable';
import PercentageTable from '../../components/Costs/PercentageTable';
import SwingDoor from '../../components/Costs/SwingDoor';
import CostDropdown from './CostDropdown';
import SQModal from '../../components/Common/SQModal/SQModal';
import FormPricingModal from './FormPricingModal';
import {NotifyAlert} from '../../components/Common/notify.js';
import DeleteDialog from '../../components/Common/DeleteDialog';
import { ApiOption, ApiCost, ApiComponent } from '../../api';
import Loader from '../../components/loader/Loader';

const FixedCostfields = {
    name : "Cost Name",
    amount: "Amount",
    taxable: "Taxable",
    markup: "Markup",
}

const panelCostfields = {
    name : "Cost Name",
    amount: "Per Panel",
    swings: "Include Swings",
    taxable: "Taxable",
    markup: "Markup",
}
const PercentageCostfields = {
    name : "Cost Name",
    amount: "Amount",
    taxable: "Taxable",
    markup: "Markup",
}
const PART_COST_TYPE = 'PartCost'

class PriceOption extends React.Component {
    constructor(){
        super()
        this.state = {
            parts: [],
            content: <div>No data</div>,
            isSaving: false,
            updatingPartCost: false,
            clickedType: null,
            costsForms: null,
            isLoading: false,
            costs: []
        };
        this.costTypes = [
            {type:'PartCost', title: 'Part Size Cost'},
            {type:'PercentageCost', title: 'Percentage Cost'},
            {type:'FixedCost', title: 'Fixed Cost'},
            {type:'SwingDoor', title: 'Per Swing Door Cost'},
            {type:'PanelCost', title: 'Per Panel Cost'}
        ];

        this.onChangeCost = this.onChangeCost.bind(this);
        this.onDeleteCost = this.onDeleteCost.bind(this);
        this.onDeleteCosts = this.onDeleteCosts.bind(this);
        this.openModal = this.openModal.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onCheckChange = this.onCheckChange.bind(this);
    }
    componentWillMount(){
        if(this.props.option || this.props.component){
            if(this.props.component.type === 'Color' && this.props.option.type === 'ColorGroup' || this.props.component.type === 'Dimensions') {
                this.props.ApiGetComponentParts(this.props.systemID, this.props.componentID);
                this.props.ApiGetComponentCosts(this.props.systemID, this.props.componentID);
            } else {
                this.props.getOptionParts(this.props.systemID, this.props.componentID, this.props.option.id);
                this.props.getOptionCosts(this.props.systemID, this.props.componentID, this.props.option.id);
            }
        }
    }

    componentWillReceiveProps(nextProps){

        if(this.props.component.type === 'ColorGroup' || this.props.component.type === 'Color' || this.props.component.type === 'Dimensions'){
            if(nextProps.cmp.componentID === this.props.componentID){
                this.setState({parts: nextProps.cmp.parts, costs: nextProps.cmp.costs, isLoading: nextProps.cmp.isLoading})
            }
        }else{
            if(nextProps.opt.optionID === this.props.option.id){
                this.setState({parts: nextProps.opt.parts, costs: nextProps.opt.costs, isLoading: nextProps.opt.isLoading})
            }
        }
        if(nextProps.cmp.parts.length && nextProps.cmp.costs.length && !this.state.updatingPartCost){
            let shouldSave = false;
            nextProps.cmp.parts.forEach((part)=>{
                if(part.costs && !part.costs.find((cost)=>{return cost.cost_type === 'PartCost'})){shouldSave = true}
            })
            if(shouldSave){
                this.setState({updatingPartCost: true}, function(){this.openModal('PartCost')})
            }
        }
    }
    openModal (clickedType, data) {
        if(data) {
            delete data.rawCost;
            delete data.finalCost;
            delete data.deleteBtn;
        }
        this.setState({clickedType: clickedType, costsForms: Object.assign({}, this.state.costsForms, data, {cost_type: clickedType})},function(){
            if(clickedType == PART_COST_TYPE){
                this.onSave()
            }else{
                this.panelForm.open();
            }
        })
    }
    handleChange(key, formType, fieldType){

        return (event) => {
            let value = fieldType === 'checkbox' ? event.target.checked : event.target.value;
            //const type = formType.substr(0, 1).toLowerCase() + formType.substr(1).replace(/\s/g, '');
            this.setState({
                //costsForms: Object.assign({}, this.state.costsForms, {[type]: Object.assign({}, this.state.costsForms[type], {[key]: value})})
                costsForms: Object.assign({}, this.state.costsForms, {cost_type: formType, [key]: value})
            });
        }
    }
    onSave () {
        this.setState({isSaving: true})

        // this.props.addOptionPartCost(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms, this.state.costs || [])
        // .then(res => this.setState({isSaving: false}))

        if(this.state.clickedType === 'PercentageCost' || this.state.clickedType === 'FixedCost' || this.state.clickedType === 'PanelCost') {
            if(this.props.option && this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions'){
                if(this.state.costsForms && this.state.costsForms.id) {
                    if(this.props.option.type === 'Dimensions'){
                        this.props.ApiUpdateComponentCostData(this.props.systemID, this.props.componentID,this.state.costsForms.id, this.state.costsForms, this.props.cmp.costs)

                    }
                    this.props.ApiUpdateCostData(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms.id, this.state.costsForms)
                } else {
                    if(this.props.option.type === 'Dimensions'){
                        console.log(121)
                        this.props.ApiAddComponentPartCost(this.props.systemID, this.props.componentID, this.state.costsForms, this.props.cmp.costs)
                                            
                    }
                    else{
                    this.props.ApiAddOptionPartCost(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms)
                        .then(res => {
                            //this.props.getOptionCosts(this.props.systemID, this.props.componentID, 999).then(res=>{console.log(111, res)});
                        })
                    }
                }
            } else {
                if(this.state.costsForms && this.state.costsForms.id) {
                    this.props.ApiUpdateCostData(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms.id, this.state.costsForms)
                } else {
                    this.props.ApiAddOptionPartCost(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms)
                        .then(res => {
                            //this.props.getOptionCosts(this.props.systemID, this.props.componentID, this.props.option.id);
                        })
                }
            }
            this.setState({costsForms: null})
        } else {

            if(this.state.costsForms && this.state.costsForms.id) {
                this.props.ApiUpdateCostData(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms.id, this.state.costsForms)
            } else {
                var self = this;
                var type = this.state.clickedType;
                if(this.props.option){
                    this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions' ?
                        this.props.ApiGetComponentParts(this.props.systemID, this.props.componentID).then((res)=>{
                            if(!res.length){
                                NotifyAlert("Parts not found", 'warning');
                                return;
                            }
                            if(type === PART_COST_TYPE){
                                res = res.filter(part => !part.costs.find((cost)=>{return cost.cost_type === 'PartCost'}))
                            }
                            let data = res.map((part)=>{
                                return type === PART_COST_TYPE ?
                                    Object.assign({ name: type, part_id: part.id}, self.state.costsForms)
                                    :
                                    Object.assign({}, {part_id: part.id}, self.state.costsForms);
                            })
                            self.props.ApiCreatePartsCosts(self.props.systemID, self.props.componentID, 0, data).then(()=>{
                                self.props.ApiGetComponentParts(self.props.systemID, self.props.componentID)
                                self.setState({costsForms: null,updatingPartCost:false});
                            });
                        })
                        :
                        this.props.getOptionParts(this.props.systemID, this.props.componentID, this.props.option.id).then((res)=>{
                            if(!res.length){
                                NotifyAlert("Parts not found",'warning');
                                return;
                            }
                            let data = res.map((part)=>{
                                return type === PART_COST_TYPE ?
                                    Object.assign({ name: type, part_id: part.id}, self.state.costsForms)
                                    :
                                    Object.assign({}, {part_id: part.id}, self.state.costsForms);
                            })
                            self.props.ApiCreatePartsCosts(self.props.systemID, self.props.componentID, self.props.option.id, data).then((res)=>{
                                self.props.getOptionParts(self.props.systemID, self.props.componentID, self.props.option.id)
                                self.setState({costsForms: null,updatingPartCost:false});
                            });
                        })
                }
            }
        }
        this.setState({isSaving: false})
        // if(this.state.costsForms && this.state.costsForms.id) {
        //     this.props.updateCostData(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms.id, this.state.costsForms)
        // if(this.props.option && this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions'){
        //     NotifyAlert("Parts not found",'warning');
        // } else {
        //     if(this.state.costsForms && this.state.costsForms.id) {
        //         this.props.updateCostData(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms.id, this.state.costsForms)
        //     } else {
        //         this.props.addOptionPartCost(this.props.systemID, this.props.componentID, this.props.option.id, this.state.costsForms)
        //     }
        // }
        //this.setState({costsForms: {name: ' '}})
        this.panelForm.close();
    }

    onCheckChange(data, clickedType){
        delete data.rawCost;
        this.setState({costsForms:data, clickedType},()=>{
            this.onSave()
        })
    }

    onChangeCost(savedCost){
        // this.props.getOptionParts(this.props.systemID, this.props.componentID, this.props.option.id);
        var parts = this.state.parts;
        var newParts = [];
        parts.forEach((part)=>{
            if(part.id == savedCost.part_id){
                var newCosts = [];
                part.costs.forEach((cost)=>{
                    if(cost.id == savedCost.id){
                        newCosts.push(savedCost)
                    }else{
                        newCosts.push(cost)
                    }
                })
                part.costs = newCosts;
            }
            newParts.push(part)
        })
        this.setState({parts:newParts})
    }
    onDeleteCost(id){
        var parts = this.state.parts;
        var newParts = [];
        parts.forEach((part)=>{
            var newCosts = [];
            part.costs = part.costs.filter((cost)=>{
                return cost.id != id
            })
            newParts.push(part)
        })
        this.setState({parts:newParts})
    }
    onDeleteCosts(){
        this.costTypes.unshift({type:'PartCost', title: 'Part Size Cost'});
        this.setState({parts: []});
    }
    getTitle(type){
        if(type){
            let idx = this.costTypes.findIndex(function(ctype){return ctype && ctype.type == type})
            if(idx !== -1)return this.costTypes[idx].title
        }
    }
    getCostsInfo(){
        var info = {
            partCost: false,
            percentage: false,
            fixed: false,
            perSwing: false,
            perPanel: false
        }
        this.state.parts.forEach((part)=>{
            if(part.costs && part.costs.find((cost)=>{return cost.cost_type === PART_COST_TYPE})){info.partCost = true}
            if(part.costs && part.costs.find((cost)=>{return cost.cost_type === 'SwingDoor'})){info.perSwing = true}
        })
        let currentCosts = this.props.currentCosts
        if(this.props.component && this.props.component.type === 'Dimensions'){
            currentCosts = this.props.cmp.costs;
        }
        if(currentCosts && currentCosts.find((cost)=>{return cost.cost_type === 'PercentageCost'})) {
            info.percentage = true
        }
        if(currentCosts && currentCosts.find((cost)=>{return cost.cost_type === 'FixedCost'})) {
            info.fixed = true
        }

        if(currentCosts && currentCosts.find((cost)=>{return cost.cost_type === 'PanelCost'})) {

            info.panelCost = true

        }

        return info;
    }

    onDelete (data) {
        const self = this;
        if(data) {
            DeleteDialog({
                text: "Are you sure you want to delete this cost? ",
                onConfirm: function(){
                    if(self.props.option.type === 'Dimensions'){
                        self.props.ApiDeleteComponentCost(self.props.systemID, self.props.componentID, data.id,self.props.cmp.costs)
                        .then(()=>swal("Deleted!", "Cost has been deleted.", "success"));
                    }
                    else{
                        self.props.ApiDeleteCost(self.props.systemID, self.props.componentID, self.props.option.id, data.id)
                        .then(()=>swal("Deleted!", "Cost has been deleted.", "success"));
                    }
                    
                    self.setState({costsForms: null})
                }
            })
        }
    }

    render(){
        const title = this.props.option && this.props.option.traits.find((trait)=>{ return trait.name === 'title' });
        const costInfo = this.getCostsInfo();
        const {sampleDoors} = this.props.system;
        let markup = 0;
        let tax = 0;
        let panels = 0;
        let swingDoors = 0;
        let defaultTotal = 0;
        let taxObj = sampleDoors && sampleDoors.length && sampleDoors[0].traits.find(item=>item.name === 'tax')
        //let tax = +(taxObj && taxObj.value)

        const {isLoading} = this.state;

        if(sampleDoors && sampleDoors.length && sampleDoors[0].traits) {
            sampleDoors && sampleDoors.length && sampleDoors[0].traits.forEach(trait => {
                if(trait.name == 'markUp') {
                    markup = parseInt(trait.value, 10);
                }
                if(trait.name == 'tax') {
                    tax = parseInt(trait.value, 10);
                }
                if(trait.name == 'defaultTotal') {
                    defaultTotal = trait.value;
                }
                if(trait.name == 'panels') {
                    panels = parseInt(trait.value, 10);
                }
                if(trait.name == 'swings') {
                    swingDoors = trait.value;
                }
            })
        }
        let currentCosts  = this.props.currentCosts;
        if(this.props.component && this.props.component.type === 'Dimensions'){
            currentCosts = this.props.cmp.costs;
        }

        const PerPanelCosts = costInfo.panelCost ? currentCosts && currentCosts.filter(cost => cost.cost_type === 'PanelCost' || cost.cost_type === 'Panel Cost').map(cost => {
                return Object.assign({}, cost, {
                    rawCost: cost.swings ? cost.amount * panels : cost.amount * (panels - swingDoors)
                })
            }) : [];

        const PercentageCosts = costInfo.percentage ? currentCosts && currentCosts.filter(cost => cost.cost_type === 'Percentage Cost' || cost.cost_type === 'PercentageCost') : [];
        const FixedCosts = costInfo.fixed ? currentCosts && currentCosts.filter(cost => cost.cost_type === 'Fixed Cost' || cost.cost_type === 'FixedCost') : [];
        if(costInfo.partCost){
            let idx = this.costTypes.findIndex(ctype => ctype.type == 'PartCost')
            if(idx === 0) this.costTypes.splice(idx, 1)
        }
        return(
            <div className="optionsBox">
                <div className="colorGroupMenu">
                    {this.props.option.type === 'Dimensions' ? null : (
                            this.props.option.type === 'ColorGroup' ?
                                <div className="groupTitle mb-sm mr-sm btn btn-success btn-outline colorGroupButton">{title && title.value}</div>
                                :
                                <div className="mb-sm mr-sm btn btn-info btn-outline colorGroupButton optionsTitle">Option: {title && title.value}</div>

                        )}
                    <CostDropdown checkItem={this.openModal} types={this.costTypes} name="Add Cost" id={'dd-'+this.props.componentID} />
                </div>
                <div className="editSave">

                    {this.state.updatingPartCost && <Loader/>}
                    {costInfo.partCost ?
                        <PartSize id={"partSize-"+this.props.option.id}
                                  systemID={this.props.systemID}
                                  componentID={this.props.componentID}
                                  parts={this.state.parts}
                                  optionID={this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions' ? 0 : this.props.option.id}
                                  onChangeCost={this.onChangeCost}
                                  onDeleteCosts={this.onDeleteCosts}
                        />
                        : null
                    }
                    {costInfo.percentage ?
                        <PercentageTable id={"fixed-"+this.props.option.id}
                                         heading={'Percentage Cost'}
                                         systemID={this.props.systemID}
                                         componentID={this.props.componentID}
                                         data={PercentageCosts}
                                         onDelete={this.onDelete}
                                         markup={markup}
                                         isLoading={isLoading}
                                         tax={tax}
                                         defaultTotal={defaultTotal}
                                         openModal={this.openModal}
                                         traits={sampleDoors && sampleDoors.length && sampleDoors[0].traits || []}
                                         optionID={this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions' ? 0 : this.props.option.id}
                        /> : null
                    }
                    {costInfo.fixed ?
                        <PricingDataTable id={"fixed-"+this.props.option.id}
                                          heading={'Fixed Cost'}
                                          systemID={this.props.systemID}
                                          componentID={this.props.componentID}
                                          data={FixedCosts}
                                          onDelete={this.onDelete}
                                          fields={FixedCostfields}
                                          markup={markup}
                                          isLoading={isLoading}
                                          tax={tax}
                                          onEdit={this.openModal}
                                          onCheckbox={(data) => this.onCheckChange(data, 'FixedCost')}
                                          optionID={this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions' ? 0 : this.props.option.id}
                        /> : null
                    }
                    {costInfo.panelCost ?
                        <PricingDataTable id={"panelCost-"+this.props.option.id}
                                          heading={'Per Panel Cost'}
                                          systemID={this.props.systemID}
                                          componentID={this.props.componentID}
                                          data={PerPanelCosts}
                                          onDelete={this.onDelete}
                                          fields={panelCostfields}
                                          markup={markup}
                                          isLoading={isLoading}
                                          tax={tax}
                                          onEdit={this.openModal}
                                          onCheckbox={(data) => this.onCheckChange(data, 'PanelCost')}
                                          optionID={this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions' ? 0 : this.props.option.id}
                        /> : null
                    }
                    {costInfo.perSwing ?
                        <SwingDoor id={"swingDoor-"+this.props.option.id}
                                   systemID={this.props.systemID}
                                   componentID={this.props.componentID}
                                   parts={this.state.parts}
                                   optionID={this.props.option.type === 'ColorGroup' || this.props.option.type === 'Dimensions' ? 0 : this.props.option.id}
                                   onChangeCost={this.onChangeCost}
                                   onDeleteCost={this.onDeleteCost}
                        />
                        : null
                    }
                    <SQModal
                        ref={(c)=>this.panelForm = c}
                        title={this.getTitle(this.state.clickedType)}
                        onConfirm={this.onSave}
                        onClose={this.onClose}
                        disabled={this.state.isSaving}
                        confirmText={this.state.isSaving ? "Saving..." : "Save"}>
                        <FormPricingModal
                            self={this}
                            handleChange={this.handleChange}
                            type={this.state.clickedType}
                            data={this.state.costsForms}
                            title={this.getTitle(this.state.clickedType)} />
                    </SQModal>
                </div>
            </div>
        )
    }
}

const apiActions = Object.assign({}, ApiOption, ApiComponent, ApiCost);

export default compose(
    connect(state => ({
        opt: state.option,
        cmp: state.component,
        system: state.system,
    }), Object.assign({}, componentAction, optionAction, costAction),
        (stateProps, dispatchProps, ownProps) => (
            Object.assign({}, ownProps, stateProps, dispatchProps, {currentCosts: stateProps.opt.costs[ownProps.option.id], costs: stateProps.opt.costs}
        )),
    ),
    withProps(apiActions),
)(PriceOption);
