import React, {Component} from 'react';
import {connect} from "react-redux";
import {ButtonToolbar} from "react-bootstrap";
import ContentWrapper from "../../components/Layout/ContentWrapper";
import PageContent from "../../components/Layout/PageContent";
import ServicePanel from "../../components/Service/ServicePanel";
import PageTitle from "../../components/Layout/PageTitle";
import SQJobForm from "../../components/Service/Jobs/SQJobForm"
import JobDoors from "../../components/Service/Jobs/JobDoors";
import JobCustomers from '../../components/Service/Jobs/JobCustomers';
import JobButtons from '../../components/Service/Jobs/JobButtons'
import ServiceAddressList from '../../components/Service/Jobs/ServiceAddressList'
import * as ApiServiceJob from "../../api/ApiServiceJob";
import { setJobTab } from "../../actions/serviceJob";
import moment from 'moment';
import ServiceButton from '../../components/Service/ServiceButton';
import JobActivityLog from './JobActivityLog';
import JobEmails from './JobEmails';
 
class ServiceJobsDetails extends Component {

    state = {
        currentJob: {},
        hasChanges: false,
        isLoading: false
    }

    componentWillReceiveProps (nextProps) {

        const nextJob = (nextProps.serviceJob && nextProps.serviceJob.currentJob) || {}
        const { currentJob } = this.props.serviceJob

        if((nextJob && nextJob.id) !== (currentJob && currentJob.id)) {
            this.setJob(nextJob)
        }
    }

    componentDidMount () {
        const { currentJob } = this.props.serviceJob
        this.setJob(currentJob)
        this.changeTab('detail')
    }

    componentWillMount () {
        this.getJob()
    }

    getJob = () => {
        let {jobId} = this.props.params;
        ApiServiceJob.ApiGetJobs(jobId);
        ApiServiceJob.ApiGetJobFiles(jobId)
    }

    setJob = currentJob => this.setState({
        currentJob
    })

    onChangeText = e => {
        const { currentJob } = this.state;
        const { name, value } = e.target

        this.setState({
            currentJob: {
                ...currentJob,
                [name]: value
            }
        })
    }

    onChangeTech = value => {
        const { id, name } = value;

        this.setJob({
            ...this.state.currentJob,
            tech: id,
            tech_name: name
        })
    }

    onChangeStatus = (filter,value) => {
        this.setJob({
            ...this.state.currentJob,
            job_status: value,
            filter: filter
        })
    }

    onChangeTime = (value) => {
        this.setJob({
            ...this.state.currentJob,
            start_time: moment.utc(value*1000).format('LT')
        })
    }

    onChangeDate = date => this.setJob({
        ...this.state.currentJob,
        start_date: moment(date).format('YYYY-MM-DD')
    })

    onSaveJob = () => {

        this.setState({
            isLoading: true
        })

        ApiServiceJob.ApiUpdateJob(this.state.currentJob)
            .then(
                response => {
                    
                    this.setState({
                        isLoading: false
                    })

                    swal("Saved!", "Job has been saved.", "success");

                    this.getJob()
                }
            )
            .catch(
                err => {
                    this.setState({
                        isLoading: false
                    })

                    swal("Saved!", "Some error occured.", "error");
                }
            )
    }

    onCancel = () => {
        this.context.router.push('Service/jobs')
    }

    changeTab = tabName => {
        this.props.dispatch(setJobTab(tabName))
    }

    getJobActivity = () => {
        const {jobId} = this.props.params;
        ApiServiceJob.ApiGetJobActivity(jobId)
    }

    getJobSentEmail = () => {
        const {jobId} = this.props.params;
        ApiServiceJob.ApiGetSendEmailDetail(jobId)
    }

    



    renderJobContent = () => {

        const { activeTab } = this.props.serviceJob;
        const {jobId} = this.props.params;
        const {currentJob = {}} = this.state;

        switch ( activeTab ) {
            
            case 'detail':
                return (
                    <ServicePanel>
                        <div className="JobDetail_Header">
                            <h4 className="jobfill">Job Detail</h4>
                            <ButtonToolbar className='job_headerBtn' >
                                <ServiceButton  className="btn btn-default cancelbtn" label={'Job Activity'} onClick={() => this.changeTab('activity')} />
                            </ButtonToolbar>
                            <ButtonToolbar className='job_headerBtn job_email_tab' >
                                <ServiceButton  className="btn btn-default cancelbtn" label={'Send Email'} onClick={() => this.changeTab('email')} />
                            </ButtonToolbar>
                        </div>
                        <div className="Jobsection">
                            <div >
                                <SQJobForm
                                currentJob={currentJob}
                                onChangeText={this.onChangeText}
                                onChangeDate={this.onChangeDate}
                                onChangeTech={this.onChangeTech}
                                onChangeTime={this.onChangeTime}
                                onChangeStatus={this.onChangeStatus}
                                />
                                <JobCustomers jobId={jobId}/>
                                <JobDoors jobId={jobId} />
                                <ServiceAddressList jobId={jobId}/>
                            </div>
                        </div>
                        <JobButtons disabled={this.state.isLoading} onCancel={this.onCancel} onSave={this.onSaveJob} jobId={jobId}/>
                   </ServicePanel>
                )

            case 'activity':
                return (
                    <JobActivityLog 
                        currentJob={currentJob} 
                        gotoDetailPage={() => this.changeTab('detail')}
                        onLoad={this.getJobActivity}
                    />
                )
            
            case 'email':
                return (
                    <JobEmails 
                    gotoDetailPage={() => this.changeTab('detail')}
                    onLoadMail = {this.getJobSentEmail}
                    />
                )

        } 
    }

    render() {

        return (
            <ContentWrapper>
	            <PageTitle key={'add-order'} title={'Job'}/>
	            <PageContent key={'add-order-content'} >
                    <div className="job-order">
	            	{this.renderJobContent()}
                   </div>
	            </PageContent>
        	</ContentWrapper>
        )
    }
}


ServiceJobsDetails.contextTypes = {
    router: React.PropTypes.object.isRequired
 };

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob,
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceJobsDetails)