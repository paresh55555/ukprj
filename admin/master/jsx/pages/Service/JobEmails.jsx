import React from 'react';
import {connect} from "react-redux";
import ServicePanel from '../../components/Service/ServicePanel';
import {Row, Col,ButtonToolbar, FormControl, Form} from "react-bootstrap";
import ServiceButton from '../../components/Service/ServiceButton';
import MaterialIcon from "material-icons-react";
import SQPopover from '../../components/Service/Jobs/SQPopover';
import FilterByContent from '../../components/Service/Jobs/FilterByContent';
import EmailPreviewComponent from '../../components/Service/Jobs/JobEmail/EmailPreviewComponent';
import SendNewMailDiv from '../../components/Service/Jobs/JobEmail/SendNewMailDiv';
import EmailMassageBox from '../../components/Service/Jobs/JobEmail/EmailMassageBox';
import { ApiSendEmail } from '../../api/ApiServiceJob';

const filterList = [
    {
        "id": 1,
        "title": "Date",
    },
    {
        "id": 2,
        "title": "Unread",
    }
]
class JobEmails extends React.Component {

    state = {
        active : 0,
        isOpen: false,
        mailMassage:{},
        searchTerm: ''
    }

    openEmailModal = () => {
        console.log('emailModal')
        this.setState({
            isOpen:true
        })
    }

    closeEmailModal = () => {
        this.setState({
            isOpen:false
        })
    }

    activeDetail = (index) => {

        this.setState({
            active: index
        })
    }

    onSendEmail = (data) => {
        const {currentJob} = this.props.serviceJob
       return ApiSendEmail(currentJob.id ,data)
    }
 
    componentDidMount(){
        this.props.onLoadMail()
    }

    renderNewMailMassanger = () => {
        if(this.state.isOpen){
            return (<div className='newMessage'>
                        <SendNewMailDiv 
                            close = {this.closeEmailModal} 
                            onEmail = {this.onSendEmail}
                        />
                </div>
            )
        }
        else{
           return ''
        }
    }

    filterEmails = (emails, searchTerm) => {
        return emails.filter(
            email => 
                email.message.toLowerCase().includes(
                    searchTerm.toLowerCase()
                ) || email.subject.toLowerCase().includes(
                    searchTerm.toLowerCase()
                )
        )
    }

    render () {

        const { sendedEmail } = this.props.serviceJob

        let filteredEmails = sendedEmail

        if (this.state.searchTerm) {
            filteredEmails = this.filterEmails(sendedEmail, this.state.searchTerm)
        }

        return(
            <ServicePanel>
                <div className="job_email_container">
                    <div className="JobDetail_Header email_header">
                        <div className='pull-left'>
                            <h4 className=""><MaterialIcon icon="keyboard_arrow_right" />{'Sent Emails'}</h4>
                        </div>
                        <ButtonToolbar className='job_headerBtn' >
                            <ServiceButton  className="btn btn-default cancelbtn" label={<div className='backtoDetail'><MaterialIcon icon="arrow_back" /> <span>Job detail</span></div>} onClick={this.props.gotoDetailPage} />
                        </ButtonToolbar>
                    </div>
                  
                    <div className='search_header'>
                        <Row className={'top-section'}>
                            <Col sm={6}>
                                <FormControl
                                    className={'pull-left orders-search'}
                                    placeholder={"Search"}
                                    value={this.state.searchTerm}
                                    onChange={e => this.setState({
                                        searchTerm: e.target.value
                                    })}
                                />
                                <div className='email_filter pull-left'>
                                    <SQPopover
                                        label={
                                            <em className="fa fa-filter" aria-hidden="true"></em>
                                        }
                                    >
                                     <FilterByContent 
                                        filterList={filterList}
                                     />
                                    </SQPopover>
                                </div>
                            </Col>
                            <Col sm={6}>
                                <ServiceButton className={'pull-right new_massage'} label={'New mail'} onClick={this.openEmailModal}/>
                            </Col>
                        </Row>
                    </div>

                    <div className='email_contentBar'>
                        <div className='email_preview'>
                        {filteredEmails && filteredEmails.map((previewList, index) => {
                            return (
                                <div key={index}>
                                    <EmailPreviewComponent
                                        email={previewList}
                                        onClick={() => this.activeDetail(index, previewList)}
                                        active={index == this.state.active}
                                    />
                                </div>
                            )
                        })}
                        </div>
                       <EmailMassageBox 
                            mail={sendedEmail[this.state.active]}
                       />
                    </div>
                    {this.renderNewMailMassanger()}
                </div>
            </ServicePanel>
        )
    }
}

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob,
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobEmails)