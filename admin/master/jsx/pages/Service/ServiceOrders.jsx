import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import ContentWrapper from "../../components/Layout/ContentWrapper";
import * as ApiService from "../../api/ApiService";
import AddOrderButton from "../../components/Service/Orders/AddOrderButton";
import AddOrder from "../../components/Service/Orders/AddOrder";
import PageContent from "../../components/Layout/PageContent";
import PageTitle from "../../components/Layout/PageTitle";
import {setCurrentOrder, toggleAddOrder} from "../../actions/service";
import UpdateOrder from "../../components/Service/Orders/UpdateOrder";
import ServiceTable from "../../components/Service/ServiceTable";
import {DATE_FORMAT} from "../../components/Common/constants";
import moment from "moment/moment";

const table_fields = {
    order_number:'Order ID',
    warranty_end_date:'Warranty',
    warranty_status: 'Warranty Status',
    doors:'Doors',
    jobs:'Jobs',
    customers:'customers',
    docs:'Docs',
    site:'Site',
}
const defs = [
    {
        "targets": 2,
        "data": "warranty_end_date",
        "render": function ( data, type, row, meta ) {
            if(!row.warranty_end_date)return 'Void';
            let isExpired = !moment(row.warranty_end_date, DATE_FORMAT).isAfter(moment())
            return isExpired ? 'Expired' : 'Active'
        }
    },
]
class ServiceOrders extends Component {

    onClick = (order) =>{
        this.props.dispatch(setCurrentOrder(order))
        this.props.dispatch(toggleAddOrder(true))
    }
    componentWillMount(){
        ApiService.ApiGetOrders();
    }
    
    renderOrderPage = () =>{
        return ([
            <PageTitle key={'add-order'} title={'Orders'}/>,
            <PageContent key={'add-order-content'}>
                {this.props.service.currentOrder ? <UpdateOrder /> : <AddOrder />}
            </PageContent>
        ])
    }
    setColumnsWidth(){
        let width = "12.5%";
        for(let i=0;i<8;i++){ defs.push({ "width": width, "targets": i }) }
    }
    renderOrdersList = () =>{
        this.setColumnsWidth()
        return ([
            <PageTitle key={'orders'} title={'Orders'}/>,
            <PageContent key={'orders-content'}>
                <ServiceTable defs={defs} data={this.props.service.orders} fields={table_fields} onClick={this.onClick} topButton={<AddOrderButton />}/>
            </PageContent>
        ])
    }
    render() {
        return (
            <ContentWrapper>
                {this.props.service.addOrderOpened ?
                    this.renderOrderPage()
                    :
                    this.renderOrdersList()
                }
            </ContentWrapper>
        );
    }
}

ServiceOrders.propTypes = {

};

const mapStateToProps = (state) => ({
    service: state.service
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceOrders)