import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import ContentWrapper from "../../components/Layout/ContentWrapper";
import PageContent from "../../components/Layout/PageContent";
import ServicePanel from "../../components/Service/ServicePanel";
import PageTitle from "../../components/Layout/PageTitle";
import CustomerFrom from "../../components/Service/customers/CustomerFrom"
import * as ApiServiceCustomer from "../../api/ApiServiceCustomer";
import {setServiceCustomers,setCurrentJob} from "../../actions/serviceJob";
import {setCurrentOrder,toggleAddOrder} from "../../actions/service";
import moment from 'moment';
import { setCurrentCustomer } from '../../actions/serviceCustomer';
import { Row, Col } from 'react-bootstrap'
 
class ServiceCustomersDetails extends Component {

    state = {
       customer:{
            first_name:'',
            last_name:'',
            email:'',
            phone:''
       }
    }

    onChangeText = name => e => {
        this.setState({
            customer: {
                ...customer,
                [name]: e.target.value
            }
        })
    }

    componentDidMount(){
        const {customerId} = this.props.params;

        if (customerId !== 'new') {
            ApiServiceCustomer.ApiGetCustomersById(customerId);
            ApiServiceCustomer.ApiGetCustomersOrders(customerId);
            ApiServiceCustomer.ApiGetCustomersJobs(customerId);
        } else {
            this.props.dispatch(setCurrentCustomer({}))
        }

        
    }

    gotoServiceJob = (jobs) => {
        this.context.router.push('service/jobs/'+ jobs.id)
    }
    
    gotoServiceOrder = (order) =>{
        this.props.dispatch(setCurrentOrder(order))
        this.props.dispatch(toggleAddOrder(true))
        this.context.router.push('service/orders')
    }

    addServiceCustomer = (customer = {}) => {


        if(customer.id) {
            ApiServiceCustomer.ApiUpdateServiceCustomer(customer)
                .then(
                    res => swal("Updated!", "Customer is updated.", "success")
                )
        } else {
            ApiServiceCustomer.ApiAddServiceCustomer(customer)
                .then(
                    res => {
                        swal("Success!", "Customer is created.", "success")
                        this.context.router.push('service/customers')
                    }
                )
        }

    }

    onCancel = () => {
        this.context.router.push('service/customers')
    }

    render() {
        const {customerOrders, customerJobs, currentCustomer} = this.props.serviceCustomer
        const {customerId} = this.props.params;

        const customerData = currentCustomer;
        
        const isNewCustomer = this.props.params.customerId == 'new'
         
        return (
            <ContentWrapper>
	            <PageTitle key={'add-order'} title={'Customers'}/>
	            <PageContent key={'add-order-content'} >
                    <div className="job-order">
	            	<ServicePanel>
                    <h4 className="jobfill">{isNewCustomer ? 'Add' : 'Update'} Customer</h4>
                    <Row>
                        <Col sm={12} md={10}>
                            <CustomerFrom 
                                customer={customerData}
                                customerOrders={customerOrders}
                                customerJobs={customerJobs}
                                gotoServiceJob={this.gotoServiceJob}
                                gotoServiceOrder={this.gotoServiceOrder}
                                onSave={this.addServiceCustomer}
                                onCancel={this.onCancel}
                                isNewCustomer={isNewCustomer}
                            />
                        </Col>
                    </Row>
	               </ServicePanel>
                   </div>
	            </PageContent>
        	</ContentWrapper>
        )
    }
}

ServiceCustomersDetails.propTypes = {
    
};

ServiceCustomersDetails.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    serviceCustomer: state.serviceCustomer
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceCustomersDetails)