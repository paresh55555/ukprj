import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import ContentWrapper from "../../components/Layout/ContentWrapper";
import PageContent from "../../components/Layout/PageContent";
import PageTitle from "../../components/Layout/PageTitle";
import * as ApiServiceCustomer from "../../api/ApiServiceCustomer";
import {setServiceCustomers} from "../../actions/serviceCustomer";
import ServiceTable from "../../components/Service/ServiceTable";
import ServiceButton from "../../components/Service/ServiceButton";


const table_fields = {
    name: 'Customer Name',
    email: 'Email',
    phone: 'Phone',
    address: 'Address'
}
class ServiceCustomers extends Component {

    onClick = (data) =>{
        this.context.router.push('Service/customers/' + data.id)
        ApiServiceCustomer.ApiGetCustomersById(data.id)
    }

    createCustomer = () =>{
        this.context.router.push('Service/customers/new')
        this.props.dispatch(setServiceCustomers([]))
    }

    componentWillMount(){
        ApiServiceCustomer.ApiGetCustomers();
    }

    render() {

        const {customers} = this.props.serviceCustomer

        const modfiedCustomers =  customers.map(
            customer => (
                {
                    id : customer.customer_id,
                    email: customer.email,
                    phone:customer.phone,
                    name: `${((customer.first_name && customer.first_name) || '')} ${((customer.last_name && customer.last_name) || '')}`,
                    address: customer.address_1||customer.address_2 + ',' +customer.city + ',' + customer.state + customer.zip
                }
            )
        )

        return (
            <ContentWrapper>
                <PageTitle key={'customers'} title={'Customers'}/>
                <PageContent key={'customers-content'}>
                    <ServiceTable data={modfiedCustomers} fields={table_fields} onClick={(data) => this.onClick(data)}
                                  topButton={<ServiceButton bsStyle={'primary'} className={'pull-right'} label={'Create Customer'} onClick={this.createCustomer}/>}/>
                </PageContent>
            </ContentWrapper>
        );
    }
}

ServiceCustomers.propTypes = {

};

ServiceCustomers.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    serviceCustomer: state.serviceCustomer
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceCustomers)