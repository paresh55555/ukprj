import React, {Component, PropTypes} from 'react';
import {connect} from "react-redux";
import { Router, useRouterHistory, Route, Link, hashHistory, browserHistory, IndexRoute } from 'react-router';
import ContentWrapper from "../../components/Layout/ContentWrapper";
import PageContent from "../../components/Layout/PageContent";
import PageTitle from "../../components/Layout/PageTitle";
import * as ApiServiceJob from "../../api/ApiServiceJob";
import ServiceTable from "../../components/Service/ServiceTable";
import PersonAddress from "../../components/Service/Purchasers/PersonAddress";
import ReactDOMServer from "react-dom/server";
import {setCurrentJob, setServiceJobs} from "../../actions/serviceJob";
import ServiceTech from "../../components/Service/Jobs/ServiceTech";
import AddJobsButton from "../../components/Service/Jobs/AddJobsButton";
import ServiceJobsDetails from './ServiceJobsDetails';


const table_fields = {
    job_number:'Job ID',
    start_date:'Start Date',
    start_time:'Start Time',
    tech_name:'Tech',
    customer: 'Customer',
    customer_phone:'Customer Phone',
    service_address:'Service Address',
    job_status: 'Job Status',
}

const defs = [
    {
        "targets": 0,
        "data": "",
        "render": function ( data, type, row, meta ) {
            return `<span>${ReactDOMServer.renderToString(<em style={{fontSize:'16px',paddingRight:'7px',color:'#b4b4b4'}} className="fa fa-paperclip"></em>)} ${row.job_number}</span>`
        }
    },
    {
        "targets": 4,
        "data": "customer",
        "render": function ( data, type, row, meta ) {
            return row.customer_first_name + ' ' + row.customer_last_name
        }
    },
    
    {
        "targets": 6,
        "data": null,
        "render": function ( data, type, row, meta ) {

            // api was not giving exact schema as needed in frontend.
            const {
                address_address_1,
                address_address_2,
                address_city,
                address_state,
                address_zip
            } = row;

            const customer = {
                address_1: address_address_1,
                address_2: address_address_2,
                city: address_city,
                state: address_state,
                zip: address_zip
            }

            return ReactDOMServer.renderToString(<PersonAddress info={customer} />);
        }
    }
]
class ServiceJobs extends Component {

    onClick = (job) =>{
        this.props.dispatch(setCurrentJob(job))
        this.context.router.push('Service/jobs/'+job.id)
    }

    openJobForm = () => {
        this.props.dispatch(setCurrentJob({}))
        this.context.router.push('Service/jobs/new')
    }

   
    componentWillMount(){
        ApiServiceJob.ApiGetJobs();

    }

    renderJobsPage = () =>{
        return (       
            <ServiceJobsDetails  />      
        )
    }

    renderJobsList = () =>{

        return (
            <ContentWrapper>
                 <PageTitle key={'jobs'} title={'Jobs'}/>
                    <PageContent key={'jobs-content'}>
                        <ServiceTable defs={defs} data={this.props.serviceJob.jobs} fields={table_fields} onClick={this.onClick} />
                    </PageContent>
            </ContentWrapper>
        )
    }

    render() {

        return (
            <div>
                { this.renderJobsList()}
            </div>
        );
    }
}

ServiceJobs.propTypes = {

};

ServiceJobs.contextTypes = {
   router: React.PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(ServiceJobs)