import React from 'react';
import {connect} from "react-redux";
import ServicePanel from '../../components/Service/ServicePanel';
import {Col,ButtonToolbar, FormControl, Form} from "react-bootstrap";
import MaterialIcon from "material-icons-react";
import ServiceButton from '../../components/Service/ServiceButton';
import ServiceFormRow from '../../components/Service/ServiceFormRow';
import JobStatus from '../../components/Service/Jobs/JobStatus';
import ActivityList from '../../components/Common/ActivityLog/ActivityList';

class JobActivityLog extends React.Component {

    componentDidMount(){
        this.props.onLoad()
    }

    render () {

        const { currentJob } = this.props;
        const { currentJobActivity } = this.props.serviceJob;

        return(
            <ServicePanel>
                <div className="jobActivityContainer">
                    <div className="JobDetail_Header activity">
                        <ButtonToolbar className='job_headerBtn' >
                            <ServiceButton  className="btn btn-default cancelbtn" label={<div className='backtoDetail'><MaterialIcon icon="arrow_back" /> <span>Job detail</span></div>} onClick={this.props.gotoDetailPage} />
                        </ButtonToolbar>
                    </div>
                    <Form className={'add-order'}>
                        <ServiceFormRow label={'Job ID & Order ID'}>
                            <Col sm={6} className={'no-pd pull-left innerJob'}><FormControl onChange={() => {}} name={'job_number'} value={currentJob.job_number}  disabled/></Col>
                            <Col sm={6} className={'no-pd pull-right innerJob'}> <FormControl onChange={() => {}} name={'order_number'} value={currentJob.order_number}  disabled/>  </Col>  
                        </ServiceFormRow>
                        <ServiceFormRow label={'Status'}>   
                            <JobStatus
                                selected={currentJob.job_status}
                                disabled={true}
                            />
                        </ServiceFormRow>
                        <ActivityList
                            heading={'Recent Activity'}
                            logs={currentJobActivity}
                        />
                    </Form>
                </div>
            </ServicePanel>
        )
    }
}

const mapStateToProps = (state) => ({
    serviceJob: state.serviceJob,
})

const mapDispatchToProps = dispatch => ({
    dispatch
})

export default connect(mapStateToProps, mapDispatchToProps)(JobActivityLog)