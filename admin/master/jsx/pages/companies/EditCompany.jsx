import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router , Route, browserHistory } from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, FormControl, DropdownButton, MenuItem, Modal } from 'react-bootstrap';
import * as constant from '../../constant.js'
import {companyAction} from '../../actions';
import {NotifyAlert} from '../../components/Common/notify.js'
import DeleteDialog from '../../components/Common/DeleteDialog'
import swal from 'sweetalert'

import {ApiCompany} from '../../api';

let form = ''
var self='';
var handle=0;

class EditCompany extends React.Component {
    constructor() {
      super();
      self=this;
      this.state = {company:{}, title:'Edit Company', showLoader:true, edited:false,saving:false, showDelete:false,users:[]}
      this._handleChange = this._handleChange.bind(this);
      this.routerWillLeave = this.routerWillLeave.bind(this);
      this._saveCompany = this._saveCompany.bind(this);
      this._deleteCompany = this._deleteCompany.bind(this);
      this.gotoUser = this.gotoUser.bind(this)
     } 
     componentWillMount() {
        const {ApiGetCompanyRawData, ApiGetCompanyData, ApiGetCompanyUserData} = this.props;
        var self = this;
        const company_id = this.props.companyId;
        if(company_id == 'new'){
            ApiGetCompanyRawData(this.props.dispatch)
            self.setState({title:'Add Company'})
        }
        else{
            self.setState({title:'Edit Company', showDelete:true});
            this.props.ApiGetCompanyData(company_id)
            this.props.ApiGetCompanyUserData(company_id);
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.company.detail != this.props.company.detail){
          self.setState({company : nextProps.company.detail});
        }
        if(nextProps.company.usersList != this.props.company.usersList){
          self.setState({users : nextProps.company.usersList});
        }
        if(nextProps.company.isSaved != this.props.company.isSaved && nextProps.company.isSaved){
            if(this.props.companyId === 'new'){
                NotifyAlert('Company added successfully');
            }
            else{
                NotifyAlert('Company updated successfully');
            }
            self.context.router.push(constant.SQ_PREFIX+'companies');
        }
        if(nextProps.company.isDeleted != this.props.company.isDeleted && nextProps.company.isDeleted){
            swal("Deleted!",
                      this.state.company.companyName+" has been deleted.",
                       "success");
                 self.setState({edited:false})
                self._backtoCompany();
        }
     }




    componentDidMount() {
        const { route } = this.props;
        const { router } = this.context;
        // router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
      // return false to prevent a transition w/o prompting the user,
      // or return a string to allow the user to decide:
      if (this.state.edited)
        return 'Your work is not saved! Are you sure you want to leave?'
    }
    _handleChange(value, e){
        var company = this.state.company;
        company[value] = e.target.value;
        this.setState({company:company, edited:true})
    }
    _backtoCompany(){
        self.props.onClose();
    }
    gotoUser(id){
     self.context.router.push(constant.SQ_PREFIX+'user/'+id);
    }
    _saveCompany(e){
        e.preventDefault();
        const {company} = this.state;
        if($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0}) && !this.state.dublicateError && !this.state.checking){
            this.setState({edited:false});
            if(this.props.companyId === 'new'){
                this.props.ApiSaveCompany(company)
            }
            else{
                this.props.ApiUpdateCompany(company)
            }
        }
    }
    _deleteCompany(){
        const { company } = this.state;
        this.setState({edited:false})
        var self=this;
        DeleteDialog({
            text: "Are you sure you want to delete this company? ",
            onConfirm: function(){
                self.props.ApiDeleteCompany(company.id);
            }
        })
    }
    
    render() {
        return (
            <ContentWrapper>
                <h3>{this.state.title} </h3>
                { /* START row */ }
                <Row>
                  <Col sm={ 12 }>
                        { /* START panel */ }
                        <Panel header="">
                           {this.props.company.isLoading ? <div className="ball-pulse">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div> :
                            <form className="form-vertical" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                                <div className="form-group">
                                    <label className=" control-label">Company Name</label>
                                    <FormControl   type="text" placeholder="Company Name" className="form-control" value={this.state.company.companyName} onChange={this._handleChange.bind(this, 'companyName')} required />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">First Name</label>
                                    <FormControl   type="text" placeholder="First Name" className="form-control" value={this.state.company.firstName} onChange={this._handleChange.bind(this, 'firstName')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">Last Name</label>
                                    <FormControl   type="text" placeholder="Last Name" className="form-control" value={this.state.company.lastName} onChange={this._handleChange.bind(this, 'lastName')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">Phone</label>
                                    <FormControl   type="text" pattern="\d*" placeholder="Phone" className="form-control" value={this.state.company.phone} onChange={this._handleChange.bind(this, 'phone')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">Address 1</label>
                                    <FormControl   type="text" placeholder="Address 1" className="form-control" value={this.state.company.address1} onChange={this._handleChange.bind(this, 'address1')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">Address 2</label>
                                    <FormControl   type="text" placeholder="Address 2" className="form-control" value={this.state.company.address2} onChange={this._handleChange.bind(this, 'address2')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">City</label>
                                    <FormControl   type="text" placeholder="City" className="form-control" value={this.state.company.city} onChange={this._handleChange.bind(this, 'city')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">State</label>
                                    <FormControl   type="text" placeholder="State" className="form-control" value={this.state.company.state} onChange={this._handleChange.bind(this, 'state')} />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">Zip</label>
                                    <FormControl   type="text" pattern="\d*" placeholder="Zip" className="form-control" value={this.state.company.zip} onChange={this._handleChange.bind(this, 'zip')} />
                                </div>
                                <div className="form-group">
                                    <Button disabled={this.props.company.isSaving} type="button" bsStyle="primary" bsSize="small" onClick={this._saveCompany}>Save</Button>
                                    <Button type="button" className="mh" bsStyle="default" bsSize="small" onClick={this._backtoCompany}>Cancel</Button>
                                    {this.state.showDelete ? <Button type="button" className="pull-right" bsStyle="danger" bsSize="small" onClick={this._deleteCompany}>Delete</Button> : null }
                                </div>
                            </form> }
                        </Panel>
                        { /* END panel */ }
                    </Col>
                    {/*<Col sm={4} smOffset={2}>
                                            <div className="list-group mb0">
                                            {this.state.users.map(function(user){
                                              let gotoUser = self.gotoUser.bind(this, user.id)
                                             return <a onClick={gotoUser} className="list-group-item" key={user.id}>{user.name} <em className="pull-right fa fa-chevron-circle-right"></em></a>  
                                            })
                                          }
                                            </div>
                                        </Col>*/}
                </Row>
                { /* END row */ }
            </ContentWrapper>
            );
    }

}

EditCompany.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, companyAction, ApiCompany, dispatch);
    return allActionProps;
}

// Notice mapDispatch is in the same block as the state, if added after the parentheses,
// the functions get wrapped with the dispatch call but you no longer have dispatch attached to the props.

export default connect(state => ({
    company: state.company
}, mapDispatch))(EditCompany);


