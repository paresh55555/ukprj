import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, Table, Alert, Modal } from 'react-bootstrap';
import DataTable from '../../components/datatable/DataTable.jsx'
import * as constant from '../../constant.js';
import {companyAction} from '../../actions';
import EditCompany from './EditCompany';

import {ApiCompany} from '../../api';

var companyTable ='';

class CompaniesList extends React.Component {
    constructor() {
      super();
      self= this;
      this.state = {
        editModal: false,
        editingCompanyId: null
      };

      this.showEditModal = this.showEditModal.bind(this);
      this.hideEditModal = this.hideEditModal.bind(this);
      this.editCompany = this.editCompany.bind(this);
      this.onEditModalClose = this.onEditModalClose.bind(this);
      this.newUser = this.newUser.bind(this);
    }

    componentWillMount(){
        const {ApiGetCompanies} = this.props;

        ApiGetCompanies();
    }

    showEditModal () {
        this.setState({
            editModal: true
        })
    }

    hideEditModal () {
        this.setState({
            editModal: false
        })
    }

    editCompany (data) {
        this.setState({
            editingCompanyId: data.id
        })
        this.showEditModal();
    }

    onEditModalClose () {
        this.hideEditModal();
        this.props.ApiGetCompanies();
                                
    }

    newUser () {
        this.editCompany({
            id: 'new'
        })
    }

    render() {
        var self = this;

        const {editModal, editingCompanyId} = this.state;

        return (
            <ContentWrapper>
                <h3>Companies List</h3>
                <Grid fluid>
                    <Row>
                        <Col lg={ 12 }>
                            <a onClick={this.newUser} className="mb mv btn btn-labeled btn-info mr pull-right">
                              <span className="btn-label"><i className="fa fa-plus"></i></span>Add Company  
    	                    </a>
    	                    <div className="clearfix"></div>
                            <Panel>
                                <DataTable id="companiesTable" onClick={this.editCompany} data={this.props.company.list} isLoading={this.props.company.isLoading} />
                            </Panel>
                            <Modal show={editModal} onHide={this.hideEditModal}>
                                <EditCompany companyId={editingCompanyId} onClose={this.onEditModalClose}/>
                            </Modal>
                         </Col>
                    </Row>
                </Grid>
            </ContentWrapper>
        );
    }

}

CompaniesList.contextTypes = {
  router: React.PropTypes.object.isRequired
};

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, companyAction, ApiCompany, dispatch);
    return allActionProps;
}

// Notice mapDispatch is in the same block as the state, if added after the parentheses,
// the functions get wrapped with the dispatch call but you no longer have dispatch attached to the props.

export default connect(state => ({
    company: state.company
}, mapDispatch))(CompaniesList);

