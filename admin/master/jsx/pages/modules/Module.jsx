import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import ModuleBox from '../../components/modulebox/ModuleBox.jsx'
import { compose, withProps } from 'recompose';
import { ApiModules } from '../../api';

class Module extends React.Component {
	constructor() {
      super();
     } 
    componentWillMount() {
        this.props.ApiGetModulesList();
    }

    render() {
    	const ModulesList = this.props.module.list.map(function (module, i) {
    		return(
    				<ModuleBox link={'modules/'+module.id} url={module.url} title={module.title} key={i} />
    			)
    	})
        return (
            <ContentWrapper>
                <h3>Modules</h3>
                <Grid fluid>
                    <Row>
                    {this.props.module.isLoading ? <div className="ball-pulse">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div> :
                   		ModulesList}
                    </Row>
                </Grid>
            </ContentWrapper>
            );
    }

}

// export default Module;

Module.propTypes = {
    ApiGetModulesList: PropTypes.func.isRequired
};

const apiActions = Object.assign({}, ApiModules);

export default compose(
    connect(state => ({
        module: state.module
    })),
    withProps(apiActions),
)(Module);