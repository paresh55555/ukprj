import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, FormControl } from 'react-bootstrap';
import * as constant from '../../constant.js'
import {NotifyAlert} from '../../components/Common/notify.js'
import { compose, withProps } from 'recompose';
import { ApiModules } from '../../api';

var type = 'size',
	id='';
class EditMinMax extends React.Component {

    constructor() {
      super();
      this.state = {range:{}, widtherror:'', heighterror:'',edited:false,minheighterror:'',minwidtherror:''};
      this._handleChange = this._handleChange.bind(this);
      this._saveRange = this._saveRange.bind(this);
      this._backtoPanel = this._backtoPanel.bind(this);
      this.routerWillLeave = this.routerWillLeave.bind(this);
     } 
     componentWillMount() {
     	var self = this;
     	id = this.props.routeParams.id;
            this.props.ApiGetMinMaxData(id);
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.module.min_max != this.props.module.min_max){
            this.setState({range:nextProps.module.min_max})
        }
        if(nextProps.module.isSaved != this.props.module.isSaved && nextProps.module.isSaved){
            NotifyAlert('Min / Max sizes updated successfully');
            this._backtoPanel();
        }
     }
    componentDidMount(){
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
      // return false to prevent a transition w/o prompting the user,
      // or return a string to allow the user to decide:
      if (this.state.edited)
        return 'Your work is not saved! Are you sure you want to leave?'
    }
    _handleChange(value, e){
        var range = this.state.range;
        range[value] = e.target.value;
        this.setState({range:range,edited:true})
        if(value=='maxHeight' && parseFloat(e.target.value)<this.state.range.minHeight){
            this.setState({heighterror:'Maximum height should be greater than minimum height'})
        }
        else if(value=='minHeight' && parseFloat(e.target.value)>this.state.range.maxHeight){
            this.setState({minheighterror:'',heighterror:'Maximum height should be greater than minimum height'})
        }
        else if(value=='minHeight'  || value=='maxHeight'){
            this.setState({heighterror:'',minheighterror:''})
        }
        if(value=='maxWidth' && range["minWidth"]>parseFloat(e.target.value)){
            this.setState({widtherror:'Maximum width should be greater than minimum width'})
        }
        else if(value=='minWidth' && parseFloat(e.target.value)>=range["maxWidth"]){
            this.setState({minwidtherror:'',widtherror:'Maximum width should be greater than minimum width'})
        }
        else if(value=='minWidth' || value=='maxWidth'){
            this.setState({widtherror:'',minwidtherror:''})   
        }
    }
    _saveRange(e){
        e.preventDefault();
        var self = this;
        var range= this.state.range;
        if($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0}))
        {
            if(parseFloat(range["minWidth"])<=parseFloat(range["maxWidth"]) && parseFloat(range["minHeight"]) <= parseFloat(range["maxHeight"])){
                    self.setState({edited:false});
                    this.props.ApiUpdateMinMax(range, id)
            }
            else{
                if(range["minWidth"]>=range["maxWidth"]){
                    this.setState({widtherror:'Maximum width should be greater than minimum width'})
                }
                if(range["minHeight"]>=range["maxHeight"]){
                    this.setState({heighterror:'Maximum height should be greater than minimum height'})
                }
            }
        }
    }
    _backtoPanel(){
            this.context.router.push(constant.SQ_PREFIX+'modules/'+id)
        }
    render() {
        
        return (
            <ContentWrapper>
                <h3>Edit Min/Max Sizes </h3>
                { /* START row */ }
                <Row>
                  <Col sm={ 6 }>
                        { /* START panel */ }
                        <Panel header="">
                        
                           {this.props.module.isLoading ? <div className="ball-pulse">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div> :
                            <form  className="form-vertical" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                                <div className="form-group">
			                        <label className=" control-label">Min Width</label>
			                        <FormControl   type="text" placeholder="Min Width" className={this.state.minwidtherror ? "form-control cerror" : "form-control" } value={this.state.range.minWidth} onChange={this._handleChange.bind(this, 'minWidth')} required />
                                    {this.state.minwidtherror ? <p className='cterror'>{this.state.minwidtherror}</p>:null}
			                    </div>
			                    <div className="form-group">
			                        <label className=" control-label">Max Width</label>
			                        <FormControl   type="text" placeholder="Max Width" className={this.state.widtherror ? "form-control cerror" : "form-control" } value={this.state.range.maxWidth} onChange={this._handleChange.bind(this, 'maxWidth')} required />
                                    {this.state.widtherror ? <p className='cterror'>{this.state.widtherror}</p>:null}

			                    </div>
			                    <div className="form-group">
			                        <label className=" control-label">Min Height</label>
			                        <FormControl   type="text" placeholder="Min Height" className={this.state.minheighterror ? "form-control cerror" : "form-control" } value={this.state.range.minHeight} onChange={this._handleChange.bind(this, 'minHeight')} required />
                                    {this.state.minheighterror ? <p className='cterror'>{this.state.minheighterror}</p>:null}
			                    </div>
			                    <div className="form-group">
			                        <label className=" control-label">Max Height</label>
			                        <FormControl   type="text" placeholder="Max Height" className={this.state.heighterror ? "form-control cerror" : "form-control" } value={this.state.range.maxHeight} onChange={this._handleChange.bind(this, 'maxHeight')} required />
                                    {this.state.heighterror ? <p className='cterror'>{this.state.heighterror}</p>:null}
			                    </div>

                                <div className="form-group">
                                        <Button disabled={this.props.module.isSaving} type="button" bsStyle="primary" bsSize="small" onClick={this._saveRange}>Save</Button>
                                        <Button type="button" className="mh" bsStyle="default" bsSize="small" onClick={this._backtoPanel}>Cancel</Button>
                                </div>
                            </form>}
                        </Panel>
                        { /* END panel */ }
                    </Col>
                </Row>
                { /* END row */ }
               
            </ContentWrapper>
            );
    }

}

EditMinMax.contextTypes = {
    router: React.PropTypes.object.isRequired
};
EditMinMax.propTypes = {
    ApiGetMinMaxData: PropTypes.func.isRequired,
    ApiUpdateMinMax: PropTypes.func.isRequired
};

const apiActions = Object.assign({}, ApiModules);

export default compose(
    connect(state => ({
        module: state.module
    })),
    withProps(apiActions),
)(EditMinMax);