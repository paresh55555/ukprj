import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {moduleAction} from '../../actions';
import LinkBox from '../../components/linkbox/LinkBox'
import { compose, withProps } from 'recompose';
import { ApiModules } from '../../api';

let module_id = ''
class ModuleLanding extends React.Component {
	constructor() {
      super();
     } 
    componentWillMount() {
        module_id = this.props.params.id
        this.props.ApiGetModuleDetails(module_id);
    }

    render() {
        const {listDetail} = this.props.module;
        return (
            <ContentWrapper>
                <h3>{listDetail.title}</h3>
                <Grid fluid>
                    <Row>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'/modules/'+module_id+'/panelRanges'} title={'Panel Ranges'} />
                        </Col>
                        <Col md={2} sm={4}> 
                        <LinkBox url={'/modules/'+module_id+'/glass'} title={'Glass Formulas'} />
                        </Col>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'/modules/'+module_id+'/fixed'} title={'Fixed Formulas'} />
                        </Col>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'/modules/'+module_id+'/frame'} title={'Frame Formulas'} />
                        </Col>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'/modules/'+module_id+'/percent'} title={'Percentage Formulas'} />
                        </Col>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'/module/size/'+module_id} title={'Min / Max sizes'} />
                        </Col>                        
                    </Row>
                </Grid>
            </ContentWrapper>
            );
    }

}

ModuleLanding.propTypes = {
    ApiGetModuleDetails: PropTypes.func.isRequired
};

const apiActions = Object.assign({}, ApiModules);

export default compose(
    connect(state => ({
        module: state.module
    })),
    withProps(apiActions),
)(ModuleLanding);