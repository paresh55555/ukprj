import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, Table } from 'react-bootstrap';
import DataTable from '../../components/datatable/DataTable.jsx'
import {getModuleTitle} from '../../services/moduleService.js';
import { compose, withProps } from 'recompose';
import { ApiModules } from '../../api';

let id ='',
    type= '',
    title='',
    moduleTable = '';
class PanelRange extends React.Component {
    constructor() {
      super();
      self= this;
     } 
    componentWillMount() {
        id = this.props.routeParams.id;
        type = this.props.routeParams.type;
        this.props.ApiModuleTableList(id, type);
        title = getModuleTitle(type);
    }

    render() {
        return (
            <ContentWrapper>
                <h3>{title}</h3>
                <Grid fluid>
                    <Row>
                        <Col lg={ 12 }>
                            <Link to={'/modules/'+id+'/'+type+'/new'} className="mb mv btn btn-labeled btn-info mr pull-right">
                                <span className="btn-label"><i className="fa fa-plus"></i></span>Add {title}
    	                    </Link>
    	                    <div className="clearfix"></div>
                            <Panel>
                                <DataTable id={'Table'+type} link={'modules/'+id+'/'+type} data={this.props.module.table_data} isLoading={this.props.module.isLoading} />
                            </Panel>
                         </Col>
                    </Row>
                </Grid>
            </ContentWrapper>
            );
    }

}

PanelRange.contextTypes = {
    router: React.PropTypes.object.isRequired
};
PanelRange.propTypes = {
    ApiModuleTableList: PropTypes.func.isRequired
};

const apiActions = Object.assign({}, ApiModules);

export default compose(
    connect(state => ({
        module: state.module
    })),
    withProps(apiActions),
)(PanelRange);