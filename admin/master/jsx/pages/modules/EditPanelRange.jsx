import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import {getPanelObject, getModuleTitle} from '../../services/moduleService.js';
import { Grid, Row, Col, Panel, Button, FormControl, Modal } from 'react-bootstrap';
import swal from 'sweetalert';
import * as constant from '../../constant.js'
import {NotifyAlert} from '../../components/Common/notify.js'
import DeleteDialog from '../../components/Common/DeleteDialog'
import { compose, withProps } from 'recompose';
import { ApiModules } from '../../api';

let range_id ='',
    id = '',
    type = ''

class EditPanelRange extends React.Component {

    constructor() {
      super();
      self = this;
      var arr=['costItem','panels','low','high', 'percentage', 'option_type', 'cost_meter', 'cost_paint_meter', 'waste_percent', 'forCutSheet', 'burnOff', 'myOrder', 'costPerPanel', 'costSquareMeter'];
      this.state = {minmaxerror:'',panelConut:0,costerror:[],showLoader:true, range:{},type:'Edit', title:'', showDelete:false, showDeleteModal:false,numbertype:arr,edited:false,saving:false};
      this._handleChange = this._handleChange.bind(this);
      this._saveRange = this._saveRange.bind(this);
      this._deleteRange = this._deleteRange.bind(this);
      this._backtoPanel = this._backtoPanel.bind(this);
      this.showDelete = this.showDelete.bind(this);
      this.closeDelete = this.closeDelete.bind(this);
      this.routerWillLeave = this.routerWillLeave.bind(this);
     } 
     componentWillMount() {
        range_id = this.props.routeParams.pid;
        id = this.props.routeParams.id;
        type = this.props.routeParams.type;
        self.setState({title:getModuleTitle(type)})
        if(range_id == 'new'){
            this.props.ApiGetRawPanelRange(type, id)
            self.setState({type:'Add'})
        }
        else{
            self.setState({type:'Edit',showDelete:true})
            this.props.ApiGetModuleData(id,range_id, type)
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.module.detail != this.props.module.detail){
            this.setState({range:nextProps.module.detail})
        }
        if(nextProps.module.isSaved != this.props.module.isSaved && nextProps.module.isSaved){

            if(this.state.type=='Add'){
                NotifyAlert(this.state.title+' created successfully');
            }
            else{
                
                NotifyAlert(this.state.title+' updated successfully');
            }

            this._backtoPanel();
        }
        if(nextProps.module.isDeleted != this.props.module.isDeleted && nextProps.module.isDeleted){
            const {title} = this.state;
            const panel_object = this.state.range[getPanelObject(type)]
            swal("Deleted!", panel_object+' '+title+" has been deleted.","success");
            this._backtoPanel();
        }
        if(nextProps.module.saveError != this.props.module.saveError && nextProps.module.saveError){
            self.setState({panelConut:nextProps.module.error_msg,saving:false})
        }
     }
    componentDidMount(){
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
      // return false to prevent a transition w/o prompting the user,
      // or return a string to allow the user to decide:
      if (this.state.edited)
        return 'Your work is not saved! Are you sure you want to leave?'
    }
    generateInputFields(){
        var jsonData = this.state.range;
        var field = []
         var i = 0;
        for(var obj in jsonData){
            if(obj != 'id' && obj != 'module_id'){
                var title = obj.replace(/([A-Z])/g, ' $1').trim();
                var title = title.replace(/_/g, ' ').trim().toLowerCase();
                field.push(
                    <div key={i} className="form-group">
                        <label className=" control-label">{title}</label>
                        {this.state.numbertype.indexOf(obj) > -1 ? 
                            obj == 'percentage' ?
                            <FormControl 
                                  
                                type="text" 
                                placeholder={title} 
                                className={this.state.costerror.indexOf(obj)>=0 ? "form-control cerror" : "form-control" } 
                                value={this.state.range[obj]} 
                                onChange={this._handleChange.bind(this, obj)} 
                                data-parsley-type="number"
                                data-parsley-max="100"
                            />
                            :<FormControl 
                                  
                                type="text" 
                                placeholder={title} 
                                className={this.state.costerror.indexOf(obj)>=0 ? "form-control cerror" : "form-control" } 
                                value={this.state.range[obj]} 
                                onChange={this._handleChange.bind(this, obj)} 
                                disabled={type== 'panelRanges' && this.state.type=="Edit" && obj == 'panels'}
                            
                                pattern="[0-9]+([,\.][0-9]+)?"
                            />
                            :<FormControl 
                                  
                                type="text" 
                                placeholder={title} 
                                className={this.state.costerror.indexOf(obj)>=0 ? "form-control cerror" : "form-control" } 
                                value={this.state.range[obj]} 
                                onChange={this._handleChange.bind(this, obj)} 
                                required
                            />
                        }
                         {obj=='panels' && this.state.panelConut ? <p className='cterror'>{this.state.panelConut} </p>:null}
                        {obj=='high' && this.state.minmaxerror ? <p className='cterror'>{this.state.minmaxerror}</p>:null}
                    </div>
                        )
            }    
            i++;
        }
        return field
    }
    _handleChange(value, e){
        var range = this.state.range;
       
        if(value=='panels' && e.target.value.toString().indexOf('.')>=0){
            this.setState({panelConut:'It should be numeric'})
        }
        else if(value=='panels'){
            this.setState({panelConut:''})
        }
        range[value] = e.target.value;
        this.setState({range:range,edited:true})
    }
    _saveRange(e){
        e.preventDefault();
        self = this;
        var range= this.state.range;
        if($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0}) && this.state.costerror.length < 1)
        {
            if(range["low"] != undefined){
                if(parseInt(range["low"])>=parseInt(range["high"])){
                   return this.setState({minmaxerror:'High should be greater than low'})
                }
            }
                if(this.state.range.panels && this.state.range.panels.toString().indexOf('.') > 0){
                    return;
                }
                    self.setState({edited:false});
                  if(this.props.routeParams.pid === 'new'){
                       this.props.ApiSaveModuleLayout(range, id, type)
                    }
                    else{
                        this.props.ApiUpdateModuleLayout(range, id, type)
                    }
                    this.setState({minmaxerror:''});
                
            }
        
    }
    _deleteRange(){
        this.setState({showDeleteModal:false})
        this.props.ApiDeleteModuleLayout(range_id, id, type).then(function() {
            browserHistory.push(constant.SQ_PREFIX+'modules/'+id+'/'+type)
        })
    }
    showDelete(){
        const {title} = this.state;
        const panel_object = this.state.range[getPanelObject(type)]
        var self = this;
        this.setState({edited:false})
        DeleteDialog({
            text: "Are you sure you want to delete "+panel_object+" from "+title+"? ",
            onConfirm: function(){
                self.props.ApiDeleteModuleLayout(range_id, id, type)
            }
        })
    }
    closeDelete(){
        this.setState({showDeleteModal:false})
    }
    _backtoPanel(){
            this.context.router.push(constant.SQ_PREFIX+'modules/'+id+'/'+type)
        }
    render() {

        return (
            <ContentWrapper>
                <h3>{this.state.type +' '+ this.state.title} </h3>
                { /* START row */ }
                <Row>
                  <Col sm={ 6 }>
                        { /* START panel */ }
                        <Panel header="">
                        
                           {this.props.module.isLoading ? <div className="ball-pulse">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div> :
                            <form  className="form-vertical" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                                {this.generateInputFields()}
                                <div className="form-group">
                                        <Button disabled={this.props.module.isSaving} type="button" bsStyle="primary" bsSize="small" onClick={this._saveRange}>Save</Button>
                                        <Button type="button" className="mh" bsStyle="default" bsSize="small" onClick={this._backtoPanel}>Cancel</Button>
                                        {this.state.showDelete ? <Button type="button" className="pull-right" bsStyle="danger" bsSize="small" onClick={this.showDelete}>Delete</Button> : null}
                                </div>
                            </form>}
                        </Panel>
                        { /* END panel */ }
                    </Col>
                </Row>
                { /* END row */ }
               <Modal bsSize="small" show={this.state.showDeleteModal} onHide={this.close}>
                  
                  <Modal.Body>
                    <p>Are you sure ?</p>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button bsStyle="primary" onClick={this._deleteRange}>Yes</Button>
                    <Button onClick={this.closeDelete}>Cancel</Button>
                  </Modal.Footer>
                </Modal>
            </ContentWrapper>
            );
    }

}

EditPanelRange.contextTypes = {
    router: React.PropTypes.object.isRequired
};
EditPanelRange.propTypes = {
    ApiGetModuleData: PropTypes.func.isRequired,
    ApiGetRawPanelRange: PropTypes.func.isRequired,
    ApiUpdateModuleLayout: PropTypes.func.isRequired,
    ApiSaveModuleLayout: PropTypes.func.isRequired,
    ApiDeleteModuleLayout:PropTypes.func.isRequired
};

const apiActions = Object.assign({}, ApiModules);

export default compose(
    connect(state => ({
        module: state.module
    })),
    withProps(apiActions),
)(EditPanelRange);