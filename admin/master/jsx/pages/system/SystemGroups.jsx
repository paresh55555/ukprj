import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import Loader from '../../components/loader/Loader';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {API_URL} from '../../constant.js';
import {systemAction, componentAction} from '../../actions';
import SystemGroupTitle from './SystemGroupTitle'
import SystemComponent from './SystemComponent'
import {NotifyAlert} from '../../components/Common/notify.js'
import {ApiSystems} from '../../api';

class SystemGroups extends React.Component {
    constructor() {
        super();
        this.state = {
            action: '',
            published:false,
        }
        this.addSystemGroup = this.addSystemGroup.bind(this);
        this.publishSystemGroup = this.publishSystemGroup.bind(this);
        this.routerWillLeave = this.routerWillLeave.bind(this);
    }
    componentWillMount() {
        ApiSystems.getSystemGroupsList();
    }
    componentDidMount(){
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
      // return false to prevent a transition w/o prompting the user,
      // or return a string to allow the user to decide:
      if (this.props.component.edit_mode)
        return 'Your work is not saved! Are you sure you want to leave?'
    }
    componentWillUnmount(){
        this.props.setEditMode(false)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.system.isSaved != this.props.system.isSaved && nextProps.system.isSaved ||
            nextProps.system.isDeleted != this.props.system.isDeleted && nextProps.system.isDeleted ){
            ApiSystems.getSystemGroupsList();
        }
        if(nextProps.system.isPublished && this.state.published){
            NotifyAlert(`"System Groups" published successfully`);
            this.setState({published:false})
        }
    }

    addSystemGroup(){
        ApiSystems.saveSystemGroup();
        this.setState({action:'add'});
    }

    publishSystemGroup(){
        this.setState({published:true})
        ApiSystems.publishSystemGroup();
    }

    render() {
        var self = this;
    	const GroupsList = this.props.system.systemGroups.length ?
            this.props.system.systemGroups.map(function (group, i) {
                let systems = [],
                    splittedSystems = [];
                group.systems.map((g,i)=>{
                    systems.push(g);
                });
                while(systems.length){
                    splittedSystems.push(systems.splice(0,3));
                }
                return ([
                    <SystemGroupTitle component={group} traits={group.traits} cid={group.id}
                                      edit_mode={self.props.component.edit_mode} setEditMode={self.props.setEditMode}
                                      key={i}/>,
                    <div className="optionsBox">
                        {splittedSystems.map((s,i)=>{
                            return ([
                                <div className="fullOption">
                                    {s.map((system, idx)=>{
                                        return <SystemComponent component={system} traits={system.traits} cid={system.id}
                                                                edit_mode={self.props.component.edit_mode} setEditMode={self.props.setEditMode}
                                                                key={idx}/>
                                    })}
                                </div>
                            ])
                        })}
                    </div>
                ])
            })
            :
            'No system groups available'
        return (
            <ContentWrapper>
                <h3>Active Systems</h3>
                <div className="panel-default panel mainBox">
                    {this.props.system.isSaving && <Loader/>}
                    <div className="panel-body">
                        <Button onClick={this.publishSystemGroup} className="mb mv btn btn-labeled btn-info pull-left">
                            <span className="btn-label"><i className="fa fa-upload"></i></span> Publish System Group
                        </Button>
                        <Button onClick={this.addSystemGroup} className="mb mv btn btn-labeled btn-info pull-right">
                            <span className="btn-label"><i className="fa fa-plus"></i></span> Add System Group
                        </Button>
                        <div className="clearfix"></div>
                        <div>
                            {GroupsList}
                        </div>
                    </div>
                </div>
            </ContentWrapper>
        );
    }

}
SystemGroups.contextTypes = {
    router: React.PropTypes.object.isRequired
};
export default connect(state => ({
    system: state.system,
    component: state.component
}), Object.assign({}, systemAction, componentAction))(SystemGroups);
