import React, {Component} from 'react';
import { connect } from 'react-redux';
import {SQ_PREFIX} from '../../constant.js'
import {systemAction} from '../../actions';
import {Button, DropdownButton, MenuItem, Dropdown} from 'react-bootstrap';
import Note from '../../components/Common/Note'
import Title from '../../components/Common/Title'
import DeleteDialog from '../../components/Common/DeleteDialog'
import {ApiSystems} from '../../api';

class SystemGroupTitle extends Component{
    constructor() {
        super();
        this.state = {
            title: {},
            notes: [],
            deletedNotes: [],
            backupTraits: null,
            preMadeSystems: [],
            isEdit: false
        }
        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
        this.addSystem = this.addSystem.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.addNote = this.addNote.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
        this.onNoteChange = this.onNoteChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.duplicate = this.duplicate.bind(this);
        this.addPremadeSystem = this.addPremadeSystem.bind(this);
    }

    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
    }

    generateTraits(traits){
        var newTitle={
                name: 'title',
                type: 'component',
                value: ''
            },
            newNotes = [];
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
            if(trait.name === 'note'){
                newNotes.push(trait)
            }
        })
        this.setState({title:newTitle, notes:newNotes, isEdit: false});
    }

    enableEdit(){
        if(this.state.isEdit) return;
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(Object.assign({},note));
        })
        var currentItems = {
            title: Object.assign({},this.state.title),
            notes: newNotes
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title,
            oldNotes = backupTraits.notes;
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle, notes: oldNotes});
        this.props.setEditMode(false)
    }

    addNote(){
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(note);
        })
        newNotes.push({
            name:'note',
            type: 'component',
            value:''
        })
        this.setState({notes:newNotes});
    }

    deleteNote(){
        var notes = this.state.notes;
        var deletedNotes = this.state.deletedNotes;
        var deletedNote = notes.pop();
        if(deletedNote.id){
            deletedNotes.push(deletedNote);
        }
        this.setState({notes:notes, deletedNotes:deletedNotes});
    }

    onNoteChange(index, value){
        var notes = this.state.notes;
        notes[index].value = value;
        this.setState({notes:notes});
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    addSystem(){
        var data = {
            grouped_under: this.props.component.id
        }
        ApiSystems.saveSystem(data);
    }

    addPremadeSystem(system){
        if(!system) {
            if(this.props.title === 'Door Template'){
                ApiSystems.newDoorSystem({name: this.props.systemNameKey, grouped_under: this.props.component.id});
            }
            else{
                ApiSystems.newWindowSystem({name: this.props.systemNameKey, grouped_under: this.props.component.id});
            }
        } else {
            let name = system.name == 'Door system' ? 'PreMadeDoorSystemCopy' : 'PreMadeWindowSystemCopy'
            let data = {name: name, grouped_under: this.props.component.id}
            ApiSystems.duplicateSystem(system.component_id, data).then((res)=>{
                swal("Success", "System has been added.", "success")
                this.props.setEditMode(false)
            });
        }
    }

    duplicate(id){
        return ()=>{
            ApiSystems.duplicateSystem(id, {name: "System Group"}).then((res)=>{
                swal("Duplicated", "System has been duplicated.", "success")
                this.props.setEditMode(false)
            });
        }

    }

    save(){
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [];
        saveTraits.push(this.state.title);
        this.state.notes.map((note)=>{
            saveTraits.push(note);
        })
        ApiSystems.saveTitleTraits(
            this.props.component.id,
            this.state.deletedNotes,
            saveTraits
        );
        this.props.setEditMode(false)
    }

    delete(){
        var self = this;
        DeleteDialog({
            text: "Are you sure you want to delete this System Group? ",
            onConfirm: function(){
                ApiSystems.deleteSystemGroup(self.props.component.id)
                    .then((res)=>swal("Deleted!", "System Group has been deleted.", "success"));
                self.props.setEditMode(false)
            }
        })
    }

    componentDidMount() {
        ApiSystems.ApiGetPreMadeSystems()
            .then(data => {
                data.response_data.forEach((group)=>{
                  group.systems.forEach((system, index) => {

                        const component_with_title = system.traits.find((trait) => {
                            return trait.name === "title"
                        })

                        if(component_with_title) {
                            this.setState((prevState) => ({
                                preMadeSystems: prevState.preMadeSystems.concat(component_with_title)
                            }))
                        }

                    })
                })
            });
    }

    render(){

        const {preMadeSystems} = this.state;
        const title = this.props.title || 'System';

        var buttons = this.state.isEdit ?
            <div className="editSave">
                <Button onClick={this.save} className="btn btn-default">Save</Button>
                <Button onClick={this.cancelEdit} className="btn btn-default btn-outline ">Cancel</Button>
                <Button onClick={this.addNote} className="btn btn-default">Add Note</Button>
                { this.state.notes.length ?
                    <Button onClick={this.deleteNote} className="btn btn-default">Delete Note</Button> : null
                }
            </div>
            :
            <div className="editSave">
                <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className="btn btn-default">Edit</Button>
                {this.props.singleBuilder ? 
                    <Button className="btn btn-default" onClick={() => this.addPremadeSystem()}>Add {title}</Button>
                : <Dropdown id="dropdown-custom-1">
                      <Dropdown.Toggle>
                        Add {title}
                      </Dropdown.Toggle>
                      <Dropdown.Menu className={'animated fadeIn'}>
                        {preMadeSystems 
                            && preMadeSystems.length > 0 
                            && preMadeSystems.map((component, index) => 
                            <MenuItem onClick={() => this.addPremadeSystem(component)} key={index}>
                                {component.value}
                            </MenuItem>
                        )}
                      </Dropdown.Menu>
                    </Dropdown>}
                <Dropdown id="dropdown-custom-1">
                      <Dropdown.Toggle>
                        Duplicate {title}
                      </Dropdown.Toggle>
                      <Dropdown.Menu className={'animated fadeIn'}>
                        {(this.props.component.systems.length) ?
                            this.props.component.systems.map((system,i)=>{
                                let title = system.traits.find((trait)=>trait.name === 'title');
                                return <MenuItem key={i} onClick={this.duplicate(system.id)}>{title && title.value}</MenuItem>
                            })
                        :
                            <div className="text-center">No systems available</div>
                        }
                      </Dropdown.Menu>
                    </Dropdown>
                {this.props.isBuilder ?
                    <Dropdown>
                        <Dropdown.Toggle>
                            {"Duplicate "+this.props.systemName}
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            {this.props.systems.map(system=>{
                                const component = system.traits.find(trait=>trait.name === "title");
                                if(component) {
                                    return <MenuItem onClick={this.duplicate(component.component_id)}>{component.value}</MenuItem>
                                }
                            })}
                        </Dropdown.Menu>
                    </Dropdown>: null
                }
                {!this.props.isBuilder && !this.props.singleBuilder ?
                    <Button onClick={this.delete} disabled={this.props.edit_mode} className="btn btn-danger noBackground delete">Delete group</Button> : null}
            </div>;
        return(
            <div>
                <div className="bg-info-dark defaultWidth">
                    <Title title={this.state.title} cssClass="optionTitle" editing={this.state.isEdit} defaultID={this.props.component.id} onTitleChange={this.onTitleChange}/>
                    {
                        this.state.notes.map((note, index) => {
                            return <Note key={index} note={note} cssClass="optionNote" editing={this.state.isEdit} index={index} onNoteChange={this.onNoteChange} />
                        })
                    }
                </div>
                {buttons}
            </div>
        );
    }
}

SystemGroupTitle.contextTypes = {
    router: React.PropTypes.object.isRequired
};

SystemGroupTitle.propTypes = {
    component: React.PropTypes.object.isRequired,
    traits: React.PropTypes.array.isRequired
};

export default connect(state => ({
}), Object.assign({},systemAction))(SystemGroupTitle);

