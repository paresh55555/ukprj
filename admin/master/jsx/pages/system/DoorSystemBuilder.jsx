import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import Loader from '../../components/loader/Loader';
import {systemAction, componentAction} from '../../actions';
import SystemGroupTitle from './SystemGroupTitle'
import SystemComponent from './SystemComponent'
import {NotifyAlert} from '../../components/Common/notify.js'

// api for getting system builder

import {ApiSystems} from '../../api';
import { compose, withProps } from 'recompose';

class DoorSystemBuilder extends React.Component {
    constructor() {
        super();
        this.state = {
            action: '',
            published:false,
        }
        this.routerWillLeave = this.routerWillLeave.bind(this);
        this.getPreMadeSystems = this.getPreMadeSystems.bind(this);
    }

    getPreMadeSystems () {

        // get pre made systems

        ApiSystems.ApiGetPreMadeSystems();

    }

    componentWillMount() {

        this.getPreMadeSystems()

    }
    componentDidMount(){
        
        const { route } = this.props;
        const { router } = this.context;

        router.setRouteLeaveHook(route, this.routerWillLeave)
    }

    routerWillLeave(nextLocation) {
        // return false to prevent a transition w/o prompting the user,
        // or return a string to allow the user to decide:
       
        if (this.props.component.edit_mode)
            return 'Your work is not saved! Are you sure you want to leave?'
    
    }

    componentWillUnmount(){
        
        this.props.setEditMode(false)
    
    }
    
    componentWillReceiveProps(nextProps){

        if(nextProps.system.isSaved != this.props.system.isSaved && nextProps.system.isSaved ||
            nextProps.system.isDeleted != this.props.system.isDeleted && nextProps.system.isDeleted ){
            this.getPreMadeSystems();
        }
        
        if(nextProps.system.isPublished && this.state.published){
            NotifyAlert(`"System Groups" published successfully`);
            this.setState({published:false})
        }
    }

    render() {
        const {premadeSystem} = this.props.system;
        const builders = [];
        premadeSystem.forEach(item=>{
            if(item.name === "PreMadeDoorSystem"){
                builders.push(item);
            }
        });
        return (
            <ContentWrapper>
                <h3>Door Systems Template</h3>
                <div className="panel-default panel mainBox">
                    {this.props.system.isSaving && <Loader/>}
                    <div className="panel-body">
                        {builders.map((item, i)=>(
                            premadeSystem.length ?
                                <div key={i}>
                                    <SystemGroupTitle component={item}
                                                      traits={item.traits}
                                                      cid={item.id}
                                                      edit_mode={this.props.component.edit_mode}
                                                      setEditMode={this.props.setEditMode}
                                                      systemName={item.systems[0] && item.systems[0].name}
                                                      systems={item.systems}
                                                      key={i}
                                                      systemNameKey={'PreMadeDoorSystemCopy'}
                                                      singleBuilder={true}
                                                      title={'Door Template'}/>
                                    <div className="optionsBox">
                                        {item.systems.map((system,idx)=>{
                                            return [<SystemComponent component={system}
                                             traits={system.traits}
                                             cid={system.id}
                                             edit_mode={this.props.component.edit_mode}
                                             setEditMode={this.props.setEditMode}
                                             key={idx}/>,
                                             ((idx+1)%3 === 0) ? <div className="fullOption"></div>: ''
                                             ]
                                        })}
                                    </div>
                                </div>
                                : 'No system groups available'
                        ))}
                    </div>
                </div>
            </ContentWrapper>
        );
    }

}

DoorSystemBuilder.contextTypes = {
    router: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiSystems);

export default compose(
    connect(state => ({
        system: state.system,
        component: state.component
    }), Object.assign({}, systemAction, componentAction)),
    withProps(apiActions),
)(DoorSystemBuilder);