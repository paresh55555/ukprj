import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Router, Route, Link, History } from 'react-router';
import {systemAction} from '../../actions';
import {Button, ButtonToolbar, Col} from 'react-bootstrap';
import { SQ_PREFIX, SYSTEM_LIST_URL } from '../../constant.js'
import ImageUploader from '../../components/Common/ImageUploader/ImageUploader'
import Note from '../../components/Common/Note'
import Title from '../../components/Common/Title'
import ComponentImage from '../../components/Common/ComponentImage'
import DeleteDialog from '../../components/Common/DeleteDialog'
import {ApiSystems} from '../../api'

class SystemComponent extends React.Component {
    constructor(){
        super()
        this.state = {
            title: {},
            image: {},
            notes: [],
            deletedNotes: [],
            backupTraits: null,
            isEdit: false,
            showUpload: false
        }
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.addNote = this.addNote.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
        this.onNoteChange = this.onNoteChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.deleteSystem = this.deleteSystem.bind(this);
        this.gotoSystem = this.gotoSystem.bind(this);
        this.openUpload = this.openUpload.bind(this);
        this.onUpload = this.onUpload.bind(this);
        this.closeUpload = this.closeUpload.bind(this);
        this.duplicate = this.duplicate.bind(this);
    }

    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(nextProps.trait.isSaving) return;
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
        if(this.props.trait.traits !== nextProps.trait.traits && nextProps.trait.isSaved && (this.props.optionID === nextProps.trait.optionId)){
            this.generateTraits(nextProps.trait.traits)
        }
    }

    generateTraits(traits){
        var self = this,
            newTitle={
                name: 'title',
                type: 'component',
                value: ''
            },
            newNotes = [],
            newImage = {};
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
            if(trait.name === 'note'){
                newNotes.push(trait);
            }
            if(trait.name === 'imgUrl' && trait.value !== newImage.value){
                newImage = trait;
            }
        })

        if(!newImage.id){
            let newImage = {
                name: 'imgUrl',
                value: 'tmp',
                visible: 1
            }
            ApiSystems.saveSystemTrait(
                this.props.cid,
                [newImage]
            )
        }else {
            this.setState({title: newTitle, notes: newNotes, image: newImage, isEdit: false});
        }
    }

    enableEdit(){
        if(this.state.isEdit) return;
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(Object.assign({},note));
        })
        var currentItems = {
            title: Object.assign({},this.state.title),
            notes: newNotes,
            image: Object.assign({},this.state.image)
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title,
            oldNotes = backupTraits.notes,
            oldImage = backupTraits.image;
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle, notes: oldNotes, image: oldImage});
        this.props.setEditMode(false)
    }

    addNote(){
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(note);
        })
        newNotes.push({
            name:'note',
            type: 'component',
            value:''
        })
        this.setState({notes:newNotes});
    }

    deleteNote(){
        var notes = this.state.notes;
        var deletedNotes = this.state.deletedNotes;
        var deletedNote = notes.pop();
        if(deletedNote.id){
            deletedNotes.push(deletedNote);
        }
        this.setState({notes:notes, deletedNotes:deletedNotes});
    }

    onNoteChange(index, value){
        var notes = this.state.notes;
        notes[index].value = value;
        this.setState({notes:notes});
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    save(){
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [];
        saveTraits.push(this.state.title);
        this.state.notes.map((note)=>{
            saveTraits.push(note);
        })
        if(this.state.image && this.state.image.value){
            saveTraits.push(this.state.image);
        }
        ApiSystems.saveTitleTraits(
            this.props.component.id,
            this.state.deletedNotes,
            saveTraits
        );
        this.props.setEditMode(false)
    }
    gotoSystem(){
      if(this.state.isEdit){
        return
      }
      this.context.router.push(SQ_PREFIX+'systems/'+ this.props.component.id);
    }

    deleteSystem(){
        var self = this;
        var id = this.props.component.id;

        DeleteDialog({
            text: "Are you sure you want to delete this type? ",
            onConfirm: function(){
                ApiSystems.deleteSystem(id)
                    .then((res)=> {
                        swal("Deleted!", "System has been deleted.", "success")
                    });
                self.props.setEditMode(false)
            }
        })
    }

    openUpload(){
        if (!this.state.isEdit) {
            return;
        }
        this.setState({showUpload:true})
    }

    onUpload(imgUrl){
        var image = this.state.image;
        if(imgUrl){
            image['name'] = 'imgUrl';
            image['value'] = imgUrl;
            image['type'] = 'component';
            image['visible'] = 1;
        }
        this.setState({image:image})
    }

    closeUpload(){
        this.setState({showUpload: false})
    }

    duplicate(){
        let self = this
        ApiSystems.duplicateSystem(this.props.cid, {name: 'System Group'}).then((res)=>{
            swal("Duplicated", "System has been duplicated.", "success")
            self.props.setEditMode(false)
        });
    }

    render(){
        var style = { border: 0 };
        return(
            <Col sm={4}>
                <div className="panel panel-primary system" onClick={this.gotoSystem}>
                    <div className="panel-content">
                        <ComponentImage image={this.state.image} width={267} height={332} onClick={this.openUpload} iconClass="systemImageUpload"/>
                    </div>
                    <div className="panel-footer bg-info-dark bt0 clearfix btn-block">
                        <div className="container-fluid">
                            <Title title={this.state.title} cssClass="pull-left" editing={this.state.isEdit}
                                   defaultID={this.props.component.id} onTitleChange={this.onTitleChange}/>
                            <span className="pull-right">
                            <em className="fa fa-chevron-circle-right"></em>
                        </span>
                        </div>
                        <div className="container-fluid">
                            {
                                this.state.notes.map((note, index) => {
                                    return <Note key={index} note={note} cssClass="optionNote"
                                                 editing={this.state.isEdit} index={index}
                                                 onNoteChange={this.onNoteChange}/>
                                })
                            }
                        </div>
                    </div>
                </div>
                {this.state.isEdit ?
                    <ButtonToolbar>
                        <Button onClick={this.save} className="btn btn-default">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default">Cancel</Button>
                        <Button onClick={this.deleteSystem} className="pull-right btn btn-danger noBackground delete">Delete</Button>
                        <Button onClick={this.addNote} className="btn btn-default">Add Note</Button>
                        { this.state.notes.length ?
                            <Button onClick={this.deleteNote} className="btn btn-default">Delete Note</Button> : null
                        }
                        <Button onClick={this.duplicate} className="btn btn-default">Duplicate</Button>
                    </ButtonToolbar>
                    :
                    <div>
                        <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className="btn btn-default">Edit</Button>
                        <Button onClick={this.deleteSystem} disabled={this.props.edit_mode} className="btn btn-danger noBackground pull-right delete">Delete</Button>
                    </div>
                }
                <ImageUploader show={this.state.showUpload} config={{
                    system_id: this.props.cid,
                    trait_id: this.state.image.id,
                    type: 'system',
                    width: 267,
                    height: 332,
                }} onUpload={this.onUpload} onClose={this.closeUpload}/>
            </Col>
        )
    }
}

SystemComponent.contextTypes = {
    router: React.PropTypes.object.isRequired
};

export default connect(state => ({
    trait: state.optionTrait
}), Object.assign({}, systemAction))(SystemComponent);