import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button, FormControl} from 'react-bootstrap';
import { SQ_PREFIX } from '../../constant.js'
import {systemAction} from '../../actions';
import {NotifyAlert} from '../../components/Common/notify.js'
import Loader from '../../components/loader/Loader'
import swal from 'sweetalert'
import {ApiSystems} from '../../api';

class AddSystem extends React.Component {
  constructor(props){
    super(props);
    this.state ={title:'Add',system:{},edited:false, showDelete:false}
    self = this;
  }
  componentWillMount(){
    const system_id = this.props.routeParams.id
    if(system_id == 'new'){
      this.setState({title:'Add', showDelete:false, system:{}})
    }
    else{
      this.setState({title:'Edit', showDelete:true})
    }
    this.props.getSystemDetail(system_id);
  }
  componentWillReceiveProps(nextProps){
    if(this.props.system.system != nextProps.system.system){
      this.setState({system:nextProps.system.system});
    }
    if(this.props.system.isSaved != nextProps.system.isSaved && nextProps.system.isSaved){
      var msg = this.props.routeParams.id === 'new' ? 'added' : 'updated';
      NotifyAlert(`System ${msg} successfully`)
      this._backtoSystem();
    }
  }
  componentDidMount() {
    const { route } = this.props;
    const { router } = this.context;
    router.setRouteLeaveHook(route, this.routerWillLeave)
  }
  routerWillLeave(nextLocation) {
    // return false to prevent a transition w/o prompting the user,
    // or return a string to allow the user to decide:
    if (self.state.edited)
      return 'Your work is not saved! Are you sure you want to leave?';
  }
  _handleChange(value, e){
    var system = this.state.system;
    system[value] = e.target.value;
    this.setState({system:system, edited:true})
  }
  _backtoSystem(){
    self.context.router.push(SQ_PREFIX+'systems');
  }
  _saveCompany(){
    if($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0})){
      self.setState({edited:false});
      if(self.props.routeParams.id === 'new'){
        ApiSystems.saveSystem(self.state.system)
      }
      else{
        self.props.updateSystem(self.state.system)

      }
    }
  }
  _deleteCompany(){

  }
    render() {
      const system = this.props.system
        return (
            <ContentWrapper>
                <h3>{this.state.title} System</h3>
                { /* START row */ }
                <Row>
                  <Col sm={ 6 }>
                        { /* START panel */ } 
                        <Panel header="">
                           {system.isLoading ? <Loader /> :
                            <form className="form-vertical" data-parsley-validate="" data-parsley-group="block-0" noValidate>
                                <div className="form-group">
                                    <label className=" control-label">System Name</label>
                                    <FormControl   type="text" placeholder="System Name" className="form-control" value={this.state.system.name} onChange={this._handleChange.bind(this, 'name')} required />
                                </div>
                                <div className="form-group">
                                    <label className=" control-label">System URL</label>
                                    <FormControl   type="text" placeholder="System URL" className="form-control" value={this.state.system.url} onChange={this._handleChange.bind(this, 'url')} required />
                                </div>
                                
                                <div className="form-group">
                                    <Button disabled={system.isSaving} type="button" bsStyle="primary" bsSize="small" onClick={this._saveCompany}>Save</Button>
                                    <Button type="button" className="mh" bsStyle="default" bsSize="small" onClick={this._backtoSystem}>Cancel</Button>
                                    {this.state.showDelete ? <Button type="button" className="pull-right" bsStyle="danger" bsSize="small" onClick={this._deleteCompany}>Delete</Button> : null }
                                </div>
                            </form> }
                        </Panel>
                        { /* END panel */ }
                    </Col>
                </Row>
                { /* END row */ }
            </ContentWrapper>
            );
    }

}
 
AddSystem.contextTypes = {
        router: React.PropTypes.object.isRequired
      };

AddSystem.propTypes = {
    getSystemDetail:PropTypes.func.isRequired,
    updateSystem:PropTypes.func.isRequired,
    deleteSystem:PropTypes.func.isRequired,
  };
export default connect(state => ({
  system: state.system
}), Object.assign({}, systemAction))(AddSystem);