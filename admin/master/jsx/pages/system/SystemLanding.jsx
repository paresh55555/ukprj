import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col } from 'react-bootstrap';

import LinkBox from '../../components/linkbox/LinkBox'
import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';
import {componentAction, systemAction} from '../../actions';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';
import { isDoorTemplate, getTraitByName } from '../../components/Common/helpers';


class SystemLanding extends React.Component {
	constructor() {
      super();
    }
    componentWillMount(){
        this.props.ApiGetSystemComponent(this.props.params.id);
    }

    render() {
        const systemTitle = getTraitByName(this.props.component && this.props.component.system, 'title');
        const name =  this.props.component.system.name === 'Door System' ? 'Door System Template': (this.props.component.system.name === 'Window System' ? 'Window System Template' : 'Active System')
        const url =  this.props.component.system.name === 'Door System' ? '/builder/systems/doors': (this.props.component.system.name === 'Window System' ? '/builder/systems/windows' : '/systems')
        const breadCrumbData = [
            {
                title: name, 
                url:url,
                active:false
            },{
                title : systemTitle && systemTitle.value || '',
                url:`/systems/${this.props.params.id}`,
                active:true
            }

        ]
        return (
            <ContentWrapper>
                <Breadcrumbs data={breadCrumbData} />
                <Grid fluid>
                    <Row>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'systems/'+this.props.params.id+'/components'} title={'Frontend UI'} />
                        </Col>
                        <Col md={2} sm={4}> 
                            <LinkBox url={'systems/'+this.props.params.id+'/panelRanges'} title={'Panel Ranges'} />
                        </Col>
                        <Col md={2} sm={4}>
                            <LinkBox url={'systems/'+this.props.params.id+'/pricing'} title={'Pricing'} />
                        </Col>
                        <Col md={2} sm={4}>
                            <LinkBox url={'systems/'+this.props.params.id+'/parts'} title={'Parts'} />
                        </Col>
                    </Row>
                </Grid>
            </ContentWrapper>
        );
    }

}


const apiActions = Object.assign({}, ApiComponent);
export default compose(
    connect(state => ({
        component: state.component
    }), Object.assign({}, componentAction)),
    withProps(apiActions))(SystemLanding);
