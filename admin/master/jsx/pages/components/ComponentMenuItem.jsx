import React from 'react'
import ContentEditable from  'react-contenteditable';

class ComponentMenuItem extends React.Component {
	constructor(){
		super()
		this.state = {
			traits: {
				title: ''
			},
            traitids: {},
            selected: false
		}
		this.setTraits = this.setTraits.bind(this);
	}
	componentWillReceiveProps(nextProps){
        if(nextProps.data && nextProps.data.traits && this.props.data.traits !== nextProps.data.traits){
            this.generateTraits(nextProps.data.traits)
        }
        if(nextProps.loaded){
            this.initEditable();
        }
	}
	componentDidMount(){
        if(this.props.data && this.props.data.traits){
            this.generateTraits(this.props.data.traits)
        }
        if(this.props.loaded) {
            this.initEditable();
        }
	}
	initEditable(){
		var self = this;
		var title = this.state.traits.title;
		var elem = $('.optionMenuText.item-' + this.props.index);
        if(elem.data('editable')){
            elem.editable('option', 'value', title ? title : (this.props.editing ? "" : "< Add Menu >"));
            elem.editable('option', 'disabled', !this.props.editing);
            elem.addClass('editable editable-click');
        }else {
            elem.editable({
                disabled: !self.props.editing,
                placeholder: '< Add Menu >',
                emptytext: '< Add Menu >',
                value: title ? title : "",
                success: function (response, newValue) {
                    let traits = self.state.traits;
                    traits['title'] = newValue;
                    self.setTraits(traits);
                }
            });
        }
	}

    componentDidUpdate(){
        this.initEditable()
    }


    change(newValue){
        this.setState({title: newValue});
        this.props.onTitleChange(newValue);
    }

    handleClick(e){
        if($(e.target).hasClass('optionMenu') ||
            $(e.target).hasClass('optionMenuText') && !this.props.editing){
            this.props.onSelectItem(this.props.data.id || this.props.data.tmpid);
        }else{
            return;
        }
	}

    generateTraits(traits){
        let newtraits={};
        let traitids = {}
        traits.map((trait)=>{
            newtraits[trait.name] = trait.value;
            traitids[trait.name] = trait.id
        })
        this.setState({traits:newtraits,traitids:traitids})
        this.initEditable();
	}

	setTraits(traits){
        this.setState({traits:traits});
        var newTraits = [],
            traitids = this.state.traitids;
        for(let key in traits){
            let trait = {
                name:key,
				type:'component',
                value:traits[key],
            }
            if(traitids[key]){
                trait['id'] = traitids[key];
            }
            newTraits.push(trait);
		}
        this.props.onTraitsChange(newTraits, this.props.index);
    }

    render() {
        const {menu, index, color} = this.props;
        const selected = this.props.selected;
        const title = this.state.traits.title;
        return (
			<div onClick={(e) => this.handleClick(e)} className={"optionMenu" + (this.props.selected ? " optionMenuChoosen" : "")}>
				<span className={'optionMenuText item-'+this.props.index} title={title}>{title}</span>
			</div>
        )
    }
}
export default ComponentMenuItem;