import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {SQ_PREFIX} from '../../constant.js'
import {componentAction, systemAction} from '../../actions';
import ComponentTitle from '../../pages/components/ComponentTitle'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ComponentList extends React.Component {
    constructor() {
        super();
        this.state = {
            components: [],
        };
    }

    componentWillMount() {
        const system_id = this.props.params.id;
    }

    componentWillReceiveProps(nextProps) {
        if(/tmp/.test(nextProps.selectedID)){
            this.generateComponentsList([]);
            return;
        }
        if((nextProps.selectedID !== null && this.props.selectedID !== nextProps.selectedID) ||
            nextProps.component.updateList){
            this.props.ApiGetComponentsByMenu(nextProps.params.id, nextProps.selectedID);
        }
        if(this.props.component.components_list !== nextProps.component.components_list){
            this.generateComponentsList(nextProps.component.components_list);
        }
    }

    generateComponentsList(components){
        this.setState({components: components});
    }

    render() {
        const components = this.state.components;
        return (
            <div>
            {
                components.length ?
                    components.map((component, index)=>{
                        return <ComponentTitle
                            key={index}
                            component={component}
                            traits={component.traits}
                            params={this.props.params}
                        />
                    })
                :
                <div>No components available</div>
            }
            </div>
        );
    }

}

ComponentList.contextTypes = {
    router: React.PropTypes.object.isRequired
};

ComponentList.propTypes = {
    ApiGetComponentsByMenu: PropTypes.func.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        component: state.component,
        system: state.system
    }), Object.assign({}, componentAction, systemAction)),
    withProps(apiActions),
)(ComponentList);