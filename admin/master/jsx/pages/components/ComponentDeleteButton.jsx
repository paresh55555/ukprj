import React, {Component} from 'react';
import { connect } from 'react-redux';
import {Button} from 'react-bootstrap';
import {componentAction} from '../../actions';
import DeleteDialog from '../../components/Common/DeleteDialog'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ComponentDeleteButton extends Component{
    constructor() {
        super();
        this.delete = this.delete.bind(this);
    }

    delete(){
        var self = this;
        DeleteDialog({
            text: "Are you sure you want to delete this component?",
            onConfirm: function(){
                self.props.ApiDeleteComponent(self.props.systemID,self.props.componentID)
                    .then((res)=>{
                        swal("Deleted!", "Component has been deleted.", "success")
                    });
            }
        })
    }

    render(){
        return(
            <Button onClick={this.delete} disabled={this.props.disabled} className="btn btn-danger noBackground delete">Delete</Button>
        );
    }
}

ComponentDeleteButton.contextTypes = {
    router: React.PropTypes.object.isRequired
};

ComponentDeleteButton.propTypes = {
    systemID: React.PropTypes.number.isRequired,
    componentID: React.PropTypes.number.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({}), Object.assign({}, componentAction)),
    withProps(apiActions),
)(ComponentDeleteButton);