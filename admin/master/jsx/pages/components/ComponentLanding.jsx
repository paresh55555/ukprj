import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {SQ_PREFIX} from '../../constant.js'
import {componentAction, systemAction} from '../../actions';
import { Button } from 'react-bootstrap';
import {NotifyAlert} from '../../components/Common/notify.js'
import ContentWrapper from '../../components/Layout/ContentWrapper';
import ComponentTitle from './ComponentTitle'
import ComponentMenu from './ComponentMenu'

import Dimensions from '../../components/dimensions/Dimensions';
import ComponentLayout from '../../components/ComponentLayout/ComponentLayout';
import Color from '../../components/Color/Color';
import SwingDirection from '../../components/SwingDirection/SwingDirection';
import PanelMovement from '../../components/PanelMovement/PanelMovement';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

import PremadeLanding from '../premade/PremadeLanding';
import DoorPanels from '../../components/Doors/DoorPanels';
import WindowComponent from '../../components/Window/WindowComponent';
import NextTab from '../../components/NextTab/NextTab';
import Loader from "../../components/loader/Loader";

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';
import { isDoorTemplate } from '../../components/Common/helpers';

class ComponentLanding extends React.Component {
    constructor() {
        super();
        this.state = {
            components: [],
            premadeData: [],
            menuData: [],
            selectedID: null,
            published: false,
        };
        this.setSelectedID = this.setSelectedID.bind(this)
        this.setPremadeData = this.setPremadeData.bind(this)
        this.setMenuData = this.setMenuData.bind(this)
        this.onChangeColorComponent = this.onChangeColorComponent.bind(this)
        this.routerWillLeave = this.routerWillLeave.bind(this)
    }

    componentWillMount(){
        this.props.ApiGetSystemComponent(this.props.params.id);
        var savedSelectedMenu = parseInt(localStorage.getItem('adminSelectedMenu'));
        if(savedSelectedMenu){
            this.setSelectedID(savedSelectedMenu);
        }
    }
    componentDidMount(){
        const { route } = this.props;
        const { router } = this.context;
        router.setRouteLeaveHook(route, this.routerWillLeave)
    }
    routerWillLeave(nextLocation) {
      // return false to prevent a transition w/o prompting the user,
      // or return a string to allow the user to decide:
      if (this.props.component.edit_mode ||
          this.props.component.premadeData && this.props.component.premadeData.enable)
        return 'Your work is not saved! Are you sure you want to leave?'
    }
    componentWillUnmount(){
        this.props.setEditMode(false)
    }

    setSelectedID(id){
        if(/tmp/.test(id)){
            this.setState({selectedID:id});
            this.generateComponentsList([]);
            return;
        }
        if(id !== null){
            localStorage.setItem('adminSelectedMenu', id)
            this.setState({selectedID:id});
            this.props.ApiGetComponentsByMenu(this.props.params.id, id);
        }
    }

    setPremadeData(data){
        this.setState({premadeData: data})
    }

    setMenuData(data){
        this.setState({menuData: data})
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.component.isSaving || nextProps.component.isLoading) return;
        if(/tmp/.test(this.state.selectedID)){
            this.generateComponentsList([]);
            return;
        }
        if(this.state.selectedID !== null && nextProps.component.updateList){
            this.props.ApiGetComponentsByMenu(nextProps.params.id, this.state.selectedID);
        }
        if(nextProps.component.updateComponent){
            this.onChangeColorComponent(nextProps.component.newData)
        }
        if(this.props.component.components_list !== nextProps.component.components_list){
            this.generateComponentsList(nextProps.component.components_list);
        }
        if(nextProps.component.isPublished && this.state.published){
            NotifyAlert(`System published successfully`);
            this.setState({published:false})
        }
        // if(!!nextProps.component.isDeleted){
        //    this.setSelectedID(this.state.selectedID);
        // }
    }

    generateComponentsList(components){
        this.setState({components: components});
    }

    onChangeColorComponent(newData){
        var components = this.state.components;
        var index = null;
        components.map((obj, idx)=>{
            if((obj.name === 'Interior Color' || obj.name === 'Exterior Color') && obj.id === newData.id){
                index = idx
            }
        })
        if(index !== null){
            components[index] = newData;
            this.setState({components: components})
        }
    }

    publishSystem(){
        const systemTitle = this.props.component && this.props.component.system && this.props.component.system.traits && this.props.component.system.traits.length &&
            this.props.component.system.traits.find((trait)=>trait.name ==='title')
        this.setState({published:true})
        this.props.ApiPublishSystemComponents(this.props.params.id, systemTitle && systemTitle.value)
    }

    getComponentLayout(component, type){
        type = type || "halfline"
        return <ComponentLayout edit_mode={this.props.component.edit_mode} setEditMode={this.props.setEditMode}
                                cname={component.name} ctype={type} systemID={parseInt(this.props.params.id)}
                                component={component} traits={component.traits}
                                cid={component.id}
        />
    };
    getBuilder(component){
        var builder;
        switch (component.name) {
            case 'Dimensions':
                builder = <Dimensions edit_mode={this.props.component.edit_mode}  params={this.props.params}
                                      systemID={parseInt(this.props.params.id)}
                                      component={component} traits={component.traits} cid={component.id}
                            />
                break;
            case 'Track':
            case 'Frame':
                builder = this.getComponentLayout(component, "halfline")
                break;
            case 'Glass':
            case 'GlassOptions':
            case 'HardwareOptions':
                builder = this.getComponentLayout(component, "fullline")
                break;
            case 'Thumbnail':
            case 'Hardware':
                builder = this.getComponentLayout(component, "thumb")
                break;
            case 'Interior Color':
            case 'Exterior Color':
                builder = <Color edit_mode={this.props.component.edit_mode} systemID={parseInt(this.props.params.id)} data={component} traits={component.traits} cid={component.id} />
                break;
            case 'SwingDirection':
                builder = <SwingDirection edit_mode={this.props.component.edit_mode} systemID={parseInt(this.props.params.id)} component={component} traits={component.traits} />
                break;
            case 'DoorPanels':
                builder = <DoorPanels edit_mode={this.props.component.edit_mode} setEditMode={this.props.setEditMode}
                                      cname={component.name} systemID={parseInt(this.props.params.id)}
                                      component={component} traits={component.traits}
                                      cid={component.id}
                />
                break;
            case 'PanelMovement':
                builder = <PanelMovement edit_mode={this.props.component.edit_mode} systemID={parseInt(this.props.params.id)} component={component} traits={component.traits} />
                break;
            case 'Door Panels':
                builder = <DoorPanels edit_mode={this.props.component.edit_mode} setEditMode={this.props.setEditMode} cname={component.name} systemID={parseInt(this.props.params.id)} component={component} traits={component.traits} cid={component.id} onEdit={this.setCurrentEditing} />
                break;
            case 'Windows':
                builder = <WindowComponent edit_mode={this.props.component.edit_mode} systemID={parseInt(this.props.params.id)} component={component} traits={component.traits}  />
                break;
            default:
                builder = <div className="defaultWidth">No builder assigned for {component.name} component</div>
        }
        return builder
    }

    render() {
        const components = this.state.components;
        const systemTitle = this.props.component && this.props.component.system && this.props.component.system.traits && this.props.component.system.traits.length &&
            this.props.component.system.traits.find((trait)=>trait.name ==='title');
        const premadeData = this.props.component.premadeData
        const nextTabComponent = this.state.components.find((component)=>component.name ==='NextTab')
        const doorTemplate = isDoorTemplate(this.props.component.system)
        const name =  this.props.component.system.name === 'Door System' ? 'Door System Template': (this.props.component.system.name === 'Window System' ? 'Window System Template' : 'Active System')
        const url =  this.props.component.system.name === 'Door System' ? '/builder/systems/doors': (this.props.component.system.name === 'Window System' ? '/builder/systems/windows' : '/systems')
        const breadCrumbData = [
            {
                title: name, 
                url:url,
                active:false
            },{
                title : systemTitle && systemTitle.value || '',
                url:`/systems/${this.props.params.id}`,
                active:false
            },
            {
                title : 'Frontend UI',
                url:'',
                active:true
            }
        ]
        return (
            <ContentWrapper>
            <Breadcrumbs data={breadCrumbData} />
            {premadeData && premadeData.enable ?
                <PremadeLanding data={this.state.premadeData} systemID={premadeData.systemID}
                                setPremadeData={this.setPremadeData}
                                componentID={premadeData.componentID}
                                colorComponentID={premadeData.colorComponentID}
                                mode={premadeData.mode} />
                :
                <div>
                    {doorTemplate ?
                        null :
                        <h3 className="p0"><Button disabled={this.props.component.isLoading} onClick={this.publishSystem.bind(this)} className="publish-system btn btn-labeled btn-info pull-left">
                            Publish "{!!systemTitle && systemTitle.value}"
                        </Button></h3>
                    }
                    <div className="panel-default panel mainBox">
                        <div className="panel-body">
                            {this.props.component.isLoading ? <Loader /> : null}
                            <ComponentMenu data={this.state.menuData} edit_mode={this.props.component.edit_mode}
                                           systemID={this.props.params.id}
                                           selectedID={this.state.selectedID}
                                           setMenuData={this.setMenuData}
                                           onSetSelectedID={this.setSelectedID}
                                           doorTemplate={doorTemplate}
                            />
                            {
                                components.length ?
                                    components.map((component, index)=>{
                                        if(component.name === 'NextTab')return true;
                                        return <div key={index}>{this.getBuilder(component)}</div>
                                    })
                                    :
                                    <div>No components available</div>
                            }
                            {nextTabComponent ? <NextTab edit_mode={this.props.component.edit_mode} systemID={parseInt(this.props.params.id)} component={nextTabComponent} traits={nextTabComponent.traits} /> : null}
                        </div>
                    </div>
                </div>
            }
            </ContentWrapper>
        );
    }

}

ComponentLanding.contextTypes = {
    router: React.PropTypes.object.isRequired
};

ComponentLanding.propTypes = {
    ApiGetComponentsByMenu: PropTypes.func.isRequired,
    setEditMode: PropTypes.func.isRequired
};
const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        component: state.component
    }), Object.assign({}, componentAction)),
    withProps(apiActions),
)(ComponentLanding);