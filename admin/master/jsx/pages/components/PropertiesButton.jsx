import React, {Component} from 'react';
import {connect} from 'react-redux';
import {changeHasDefault, changeAllowMultiple, changeVisible, changeAllowGuests} from '../../actions/option';


class PropertiesButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdown: true
        };
        this.toggleDropdown = this.toggleDropdown.bind(this);
        this.changeMultiples = this.changeMultiples.bind(this);
        this.changeDefault = this.changeDefault.bind(this)
        this.changeVisiblity = this.changeVisiblity.bind(this)
        this.changeGuests = this.changeGuests.bind(this)
    }

    changeMultiples() {
        this.props.changeAllowMultiple(this.props.cid)
    }

    changeDefault() {
        this.props.changeHasDefault(this.props.cid)
    }

    changeVisiblity() {
        this.props.changeVisible(this.props.cid)
    }

    changeGuests() {
        this.props.changeAllowGuests(this.props.cid)
    }

    toggleDropdown() {
        this.setState({dropdown: !this.state.dropdown})
    }

    render() {
        const {multiples, hasDefault, guestsAllow, visible} = this.props.componentDropdownState['id'+this.props.cid];
        return (
            <div className="dropdown-properties">
                <div className="dropdown open btn-group">
                    <button type="button" className="btn btn-default" onClick={this.toggleDropdown}>Properties</button>
                    {this.state.dropdown ? <ul className="dropdown-menu">
                        <li>
                            <div className="checkbox"><label><input type="checkbox" checked={multiples} onChange={this.changeMultiples}/>AllowMultiples</label></div>
                        </li>
                        <li>
                            <div className="checkbox"><label><input type="checkbox" checked={visible} onChange={this.changeVisiblity} />Is Visible</label></div>
                        </li>
                        <li>
                            <div className="checkbox"><label><input type="checkbox" checked={hasDefault} onChange={this.changeDefault} />Has Default</label></div>
                        </li>
                        <li>
                            <div className="checkbox"><label><input type="checkbox" checked={guestsAllow} onChange={this.changeGuests} />Allow Guests</label></div>
                        </li>
                    </ul>
                    : null}
                </div>
            </div>

    )}
}


const mapStateToProps = (state) => ({
    componentDropdownState: state.option.componentDropdownState
});
const actions = {
    changeAllowMultiple,
    changeHasDefault,
    changeVisible,
    changeAllowGuests
};

export default connect(mapStateToProps, actions)(PropertiesButton);