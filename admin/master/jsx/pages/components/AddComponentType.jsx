import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {NotifyAlert} from '../../components/Common/notify.js'
import {componentAction} from '../../actions';

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

import {
    Row,
    Panel,
    ListGroup,
    ListGroupItem,
    Dropdown,
    MenuItem
} from 'react-bootstrap';

var clicked = false;
class AddComponentType extends React.Component {
    constructor() {
        super();
        this.state = {
            types: []
        }
    }
    componentWillMount(){
        this.props.ApiGetComponentData(this.props.systemID).then(()=>{
            this.props.ApiGetComponentTypes().then(()=>{
                this.setTypes()
            })
        })
    }
    componentWillReceiveProps(nextProps){
        if(this.props.components_list !== nextProps.components_list){
            this.props.ApiGetComponentData(this.props.systemID).then(()=>{
                this.setTypes()
            })
        }
    }
    setTypes(){
        var self = this;
        let component_types = this.props.component.component_types.map(function(item) {
            return item['name'];
        }).sort();
        component_types = component_types.filter((type)=>{
            return !self.props.component.list.find((c)=>{ return type == c.type})
        });
        let temp = component_types[0];
        component_types[0] = component_types[1];
        component_types[1] = temp;
        this.setState({types: component_types})
    }
    CreateComponent(ctype){
        if(clicked)return false;
        clicked = true;
        const systemID = this.props.systemID;
        const menuID = this.props.menuID;
        let self = this
        let newComponent = {
            name:ctype,
            type:ctype
        }
        this.props.ApiAddComponent(systemID, menuID, newComponent).then((res)=>{
            clicked = false;
            NotifyAlert(`Component added successfully`);
        });
    }

    render() {
        const {types} = this.state;
        return (
            <div>
            <Dropdown disabled={this.props.edit_mode} id="dropdown-custom-1">
              <Dropdown.Toggle>
                Add Component
              </Dropdown.Toggle>
              <Dropdown.Menu className={'animated fadeIn'}>
                {!!types && types.map((ctype, index)=>{
                    let typeName = this.props.doorTemplate ? ctype && ctype.replace('Door','') : ctype
                    return(<MenuItem key={index} onClick={this.CreateComponent.bind(this,ctype)}>{this.props.doorTemplate ? 'Door': ''} {typeName}</MenuItem>)
                })}
              </Dropdown.Menu>
            </Dropdown>
            </div>
        );
    }

}

AddComponentType.contextTypes = {
    router: React.PropTypes.object.isRequired
};

AddComponentType.propTypes = {
    ApiGetComponentTypes:PropTypes.func.isRequired,
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        component: state.component,
        components_list: state.component.components_list
    }), Object.assign({}, componentAction)),
    withProps(apiActions),
)(AddComponentType);