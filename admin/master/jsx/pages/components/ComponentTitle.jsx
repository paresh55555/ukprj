import React, {Component} from 'react';
import { connect } from 'react-redux';
import {SQ_PREFIX} from '../../constant.js'
import {componentAction, optionAction} from '../../actions';
import {Panel, Button} from 'react-bootstrap';
import Note from '../../components/Common/Note'
import Title from '../../components/Common/Title'
import ComponentDeleteButton from '../../pages/components/ComponentDeleteButton'
import PropertiesButton from './PropertiesButton'

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ComponentTitle extends Component{
    constructor() {
        super();
        this.state = {
            title: {},
            notes: [],
            deletedNotes: [],
            backupTraits: null,
            isEdit: false,
            componentDisabled: false
        }
        this.save = this.save.bind(this);
        this.enableEdit = this.enableEdit.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.addNote = this.addNote.bind(this);
        this.deleteNote = this.deleteNote.bind(this);
        this.onNoteChange = this.onNoteChange.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.getNewProperty = this.getNewProperty.bind(this);
    }

    componentWillMount(){
        this.generateTraits(this.props.traits)
    }

    componentWillReceiveProps(nextProps){
        if(this.props.traits !== nextProps.traits){
            this.generateTraits(nextProps.traits)
        }
    }

    generateTraits(traits){
        var newTitle={
                name: 'title',
                type: 'component',
                value: ''
            },
            newNotes = [];
        traits.map((trait)=>{
            if(trait.name === 'title'){
                newTitle = trait;
            }
            if(trait.name === 'note'){
                newNotes.push(trait)
            }
        })
        this.setState({title:newTitle, notes:newNotes});
    }

    enableEdit(){
        if(this.state.isEdit) return;
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(Object.assign({},note));
        });
        var currentItems = {
            title: Object.assign({},this.state.title),
            notes: newNotes
        };
        this.setState({isEdit: true, backupTraits:currentItems});
        if(this.props.changeComponentEditMode) {
            this.props.changeComponentEditMode(true);
        }
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backupTraits = this.state.backupTraits,
            oldTitle = backupTraits.title,
            oldNotes = backupTraits.notes;
        this.setState({isEdit: false, deletedNotes: [], title: oldTitle, notes: oldNotes});
        this.props.backToBaseProperties(this.props.cid);
        if(this.props.changeComponentEditMode) {
            this.props.changeComponentEditMode(false);
        }
        this.props.setEditMode(false)
    }

    addNote(){
        var newNotes = [];
        this.state.notes.map((note)=>{
            newNotes.push(note);
        })
        newNotes.push({
            name:'note',
            type: 'component',
            value:''
        })
        this.setState({notes:newNotes});
    }

    deleteNote(){
        var notes = this.state.notes;
        var deletedNotes = this.state.deletedNotes;
        var deletedNote = notes.pop();
        if(deletedNote.id){
            deletedNotes.push(deletedNote);
        }
        this.setState({notes:notes, deletedNotes:deletedNotes});
    }

    onNoteChange(index, value){
        var notes = this.state.notes;
        notes[index].value = value;
        this.setState({notes:notes});
    }

    onTitleChange(newValue){
        var title = this.state.title;
        title['value'] = newValue;
        this.setState({title:title});
    }

    getNewProperty(propertyName, currentState) {
        let propertyFromProps = this.props.component.traits.find(item => {
            return item.name === propertyName;
        });
        if(propertyFromProps) {
            const newProperty = Object.assign({}, propertyFromProps);
            newProperty.value = currentState[propertyName];
            return newProperty;
        }
        return  {
            name: propertyName,
            value: currentState[propertyName]
        }

    }

    save(){
        this.setState({isEdit:false, deletedNotes: []});
        var saveTraits = [];
        if(!this.props.hideProperties) {
            var visible = this.getNewProperty('visible', this.props.componentDropdownState['id'+this.props.cid]);
            var multiples = this.getNewProperty('multiples', this.props.componentDropdownState['id'+this.props.cid]);
            var hasDefault = this.getNewProperty('hasDefault', this.props.componentDropdownState['id'+this.props.cid]);
            var guestsAllow = this.getNewProperty('guestsAllow',this.props.componentDropdownState['id'+this.props.cid]);
            var selectedIds = this.getNewProperty('selectedIds', this.props);
            selectedIds.value = JSON.stringify(this.props.selectedIds);
            saveTraits.push(visible, multiples, hasDefault, guestsAllow, selectedIds);
        }
        saveTraits.push(this.state.title);
        this.state.notes.map((note)=>{
            saveTraits.push(note);
        })
        this.props.ApiSaveComponentTitleTraits(
            this.props.systemID,
            this.props.cid,
            this.state.deletedNotes,
            saveTraits
        );
        if(this.props.changeComponentEditMode) {
            this.props.changeComponentEditMode(false);
        }
        this.props.setEditMode(false)
    }

    render(){
        return(
            <div>
                <div className="bg-info-dark defaultWidth">
                    <Title title={this.state.title} cssClass="optionTitle" defaultID={this.props.cid} editing={this.state.isEdit} onTitleChange={this.onTitleChange}/>
                    {
                        this.state.notes.map((note, index) => {
                            return <Note key={index} note={note} cssClass="optionNote" editing={this.state.isEdit} index={index} onNoteChange={this.onNoteChange} />
                        })
                    }
                </div>
                { this.state.isEdit ?
                    <div className="editSave">
                        <Button onClick={this.save} className="btn btn-default">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default btn-outline ">Cancel</Button>
                        <Button onClick={this.addNote} className="btn btn-default">Add Note</Button>
                        { this.state.notes.length ?
                            <Button onClick={this.deleteNote} className="btn btn-default">Delete Note</Button> : null
                        }
                        {this.props.hideProperties ? null : <PropertiesButton cid={this.props.cid}/>}
                        <ComponentDeleteButton disabled={this.props.edit_mode} systemID={this.props.systemID} componentID={this.props.cid} />
                    </div>
                :
                    <div className="editSave">
                        <Button onClick={this.enableEdit} disabled={this.props.edit_mode} className="btn btn-default">Edit</Button>
                        <ComponentDeleteButton disabled={this.props.edit_mode} systemID={this.props.systemID} componentID={this.props.cid} />
                        {this.props.additionalButtons(this.props.edit_mode)}
                    </div>
                }
            </div>
        );
    }
}

ComponentTitle.contextTypes = {
    router: React.PropTypes.object.isRequired
};

ComponentTitle.propTypes = {
    component: React.PropTypes.object.isRequired
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        componentDropdownState: state.option.componentDropdownState
    }), Object.assign({}, componentAction, optionAction)),
    withProps(apiActions),
)(ComponentTitle);