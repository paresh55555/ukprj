import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {componentAction} from '../../actions';
import ComponentMenuItem from './ComponentMenuItem'
import AddComponentType from './AddComponentType'
import DeleteDialog from '../../components/Common/DeleteDialog'
import {
    Button,
} from 'react-bootstrap';

import {ApiComponent} from '../../api';
import { compose, withProps } from 'recompose';

class ComponentMenu extends React.Component {
    constructor() {
        super();
        this.state = {
            system: {},
            menuItems:[{
                name: 'Menu',
                type: 'menu',
                traits: []
            }],
            deletedItems: [],
            backupItems: null, // for "Cancel" functionality
            editing: false,
            loaded: false,
        };
        this.edit = this.edit.bind(this);
        this.save = this.save.bind(this);
        this.addMenu = this.addMenu.bind(this);
        this.deleteMenu = this.deleteMenu.bind(this);
        this.selectMenu = this.selectMenu.bind(this);
        this.cancelEdit = this.cancelEdit.bind(this);
        this.updateTraits = this.updateTraits.bind(this);
    }
    componentWillMount(){
        const system_id = this.props.systemID;
        if(this.props.data && this.props.data.length){
            this.generateMenuItems(this.props.data)
        }else{
            this.props.ApiGetMenuData(system_id);
        }
    }
    componentWillReceiveProps(nextProps){
        if(this.props.component.menuList !== nextProps.component.menuList){
            this.generateMenuItems(nextProps.component.menuList);
        }
        if(nextProps.component.isSaved != this.props.component.isSaved && nextProps.component.isSaved &&
            this.props.component.menuList !== nextProps.component.menuList){
            this.generateMenuItems(nextProps.component.menuList);
        }
    }

    generateMenuItems(items){
        if(items && items.length){
            this.props.setMenuData(items);
            var newState = {menuItems:items, loaded: true};
            if(this.props.selectedID === null || !items.find((el)=>{return el.id === this.props.selectedID})){
                this.props.onSetSelectedID(items[0].id);
            }
        }else{
            var newState = {menuItems:[{
                tmpid: 'tmp' + (new Date().getTime()),
                name: 'Menu',
                type: 'menu',
                traits: []
            }], loaded: true};
        }
        this.setState(newState);
    }

    edit(){
        var currentItems = [];
        this.state.menuItems.map((item)=>{
            currentItems.push(Object.assign({},item));
        })
        this.setState({editing:true, backupItems:currentItems});
        this.props.setEditMode(true)
    }

    cancelEdit(){
        var backup = this.state.backupItems;
        this.setState({editing:false,menuItems:backup});
        this.props.setEditMode(false)
    }

    addMenu(){
        var menuItems = this.state.menuItems,
            newItem = {
                tmpid: 'tmp' + (new Date().getTime()),
                name: 'Menu',
                type: 'menu',
                traits: []
            };
        menuItems.push(newItem)
        this.setState({menuItems:menuItems});
    }

    deleteMenu(){
        var self = this;
        var menuItems = self.state.menuItems;
        var menuTitle = '',
            idx = 0;
        $.each(menuItems, function(i, item){
            if(item && (item.id === self.props.selectedID ||
                item.tmpid === self.props.selectedID)){
                menuTitle = item.traits && item.traits[0] && item.traits[0].value ? item.traits[0].value : '< Add menu >';
                if(i > 0)idx = i - 1;
                return false;
            }
        });
        DeleteDialog({
            text: 'Are you sure you want to delete "' + menuTitle + '" ? ',
            onConfirm: function(){
                self.props.ApiDeleteComponent(+self.props.systemID, self.props.selectedID)
                    .then(()=>{
                        if(menuItems.length)self.props.onSetSelectedID(menuItems[idx].id);
                        swal("Deleted!", "Component has been deleted.", "success")
                    });
                var menuItems = self.state.menuItems;
                var deletedItem = null;
                $.each(menuItems, function(i, item){
                    if(item && (item.id === self.props.selectedID ||
                        item.tmpid === self.props.selectedID)){
                        deletedItem = item;
                        menuItems.splice(i,1);
                        return false;
                    }
                });
            }
        })
    }

    selectMenu(id){
        // this.setState({selectedID:id});
        this.props.onSetSelectedID(id);
    }


    updateTraits(traits, index){
        var menuItems = this.state.menuItems;
        if(!menuItems[index]){
            console.error('Menu item not exists. Index is ', index)
            return
        }
        menuItems[index].traits = traits;
        this.setState({menuItems:menuItems});
    }

    save(){
        var systemID = this.props.systemID;
        this.props.ApiSaveMenu(systemID, this.state.deletedItems, this.state.menuItems);
        this.setState({deletedItems: [], backupItems: null, editing:false,loaded:false});
        this.props.setEditMode(false)
    }
    getMenuTitle(){
        var title = ''
        const selectedMenu = this.state.menuItems.find(menu=>menu.id === this.props.selectedID)
        if(selectedMenu){
            title = selectedMenu.traits && selectedMenu.traits[0] && selectedMenu.traits[0].value ? selectedMenu.traits[0].value : '< Add menu >';
        }
        return title
    }
    render() {
        const menuItems = this.state.menuItems;
        var self = this;
        let showcomponentType = false;
        if(menuItems.find((menu)=>menu.id ==this.props.selectedID)){
            showcomponentType = this.props.selectedID && !/tmp/.test(this.props.selectedID);
        }
        const showAdd = menuItems.length < 7;
        return (
            <div>
                <div className="menuBox">
                    {
                        menuItems.length ?
                            menuItems.map((menuItem, index)=>{
                                var selected = false;
                                if(menuItem.id && (menuItem.id === self.props.selectedID) ||
                                    menuItem.tmpid && (menuItem.tmpid === self.props.selectedID)
                                    || self.props.selectedID === null && index === 0){
                                    selected = true;
                                }
                                return ([
                                    <ComponentMenuItem
                                        selected={selected}
                                        loaded={this.state.loaded}
                                        editing={this.state.editing}
                                        onSelectItem={this.selectMenu}
                                        onTraitsChange={this.updateTraits}
                                        data={menuItem} key={index} index={index} />,
                                    menuItems.length > 1 ? <div className="menuSpace"></div> : null
                                ])
                            })
                            :
                            <ComponentMenuItem editing={this.state.editing} onSelectItem={this.selectMenu} />
                    }
                </div>
                { this.state.editing ?
                    <div className="editSave">
                        <Button onClick={this.save} className="btn btn-default">Save</Button>
                        <Button onClick={this.cancelEdit} className="btn btn-default btn-outline ">Cancel</Button>
                        {showAdd && <Button onClick={this.addMenu} className="btn btn-default">Add Menu</Button>}
                        { this.state.menuItems.length > 1 ? <Button onClick={this.deleteMenu} className="btn btn-default">Delete {this.getMenuTitle()}</Button> : null}
                    </div>
                    :
                    <div className="editSave">
                        <Button onClick={this.edit} disabled={this.props.edit_mode} className="btn btn-default">Edit</Button>
                        { showcomponentType &&
                            <div style={{display:'inline-block'}}>
                                <AddComponentType doorTemplate={this.props.doorTemplate} edit_mode={this.props.component.edit_mode} systemID={this.props.systemID} menuID={this.props.selectedID} />
                            </div>
                        }
                    </div>
                }
            </div>
        );
    }

}

ComponentMenu.contextTypes = {
    router: React.PropTypes.object.isRequired
};

ComponentMenu.propTypes = {
    ApiGetMenuData: PropTypes.func.isRequired,
};

const apiActions = Object.assign({}, ApiComponent);

export default compose(
    connect(state => ({
        component: state.component,
    }), Object.assign({}, componentAction)),
    withProps(apiActions),
)(ComponentMenu);