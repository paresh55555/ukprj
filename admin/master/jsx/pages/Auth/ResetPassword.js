import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {Auth} from '../../services/authService'
import {authAction} from '../../actions';
import {ApiLogin} from '../../api';

class ResetPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            disabled:false, 
            error:'Password and Confirm Password dosen\'t match',
            showError:'',
            formType:'reset'
        };
        this._reset = this._reset.bind(this)
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.auth != this.props.auth){
            this.setState({showError:nextProps.auth.error, disabled: false})
        }
    }
    componentWillMount() {
        localStorage.clear();
        sessionStorage.clear();
    }
    _reset(e){
        e.preventDefault();
        const password = this.refs.password.value;
        const confirm_password = this.refs.confirm_password.value;
        if(!password || !confirm_password || (confirm_password !== password)){
            return this.setState({showError:true})
        }
        this.setState({disabled:true})
        const {query} = this.props.routing.locationBeforeTransitions;
        const data = Object.assign(query, {password:password})        

        const {dispatch} = this.props;
        console.log(ApiLogin, data)
        ApiLogin.ApiResetPassword(dispatch,data)
            .then(res=>{
                this.setState({formType:'resetSuccess',disabled:false})
            })
    }
    onFocus(){
        if(this.state.showError){
            this.setState({showError:false})
        }
    }
    initLogin(){
        this.context.router.push('/login')
    }
   
    render() {
        const {formType} = this.state
        let title = formType === 'reset' ?'Enter New Password' :'Password Updated ';
        return (
            <div className="block-center mt-xl wd-xl">
                { /* START panel */ }
                <div className="panel panel-dark panel-flat">
                    <div className="panel-heading text-center">
                        <a href="#">
                            Sales Quoter
                        </a>
                    </div>
                    <div className="panel-body">
                        <p className="text-center pv">{title}</p>
                        <form role="form" data-parsley-validate="" noValidate className="mb-lg">
                            {formType === 'reset' && <div className="form-group has-feedback">
                                <input id="passwrod" ref="password" type="password" placeholder="Passwrod" autoComplete="off" required="required" className="form-control"  onFocus={this.onFocus.bind(this)}/>
                                <span className="fa fa-lock form-control-feedback text-muted"></span>
                            </div>}
                            {formType === 'reset' &&<div className="form-group has-feedback">
                                <input id="confirm_passwrod" ref="confirm_password" type="password" placeholder="Confirm Password" required="required" className="form-control"  onFocus={this.onFocus.bind(this)}/>
                                <span className="fa fa-lock form-control-feedback text-muted"></span>
                            </div>}
                            {this.state.showError ? <p className="alert alert-danger">{this.state.error}</p>:null}
                            {formType !== 'reset' && <button type="button" className="btn btn-block btn-primary mt-lg" onClick={this.initLogin.bind(this)} disabled={this.state.disabled}>Login</button>}
                            {this.state.showError && formType !== 'reset' ? <p style={{marginTop:55}}>
                                <button type="button" onClick={this.initReset.bind(this)} className="btn btn-default btn-block mt-lg" disabled={this.state.disabled}>Reset Password?</button>
                            </p>:null 
                            }
                            {formType == 'reset'?
                              <div>
                                <button type="button" onClick={this._reset.bind(this)} disabled={this.state.disabled} className="btn btn-block btn-primary mt-lg">Change Password</button>
                                 <p style ={{marginTop:55}}><button type="button" className="btn btn-default btn-block mt-lg" onClick={this.initLogin.bind(this)}>Cancel</button></p>
                              </div>
                              :null
                            }
                        </form>
                    </div>
                </div>
                { /* END panel */ }
            </div>
        );
    }


}
ResetPassword.contextTypes = {
    router: React.PropTypes.object.isRequired
};
ResetPassword.propTypes = {
    ApiLogin: PropTypes.func.isRequired

};

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, ApiLogin, dispatch);
    return allActionProps;
}

export default connect(
    state => ({
        auth: state.auth
    }, mapDispatch)  )(ResetPassword);


