import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {Auth} from '../../services/authService'
import {authAction} from '../../actions';
import {ApiLogin} from '../../api';

class Login extends React.Component {

    constructor(props) {
        super(props);
        self = this;
        this.state = {
            auth:{username:'',password:''},
            disabled:false, 
            error:'Wrong Email or Password',
            resetError:'Email not found',
            showError:'',
            formType:'main'
        };

        this._login = this._login.bind(this)
    }
    componentWillReceiveProps(nextProps){
        if (nextProps.auth.action == 'Login') {
            if(nextProps.auth.data.type === 'service'){
                
                if ( window.innerWidth <= 640 ) {
                    this.context.router.push('/serviceTechJobs');
                } else {
                    this.context.router.push('/service/orders');
                }

            }else{
                this.context.router.push('/users');
            }
        }

        if(nextProps.auth != this.props.auth){
            this.setState({showError:nextProps.auth.error, disabled: false})
        }
    }

    componentWillMount() {
        localStorage.clear();
        sessionStorage.clear();
    }

    _login(e){
        e.preventDefault();
        if(this.state.formType === 'resetSuccess'){
            return this.setState({formType:'main'})
        }
        this.setState(
            {"disabled": true}
        )

        var username = this.refs.username.value;
        var password = this.refs.password.value;
        var auth = {'username':username, 'password':password}

        const {dispatch, ApiLogin} = this.props;

        ApiLogin(dispatch, auth);
    }

    onFocus(){
        if(this.state.showError){
            this.setState({showError:false})
        }
    }

    initReset(){
        this.setState({formType:'reset', "showError" : false, disabled:false})
    }
    initLogin(){
        this.setState({formType:'main', "showError" : false, disabled:false})
    }
    onReset(){
        var username = this.refs.username.value;
        const {dispatch} = this.props;
        this.setState(
            {"disabled": true}
        )
       ApiLogin.ApiReset(dispatch,{email:username})
             .then(res=>{
                 if(res){
                 this.setState({formType:'resetSuccess',disabled:false})
             }
         })
    }
    render() {
        const {formType} = this.state
        let title = formType === 'reset' ?'ENTER EMAIL TO RESET PASSWORD' : formType ==='resetSuccess' ? 'RESET EMAIL SENT' : 'SIGN IN TO CONTINUE' ;
        return (
            <div className="block-center mt-xl wd-xl">
                { /* START panel */ }
                <div className="panel panel-dark panel-flat">
                    <div className="panel-heading text-center">
                        <a href="#">
                            Sales Quoter
                        </a>
                    </div>
                    <div className="panel-body">
                        <p className="text-center pv">{title}</p>
                        <form role="form" data-parsley-validate="" noValidate className="mb-lg">
                            {formType !== 'resetSuccess' && <div className="form-group has-feedback">
                                <input id="exampleInputEmail1" ref="username" type="text" placeholder={formType === 'main' ? 'Username' : 'Email'} autoComplete="off" required="required" className="form-control"  onFocus={this.onFocus.bind(this)}/>
                                <span className="fa fa-envelope form-control-feedback text-muted"></span>
                            </div>}
                            {formType === 'main' &&<div className="form-group has-feedback">
                                <input id="exampleInputPassword1" ref="password" type="password" placeholder="Password" required="required" className="form-control"  onFocus={this.onFocus.bind(this)}/>
                                <span className="fa fa-lock form-control-feedback text-muted"></span>
                            </div>}
                            {this.state.showError ? <p className="alert alert-danger">{formType === 'main' ? this.state.error : this.state.resetError}</p>:null}
                            {formType !== 'reset' && <button type="submit" className="btn btn-block btn-primary mt-lg" onClick={this._login} disabled={this.state.disabled}>Login</button>}
                            {this.state.showError && formType !== 'reset' ? <p style={{marginTop:55}}>
                                <button type="button" onClick={this.initReset.bind(this)} className="btn btn-default btn-block mt-lg" disabled={this.state.disabled}>Reset Password?</button>
                            </p>:null 
                            }
                            {formType == 'reset'?
                              <div>
                                <button type="button" onClick={this.onReset.bind(this)} disabled={this.state.disabled} className="btn btn-block btn-primary mt-lg">Reset Password</button>
                                 <p style ={{marginTop:55}}><button type="button" className="btn btn-default btn-block mt-lg" onClick={this.initLogin.bind(this)}>Cancel</button></p>
                              </div>
                              :null
                            }
                        </form>
                    </div>
                </div>
                { /* END panel */ }
            </div>
        );
    }


}
Login.contextTypes = {
    router: React.PropTypes.object.isRequired
};
Login.propTypes = {
    ApiLogin: PropTypes.func.isRequired

};

const mapDispatch = (dispatch) => {
    const allActionProps = Object.assign({}, ApiLogin, dispatch);
    return allActionProps;
}

export default connect(
    state => ({
        auth: state.auth
    }, mapDispatch)  )(Login);


