import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import { cartAction } from '../../actions/index';
import { apiQuotes } from '../../api/index';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import LinkBox from '../../components/linkbox/LinkBox';

class Carts extends Component {
    componentWillMount() {
        const { getQuotes } = this.props;
        getQuotes(this.props.params.type);
    }
    render() {
        const { carts, setCurrentElement } = this.props;
        const {type} = this.props.params
        return (
            <ContentWrapper>
                <h3 style={{textTransform:'capitalize'}}>{type} Builder</h3>
                <Button style={{"marginBottom":"10px", textTransform:'capitalize'}} onClick={() => this.context.router.push('builder/'+type+'/newquote')}>
                    {'Add '+type}
                </Button>
                <Grid fluid>
                    <Row>
                        {carts.length > 0 ?
                            carts.map(
                                cart => (
                                    <Col md={2} sm={4} key={cart.id}>
                                        <span role="button" tabIndex={0} onClick={() => setCurrentElement(cart)}>
                                            <LinkBox
                                                onClick={this.onClick}
                                                url={`builder/${type}/${cart.id}`}
                                                params={{ name: cart.name }}
                                                title={cart.name}
                                            />
                                        </span>
                                    </Col>
                                ),
                            )
                            : null
                        }
                       
                    </Row>
                </Grid>
            </ContentWrapper>
        );
    }
}
Carts.contextTypes = {
    router: React.PropTypes.object.isRequired,
};
Carts.propTypes = {
    getQuotes: PropTypes.func.isRequired,
    setCurrentElement: PropTypes.func.isRequired,
    carts: PropTypes.array.isRequired,
};

export default compose(
    connect(state => ({
        carts: state.cart.carts,
    }), cartAction),
    withProps(apiQuotes),
)(Carts);