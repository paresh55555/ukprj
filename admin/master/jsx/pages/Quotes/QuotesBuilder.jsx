import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { cartAction } from '../../actions/index';
import { apiQuotes } from '../../api/index';
import Loader from '../../components/loader/Loader';
import LineLayout from '../Cart/LineLayout';
import BoxLayout from '../Cart/BoxLayout';
import LayoutNameEdit from '../Cart/LayoutNameEdit';
import EditButtonsLayout from '../Cart/EditButtonsLayout';
import { generateColumns, findNextOrder } from '../Cart/helpers';

class QuotesBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
        };
        this.changeName = this.changeName.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.changeOrders = this.changeOrders.bind(this);
        this.addBoxes = this.addBoxes.bind(this);
        this.saveLayouts = this.saveLayouts.bind(this);
        this.backToQuotesPage = this.backToQuotesPage.bind(this);
    }
    componentWillMount() {
        const { clearLayout } = this.props;
        clearLayout();
    }
    onSave() {
        const { saveQuotes, finishSaving } = this.props;
        if (!this.state.name) {
            swal({
                title: 'Name field is required!',
                text: 'Please, fill the name field!',
                type: 'warning',
            });
        } else {
            console.logI
            saveQuotes(this.props.params.type,{ name: this.state.name })
                .then(
                    (data) => {
                        const saveLayoutRequest = this.saveLayouts(data.id);
                        saveLayoutRequest
                            .then(
                                () => {
                                    this.backToQuotesPage();
                                    finishSaving();
                                },
                            );
                    });
        }
    }
    onCancel() {
        this.backToQuotesPage();
    }
    backToQuotesPage() {
        this.context.router.push('/builder/'+this.props.params.type);
    }
    saveLayouts(cartId) {
        const { saveQuotesLayout, cartsLayout } = this.props;
        const requests = cartsLayout.map((newLayout) => (
            saveQuotesLayout(
                cartId,
                newLayout,
                { order: newLayout.order },
                newLayout.isHorizontalLine,
            )
        ));
        return Promise.all(requests);
    }
    changeName(e) {
        this.setState({ name: e.target.value });
    }
    addBoxes(number, isHorizontalLine) {
        const { saveElementLayoutAction, cartsLayout } = this.props;
        const columnsArray = generateColumns(number);
        if (isHorizontalLine) {
            const horizontalLineObj = {
                component_type: 'horizontalLine',
                item: 'spacing',
                label: 'horizontalLine',
            };
            columnsArray[0].items.push(horizontalLineObj);
        }
        const order = findNextOrder(cartsLayout);
        const layout = {
            order,
            id: order,
            number_of_columns: number,
            columns: columnsArray,
            isNew: true,
            isHorizontalLine,
        };
        saveElementLayoutAction(layout);
    }
    changeOrders(selectedId, dropedId) {
        const { cartsLayout } = this.props;
        const cartsLayoutCopy = [...cartsLayout];
        const selectedObject = Object.assign({}, cartsLayout[selectedId]);
        const dropedObject = Object.assign({}, cartsLayout[dropedId]);
        const selectedOrder = selectedObject.order;
        const dropedOrder = dropedObject.order;
        dropedObject.order = selectedOrder;
        selectedObject.order = dropedOrder;
        cartsLayoutCopy[selectedId] = dropedObject;
        cartsLayoutCopy[dropedId] = selectedObject;
        return cartsLayoutCopy;
    }
    render() {
        const { name } = this.state;
        const {
            cartsLayout,
            changeEditMode,
            changeElementPosition,
            changeSingleEditMode,
            updateColumnQuotesLayout,
            deleteElementLayoutAction,
            isSaving,
        } = this.props;
        const type = this.props.params.type
        return (
            <ContentWrapper>
                <h3 style={{textTransform:'capitalize'}}>{type} Builder</h3>
                {!isSaving ?
                    <div>
                        <LayoutNameEdit name={name} changeName={this.changeName} fieldName={type} placeholder={type+' name'}/>
                        <div className="panel-default panel mainBox">
                            <div className="panel-body">
                                {cartsLayout.map((cart, i) => (
                                    <LineLayout
                                        key={cart.id}
                                        changePosition={changeElementPosition}
                                        changeOrders={this.changeOrders}
                                        id={i}
                                        layoutId={cart.id}
                                        cartId={0}
                                        isEdit
                                        isSingleEdit={false}
                                        onDelete={deleteElementLayoutAction}
                                    >
                                        {cart.columns.map(column => (
                                            <BoxLayout
                                                key={column.id}
                                                id={column.id}
                                                layoutId={cart.id}
                                                cartId={0}
                                                isEdit
                                                isSingleEdit={false}
                                                isSaving={isSaving}
                                                column={column}
                                                openModal={() => {}}
                                                col={12 / cart.columns.length}
                                                changeSingleEditMode={changeSingleEditMode}
                                                updateColumn={updateColumnQuotesLayout}
                                                saveItemsRequest={() => {}}
                                                updateItemsRequest={() => {}}
                                            />
                                        ))}
                                    </LineLayout>
                                ))
                                }
                            </div>
                        </div>
                        <EditButtonsLayout
                            isEdit
                            isSingleEdit={false}
                            changeEditMode={changeEditMode}
                            onDeleteCart={() => {}}
                            onSave={this.onSave}
                            onCancel={this.onCancel}
                            addBoxes={this.addBoxes}
                        />
                    </div>
                    : <Loader />
                }
            </ContentWrapper>
        );
    }
}

QuotesBuilder.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

QuotesBuilder.propTypes = {
    saveQuotes: PropTypes.func.isRequired,
    changeElementPosition: PropTypes.func.isRequired,
    changeEditMode: PropTypes.func.isRequired,
    changeCurrentElementName: PropTypes.func.isRequired,
    deleteElementLayoutAction: PropTypes.func.isRequired,
    changeSingleEditMode: PropTypes.func.isRequired,
    saveElementLayoutAction: PropTypes.func.isRequired,
    clearLayout: PropTypes.func.isRequired,
    saveQuotesLayout: PropTypes.func.isRequired,
    closeEditModes: PropTypes.func.isRequired,
    updateColumnQuotesLayout: PropTypes.func.isRequired,
    finishSaving: PropTypes.func.isRequired,
    backToPreviousLayout: PropTypes.func.isRequired,
    cartsLayout: PropTypes.array.isRequired,
    isSaving: PropTypes.bool.isRequired,
};

export default compose(
    connect(state => ({
        cartsLayout: state.cart.cartsLayout,
        isSingleEdit: state.cart.isSingleEdit,
        isSaving: state.cart.isSaving,
    }), cartAction),
    withProps(apiQuotes),
)(QuotesBuilder);

