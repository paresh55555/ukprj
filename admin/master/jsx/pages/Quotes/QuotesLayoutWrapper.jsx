import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { cartAction } from '../../actions/index';
import { apiQuotes } from '../../api/index';
import QuotesLayout from './QuotesLayout';

class QuotesLayoutWrapper extends Component {
    constructor(props) {
        super(props);
        this.backToQuotesPage = this.backToQuotesPage.bind(this);
    }
    componentWillMount() {
        const { params, getQuotesLayouts, currentCart, setCurrentElement } = this.props;
        if (currentCart) {
            getQuotesLayouts(currentCart.id);
        } else {
            const cartFromStorage = JSON.parse(sessionStorage.getItem('currentCart'));
            const idFromParams = +params.id;
            if (cartFromStorage && cartFromStorage.id === idFromParams) {
                setCurrentElement(cartFromStorage);
                getQuotesLayouts(cartFromStorage.id);
            }
        }
    }
    backToQuotesPage() {
        this.context.router.push('/builder/'+this.props.params.type);
    }
    render() {
        const {type} = this.props.params;
        return (
            <QuotesLayout
                backToQuotesPage={this.backToQuotesPage}
                type={type}
            />
        );
    }
}

QuotesLayoutWrapper.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

QuotesLayoutWrapper.propTypes = {
    params: PropTypes.object.isRequired,
    currentCart: PropTypes.object,
    getQuotesLayouts: PropTypes.func.isRequired,
    setCurrentElement: PropTypes.func.isRequired,
};

export default compose(
    connect(state => ({
        cartsLayout: state.cart.cartsLayout,
        currentCart: state.cart.currentCart,
    }), cartAction),
    withProps(apiQuotes),
)(QuotesLayoutWrapper);
