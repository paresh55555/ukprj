import React, { PropTypes } from 'react';
import { compose, withState } from 'recompose';
import { Button } from 'react-bootstrap';

function AddQuotesElement(props) {
    const {
        addItem,
        quoteInfoWithLabel,
        changeQuoteInfoLabel,
        companyInfoWithLabel,
        changeCompanyInfoLabel,
        customerInfoWithLabel,
        changeCustomerInfoLabel,
    } = props;
    const onChangeQuoteInfoLabel = () => {
        if (!quoteInfoWithLabel) {
            changeQuoteInfoLabel(true);
        } else {
            changeQuoteInfoLabel(false);
        }
    };
    const onChangeCompanyInfoLabel = () => {
        if (!companyInfoWithLabel) {
            changeCompanyInfoLabel(true);
        } else {
            changeCompanyInfoLabel(false);
        }
    };
    const onChangeCustomerInfoLabel = () => {
        if (!customerInfoWithLabel) {
            changeCustomerInfoLabel(true);
        } else {
            changeCustomerInfoLabel(false);
        }
    };
    return (
        <div>
            <div className="form-group">
                <div className="control-label">Logo:</div>
                <Button onClick={() => addItem('Large Logo', 'logo')} className="btn-primary">Large Logo</Button>
                <Button onClick={() => addItem('Medium Logo', 'logo')} className="btn-primary">Medium Logo</Button>
                <Button onClick={() => addItem('Small Logo', 'logo')} className="btn-primary">Small Logo</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Quote Info:<span className="with-label"><input onClick={onChangeQuoteInfoLabel} type="checkbox" /> With label</span></div>
                <Button onClick={() => addItem('Quote Number', `text${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Quote Number</Button>
                <Button onClick={() => addItem('Quote Date', `text${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Quote Date</Button>
                <Button onClick={() => addItem('PO Field', `inputField${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">PO #</Button>
                <Button onClick={() => addItem('Small Text', `text${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Estimate Completion</Button>
                <Button onClick={() => addItem('Text Box', `text${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Pickup / Delivery</Button>
                <Button onClick={() => addItem('Text Box', `text${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Comments</Button>
                <Button onClick={() => addItem('Text Box', `text${quoteInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Sales Rep</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Company Info:<span className="with-label"><input onClick={onChangeCompanyInfoLabel} type="checkbox" /> With label</span></div>
                <Button onClick={() => addItem('Name Field', `inputField${companyInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Name</Button>
                <Button onClick={() => addItem('Address 1 Field', `inputField${companyInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Address 1</Button>
                <Button onClick={() => addItem('Address 2 Field', `inputField${companyInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Address 2</Button>
                <Button onClick={() => addItem('City Field', `inputField${companyInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">City</Button>
                <Button onClick={() => addItem('State Field', `inputField${companyInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">State</Button>
                <Button onClick={() => addItem('Zip Field', `inputField${companyInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Zip</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Customer Info:<span className="with-label"><input onClick={onChangeCustomerInfoLabel} type="checkbox" /> With label</span></div>
                <Button onClick={() => addItem('Company Name Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Company Name</Button>
                <Button onClick={() => addItem('Billing Address 1 Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing Address 1</Button>
                <Button onClick={() => addItem('Billing Address 2 Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing Address 2</Button>
                <Button onClick={() => addItem('Billing City Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing City</Button>
                <Button onClick={() => addItem('Billing State Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing State</Button>
                <Button onClick={() => addItem('Billing Zip Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing Zip</Button>
                <Button onClick={() => addItem('Billing Email Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing Email</Button>
                <Button onClick={() => addItem('Billing Phone Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Billing Phone</Button>
                <Button onClick={() => addItem('Shipping Name Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping Name</Button>
                <Button onClick={() => addItem('Shipping Address 1 Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping Address 1</Button>
                <Button onClick={() => addItem('Shipping Address 2 Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping Address 2</Button>
                <Button onClick={() => addItem('Shipping City Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping City</Button>
                <Button onClick={() => addItem('Shipping State Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping State</Button>
                <Button onClick={() => addItem('Shipping Zip Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping Zip</Button>
                <Button onClick={() => addItem('Shipping Email Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping Email</Button>
                <Button onClick={() => addItem('Shipping Phone Field', `inputField${customerInfoWithLabel ? 'WithLabel' : ''}`)} className="btn-primary">Shipping Phone</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Text:</div>
                <Button onClick={() => addItem('Large Title', 'text')} className="btn-primary">Large Title</Button>
                <Button onClick={() => addItem('Bold Title', 'text')} className="btn-primary">Bold Title</Button>
                <Button onClick={() => addItem('Simple Text', 'text')} className="btn-primary">Text</Button>
                <Button onClick={() => addItem('Small Text', 'text')} className="btn-primary">Small Text</Button>
                <Button onClick={() => addItem('Text Box', 'text')} className="btn-primary">Text Box</Button>
            </div>
            <div className="form-group">
                <div className="control-label">Spacing:</div>
                <Button onClick={() => addItem('Large Space', 'spacing')} className="btn-primary">Large Space</Button>
                <Button onClick={() => addItem('Small Space', 'spacing')} className="btn-primary">Small Space</Button>
                <Button onClick={() => addItem('Horizontal Line', 'spacing')} className="btn-primary">Horizontal Line</Button>
            </div>
        </div>
    );
}

AddQuotesElement.propTypes = {
    addItem: PropTypes.func.isRequired,
    changeQuoteInfoLabel: PropTypes.func.isRequired,
    changeCompanyInfoLabel: PropTypes.func.isRequired,
    changeCustomerInfoLabel: PropTypes.func.isRequired,
    quoteInfoWithLabel: PropTypes.bool.isRequired,
    companyInfoWithLabel: PropTypes.bool.isRequired,
    customerInfoWithLabel: PropTypes.bool.isRequired,
};

export default compose(
    withState('quoteInfoWithLabel', 'changeQuoteInfoLabel', false),
    withState('companyInfoWithLabel', 'changeCompanyInfoLabel', false),
    withState('customerInfoWithLabel', 'changeCustomerInfoLabel', false),
)(AddQuotesElement);
