import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { generateColumns, findNextOrder, generateSendingItem, confirmMessage } from '../Cart/helpers';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { cartAction } from '../../actions/index';
import { apiQuotes } from '../../api/index';
import LineLayout from '../Cart/LineLayout';
import BoxLayout from '../Cart/BoxLayout';
import LayoutNameEdit from '../Cart/LayoutNameEdit';
import EditButtonsLayout from '../Cart/EditButtonsLayout';
import ModalCart from '../Cart/ModalCart';
import AddQuotesElement from './AddQuotesElement';
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';


class QuotesLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: props.cartName,
            isOptionModal: false,
            isImageOption: false,
        };
        this.changeName = this.changeName.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onDeleteCart = this.onDeleteCart.bind(this);
        this.changeOrders = this.changeOrders.bind(this);
        this.addBoxes = this.addBoxes.bind(this);
        this.saveLayouts = this.saveLayouts.bind(this);
        this.updateIfOrderChanged = this.updateIfOrderChanged.bind(this);
        this.deleteLayouts = this.deleteLayouts.bind(this);
        this.addItem = this.addItem.bind(this);
        this.changeOptionModal = this.changeOptionModal.bind(this);
        this.onCancelOptionModal = this.onCancelOptionModal.bind(this);
        this.onDeleteRow = this.onDeleteRow.bind(this);
    }
    componentWillUnmount() {
        const { closeEditModes } = this.props;
        closeEditModes();
    }
    onSave() {
        const { cartId, cartName, updateQuotes, changeEditMode, changeCurrentElementName, startSaving, finishSaving } = this.props;
        if (cartName !== this.state.name) {
            changeCurrentElementName(this.state.name);
            updateQuotes({ id: cartId, name: this.state.name });
        }
        startSaving();
        this.saveLayouts()
            .then(() => {
                this.deleteLayouts()
                    .then(() => {
                        finishSaving()
                        this.updateIfOrderChanged();
                    });
            });
        changeEditMode();
    }
    onDeleteCart() {
        const { deleteQuotes, cartId, backToQuotesPage } = this.props;
        const callback = () => {
            deleteQuotes(cartId);
            backToQuotesPage();
        };
        confirmMessage('quote', callback);
    }
    onDeleteRow(withoutEdit, ...args) {
        const { deleteElementLayoutAction, deleteQuotesLayout } = this.props;
        const callback = () => {
            if (withoutEdit) {
                deleteQuotesLayout(args[0], args[1], args[2]);
            } else {
                deleteElementLayoutAction(args[0]);
            }
        };
        confirmMessage('quote row', callback);
    }
    onCancel() {
        const { backToPreviousLayout } = this.props;
        backToPreviousLayout();
    }
    onCancelOptionModal() {
        const { changeModalState } = this.props;
        this.setState({ isOptionModal: false, isImageOption: false });
        changeModalState();
    }
    updateIfOrderChanged() {
        const { cartId, cartsLayout, cartsLayoutCopy, updateQuotesLayout } = this.props;
        cartsLayout.forEach((newLayout) => {
            cartsLayoutCopy.forEach((oldLayout) => {
                if (!newLayout.isNew && oldLayout.id === newLayout.id && oldLayout.order !== newLayout.order) { //
                    updateQuotesLayout(cartId, newLayout.id, { order: newLayout.order });
                }
            });
        });
    }
    saveLayouts() {
        const { cartId, saveQuotesLayout, cartsLayout, cartsLayoutCopy } = this.props;
        const requests = [];
        cartsLayout.forEach((newLayout) => {
            const isPreviousExist = cartsLayoutCopy.find(item => item.id === newLayout.id);
            if (!isPreviousExist) {
                requests.push(
                    saveQuotesLayout(
                        cartId,
                        newLayout,
                        { order: newLayout.order },
                        newLayout.isHorizontalLine,
                    ),
                );
            }
        });
        return Promise.all(requests);
    }
    deleteLayouts() {
        const { cartId, cartsLayout, cartsLayoutCopy, deleteQuotesLayout } = this.props;
        const requests = [];
        cartsLayoutCopy.forEach((oldLayout) => {
            const isLayoutStillExist = cartsLayout.find(item => item.id === oldLayout.id);
            if (!isLayoutStillExist) {
                requests.push(
                    deleteQuotesLayout(cartId, oldLayout.id, false),
                );
            }
        });
        return Promise.all(requests);
    }
    changeName(e) {
        this.setState({ name: e.target.value });
    }
    changeOptionModal(isImage) {
        this.setState({ isOptionModal: !this.state.isOptionModal, isImageOption: isImage });
    }
    addBoxes(number, isHorizontalLine) {
        const { saveElementLayoutAction, cartsLayout } = this.props;
        const columnsArray = generateColumns(number);
        if (isHorizontalLine) {
            const horizontalLineObj = {
                component_type: 'horizontalLine',
                item: 'spacing',
                label: 'horizontalLine',
            };
            columnsArray[0].items.push(horizontalLineObj);
        }
        const order = findNextOrder(cartsLayout);
        const layout = {
            order,
            id: order,
            number_of_columns: number,
            columns: columnsArray,
            isNew: true,
            isHorizontalLine,
        };
        saveElementLayoutAction(layout);
    }
    changeOrders(selectedId, dropedId) {
        const { cartsLayout } = this.props;
        const cartsLayoutCopy = [...cartsLayout];
        const selectedObject = Object.assign({}, cartsLayout[selectedId]);
        const dropedObject = Object.assign({}, cartsLayout[dropedId]);
        const selectedOrder = selectedObject.order;
        const dropedOrder = dropedObject.order;
        dropedObject.order = selectedOrder;
        selectedObject.order = dropedOrder;
        cartsLayoutCopy[selectedId] = dropedObject;
        cartsLayoutCopy[dropedId] = selectedObject;
        return cartsLayoutCopy;
    }
    addItem(name, item) {
        const { addItemAction, currentColumn } = this.props;
        const data = generateSendingItem(name, item, this.state.isImageOption);
        addItemAction(currentColumn.layoutId, currentColumn.id, data);
        this.onCancelOptionModal();
    }
    render() {
        const { name } = this.state;

        const {
            cartId,
            cartsLayout,
            changeEditMode,
            isEdit,
            changeElementPosition,
            cartName,
            isSingleEdit,
            changeModalState,
            showModal,
            isSaving,
            saveItemColumnLayout,
            updateColumnQuotesLayout,
            type
        } = this.props;
        const breadCrumbData = [
                {
                    title: type+' Builder', 
                    url:'/builder/'+type,
                    active:false
                },
                {
                    title : cartName,
                    url:'',
                    active:true
                }

        ]
        return (
            <ContentWrapper>
                <Breadcrumbs data={breadCrumbData} />
                
                {isEdit ?
                    <LayoutNameEdit name={name} changeName={this.changeName} fieldName="Quote" />
                    : null
                }
                <div className="panel-default panel mainBox">
                    <div className="panel-body">
                        {cartsLayout.map((cart, i) => (
                            <LineLayout
                                key={cart.id}
                                changePosition={changeElementPosition}
                                changeOrders={this.changeOrders}
                                id={i}
                                layoutId={cart.id}
                                cartId={cartId}
                                isEdit={isEdit}
                                isSingleEdit={isSingleEdit}
                                onDelete={this.onDeleteRow}
                            >
                                {cart.columns.map(column => (
                                    <BoxLayout
                                        key={column.id}
                                        id={column.id}
                                        layoutId={cart.id}
                                        cartId={cartId}
                                        isEdit={isEdit}
                                        isSingleEdit={isSingleEdit}
                                        openModal={changeModalState}
                                        column={column}
                                        col={12 / cart.columns.length}
                                        isSaving={isSaving}
                                        saveItemsRequest={saveItemColumnLayout}
                                        updateItemsRequest={updateColumnQuotesLayout}
                                    />
                                ))}
                            </LineLayout>
                        ))}
                    </div>
                </div>
                <EditButtonsLayout
                    isEdit={isEdit}
                    isSingleEdit={isSingleEdit}
                    changeEditMode={changeEditMode}
                    onDeleteCart={this.onDeleteCart}
                    onSave={this.onSave}
                    onCancel={this.onCancel}
                    addBoxes={this.addBoxes}
                    type={type}
                />
                <ModalCart
                    show={showModal}
                    onClose={this.onCancelOptionModal}
                    title={'Add Element'}
                >
                    <AddQuotesElement
                        addItem={this.addItem}
                        changeOptionModal={this.changeOptionModal}
                    />
                </ModalCart>
            </ContentWrapper>
        );
    }
}

QuotesLayout.propTypes = {
    changeEditMode: PropTypes.func.isRequired,
    changeElementPosition: PropTypes.func.isRequired,
    changeCurrentElementName: PropTypes.func.isRequired,
    deleteElementLayoutAction: PropTypes.func.isRequired,
    cartsLayoutCopy: PropTypes.array.isRequired,
    currentColumnItemsCopy: PropTypes.array.isRequired,
    saveElementLayoutAction: PropTypes.func.isRequired,
    saveQuotesLayout: PropTypes.func.isRequired,
    closeEditModes: PropTypes.func.isRequired,
    deleteQuotes: PropTypes.func.isRequired,
    updateQuotes: PropTypes.func.isRequired,
    backToPreviousLayout: PropTypes.func.isRequired,
    addItemAction: PropTypes.func.isRequired,
    changeModalState: PropTypes.func.isRequired,
    backToQuotesPage: PropTypes.func.isRequired,
    updateColumnQuotesLayout: PropTypes.func.isRequired,
    saveItemColumnLayout: PropTypes.func.isRequired,
    finishSaving: PropTypes.func.isRequired,
    startSaving: PropTypes.func.isRequired,
    deleteQuotesLayout: PropTypes.func.isRequired,
    isEdit: PropTypes.bool.isRequired,
    isSingleEdit: PropTypes.bool.isRequired,
    showModal: PropTypes.bool.isRequired,
    isSaving: PropTypes.bool.isRequired,
    cartsLayout: PropTypes.array.isRequired,
    cartName: PropTypes.string.isRequired,
    cartId: PropTypes.number.isRequired,
};

export default compose(
    connect(state => ({
        cartsLayout: state.cart.cartsLayout,
        cartsLayoutCopy: state.cart.cartsLayoutCopy,
        isEdit: state.cart.isEdit,
        isSingleEdit: state.cart.isSingleEdit,
        cartId: state.cart.currentCart.id,
        cartName: state.cart.currentCart.name,
        showModal: state.cart.showModal,
        currentColumn: state.cart.currentColumn,
        isSaving: state.cart.isSaving,
        currentColumnItemsCopy: state.cart.currentColumnItemsCopy,
    }), cartAction),
    withProps(apiQuotes),
)(QuotesLayout);

