import React, { Component, PropTypes } from 'react';
import autobind from 'autobind-decorator'
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Panel } from 'react-bootstrap';
import Menu from '../../components/Menu/Menu'

class MenuPage extends React.Component {
	constructor() {
		super();
		self = this;
		const menu = ['Size & Panel','Color / Finish', 'Glass', 'Hardware']
		this.state = {active:0,menu:menu};
		this.onClicks = this.onClicks.bind(this);
		this.addMenu = this.addMenu.bind(this);
	}

	onClicks(i){
		this.setState({active:i})
	}
	addMenu(i){
		let menu = this.state.menu;
		menu.push(`Menu ${menu.length+1}`)
		this.setState({menu:menu})
	}
	render() {
		var self = this
		const {menu, active} = this.state;
		return (
			<ContentWrapper>
				<h3>Menu Component</h3>
				<Grid fluid>
					<Row>
						<Col sm={12}>
							<Menu  menus={menu} active={active} onClicks={this.onClicks} addMenu={this.addMenu}/>
						</Col>
					</Row>
				</Grid>
			</ContentWrapper>
		);
	}

}

MenuPage.contextTypes = {
	router: React.PropTypes.object.isRequired
};

export default MenuPage;
