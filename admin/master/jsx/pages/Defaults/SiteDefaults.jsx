import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Button } from 'react-bootstrap';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import Loader from '../../components/loader/Loader';
import Site from './Site';
import { defaultsAction } from '../../actions';
import { ApiDefaults } from '../../api';

class SiteDefaults extends Component {
    constructor(props) {
        super(props);
        this.saveSiteData = this.saveSiteData.bind(this);
        this.cancelSite = this.cancelSite.bind(this);
        this.backToDefaultsPage = this.backToDefaultsPage.bind(this);
    }
    componentWillMount() {
        this.props.getSiteDefults();
    }
    saveSiteData() {
        const { siteDefaults, siteDefaultsOriginal, changeSiteDefults } = this.props;
        siteDefaultsOriginal.forEach((originalItem) => {
            siteDefaults.forEach((currentItem) => {
                if (originalItem.value !== currentItem.value) {
                    const savingData = { name: currentItem.name, value: currentItem.value };
                    changeSiteDefults(currentItem.id, savingData);
                }
            });
        });
        this.backToDefaultsPage();
    }
    cancelSite() {
        const { backToOriginalDefaults } = this.props;
        backToOriginalDefaults();
        this.backToDefaultsPage();
    }
    backToDefaultsPage() {
        this.context.router.push('/defaults');
    }
    render() {
        const { siteDefaults, changeDateFormat, changeTimeFormat, changeCurrency } = this.props;
        return (
            <ContentWrapper>
                <h3>Defaults -- Site</h3>
                <div className="panel-default panel mainBox">
                    {siteDefaults.length > 0 ?
                        <div className="panel-body">
                            <Site
                                changeDateFormat={changeDateFormat}
                                changeTimeFormat={changeTimeFormat}
                                changeCurrency={changeCurrency}
                                data={siteDefaults}
                            />
                            <Button className="btn btn-sm btn-primary" onClick={this.saveSiteData}>Save</Button>
                            <Button className="mh btn btn-sm btn-default" onClick={this.cancelSite}>Cancel</Button>
                        </div> : <Loader />
                    }
                </div>
            </ContentWrapper>
        );
    }
}

SiteDefaults.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

SiteDefaults.propTypes = {
    siteDefaults: PropTypes.arrayOf(PropTypes.object).isRequired,
    siteDefaultsOriginal: PropTypes.arrayOf(PropTypes.object).isRequired,
    changeDateFormat: PropTypes.func.isRequired,
    changeTimeFormat: PropTypes.func.isRequired,
    changeCurrency: PropTypes.func.isRequired,
    getSiteDefults: PropTypes.func.isRequired,
    changeSiteDefults: PropTypes.func.isRequired,
    backToOriginalDefaults: PropTypes.func.isRequired,
};

export default compose(
    connect(state => ({
        siteDefaults: state.defaults.siteDefaults,
        siteDefaultsOriginal: state.defaults.siteDefaultsOriginal,
    }), defaultsAction),
    withProps(ApiDefaults),
)(SiteDefaults);
