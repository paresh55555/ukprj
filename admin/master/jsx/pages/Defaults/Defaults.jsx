import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import LinkBox from '../../components/linkbox/LinkBox';

export default function Defaults() {
    return (
        <ContentWrapper>
            <h3>Defaults</h3>
            <Grid fluid>
                <Row>
                    <Col md={2} sm={4}>
                        <LinkBox url={'defaults/site'} title={'Site'} />
                    </Col>
                </Row>
            </Grid>
        </ContentWrapper>
    );
}
