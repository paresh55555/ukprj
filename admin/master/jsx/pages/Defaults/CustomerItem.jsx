import React, { PropTypes } from 'react';
import Title from '../../components/Common/Title';

export default function CustomerItem(props) {
    const { data, changeTitle, changePlaceholder, changeDisplay, changeRequired, changeErrorMessage } = props;
    const { id, title, placeholder, display, required, error } = data;
    const onTitleChange = titleName => changeTitle(id, titleName);
    const onErrorMessageChange = titleName => changeErrorMessage(id, titleName);
    const onPlaceholderChange = e => changePlaceholder(id, e.target.value);
    const onDisplayChange = () => changeDisplay(id, display ? 0 : 1);
    const onRequiredChange = () => changeRequired(id, required ? 0 : 1);
    return (
        <div className="defaults-item">
            <Title title={{ value: title, id }} editing cssClass="defaults-title" onTitleChange={onTitleChange} />
            <input onChange={onPlaceholderChange} placeholder={placeholder} value={placeholder} disabled={!display} className="form-control form-control" type="text" />
            {required ? <Title title={{ value: error, id }} cssClass="defaults-required" editing onTitleChange={onErrorMessageChange} /> : null}
            <span className="defaults-checkbox"><input onChange={onDisplayChange} checked={display} value="first_checkbox" type="checkbox" />Display</span>
            <span className="defaults-checkbox"><input onChange={onRequiredChange} checked={required} value="first_checkbox" type="checkbox" />Required</span>
        </div>
    );
}

CustomerItem.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string.isRequired,
        placeholder: PropTypes.string.isRequired,
        error: PropTypes.string.isRequired,
        display: PropTypes.number.isRequired,
        required: PropTypes.number.isRequired,
        id: PropTypes.number.isRequired,
    }).isRequired,
    changeTitle: PropTypes.func.isRequired,
    changePlaceholder: PropTypes.func.isRequired,
    changeDisplay: PropTypes.func.isRequired,
    changeRequired: PropTypes.func.isRequired,
    changeErrorMessage: PropTypes.func.isRequired,
};
