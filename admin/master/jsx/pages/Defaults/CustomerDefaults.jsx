import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import { Button } from 'react-bootstrap';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import Loader from '../../components/loader/Loader';
import CustomerItem from './CustomerItem';
import { defaultsAction } from '../../actions';
import { ApiDefaults } from '../../api';

class CustomerDefaults extends Component {
    constructor(props) {
        super(props);
        this.saveCustomerData = this.saveCustomerData.bind(this);
        this.cancelCustomer = this.cancelCustomer.bind(this);
        this.backToDefaultsPage = this.backToDefaultsPage.bind(this);
    }
    componentWillMount() {
        this.props.getCustomerDefults();
    }
    saveCustomerData() {
        const { customerDefaults, customerDefaultsOriginal, changeCustomerDefults } = this.props;
        customerDefaultsOriginal.forEach((originalItem) => {
            customerDefaults.forEach((currentItem) => {
                if (originalItem.id === currentItem.id && (
                    originalItem.placeholder !== currentItem.placeholder ||
                    originalItem.title !== currentItem.title ||
                    originalItem.display !== currentItem.display ||
                    originalItem.required !== currentItem.required ||
                    originalItem.error !== currentItem.error
                )) {
                    const savingData = {
                        title: currentItem.title,
                        placeholder: currentItem.placeholder,
                        display: currentItem.display,
                        required: currentItem.required,
                        error: currentItem.error,
                        type: currentItem.type,
                    };
                    changeCustomerDefults(currentItem.id, savingData);
                }
            });
        });
        this.backToDefaultsPage();
    }
    backToDefaultsPage() {
        this.context.router.push('/defaults');
    }
    cancelCustomer() {
        const { backToOriginalCustomer } = this.props;
        backToOriginalCustomer();
        this.backToDefaultsPage();
    }
    render() {
        const { customerDefaults, changePlaceholder, changeDisplay, changeRequired, changeTitle, changeErrorMessage } = this.props;
        return (
            <ContentWrapper>
                <h3>Customer</h3>
                <div className="panel-default panel mainBox">
                    {customerDefaults.length > 0 ?
                        <div className="panel-body">
                            {customerDefaults.map(item => (
                                <CustomerItem
                                    key={item.id}
                                    changeTitle={changeTitle}
                                    changePlaceholder={changePlaceholder}
                                    changeDisplay={changeDisplay}
                                    changeRequired={changeRequired}
                                    changeErrorMessage={changeErrorMessage}
                                    data={item}
                                />
                            ))}
                            <Button className="btn btn-sm btn-primary" onClick={this.saveCustomerData}>Save</Button>
                            <Button className="mh btn btn-sm btn-default" onClick={this.cancelCustomer}>Cancel</Button>
                        </div> : <Loader />
                    }
                </div>
            </ContentWrapper>
        );
    }
}

CustomerDefaults.contextTypes = {
    router: React.PropTypes.object.isRequired,
};

CustomerDefaults.propTypes = {
    customerDefaults: PropTypes.arrayOf(PropTypes.object).isRequired,
    customerDefaultsOriginal: PropTypes.arrayOf(PropTypes.object).isRequired,
    changeTitle: PropTypes.func.isRequired,
    changePlaceholder: PropTypes.func.isRequired,
    changeDisplay: PropTypes.func.isRequired,
    changeRequired: PropTypes.func.isRequired,
    backToOriginalCustomer: PropTypes.func.isRequired,
    getCustomerDefults: PropTypes.func.isRequired,
    changeCustomerDefults: PropTypes.func.isRequired,
    changeErrorMessage: PropTypes.func.isRequired,
};

export default compose(
    connect(state => ({
        customerDefaults: state.defaults.customerDefaults,
        customerDefaultsOriginal: state.defaults.customerDefaultsOriginal,
    }), defaultsAction),
    withProps(ApiDefaults),
)(CustomerDefaults);
