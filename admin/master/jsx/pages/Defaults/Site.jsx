import React, { PropTypes } from 'react';
import moment from 'moment';

export default function Site(props) {
    const { data, changeDateFormat, changeTimeFormat, changeCurrency } = props;
    const [currency, time, date] = data;
    const getCurrentDayFormat = (dateStyle) => {
        switch (dateStyle) {
        case 'uk': return 'DD/MM/YYYY';
        case 'int': return 'YYYY-MM-DD';
        default: return 'MM/DD/YYYY';
        }
    };
    const currentDayFormat = getCurrentDayFormat(date.value);
    return (
        <div>
            <div className="defaults-item">
                <div>
                    <span className="defaults-title">Date Format:</span> {moment().format(currentDayFormat)}
                </div>
                <div><input onChange={() => changeDateFormat(date.id, 'us')} type="radio" checked={date.value === 'us'} /> MM/DD/YYYY</div>
                <div><input onChange={() => changeDateFormat(date.id, 'uk')} type="radio" checked={date.value === 'uk'} /> DD/MM/YYYY</div>
                <div><input onChange={() => changeDateFormat(date.id, 'int')} type="radio" checked={date.value === 'int'} /> YYYY-MM-DD</div>
            </div>
            <div className="defaults-item">
                <div>
                    <span className="defaults-title">Time Format:</span> {time.value === '12hr' ? moment().format('h:mm a') : moment().format('H:mm')}
                </div>
                <div><input onChange={() => changeTimeFormat(time.id, '12hr')} type="radio" checked={time.value === '12hr'} /> 12hr am/pm</div>
                <div><input onChange={() => changeTimeFormat(time.id, '24hr')} type="radio" checked={time.value === '24hr'} /> 24hr</div>
            </div>
            <div className="defaults-item">
                <div>
                    <span className="defaults-title">Currency:</span> {currency.value === 'dollars' ? '$' : '£'}1,000.00
                </div>
                <div><input onChange={() => changeCurrency(currency.id, 'dollars')} type="radio" name="currency" checked={currency.value === 'dollars'}/> $</div>
                <div><input onChange={() => changeCurrency(currency.id, 'pounds')} type="radio" name="currency" checked={currency.value === 'pounds'}/> £</div>
            </div>
        </div>
    );
}

Site.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    changeDateFormat: PropTypes.func.isRequired,
    changeTimeFormat: PropTypes.func.isRequired,
    changeCurrency: PropTypes.func.isRequired,
};
