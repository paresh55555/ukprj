import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col, Button, Panel } from 'react-bootstrap';
import LinkBox from '../../components/linkbox/LinkBox'
import {systemAction} from '../../actions';
import DataTable from '../../components/datatable/DataTable.jsx'
import PanelForm from '../../components/PanelRanges/PanelForm'
import SQModal from '../../components/Common/SQModal/SQModal'
import {ApiSystems, ApiComponent} from '../../api';
import {NotifyAlert} from '../../components/Common/notify.js'
import Breadcrumbs from '../../components/Breadcrumbs/Breadcrumbs';

const table_fields = {
    panels:'number_of_panels',
    min_width:'min_width', 
    max_width:'max_width', 
    min_height:'min_height', 
    max_height:'max_height', 
}
class SystemPanelRanges extends React.Component {
	constructor() {
      super();
      this.state = {panel:{},showLoading:false}
      this.onChange = this.onChange.bind(this)
      this.addPanel = this.addPanel.bind(this)
      this.onSave = this.onSave.bind(this)
      this.onDelete = this.onDelete.bind(this)
      this.resetState = this.resetState.bind(this)
    }
    componentWillMount(){
      ApiSystems.getPanelRanges(this.props.routeParams.id);
      ApiComponent.ApiGetSystemComponent(this.props.routeParams.id)
    }
    editPanel(panel){
        this.setState({panel:panel})
        this.panelForm.open()
    }
    addPanel(){
        this.resetState();
        this.panelForm.open()
    }
    onChange(key,event){
        let panel = this.state.panel;
        let value = parseInt(event.target.value);
        panel[key] = !!value ? value : 0;
        this.setState({panel:panel});
    }
    resetState(){
        this.setState({panel:{panels:'',min_height:'',max_height:'',min_width:'',max_width:''},showLoading:false})
    }
    onDelete(data){
        this.setState({edited:false})
        let self = this;
         swal({
              title: "Are you sure?",
              text: "Are you sure you want to delete this panel range? ",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete it!",
              closeOnConfirm: false,
            }, function(){ 
                ApiSystems.deletePanelRange(data)
                    .then((res)=>{
                        window.onkeydown = null;
                        window.onfocus = null;
                        swal("Deleted!",
                          "panel range has been deleted.",
                           "success");
                        ApiSystems.getPanelRanges(self.props.routeParams.id);
                    })
            });
    }
    onSave(){
        if($('form[data-parsley-validate]').parsley().validate({group: 'block-' + 0}))
        {
            this.setState({showLoading:true})
            if(this.state.panel.id){
                ApiSystems.updatePanelRange(this.state.panel)
                    .then((res)=>{
                        NotifyAlert('Panel Range updated successfully');
                        ApiSystems.getPanelRanges(this.props.routeParams.id)
                            .then(res=>{
                                this.onSaved()
                            })
                        
                    })
            }
            else{
                ApiSystems.savePanelRange(this.props.routeParams.id, this.state.panel)
                    .then((res)=>{
                        ApiSystems.getPanelRanges(this.props.routeParams.id)
                            .then(res=>{
                                this.onSaved()
                            })
                    })
            }
        }
    }
    onSaved(){
        this.panelForm.close()
        this.resetState()
    }
    render() {
        const {panelRanges, isLoading} = this.props.system
     const systemTitle = this.props.component && this.props.component.system && this.props.component.system.traits && this.props.component.system.traits.length &&
        this.props.component.system.traits.find((trait)=>trait.name ==='title');
        const name =  this.props.component.system.name === 'Door System' ? 'Door System Template': (this.props.component.system.name === 'Window System' ? 'Window System Template' : 'Active System')
        const url =  this.props.component.system.name === 'Door System' ? '/builder/systems/doors': (this.props.component.system.name === 'Window System' ? '/builder/systems/windows' : '/systems')
        const breadCrumbData = [
            {
                title: name, 
                url:url,
                active:false
            },{
                title : systemTitle && systemTitle.value || '',
                url:`/systems/${this.props.params.id}`,
                active:false
            },
            {
                title : 'Panel Ranges',
                url:'',
                active:true
            }

        ]
        const actionTag = <Button className="pull-right" bsStyle={'danger'}>Delete</Button>
        return (
            <ContentWrapper>
            <Breadcrumbs data={breadCrumbData} />
                
                <Grid fluid>
                    <Row>
                        <Col md={12}> 
                        <Button className="mb mv btn-labeled btn btn-info mr pull-right" onClick={this.addPanel}>
                            <span className="btn-label"><i className="fa fa-plus"></i></span>Add Panel Range
                        </Button>
                        <div className="clearfix"></div>
                            <Panel>
                                <DataTable 
                                    id="userTable" 
                                    onClick={this.editPanel.bind(this)} 
                                    data={panelRanges} 
                                    fields={table_fields} 
                                    isLoading={isLoading}
                                    action={this.onDelete}
                                    actionTag = {actionTag}
                                    defaultOrder={0}
                                />
                                <SQModal 
                                    onChange={this.onChange} 
                                    ref={(c)=>this.panelForm = c}
                                    title={!!this.state.panel.id ? 'Edit Panel' : 'Add Panel'}
                                    onConfirm={this.onSave}
                                    disabled={this.state.showLoading}
                                    confirmText={this.state.showLoading ? "Saving.." : "Save"}>
                                    <PanelForm 
                                        onChange={this.onChange} 
                                        panel={this.state.panel}
                                    />
                                </SQModal>
                            </Panel>
                        </Col>
                    </Row>
                </Grid>
            </ContentWrapper>
        );
    }

}
SystemPanelRanges.contextTypes = {
    router: React.PropTypes.object.isRequired
};
export default connect(state => ({
  system: state.system,
  component: state.component

}), Object.assign({}, systemAction))(SystemPanelRanges);
