import React, { PropTypes } from 'react';
import SQTable from '../../components/Common/SQTable';

export default function TableRequests(props) {
    const fields = {
        job_number: 'Job Number',
        total: 'Total',
        number_of_panels: 'Number Of Panels',
        quoted: 'Quote Date',
    };
    const panelsSum = props.data.reduce((sum, current) => sum + current.number_of_panels, 0);
    const totalSum = Math.round(props.data.reduce((sum, current) => sum + current.total, 0) * 100) / 100;
    let data = props.data.length > 0 ? [...props.data, { job_number: 'Total', total: totalSum, number_of_panels: panelsSum, quoted: '' }] : props.data;
    const numberWithCommas = number => number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    data = data.map(item => Object.assign({}, item, { total: props.currency.value === 'pounds' ? `£ ${numberWithCommas(item.total)}` : `$ ${numberWithCommas(item.total)}` }));
    return (
        <div className="panel panel-default">
            <div className="panel-body">
                <div>
                    <SQTable
                        pagination={false}
                        noClick
                        data={data}
                        columns={fields}
                        isLoading={props.isLoading}
                        id="salesReportsTable"
                    />
                </div>
            </div>
        </div>
    );
}

TableRequests.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    isLoading: PropTypes.bool.isRequired,
    currency: PropTypes.shape({
        value: PropTypes.string,
    }).isRequired,
};
