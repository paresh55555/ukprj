import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import TableDeliveredOrders from './TableDeliveredOrders';
import { ApiReports, ApiDefaults } from '../../api';


class DeliveredOrders extends Component {
    componentDidMount() {
        const { getDeliveredOrdersReports, getSiteDefults, deliveredOrdersReports } = this.props;
        if (deliveredOrdersReports.length === 0) {
            getSiteDefults();
        }
        getDeliveredOrdersReports();
    }
    render() {
        const { deliveredOrdersReports, isLoading, currency } = this.props;
        return (
            <ContentWrapper>
                <h3>Delivered Orders</h3>
                {deliveredOrdersReports.length > 0 ?
                    <TableDeliveredOrders
                        currency={currency}
                        isLoading={isLoading}
                        data={deliveredOrdersReports}
                    /> : null
                }
            </ContentWrapper>
        );
    }
}

DeliveredOrders.propTypes = {
    getDeliveredOrdersReports: PropTypes.func.isRequired,
    getSiteDefults: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    deliveredOrdersReports: PropTypes.arrayOf(PropTypes.object).isRequired,
    currency: PropTypes.shape({
        name: PropTypes.string,
    }).isRequired,
};

const apiActions = Object.assign({}, ApiReports, ApiDefaults);

const mergeProps = stateProps => Object.assign({}, stateProps, {
    currency: stateProps.siteDefaults && stateProps.siteDefaults.length > 0 ? stateProps.siteDefaults.find(item => item.name === 'currency') : {},
});

export default compose(
    connect(state => ({
        deliveredOrdersReports: state.reports.deliveredOrdersReports,
        isLoading: state.reports.isLoading,
    }), {}, mergeProps),
    withProps(apiActions),
)(DeliveredOrders);
