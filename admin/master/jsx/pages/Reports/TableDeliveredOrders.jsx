import React, { PropTypes } from 'react';
import SQTable from '../../components/Common/SQTable';

export default function TableDeliveredOrders(props) {
    const { data, currency } = props;
    const fields = {
        Order_number: 'Order#',
        last_name: 'Last Name',
        first_name: 'First Name',
        company_name: 'Company',
        total: 'Job Total',
        balanceDue: 'Balance Due',
    };
    const balanceSum = Math.round(data.reduce((sum, current) => sum + current.balanceDue, 0) * 100) / 100;
    const totalSum = Math.round(data.reduce((sum, current) => sum + current.total, 0) * 100) / 100;
    let newData = data.length > 0 ? [...data, { Order_number: 'Total', total: totalSum, balanceDue: balanceSum, last_name: '', first_name: '', company_name: '' }] : data;
    const numberWithCommas = number => number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    newData = newData.map(item => {
        const newTotal = numberWithCommas(item.total.toFixed(2));
        const newBalance = numberWithCommas(item.balanceDue.toFixed(2));
        return Object.assign({}, item, {
            total: currency.value === 'pounds' ? `£ ${newTotal}` : `$ ${newTotal}`,
            balanceDue: !item.balanceDue ? '' : currency.value === 'pounds' ? `£ ${newBalance}` : `$ ${newBalance}`,
        });
    });

    return (
        <div className="panel panel-default">
            <div className="panel-body">
                <div>
                    <SQTable
                        pagination={false}
                        noClick
                        data={newData}
                        columns={fields}
                        isLoading={false}
                        id="deliveredOrderedTable"
                    />
                </div>
            </div>
        </div>
    );
}

TableDeliveredOrders.propTypes = {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    isLoading: PropTypes.bool.isRequired,
    currency: PropTypes.shape({
        value: PropTypes.string,
    }).isRequired,
};
