import React, { Component, PropTypes } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

class RequestReports extends Component {
    constructor() {
        super();
        this.state = {
            showNotification: false,
            startDate: moment().subtract(1, 'days'),
            endDate: moment(),
            quotes: true,
            orders: false,
        };
        this.handleChangeStart = this.handleChangeStart.bind(this);
        this.handleChangeEnd = this.handleChangeEnd.bind(this);
        this.handleOptions = this.handleOptions.bind(this);
        this.handleQuotes = this.handleQuotes.bind(this);
        this.handleOrders = this.handleOrders.bind(this);
        this.findReports = this.findReports.bind(this);
    }

    handleChangeStart(date) {
        if (moment().diff(date, 'days') <= 0) {
            this.setState({ startDate: moment().subtract(1, 'days') });
        } else {
            this.setState({ startDate: date });
        }
    }

    handleChangeEnd(date) {
        if (moment().diff(date, 'days') < 0) {
            this.setState({ endDate: moment() });
        } else {
            this.setState({ endDate: date });
        }
    }

    handleOptions(field) {
        this.setState({ [field]: !this.state[field] });
    }

    handleQuotes() {
        if (!this.state.orders) {
            this.setState({ quotes: true, showNotification: true });
        } else {
            this.setState({ quotes: !this.state.quotes, showNotification: false  });
        }
    }

    handleOrders() {
        if (!this.state.quotes) {
            this.setState({ orders: true, showNotification: true });
        } else {
            this.setState({ orders: !this.state.orders, showNotification: false });
        }
    }

    findReports() {
        const startDate = this.state.startDate.format('YYYY-MM-DD');
        const endDate = this.state.endDate.format('YYYY-MM-DD');
        const quotes = this.state.quotes ? 1 : 0;
        const orders = this.state.orders ? 1 : 0;
        const requestString = `startDate=${startDate}&endDate=${endDate}&quote=${quotes}&order=${orders}`;
        this.setState({ showNotification: false });
        this.props.fetchData(requestString);
    }

    render() {
        const { startDate, endDate, quotes, orders, showNotification } = this.state;
        return (
            <div className="container-fluid report-request indent-after">
                <div>Start Date:</div>
                <div className="report-datepicker">
                    <DatePicker
                        onChangeRaw={(e)=>e.preventDefault()}
                        dropdownMode="select"
                        selected={startDate}
                        selectsStart
                        startDate={startDate}
                        endDate={endDate}
                        onChange={this.handleChangeStart}
                    />
                </div>
                <div>End Date:</div>
                <div className="report-datepicker">
                    <DatePicker
                        onChangeRaw={(e)=>e.preventDefault()}
                        dropdownMode="select"
                        selected={endDate}
                        selectsEnd
                        startDate={startDate}
                        endDate={endDate}
                        onChange={this.handleChangeEnd}
                    />
                </div>
                <div className="request-checkbox-container">
                    <div><input type="checkbox" onChange={this.handleQuotes} checked={quotes} />Quotes</div>
                    <div><input type="checkbox" onChange={this.handleOrders} checked={orders} />Orders</div>
                    <div
                        role="button"
                        tabIndex={0}
                        onClick={this.findReports}
                        className="groupTitle mb-sm mr-sm btn btn-success btn-outline colorGroupButton report-confirm"
                    >
                        <span className="undefined item-tmp-2391 editable editable-click editable-disabled">GO</span>
                    </div>
                    {showNotification ? <span className="request-notification">At least one field must be selected</span> : null}
                </div>
            </div>
        );
    }
}

RequestReports.propTypes = {
    fetchData: PropTypes.func.isRequired,
};

export default RequestReports;
