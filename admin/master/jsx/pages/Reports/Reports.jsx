import React from 'react';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import { Grid, Row, Col } from 'react-bootstrap';
import LinkBox from '../../components/linkbox/LinkBox'

export default function Reports (props) {
    return (
        <ContentWrapper>
            <h3>Reports</h3>
            <Grid fluid>
                <Row>
                    <Col md={2} sm={4}>
                        <LinkBox url={'reports/salesReports'} title={'Sales Reports'} />
                    </Col>
                </Row>
            </Grid>
        </ContentWrapper>
    )
}
