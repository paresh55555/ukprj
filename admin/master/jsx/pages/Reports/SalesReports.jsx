import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';
import ContentWrapper from '../../components/Layout/ContentWrapper';
import RequestReports from './RequestReports';
import TableRequests from './TableRequests';
import { ApiReports, ApiDefaults } from '../../api';


class SalesReports extends Component {
    componentWillMount() {
        if (this.props.siteDefaults.length === 0) {
            this.props.getSiteDefults();
        }
    }
    render() {
        const { getSalesReports, reports, currency } = this.props;
        return (
            <ContentWrapper>
                <h3>SalesReports</h3>
                <RequestReports fetchData={getSalesReports} />
                <TableRequests
                    currency={currency}
                    isLoading={reports.isLoading}
                    data={reports.salesReports}
                />
            </ContentWrapper>
        );
    }
}

SalesReports.propTypes = {
    siteDefaults: PropTypes.array.isRequired,
    getSalesReports: PropTypes.func.isRequired,
    getSiteDefults: PropTypes.func.isRequired,
    reports: PropTypes.shape(({
        isLoading: PropTypes.bool,
        salesReports: PropTypes.array,
    })).isRequired,
    currency: PropTypes.shape(({
        name: PropTypes.string,
    })).isRequired,
};

const apiActions = Object.assign({}, ApiReports, ApiDefaults);

const mergeProps = stateProps => Object.assign({}, stateProps, {
    currency: stateProps.siteDefaults.length > 0 ? stateProps.siteDefaults.find(item => item.name === 'currency') : {},
});

export default compose(
    connect(state => ({
        reports: state.reports,
        siteDefaults: state.defaults.siteDefaults,
        isLoadingSiteDefaults: state.defaults.isLoading,
    }), {}, mergeProps),
    withProps(apiActions),
)(SalesReports);
