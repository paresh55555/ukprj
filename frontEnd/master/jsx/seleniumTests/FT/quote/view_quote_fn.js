const cf = require('../commonFunctions/cf');

const quoteClick = (d) => {
    return d.executeScript("document.getElementById('menuOptions').getElementsByClassName('menuText')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const searchInput = (d, wd, value) => {
    return d.findElement(wd.By.id('search')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two).then(() => d.findElement(wd.By.id('search')).clear()))
}

const itemsPerPageSelect = (d) => {
    return d.executeScript('document.getElementById("myLimit").selectedIndex = 2').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const sortItems = (d, wd, value) => {
    return d.findElement(wd.By.id(value)).click().then(() => cf.delayBySecond(cf.delayBySecond.ten))
}

const newButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("new")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const hotButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("hot")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const mediumButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("medium")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const coldButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("cold")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const holdButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("hold")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const archivedButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("archived")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const viewAllButtonClick = (d, wd) => {
    return d.findElement(wd.By.id("viewAll")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const qualitySelect = (d) => {
    return d.executeScript('document.getElementsByClassName("leadStatusPullDown")[0].selectedIndex = 2').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const quoteIdClick = (d) => {
    return d.executeScript('document.getElementsByClassName("quoteRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const pickupDeliverySelection = (d, wd, index) => {
    return d.executeScript("window.scrollTo(0,600)").then(() => d.executeScript("document.getElementById('pickupDelivery').selectedIndex = " + index).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const productionDetailNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.id("productionDetailNotes88")).clear().then(() =>d.findElement(wd.By.id("productionDetailNotes88")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingContactInput = (d, wd, value) => {
    return d.executeScript('window.scrollTo(0,1200)').then(() => d.findElement(wd.By.id("billing_contact")).clear()).then(() =>d.findElement(wd.By.id("billing_contact")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingAddress1Input = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_address1")).clear().then(() =>d.findElement(wd.By.id("billing_address1")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingAddress2Input = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_address2")).clear().then(() =>d.findElement(wd.By.id("billing_address2")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingCityInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_city")).clear().then(() =>d.findElement(wd.By.id("billing_city")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingStateInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_state")).clear().then(() =>d.findElement(wd.By.id("billing_state")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingZipInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_zip")).clear().then(() =>d.findElement(wd.By.id("billing_zip")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingPhoneInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_phone")).clear().then(() =>d.findElement(wd.By.id("billing_phone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const billingEmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("billing_email")).clear().then(() =>d.findElement(wd.By.id("billing_email")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryContactInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_contact")).clear().then(() =>d.findElement(wd.By.id("shipping_contact")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryAddress1Input = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_address1")).clear().then(() =>d.findElement(wd.By.id("shipping_address1")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryAddress2Input = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_address2")).clear().then(() =>d.findElement(wd.By.id("shipping_address2")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryCityInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_city")).clear().then(() =>d.findElement(wd.By.id("shipping_city")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryStateInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_state")).clear().then(() =>d.findElement(wd.By.id("shipping_state")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryZipInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_zip")).clear().then(() =>d.findElement(wd.By.id("shipping_zip")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryPhoneInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_phone")).clear().then(() =>d.findElement(wd.By.id("shipping_phone")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const deliveryEmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("shipping_email")).clear().then(() =>d.findElement(wd.By.id("shipping_email")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const paymentInfoClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,0)').then(() => d.findElement(wd.By.id('enterPaymentInfo')).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
};

const paymentOptionClick = (d) => {
    return d.executeScript('document.getElementsByClassName("paymentOptionsBox")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const last4DigitsOfCreditCardInput = (d, wd, value) => {
    return d.findElement(wd.By.id('lastFour')).sendKeys(value);
};

const paymentAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id('paymentAmount')).sendKeys(value);
};

const paymentDateInput = (d, wd) => {
    let selectedDate = new Date();
    selectedDate.setMonth(selectedDate.getMonth() + 1);
    return d.findElement(wd.By.id('paymentDate')).sendKeys(moment(selectedDate).format('DD/MM/YYY'));
};

const addPaymentClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('addPaymentButton myGreen')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deletePaymentClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('paymentsLeft deletePayment')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentOrderNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.id('paymentOrderNotes')).clear().then(() => d.findElement(wd.By.id('paymentOrderNotes')).sendKeys(value));
};

const saveToQuoteClick = (d, wd, value) => {
    return d.executeScript("window.scrollTo(0,0); document.getElementsByClassName('quoteActionButtons')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    quoteClick,
    newButtonClick,
    searchInput,
    sortItems,
    itemsPerPageSelect,
    hotButtonClick,
    mediumButtonClick,
    coldButtonClick,
    holdButtonClick,
    archivedButtonClick,
    viewAllButtonClick,
    qualitySelect,
    quoteIdClick,
    pickupDeliverySelection,
    productionDetailNotesInput,
    billingContactInput,
    billingAddress1Input,
    billingAddress2Input,
    billingCityInput,
    billingStateInput,
    billingZipInput,
    billingPhoneInput,
    billingEmailInput,
    deliveryContactInput,
    deliveryAddress1Input,
    deliveryAddress2Input,
    deliveryCityInput,
    deliveryStateInput,
    deliveryZipInput,
    deliveryPhoneInput,
    deliveryEmailInput,
    paymentInfoClick,
    last4DigitsOfCreditCardInput,
    paymentAmountInput,
    paymentDateInput,
    paymentOrderNotesInput,
    paymentOptionClick,
    addPaymentClick,
    deletePaymentClick,
    saveToQuoteClick
};