const cf = require('../commonFunctions/cf');

const doorTypeClick = (d, wd, door) => {
    return d.executeScript("document.getElementById('doors').getElementsByClassName('moduleTwoAcross')[" + door + "].getElementsByTagName('img')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.ten));
};

const widthInput = (d, wd, value) => {
    return d.findElement(wd.By.id("width")).clear().then(() => d.findElement(wd.By.id("width")).sendKeys(value)).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const heightInput = (d, wd, value) => {
    return d.findElement(wd.By.id("height")).clear().then(() => d.findElement(wd.By.id("height")).sendKeys(value)).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const numberOfPanelsClick = (d, wd, panel) => {
    return d.findElement(wd.By.id(panel)).click();
};

const scrollOnDiv = (d, wd, div) => {
    return d.executeScript("var element = document.getElementById('" + div + "');element.scrollIntoView();");
};

const swingDirectionSelection = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click();
};

const nextButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('nextButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const exteriorColourClick = (d, wd) => {
    return d.findElement(wd.By.id("extColor-basic-all-White-null-#fcffff-")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const extrasClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const extrasByClassClick = (d, wd, className) => {
    return d.executeScript("document.getElementsByClassName('extendedButtonTrickle')[0]").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const selectHardwareClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const addToQuoteButtonClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('nextTitle')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const saveAsQuoteClick = (d, wd) => {
    return d.findElement(wd.By.id('saveAsQuote')).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const emailQuoteClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteEmailButton')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const emailAddressInput = (d, wd, value) => {
    return d.findElement(wd.By.id('emailAddresses')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const emailMsgInput = (d, wd, value) => {
    return d.findElement(wd.By.id('emailMessage')).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const sendEmailClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('emailButtons')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const closeQuoteClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteActionButtons')[4].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deleteClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteRow')[0].getElementsByTagName('img')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const acceptDeleteClick = (d, wd) => {
    return d.switchTo().alert().then(function(alert) {
        return alert.accept();
    });
};

module.exports = {
    doorTypeClick,
    widthInput,
    heightInput,
    numberOfPanelsClick,
    scrollOnDiv,
    swingDirectionSelection,
    nextButtonClick,
    exteriorColourClick,
    extrasClick,
    extrasByClassClick,
    selectHardwareClick,
    addToQuoteButtonClick,
    saveAsQuoteClick,
    emailQuoteClick,
    emailAddressInput,
    emailMsgInput,
    sendEmailClick,
    closeQuoteClick,
    deleteClick,
    acceptDeleteClick
};