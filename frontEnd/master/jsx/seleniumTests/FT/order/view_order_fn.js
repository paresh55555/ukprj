const cf = require('../commonFunctions/cf');
const moment = require('moment');

const orderClick = (d) => {
    return d.executeScript("document.getElementById('menuOptions').getElementsByClassName('menuText')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};
const viewOrderClick = (d, wd) => {
    return d.findElement(wd.By.id("viewOrders")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const searchOrderInput = (d, wd, value) => {
    return d.findElement(wd.By.id("search")).sendKeys(value).then(() => cf.delayBySecond(cf.delaySecond.two)).then(() => d.findElement(wd.By.id("search")).clear());
};

const sortOrderByIdClick = (d, wd) => {
    return d.findElement(wd.By.id("id")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const orderIdClick = (d, wd) => {
    return d.executeScript('document.getElementsByClassName("orderRow")[0].firstElementChild.click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const scrollOnDiv = (d, wd, div) => {
    return d.executeScript("var element = document.getElementsByClassName('cartRow')[0];element.scrollIntoView();");
};

const itemPerPageSelect = (d) => {
    return d.executeScript('document.getElementById("myLimit").selectedIndex = 2').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const onHoldSelection = (d, wd, index) => {
    return d.executeScript("document.getElementById('onHold').selectedIndex = " + index).then(() => cf.delayBySecond(cf.delaySecond.two));
};

const viewOrdersPaymentClick = (d, wd) => {
    return d.findElement(wd.By.id("enterPaymentInfo")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentOptionClick = (d) => {
    return d.executeScript('document.getElementsByClassName("paymentOptionsBox")[0].click()').then(() => cf.delayBySecond(cf.delaySecond.two));
};

const last4DigitsOfCreditCardInput = (d, wd, value) => {
    return d.findElement(wd.By.id('lastFour')).sendKeys(value);
};

const paymentAmountInput = (d, wd, value) => {
    return d.findElement(wd.By.id('paymentAmount')).sendKeys(value);
};

const addPaymentClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('addPaymentButton myGreen')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const deletePaymentClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('paymentsLeft deletePayment')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentOrderNotesInput = (d, wd, value) => {
    return d.findElement(wd.By.id('paymentOrderNotes')).clear().then(() => d.findElement(wd.By.id('paymentOrderNotes')).sendKeys(value));
};

const closePaymentClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('editButtons')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const paymentDateInput = (d, wd) => {
    let selectedDate = new Date();
    selectedDate.setMonth(selectedDate.getMonth() + 1);
    return d.findElement(wd.By.id('paymentDate')).sendKeys(moment(selectedDate).format('DD/MM/YYY'));
};

const closeOrderClick = (d, wd) => {
    return d.executeScript("document.getElementsByClassName('quoteActionButtons')[2].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

const editQuoteClick = (d, wd) => {
    return d.executeScript("document.getElementById('nextButtonHardware')").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    orderClick,
    viewOrderClick,
    searchOrderInput,
    sortOrderByIdClick,
    orderIdClick,
    itemPerPageSelect,
    viewOrdersPaymentClick,
    last4DigitsOfCreditCardInput,
    paymentAmountInput,
    deletePaymentClick,
    paymentOrderNotesInput,
    addPaymentClick,
    paymentDateInput,
    paymentOptionClick,
    closePaymentClick,
    onHoldSelection,
    closeOrderClick,
    scrollOnDiv,
    editQuoteClick
};