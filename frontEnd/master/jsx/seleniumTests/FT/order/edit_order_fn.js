const cf = require('../commonFunctions/cf');

const clickEdit = (d, wd, index) => {
    return d.executeScript("document.getElementsByClassName('cartIcons')[" + index + "].click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};

module.exports = {
    clickEdit
};