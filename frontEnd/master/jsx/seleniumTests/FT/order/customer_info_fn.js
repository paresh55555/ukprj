const cf = require('../commonFunctions/cf');

const attachCustomerClick = (d, wd) => {
    return d.findElement(wd.By.className("changeCustomer")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const revenueTypeSelect = (d) => {
    return d.executeScript("document.getElementById('revenueType').selectedIndex = 1").then(() => cf.delayBySecond(cf.delaySecond.two));
}

const channelTypeSelect = (d) => {
    return d.executeScript("document.getElementById('channel').selectedIndex = 1").then(() => cf.delayBySecond(cf.delaySecond.two));
}


const searchPreviousCustomerClick = (d, wd) => {
    return d.findElement(wd.By.id("previousCustomer")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const searchPreviousCustomerInput = (d, wd, value) => {
    return d.findElement(wd.By.name("search")).sendKeys(value);
}

const leadSourceSelect = (d) => {
    return d.executeScript('document.getElementById("leadSource").selectedIndex = 2').then(() => cf.delayBySecond(cf.delaySecond.two));
}

const firstNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("firstName")).sendKeys(value);
}

const lastNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("lastName")).sendKeys(value);
}

const emailInput = (d, wd, value) => {
    return d.findElement(wd.By.name("email")).sendKeys(value);
}

const attachToQuoteClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.findElement(wd.By.className("nextTitle")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const editCustomerClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,0)').then(() => d.findElement(wd.By.className("editCustomer")).click().then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const editFirstNameInput = (d, wd, value) => {
    return d.findElement(wd.By.name("firstName")).clear().then( d.findElement(wd.By.name("firstName")).sendKeys(value));
}

const saveChangesClick = (d, wd) => {
    return d.executeScript('window.scrollTo(0,window.outerHeight)').then(() => d.executeScript("document.getElementsByClassName('editButtons')[1].click()").then(() => cf.delayBySecond(cf.delaySecond.two)));
}

const quoteViewButtonClick = (d, wd) => {
    return d.findElement(wd.By.className("quoteViewButton")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
}

const dueDatevViewOrder = (d, wd,value) => {
    return d.executeScript("document.getElementsByClassName('viewOrderDateBox')[0].value='"+value+"'").then(() => cf.delayBySecond(cf.delaySecond.two));
}

module.exports = {
    attachCustomerClick,
    firstNameInput,
    lastNameInput,
    emailInput,
    searchPreviousCustomerClick,
    searchPreviousCustomerInput,
    leadSourceSelect,
    attachToQuoteClick,
    editCustomerClick,
    editFirstNameInput,
    saveChangesClick,
    revenueTypeSelect,
    channelTypeSelect,
    quoteViewButtonClick,
    dueDatevViewOrder
};