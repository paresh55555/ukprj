var request = require("request-promise");
const config = require('../config/config');
var delaySecond = Object.freeze(
    {
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6,
        "seven": 7,
        "eight": 8,
        "nine": 9,
        "ten": 10,
        "twenty": 20,
    }
);
const API_base_URI = "https://" + config.user + ":" + config.secret + "@www.browserstack.com/automate/"

function getSessionId(d) {
    return d.getSession().then(function (session) {
        return session.id_;
    });
}

function callAPI(uri, method, form) {
    return request({
        uri,
        method,
        form
    });
}

function markFailed(d, reason) {
    return getSessionId(d)
        .then((sessionId) => {
            let form = {
                "status": "failed",
                reason
            }
            return callAPI(API_base_URI + "sessions/" + sessionId + ".json", 'PUT', form)
        })
}

function changeSessionTestName(d, name) {
    return getSessionId(d)
        .then((sessionId) => {
            let form = {
                name
            }
            return callAPI(API_base_URI + "sessions/" + sessionId + ".json", 'PUT', form)
        })
}

function test(driver, title, url, urlIsContain, content) {
    let promises = [];
    title && promises.push(testTitle(driver, title));
    url && promises.push(testURL(driver, url, urlIsContain));
    content && promises.push(testContent(driver, url));
    return Promise.all(promises).then(function (values) {
        let isTestPassed = true;
        let reasonsArray = [];
        values.forEach(function (e, i) {
            if (!e.isTestPassed) {
                isTestPassed = false;
                reasonsArray.push(e.reason);
            }
        });
        let reasons = reasonsArray.join();
        if (isTestPassed)
            return true;
        else
            throw reasons;
    });
}

function testTitle(driver, title) {
    return driver.getTitle().then(function (t) {
        if (title !== t) {
            let reason = 'expected title "' + title + '" is not getting matched with retrieved title "' + t + '"'
            return {isTestPassed: false, reason};
        }
        return {isTestPassed: true};
    })
}

function testURL(driver, url, isContain) {
    return driver.getCurrentUrl().then(function (u) {
        if ((!isContain && url !== u) || (!!isContain && u.indexOf(url) < 0)) {
            let reason = ''
            if (!isContain)
                reason = 'expected url "' + url + '" is not getting matched with retrieved url "' + u + '"'
            else
                reason = 'expected url "' + url + '" is not part of retrieved url "' + u + '"'
            return {isTestPassed: false, reason};
        }
        return {isTestPassed: true};
    })
}

function testContent(driver, content) {
    return driver.getPageSource().then(function (p) {
        if (p.indexOf(content) < 0) {
            let reason = '"' + content + '" is not found in page source'
            return {isTestPassed: false, reason};
        }
        return {isTestPassed: true};
    })
}

function testContentByArray(driver, contentArray) {
    let contentNotMatchArray = []
    return driver.getPageSource().then(function (p) {
        contentArray.forEach((content) => {
            if (p.indexOf(content) < 0) {
                contentNotMatchArray.push('"' + content + '" is not found in page source')
            }
        })
        if (contentNotMatchArray.length > 0)
            return Promise.reject(contentNotMatchArray.join());
        return Promise.resolve();
    })
}

function delayBySecond(t, v) {
    return new Promise(function (resolve) {
        setTimeout(resolve.bind(null, v), t * 1000)
    });
}

function handleOutput(d, pc, capability, testsuite) {
    return pc.then(() => {
            process.on('unhandledRejection', (reason, promise) => {});
            config.isSuccess.push('pass');
            console.log('\x1b[32m%s\x1b[0m', 'all ' + testsuite + ' Integration Test passed successfully for ' + capability);
            return d.quit()
        }
    ).catch((reason) => {
        return markFailed(d, reason).then(() => {
            config.isSuccess.push('fail');
            console.log('\x1b[31m%s\x1b[0m', testsuite + ' Integration Test Failed (for ' + capability + ') \n' + reason);
            return d.quit();
        });
    })
}

module.exports = {changeSessionTestName, test, testContentByArray, delayBySecond, delaySecond, handleOutput};