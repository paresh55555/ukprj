const cf = require('./commonFunctions/cf');

const signIn = (d, wd) => {
    return d.findElement(wd.By.id("signInButton")).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const EmailInput = (d, wd, value) => {
    return d.findElement(wd.By.id("email")).sendKeys(value);
};

const PasswordInput = (d, wd, value) => {
    return d.findElement(wd.By.id("password")).sendKeys(value);
};

const menuClick = (d, wd) => {
    return d.findElement(wd.By.id("headerRightMenu")).click().then(() => cf.delayBySecond(cf.delaySecond.two));
};

const signOutClick = (d) => {
    return d.executeScript("document.getElementById('menuOptions').lastElementChild.click()").then(() => cf.delayBySecond(cf.delaySecond.two));
};


module.exports = {signIn, EmailInput, PasswordInput, menuClick, signOutClick};