const webdriver = require('selenium-webdriver');
const user = 'frackafuture2';
const secret = 'hBpqeZsuy5HW2Yosw4pU';
const BASE_URL = 'https://qa-uk.rioft.com/';
const SERVER_URL = 'http://hub.browserstack.com/wd/hub';
const isSuccess = [];

const Capabilities = {
    "mac_mojave_chrome_1920_1080": {
        'browserName': 'Chrome',
        'browser_version': '70.0',
        'os': 'OS X',
        'os_version': 'Mojave',
        'resolution': '1920x1080',
        'chromeOptions': { 'args': ['start-fullscreen'] },
        'browserstack.user': user,
        'browserstack.key': secret
    },
    "mac_mojave_safari_1920_1080": {
        'browserName': 'Safari',
        'browser_version': '12.0',
        'os': 'OS X',
        'os_version': 'Mojave',
        'resolution': '1920x1080',
        'browserstack.user': user,
        'browserstack.key': secret
    },
    "mac_mojave_firefox_1920_1080": {
        'browserName': 'Firefox',
        'browser_version': '64.0',
        'os': 'OS X',
        'os_version': 'Mojave',
        'resolution': '1920x1080',
        'browserstack.user': user,
        'browserstack.key': secret
    },
    "window10_chrome_1920_1080": {
        'browserName': 'Chrome',
        'browser_version': '71.0 beta',
        'os': 'Windows',
        'os_version': '10',
        'resolution': '1920x1080',
        'chromeOptions': { 'args': ['start-fullscreen'] },
        'browserstack.user': user,
        'browserstack.key': secret
    },
    "window10_firefox_1920_1080": {
        'browserName': 'Firefox',
        'browser_version': '64.0',
        'os': 'Windows',
        'os_version': '10',
        'resolution': '1920x1080',
        'chromeOptions': { 'args': ['start-fullscreen'] },
        'browserstack.user': user,
        'browserstack.key': secret
    }
};

function getDriver(Capability) {   
    return new webdriver.Builder().usingServer(SERVER_URL).withCapabilities(Capability).build();
};

module.exports = { user, secret, Capabilities, BASE_URL, SERVER_URL, getDriver, isSuccess };