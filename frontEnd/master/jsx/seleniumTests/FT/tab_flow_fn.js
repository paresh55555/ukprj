const cf = require('./commonFunctions/cf');

const tabClick = (d, wd, id) => {
    return d.findElement(wd.By.id(id)).click().then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const tabByClassClick = (d, wd, className) => {
    return d.executeScript("document.getElementsByClassName('" + className + "')[0].click()").then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

const viewAllTabClick = (d) => {
    return d.executeScript("document.getElementById('preMainContent').getElementsByClassName('orderStatus')[0]").then(() => cf.delayBySecond(cf.delaySecond.twenty));
};

module.exports = {
    tabClick,
    tabByClassClick,
    viewAllTabClick
};