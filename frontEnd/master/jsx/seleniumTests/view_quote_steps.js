const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/login_fn');
const viewQuote = require('./FT/quote/view_quote_fn');

let _u = undefined;

function view_quote_step(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: View Quote'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));

    // ADMIN LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => viewQuote.quoteClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    // SORT & SEARCH
    pc = pc.then(() => viewQuote.searchInput(d, wd, 'a')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => viewQuote.sortItems(d, wd, 'leadQuality')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&order=leadQuality&', true, _u));
    pc = pc.then(() => viewQuote.sortItems(d, wd, 'total')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&order=total&', true, _u));

    // TAB
    pc = pc.then(() => viewQuote.itemsPerPageSelect(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&order=total', true, _u));
    pc = pc.then(() => viewQuote.newButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesNew', Config.BASE_URL + '?tab=viewQuotesNew&', true, _u));
    pc = pc.then(() => viewQuote.hotButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesHot', Config.BASE_URL + '?tab=viewQuotesHot&', true, _u));
    pc = pc.then(() => viewQuote.mediumButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => viewQuote.coldButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesCold', Config.BASE_URL + '?tab=viewQuotesCold&', true, _u));
    pc = pc.then(() => viewQuote.holdButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesHold', Config.BASE_URL + '?tab=viewQuotesHold&', true, _u));
    pc = pc.then(() => viewQuote.archivedButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotesArchived', Config.BASE_URL + '?tab=viewQuotesArchived&', true, _u));
    pc = pc.then(() => viewQuote.viewAllButtonClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    // SELECT QUOTE
    pc = pc.then(() => viewQuote.qualitySelect(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    pc = pc.then(() => viewQuote.quoteIdClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&quote', true, _u));

    // PROJECT INFO
    pc = pc.then(() => viewQuote.pickupDeliverySelection(d, wd, 1)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.pickupDeliverySelection(d, wd, 0)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.productionDetailNotesInput(d, wd, "Some note")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

    // BILLING INFO
    pc = pc.then(() => viewQuote.billingContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingZipInput(d, wd, "94402")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.billingEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

   // DELIVERY INFO
    pc = pc.then(() => viewQuote.deliveryContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryZipInput(d, wd, "94402")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    pc = pc.then(() => viewQuote.deliveryEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));

    // SAVE QUOTE
    pc = pc.then(() => viewQuote.saveToQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote', true, _u));
    
    pc = cf.handleOutput(d, pc, capability, 'View Quote by salesPerson');
    return pc;
}

module.exports = {view_quote_step};