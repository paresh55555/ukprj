const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/login_fn');
const viewQuote = require('./FT/quote/view_quote_fn');
const viewOrder = require('./FT/order/view_order_fn');
const tabFlow = require('./FT/tab_flow_fn');

let _u = undefined;

function tab_flow_step(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: Tab Flow'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));

    // ADMIN LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => viewQuote.quoteClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));

    // QUOTE TAB
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'viewAll')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'new')).then(() => cf.test(d, 'viewQuotesNew', Config.BASE_URL + '?tab=viewQuotesNew', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'hot')).then(() => cf.test(d, 'viewQuotesHot', Config.BASE_URL + '?tab=viewQuotesHot', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'medium')).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'cold')).then(() => cf.test(d, 'viewQuotesCold', Config.BASE_URL + '?tab=viewQuotesCold', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'hold')).then(() => cf.test(d, 'viewQuotesHold', Config.BASE_URL + '?tab=viewQuotesHold', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'archived')).then(() => cf.test(d, 'viewQuotesArchived', Config.BASE_URL + '?tab=viewQuotesArchived', true, _u));

    // ORDER TAB
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'viewQuotesArchived', Config.BASE_URL + '?tab=viewQuotesArchived', true, _u));
    pc = pc.then(() => viewOrder.orderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));

    pc = pc.then(() => tabFlow.viewAllTabClick(d)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'needsPayment')).then(() => cf.test(d, 'viewOrdersNeedsPayment', Config.BASE_URL + '?tab=viewOrdersNeedsPayment', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'holdOrder')).then(() => cf.test(d, 'viewOrdersHold', Config.BASE_URL + '?tab=viewOrdersHold', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accounting')).then(() => cf.test(d, 'viewOrdersAccounting', Config.BASE_URL + '?tab=viewOrdersAccounting', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'preProduction')).then(() => cf.test(d, 'viewOrdersPreProduction', Config.BASE_URL + '?tab=viewOrdersPreProduction', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'production')).then(() => cf.test(d, 'viewOrdersProduction', Config.BASE_URL + '?tab=viewOrdersProduction', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'completed')).then(() => cf.test(d, 'viewOrdersCompleted', Config.BASE_URL + '?tab=viewOrdersCompleted', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'delivered')).then(() => cf.test(d, 'viewOrdersDelivered', Config.BASE_URL + '?tab=viewOrdersDelivered', true, _u));

    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'viewOrdersDelivered', Config.BASE_URL + '?tab=viewOrdersDelivered', true, _u));
    pc = pc.then(() => login.signOutClick(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));

    // PRODUCTION LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "production")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));

    pc = pc.then(() => tabFlow.tabClick(d, wd, 'viewAllProduction')).then(() => cf.test(d, 'viewAllProduction', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => tabFlow.tabByClassClick(d, wd, 'prodPreProduction')).then(() => cf.test(d, 'viewPreProduction', Config.BASE_URL + '?tab=viewPreProduction', true, _u));
    pc = pc.then(() => tabFlow.tabByClassClick(d, wd, 'prodProduction')).then(() => cf.test(d, 'viewProduction', Config.BASE_URL + '?tab=viewProduction', true, _u));
    pc = pc.then(() => tabFlow.tabByClassClick(d, wd, 'prodCompleted')).then(() => cf.test(d, 'viewCompleted', Config.BASE_URL + '?tab=viewCompleted', true, _u));
    pc = pc.then(() => tabFlow.tabByClassClick(d, wd, 'prodDelivered')).then(() => cf.test(d, 'viewDelivered', Config.BASE_URL + '?tab=viewDelivered', true, _u));
    pc = pc.then(() => tabFlow.tabByClassClick(d, wd, 'prodHold')).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));

    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'viewHold', Config.BASE_URL + '?tab=viewHold', true, _u));
    pc = pc.then(() => login.signOutClick(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));

    // PREPRODUCTION LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "pdaccounts")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));

    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingAll')).then(() => cf.test(d, 'accountingAll', Config.BASE_URL + '?tab=accountingAll', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'verifyDeposit')).then(() => cf.test(d, 'verifyDeposit', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingNeedsSurvey')).then(() => cf.test(d, 'accountingNeedsSurvey', Config.BASE_URL + '?tab=accountingNeedsSurvey', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingPreProduction')).then(() => cf.test(d, 'accountingPreProduction', Config.BASE_URL + '?tab=accountingPreProduction', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingProduction')).then(() => cf.test(d, 'accountingProduction', Config.BASE_URL + '?tab=accountingProduction', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingCompleted')).then(() => cf.test(d, 'accountingCompleted', Config.BASE_URL + '?tab=accountingCompleted', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'balanceDue')).then(() => cf.test(d, 'balanceDue', Config.BASE_URL + '?tab=balanceDue', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingDelivered')).then(() => cf.test(d, 'accountingDelivered', Config.BASE_URL + '?tab=accountingDelivered', true, _u));
    pc = pc.then(() => tabFlow.tabClick(d, wd, 'accountingHold')).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));

    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'accountingHold', Config.BASE_URL + '?tab=accountingHold', true, _u));
    pc = pc.then(() => login.signOutClick(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));



    pc = cf.handleOutput(d, pc, capability, 'Tab Flow');
    return pc;
}

module.exports = {tab_flow_step};