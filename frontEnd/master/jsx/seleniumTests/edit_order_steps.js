const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/login_fn');
const viewOrder = require('./FT/order/view_order_fn');
const editQuote = require('./FT/order/edit_order_fn');
const createQuote = require('./FT/quote/create_quote_fn');

let _u = undefined;

function edit_quote_step(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: Edit Order'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));

    // ADMIN LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => viewOrder.orderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));
    pc = pc.then(() => viewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => viewOrder.scrollOnDiv(d, wd, "windowBuilder")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => editQuote.clickEdit(d, wd, 3)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // SIZE & PANELS
    pc = pc.then(() => createQuote.widthInput(d, wd, 6000)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.heightInput(d, wd, 1000)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => createQuote.numberOfPanelsClick(d, wd, 'panels-6')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "swingDirection")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.swingDirectionSelection(d, wd, 'swingDirection-left')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "swingInOrOut")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.swingDirectionSelection(d, wd, 'movement-left-0')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.swingDirectionSelection(d, wd, 'swingInOrOut-Outswing')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));


    // COLOUR/FINISH
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.exteriorColourClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // EXTRAS
    pc = pc.then(() => createQuote.extrasClick(d, wd, 'sill-Standard-Upstand')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "chooseTricleVent")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => createQuote.extrasByClassClick(d, wd, 'extendedButtonTrickle')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "chooseScreen")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => createQuote.extrasClick(d, wd, 'screen--No Screen')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "nextSection")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    // HARDWARE
    pc = pc.then(() => createQuote.selectHardwareClick(d, wd, 'hardware-White')).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "nextHardwareTitle")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));
    pc = pc.then(() => createQuote.addToQuoteButtonClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['6000mm width x 1000mm height', 'Left']
    ));

    pc = cf.handleOutput(d, pc, capability, 'Edit Order by admin');
    return pc;
}

module.exports = {edit_quote_step};