const cf = require('./FT/commonFunctions/cf');
const login = require('./login_steps');
const create_quote = require('./create_quote_steps');
const view_quote = require('./view_quote_steps');
const view_order = require('./view_order_steps');
const edit_order = require('./edit_order_steps');
const tab_flow = require('./tab_flow_steps');
const Config = require('./FT/config/config');

let pc;
pc = cf.delayBySecond(1);

const browsers = Object.getOwnPropertyNames(Config.Capabilities);

if (process.argv.length === 3) {
    switch (process.argv[2].toLowerCase()) {
        case 'login':
            return loginStep(pc).then(() => {
                caseSummary();
                return;
            });
        case 'createquote':
            return createQuoteStep(pc).then(() => {
                caseSummary();
                return;
            });
        case 'viewquote':
            return viewQuoteStep(pc).then(() => {
                caseSummary();
                return;
            });
        case 'vieworder':
            return viewOrderStep(pc).then(() => {
                caseSummary();
                return;
            });
        case 'editorder':
            return editOrderStep(pc).then(() => {
                caseSummary();
                return;
            });
        case 'tabflow':
            return tabFlowStep(pc).then(() => {
                caseSummary();
                return;
            });
        default: {
            console.log('\x1b[31m%s\x1b[0m', process.argv[2] + ' test not found');
        }
    }
} else {
    executeAll(pc).then(() => {
        caseSummary();
    });
}

function caseSummary() {
    let passCount = filterCase('pass');
    let failCount = filterCase('fail');
    console.log('\x1b[33m%s\x1b[1m', '\n-------------TEST CASE-------------');
    console.log('\x1b[32m%s\x1b[1m', 'Pass : ' + passCount);
    console.log('\x1b[31m%s\x1b[0m', 'Fail : ' + failCount);
    console.log('\x1b[33m%s\x1b[0m', '-----------------------------------\n');
    if(failCount > 0) {
        process.exit(1);
    }
    process.exit(0);
}

function filterCase(caseResult) {
    var count = 0;
    Config.isSuccess.filter(value => {
        if(value === caseResult){
            count = count + 1;
        }
    });
    return count;
}

function loginStep(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => login.login_step(browsers[i]));
    }
    return pc;
}

function createQuoteStep(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => create_quote.create_quote_step(browsers[i]));
    }
    return pc;
}

function viewQuoteStep(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => view_quote.view_quote_step(browsers[i]));
    }
    return pc;
}

function viewOrderStep(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => view_order.view_quote_step(browsers[i]));
    }
    return pc;
}

function editOrderStep(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => edit_order.edit_quote_step(browsers[i]));
    }
    return pc;
}

function tabFlowStep(pc) {
    for (let i = 0; i < browsers.length; i++) {
        pc = pc.then(() => tab_flow.tab_flow_step(browsers[i]));
    }
    return pc;
}

function executeAll(pc) {
    pc = loginStep(pc);
    pc = createQuoteStep(pc);
    pc = viewQuoteStep(pc);
    pc = viewOrderStep(pc);
    return pc;
}