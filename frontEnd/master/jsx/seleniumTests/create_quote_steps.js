const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const login = require('./FT/login_fn');
const createQuote = require('./FT/quote/create_quote_fn');
const cf = require('./FT/commonFunctions/cf');

let _u = undefined;

function create_quote_step(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: Create Quote'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));

    // ADMIN LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));

    // SELECT DOOR TYPE
    pc = pc.then(() => createQuote.doorTypeClick(d, wd, 0)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    // SIZE & PANELS
    pc = pc.then(() => createQuote.widthInput(d, wd, 6000)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => createQuote.heightInput(d, wd, 1000)).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));

    pc = pc.then(() => createQuote.numberOfPanelsClick(d, wd, 'panels-6')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "swingDirection")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => createQuote.swingDirectionSelection(d, wd, 'swingDirection-left')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "swingInOrOut")).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => createQuote.swingDirectionSelection(d, wd, 'movement-left-0')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));
    pc = pc.then(() => createQuote.swingDirectionSelection(d, wd, 'swingInOrOut-Outswing')).then(() => cf.test(d, 'size', Config.BASE_URL + '?tab=size&', true, _u));


    // COLOUR/FINISH
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => createQuote.exteriorColourClick(d, wd)).then(() => cf.test(d, 'color', Config.BASE_URL + '?tab=color&', true, _u));
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'extras', Config.BASE_URL + '?tab=extras&', true, _u));

    // EXTRAS
    pc = pc.then(() => createQuote.extrasClick(d, wd, 'sill-Standard Upstand')).then(() => cf.test(d, 'extras', Config.BASE_URL + '?tab=extras&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "chooseTricleVent")).then(() => cf.test(d, 'extras', Config.BASE_URL + '?tab=extras&', true, _u));

    pc = pc.then(() => createQuote.extrasByClassClick(d, wd, 'extendedButtonTrickle')).then(() => cf.test(d, 'extras', Config.BASE_URL + '?tab=extras&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "chooseScreen")).then(() => cf.test(d, 'extras', Config.BASE_URL + '?tab=extras&', true, _u));

    pc = pc.then(() => createQuote.extrasClick(d, wd, 'screen--No Screen')).then(() => cf.test(d, 'extras', Config.BASE_URL + '?tab=extras&', true, _u));
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));

    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "nextSection")).then(() => cf.test(d, 'glass', Config.BASE_URL + '?tab=glass&', true, _u));
    pc = pc.then(() => createQuote.nextButtonClick(d, wd)).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));

    // HARDWARE
    pc = pc.then(() => createQuote.selectHardwareClick(d, wd, 'hardware-White')).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => createQuote.scrollOnDiv(d, wd, "nextHardwareTitle")).then(() => cf.test(d, 'hardware', Config.BASE_URL + '?tab=hardware&', true, _u));
    pc = pc.then(() => createQuote.addToQuoteButtonClick(d, wd)).then(() => cf.test(d, 'cart', Config.BASE_URL + '?tab=cart&', true, _u));

    pc = pc.then(() => cf.testContentByArray(d,
        ['PVC Panoramic Door', '6000mm width x 1000mm height', '973.50mm', 'Left', 'PVC', 'Outswing']
    ));
    pc = pc.then(() => createQuote.saveAsQuoteClick(d, wd)).then(() => cf.test(d, 'viewSavedQuoteConfirmation', Config.BASE_URL + '?tab=viewSavedQuoteConfirmation&', true, _u));

    // SEND EMAIL
    pc = pc.then(() => createQuote.emailQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => createQuote.emailAddressInput(d, wd, 'FTtest@demo.com')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => createQuote.emailMsgInput(d, wd, 'test')).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => createQuote.sendEmailClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));
    pc = pc.then(() => createQuote.sendEmailClick(d, wd)).then(() => cf.test(d, 'viewQuote', Config.BASE_URL + '?tab=viewQuote&', true, _u));

    pc = pc.then(() => createQuote.closeQuoteClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    //pc = pc.then(() => createQuote.deleteClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));
    //pc = pc.then(() => createQuote.acceptDeleteClick(d, wd)).then(() => cf.test(d, 'viewQuotes', Config.BASE_URL + '?tab=viewQuotes&', true, _u));

    return cf.handleOutput(d, pc, capability, 'Create Quote');
}

module.exports = {create_quote_step};