const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const login = require('./FT/login_fn');
const cf = require('./FT/commonFunctions/cf');

let _u = undefined;

function login_step(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);     
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: Login'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));

    // ADMIN LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => login.signOutClick(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));

    // PRODUCTION LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "production")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=viewAllProduction', true, _u));
    pc = pc.then(() => login.signOutClick(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));

    pc = pc.then(() => login.EmailInput(d, wd, "pdaccounts")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=verifyDeposit', true, _u));
    pc = pc.then(() => login.signOutClick(d, wd)).then(() => cf.test(d, 'signIn', Config.BASE_URL , true, _u));

    return cf.handleOutput(d, pc, capability, 'Login');
}

module.exports = {login_step};