const wd = require('selenium-webdriver');
const Config = require('./FT/config/config');
const cf = require('./FT/commonFunctions/cf');
const login = require('./FT/login_fn');
const viewOrder = require('./FT/order/view_order_fn');
const viewQuote = require('./FT/quote/view_quote_fn');

let _u = undefined;

function view_quote_step(capability) {
    let d = Config.getDriver(Config.Capabilities[capability]);
    d.manage().window().maximize();
    let pc;
    pc = d.get(Config.BASE_URL).then(() => cf.changeSessionTestName(d, 'UK: View Order'));
    pc = pc.then(() => d.get(Config.BASE_URL + "?tab=signIn"));

    // ADMIN LOGIN
    pc = pc.then(() => login.EmailInput(d, wd, "admin")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.PasswordInput(d, wd, "test")).then(() => cf.test(d, 'Sales Quoter', _u, _u, _u));
    pc = pc.then(() => login.signIn(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => login.menuClick(d, wd)).then(() => cf.test(d, 'Sales Quoter', Config.BASE_URL + '?tab=home', true, _u));
    pc = pc.then(() => viewOrder.orderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders', true, _u));

    pc = pc.then(() => viewOrder.viewOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => viewOrder.searchOrderInput(d, wd, "test")).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => viewOrder.itemPerPageSelect(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => viewOrder.sortOrderByIdClick(d, wd)).then(() => cf.test(d, 'viewOrders', Config.BASE_URL + '?tab=viewOrders&', true, _u));
    pc = pc.then(() => viewOrder.orderIdClick(d, wd)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder&', true, _u));

    pc = pc.then(() => viewOrder.onHoldSelection(d, wd, 1)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewOrder.onHoldSelection(d, wd, 0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));

    pc = pc.then(() => viewOrder.viewOrdersPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo&', true, _u));
    pc = pc.then(() => viewOrder.paymentOptionClick(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => viewOrder.last4DigitsOfCreditCardInput(d, wd, '1111')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => viewOrder.paymentAmountInput(d, wd, '2222')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => viewOrder.paymentDateInput(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => viewOrder.addPaymentClick(d, wd)).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => viewOrder.paymentOrderNotesInput(d, wd, 'Payment Order Notes test')).then(() => cf.test(d, 'paymentInfo', Config.BASE_URL + '?tab=paymentInfo', true, _u));
    pc = pc.then(() => viewOrder.closePaymentClick(d, wd)).then(() => cf.test(d, 'viewOrder',  Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewOrder.deletePaymentClick(d, wd)).then(() => cf.test(d, 'viewOrder',  Config.BASE_URL + '?tab=viewOrder', true, _u));
    //pc = pc.then(() => viewOrder.closeOrderClick(d, wd)).then(() => cf.test(d, 'viewOrders',  Config.BASE_URL + '?tab=viewOrders', true, _u));

    // PROJECT INFO
    pc = pc.then(() => viewQuote.pickupDeliverySelection(d, wd, 1)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.pickupDeliverySelection(d, wd, 0)).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.productionDetailNotesInput(d, wd, "Some note")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));

    // BILLING INFO
    pc = pc.then(() => viewQuote.billingContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.billingAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.billingZipInput(d, wd, "94402")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.billingPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.billingEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));

    // DELIVERY INFO
    pc = pc.then(() => viewQuote.deliveryContactInput(d, wd, "test name")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryAddress1Input(d, wd, "some address")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryAddress2Input(d, wd, "test")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryCityInput(d, wd, "San Mateo")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryStateInput(d, wd, "CA")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryZipInput(d, wd, "94402")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryPhoneInput(d, wd, "012-345-6789")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));
    pc = pc.then(() => viewQuote.deliveryEmailInput(d, wd, "test@test.com")).then(() => cf.test(d, 'viewOrder', Config.BASE_URL + '?tab=viewOrder', true, _u));


    pc = cf.handleOutput(d, pc, capability, 'View Order by salesPerson');
    return pc;
}

module.exports = {view_quote_step};