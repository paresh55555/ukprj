/* @flow */
import {
  getSiteBranding as getSiteBrandingAPI
} from '../api/branding-api'
// ------------------------------------
// Constants
// ------------------------------------
import {
  SET_SITE_BRANDING
} from './actionTypes';

export const setSiteBranding = (payload): Action => ({
  type: SET_SITE_BRANDING,
  payload: payload
})

export const getSiteBranding = (): Function => {
  return (dispatch: Function): Promise => {
    return getSiteBrandingAPI()
    .then((res) => {
      dispatch(setSiteBranding(res.data[0]))
    })
  }
}
