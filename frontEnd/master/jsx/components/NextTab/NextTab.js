import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {componentAction,optionAction} from '../../actions';
import Img from '../../components/Common/Img'

class NextTab extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentWillMount(){
    }

    render() {
        const {component} = this.props
        const title = component.traits.find((title)=>{return title.name === 'title'})
        const image = component.traits.find((title)=>{return title.name === 'imgUrl'}) || {}
        return (
            <div className="optionsBox text-center">
                <div id="nextSection">
                    <div className="nextButton" onClick={this.props.onNextTab}>
                        <div className="nextTitle">
                            <span className="nextTab">{!!title && title.value}</span>
                        </div>
                        <div className="nextImage">
                            { image && image.value ?
                                <Img style={{width:39,height:39}} src={image} />
                                :
                                <div className="img_placeholder" style={{width:39,height:39}}></div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default connect(state => ({
    cart: state.component.cart
}), Object.assign({}, componentAction))(NextTab);