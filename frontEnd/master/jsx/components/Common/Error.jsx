import React from 'react';
import { Router, Route, Link, History } from 'react-router';
import ContentWrapper from '../Layout/ContentWrapper';
import { Grid, Row, Col, Panel, Button } from 'react-bootstrap';
import {MAIN_HOST} from '../../constant.js';

class Error extends React.Component {
	
    render() {
    	
        return (
            <div className="abs-center wd-xl">
                <div className="text-center mb-xl">
                    <div className="text-lg mb-lg">Some Thing Went Wrong</div>
                    <p className="lead m0">We couldn't find this page.</p>
                    <p>The page you are looking for does not exists.</p>
                </div>
                <div className="input-group mb-xl">
                    
                </div>

            </div>
            );
    }

}

export default Error;
