import React, {Component, PropTypes} from 'react';

class DoorImage extends React.Component{
    constructor() {
        super();
    }
    getDefaultImage(type){
        switch (type){
            case 'panel':
                return "/img/slide-panel.gif"
                break;
            case 'left':
                return "/img/swing-left.gif"
                break;
            case 'right':
                return "/img/swing-right.gif"
                break;
            case 'leftSlide':
                return "/img/slide-left.gif"
                break;
            case 'rightSlide':
                return "/img/slide-right.gif"
                break;
            case 'divider':
                return "/img/divider-bar.gif"
                break;
            default:
                return ''
        }
    }
    render(){
        return(
            <img className="doorImage" src={this.props.imgUrl ? '/'+this.props.imgUrl : this.getDefaultImage(this.props.doorType)}
                 onClick={this.props.onClick} />
        )
    }
}
DoorImage.propTypes = {
    doorType: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}
export default DoorImage;
