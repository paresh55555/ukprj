import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {componentAction} from '../../actions';

import DoorImage from '../../components/Common/DoorImage'
import Title from '../../components/Common/Title'

class PanelMovement extends React.Component {

    constructor() {
        super();
        this.state = {
            numberLeft: null,
        };
        this.setPanelMovement = this.setPanelMovement.bind(this);
    }

    componentWillMount(){
        const cart = JSON.parse(localStorage.getItem('cart'));
        if(cart) {
            this.setState({numberLeft: cart.numberLeft})
            this.props.setPanelMovement(cart.numberLeft);
        }
    }

    setPanelMovement(num){
        this.setState({numberLeft: num})
        this.props.setPanelMovement(num);
    }

    buildPanels(cart, component){
        var numberLeft = null,
            panels = [];
        switch(cart.swingDoorDirection){
            case 'left':
                numberLeft = 0
                panels = this.buildSinglePanelMovement(cart, component, numberLeft);
                break;
            case 'right':
                numberLeft = cart.panels - 1
                panels = this.buildSinglePanelMovement(cart, component, numberLeft);
                break;
            case 'both':
                for (let a = 1; a < cart.panels - 2; a++) {
                    let panel = this.buildSinglePanelMovement(cart, component, a);
                    panels.push(panel);
                }
                break;
        }
        return panels
    }

    buildSinglePanelMovement(cart, component, numberLeft){
        const title = !!component.traits && component.traits.find((trait)=>trait.name === cart.swingDoorDirection + 'Title')
        const leftTitle = !!component.traits && component.traits.find((trait)=>trait.name === cart.swingDoorDirection+'LeftTitle')
        const rightTitle = !!component.traits && component.traits.find((trait)=>trait.name === cart.swingDoorDirection+'RightTitle')
        const numPlace = !!component.traits && component.traits.find((trait)=>trait.name === cart.swingDoorDirection + 'NumPlace')
        return <div className="panelOptions">
            <div className="horizontalOption" onClick={this.setPanelMovement.bind(this, numberLeft)}>
                <div className="horizontalPanelImage">
                    {numberLeft === this.state.numberLeft ? <div className="selectedPanels"></div> : null}
                    {this.getPattern(cart.swingDoorDirection, numberLeft)}
                </div>
                <div className="thumbnailTitle">
                    {cart.swingDoorDirection === 'both' ?
                        <span>
                            {numPlace && numPlace.value && numPlace.value === 'left' ? numberLeft + ' ' : null}
                            <span>{!!leftTitle && leftTitle.value}</span>
                            {numPlace && numPlace.value && numPlace.value === 'right' ? ' ' + numberLeft : null} /
                            {numPlace && numPlace.value && numPlace.value === 'left' ? (cart.panels - numberLeft - 2) + ' ' : null}
                            <span>{!!rightTitle && rightTitle.value}</span>
                            {numPlace && numPlace.value && numPlace.value === 'right' ? ' ' + (cart.panels - numberLeft - 2) : null}
                        </span>
                    :
                        <span>
                            {numPlace && numPlace.value && numPlace.value === 'left' ? cart.panels+' ' : null}
                            {!!title && title.value}
                            {numPlace && numPlace.value && numPlace.value === 'right' ? ' '+cart.panels : null}
                        </span>
                    }
                </div>
            </div>
        </div>
    }

    getPattern(type, numberLeft){
        var slidePanels = []
        const {cart, component} = this.props
        const leftImg = component.traits.find(function(el,i){return el.name === 'leftImg'});
        const rightImg = component.traits.find(function(el,i){return el.name === 'rightImg'});
        const leftSlideImg = component.traits.find(function(el,i){return el.name === 'leftSlideImg'});
        const rightSlideImg = component.traits.find(function(el,i){return el.name === 'rightSlideImg'});
        const dividerImg = component.traits.find(function(el,i){return el.name === 'dividerImg'});
        let funcCap = function(){return false},
            left = (i)=>{return <DoorImage key={i} doorType={'left'} imgUrl={leftImg && leftImg.value} onClick={funcCap} />},
            right = (i)=>{return <DoorImage key={i} doorType={'right'} imgUrl={rightImg && rightImg.value} onClick={funcCap} />},
            leftSlide = (i)=>{return <DoorImage key={i} doorType={'leftSlide'} imgUrl={leftSlideImg && leftSlideImg.value} onClick={funcCap} />},
            rightSlide = (i)=>{return <DoorImage key={i} doorType={'rightSlide'} imgUrl={rightSlideImg && rightSlideImg.value} onClick={funcCap} />},
            divider = (i)=>{return <DoorImage key={i} doorType={'divider'} imgUrl={dividerImg && dividerImg.value} onClick={funcCap} />};
        switch (type){
            case 'left':
                slidePanels = Array.apply(null, Array(cart.panels - 1)).map(function(){return leftSlide})
                return [left].concat(slidePanels, divider).map((func, i)=>{ return func(i) })
                break;
            case 'right':
                slidePanels = Array.apply(null, Array(cart.panels - 1)).map(function(){return rightSlide})
                return [divider].concat(slidePanels, right).map((func, i)=>{ return func(i) })
                break;
            case 'both':
                let leftSlidePanels = Array.apply(null, Array(numberLeft)).map(function(){return leftSlide})
                let rightSlidePanels = Array.apply(null, Array(cart.panels - numberLeft - 2)).map(function(){return rightSlide})
                return [left].concat(leftSlidePanels, divider, rightSlidePanels, right).map((func, i)=>{ return func(i) })
                break;
            default:
                return ''
        }
    }

    render(){
        const {cart, component} = this.props
        return(
            <div>
                <Title component={this.props.component} />
                <div className="optionsBox">
                    {this.buildPanels(cart, component)}
                </div>
            </div>
        )
    }
}

export default connect(state => ({
    cart: state.component.cart
}), Object.assign({}, componentAction))(PanelMovement);