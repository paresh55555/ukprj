import React from 'react';
import pubsub from 'pubsub-js';
import HeaderRun from './Header.run'
import { NavDropdown, MenuItem, NavItem } from 'react-bootstrap';
import { Router, Route, Link, History } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';
import Branding from './Branding'
import Loader from './Loader'
import { connect } from 'react-redux';
import {brandingAction as actions, authAction } from '../../actions';
@connect(state => (state))
class HeaderHorizontal extends React.Component {

    componentWillMount(){
        this.props.dispatch(actions.getSiteBranding());
    }
    render() {


        return (
            <header className="topnavbar-wrapper">
                {this.props.branding.isLoading?
                    <div className="pageLoading">
                        <Loader />
                    </div>
                    :''
                }
                <Branding branding={this.props.branding.siteBranding} />
            </header>
            );
    }

}

export default HeaderHorizontal;
