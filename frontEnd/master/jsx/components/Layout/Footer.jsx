import React from 'react';

class Footer extends React.Component {

    render() {
    	let year = new Date().getFullYear();
        return (
            <footer>
                <span>&copy; {year} - Sales Quoter</span>
            </footer>
        );
    }

}

export default Footer;
