import React, { Component, PropTypes } from 'react';
import {getImageUrl} from '../../components/Common/helpers'
import ColorOption from '../../components/Color/ColorOption'

export default class ColorSubGroups extends React.Component {
    constructor() {
        super()
        this.state = {
            selectedSubGroup: null,
            selectedIds: []
        }
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
        this.checkProperties = this.checkProperties.bind(this);
    }

    componentWillMount() {
        if (!this.state.selectedSubGroup && this.props.subGroups && this.props.subGroups.length) {
            this.setState({selectedSubGroup: this.props.subGroups[0].id})
        }
        if(this.props.component && this.props.component.traits &&  this.checkProperties('hasDefault')) {
            let selected = this.props.component.traits.find(item => item.name === 'selectedIds');
            if(selected) {
                const selectedIds = JSON.parse(selected.value);
                this.setState({selectedIds});
            }
        }
    }

    selectSubGroup(id) {
        this.setState({selectedSubGroup: id})
    }

    addSelected(id) {
        return () => {
            const sameId = this.state.selectedIds.find(item=>item === id);
            let selectedFromTraits = this.props.component.traits.find(item => item.name === 'selectedIds');
            selectedFromTraits = selectedFromTraits ? JSON.parse(selectedFromTraits.value) : [];
            const multiples = this.checkProperties('multiples')
            const selected =  [];
            if (sameId === id && multiples) {
                const index = selected.indexOf(id);
                selected.splice(index, 1);
            } else {
                selected.push(id);
            }
            this.setState({selectedIds: multiples ? selected.concat(selectedFromTraits) : selected});
        };
    }

    checkSelection(id) {
        const result = this.state.selectedIds.find(item => {
            if (item === id) {
                return item
            }
        });
        return !!result;
    }

    checkProperties(name) {
        let property = this.props.component.traits.find((trait) => (trait.name === name ? trait : null));
        if (property && property.value) {
            return true;
        }
        return false;
    }

    render() {
        const self = this
        const {subGroups} = this.props
        var subGroupContent = subGroups.map((subGroup, i) => {
            if (subGroup.id === self.state.selectedSubGroup) {
                let colors = subGroup.options;
                if (colors && colors.length) {
                    return (
                        <div className="optionsBox" key={i}>
                            {colors.map((color, index) => {
                                return ([
                                    <ColorOption key={color.id} color={color} id={color.id}
                                                 selected={this.checkSelection(color.id)}
                                                 addSelected={this.addSelected}/>,
                                    (index+1)%5 === 0 ? <div className='clearfix'></div> : null
                                ])
                            })}
                        </div>
                    )
                }
            }
        })
        return (
            <div>
                <div className="optionsBox">
                    {subGroups.map((subGroup, index) => {
                        const titleClass = (this.state.selectedSubGroup === subGroup.id) ? "" : "subColorMenuNotSelected";
                        const title = !!subGroup && subGroup.traits.find((trait) => trait.name === 'title');
                        const image = !!subGroup && subGroup.traits.find((trait) => trait.name === 'imgUrl');
                        return (
                            <div key={subGroup.id} className="subColorMenu">
                                <div className={titleClass}>
                                    { image && image.value ?
                                        <div onClick={(...args) => self.selectSubGroup(subGroup.id, ...args)}
                                             className="subColorThumbnail thinBorder2"
                                             style={{
                                                 backgroundImage: `url(${getImageUrl(image)})`,
                                                 backgroundSize: getImageUrl(image).search(/.svg/) > 0 ? '' : 'contain'
                                             }}></div>
                                        :
                                        <div onClick={(...args) => self.selectSubGroup(subGroup.id, ...args)}
                                             className="subColorImage">
                                            <div className="subColorThumbnail img_placeholder"></div>
                                        </div>
                                    }
                                    <div className="thumbnailTitle">
                                        {title && title.value}
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
                {subGroupContent}
            </div>
        )
    }
}