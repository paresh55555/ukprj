import React, { Component, PropTypes } from 'react';
import {getNoteText} from '../../components/Common/helpers'
import Img from '../../components/Common/Img'

export default class ColorOption extends React.Component {
    render(){
        const {color, addSelected, id} = this.props
        const title = !!color && color.traits.find((trait)=>trait.name ==='title');
        const image = !!color && color.traits.find((trait)=>trait.name ==='imgUrl');
        const hex = !!color && color.traits.find((trait)=>trait.name ==='hex');
        const notes = !!color && color.traits.filter((trait)=>trait.name ==='note');
        return(
            <div className="thumbOption">
                <div className="horizontalOption">
                    <div className="horizontalImage" onClick={addSelected(id)}>
                        {this.props.selected ? <div className="demoSelected"></div> : null}
                        { image && image.value ?
                            <Img style={{width:121,height:121}} src={image} />
                            :
                            hex && hex.value ?
                                <div style={{background: "#"+hex.value.replace('#','')}}
                                     className="hexPreview thumbnail2 thinBorder2"></div>
                                :
                                <div className="img_placeholder" style={{width:121,height:121}}></div>

                        }
                    </div>
                    <div className="horizontalImage">
                    </div>
                    <div className="text-center">
                        <h4 className="thumbnailTitle">{!!title && title.value}</h4>
                        {!!notes.length && notes.map((note, i)=>{
                            return(<p key={i} className="thumbnailNote" dangerouslySetInnerHTML={{__html: getNoteText(note)}}></p>)
                        })}
                    </div>
                </div>
            </div>
        )
    }
}