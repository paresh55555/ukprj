import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ColorOption from '../../components/Color/ColorOption'
import ColorSubGroups from '../../components/Color/ColorSubGroups'
import Title from '../../components/Common/Title'
import {cartAction} from '../../actions';
class Color extends React.Component {
    constructor(){
        super()
        this.state = {
            selectedGroup: null
        };
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
        this.checkProperties = this.checkProperties.bind(this);
    }
    componentWillMount(){
        if(!this.state.selectedGroup && this.props.groups && this.props.groups.length){
            this.setState({selectedGroup: this.props.groups[0].id})
        }
        if(this.props.component && this.props.component.traits &&  this.checkProperties('hasDefault')) {
            let selected = this.props.component.traits.find(item => item.name === 'selectedIds');
            if(selected) {
                const selectedIds = JSON.parse(selected.value);
                let cartIds = this.props.cart.cartItems;
                cartIds[this.props.component.id]=selectedIds;
                this.props.setCartItem(cartIds)
            }
        }
    }
    selectGroup(id){
        this.setState({selectedGroup: id})
    }

    addSelected(id) {
        return () => {
            let cartIds = this.props.cart.cartItems;
            let comopnentIds = this.props.cart.cartItems[this.props.component.id] || []
            const sameId = comopnentIds.find(item=>item === id);
            let selectedFromTraits = this.props.component.traits.find(item => item.name === 'selectedIds');
            selectedFromTraits = selectedFromTraits ? JSON.parse(selectedFromTraits.value) : [];
            const multiples = this.checkProperties('multiples');
            const selected = multiples ? comopnentIds : [];
            if(!multiples) {
                selected.push(id);
            } else {
                if (sameId === id) {
                    if(multiples) {
                        const index = selected.indexOf(id);
                        selected.splice(index, 1);
                    }
                } else {
                    selected.push(id);
                }
            }
            let selectedIds = multiples ? selected.concat(selectedFromTraits) : selected;
            selectedIds = selectedIds.filter(function(elem, index, self) {
                                return index == self.indexOf(elem);
                            })
            cartIds[this.props.component.id]=selectedIds;
            this.props.setCartItem(cartIds)
        };
    }
    checkSelection(id) {
        let cartIds = this.props.cart.cartItems;
        let comopnentIds = this.props.cart.cartItems[this.props.component.id] || []
        const result = comopnentIds.find(item => {if(item === id){return item}});
        return !!result;
    }

    checkProperties(name) {
        let property = this.props.component.traits.find((trait)=>(trait.name === name ? trait : null));
        if(property && property.value) {
            return true;
        }
        return false;
    }

    render(){
        const self = this
        const {component, groups} = this.props
        var content = null;
        if(component.options && component.options.length){
            content = <div className="optionsBox">
                {component.options.map((color,index) =>{
                    return ([
                        <ColorOption key={color.id} color={color} id={color.id} selected={this.checkSelection(color.id)} addSelected={this.addSelected}  />,
                        (index+1)%5 === 0 ? <div className='clearfix'></div> : null
                    ])
                })}
            </div>
        }else{
            content = [<div key={component.id} className="optionsBox">
                {groups.map((group,index) =>{
                    const title = !!group && group.traits.find((trait)=>trait.name ==='title');
                    const buttonClass = "colorGroupButton" + ((this.state.selectedGroup === group.id) ? "" : "-NotSelected");
                    return (
                        <div key={group.id} className="colorGroupMenu">
                            <div onClick={(...args) => self.selectGroup(group.id, ...args)} className={"groupTitle mb-sm mr-sm btn btn-success btn-outline "+buttonClass}>
                                {title && title.value}
                            </div>
                        </div>
                    )
                })}
            </div>,
                groups.map((group,i) =>{
                    if(group.id === self.state.selectedGroup){
                        let colors = group.options;
                        let subGroups = group.subGroups;
                        if(colors && colors.length){
                            return (
                                <div className="optionsBox" key={i}>
                                    {colors.map((color,index) =>{
                                        return ([
                                            <ColorOption key={color.id} color={color} id={color.id} selected={this.checkSelection(color.id)} addSelected={this.addSelected} />,
                                            (index+1)%5 === 0 ? <div className='clearfix'></div> : null
                                        ])
                                    })}
                                </div>
                            )
                        }else if(subGroups && subGroups.length){
                            return (
                                <ColorSubGroups
                                    key={i}
                                    subGroups={subGroups}
                                    onSelectSubGroup={self.selectSubGroup}
                                    selectedSubGroup={self.state.selectedSubGroupID}
                                    component={this.props.component}
                                    groupID={self.state.selectedGroupID} />
                            )
                        }
                    }
                })]
        }
        return(
            <div>
                <Title component={component} />
                {content}
            </div>
        )
    }
}
export default connect(state => ({
    cart: state.cart,
}), Object.assign({}, cartAction))(Color);