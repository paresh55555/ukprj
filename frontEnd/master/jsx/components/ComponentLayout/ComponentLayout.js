import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import HalfLineOption from '../../components/ComponentLayout/HalfLineOption'
import FullLineOption from '../../components/ComponentLayout/FullLineOption'
import ThumbnailOption from '../../components/ComponentLayout/ThumbnailOption'
import Title from '../../components/Common/Title'
import {cartAction} from '../../actions';

const optionComponents = {
    halfline: HalfLineOption,
    fullline: FullLineOption,
    thumb: ThumbnailOption,
};

class ComponentLayout extends React.Component {
    constructor(props) {
        super(props);
        this.addSelected = this.addSelected.bind(this);
        this.checkSelection = this.checkSelection.bind(this);
        this.checkProperties = this.checkProperties.bind(this);
    }

    componentWillMount() {
        if(this.props.component && this.props.component.traits && this.checkProperties('hasDefault')) {
            let selected = this.props.component.traits.find(item => item.name === 'selectedIds');
            if(selected) {
                const selectedIds = JSON.parse(selected.value);
                let cartIds = this.props.cart.cartItems;
                cartIds[this.props.component.id]=selectedIds;
                this.props.setCartItem(cartIds)
            }
        }

    }
    addSelected(id) {
       return () => {
            let cartIds = this.props.cart.cartItems;
            let comopnentIds = this.props.cart.cartItems[this.props.component.id] || []
            const sameId = comopnentIds.find(item=>item === id);
            let selectedFromTraits = this.props.component.traits.find(item => item.name === 'selectedIds');
            selectedFromTraits = selectedFromTraits ? JSON.parse(selectedFromTraits.value) : [];
            let multiples = this.checkProperties('multiples')
            
            let selected =  [];
            if(multiples){
                selected=comopnentIds;
            }
            if (sameId === id) {
                const index = selected.indexOf(id)
                if(comopnentIds.length === 1){
                    return selected=comopnentIds;
                }
                selected.splice(index, 1);
            } else {
                selected.push(id);
            }
            let selectedIds = multiples ? selected.concat(selectedFromTraits) : selected;
            selectedIds = selectedIds.filter(function(elem, index, self) {
                                return index == self.indexOf(elem);
                            })
            cartIds[this.props.component.id]=selectedIds;
            this.props.setCartItem(cartIds)
            
        };
    }
    checkSelection(id) {
        let cartIds = this.props.cart.cartItems;
        let comopnentIds = this.props.cart.cartItems[this.props.component.id] || []
        const result = comopnentIds.find(item => {if(item === id){return item}});
        return !!result;
    }

    getOptionContent(option){
        const type = this.props.type || 'HalfLine';
        const OptionComponent = optionComponents[this.props.type];
        return <OptionComponent key={option.id} id={option.id} option={option} selected={this.checkSelection(option.id)} addSelected={this.addSelected} />
    }
    checkProperties(name) {
        let property = this.props.component.traits.find((trait)=>(trait.name === name ? trait : null));
        if(property && property.value) {
            return true;
        }
        return false;
    }
    render(){
        const self = this;
        const {component} = this.props;
        return(
            <div>
                <Title component={component} />
                <div className="optionsBox">
                    {!!component && component.options.map((option, index)=>{
                        let clearfix = ''
                        if(self.props.type === 'thumb'){
                            clearfix = <div key={index} className={(index+1)%5 === 0 ? 'clearfix' : ''}>{(index+1)%5 === 0 ? <hr key={index} /> : ''}</div>
                        }
                        return ([self.getOptionContent(option),clearfix])
                    })}
                </div>
            </div>
        )
    }
}
export default connect(state => ({
    cart: state.cart,
}), Object.assign({}, cartAction))(ComponentLayout);