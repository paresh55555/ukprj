import React, { Component, PropTypes } from 'react';
import {getNoteText} from '../../components/Common/helpers'
import Img from '../../components/Common/Img'

export default class ThumbnailOption extends React.Component {
    render(){
        const {option, addSelected, id} = this.props
        const title = !!option && option.traits.find((trait)=>trait.name ==='title');
        const image = !!option && option.traits.find((trait)=>trait.name ==='imgUrl');
        const notes = !!option && option.traits.filter((trait)=>trait.name ==='note');
        return(
            <div className="thumbOption">
                <div className="horizontalOption">
                    <div className="horizontalImage" onClick={addSelected(id)}>
                        {this.props.selected ? <div className="demoSelected" style={{marginTop: '40px'}}></div> : null}
                        { image && image.value ?
                            <Img style={{width:121,height:191}} src={image} />
                            :
                            <div className="img_placeholder" style={{width:121,height:191}}></div>
                        }
                    </div>
                    <div className="thumbnailTitle">
                        <span>{!!title && title.value}</span>
                    </div>
                    <div className="thumbnailNote">
                        {!!notes.length && notes.map((note, i)=>{
                        return(<p key={i} className="horizontalNote clearfix" dangerouslySetInnerHTML={{__html: getNoteText(note)}}></p>)
                    })}
                    </div>
                </div>
            </div>
        )
    }
}