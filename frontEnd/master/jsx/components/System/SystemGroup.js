import React, { Component, PropTypes } from 'react';
import { Grid, Row, Col, Panel, Button, Form, ControlLabel, FormControl, Radio, FormGroup } from 'react-bootstrap';
import { Link } from 'react-router';
import SystemGrid from './SystemGrid'
export default class SystemGroup extends React.Component {
	render(){
		const {systemGroup} = this.props;
		const title =!!systemGroup && systemGroup.traits.find((trait)=>trait.name ==='title');
		const notes = !!systemGroup && systemGroup.traits.filter((trait)=>trait.name ==='note');
		return(
			<div className="panel panel-primary mainBox">
				<div className="bg-info-dark defaultWidth nofloat">
					<h3>{!!title && title.value}</h3>
					{!!notes.length && notes.map((note)=>{
						return(<p key={note.id}>{note.value}</p>)
					})}
				</div>
				<div className="panel-body">
					{!!systemGroup.systems && !!systemGroup.systems.length && systemGroup.systems.map((system,index)=>{
						index = index + 1
						if(!!index && index%3 === 0){
							return [<SystemGrid key={system.id} system={system}/>,<div key={index} id={index} className="clearfix"></div>]
						}
						else{
							return (<SystemGrid key={system.id} system={system}/>)
						}
					})}
				</div>
			</div>
		)
	}
}