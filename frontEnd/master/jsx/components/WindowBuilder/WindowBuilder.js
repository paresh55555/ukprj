import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import {componentAction} from '../../actions';
import * as builder from './_windowsBuilder';
import Title from "../Common/Title";

class WindowBuilder extends React.Component{
    constructor(){
        super();
    }
    componentDidMount(){
        builder.setWindowsForm(builder.readWindowForm())
        builder.processWindowTypes(this.getData())
        builder.setUpWindowHeightAndWidth()
        builder.restoreFormState()
    }
    selectType = (e) => {
        builder.selectWindowType.call(e.currentTarget);
    }

    getData(){
        return [
            {"id":1,"windowSection":1,"Layout":"V-1","stacking":"vertical","order":1,"img":"\/img\/windows\/1\/H-1.svg","minWidthSize":250,"maxWidthSize":2000,"minHeightSize":250,"maxHeightSize":2000},
            {"id":2,"windowSection":2,"Layout":"V-1-1","stacking":"vertical","order":1,"img":"\/img\/windows\/2\/V-1-1.svg","minWidthSize":250,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":3,"windowSection":2,"Layout":"H-1-1","stacking":"horizontal","order":2,"img":"\/img\/windows\/2\/H-1-1.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":250,"maxHeightSize":2000},
            {"id":4,"windowSection":3,"Layout":"H-2-1","stacking":"horizontal","order":1,"img":"\/img\/windows\/3\/H-2-1.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":5,"windowSection":3,"Layout":"H-1-2","stacking":"horizontal","order":2,"img":"\/img\/windows\/3\/H-1-2.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":6,"windowSection":3,"Layout":"V-2-1","stacking":"vertical","order":3,"img":"\/img\/windows\/3\/V-2-1.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":7,"windowSection":3,"Layout":"V-1-2","stacking":"vertical","order":4,"img":"\/img\/windows\/3\/V-1-2.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":8,"windowSection":3,"Layout":"V-1-1-1","stacking":"vertical ","order":5,"img":"\/img\/windows\/3\/V-1-1-1.svg","minWidthSize":250,"maxWidthSize":2000,"minHeightSize":750,"maxHeightSize":3000},
            {"id":9,"windowSection":3,"Layout":"H-1-1-1","stacking":"horizontal","order":6,"img":"\/img\/windows\/3\/H-1-1-1.svg","minWidthSize":750,"maxWidthSize":3000,"minHeightSize":250,"maxHeightSize":2000},
            {"id":22,"windowSection":4,"Layout":"V-2-1-1","stacking":"vertical","order":15,"img":"\/img\/windows\/4\/V-2-1-1.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":750,"maxHeightSize":3000},
            {"id":17,"windowSection":4,"Layout":"H-1-1-2","stacking":"horizontal","order":14,"img":"\/img\/windows\/4\/H-1-1-2.svg","minWidthSize":750,"maxWidthSize":3000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":16,"windowSection":4,"Layout":"H-1-2-1","stacking":"horizontal","order":13,"img":"\/img\/windows\/4\/H-1-2-1.svg","minWidthSize":750,"maxWidthSize":3000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":15,"windowSection":4,"Layout":"H-2-1-1","stacking":"horizontal","order":12,"img":"\/img\/windows\/4\/H-2-1-1.svg","minWidthSize":750,"maxWidthSize":3000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":14,"windowSection":4,"Layout":"V-3-1","stacking":"vertical","order":11,"img":"\/img\/windows\/4\/V-3-1.svg","minWidthSize":750,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":12,"windowSection":4,"Layout":"H-2-2","stacking":"horizontal","order":9,"img":"\/img\/windows\/4\/H-2-2.svg","minWidthSize":500,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":11,"windowSection":4,"Layout":"V-1-1-1-1","stacking":"vertical","order":8,"img":"\/img\/windows\/4\/V-1-1-1-1.svg","minWidthSize":250,"maxWidthSize":2000,"minHeightSize":1000,"maxHeightSize":4000},
            {"id":10,"windowSection":4,"Layout":"H-1-1-1-1","stacking":"horizontal","order":7,"img":"\/img\/windows\/4\/H-1-1-1-1.svg","minWidthSize":1000,"maxWidthSize":4000,"minHeightSize":250,"maxHeightSize":2000},
            {"id":18,"windowSection":5,"Layout":"V-4-1","stacking":"vertical","order":15,"img":"\/img\/windows\/5\/V-4-1.svg","minWidthSize":1000,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":19,"windowSection":5,"Layout":"V-1-4","stacking":"vertical","order":16,"img":"\/img\/windows\/5\/V-1-4.svg","minWidthSize":1000,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":20,"windowSection":5,"Layout":"H-2-1-2","stacking":"horizontal","order":17,"img":"\/img\/windows\/5\/H-2-1-2.svg","minWidthSize":750,"maxWidthSize":3000,"minHeightSize":500,"maxHeightSize":2000},
            {"id":21,"windowSection":6,"Layout":"V-5-1","stacking":"vertical","order":18,"img":"\/img\/windows\/6\/V-5-1.svg","minWidthSize":1250,"maxWidthSize":2000,"minHeightSize":500,"maxHeightSize":2000}]
    }
    buildWindowSection(windows, section){
        return [
            <div className="windowSection">
                <img src={"/img/windows/sections/Section"+section+".png"} />
            </div>,
            this.buildWindowOptions(windows)
        ]
    }
    buildWindowOptions(windows){
        var self = this;
        return windows.map((window, i)=>{
            return (
                <div id={window.Layout} className="windowButton" onClick={self.selectType}>
                    <div className="windowButtonImage"><img src={window.img} /></div>
                </div>
            )
        })
    }
    getWindowOptions(data){
        var section = null;
        var windows = [];
        var result = [];
        for (var a = 0; a < data.length; a++) {

            var window = data[a];

            if (!section) {
                section = window.windowSection;
            }
            if (window.windowSection != section) {

                var newSection = this.buildWindowSection(windows, section);
                var sectionLine = <div className="windowSeparator"></div>;

                section = window.windowSection;

                result.push([newSection, sectionLine]);
                windows = new Array();
            }

            if (builder.getOptionIndex(window.Layout, 0) == 'V') {
                windows.push(window);
            }
        }
        result.push([this.buildWindowSection(windows, section), <div className="sectionEnd"></div>]);
        return result
    }
    render(){
        const data = this.getData()
        return(
            <Row>
                <Col sm={12} >
                    <Title component={this.props.component} />
                    <div className="form-group dimensionFrom optionsBox">
                        <div className="col-sm-5">
                            <label className="control-label">
                                <span>Width (round to nearest mm)</span>
                            </label>
                            <div className="input-group m-b">
                                <input className="form-control" type="text" id="width"/>
                                <span className="input-group-addon">
                                <span>mm</span>
                            </span>
                            </div>
                            <br />
                            <label className="control-label">
                                <span>Height (round to nearest mm)</span>
                            </label>
                            <div className="input-group m-b">
                                <input className="form-control" type="text" id="height"/>
                                <span className="input-group-addon">
                                <span>mm</span>
                            </span>
                            </div>
                        </div>
                    </div>
                    <div id="windowOptionsSection">
                        <div className="bg-info-dark defaultWidth optionTitle">Select Your Type of Window</div>
                        <div id="windowOptions">
                            {this.getWindowOptions(data)}
                        </div>
                    </div>
                    <div id="windowBuilderSection">
                        <div className="bg-info-dark defaultWidth optionTitle">Enter Your Windows Dimensions</div>
                        <div className="sashNote"></div>
                        <div id="windowBuilder"></div>
                        <div id="operations">
                            <div id="none" className="operation hingeBorder">
                                <img width="100%" height="100%" src="/img/openers/white/noneWhite.svg"/>
                            </div>
                            <div id="hingeBottom" className="operation hingeBorder">
                                <img width="100%" height="100%" src="/img/openers/white/hingeBottomWhite.svg"/>
                            </div>
                            <div id="hingeTop" className="operation hingeBorder">
                                <img width="100%" height="100%" src="/img/openers/white/hingeTopWhite.svg"/>
                            </div>
                            <div id="hingeRight" className="operation hingeBorder ">
                                <img width="100%" height="100%" src="/img/openers/white/hingeRightWhite.svg"/>
                            </div>
                            <div id="hingeLeft" className="operation hingeBorder">
                                <img width="100%" height="100%" src="/img/openers/white/hingeLeftWhite.svg"/>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        )
    }
}

export default connect(state => ({
    cart: state.component.cart
}), Object.assign({}, componentAction))(WindowBuilder);