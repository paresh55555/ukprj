'use strict'

var sash = {"marginHeight": 5, "marginWidth": 5};
var frame = {"width": 700, "height": 400, "marginHeight": 10, "marginWidth": 10, "border": 1};
var xMult = 1;
var yMult = 1;

var textBoxMargin = 3;
var textBoxHeight = 22 + (textBoxMargin * 2);
var textBoxWidth = 26 + (textBoxMargin * 2);
var border = 1;
var maxUIHeight = 400;
var maxUIWidth = 700;

var delayAmount = 300;
var chooseDimensionLeftRight = 'left';
var chooseDimensionTopBottom = 'top';
var selectedWindow;

var windowsForm = {};

// Utils

function writeLocalStorageWithKeyAndData(key, data) {

    if (typeof(Storage) !== "undefined") {
        try {
            localStorage[key] = JSON.stringify(data);
        } catch (e) {
            alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Sales Quoter is not compatible with "Private Browsing Mode"');
        }


    }

}

function readLocalStorageWithKey(key) {

    var data = undefined;
    if (typeof(Storage) !== "undefined") {
        if (typeof(localStorage) !== "undefined") {
            if (localStorage[key] !== "undefined") {
                if (localStorage[key]) {
                    data = JSON.parse(localStorage[key]);
                }

            }
        }
    }
    return data;

}

function writeWindowsForm() {

    if (typeof(windowsForm) == "undefined") {
        windowsForm = windowFormDefaults();
    } else {
        writeLocalStorageWithKeyAndData("windowsForm", windowsForm);
    }
}

export function readWindowForm() {

    var windowsForm = readLocalStorageWithKey('windowsForm');
    if (empty(windowsForm)) {
        windowsForm = windowFormDefaults();
    }
    return windowsForm;
}

export function setWindowsForm(data){
    windowsForm = data;
}

function windowFormDefaults() {
    var windowsForm = {
        width: '',
        height: '',
        openers: {},
        columns: '',
        rows: ''

    };

    return windowsForm;
}

function displaySelected(selector) {

    var height = $(selector).height();
    var width = $(selector).width();

    if (width < 10) {
        width = 250;
    }
    if (height < 10) {
        height = 120;
    }

    var id = $(selector).attr('id');

    var newWidth = width / 2 - 21;
    var newHeight = -1 * (height / 2 + 35);

    var marginLeft = newWidth + 'px';
    var marginTop = newHeight + 'px';

    var checkImage = $('<img>', {
        src: '/img/icon-selected.svg'
    });

    checkImage.css({"margin-left": marginLeft, "margin-top": marginTop, "z-index": 10, "float": "left"});

    var groupOfButtons = $(selector).parent();
    if ($(".selectedDiv", groupOfButtons).length > 0) {
        $(".selectedDiv", groupOfButtons).remove();
        $('.buttonLabel', groupOfButtons).css({"background-color": "#FFFFFF", "color": "#000000"});
    }

    $('.buttonLabel', selector).css({"background-color": "#44ac61", "color": "#FFFFFF"});

    jQuery('<div/>', {
        class: 'selectedDiv'
    }).appendTo($(selector));

    checkImage.appendTo($(".selectedDiv", selector));
}

export function restoreFormState() {

    restoreDimensions();
    checkWidth('yes');
    checkHeight('yes');
    restoreWindowType();
    restoreOpeners();
}


function restoreDimensions() {
    columns = windowsForm.columns;
    rows = windowsForm.rows;
    restoreOpeners();
}


function restoreOpeners() {
    var keys = Object.keys(windowsForm.openers);
    for (var a = 0; a < keys.length; a++) {
        var key = keys[a];
        var value = windowsForm.openers[key];

        selectedWindow = key;
        buildAddOpener("#" + value);
    }
}


function restoreWindowType() {

    if (!empty(windowsForm.windowType)) {

        // var selector = '#' + windowsForm.windowType;
        renderWindowPassedType(windowsForm.windowType);
        // renderWindowType(selector);
    }
}


// Size and sash

'use strict';
var rows;
var columns;
var windowTypes;
var minWidth = 0;
var maxWidth;
var minHeight;
var maxHeight;
var currentHeight;
var currentWidth;



export function setUpWindowHeightAndWidth() {

    var widthSelector = $('#width');
    widthSelector.unbind("keyup").bind("keyup", checkWidth);
    widthSelector.val(windowsForm.width);

    var heightSelector = $('#height');
    heightSelector.unbind("keyup").bind("keyup", checkHeight);
    heightSelector.val(windowsForm.height);
}


function windowMinsAndMaxes() {

    var numberOfWindowChooses = windowTypes.length;

    for (var a = 0; a < numberOfWindowChooses; a++) {

        if (minWidth == 0) {
            minWidth = windowTypes[a].minWidthSize;
            maxWidth = windowTypes[a].maxWidthSize;
            minHeight = windowTypes[a].minHeightSize;
            maxHeight = windowTypes[a].maxHeightSize;
        }

        if (windowTypes[a].minWidthSize < minWidth) {
            minWidth = windowTypes[a].minWidthSize;
        }
        if (windowTypes[a].maxWidthSize > maxWidth) {
            maxWidth = windowTypes[a].maxWidthSize;
        }
        if (windowTypes[a].minHeightSize < minHeight) {
            minHeight = windowTypes[a].minHeightSize;
        }
        if (windowTypes[a].maxHeightSize > maxHeight) {
            maxHeight = windowTypes[a].maxHeightSize;
        }
    }
}


function isBlankValueAndFirstTime(value, firstTime) {

    if (empty(firstTime)) {

        return false;
    }

    if (empty(value)) {
        onlyWidthAndHeightForSash();

        return true;
    }

    return false;
}


function checkHeight(firstTime) {
    var type = 'height';
    checkWidthOrHeightAndFirstTime(type, firstTime);
}


function checkWidth(firstTime) {
    var type = 'width';
    checkWidthOrHeightAndFirstTime(type, firstTime);
}


function checkWidthOrHeightAndFirstTime(type, firstTime) {


    var selector = $('#' + type);
    var value = selector.val();
    if (isBlankValueAndFirstTime(value, firstTime)) {
        return;
    }

    if (isValidTypeWithValue(type, value)) {
        selector.removeClass('backgroundPink');
        setFrameAndMoveToNextSection();
    } else {
        selector.addClass('backgroundPink');
        onlyWidthAndHeightForSash();
    }
}


function isValidTypeWithValue(type, value) {

    if (type == 'width') {
        if (validWidth(value)) {
            return true;
        }
    } else {
        if (validHeight(value)) {
            return true;
        }
    }

    return false;
}


function validWidth(width) {

    if (width >= minWidth && width <= maxWidth) {
        return true;
    }
    return false;
}


function validHeight(height) {

    if (height >= minHeight && height <= maxHeight) {
        return true;
    }
    return false;
}


function onlyWidthAndHeightForSash() {

    $('#windowOptionsSection').hide();
    $('#windowBuilderSection').hide();
}


function setFrameAndMoveToNextSection() {

    currentHeight = $('#height').val();
    currentWidth = $('#width').val();
    windowsForm.width = currentWidth;
    windowsForm.height = currentHeight;
    //windowsForm.openers = {};
    writeWindowsForm();

    if (!validWidth(currentWidth) || !validHeight(currentHeight)) {
        return;
    }

    // var html = buildSectionsAndButtonsForWindows(windowTypes);

    // $('#windowOptions').html(html);
    $('#windowOptions').show();
    $('.windowButton').unbind("click").bind("click", selectWindowType);

    setFrame();
    $('#windowOptionsSection').show();
}


function setFrame() {

    var height = $('#height').val();
    var width = $('#width').val();

    var aspectRatio = width / height;
    var UIAspectRatio = maxUIWidth / maxUIHeight;

    if (aspectRatio > UIAspectRatio) {
        chooseWidthMultiplier(width, height);
    } else {
        chooseHeightMultiplier(width, height);
    }
}


function doesWindowFit(window) {

    if (window.maxHeightSize < currentHeight || window.minHeightSize > currentHeight) {
        return false;
    }
    if (window.maxWidthSize < currentWidth || window.minWidthSize > currentWidth) {
        return false;
    }

    return true;
}


function buildSectionsAndButtonsForWindows(windowOptions) {

    var section = null;
    var windows = [];
    var html = '';

    for (var a = 0; a < windowOptions.length; a++) {

        var window = windowOptions[a];

        if (!doesWindowFit(window)) {
            continue;
        }


        if (empty(section)) {
            section = window.windowSection;
        }
        if (window.windowSection != section) {

            var newSection = buildWindowSection(windows, section);
            var sectionLine = '<div class="windowSeparator"></div>';

            section = window.windowSection;

            html = html + newSection + sectionLine;
            windows = new Array();
        }

        if (splitForFirstOption(window.Layout) == 'V') {
            windows.push(window);
        }
    }

    var sectionEnd = '<div class="sectionEnd"></div>';
    html = html + buildWindowSection(windows, section) + sectionEnd;

    return html;
}


function buildWindowSection(windows, section) {

    var section = '<div class="windowSection"> <img src="/images/windows/sections/Section' + section + '.png"> </div>';
    var buttons = buildWindowOptions(windows);

    return section + buttons;
}


function getWindowTypes() {

    var url = "https://" + apiHostV1 + "/api/windows?module=" + formStatus.module + authorize() + addSite();

    var json = {"width": 1000, "height": 1000};
    json = JSON.stringify(json);

    var myPromise = $.post(url, json, function (data) {
        windowTypes = jQuery.parseJSON(data);
        windowMinsAndMaxes(windowTypes);
    });

    return myPromise;
}

export function processWindowTypes(data){
    windowTypes = data;
    windowMinsAndMaxes(windowTypes);
}


function addWidthsToRow(row) {

    for (var a = 1; a < row.numberOfWindows; a++) {
        row['width' + a] = Math.round(frame.width * xMult / row.numberOfWindows)
    }

    return row;
}

function isEmptyObject(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

export function selectWindowType() {
    columns = '';
    rows = '';
    windowsForm.openers = {};
    windowsForm.rows = {};

    renderWindowType(this);
    //updateWindowPrice();
}

function renderWindowPassedType(type) {

    var res = type.split("-");
    var direction = res.shift();

    windowsForm['windowType'] = type;
    writeWindowsForm();
    displaySelected('#'+type);

    if (direction == 'V') {

        if (isEmptyObject(rows)) {
            rows = buildVerticalData(res);
            windowsForm.rows = rows ;
            writeWindowsForm();
        }
        buildVerticalWindows(rows);

    } else {
        if (isEmptyObject(columns)) {
            columns = buildHorizontalData(res);
        }
        buildHorizontalWindows(columns);
    }

    var section = $('#windowBuilderSection');
    section.show();

    // scrollToPositionAndOffset(section, -200);
    // updateWindowsPrice();
}


function renderWindowType(selector) {

    var type = $(selector).attr('id');
    renderWindowPassedType(type);
}


function buildWindowOptions(windowOptions) {

    var html = '';

    for (var a = 0; a < windowOptions.length; a++) {
        var window = windowOptions[a];
        var button = buildWindowTypeButton(window);

        // if (window.stacking == 'vertical') {
        html = html + button;
        // }
    }

    return (html);
}


function buildWindowTypeButton(window) {

    var id = window.id;

    var button = '' +
        '<div id="' + window.Layout + '" class="windowButton">' +
        '<div class="windowButtonImage"><img src="' + window.img + '"></div>' +
        '</div>';

    return button;
}


// Builder

function addOpener() {

    buildAddOpener(this);
}


function buildAddOpener(selector) {

    var imageType = $(selector).attr('id');
    $('.selectedOption').removeClass('selectedOption');

    var selector = $('#' + selectedWindow);
    var parent = selector.parent();

    windowsForm.openers[selectedWindow] = imageType;
    writeWindowsForm();

    var openerMarginHeight = 15;
    var openerMarginWidth = 18;

    if (imageType == 'none') {
        openerMarginHeight = sash.marginHeight;
        openerMarginWidth = sash.marginWidth;
        selector.css({
            'margin-left': sash.marginWidth + 'px', 'margin-right': sash.marginWidth + 'px',
            'margin-top': sash.marginHeight + 'px', 'margin-bottom': sash.marginHeight + 'px'
        });
    } else {
        selector.css({
            'margin-left': openerMarginWidth + 'px', 'margin-right': openerMarginWidth + 'px',
            'margin-top': openerMarginHeight + 'px', 'margin-bottom': openerMarginHeight + 'px'
        });
    }

    var height = parent.height() - (openerMarginHeight * 2) - 2;
    var width = parent.width() - (openerMarginWidth * 2) - 2;

    selector.height(height);
    selector.width(width);

    selector.html('<img align="left" src="/img/openers/white/' + imageType + 'White.svg" height="' + height + 'px" width="' + width + 'px">');
    selector.removeClass('selectedWindow');
    deActivateOpeners();
}


function addDimensionsToWindows(rows) {

    //Order is Important. Top/Bottom Left/Right Placement needs to be decided before added Total Dimensions
    buildDimensionsVertical(rows);
    buildDimensionsHorizontal(rows);

    buildTotalDimensionsVertical();
    buildTotalDimensionsHorizontal();
}


function deActivateOpeners() {

    var selector = $('.operation');
    selector.css({opacity: 0.3});
    //updateWindowPrice();
    selector.unbind("click").bind("click", disabledOpeners);
}


function disabledOpeners() {

    alert("Select a window first and then choose the direction of opener you wish for that window");
}


function activateOpenerSelection() {

    var selector = $('.operation');
    selector.css({opacity: 1.0});
    selector.unbind("click").bind("click", addOpener);
}


function selectSash() {

    $('.selectedOption').removeClass('selectedOption');
    $('.sash').removeClass('selectedWindow');
    $(this).addClass('selectedWindow');
    selectedWindow = $(this).attr('id');

    var imagePath = $(this).children().first().attr('src');

    if (!empty(imagePath)) {
        var image = imagePath.split("white\/");
        var type = image[1].split("White.svg");
        $('#' + type[0]).addClass('selectedOption');
    }

    activateOpenerSelection();
}


function empty(variable) {

    var status = false;

    if (variable == 'NaN' || variable === undefined || variable === null || variable == '' || variable == 'null' || variable == 'undefined') {
        status = true;
    }
    return status;
}


function updateDimensions() {

    var selector = $(this);

    delay(function () {
        if (isValidHorizontalDimensionWithSelectorAndType(selector, 'vertical')) {
            updateWindows(selector);
            restoreDimensions();
        } else {
            selector.addClass('backgroundPink');
        }
    }, delayAmount);
}


function updateVerticalDimensions() {

    var selector = $(this);

    delay(function () {
        if (isValidVerticalDimensionWithSelectorAndType(selector, "vertical")) {
            updateVerticalWindows(selector);
            restoreDimensions();
        } else {
            selector.addClass('backgroundPink');
        }

    }, delayAmount);
}


function updateVerticalWindows(selector) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var row = parseInt(getOptionIndex(options, 1));
    var type = getOptionIndex(options, 2);

    if (row == rows.length - 1) {

        row = 0;
        var windows = [];
        for (var a = 0; a < rows.length - 1; a++) {
            windows.push({"height": rows[a].height});
        }

        var height = remainingHeight(windows);
        var addHeight = height - value;
        var firstHeight = rows[row].height;
        value = firstHeight + addHeight;

    }

    rows[row][type] = value;

    windowsForm.rows = rows;
    writeWindowsForm();
    buildVerticalWindows(rows);
}


function updateWindows(selector) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var row = getOptionIndex(options, 1);
    row = parseInt(row) - 1;
    var type = getOptionIndex(options, 2);
    var windowNumber = getOptionIndex(options, 3);
    windowNumber = parseInt(windowNumber) + 1;

    if (windowNumber == rows[row].numberOfWindows) {
        var width;
        var windows = [];

        for (var a = 1; a < rows[row].numberOfWindows; a++) {
            width = rows[row]['width' + a];
            windows.push({"width": width});
        }

        width = remainingWidthVertical(windows);
        var firstWidth = width - value;
        var width1 = rows[row].width1;
        value = width1 + firstWidth;
        windowNumber = 1;

    }

    rows[row][type + windowNumber] = value

    windowsForm.rows = rows;
    writeWindowsForm();
    buildVerticalWindows(rows);
}


function splitForSecondOption(option) {

    return getOptionIndex(option, 1);
}


function splitForFirstOption(option) {

    return getOptionIndex(option, 0);
}


export function getOptionIndex(option, index) {

    var arrayOption = '';
    if (option) {
        arrayOption = option.split(/-/);
    }


    if (arrayOption[index]) {
        return arrayOption[index];
    } else {
        return '';
    }
}


function getWidthOfWindows(windows) {

    var width = 0
    for (var a = 0; a < windows.length; a++) {
        width = windows[a].width + width;
    }
    return width;
}


function getHeightOfWindows(windows) {

    var height = 0
    for (var a = 0; a < windows.length; a++) {
        height = windows[a].height + height;
    }
    return height;
}


function remainingHeight(windows) {

    var heigthOfWindows = getHeightOfWindows(windows);

    var remainingSpace = (frame.height * yMult) - heigthOfWindows;
    return remainingSpace;
}


function lessWidth() {

    var lessWidth = frame.marginWidth - border * 3;
    return lessWidth;
}


function lessHeight() {

    var lessHeight = frame.marginHeight - border * 3;
    return lessHeight;
}


function buildHolders() {

    //Order is Important
    horizontalDimensionsHolder();
    verticalDimensionsHolder();
    windowsHolder();
    verticalTotalDimensionHolder();
    horizontalTotalDimensionHolder();
}


function horizontalDimensionsHolder() {

    jQuery('<div/>', {
        id: 'topDimension'
    }).append($('<div>', {
        id: 'horizontalSizes',
        class: 'sizes',
        width: frame.width + border * 3
    })).appendTo('#windowBuilder');
}


function verticalDimensionsHolder() {

    jQuery('<div/>', {
        id: 'verticalSizes',
        class: 'sizes',
        height: frame.height - lessHeight()
    }).appendTo('#windowBuilder');
}


function windowsHolder() {

    jQuery('<div/>', {
        id: 'mainFrame',
        width: frame.width - lessWidth(),
        height: frame.height - lessHeight()
    }).appendTo('#windowBuilder');
}


function verticalTotalDimensionHolder() {

    jQuery('<div/>', {
        id: 'totalVertical',
        class: 'totalVerticalDimension',
        width: 100,
        height: frame.height - lessHeight()
    }).appendTo('#windowBuilder');
}


function horizontalTotalDimensionHolder() {

    jQuery('<div/>', {
        id: 'bottomDimension'
    }).appendTo('#windowBuilder');
}

// Horizontal

'use strict'

function buildHorizontalWindows(columnsPassed) {

    columns = columnsPassed;
    $('#windowBuilder').html('');

    var changedRows = translateColumnsToDimensions(columns);

    buildHolders();
    buildHorizontal(columns);
    addDimensionsToWindows(changedRows);

    $('.dimension').unbind("keyup").bind("keyup", updateDimensionsHorizontal);
    $('.dimensionVertical').unbind("keyup").bind("keyup", updateVerticalDimensionsHorizontal);

    $('#heightWindow').keyup(updateHorizontalHeight);
    $('#widthWindow').keyup(updateHorizontalWidth);

    $('.sash').unbind("click").bind("click", selectSash);
    deActivateOpeners();
}


function numberOfWindowsByType(options, type) {

    if (type == 'vertical') {
        return rows.length;
    } else {
        var columnNumber = getOptionIndex(options, 3);
        var numberOfWindows = columns[columnNumber].numberOfWindows;
        return numberOfWindows;
    }
}


function findRemainingSpaceLeftForRowsForHorizontal(options) {

    var rowNumber = getOptionIndex(options, 1);
    var columnNumber = parseInt(getOptionIndex(options, 3)) + 1;
    var row = rows[rowNumber - 1]

    var skipColumn = columnNumber;
    if (columnNumber == row.numberOfWindows) {
        skipColumn = 1;
    }

    var width = 0;
    for (var a = 1; a <= row.numberOfWindows; a++) {
        if (a != skipColumn && a != row.numberOfWindows) {
            width = width + parseInt(row['width' + a]);
        }
    }

    width = width + parseInt(minWidth);
    var currentWindowSize = $('#widthWindow').val();

    return currentWindowSize - width;
}


function findRemainingSpaceLeftForRows(currentRow) {

    var skipRow = currentRow;

    if (currentRow == rows.length - 1) {
        skipRow = 0;
    }

    var height = 0;
    for (var a = 0; a < rows.length; a++) {
        if (a != skipRow && a != rows.length - 1) {
            height = height + parseInt(rows[a].height);
        }
    }
    height = height + parseInt(minHeight);
    var currentWindowSize = $('#heightWindow').val();

    return currentWindowSize - height;
}


function isValidVerticalDimensionWithSelectorAndType(selector, type) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var rowNumber = getOptionIndex(options, 1);
    var maxWindowHeight = findRemainingSpaceLeftForRows(rowNumber);

    if (isNaN(value)) {
        return false;
    }
    if (value < minHeight) {
        return false;
    }
    if (value > maxHeight || value > maxWindowHeight) {
        return false;
    }

    return true;
}


function updateVerticalDimensionsHorizontal() {

    var selector = $(this);

    delay(function () {
        if (isValidVerticalDimensionWithSelectorAndType(selector, 'horizontal')) {
            updateVerticalWindowsHorizontal(selector);
            restoreDimensions();
        } else {
            selector.addClass('backgroundPink');
        }
    }, delayAmount);
}


function updateDimensionsHorizontal() {

    var selector = $(this);

    delay(function () {

        if (isValidHorizontalDimensionWithSelectorAndType(selector, 'horizontal')) {
            updateWindowsHorizontal(selector);
            restoreOpeners();

        } else {
            selector.addClass('backgroundPink');
        }
    }, delayAmount);
}


function findRemainingSpaceLeftForColumns(currentColumn) {

    var skipColumn = currentColumn;
    if (currentColumn == columns.length - 1) {
        skipColumn = 0;
    }

    var width = 0;
    for (var a = 0; a < columns.length; a++) {

        if (currentColumn)

            if (a != skipColumn && a != columns.length - 1) {
                width = width + parseInt(columns[a].width);
            }
    }

    width = width + parseInt(minWidth);
    var currentWindowSize = $('#widthWindow').val();

    return currentWindowSize - width;
}


function isValidHorizontalDimensionWithSelectorAndType(selector, type) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var numberOfWindows = numberOfWindowsByType(options, type);
    var maxWindowWidth = 0;

    if (type == 'vertical') {
        maxWindowWidth = findRemainingSpaceLeftForRowsForHorizontal(options);
    } else {
        var columnNumber = getOptionIndex(options, 3);
        maxWindowWidth = findRemainingSpaceLeftForColumns(columnNumber);
    }

    if (isNaN(value)) {
        return false;
    }
    if (value < minWidth) {
        return false;
    }
    if (value > maxWidth || value > maxWindowWidth) {
        return false;
    }

    return true;
}


function updateVerticalWindowsHorizontal(selector) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var row = getOptionIndex(options, 1);
    row = parseInt(row) + 1
    var type = getOptionIndex(options, 2);

    var columnNumber = getOptionIndex(options, 3);
    var numberOfRowsInColumn = columns[columnNumber].numberOfWindows;

    var heightField = 'height' + row;

    if (row == numberOfRowsInColumn) {

        var lastHeight = dimensionForLastHeightHorizontal(columnNumber);

        var amountToRemoveFromFirstWidth = value - lastHeight;
        value = columns[columnNumber]['height1'] - amountToRemoveFromFirstWidth;

        heightField = 'height1';
        columns[columnNumber][heightField] = value;
    } else {
        columns[columnNumber][heightField] = value;
    }

    windowsForm.columns = columns;
    writeWindowsForm();

    buildHorizontalWindows(columns);
}


function dimensionForLastHeightHorizontal(columnNumber) {

    var numberOfRowsInColumn = columns[columnNumber].numberOfWindows;
    var height;
    var windows = [];
    for (var a = 1; a < numberOfRowsInColumn; a++) {
        height = columns[columnNumber]['height' + a];
        windows.push({"height": height});
    }
    var lastHeight = remainingHeight(windows);
    return lastHeight;

}


function updateWindowsHorizontal(selector) {

    var value = parseFloat(selector.val());
    var options = selector.attr('id');

    var columnNumber = getOptionIndex(options, 3);
    columnNumber = parseInt(columnNumber);

    if (columnNumber == columns.length - 1) {
        var lastWidth = dimensionForLastWidthHorizontal();
        columns[columnNumber]['width'] = value;
        var amountToRemoveFromFirstWidth = value - lastWidth;
        value = columns[0]['width'] - amountToRemoveFromFirstWidth;
        columnNumber = 0;
        columns[columnNumber]['width'] = value;
    } else {
        columns[columnNumber]['width'] = value;
        var lastColumn = columns.length - 1;
        columns[lastColumn]['width'] = dimensionForLastWidthHorizontal();
    }

    windowsForm.columns = columns;
    writeWindowsForm();

    buildHorizontalWindows(columns);
}


function dimensionForLastWidthHorizontal() {

    var width;
    var windows = [];

    for (var a = 0; a < columns.length - 1; a++) {
        width = columns[a]['width'];
        windows.push({"width": width});
    }

    var lastWidth = remainingWidthVertical(windows);

    return lastWidth;
}


function updateHorizontalHeight() {

    updateHeightDelay('horizontal');
}


function updateHorizontalWidth() {

    updateWidthDelay('horizontal');
}


function buildHorizontalData(res) {

    var numberOfColumns = res.length;
    var width = Math.round(frame.width * xMult / numberOfColumns);

    var columns = [];
    for (var a = 0; a < numberOfColumns; a++) {

        var column = {"numberOfWindows": res[a], "width": width, "number": a};
        column = addHeightToColumn(column);
        columns.push(column);
    }

    return (columns);
}


function chooseBestRowForDimensionsAndSetTopBottom(rows) {

    var maxWindows = 0;
    var rowWithMax;
    for (var a = 0; a < rows.length; a++) {

        var row = rows[a];
        if (row.numberOfWindows > maxWindows) {
            maxWindows = row.numberOfWindows;
            rowWithMax = a;
        }
    }
    chooseDimensionTopBottom = 'top';
    if (rowWithMax == rows.length - 1 && rows.length > 1) {
        chooseDimensionTopBottom = 'bottom';
    }
    var rowWithMostWindows = rows[rowWithMax];

    return rowWithMostWindows;
}


function setUpHorizontalDimensionBasedOnTopOrBottom() {

    if (chooseDimensionTopBottom == 'top') {
        var horizontalSizes = $('#horizontalSizes');
        horizontalSizes.html('');
        horizontalSizes.addClass('newHorizontalSizes');
        $('#bottomDimension').removeClass('bottomDimensionOverOne');

    } else {
        $('#horizontalSizes').removeClass('newHorizontalSizes');
        $('#bottomDimension').html('');
        $('#bottomDimension').addClass('bottomDimensionOverOne');
    }
}


function buildDimensionsHorizontal(rows) {

    var row = chooseBestRowForDimensionsAndSetTopBottom(rows);
    setUpHorizontalDimensionBasedOnTopOrBottom();

    var windows = [];

    var unSelectable = '';
    var readOnly = false;

    if (row.numberOfWindows == 1) {
        unSelectable = 'unSelectable';
        readOnly = true;
        $('#topDimension').css('visibility', 'hidden');
    }

    for (var a = 0; a < row.numberOfWindows; a++) {

        var item = 'width' + parseInt(a + 1);

        var width = row[item];
        if (!isNaN(width)) {
            windows.push({"width": width});
        }

        if (a == row.numberOfWindows - 1) {
            width = remainingWidthVertical(windows);
        }

        var widthShown = width;
        width = width / xMult;
        var firstDim = -1;

        if (row.numberOfWindows == 3) {
            firstDim = -3;
        }
        if (row.numberOfWindows == 4) {
            firstDim = -2;
        }

        if (a == 0) {
            firstDim = 0;
            if (row.numberOfWindows == 4) {
                firstDim = -1;
            }
        }

        var push = parseInt(row.numberOfWindows) - 2;
        if (push < 0) {
            push = 0;
        }
        push = 0

        var divWidth = ((parseInt(width) + parseInt(push)) / 2 ) - 33;
        var divWidth1 = divWidth - firstDim;
        var id = "row-" + row.number + "-width-" + parseInt(a);

        if (chooseDimensionTopBottom == 'top') {

            $('<div>', {
                class: 'horizontalBox'
            }).append($('<div>', {
                class: 'leftHorizontal dimensionBorder',
                width: parseInt(divWidth)
            })).append($('<input/>', {
                id: id,
                type: 'text',
                class: 'middleTextHorizontal dimension ' + unSelectable,
                readonly: readOnly,
                value: parseInt(widthShown)
            })).append($('<div>', {
                class: 'rightHorizontal dimensionBorder' + ((a==row.numberOfWindows - 1) ? ' last' : ''),
                width: parseInt(divWidth1)
            })).appendTo('#horizontalSizes');
        } else {
            $('<div>', {
                class: 'horizontalBox'
            }).append($('<div>', {
                class: 'leftHorizontalBottom dimensionBorder',
                width: parseInt(divWidth)
            })).append($('<input/>', {
                id: id,
                type: 'text',
                class: 'middleTextHorizontalBottom dimension ' + unSelectable,
                readonly: readOnly,
                value: parseInt(widthShown)
            })).append($('<div>', {
                class: 'rightHorizontalBottom dimensionBorder' + ((a==row.numberOfWindows - 1) ? ' last' : ''),
                width: parseInt(divWidth1)
            })).appendTo('#bottomDimension');
        }
    }
}


function addHeightToColumn(column) {

    for (var a = 1; a < column.numberOfWindows; a++) {
        column['height' + a] = Math.round(frame.height * yMult / column.numberOfWindows)
    }

    return column;
}


function getColumnOfWindowsForDimensions(columns) {

    var column = 0;

    for (var a = 0; a < columns.length; a++) {
        var now = columns[a].numberOfWindows;
        var then = columns[column].numberOfWindows
        if (now > then) {
            column = a;
        }
    }

    chooseDimensionLeftRight = 'left';
    if (parseInt(column) + 1 == columns.length && columns.length != 1) {
        chooseDimensionLeftRight = 'right';
    }

    return columns[column];
}


function addWidthToDimensions(columns) {

    //var column = getColumnOfWindowsForDimensions(columns);
    //var width = column.width;

    var numberOfColumns = columns.length;

    var row = {"numberOfWindows": numberOfColumns};
    for (var a = 0; a < columns.length; a++) {
        row['width' + (parseInt(a) + 1)] = columns[a].width;
    }

    row['width' + numberOfColumns] = remainingWidthForColumns(columns);


    return row;
}


function translateColumnsToDimensions(columns) {

    var column = getColumnOfWindowsForDimensions(columns);
    var numberOfWindows = column.numberOfWindows;

    //Builds for Horizontal Dimensions
    var row = addWidthToDimensions(columns);

    //Builds for Vertical Dimensions
    var changedRows = [];
    for (var a = 1; a < numberOfWindows; a++) {
        row.height = column['height' + a];
        row.number = parseInt(a);
        row.column = column;
        var newRow = JSON.parse(JSON.stringify(row));
        changedRows.push(newRow);
    }

    var lastHeight = remainingHeight(changedRows);
    row.number = parseInt(numberOfWindows);
    row.height = lastHeight;
    var newRow2 = JSON.parse(JSON.stringify(row));
    changedRows.push(newRow2);

    return changedRows;
}


function remainingWidthForColumns(windows) {

    var widthOfWindows = getWidthOfWindows(windows);
    var remainingSpace = (frame.width * xMult) - widthOfWindows;

    return remainingSpace;
}


function buildHorizontal(columns) {

    var windows = [];

    for (var a = 0; a < columns.length - 1; a++) {
        var column = columns[a];
        windows.push({"width": column.width});
        column.columnNumber = parseInt(a) + 1;
        buildColumn(column, columns.length);
    }

    var width = remainingWidthForColumns(windows);
    var lastColumn = columns[columns.length - 1];
    lastColumn.columnNumber = columns.length;
    lastColumn.width = width;

    buildColumn(lastColumn, columns.length);
}


function buildColumn(column, numberOfColumns) {

    var frameWidthToRemove = widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, column.columnNumber);

    jQuery('<div/>', {
        id: 'column' + column.number,
        class: 'holder',
        width: parseInt(column.width / xMult) - parseInt(frameWidthToRemove) + 12,
        height: frame.height
    }).appendTo('#mainFrame');

    var size = {
        "columnNumber": column.columnNumber,
        "width": column.width,
        "place": '#column' + column.number,
        "column": column.number,
        "numberOfWindows": parseInt(column.numberOfWindows)
    };

    var windows = [];
    for (var a = 1; a < column.numberOfWindows; a++) {

        var height = column['height' + a];

        windows.push({"height": height});
        size.number = parseInt(a);
        size.height = height;
        size.column = a;
        addWindowHorizontal(size, numberOfColumns);
    }

    size.column = column.numberOfWindows;
    size.number = parseInt(column.numberOfWindows);
    size.height = remainingHeight(windows);
    size.numberOfWindows = parseInt(column.numberOfWindows);
    size.row = column.number;

    addWindowHorizontal(size, numberOfColumns);
}


function widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, columnNumber) {

    var numberOfMargins = parseInt(numberOfColumns) + 1;

    var frameWidthToRemove = parseInt(frame.marginWidth) + parseInt(2 * border) - 1;
    if (columnNumber == 1) {
        frameWidthToRemove = ((frame.marginWidth * numberOfMargins ) / numberOfColumns ) + (border * 2 * numberOfColumns) - 1;
    }
    if (columnNumber == numberOfColumns) {
        frameWidthToRemove = ((frame.marginWidth * numberOfMargins ) / numberOfColumns ) + (border * 2 * numberOfColumns) - 2;
    }

    return frameWidthToRemove;
}


function addWindowHorizontal(size, numberOfColumns) {

    var numberOfRows = parseInt(size.numberOfWindows);

    var rowsMultiplier = parseInt(numberOfRows);
    var frameHeightToRemove = ( (frame.marginHeight * rowsMultiplier) + (2 * border * rowsMultiplier) ) / numberOfRows;

    if (size.number == 1) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }
    if (size.number == numberOfRows) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }

    var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

    var frameWidthToRemove = widthOfColumnToRemoveBasedOnColumnsAndSpecificColumn(numberOfColumns, size.columnNumber);
    var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);

    jQuery('<div/>', {
        class: 'frame',
        width: parseInt(size.width / xMult) - parseInt(frameWidthToRemove),
        height: parseInt(size.height / yMult) - frameHeightToRemove
    }).append($('<div>', {
        id: 'window-' + size.row + "-" + size.column,
        class: 'sash',
        width: (size.width / xMult) - sashWidthToRemove,
        height: (size.height / yMult) - sashHeightToRemove
    })).appendTo(size.place);
}


function buildTotalDimensionsHorizontal() {

    var margins = 13;
    var textFieldWidth = 52 + margins;
    var dimensionWidth = (frame.width - textFieldWidth) / 2;

    if (chooseDimensionTopBottom == 'bottom') {
        buildTotalDimensionsForBottom(dimensionWidth);
    } else {
        buildTotalDimensionsForTop(dimensionWidth)
    }
}


function buildTotalDimensionsForTop(dimensionWidth) {

    $('#bottomDimension').html('');
    jQuery('<div>', {
        id: 'totalHorizontal',
        class: 'totalHorizontalDimension',
        width: frame.width + 2
    }).append($('<div>', {
        class: 'bottomDimensionLeft dimensionBorder',
        width: dimensionWidth - 1
    })).append($('<input/>', {
        id: "widthWindow",
        type: 'text',
        class: 'dimensionBottom unSelectable unSelectableBumpDown',
        readOnly: true,
        value: parseInt(frame.width * xMult)
    })).append($('<div>', {
        class: 'bottomDimensionRight dimensionBorder',
        width: dimensionWidth
    })).appendTo('#bottomDimension');

}


function buildTotalDimensionsForBottom(dimensionWidth) {

    $('#horizontalSizes').html('');

    jQuery('<div>', {
        id: 'totalHorizontal',
        class: 'totalHorizontalDimension',
        width: frame.width + 2
    }).append($('<div>', {
        class: 'topDimensionLeft dimensionBorder',
        width: dimensionWidth
    })).append($('<input/>', {
        id: "widthWindow",
        type: 'text',
        class: 'dimensionTop unSelectable unSelectableBumpUp',
        readOnly: true,
        value: parseInt(frame.width * xMult)
    })).append($('<div>', {
        class: 'topDimensionRight dimensionBorder',
        width: dimensionWidth
    })).appendTo('#horizontalSizes');

}

// Vertical

function buildVerticalWindows(rowsPassed) {

    $('#windowBuilder').html('');

    buildHolders();
    buildVertical(rows);

    // return
    addDimensionsToWindows(rows);

    $('.dimension').keyup(updateDimensions);
    $('.dimensionVertical').unbind("keyup").bind("keyup", updateVerticalDimensions);

    $('#heightWindow').keyup(updateVerticalHeight);
    $('#widthWindow').keyup(updateVerticalWidth);

    $('.sash').unbind("click").bind("click", selectSash);
    deActivateOpeners();
    writeWindowsForm();
    // updateWindowPrice();
}


function buildTotalDimensionsVertical() {

    var margins = 5;
    var textFieldHeight = 22 + margins;
    var dimensionHeight = (frame.height - textBoxHeight) / 2 - lessHeight() / 2;

    if (chooseDimensionLeftRight == 'left') {
        buildTotalVerticalDimensionRightWithHeight(dimensionHeight);
    } else {
        buildTotalVerticalDimensionLeftWithHeight(dimensionHeight);
    }
}


function buildTotalVerticalDimensionLeftWithHeight(height) {

    $('#verticalSizes').html('');
    jQuery('<div>', {
        class: 'dimensionVerticalBox'
    }).append($('<div>', {
        class: 'rightDimensionTop dimensionBumpUpOne dimensionBorder',
        height: height
    })).append($('<input/>', {
        id: "heightWindow",
        type: 'text',
        class: 'dimensionVertical unSelectable unSelectableBumpLeft',
        readOnly: true,
        value: parseInt(frame.height * yMult)
    })).append($('<div>', {
        class: 'rightDimensionBottom dimensionBorder',
        height: height
    })).appendTo('#verticalSizes');
}

function buildTotalVerticalDimensionRightWithHeight(height) {


    $('#totalVertical').html('');
    jQuery('<div>', {
        class: 'dimensionVerticalBox'
    }).append($('<div>', {
        class: 'leftDimensionTop dimensionBorder',
        height: height
    })).append($('<input/>', {
        id: "heightWindow",
        type: 'text',
        class: 'dimensionVertical unSelectable unSelectableBumpLeft',
        readOnly: true,
        value: parseInt(frame.height * yMult)
    })).append($('<div>', {
        class: 'leftDimensionBottom dimensionBorder',
        height: height
    })).appendTo('#totalVertical');
}


function clearOutCorrectionDimensionForVertical() {

    if (chooseDimensionLeftRight == 'left') {
        $('#verticalSizes').html('');
        $('#totalVertical').removeClass('newTotalVertical');
    } else {
        var totalVertical = $('#totalVertical');
        totalVertical.addClass('newTotalVertical');
        totalVertical.html('');
    }
}


function buildDimensionsVertical(rows) {
    clearOutCorrectionDimensionForVertical();
    buildDimensionsForVertical(rows);
}


function buildDimensionsForVertical(rows) {

    var numberOfRows = rows.length;

    var unSelectable = '';
    var readOnly = false;
    if (numberOfRows == 1) {
        unSelectable = 'unSelectable';
        readOnly = true;
        $('#verticalSizes').css('visibility', 'hidden');
    }

    for (var a = 0; a < numberOfRows; a++) {

        var firstRow = 1;

        if (a == 0) {
            firstRow = 0;
        }

        var height = rows[a].height / yMult;

        var divHeight = ((height - textBoxHeight ) / 2) - (lessHeight() / numberOfRows - 1);
        var divHeight1 = divHeight;
        var columnNumber = 'na';
        if (!empty(rows[a].column)) {
            columnNumber = rows[a].column.number;
        }

        if (chooseDimensionLeftRight == 'left') {

            $('<div>', {
                class: 'dimensionVerticalBox'
            }).append($('<div>', {
                class: 'topLeft',
                height: divHeight1
            })).append($('<div>', {
                class: 'topRight hasBorderLeftTop dimensionBorder',
                height: divHeight1
            })).append($('<input/>', {
                id: "row-" + a + "-height-" + columnNumber,
                type: 'text',
                class: 'dimensionVertical dimensionVerticalLeft ' + unSelectable,
                readOnly: readOnly,
                value: parseInt(rows[a].height)
            })).append($('<div>', {
                class: 'bottomLeft',
                height: divHeight
            })).append($('<div>', {
                class: 'bottomRight hasBorderLeftBottom dimensionBorder',
                height: divHeight
            })).appendTo('#verticalSizes');

        } else {

            $('<div>', {
                class: 'dimensionVerticalBox'
            }).append($('<div>', {
                class: 'topLeft hasBorderRightTop dimensionBorder',
                height: divHeight1
            })).append($('<div>', {
                class: 'topRight',
                height: divHeight1
            })).append($('<input/>', {
                id: "row-" + a + "-height-" + columnNumber,
                type: 'text',
                class: 'dimensionVertical dimensionVerticalLeftPlus ' + unSelectable,
                readOnly: readOnly,
                value: parseInt(rows[a].height)
            })).append($('<div>', {
                class: 'bottomLeft hasBorderRightBottom dimensionBorder',
                height: divHeight
            })).append($('<div>', {
                class: 'bottomRightRight',
                height: divHeight
            })).appendTo('#totalVertical');
        }
    }
}

function updateVerticalWidth() {

    updateWidthDelay('vertical');
}

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

function updateWidthDelay(type) {

    var width = $('#widthWindow').val();
    if (width < 300) {
        $('#widthWindow').addClass('backgroundPink');
        return;
    }
    delay(function () {
        updateFrameDimensions(type);
    }, delayAmount);
}

function updateVerticalHeight() {
    updateHeightDelay('Vertical');
}


function updateHeightDelay(type) {

    var height = $('#heightWindow').val();
    if (height < 150) {
        return;
    }
    delay(function () {
        updateFrameDimensions(type);
    }, delayAmount);
}


function chooseWidthMultiplier(width, height) {

    xMult = width / maxUIWidth;
    yMult = 1 * xMult;
    frame.height = height / yMult;
    frame.width = width / xMult;

    tempLog(width, height)
}


function tempLog(width, height) {

    return;
    f1log(width, "Entered Width");
    f1log(height, "Entered Height");
    f1log(frame.width, "Frame Width");
    f1log(frame.height, "Frame Height");
    f1log(xMult, "xMult");
    f1log(yMult, "yMult");
    f1log(maxUIWidth, "max Width");
    f1log(maxUIHeight, "max Height");
}


function chooseHeightMultiplier(width, height) {

    yMult = height / maxUIHeight;
    xMult = 1 * yMult;
    frame.width = width / xMult;
    frame.height = height / yMult;

    tempLog(width, height);
}


function updateFrameDimensions(type) {

    setFrame();

    if (type == 'horizontal') {
        buildHorizontalWindows(columns);
    } else {
        buildVerticalWindows();
    }
}


function buildVerticalData(res) {

    var numberOfRow = res.length;
    var height = Math.round(frame.height * yMult / numberOfRow);

    var rows = [];
    for (var a = 0; a < numberOfRow; a++) {

        var row = {"numberOfWindows": res[a], "height": height, "number": a};
        row = addWidthsToRow(row);
        rows.push(row);
    }

    return (rows);
}


function buildVertical(rows) {

    var numberOfRows = parseInt(rows.length);

    $('#mainFrame').html('');

    var windows = [];
    for (var a = 0; a < numberOfRows - 1; a++) {
        var row = rows[a];
        row.number = a + 1;
        windows.push({"height": row.height});
        buildRow(row, numberOfRows);
    }

    var height = remainingHeight(windows);

    var lastRow = rows[numberOfRows - 1];
    lastRow.number = numberOfRows;
    lastRow.height = height;

    buildRow(lastRow, numberOfRows);
}


function remainingWidthVertical(windows) {

    var widthOfWindows = getWidthOfWindows(windows);
    var remainingSpace = (frame.width * xMult) - (widthOfWindows);

    return remainingSpace;
}

function buildRow(row, numberOfRows) {

    var mySize = {
        "height": row.height,
        "place": '#mainFrame',
        "number": row.number,
        "numberOfWindows": row.numberOfWindows
    }

    var windows = [];
    for (var a = 1; a < row.numberOfWindows; a++) {

        var newWidth = row['width' + a]
        mySize.width = newWidth;
        mySize.column = a;
        windows.push({"width": mySize.width});
        addWindowVertical(mySize, numberOfRows);

    }

    mySize.column = row.numberOfWindows;
    mySize.width = remainingWidthVertical(windows);
    addWindowVertical(mySize, numberOfRows);

}


function addWindowVertical(size, numberOfRows) {

    var rowsMultiplier = parseFloat(numberOfRows);
    var frameHeightToRemove = ( (frame.marginHeight * rowsMultiplier) + (2 * border * rowsMultiplier) ) / numberOfRows;
    if (size.number == 1 || size.number == numberOfRows) {
        frameHeightToRemove = frameHeightToRemove + 6;
    }

    var sashHeightToRemove = frameHeightToRemove + (sash.marginHeight * 2) + (border * 2);

    var windowMultiplier = parseFloat(size.numberOfWindows);

    var frameWidthToRemove = ( (frame.marginWidth * windowMultiplier) + (2 * border * windowMultiplier) ) / size.numberOfWindows;

    if (size.numberOfWindows == 1) {
        frameWidthToRemove = frameWidthToRemove + 12;
    } else if (size.column == 1 || size.column == size.numberOfWindows) {
        frameWidthToRemove = frameWidthToRemove + 6;
    }

    var sashWidthToRemove = frameWidthToRemove + (sash.marginWidth * 2) + (border * 2);

    jQuery('<div/>', {
        class: 'frame',
        width: ( size.width / xMult) - frameWidthToRemove,
        height: (size.height / yMult) - frameHeightToRemove
    }).append($('<div>', {
        id: 'window-' + size.number + "-" + size.column,
        class: 'sash',
        width: (size.width / xMult) - sashWidthToRemove,
        height: (size.height / yMult) - sashHeightToRemove
    })).appendTo(size.place);

}
