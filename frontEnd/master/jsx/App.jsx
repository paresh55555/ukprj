import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router,useRouterHistory, Route, Link, hashHistory, browserHistory, IndexRoute } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { createHistory } from 'history';
import {authRouteResolver} from './resolver';

import * as Auth from './services/authService'

import configureStore from './store';

//const history = useRouterHistory(createHistory)({basename: '/'});
const store = configureStore();

const history = syncHistoryWithStore(browserHistory, store)
//const syncedHistory = syncHistoryWithStore(history, store);
import initTranslation from './components/Common/localize';
import initLoadCss from './components/Common/load-css';

import Base from './components/Layout/Base';
import BasePage from './components/Layout/BasePage';
import BaseHorizontal from './components/Layout/BaseHorizontal';

import NotFound from './components/Common/NotFound.jsx';
import Error from './components/Common/Error.jsx';

import Login from './components/Auth/Login.jsx';
import Home from './components/Home/Home.jsx';
import Guest from './views/guest/Guest.js';
import System from './views/system/System.js';
import SystemComponent from './views/SystemComponent/SystemComponent.js';
import WindowBuilder from './components/WindowBuilder/WindowBuilder';

// Init translation system
initTranslation();
// Init css loader (for themes)
initLoadCss();

function requireAuth(nextState, replace) {
  if (!Auth.loggedIn()) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}

window.store = store;

ReactDOM.render(
    <Provider store={store}>
    <Router history={history}>
        <Route path="/" component={BaseHorizontal} >
            <IndexRoute component={Guest} />
        </Route>
        <Route path="/" component={BaseHorizontal} onEnter={requireAuth} >
            {/* Default route*/}
            <Route path="home" component={Home} />
            <Route path="systemGroups" component={System} />
            <Route path="systems/:id" component={SystemComponent} />
        </Route>
        <Route path="/" component={BaseHorizontal} >
            <IndexRoute component={Guest} />
            {/* Default route*/}
            <Route path="guest" component={Guest} />
            <Route path="login" component={Login}/>
            <Route path="*" component={NotFound}/>
            <Route path="error" component={Error}/>
            <Route path="notfound" component={NotFound}/>
        </Route>


        
    </Router>
    </Provider>,
    document.getElementById('app')
);
