/* @flow */
import * as apiUtils from './api-utils'

export function getSystemGroup() : Promise {
  return apiUtils.get('/systemGroups/ux').then((response) => response.data)
}
export function getSystemComponent(id) : Promise {
    return apiUtils.get(`/systems/${id}/ux`).then((response) => response.data)
}