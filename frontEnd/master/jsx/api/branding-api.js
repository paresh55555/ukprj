/* @flow */
import * as apiUtils from './api-utils'

export function getSiteBranding() : Promise {
    return apiUtils.get(`/siteBranding`).then((response) => response.data)
}