import * as types from '../actions/actionTypes';
import {getPanelRanges} from '../actions/component';

export default store => next => action => {
    if(action.type === types.SET_COMPONENT_SIZE) {
        const cart = Object.assign({}, store.getState().component.cart, {width: action.width, height: action.height});
        localStorage.setItem('cart', JSON.stringify(cart))
        if(action.width && action.height) {
            store.dispatch(getPanelRanges(action.id, action.width, action.height));
        }
    }
    if(action.type === types.SET_PANELS_AMOUNT) {
        const storeCart = JSON.parse(localStorage.getItem('cart'));
        const cart = Object.assign({}, storeCart, {panels: action.amount});
        localStorage.setItem('cart', JSON.stringify(cart));
    }
    if(action.type === types.SET_SWINGDOORDIRECTION) {
        const cart = Object.assign({}, store.getState().component.cart, {swingDoorDirection: action.direction});
        localStorage.setItem('cart', JSON.stringify(cart))
    }
    if(action.type === types.SET_PANEL_MOVEMENT) {
        const cart = Object.assign({}, store.getState().component.cart, {numberLeft: action.numberLeft});
        localStorage.setItem('cart', JSON.stringify(cart))
    }
    return next(action)
}