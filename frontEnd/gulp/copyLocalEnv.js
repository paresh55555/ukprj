// copyDevEnv.js

var gulp    = require('gulp');
var configs = require('./configs');
var $ = require('gulp-load-plugins')();

gulp.task('copyLocalEnv', function() {
    configs.log('Copying needed Local Assests for Dev Server');
    // Minify and copy all JavaScript (except vendor scripts)
    return gulp.src('./dist/**/*')
        .pipe(gulp.dest(configs.paths.prod));
});
