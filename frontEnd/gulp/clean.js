// clean.js

var gulp    = require('gulp');
var configs = require('./configs');
var del = require('del');
var $ = require('gulp-load-plugins')();
var gulpsync = $.sync(gulp);


// Remove all files from dist folder
gulp.task('cleanDist', function(done) {
    configs.log('Clean dist folder..');
    return del(configs.paths.dist, {
        force: true // clean files outside current directory
    });
});

gulp.task('cleanBuild', function(done) {
    configs.log('Cleaning build folder..');
    return del(configs.paths.build, {
        force: true // clean files outside current directory
    });
});


gulp.task('cleanProd', function(done) {
    configs.log('Cleaning prod folder..');
    return del(configs.paths.prod, {
        force: true // clean files outside current directory
    });
});


gulp.task('cleanAll', gulpsync.sync([
    'cleanBuild',
    'cleanDist',
    'cleanProd'
]) );



// gulp.task('cleanAll', function(done) {
//     configs.log('Clean dist folder..');
//     del(configs.paths.dist, {
//         force: true // clean files outside current directory
//     });
//     del(configs.paths.build, {
//         force: true // clean files outside current directory
//     });
//         del(configs.paths.prod, {
//         force: true // clean files outside current directory
//     });
// });